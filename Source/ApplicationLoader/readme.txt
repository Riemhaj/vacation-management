﻿W3L AG
http://www.w3l.de/

RADApplicationLoader.exe

Parameters: 


  -i, --init         (Default: False) Initialize the runtime environment for
                     the first time.

  -y, --overwrite    (Default: False) Overwrite the application contents if
                     exists.

  -d, --drop         (Default: False) Drop and create an empty database while
                     deploying the application.

  -e, --debug        (Default: False) Enable debug mode using process
                     attachment.

  -r, --runtime      (Default: ..\Runtime) Specifies the base directory for the
                     runtime environment.

  -a, --app          (Default: ..\ApplicationDir) Specifies the directory for
                     the application contents.

  -p, --port         (Default: ) Specifies the listening port number of the web
                     server.

  -l, --locator      (Default: ) Specifies the path (uri locator) of the
                     application.

  -t, --templatestyle Specifies the default template of the application.

  -x, --x86           Force the application run in 32-bit mode.

  --help             Display this help screen.