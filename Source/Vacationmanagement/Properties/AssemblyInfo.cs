﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using RADFoundation.ProblemDomain.Meta.Attributes;


[assembly: AssemblyTitle("Vacationmanagement")]
[assembly: AssemblyDescription("Vacationmanagement")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("Vacationmanagement")]
[assembly: AssemblyCompany("Riem")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDefaultAlias("Vacationmanagement")]
[assembly: AssemblyDefaultIconMeta("Vacationmanagement.png")]
[assembly: ComVisible(false)]
[assembly: Guid("25041083-CCD6-4113-85B5-CA0BC045F15B")]
[assembly: AssemblyVersion("1.0")]
[assembly: AssemblyFileVersion("1.0")]
[assembly: AssemblyInformationalVersion("1.0")]
[assembly: ExtensionModuleMeta(DisplayName = "[DE:Vacationmanagement]", Priority = 0, ViewExtensionFactory = "VMStartPageViewFactory", NavigationTreeRelevant = true, MenuRelevant = true, GeneratingWith = "", AvailableISOLanguages = new string[] { "DE" })]

/*!<W3LJ2-PrivateCode>!*/
/*!</W3LJ2-PrivateCode>!*/
