﻿define('VacationmanagementHTMLViewExtension/UIListeners/SampleUIListener', ['UIListener/UIListener', 'Management/ComponentManager'], function (UIListener, ComponentManager) {
    'use strict';

    /*
     * LayoutFactory: SampleLayoutFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleUIListener() {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        UIListener.call(self);

        /*
         * Methode zum Erzeugen einer UIListener-Chain, bestehend aus mehreren UIListenern.
         * self.createChain('UIListener1', 'UIListener2', 'UIListener3', [...]);
         */

        /*
         * Callback-Operation bevor Save ausgeführt wird.
         * @param {context} - Der aktuelle DetailViewKontext
         * @return {bool} - undefined = true
         */
        self.onBeforeSave = function (context) {
            /*
             * DETAILVIEWCONTEXT
             */

            /*
             * Zugriff auf einen Observable-Value aus dem GenericObject
             * var value = context.genericObject.getObservable(self.detailViewItem.name);
             *
             * Zugriff auf einen reinen Werte aus dem GenericObject
             * var value = context.genericObject.getValue(self.detailViewItem.name);
             */

             /*
              * Ein-/Ausblenden einer Control-/GroupFactory
              * context.getUIFactory('{NAME}').isVisible(true|false);
              * *-Operator verfügbar
              */

            /*
             * Hinzufügen/Entfernen von Pflichtfeldmarkierungen
             * context.getUIFactory('{NAME}').isRequired(true|false);
             */

            /*
             * Aktivieren/Deaktivieren einer ControlFactory
             * context.getUIFactory('{NAME}').isEnabled(true|false);
             */

            /*
             * Zugriff auf das "Tuple" einer Control-/GroupFactory
             * var tuple = context.getCreatedTuple('{NAME}');
             * var control = tuple.control;
             * var factory = tuple.factory;
             */

            /*
             * Zugriff auf das von der DetailView erzeugte Group-/ControlTuples
             * var tuples = context.getCreatedTuples();
             * var groupTuples = context.getCreatedGroupTuples();
             * var controlTuples = context.getCreatedControlTuples();
             * var controlTuples = groupTuples[0].getCreatedControlTuples();
             */

            /*
             * Aktivierung/Deaktivierung eines ToolBar-Buttons auf Basis eines Wertes im GenericObject
             * var link = context.toolBar.getElementByMethodName('{METHOD}').isEnabled;
             * var aktiv = context.genericObject.getObservable('Aktiv');
             * aktiv.sync(link);
             *
             * var link = context.toolBar.getElementByMethodName('{METHOD}').isEnabled;
             * var inaktiv = context.genericObject.getObservable('Inaktiv');
             * inaktiv.sync(link, function (value) { return !value; });
             */
        };

        /*
         * Callback-Operation bevor Cancel ausgeführt wird.
         * @param {context} - Der aktuelle DetailViewKontext
         * @return {bool} - undefined = true
         */
        self.onBeforeCancel = function (context) {

        };

        /*
         * Callback-Operation bevor Apply ausgeführt wird.
         * @param {context} - Der aktuelle DetailViewKontext
         * @return {bool} - undefined = true
         */
        self.onBeforeApply = function (context) {

        };

        /*
         * Callback-Operation, welche aufgerufen wird, nachdem das GenericObject gespeichert wurde.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {bool} isClosing - Gibt an, ob die DetailView geschlossen wird
         */
        self.onAfterApply = function (context, isClosing) {

        };

        /*
         * Callback-Operation, wenn eine Property veraendert wurde.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {string} property - Name des geänderten Property
         */
        self.onPropertyValueUpdated = function (context, property) {

        };

        /*
         * Callback-Operation, welche aufgerufen wird, sobald die DetailView geladen wurde.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {string} property - Name des geänderten Property
         */
        self.onDetailViewReady = function (context) {
			
        };

        /*
         * Wird aufgerufen, wenn die DetailView geschlossen wurde.
         * @param {context} - Der aktuelle DetailViewKontext
         */
        self.onDetailViewClosed = function (context) {

        };

        /*
         * Callback-Operation, wenn eine Assoziation verändert wurde.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {string} relation - Der Name der Relation
         */
        self.onAssociationUpdated = function (context, relation) {
			
        };

        /*
         * Callback-Operation, welche aufgerufen wird, nachdem eine Operation durchgeführt wurde.
         * @param {context} - Der aktuelle DetailViewKontext/ListViewKontext
         * @param {string} operation - Der Name der Operation
         */
        self.onAfterCallOperation = function (context, operation, args) {

        };

        /*
         * Callback-Operation, welche aufgerufen wird, bevor eine Operation durchgeführt wurde.
         * @param {context} - Der aktuelle DetailViewKontext/ListViewKontext
         * @param {string} operation - Der Name der Operation
         * @return {bool} - undefined = true
         */
        self.onBeforeCallOperation = function (context, operation) {

        };

        /*
         * Wird aufgerufen, bevor die DetailView zum ersten mal gerendert wird.
         * @param {context} - Der aktuelle DetailViewKontext
         */
        self.onBeforeRender = function (context) {

        };

        /*
         * Benachrichtigung, bevor eine Assoziation geändert wird.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {string} relation - Der Name der Relation
         * @param {string} actionType ['Connect', 'Disconnect', 'DisconnectAndDelete']
         */
        self.onBeforeAssociationUpdated = function (context, relation, actionType) {

        };

        /*
         * Wird aufgerufen, sobald die DetailView neu gezeichnet wird.
         * @param {context} - Der aktuelle DetailViewKontext/ListViewKontext
         */
        self.onAfterRefresh = function (context) {

        };

        /*
         * Wird aufgerufen, wenn Properties im GenericObject aufgrund einer serverseitigen Aufforderung aktualisiert wurden.
         * Ein mögliches Szenario ist die Neuberechnung von Werten, wenn sich eine abgeleitete Eigenschaft geändert hat.
         * @param {context} - Der aktuelle DetailViewKontext
         * @param {array} updatedProperties - Die Namen der geänderten Properties
         */
        self.onGenericObjectUpdated = function (context, updatedProperties) {

        };

        /*
         * Callback-Operation, welche aufgerufen wird, sobald die ListView geladen wurde.
         * @param {context} - Der aktuelle ListViewKontext
         */
        self.onListViewReady = function (context) {

        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleUIListener.register = function (path) {
        ComponentManager.setUIListener('SampleUIListener', path);
    };

    return SampleUIListener;
});