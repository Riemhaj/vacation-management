﻿define('VacationmanagementHTMLViewExtension/Messages/SampleMessage', ['Messages/Message', 'Management/ComponentManager'], function (Message, ComponentManager) {
    'use strict';

    /*
     * Message: SampleMessage
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleMessage(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {message} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        Message.call(self, params);
    }

    /*
     * Statische Registrierungsfunktion
     * Die Message kann über das Modul "Utility/Messenger" und die Methode "createMessageAndSend" versendet werden
     * Die Methode "registerForMessageId" (MessageId: SampleMessage) erzeugt eine neue Abhängigkeit
     * @register
     */
    SampleMessage.register = function (path) {
        ComponentManager.setMessage('SampleMessage', path);
    };

    return SampleMessage;
});