﻿define('VacationmanagementHTMLViewExtension/SummaryViews/SampleSummaryView', ['SummaryViews/SummaryView', 'Management/ComponentManager', 'ext!VacationmanagementHTMLViewExtension.SummaryViews.SampleSummaryView.html'], function (SummaryView, ComponentManager, layout) {
    'use strict';

    /*
     * SummaryView: SampleSummaryView
     * @param {viewModel} viewModel - Eine Instanz des SampleSummaryViewModel
     * @param {jquery} element - Ein jQuery-Element auf dem die SummaryView erzeugt wird (optional)
     * @constructor
     */
    function SampleSummaryView(viewModel, element) {
        var self = this;
        var summaryView;

        /*
         * Aufruf der Oberklasse
         * @param {summaryView} self - Aufruf der Oberklasse auf der aktuellen Instanz
         */
        SummaryView.call(self);

        /*
         * - Erzeugung der SummaryView
         * - Hinzufügen von HTML-Templates
         */
        self.getSummaryView = function () {
            if (summaryView != null) {
                return summaryView;
            }

            /*
             * Anwenden eines HTML-Templates
             * @param {html} layout - In das Modul injiziertes HTML-Template
             * @param {summaryViewModel} viewModel - Das übergebene SummaryViewModel
             */
            summaryView = self.applyViewModel(layout, viewModel);
            
            /*
             * Anwenden von Bindings, die in allen Controls enthalten sein sollten
             * @param {jquery} summaryView - Das Element, auf das die Bindings angewendet werden
             * @param {object} - JS-Objekt mit Bindings:
             *
             * - controlName: Definiert den Namen der SummaryView im DOM und fügt ihm die Klasse "c-samplesummaryview" hinzu. Dient der Referenzierung in Stylesheets
             */
            self.applyBindings(summaryView, {
                controlName: 'samplesummaryview'
            });

            return summaryView;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleSummaryView.register = function (path) {
        ComponentManager.setSummaryView('SampleSummaryView', path);
    };

    return SampleSummaryView;
});