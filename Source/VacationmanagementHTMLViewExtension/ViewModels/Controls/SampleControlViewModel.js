﻿define('VacationmanagementHTMLViewExtension/ViewModels/Controls/SampleControlViewModel', ['ViewModels/ControlViewModel', 'Management/ComponentManager'], function (ControlViewModel, ComponentManager) {
    'use strict';

    /*
     * ControlViewModel: SampleControlViewModel
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleControlViewModel(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {viewModel} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ControlViewModel.call(self, params);

        /*
         * Die Definition von Properties ist auf folgende Weisen möglich:
         *
         * Standard-Property
         * self.property1 = self.paramOrDefault(params.property1, null);
         *
         * Observable-Property
         * self.property2 = self.observable(params.property2, null);
         *
         * Computed-Property
         * self.property1 = self.defineComputed(function () {
                return self.property1 + self.property2();
         * });
         *
         * Subscription
         * self.defineSubscription(self.property2, function (property2) {
                alert(property2);
         * });
         *
         */

        /*
         * Prüfung des übergebenen Parameter-Objekts. Alle darin enthaltenen Parameter sollten auch im ViewModel vorhanden sein.
         */
        ControlViewModel.checkProperties(self, params);
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleControlViewModel.register = function (path) {
        ComponentManager.setViewModel('SampleControlViewModel', path);
    };

    return SampleControlViewModel;
});