﻿define('VacationmanagementHTMLViewExtension/ViewModels/SummaryViews/SampleSummaryViewModel', ['ViewModels/SummaryViewModel', 'Management/ComponentManager'], function (SummaryViewModel, ComponentManager) {
    'use strict';

    /*
     * SummaryViewModel: SampleSummaryViewModel
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleSummaryViewModel(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {viewModel} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        SummaryViewModel.call(self, params);

        /*
         * Die Definition von Properties ist auf folgende Weisen möglich:
         *
         * Standard-Property
         * self.property1 = self.paramOrDefault(params.property1, null);
         *
         * Observable-Property
         * self.property2 = self.observable(params.property2, null);
         *
         * Computed-Property
         * self.property1 = self.defineComputed(function () {
                return self.property1 + self.property2();
         * });
         *
         * Subscription
         * self.defineSubscription(self.property2, function (property2) {
                alert(property2);
         * });
         *
         */
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleSummaryViewModel.register = function (path) {
        ComponentManager.setViewModel('SampleSummaryViewModel', path);
    };

    return SampleSummaryViewModel;
});