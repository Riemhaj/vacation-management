﻿define('VacationmanagementHTMLViewExtension/ViewModels/VMStartPageViewViewModel', ['ViewModels/ViewViewModel', 'Management/ComponentManager'], function (ViewViewModel, ComponentManager) {
    'use strict';

    /*
     * ViewViewModel: VMStartPageViewViewModel
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function VMStartPageViewViewModel(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {viewModel} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ViewViewModel.call(self, params);
        self.VersionObject = ko.observable(params.viewFactory.VersionObject);
        var moduleNameText = "";
        if (document.querySelectorAll('[data-locator="navigation.[VM].link.label"]') != undefined && document.querySelectorAll('[data-locator="navigation.[VM].link.label"]').length > 0) {
            if (document.querySelectorAll('[data-locator="navigation.[VM].link.label"]')[0] != undefined) {
                moduleNameText = document.querySelectorAll('[data-locator="navigation.[VM].link.label"]')[0].innerText

            }

        }
        self.ModuleName = ko.observable(moduleNameText);
        self.openLink = function () {
            window.open(
                "https://sites.google.com/p-cation.de/pcems/startseite", "_blank");
        };
        /*
         * Die Definition von Properties ist auf folgende Weisen möglich:
         *
         * Standard-Property
         * self.property1 = self.paramOrDefault(params.property1, null);
         *
         * Observable-Property
         * self.property2 = self.observable(params.property2, null);
         *
         * Computed-Property
         * self.property1 = self.defineComputed(function () {
                return self.property1 + self.property2();
         * });
         *
         * Subscription
         * self.defineSubscription(self.property2, function (property2) {
                alert(property2);
         * });
         *
         */
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    VMStartPageViewViewModel.register = function (path) {
        ComponentManager.setViewModel('VMStartPageViewViewModel', path);
    };

    return VMStartPageViewViewModel;
});