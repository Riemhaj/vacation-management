﻿define('VacationmanagementHTMLViewExtension/ViewModels/Views/SampleViewViewModel', ['ViewModels/ViewViewModel', 'Management/ComponentManager'], function (ViewViewModel, ComponentManager) {
    'use strict';

    /*
     * ViewViewModel: SampleViewViewModel
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleViewViewModel(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {viewModel} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ViewViewModel.call(self, params);
		
        /*
         * Die Definition von Properties ist auf folgende Weisen möglich:
         *
         * Standard-Property
         * self.property1 = self.paramOrDefault(params.property1, null);
         *
         * Observable-Property
         * self.property2 = self.observable(params.property2, null);
         *
         * Computed-Property
         * self.property1 = self.defineComputed(function () {
                return self.property1 + self.property2();
         * });
         *
         * Subscription
         * self.defineSubscription(self.property2, function (property2) {
                alert(property2);
         * });
         *
         */
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleViewViewModel.register = function (path) {
        ComponentManager.setViewModel('SampleViewViewModel', path);
    };

    return SampleViewViewModel;
});