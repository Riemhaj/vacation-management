﻿define('VacationmanagementHTMLViewExtension/Communication/SampleDTO', ['Communication/DataTransferObject', 'Management/ComponentManager'], function (DataTransferObject, ComponentManager) {
    'use strict';

    /*
     * Communication: SampleDTO
     * @constructor
     */
    function SampleDTO() {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {dto} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} - JS-Objekt (xmlProperties {array}: Namen der für die Serialisierung verwendeten Properties)
         */
        DataTransferObject.call(self, { xmlProperties: [] });
        
        /*
         * Beispiel-Properties:
         * Bei der Instanziierung von DTO-Objekten aus XML werden die Datentypen bestehender Properties beibehalten
         *
         * self.Property1 = self.defaultFromType('string');
         * self.Property2 = self.defaultFromType('array');
         * self.Property3 = self.observableOrDefault(self.defaultFromType('array'));
         */

        return self;
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleDTO.register = function (path) {
        ComponentManager.setModel('SampleDTO', path);
    };

    return SampleDTO;
});