﻿define('VacationmanagementHTMLViewExtension/Views/VMStartPageView', ['Views/View', 'Management/ComponentManager', 'ext!VacationmanagementHTMLViewExtension.Views.VMStartPageView.html'], function (View, ComponentManager, layout) {
    'use strict';

    /*
     * View: VMStartPageView
     * @param {viewModel} viewModel - Eine Instanz des VMStartPageViewViewModel
     * @param {jquery} element - Ein jQuery-Element auf dem die View erzeugt wird
     * @constructor
     */
    function VMStartPageView(viewModel, element) {
        var self = this;
        var view;

        /*
         * Aufruf der Oberklasse
         * @param {control} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {viewModel} - Das aktuelle ViewModel
         */
        View.call(self);

        /*
         * - Erzeugung der View
         * - Hinzufügen von HTML-Templates
         */
        self.getView = function () {
            if (view != null) {
                return view;
            }

            /*
             * Anwenden eines HTML-Templates
             * @param {jquery} element - Das Element, in welches das Template eingefügt werden soll
             * @param {viewModel} viewModel - Das ViewModel der View
             * @param {jquery} layout - In das Modul injiziertes HTML-Template
             */
            view = self.applyBindings(element, viewModel, layout);

            return view;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    VMStartPageView.register = function (path) {
        ComponentManager.setView('VMStartPageView', path);
    };

    return VMStartPageView;
});