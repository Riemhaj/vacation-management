﻿define('VacationmanagementHTMLViewExtension/Views/SampleView', ['Views/View', 'Management/ComponentManager', 'ext!VacationmanagementHTMLViewExtension.Views.SampleView.html'], function (View, ComponentManager, layout) {
    'use strict';

    /*
     * View: SampleView
     * @param {viewModel} viewModel - Eine Instanz des SampleViewViewModel
     * @param {jquery} element - Ein jQuery-Element auf dem die View erzeugt wird
     * @constructor
     */
    function SampleView(viewModel, element) {
        var self = this;
        var view;

        /*
         * Aufruf der Oberklasse
         * @param {control} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {viewModel} - Das aktuelle ViewModel
         */
        View.call(self);

        /*
         * - Erzeugung der View
         * - Hinzufügen von HTML-Templates
         */
        self.getView = function () {
            if (view != null) {
                return view;
            }

            /*
             * Anwenden eines HTML-Templates
             * @param {jquery} element - Das Element, in welches das Template eingefügt werden soll
             * @param {viewModel} viewModel - Das ViewModel der View
             * @param {jquery} layout - In das Modul injiziertes HTML-Template
             */
            view = self.applyBindings(element, viewModel, layout);

            return view;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleView.register = function (path) {
        ComponentManager.setView('SampleView', path);
    };

    return SampleView;
});