﻿define('VacationmanagementHTMLViewExtension/Controls/SampleControl', ['Controls/Control', 'Management/ComponentManager', 'ext!VacationmanagementHTMLViewExtension.Controls.SampleControl.html'], function (Control, ComponentManager, layout) {
    'use strict';

    /*
     * Control: SampleControl
     * @param {viewModel} viewModel - Eine Instanz des SampleControlViewModel
     * @param {jquery} element - Ein jQuery-Element auf dem das Control erzeugt wird (optional)
     * @constructor
     */
    function SampleControl(viewModel, element) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {control} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {viewModel} - Das aktuelle ViewModel
         */
        Control.call(self, viewModel);

        var control;
        var widget = self.createWidget();

        /*
         * - Erzeugung des Control
         * - Hinzufügen von HTML-Templates
         * - Anwenden von Kendo-Bindings
         * - Befüllen des Widgets
         */
        self.getControl = function () {
            if (control != null) {
                return control;
            }

            /*
             * Übergebenes Element dem Control zuweisen oder neue DOM-Struktur erzeugen
             */
            control = element || $('<div></div>');

            /*
             * Anwenden eines HTML-Templates
             * @param {html} layout - In das Modul injiziertes HTML-Template
             * @param {jquery} control - Das Element, in welches das Template eingefügt werden soll
             */
            self.applyTemplate(layout, control);

            /*
             * Befüllen des Widgets
             * @param {widget} widget - Befüllen des erzeugten Widgets
             * 
             * Bei Custom-Controls ist dem Widget in jedem Fall "null" zu übergeben
             * Zur Befüllung des Widgets bei KendoControls bitte folgende Syntax verwenden:
             *
             * self.applyBindings(control, {
             *    kendoControl: {
             *        widget: widget
             *    }
             * });
             *
             */
            widget(null);

            return control;
        };

        /*
         * Callback nach Erzeugung des Widgets
         * @param {widget} widget - Das Widget-Observable
         * @param {function} - Callback-Function
         */
        self.onWidgetCreated(widget, function (widget) {
            /*
             * Anwenden von Bindings, die in allen Controls enthalten sein sollten
             * @param {jquery} control - Das Element, auf das die Bindings angewendet werden
             * @param {object} - JS-Objekt mit Bindings:
             *
             * - controlName: Definiert den Namen des Controls im DOM und fügt ihm die Klasse "c-samplecontrol" hinzu. Dient der Referenzierung in Stylesheets
             * - controlIdent: Der Identifier ist ein eindeutiger Identifizierungsmechanismus für Controls.
             * - visible: Zeigt oder versteckt das Control entsprechend der Definition im ViewModel
             * - styles: Fügt dem Control zusätzliche Klassen hinzu: "c-style-{style}"
             * - states: Fügt dem Control zusätzliche Klassen hinzu: "c-state-{style}"
             */
            self.applyBindings(control, {
                controlName: 'samplecontrol',

                loader: self.context.isBusy,

                controlIdent: self.identifier,

                visible: self.context.isVisible,

                styles: self.definePureComputed({
                    read: function () {
                        var styles = [];

                        return styles;
                    }
                }),

                states: self.definePureComputed({
                    read: function () {
                        var states = [];

                        if (self.context.isRequired() === true) {
                            states.push('required');
                        }

                        if (self.context.isEnabled() === true) {
                            states.push('enabled');
                        } else {
                            states.push('disabled');
                        }

                        return states;
                    }
                })
            });
        });
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleControl.register = function (path) {
        ComponentManager.setControl('SampleControl', path);
    };

    return SampleControl;
});