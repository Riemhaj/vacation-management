﻿define('VacationmanagementHTMLViewExtension/Factories/LayoutFactories/SampleLayoutFactory', ['Factories/LayoutFactory', 'Management/ComponentManager'], function (LayoutFactory, ComponentManager) {
    'use strict';

    /*
     * LayoutFactory: SampleLayoutFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleLayoutFactory(params) {
        var self = this;
        var control;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        LayoutFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleControlViewModel = ComponentManager.getViewModel('SampleControlViewModel');
        var SampleControl = ComponentManager.getControl('SampleControl');

        /*
         * Instanziierung des ViewModels und des Controls
         * @returns jquery
         */
        self.getControl = function () {
            if (control != null) {
                return control;
            }

            self.viewModel = (function () {
                return new SampleControlViewModel({
                    tuples: self.tuples,
                    labelColumnWidth: self.context && self.context.labelColumnWidth
                });
            })();

            self.control = (function () {
                return new SampleControl(self.viewModel);
            })();

            var tuple = self.control.getTuple();
            control = tuple.control;

            return control;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleLayoutFactory.register = function (path) {
        ComponentManager.setFactory('SampleLayout', path);
    };

    return SampleLayoutFactory;
});