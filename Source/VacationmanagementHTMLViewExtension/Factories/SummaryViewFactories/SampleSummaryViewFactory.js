﻿define('VacationmanagementHTMLViewExtension/Factories/SummaryViewFactories/SampleSummaryViewFactory', ['Factories/SummaryViewFactory', 'Management/ComponentManager'], function (SummaryViewFactory, ComponentManager) {
    'use strict';

    /*
     * SummaryViewFactory: SampleSummaryViewFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleSummaryViewFactory(params) {
        var self = this;
        var summaryView;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        SummaryViewFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleSummaryViewModel = ComponentManager.getViewModel('SampleSummaryViewModel');
        var SampleSummaryView = ComponentManager.getSummaryView('SampleSummaryView');

        /*
         * Instanziierung des ViewModels und der View
         * @returns jquery
         */
        self.getSummaryView = function () {
            if (summaryView != null) {
                return summaryView;
            }

            self.viewModel = (function () {
                return new SampleSummaryViewModel({
                    /*
                     * Es besteht Zugriff auf die folgenden Werte
                     * - self.pdoId: Die PdoId des selektierten Objekts
                     * - self.pdTypeName: Der PDTypeName des selektierten Objekts
                     */

                    /*
                     * Beispiele zur Übergabe von Werten aus dem GenericObject
                     *
                     * Übergabe eines Observable-Values aus dem GenericObject (Default)
                     * value: self.genericObject.getObservable('PropertyName')
                     *
                     * Übergabe eines reinen Wertes aus dem GenericObject (Kein Change-Tracking)
                     * value: self.genericObject.getValue('PropertyName')
                     *
                     * Übergabe eines berechneten Wertes aus dem GenericObject (z.B. für XML-Werte)
                     * value: self.genericObject.getComputed('PropertyName', {
                     *      read: function (observable) {
                     *          var dto = DataTransferObject.parseFromXML(observable());
                     *          return dto;
                     *      },
                     *      write: function (observable, value) {
                     *          var xml = DataTransferObject.toXMLString(value);
                     *          observable(xml);
                     *      }
                     * })
                     *
                     */
				});
            })();

			self.view = (function () {
				self.summaryView = new SampleSummaryView(self.viewModel);
			})();

            var tuple = self.summaryView.getTuple();
            summaryView = tuple.summaryView;

            return summaryView;
        };

        /*
         * Definition der für die SummaryView benötigten Properties
         * @returns jquery
         */
        self.getNeededProperties = function () {
            return [];
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleSummaryViewFactory.register = function (path) {
        ComponentManager.setFactory('SampleSummaryView', path);
    };

    return SampleSummaryViewFactory;
});