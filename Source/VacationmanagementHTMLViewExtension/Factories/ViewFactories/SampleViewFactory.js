﻿define('VacationmanagementHTMLViewExtension/Factories/ViewFactories/SampleViewFactory', ['Factories/ViewFactory', 'Management/ComponentManager', 'Management/NavigationService'], function (ViewFactory, ComponentManager, NavigationService) {
    'use strict';

    /*
     * ViewFactory: SampleViewFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleViewFactory(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ViewFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleViewViewModel = ComponentManager.getViewModel('SampleViewViewModel');
        var SampleViewView = ComponentManager.getView('SampleView');
        
        /*
         * Instanziierung des ViewModels und der View
         * @returns jquery
         */
        self.getView = function () {
            /*
             * Auflösen der Abhängigkeiten für diese View
             */
            self.resolveDependencies(['Wireframe']);

            /*
             * Säubern der beinhaltenden Region und Anstoßen des Disposing-Mechanismus
             */
            NavigationService.cleanRegion('Content');

            if (self.viewModel == null) {
                self.viewModel = new SampleViewViewModel({ viewFactory: self });
            }

            /*
             * self.createRootElement erzeugt das Root-Element der View in der Region "Content" mit dem neuen View-Namen "Sample"
             */
            if (self.view == null) {
                self.view = new SampleViewView(self.viewModel, self.createRootElement('Content', 'Sample'));
            }

            var tuple = self.view.getTuple();
            return tuple.view;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * Die View wird über die URL /HTMLClient/SampleView aufrufbar
     * @register
     */
    SampleViewFactory.register = function (path) {
        ComponentManager.setFactory('SampleView', path);
    };

    return SampleViewFactory;
});