﻿define('VacationmanagementHTMLViewExtension/Factories/ColumnFactories/SampleColumnFactory', ['Factories/ColumnFactory', 'Management/ComponentManager'], function (ColumnFactory, ComponentManager) {
    'use strict';

    /*
     * ColumnFactory: SampleColumnFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleColumnFactory(params) {
        var self = this;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ColumnFactory.call(self, params);
		
        /*
         * Abrufen und Aufbereiten des angezeigten Wertes
         * @returns string | number | jquery
         */
        self.getValue = function () {
            return self.generic[self.column.field];
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleColumnFactory.register = function (path) {
        ComponentManager.setFactory('SampleColumn', path);
    };

    return SampleColumnFactory;
});