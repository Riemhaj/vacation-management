﻿define('VacationmanagementHTMLViewExtension/Factories/ViewExtensionFactories/SampleViewExtensionFactory', ['Factories/ViewExtensionFactory', 'Management/ComponentManager'], function (ViewExtensionFactory, ComponentManager) {
    'use strict';

    /*
     * ViewExtensionFactory: SampleViewExtensionFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleViewExtensionFactory(params) {
        var self = this;
        var control;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ViewExtensionFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleControlViewModel = ComponentManager.getViewModel('SampleControlViewModel');
        var SampleControl = ComponentManager.getControl('SampleControl');

        /*
         * Instanziierung des ViewModels und des Controls
         * @returns jquery
         */
        self.getControl = function () {
            if (control != null) {
                return control;
            }

            self.viewModel = (function () {
                return new SampleControlViewModel({
					
                });
            })();

            self.control = (function () {
                return new SampleControl(self.viewModel);
            })();

            var tuple = self.control.getTuple();
            control = tuple.control;

            return control;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleViewExtensionFactory.register = function (path) {
        ComponentManager.setFactory('SampleViewExtension', path);
    };

    return SampleViewExtensionFactory;
});