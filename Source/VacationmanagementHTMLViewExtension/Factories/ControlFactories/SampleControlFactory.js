﻿define('VacationmanagementHTMLViewExtension/Factories/ControlFactories/SampleControlFactory', ['Factories/ControlFactory', 'Management/ComponentManager', 'Communication/DataTransferObject'], function (ControlFactory, ComponentManager, DataTransferObject) {
    'use strict';

    /*
     * ControlFactory: SampleControlFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleControlFactory(params) {
        var self = this;
        var control;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        ControlFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleControlViewModel = ComponentManager.getViewModel('SampleControlViewModel');
        var SampleControl = ComponentManager.getControl('SampleControl');

        /*
         * Instanziierung des ViewModels und des Controls
         * @returns jquery
         */
        self.getControl = function () {
            if (control != null) {
                return control;
            }

            self.viewModel = (function () {
                return new SampleControlViewModel({
                    factory: self

                    /*
                     * Beispiele zur Übergabe von Werten aus dem GenericObject
                     *
                     * Übergabe eines Observable-Values aus dem GenericObject (Default)
                     * value: self.context.genericObject.getObservable(self.detailViewItem.name)
                     *
                     * Übergabe eines reinen Wertes aus dem GenericObject (Kein Change-Tracking)
                     * value: self.context.genericObject.getValue(self.detailViewItem.name)
                     *
                     * Übergabe eines berechneten Wertes aus dem GenericObject (z.B. für XML-Werte)
                     * value: self.context.genericObject.getComputed(self.detailViewItem.name, {
                     *      read: function (observable) {
                     *          var dto = DataTransferObject.parseFromXML(observable());
                     *          return dto;
                     *      },
                     *      write: function (observable, value) {
                     *          var xml = DataTransferObject.toXMLString(value);
                     *          observable(xml);
                     *      }
                     * })
                     *
                     */
                });
            })();

            self.control = (function () {
                return new SampleControl(self.viewModel);
            })();

            var tuple = self.control.getTuple();
            control = tuple.control;

            /*
             * Beispiele für die Registrierung von Validierungen
             *
             * Registrierung einer Standard-Validierung auf Property-Ebene. 
             * self.registerDefaultValidation([self.detailViewItem.name]);
             *
             * Registrierung einer Standard-Validierung für Werte, welche nicht im GenericObject vorhanden sind
             * self.registerObservableValidation(self.viewModel.value);
             *
             * Registrierung einer vorzeitigen Typvalidierung anhand des DotNetType
             * self.registerTypeValidation(self.detailViewItem.name, function (value, dotNetTypeName) {
             *      if (dotNetTypeName != null) {
             *          if (dotNetTypeName.indexOf('String') > -1) {
             *              return value === 'test';
             *          }
             *      }
             *
             *      return true;
             * });
             *
             */

            /*
             * Hinzufügen eines Icons für Derived-Overwritable Attribute
             * self.addOverwritableToControl(control, [self.detailViewItem.name]);
             */

            /*
             * Definition von Filter-Operatoren
             * self.getAvailableOperators = function (typeName) {
             *      var result = [];
             *
             *      result.push({ ergName: 'EQUALS', value: '=' });
             *
             *      return result;
             * };
             */

            /*
             * Berechnung einer Filter-Expression
             * self.getFilterExpression = function (operator) {
             *      var value = self.viewModel.value();
             *      return 'this.' + self.detailViewItem.name + ' ' + operator + ' ' + value;
             * };
             */

            return control;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleControlFactory.register = function (path) {
        ComponentManager.setFactory('SampleControl', path);
    };

    return SampleControlFactory;
});