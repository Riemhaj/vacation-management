define('EMSHTMLViewExtension/Factories/ControlFactories/PcatNumberTextBoxFactory', ['Factories/ControlFactory', 'Management/ComponentManager', 'Communication/RADEnvironmentProxy'], function (ControlFactory, ComponentManager, RADEnvironmentProxy) {
    'use strict';

    function PcatNumberTextBoxFactory(params) {
        var self = this;
        var control;

        ControlFactory.call(self, params);

        var LinkTextBoxViewModel = ComponentManager.getViewModel('LinkTextBoxViewModel');
        var LinkViewModel = ComponentManager.getViewModel('LinkViewModel');
        var TextBoxViewModel = ComponentManager.getViewModel('TextBoxViewModel');
        var LabelViewModel = ComponentManager.getViewModel('LabelViewModel');
        var ImageViewModel = ComponentManager.getViewModel('ImageViewModel');

        var LinkTextBox = ComponentManager.getControl('LinkTextBox');

        self.getControl = function () {
            if (control != null) {
                return control;
            }

            self.viewModel = (function () {
                return new LinkTextBoxViewModel({
                    factory: self,
                    tooltip: new LabelViewModel({
                        ergName: self.detailViewItem.tooltip
                    }),
                    parentLocator: self.locator,
                    locator: self.detailViewItem.name,
                    link: new LinkViewModel({
                        parentLocator: self.locator,
                        locator: self.detailViewItem.name + '.link',
                        image: new ImageViewModel({
                            parentLocator: self.locator,
                            locator: self.detailViewItem.name + '.link.image',
                            source: 'pcat.png'
                        }),
                        onClick: function () {
                            var companyName = self.context.genericObject.getValue('Name');

                            if (!companyName) {
                                throw new Error('Company name is not defined');
                            }

                            RADEnvironmentProxy.createIteratorForAssoziation({
                                pdTypeName: 'CRM.CRM.LegalPerson',
                                pdo_id: self.context.genericObject.pdoId,
                                assoziationName: 'Address',
                                properties: ['City'],
                                retrieveExtentCount: true
                            }).then(function (iterator) {
                                if (iterator.extentCount > 0) {
                                    return RADEnvironmentProxy.getExtent({
                                        iteratorID: iterator.id,
                                        start: 0,
                                        end: 0
                                    });
                                } else {
                                    
                                }
                            }).then(function (arrayOfGeneric) {
                                if (arrayOfGeneric != null) {
                                    return {
                                        city: arrayOfGeneric[0][0],
                                        houseNumber: arrayOfGeneric[0][1],
                                        postalCode: arrayOfGeneric[0][2],
                                        street: arrayOfGeneric[0][3],
                                        country: arrayOfGeneric[0][4]
                                    };
                                }
                            }).then(function (address) {
                                if (address != null) {
                                    RADEnvironmentProxy.callStaticOperation({
                                        pdTypeName: 'PCATNumberManagement.PCATNumberManagement.PCATNumber',
                                        operationName: 'GetOrCreatePCATNumber',
                                        operationParameters: [companyName, address.street, address.postalCode, address.city, address.country]
                                    }).then(function (operationResultDTO) {
                                        if (self.validateStatus(operationResultDTO)) {
                                            var value = self.context.genericObject.getObservable(self.detailViewItem.name);
                                            value(operationResultDTO.returnValue);
                                        }
                                    });
                                }
                            });
                        },
                        isEnabled: ko.computed(function () {
                            var value = self.context.genericObject.getObservable(self.detailViewItem.name)();
                            return self.isEnabled() && !value;
                        }),
                        tooltip: new LabelViewModel({
                            caption: 'Get or generate PCAT Number'
                        }),
                        isToolBarLink: true,
                        isFocusable: self.isFocusable,
                        isVisible: true
                    }),
                    textBox: new TextBoxViewModel({
                        value: self.context.genericObject.getObservable(self.detailViewItem.name),
                        isEnabled: self.isEnabled,
                        isFocusable: self.isFocusable,
                        isValid: self.isValid,
                        invalidationText: self.invalidationText,
                        parentLocator: self.locator,
                        locator: self.detailViewItem.name
                    })
                });
            })();

            self.control = (function () {
                return new LinkTextBox(self.viewModel);
            })();

            var tuple = self.control.getTuple();
            control = tuple.control;

            self.addOverwritableToControl(control, [self.detailViewItem.name]);
            self.registerDefaultValidation([self.detailViewItem.name]);

            return control;
        };

        self.getFilterExpression = function (operator) {
            var value = self.viewModel.value();

            if (value == null) {
                return '';
            }

            if (operator === 'LIKE') {
                return 'this.' + self.detailViewItem.name + ' ' + operator + ' \'%' + value + '%\'';
            } else {
                return 'this.' + self.detailViewItem.name + ' ' + operator + ' \'' + value + '\'';
            }
        };
    }

    PcatNumberTextBoxFactory.register = function (path) {
        ComponentManager.setFactory('PcatNumberTextBox', path);
    };

    return PcatNumberTextBoxFactory;
});