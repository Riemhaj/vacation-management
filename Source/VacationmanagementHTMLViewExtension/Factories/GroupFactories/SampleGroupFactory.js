﻿define('VacationmanagementHTMLViewExtension/Factories/GroupFactories/SampleGroupFactory', ['Factories/GroupFactory', 'Management/ComponentManager'], function (GroupFactory, ComponentManager) {
    'use strict';

    /*
     * GroupFactory: SampleGroupFactory
     * @param {object} params - Ein Objekt mit übergebenen Parametern
     * @constructor
     */
    function SampleGroupFactory(params) {
        var self = this;
        var control;

        /*
         * Aufruf der Oberklasse
         * @param {factory} - Aufruf der Oberklasse auf der aktuellen Instanz
         * @param {object} params - Ein Objekt mit übergebenen Parametern
         */
        GroupFactory.call(self, params);

        /*
         * Auslesen der für die Factory benötigten Komponenten
         */
        var SampleControlViewModel = ComponentManager.getViewModel('SampleControlViewModel');
        var SampleControl = ComponentManager.getControl('SampleControl');

        /*
         * Instanziierung des ViewModels und des Controls
         * @returns jquery
         */
        self.getControl = function () {
            if (control != null) {
                return control;
            }
			
            self.viewModel = (function () {
                return new SampleControlViewModel({
                    context: self.context,
                    layout: self.layout,
                    isSummaryViewMode: self.isSummaryViewMode,
                    detailViewGroups: self.detailViewGroup.detailViewGroups,
                    detailViewItems: self.detailViewGroup.items
                });
            })();

            self.control = (function () {
                return new SampleControl(self.viewModel);
            })();

            var tuple = self.control.getTuple();
            control = tuple.control;

            return control;
        };
    }

    /*
     * Statische Registrierungsfunktion
     * @register
     */
    SampleGroupFactory.register = function (path) {
        ComponentManager.setFactory('SampleGroup', path);
    };

    return SampleGroupFactory;
});