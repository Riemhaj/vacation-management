alter table Vacationmanagement_vorgesetzter_freigebenerantrag  drop foreign key FK5424967341149DF4;
alter table Vacationmanagement_vorgesetzter_freigebenerantrag  drop foreign key FK5424967340DB894E;
alter table RAD_encryptedsymmetrickey  drop foreign key FK47C85D987670031;
alter table RAD_AssignedUsers_EncryptionGroups  drop foreign key FK3BA3C2ED9C23EA6;
alter table RAD_historizationgroup  drop foreign key FK99D522F02FDAB49E;
alter table RAD_Principals_HistorizationPoints  drop foreign key FK41995852A5D4475B;
alter table RAD_Principals_HistorizationPoints  drop foreign key FK4199585214DBDD9F;
alter table RAD_Modules_HistorizationPoints  drop foreign key FKE45F24C4B19425E8;
alter table RAD_Modules_HistorizationPoints  drop foreign key FKE45F24C414DBDD9F;
alter table RAD_radldapconnection  drop foreign key FK159670E8E6A7819C;
alter table RAD_notificationentry  drop foreign key FKF674C3A53F49CBBB;
alter table RAD_LoginExceptions_SystemLock  drop foreign key FKDEE59B9C64302053;
alter table RAD_currency  drop foreign key FKC472B89BD1AD5A0D;
alter table RAD_currency  drop foreign key FKC472B89BFD6B86FE;
alter table RAD_currencyexchange  drop foreign key FK1133A594513DAB35;
alter table RAD_currencyexchange  drop foreign key FK1133A5946794D501;
alter table RAD_currencyexchange  drop foreign key FK1133A5942FDAB49E;
alter table RAD_language  drop foreign key FK9C1F60D1AD5A0D;
alter table RAD_resourceitem  drop foreign key FKA0049F43BBC9BFB1;
alter table RAD_pdprincipal  drop foreign key FK1A99933CA97F80FA;
alter table RAD_pdprincipal  drop foreign key FK1A99933CF121E4F4;
alter table RAD_pdprincipal  drop foreign key KF1A28671F;
alter table RAD_AccessingUsers_AccessOnPrincipals  drop foreign key FKFEDD09B52A8E5316;
alter table RAD_ReferencingMemberships_Principals  drop foreign key FKA73F5CDD930CBFEF;
alter table RAD_ReferencingMemberships_Principals  drop foreign key FKA73F5CDDA5D4475B;
alter table RAD_dvgitem  drop foreign key FKE40936EEB989855F;
alter table RAD_dvgitem  drop foreign key FKE40936EEC8E7386B;
alter table RAD_dvgitem  drop foreign key FKE40936EE240FEF05;
alter table RAD_detailviewgroup  drop foreign key FK1489C433E6ED5831;
alter table RAD_detailviewgroup  drop foreign key FK1489C43378694430;
alter table RAD_dynamicassoziation  drop foreign key FK88BB6FADBE50C532;
alter table RAD_dynamicassoziation  drop foreign key FK88BB6FADBE50C533;
alter table RAD_lvcolumn  drop foreign key FK5A236CBAC8E7386B;
alter table RAD_lvcolumn  drop foreign key FK5A236CBAACF63121;
alter table RAD_pdapplicationmeta  drop foreign key FK9508ED23DF598F4A;
alter table RAD_pdapplicationmeta  drop foreign key FK9508ED23A97F80FA;
alter table RAD_pdenumerationitemmeta  drop foreign key FK63AE2B8DF85E8D62;
alter table RAD_pdextensionmodulemeta  drop foreign key FKE773C16AB71FE9DA;
alter table RAD_treenode  drop foreign key FKDDDF57A6FDB2CB79;
alter table Vacationmanagement_abteilung  drop foreign key KFB4500140;
alter table Vacationmanagement_abteilung  drop foreign key KF7FE6D70;
alter table Vacationmanagement_Leiter_Verantwortlicher  drop foreign key FK1F42FEAA35BDFAFA;
alter table Vacationmanagement_Leiter_Verantwortlicher  drop foreign key FK1F42FEAACD7F9141;
alter table Vacationmanagement_Mitarbeiter_Mitglieder  drop foreign key FK2E957A528F32B3C7;
alter table Vacationmanagement_Mitarbeiter_Mitglieder  drop foreign key FK2E957A5257F3F5AE;
alter table Vacationmanagement_urlaubsantrag  drop foreign key KF96C6F0FE;
alter table Vacationmanagement_urlaubsantrag  drop foreign key KF25689CCE;
alter table Vacationmanagement_GestellterAntrag_Antragsteller  drop foreign key FK43A925FE44EC23EB;
alter table Vacationmanagement_GestellterAntrag_Antragsteller  drop foreign key FK43A925FED8384622;
alter table WFE_applicationspecificprotocol  drop foreign key KF4E47FE45;
alter table WFE_applicationspecificprotocol  drop foreign key KFFDE99275;
alter table WFE_workflowchunkexecutionprotocol  drop foreign key KF7A246F41;
alter table WFE_workflowchunkexecutionprotocol  drop foreign key KFC98A0371;
alter table WFE_workflowinstance  drop foreign key FKBF377D95A560B6;
alter table WFE_workflowinstance  drop foreign key KF3ABF7E93;
alter table WFE_workflowinstance  drop foreign key KF891112A3;
alter table RAD_journalentry  drop foreign key KFDF6C6F73;
alter table RAD_journalentry  drop foreign key KF6CC20343;
alter table RAD_journalentrylistviewexport  drop foreign key KFDA73BFAA;
alter table RAD_journalentrylistviewexport  drop foreign key KF69DDD39A;
alter table RAD_designwizard  drop foreign key KF7D405CB;
alter table RAD_designwizard  drop foreign key KFB47A69FB;
alter table RAD_passwordpolicy  drop foreign key KF875686D5;
alter table RAD_passwordpolicy  drop foreign key KF34F8EAE5;
alter table RAD_pddocument  drop foreign key KF82BD3EC5;
alter table RAD_pddocument  drop foreign key KF311352F5;
alter table RAD_Mails_to_Document  drop foreign key FKBB3320AE86FDB108;
alter table RAD_Mails_to_Document  drop foreign key FKBB3320AEA85442E0;
alter table RAD_pdemail  drop foreign key KF4962347C;
alter table RAD_pdemail  drop foreign key KFFACC584C;
alter table RAD_ToUserBase_ToMails  drop foreign key FK817D88D57AECAB99;
alter table RAD_CCUserBase_CCMails  drop foreign key FKB285730DAA185892;
alter table RAD_BCCUserBase_BCCMails  drop foreign key FK8DD10B158B74C74;
alter table RAD_pdmetatag  drop foreign key FKEB21DA6BA85442E0;
alter table RAD_pdmetatag  drop foreign key KFD1219385;
alter table RAD_pdmetatag  drop foreign key KF628FFFB5;
alter table RAD_pdtask  drop foreign key KFA9AB24A3;
alter table RAD_pdtask  drop foreign key KF1A054893;
alter table RAD_pdassoziationmeta  drop foreign key FKF86D2C71F10D78AF;
alter table RAD_pdassoziationmeta  drop foreign key FKF86D2C714DA74FA5;
alter table RAD_pdassoziationmeta  drop foreign key FKF86D2C7120FFC442;
alter table RAD_pdoperationmeta  drop foreign key FK1580F266F10D78AF;
alter table RAD_pdpropertymeta  drop foreign key FKB0C209EEF10D78AF;
alter table RAD_resourceitemimportexport  drop foreign key KF56A5A9FE;
alter table RAD_resourceitemimportexport  drop foreign key KFF2B3D4D3;
alter table RAD_rightsimportexport  drop foreign key KF5B792162;
alter table RAD_rightsimportexport  drop foreign key KFFF6F5C4F;
alter table RAD_dynamicmutableobject  drop foreign key KF4BCD4F2;
alter table RAD_dynamicmutableobject  drop foreign key KFFAFE97CE;
alter table WFE_task  drop foreign key FK2F0983FED11A5030;
alter table WFE_task  drop foreign key KFE7D67826;
alter table WFE_task  drop foreign key KF2B1987D0;
alter table RAD_acgroup  drop foreign key FK6F2E63651692EFDC;
alter table RAD_Subjects_ACGroups  drop foreign key FK68C207C415F45CD;
alter table RAD_Subjects_ACGroups  drop foreign key FK68C207C4CD4E534;
alter table RAD_ACItems_ACGroups  drop foreign key FK994C75A15EA4C3A;
alter table RAD_ACItems_ACGroups  drop foreign key FK994C75A14CD4E534;
alter table RAD_acitem  drop foreign key FKCCB8B3C35C546A4F;
alter table RAD_acitem  drop foreign key KFC7B5506B;
alter table RAD_acrelation  drop foreign key FK26739E5454AC99;
alter table RAD_acselector  drop foreign key KF3EB476BF;
alter table RAD_pdjob  drop foreign key FKE1CEBA3B116ECA72;
alter table RAD_pdjob  drop foreign key FKE1CEBA3BDCD3F534;
alter table RAD_pdjobresult  drop foreign key FK2A5C4B807337BA2D;
alter table RAD_systemadminaccess  drop foreign key KF8962D32A;
alter table RAD_systemadminaccess  drop foreign key KF3ACCBF1A;
alter table RAD_user  drop foreign key FK4175BFE5DF598F4A;
alter table RAD_user  drop foreign key KF35721228;
alter table Vacationmanagement_mitarbeiter  drop foreign key KF1329414;
alter table Vacationmanagement_mitarbeiter  drop foreign key KF85E1FD04;
alter table RAD_usergroup  drop foreign key KF13E96881;
alter table RAD_Obergruppe_Untergruppe  drop foreign key FKAEA64C5495B6D85;
alter table RAD_Obergruppe_Untergruppe  drop foreign key FKAEA64C53A6FAF3B;
alter table RAD_usermemberof  drop foreign key FKEC66DC16D75418D4;
alter table RAD_relview_to_operation  drop foreign key FK6DD58BD68131F491;
drop table if exists Vacationmanagement_vorgesetzter_freigebenerantrag;
drop table if exists WFE_workflowsignal;
drop table if exists RAD_domain;
drop table if exists RAD_dynamicassoziationitem;
drop table if exists RAD_encryptedsymmetrickey;
drop table if exists RAD_encryptiongroup;
drop table if exists RAD_AssignedUsers_EncryptionGroups;
drop table if exists RAD_historizationgroup;
drop table if exists RAD_historizationpoint;
drop table if exists RAD_Principals_HistorizationPoints;
drop table if exists RAD_Modules_HistorizationPoints;
drop table if exists RAD_radldapconnection;
drop table if exists RAD_radldapimport;
drop table if exists RAD_customizingjournal;
drop table if exists RAD_notificationentry;
drop table if exists RAD_systemlock;
drop table if exists RAD_LoginExceptions_SystemLock;
drop table if exists RAD_usernotification;
drop table if exists RAD_pdlinkablefile;
drop table if exists RAD_currency;
drop table if exists RAD_currencyexchange;
drop table if exists RAD_language;
drop table if exists RAD_resourceitem;
drop table if exists RAD_pdprincipal;
drop table if exists RAD_AccessingUsers_AccessOnPrincipals;
drop table if exists RAD_ReferencingMemberships_Principals;
drop table if exists WFE_pdworkflowmeta;
drop table if exists RAD_dvgitem;
drop table if exists RAD_detailviewgroup;
drop table if exists RAD_dynamicassoziation;
drop table if exists RAD_lvcolumn;
drop table if exists RAD_pdapplicationmeta;
drop table if exists RAD_pdenumerationitemmeta;
drop table if exists RAD_pdenumerationmeta;
drop table if exists RAD_pdextensionmodulemeta;
drop table if exists RAD_ACSelectors_Members;
drop table if exists RAD_pdtypemeta;
drop table if exists RAD_treenode;
drop table if exists Vacationmanagement_abteilung;
drop table if exists Vacationmanagement_Leiter_Verantwortlicher;
drop table if exists Vacationmanagement_Mitarbeiter_Mitglieder;
drop table if exists Vacationmanagement_urlaubsantrag;
drop table if exists Vacationmanagement_GestellterAntrag_Antragsteller;
drop table if exists WFE_applicationspecificprotocol;
drop table if exists WFE_workflowchunkexecutionprotocol;
drop table if exists WFE_workflowinstance;
drop table if exists RAD_journalentry;
drop table if exists RAD_journalentrylistviewexport;
drop table if exists RAD_designwizard;
drop table if exists RAD_passwordpolicy;
drop table if exists RAD_pddocument;
drop table if exists RAD_Mails_to_Document;
drop table if exists RAD_pdemail;
drop table if exists RAD_ToUserBase_ToMails;
drop table if exists RAD_CCUserBase_CCMails;
drop table if exists RAD_BCCUserBase_BCCMails;
drop table if exists RAD_pdmetatag;
drop table if exists RAD_pdtask;
drop table if exists RAD_pdassoziationmeta;
drop table if exists RAD_pdoperationmeta;
drop table if exists RAD_pdpropertymeta;
drop table if exists RAD_detailview;
drop table if exists RAD_listview;
drop table if exists RAD_resourceitemimportexport;
drop table if exists RAD_rightsimportexport;
drop table if exists RAD_dynamicmutableobject;
drop table if exists WFE_task;
drop table if exists RAD_acconditionalselector;
drop table if exists RAD_acgroup;
drop table if exists RAD_Subjects_ACGroups;
drop table if exists RAD_ACItems_ACGroups;
drop table if exists RAD_acitem;
drop table if exists RAD_acrelation;
drop table if exists RAD_acselector;
drop table if exists RAD_pdjob;
drop table if exists RAD_pdjobresult;
drop table if exists RAD_smtpconnection;
drop table if exists RAD_systemproperty;
drop table if exists RAD_systemadminaccess;
drop table if exists RAD_user;
drop table if exists Vacationmanagement_mitarbeiter;
drop table if exists RAD_usergroup;
drop table if exists RAD_Obergruppe_Untergruppe;
drop table if exists RAD_usermemberof;
drop table if exists RAD_userrecyclebin;
drop table if exists RAD_userspecificdata;
drop table if exists RAD_relview_to_operation;
drop table if exists RAD_reluserbase_to_pdtask;
drop table if exists RAD_oidcounter;
drop table if exists RAD_benchmarkdata;
drop table if exists RAD_SCA_distributedlock;
drop table if exists RAD_DynamicRight