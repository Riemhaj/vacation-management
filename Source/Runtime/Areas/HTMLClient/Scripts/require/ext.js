define(['module', 'text'], function (module, text) {
    'use strict';

    var ext = {
        load: function (name, req, onLoad, config) {
            var extension = name.split('.').pop();
            var plugin = '';

            if (extension === 'html') {
                plugin = 'text!';
            }

            var path = plugin + MODEL.ServerURL + 'HTMLClient/Extension/GetExtension?name=' + name;

            if (name.indexOf('Plugins') === -1) {
                var ns = (function () {
                    var split = name.split('.');
                    split.splice(split.length - 2, 2);

                    return split.join('.');
                })();
                var file = (function () {
                    var split = name.split('.');
                    split.splice(0, split.length - 2);

                    return split.join('.');
                })();

                path = plugin + MODEL.ServerURL + 'HTMLClient/Extension/' + ns + '/' + file;
            }

            // Relative Pfadprefix zum BaseDir
            if (!MODEL.KarmaBase) {
                requirejs.config({ baseUrl: '' });

                req([path], function (value) {
                    onLoad(value);
                });
            }
            else {
                requirejs.config({ baseUrl: MODEL.KarmaBase });

                req([plugin + '/base/' + 'RADPresentation.HTMLClient.DefaultViewExtensions/Views/MessageBoxView.html'], function (value) {
                    onLoad(value);
                });
            }
        },
    };

    return ext;
});