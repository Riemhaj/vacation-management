/*

highlight v5

Highlights arbitrary terms.

<http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>

MIT license.

Johann Burkard
<http://johannburkard.de>
<mailto:jb@eaio.com>

*/

jQuery.fn.highlight = function (pat) {
    function getAllIndexOf(searchValue, value, start) {
        var index = [];
        if (!start) {
            start = 0;
        }
        var nextPos = value.indexOf(searchValue, start);
        if (nextPos < 0) {
            return [];
        }

        index.push(nextPos);
        index = index.concat(getAllIndexOf(searchValue, value, nextPos + 1));
        return index;
    }

    function getUpperCase(str) {
        var sign = unescape("%DF");

        if (str.indexOf(sign) < 0) {
            return replaceUmlaute(str.toUpperCase());
        }

        var pos = getAllIndexOf(sign, str, 0);
        var upperStr = str.toUpperCase();

        for (var i in pos) {
            var end = pos[i];
            var newStart = pos[i] + 2;
            upperStr = upperStr.slice(0, end) + sign + upperStr.slice(newStart);
        }
        return replaceUmlaute(upperStr);
    }

    function replaceUmlaute(str) {
        var oUml = unescape("%C4");
        var aUml = unescape("%D6");
        var uUml = unescape("%DC");

        
        str = str.replace(new RegExp(oUml, 'g'), "O");
        str = str.replace(new RegExp(aUml, 'g'), "A");
        str = str.replace(new RegExp(uUml, 'g'), "U");
        return str;
    }

    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = getUpperCase(node.data).indexOf(pat);
            pos -= (node.data.substr(0, pos).toUpperCase().length - node.data.substr(0, pos).length);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.length && pat && pat.length ? this.each(function () {
        innerHighlight(this, getUpperCase(pat));
    }) : this;
};

jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with (this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};