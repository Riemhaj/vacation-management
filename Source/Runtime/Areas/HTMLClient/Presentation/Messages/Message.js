/// <amd-module name="Messages/Message" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Messages/Message", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var Message = /** @class */ (function (_super) {
        __extends(Message, _super);
        function Message(params) {
            var _this = _super.call(this) || this;
            if (params) {
                _this.context = _this.paramOrDefault(params.context, null);
            }
            _this.undisposeProperties.push('context');
            return _this;
        }
        return Message;
    }(Class));
    return Message;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVzc2FnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk1lc3NhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMENBQTBDOzs7Ozs7Ozs7Ozs7O0lBSTFDO1FBQXNCLDJCQUFLO1FBR3ZCLGlCQUFtQixNQUFXO1lBQTlCLFlBQ0ksaUJBQU8sU0FLVjtZQUpHLElBQUksTUFBTSxFQUFFO2dCQUNSLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQzVEO1lBQ0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzs7UUFDN0MsQ0FBQztRQUNMLGNBQUM7SUFBRCxDQUFDLEFBVkQsQ0FBc0IsS0FBSyxHQVUxQjtJQUVELE9BQVMsT0FBTyxDQUFDIn0=