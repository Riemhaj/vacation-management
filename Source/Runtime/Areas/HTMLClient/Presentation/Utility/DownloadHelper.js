/// <amd-module name="Utility/DownloadHelper" />
define("Utility/DownloadHelper", ["require", "exports", "Management/RADApplication"], function (require, exports, RADApplication) {
    "use strict";
    var DownloadHelper;
    (function (DownloadHelper) {
        function openDownloadDialogFor(url) {
            url = parseUrl(url);
            var iframe = $('<iframe>').hide().attr('src', url).appendTo('body');
            iframe.ready(function () {
                window.setTimeout(function () {
                    iframe.remove();
                }, 10000);
            });
        }
        DownloadHelper.openDownloadDialogFor = openDownloadDialogFor;
        function parseUrl(url) {
            if (!url) {
                return '';
            }
            url = url.replace('$$TEMPLATE_HANDLER$$', RADApplication.radEnvironmentTemplateHandler);
            url = url.replace('$$DATA_HANDLER$$', RADApplication.radEnvironmentDataHandler);
            url = url.replace('$$MEDIUM_HANDLER$$', RADApplication.radEnvironmentMediumHandler);
            if (RADApplication.token != null) {
                url += url.indexOf('?') > -1 ? '&' : '?';
                url += 'UserToken=' + RADApplication.token;
            }
            return url;
        }
        DownloadHelper.parseUrl = parseUrl;
    })(DownloadHelper || (DownloadHelper = {}));
    return DownloadHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRG93bmxvYWRIZWxwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJEb3dubG9hZEhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnREFBZ0Q7OztJQUloRCxJQUFVLGNBQWMsQ0E4QnZCO0lBOUJELFdBQVUsY0FBYztRQUVwQiwrQkFBc0MsR0FBRztZQUNyQyxHQUFHLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXBCLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVwRSxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNULE1BQU0sQ0FBQyxVQUFVLENBQUM7b0JBQ2QsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNwQixDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFWZSxvQ0FBcUIsd0JBVXBDLENBQUE7UUFFRCxrQkFBeUIsR0FBRztZQUN4QixJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNOLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRSxjQUFjLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUN4RixHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxjQUFjLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNoRixHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxjQUFjLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUVwRixJQUFJLGNBQWMsQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUM5QixHQUFHLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7Z0JBQ3pDLEdBQUcsSUFBSSxZQUFZLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQzthQUM5QztZQUVELE9BQU8sR0FBRyxDQUFDO1FBQ2YsQ0FBQztRQWZlLHVCQUFRLFdBZXZCLENBQUE7SUFDTCxDQUFDLEVBOUJTLGNBQWMsS0FBZCxjQUFjLFFBOEJ2QjtJQUVELE9BQVMsY0FBYyxDQUFDIn0=