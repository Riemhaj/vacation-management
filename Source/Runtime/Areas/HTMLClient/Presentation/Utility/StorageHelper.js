/// <amd-module name="Utility/StorageHelper" />
define("Utility/StorageHelper", ["require", "exports", "Management/RADApplication"], function (require, exports, RADApplication) {
    "use strict";
    var StorageHelper = /** @class */ (function () {
        function StorageHelper() {
            this.bindings = {};
            this.bindingsGlobal = {};
            this.LOCAL_STORAGE_SUPPORTED = Modernizr.localstorage;
        }
        StorageHelper.getInstance = function () {
            if (StorageHelper.instance == null)
                StorageHelper.instance = new StorageHelper();
            return StorageHelper.instance;
        };
        /**
         * Methode zum Auslesen eines Items aus dem Storage
         */
        StorageHelper.prototype.getItemByName = function (name) {
            var _storage = StorageHelper.getInstance().getStorage();
            return _storage[name];
        };
        /**
         * Methode zum Auslesen eines Items aus dem Machine-Storage
         */
        StorageHelper.prototype.getMachineItemByName = function (name) {
            var _storage = StorageHelper.getInstance().getStorageMachine();
            return _storage[name];
        };
        /**
         * Methode zum Erstellen oder Bearbeitens eines Items im Storage
         */
        StorageHelper.prototype.createOrUpdateItem = function (name, value, notify) {
            var helper = StorageHelper.getInstance();
            if (notify == null || notify === true) {
                var bindingForName = helper.getBinding(name);
                if (bindingForName) {
                    bindingForName(value);
                }
            }
            var _storage = helper.getStorage();
            if (_.isObject(value) || _storage[name] !== value) {
                _storage[name] = value;
                helper.setStorage(_storage);
            }
        };
        /**
         * Methode zum Erstellen oder Bearbeitens eines Items im Machine-Storage
         */
        StorageHelper.prototype.createOrUpdateMachineItem = function (name, value, notify) {
            var helper = StorageHelper.getInstance();
            if (notify == null || notify === true) {
                var bindingForName = helper.getGlobalBinding(name);
                if (bindingForName) {
                    bindingForName(value);
                }
            }
            var _storage = helper.getStorageMachine();
            if (_.isObject(value) || _storage[name] !== value) {
                _storage[name] = value;
                helper.setStorageMachine(_storage);
            }
        };
        /**
         * Prüft, ob der LocalStorage aktualisiert werden muss
         */
        StorageHelper.checkLocalStorageRefresh = function () {
            var helper = StorageHelper.getInstance();
            if (!helper.LOCAL_STORAGE_SUPPORTED)
                return false;
            var result = false;
            var savedPdApplicationChanged = window.localStorage.getItem(StorageHelper.LOCAL_STORAGE_PROPERTY_NAME + 'PDApplicationChanged');
            var currentPdApplicationchanged = RADApplication.pdApplicationChangedTime();
            if (savedPdApplicationChanged != null && currentPdApplicationchanged !== savedPdApplicationChanged) {
                window.localStorage.clear();
                result = true;
            }
            window.localStorage.setItem(StorageHelper.LOCAL_STORAGE_PROPERTY_NAME + 'PDApplicationChanged', currentPdApplicationchanged);
            return result;
        };
        /**
         * Liefert den Wert eines Items anhand des Namen
         */
        StorageHelper.get = function (name) {
            var helper = StorageHelper.getInstance();
            var bindingForName = helper.getBinding(name);
            var toJS = ko.toJS(bindingForName);
            return helper.getItemByName(name);
        };
        StorageHelper.getGlobal = function (name) {
            var helper = StorageHelper.getInstance();
            var bindingForName = helper.getGlobalBinding(name);
            var toJS = ko.toJS(bindingForName);
            return helper.getMachineItemByName(name);
        };
        /**
         * Setzt den Wert eines Items anhand des Namen
         */
        StorageHelper.set = function (name, value, notify) {
            StorageHelper.getInstance().createOrUpdateItem(name, value, notify);
        };
        StorageHelper.setGlobal = function (name, value, notify) {
            StorageHelper.getInstance().createOrUpdateMachineItem(name, value, notify);
        };
        /**
         * Wechselt einen Wert
         */
        StorageHelper.toggle = function (name) {
            StorageHelper.set(name, !(StorageHelper.get(name) || false));
        };
        StorageHelper.toggleGlobal = function (name) {
            StorageHelper.setGlobal(name, !(StorageHelper.getGlobal(name) || false));
        };
        /**
         * Fügt ein Binding hinzu
         */
        StorageHelper.prototype.addBinding = function (name, value) {
            var helper = StorageHelper.getInstance();
            if (_.isArray(value)) {
                helper.bindings[name] = ko.observableArray(value);
            }
            else {
                helper.bindings[name] = ko.observable(value);
            }
            StorageHelper.set(name, value, false);
            helper.bindings[name].subscribe(function (value) {
                StorageHelper.set(name, value, true);
            });
            return helper.bindings[name];
        };
        StorageHelper.prototype.addGlobalBinding = function (name, value) {
            if (_.isArray(value)) {
                this.bindingsGlobal[name] = ko.observableArray(value);
            }
            else {
                this.bindingsGlobal[name] = ko.observable(value);
            }
            StorageHelper.setGlobal(name, value, false);
            this.bindingsGlobal[name].subscribe(function (value) {
                StorageHelper.setGlobal(name, value, true);
            });
            return this.bindingsGlobal[name];
        };
        /**
         * Liefert alle Bindings zu einem Namen
         */
        StorageHelper.prototype.getBinding = function (name) {
            return this.bindings[name] || null;
        };
        StorageHelper.prototype.getGlobalBinding = function (name) {
            return this.bindingsGlobal[name] || null;
        };
        /**
         * Liefert ein Observable zum Lesen und Schreiben eines Wertes
         * Achtung: Wenn der 3. Parameter eine Funktion ist und diese FALSE returned, wird der defaultValue direkt zurückgegeben und ist somit nicht automatisch ein Computed!!
         * @param name
         * @param defaultValue
         * @param overrideValueORconditionalEvaluator
         */
        StorageHelper.bind = function (name, defaultValue, overrideValueORconditionalEvaluator) {
            var helper = StorageHelper.getInstance();
            var value = this.get(name);
            var overrideValue = overrideValueORconditionalEvaluator != null && !_.isFunction(overrideValueORconditionalEvaluator) ? overrideValueORconditionalEvaluator : null;
            var conditionalEvaluator = overrideValueORconditionalEvaluator != null && _.isFunction(overrideValueORconditionalEvaluator) ? overrideValueORconditionalEvaluator : null;
            if (conditionalEvaluator == null || conditionalEvaluator()) {
                value = overrideValue ? overrideValue : (value != null ? value : defaultValue);
                var binding_1 = helper.getBinding(name);
                if (binding_1 == null) {
                    binding_1 = helper.addBinding(name, value);
                }
                return ko.computed({
                    read: function () {
                        return binding_1();
                    },
                    write: function (newVal) {
                        binding_1(newVal);
                    }
                });
            }
            else {
                return defaultValue;
            }
        };
        /**
         * Achtung: Wenn der 3. Parameter eine Funktion ist und diese FALSE returned, wird der defaultValue direkt zurückgegeben und ist somit nicht automatisch ein Computed!!
         * @param name
         * @param defaultValue
         * @param overrideValueORconditionalEvaluator
         */
        StorageHelper.bindGlobal = function (name, defaultValue, overrideValueORconditionalEvaluator) {
            var helper = StorageHelper.getInstance();
            var value = this.getGlobal(name);
            var overrideValue = overrideValueORconditionalEvaluator != null && !_.isFunction(overrideValueORconditionalEvaluator) ? overrideValueORconditionalEvaluator : null;
            var conditionalEvaluator = overrideValueORconditionalEvaluator != null && _.isFunction(overrideValueORconditionalEvaluator) ? overrideValueORconditionalEvaluator : null;
            if (conditionalEvaluator == null || conditionalEvaluator()) {
                value = overrideValue ? overrideValue : (value != null ? value : defaultValue);
                var binding = helper.getGlobalBinding(name);
                if (binding == null) {
                    binding = helper.addGlobalBinding(name, value);
                }
                return binding;
            }
            else {
                return defaultValue;
            }
        };
        StorageHelper.prototype.getStorage = function () {
            var json = window.localStorage.getItem(StorageHelper.LOCAL_STORAGE_PROPERTY_NAME + RADApplication.user.pdoId());
            if (json != null) {
                return jQuery.parseJSON(json);
            }
            else {
                return {};
            }
        };
        StorageHelper.prototype.setStorage = function (storage) {
            window.setTimeout(function () {
                var json = JSON.stringify(storage);
                window.localStorage.setItem(StorageHelper.LOCAL_STORAGE_PROPERTY_NAME + RADApplication.user.pdoId(), json);
            });
        };
        StorageHelper.prototype.getStorageMachine = function () {
            var json = window.localStorage.getItem(StorageHelper.LOCAL_MACHINE_STORAGE_PROPERTY_NAME);
            if (json != null) {
                return jQuery.parseJSON(json);
            }
            else {
                return {};
            }
        };
        StorageHelper.prototype.setStorageMachine = function (storageMachine) {
            window.setTimeout(function () {
                var json = JSON.stringify(storageMachine);
                window.localStorage.setItem(StorageHelper.LOCAL_MACHINE_STORAGE_PROPERTY_NAME, json);
            });
        };
        /**
         * Der Name des Properties im Storage unter dem die Werte gespeichert werden
         */
        StorageHelper.LOCAL_STORAGE_PROPERTY_NAME = 'radStorage_';
        StorageHelper.LOCAL_MACHINE_STORAGE_PROPERTY_NAME = StorageHelper.LOCAL_STORAGE_PROPERTY_NAME + 'machine_global';
        return StorageHelper;
    }());
    return StorageHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RvcmFnZUhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlN0b3JhZ2VIZWxwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0NBQStDOzs7SUFNL0M7UUFjSTtZQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQzFELENBQUM7UUFFYyx5QkFBVyxHQUExQjtZQUNJLElBQUksYUFBYSxDQUFDLFFBQVEsSUFBSSxJQUFJO2dCQUM5QixhQUFhLENBQUMsUUFBUSxHQUFHLElBQUksYUFBYSxFQUFFLENBQUM7WUFFakQsT0FBTyxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQ2xDLENBQUM7UUFFRDs7V0FFRztRQUNLLHFDQUFhLEdBQXJCLFVBQXNCLElBQUk7WUFDdEIsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3hELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFFRDs7V0FFRztRQUNLLDRDQUFvQixHQUE1QixVQUE2QixJQUFJO1lBQzdCLElBQUksUUFBUSxHQUFHLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQy9ELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFFRDs7V0FFRztRQUNLLDBDQUFrQixHQUExQixVQUEyQixJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU07WUFDMUMsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksTUFBTSxJQUFJLElBQUksSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO2dCQUNuQyxJQUFJLGNBQWMsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUU3QyxJQUFJLGNBQWMsRUFBRTtvQkFDaEIsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QjthQUNKO1lBRUQsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBRW5DLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxFQUFFO2dCQUMvQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQy9CO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ssaURBQXlCLEdBQWpDLFVBQWtDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTTtZQUNqRCxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekMsSUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7Z0JBQ25DLElBQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFbkQsSUFBSSxjQUFjLEVBQUU7b0JBQ2hCLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDekI7YUFDSjtZQUVELElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBRTFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxFQUFFO2dCQUMvQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixNQUFNLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDdEM7UUFDTCxDQUFDO1FBR0Q7O1dBRUc7UUFDVyxzQ0FBd0IsR0FBdEM7WUFDSSxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFekMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUI7Z0JBQUUsT0FBTyxLQUFLLENBQUM7WUFFbEQsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUkseUJBQXlCLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLDJCQUEyQixHQUFHLHNCQUFzQixDQUFDLENBQUM7WUFDaEksSUFBSSwyQkFBMkIsR0FBRyxjQUFjLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztZQUU1RSxJQUFJLHlCQUF5QixJQUFJLElBQUksSUFBSSwyQkFBMkIsS0FBSyx5QkFBeUIsRUFBRTtnQkFDaEcsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUIsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNqQjtZQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQywyQkFBMkIsR0FBRyxzQkFBc0IsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzdILE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFRDs7V0FFRztRQUNXLGlCQUFHLEdBQWpCLFVBQWtCLElBQUk7WUFDbEIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRXpDLElBQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0MsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUVuQyxPQUFPLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUVhLHVCQUFTLEdBQXZCLFVBQXdCLElBQUk7WUFDeEIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRXpDLElBQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRCxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBRW5DLE9BQU8sTUFBTSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdDLENBQUM7UUFFRDs7V0FFRztRQUNXLGlCQUFHLEdBQWpCLFVBQWtCLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBZ0I7WUFDM0MsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDeEUsQ0FBQztRQUVhLHVCQUFTLEdBQXZCLFVBQXdCLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBZ0I7WUFDakQsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLHlCQUF5QixDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0UsQ0FBQztRQUVEOztXQUVHO1FBQ1csb0JBQU0sR0FBcEIsVUFBcUIsSUFBSTtZQUNyQixhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7UUFFYSwwQkFBWSxHQUExQixVQUEyQixJQUFJO1lBQzNCLGFBQWEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDN0UsQ0FBQztRQUVEOztXQUVHO1FBQ0ssa0NBQVUsR0FBbEIsVUFBbUIsSUFBSSxFQUFFLEtBQUs7WUFDMUIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRXpDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3JEO2lCQUNJO2dCQUNELE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNoRDtZQUVELGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUV0QyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQUs7Z0JBQ2xDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxDQUFDO1FBRU8sd0NBQWdCLEdBQXhCLFVBQXlCLElBQUksRUFBRSxLQUFLO1lBQ2hDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNwRDtZQUVELGFBQWEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUU1QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQUs7Z0JBQ3RDLGFBQWEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBRUQ7O1dBRUc7UUFDSyxrQ0FBVSxHQUFsQixVQUFtQixJQUFJO1lBQ25CLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDdkMsQ0FBQztRQUVPLHdDQUFnQixHQUF4QixVQUF5QixJQUFJO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDN0MsQ0FBQztRQUVEOzs7Ozs7V0FNRztRQUNXLGtCQUFJLEdBQWxCLFVBQW1CLElBQUksRUFBRSxZQUFZLEVBQUUsbUNBQW1DO1lBQ3RFLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUV6QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksYUFBYSxHQUFHLG1DQUFtQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNuSyxJQUFJLG9CQUFvQixHQUFHLG1DQUFtQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFFekssSUFBSSxvQkFBb0IsSUFBSSxJQUFJLElBQUksb0JBQW9CLEVBQUUsRUFBRTtnQkFDeEQsS0FBSyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBRS9FLElBQUksU0FBTyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRXRDLElBQUksU0FBTyxJQUFJLElBQUksRUFBRTtvQkFDakIsU0FBTyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUM1QztnQkFFRCxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7b0JBQ2YsSUFBSSxFQUFFO3dCQUNGLE9BQU8sU0FBTyxFQUFFLENBQUM7b0JBQ3JCLENBQUM7b0JBQ0QsS0FBSyxFQUFFLFVBQUMsTUFBTTt3QkFDVixTQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLENBQUM7aUJBQ0osQ0FBQyxDQUFDO2FBQ047aUJBQ0k7Z0JBQ0QsT0FBTyxZQUFZLENBQUM7YUFDdkI7UUFDTCxDQUFDO1FBRUQ7Ozs7O1dBS0c7UUFDVyx3QkFBVSxHQUF4QixVQUF5QixJQUFJLEVBQUUsWUFBWSxFQUFFLG1DQUFtQztZQUM1RSxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFekMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQyxJQUFJLGFBQWEsR0FBRyxtQ0FBbUMsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDbkssSUFBSSxvQkFBb0IsR0FBRyxtQ0FBbUMsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBRXpLLElBQUksb0JBQW9CLElBQUksSUFBSSxJQUFJLG9CQUFvQixFQUFFLEVBQUU7Z0JBQ3hELEtBQUssR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUUvRSxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRTVDLElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtvQkFDakIsT0FBTyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ2xEO2dCQUVELE9BQU8sT0FBTyxDQUFDO2FBQ2xCO2lCQUNJO2dCQUNELE9BQU8sWUFBWSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQztRQUVPLGtDQUFVLEdBQWxCO1lBQ0ksSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLDJCQUEyQixHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUVoSCxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ2QsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pDO2lCQUFNO2dCQUNILE9BQU8sRUFBRSxDQUFDO2FBQ2I7UUFDTCxDQUFDO1FBRU8sa0NBQVUsR0FBbEIsVUFBbUIsT0FBTztZQUN0QixNQUFNLENBQUMsVUFBVSxDQUFDO2dCQUNkLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQywyQkFBMkIsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQy9HLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVPLHlDQUFpQixHQUF6QjtZQUNJLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1lBRTFGLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDZCxPQUFPLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0gsT0FBTyxFQUFFLENBQUM7YUFDYjtRQUNMLENBQUM7UUFFTyx5Q0FBaUIsR0FBekIsVUFBMEIsY0FBYztZQUNwQyxNQUFNLENBQUMsVUFBVSxDQUFDO2dCQUNkLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxtQ0FBbUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6RixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUF2U0Q7O1dBRUc7UUFDWSx5Q0FBMkIsR0FBRyxhQUFhLENBQUM7UUFDNUMsaURBQW1DLEdBQUcsYUFBYSxDQUFDLDJCQUEyQixHQUFHLGdCQUFnQixDQUFDO1FBb1N0SCxvQkFBQztLQUFBLEFBMVNELElBMFNDO0lBRUQsT0FBUyxhQUFhLENBQUMifQ==