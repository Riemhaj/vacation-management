/// <amd-module name="Utility/Profiler" />
define("Utility/Profiler", ["require", "exports", "Utility/Messenger"], function (require, exports, Messenger) {
    "use strict";
    var Profiler = /** @class */ (function () {
        function Profiler() {
            var _this = this;
            this.from = null;
            this.until = null;
            this.SHOW_DISPOSE = false;
            this.logs = ko.observableArray([]);
            Messenger.registerForMessageId('NavigationStartedMessage', function (message) { _this.writeLog({ message: 'NAVIGATING: ' + message.address.toString() }, { highlight: true }); }, this);
            Messenger.registerForMessageId('ViewFactoryLoadingMessage', function (message) { _this.writeLog({ message: 'LOADING: ' + message.name }, { highlight: true }); }, this);
            Messenger.registerForMessageId('RegionCleaningMessage', function (message) { _this.writeLog({ message: 'CLEANING: ' + message.name }, { highlight: true }); }, this);
        }
        Profiler.prototype.writeLog = function (entry, params) {
            entry.date = moment();
            if (!params || params.showTime !== false) {
                entry.message = entry.date.format('hh:mm:ss:SSSS') + ': ' + entry.message;
            }
            if (params && params.highlight !== false) {
                entry.message = '<strong>' + entry.message + '</strong>';
            }
            if (params && params.light === true) {
                entry.message = '<span style="color: #ccc; font-size: 8pt;">' + entry.message + '</span>';
            }
            this.logs.push(entry);
        };
        Profiler.prototype.removeLog = function (entry) {
            var logs = this.logs();
            for (var i = 0; i < logs.length; i++) {
                if (logs[i].message.indexOf(entry.message) > -1) {
                    this.logs.splice(i, 1);
                    break;
                }
            }
        };
        Profiler.prototype.onInstanceCreated = function (instance) {
            var now = moment();
            var diff;
            if (this.from != null && now != null) {
                diff = now.diff(this.from);
            }
            this.from = now;
            var color = diff >= 20 ? (diff >= 100 ? 'red' : 'orange') : 'black';
            var weight = color === 'red' || color === 'orange' ? 'bold' : 'normal';
            var diffText = '<span style="color: ' + color + '; font-weight: ' + weight + '">[' + diff + 'ms]</span> ';
            this.writeLog({
                message: diffText + instance.identifier,
                onClick: function () {
                    console.log(instance);
                }
            });
        };
        Profiler.prototype.onInstanceDisposed = function (instance) {
            this.removeLog({ message: instance.identifier });
            if (this.SHOW_DISPOSE === true) {
                this.writeLog({ message: 'DISPOSING: ' + instance.identifier }, { light: true, highlight: false });
            }
        };
        return Profiler;
    }());
    return Profiler;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJvZmlsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJQcm9maWxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwwQ0FBMEM7OztJQU0xQztRQU1JO1lBQUEsaUJBU0M7WUFSRyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFbkMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLDBCQUEwQixFQUFFLFVBQUMsT0FBWSxJQUFPLEtBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxPQUFPLEVBQUUsY0FBYyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3RMLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQywyQkFBMkIsRUFBRSxVQUFDLE9BQVksSUFBTyxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsT0FBTyxFQUFFLFdBQVcsR0FBRyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN0SyxTQUFTLENBQUMsb0JBQW9CLENBQUMsdUJBQXVCLEVBQUUsVUFBQyxPQUFZLElBQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLE9BQU8sRUFBRSxZQUFZLEdBQUcsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkssQ0FBQztRQUVNLDJCQUFRLEdBQWYsVUFBZ0IsS0FBSyxFQUFFLE1BQU87WUFDMUIsS0FBSyxDQUFDLElBQUksR0FBRyxNQUFNLEVBQUUsQ0FBQztZQUV0QixJQUFJLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssS0FBSyxFQUFFO2dCQUN0QyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO2FBQzdFO1lBRUQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFNBQVMsS0FBSyxLQUFLLEVBQUU7Z0JBQ3RDLEtBQUssQ0FBQyxPQUFPLEdBQUcsVUFBVSxHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO2FBQzVEO1lBRUQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxJQUFJLEVBQUU7Z0JBQ2pDLEtBQUssQ0FBQyxPQUFPLEdBQUcsNkNBQTZDLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUM7YUFDN0Y7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixDQUFDO1FBRU0sNEJBQVMsR0FBaEIsVUFBaUIsS0FBSztZQUNsQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFdkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2xDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUM3QyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLE1BQU07aUJBQ1Q7YUFDSjtRQUNMLENBQUM7UUFFTSxvQ0FBaUIsR0FBeEIsVUFBeUIsUUFBUTtZQUM3QixJQUFJLEdBQUcsR0FBRyxNQUFNLEVBQUUsQ0FBQztZQUNuQixJQUFJLElBQUksQ0FBQztZQUVULElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDbEMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzlCO1lBRUQsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7WUFFaEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFDcEUsSUFBSSxNQUFNLEdBQUcsS0FBSyxLQUFLLEtBQUssSUFBSSxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUN2RSxJQUFJLFFBQVEsR0FBRyxzQkFBc0IsR0FBRyxLQUFLLEdBQUcsaUJBQWlCLEdBQUcsTUFBTSxHQUFHLEtBQUssR0FBRyxJQUFJLEdBQUcsYUFBYSxDQUFDO1lBRTFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1YsT0FBTyxFQUFFLFFBQVEsR0FBRyxRQUFRLENBQUMsVUFBVTtnQkFDdkMsT0FBTyxFQUFFO29CQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzFCLENBQUM7YUFDSixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRU0scUNBQWtCLEdBQXpCLFVBQTBCLFFBQVE7WUFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztZQUVqRCxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxFQUFFO2dCQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsT0FBTyxFQUFFLGFBQWEsR0FBRyxRQUFRLENBQUMsVUFBVSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQ3RHO1FBQ0wsQ0FBQztRQUNMLGVBQUM7SUFBRCxDQUFDLEFBM0VELElBMkVDO0lBRUQsT0FBUyxRQUFRLENBQUMifQ==