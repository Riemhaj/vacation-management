/// <amd-module name="Utility/DeferredQueue" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Utility/DeferredQueue", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var DeferredQueue = /** @class */ (function (_super) {
        __extends(DeferredQueue, _super);
        function DeferredQueue(timeout) {
            var _this = _super.call(this) || this;
            _this.queue = ko.observable([]).extend({ waiter: timeout || 50 });
            _this.def = $.Deferred();
            _this.callback = null;
            _this.length = 0;
            _this.defineSubscription(_this.queue, function (queue) {
                for (var i = 0; i < queue.length; i++) {
                    _this.def = _this.def.then(queue[i]);
                }
                _this.def.then(function (result) {
                    if (_this.callback) {
                        _this.callback(result);
                    }
                });
                _this.def.resolve();
            });
            return _this;
        }
        DeferredQueue.prototype.resolve = function () {
            this.enqueue(function () {
            });
        };
        DeferredQueue.prototype.enqueue = function (newDeferred) {
            var arr = this.queue().push(newDeferred);
            this.queue(arr);
            this.length++;
            return this;
        };
        DeferredQueue.prototype.done = function (func) {
            this.callback = func;
            return this;
        };
        return DeferredQueue;
    }(Class));
    return DeferredQueue;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGVmZXJyZWRRdWV1ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkRlZmVycmVkUXVldWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0NBQStDOzs7Ozs7Ozs7Ozs7O0lBSS9DO1FBQTRCLGlDQUFLO1FBUTdCLHVCQUFZLE9BQU87WUFBbkIsWUFDSSxpQkFBTyxTQXFCVjtZQW5CRyxLQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2pFLEtBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBRXJCLEtBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBRWhCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLFVBQUMsS0FBSztnQkFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25DLEtBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3RDO2dCQUVELEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTTtvQkFDakIsSUFBSSxLQUFJLENBQUMsUUFBUSxFQUFFO3dCQUNmLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ3pCO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7O1FBQ1AsQ0FBQztRQUVNLCtCQUFPLEdBQWQ7WUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2IsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRU0sK0JBQU8sR0FBZCxVQUFlLFdBQVc7WUFDdEIsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRWhCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUVkLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTSw0QkFBSSxHQUFYLFVBQVksSUFBSTtZQUNaLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBRXJCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDTCxvQkFBQztJQUFELENBQUMsQUFuREQsQ0FBNEIsS0FBSyxHQW1EaEM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9