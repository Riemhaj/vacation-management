/// <amd-module name="Utility/CurrencyHelper" />
define("Utility/CurrencyHelper", ["require", "exports"], function (require, exports) {
    "use strict";
    var CurrencyHelper;
    (function (CurrencyHelper) {
        function toServerValue(value) {
            if (isNullOrWhitespace(value)) {
                return null;
            }
            return Math.round(value * 100000);
        }
        CurrencyHelper.toServerValue = toServerValue;
        function toClientValue(value) {
            if (isNullOrWhitespace(value)) {
                return null;
            }
            return value / 100000;
        }
        CurrencyHelper.toClientValue = toClientValue;
        function isNullOrWhitespace(value) {
            return value === null || value === undefined || (typeof value === "string" && value.trim().length === 0);
        }
    })(CurrencyHelper || (CurrencyHelper = {}));
    return CurrencyHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VycmVuY3lIZWxwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDdXJyZW5jeUhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnREFBZ0Q7OztJQUVoRCxJQUFVLGNBQWMsQ0FxQnZCO0lBckJELFdBQVUsY0FBYztRQUVwQix1QkFBOEIsS0FBSztZQUMvQixJQUFJLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMzQixPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQztRQUN0QyxDQUFDO1FBTmUsNEJBQWEsZ0JBTTVCLENBQUE7UUFFRCx1QkFBOEIsS0FBSztZQUMvQixJQUFJLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMzQixPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsT0FBTyxLQUFLLEdBQUcsTUFBTSxDQUFDO1FBQzFCLENBQUM7UUFOZSw0QkFBYSxnQkFNNUIsQ0FBQTtRQUVELDRCQUE0QixLQUFLO1lBQzdCLE9BQU8sS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDN0csQ0FBQztJQUNMLENBQUMsRUFyQlMsY0FBYyxLQUFkLGNBQWMsUUFxQnZCO0lBRUQsT0FBUyxjQUFjLENBQUMifQ==