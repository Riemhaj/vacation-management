/// <amd-module name="Utility/ImageHandler" />
define("Utility/ImageHandler", ["require", "exports", "Management/RADApplication"], function (require, exports, RADApplication) {
    "use strict";
    var ImageHandler = /** @class */ (function () {
        function ImageHandler() {
        }
        /**
         * Asynchrone Methode, die auf die Sprite-Generierung wartet und die entsprechenden Parameter zur�ckliefer
         * @param iconUrl       URL des Bildes
         * @param width         Breite des Bildes
         * @param height        H�he des Bildes
         * @param isFileType    Wird bis zum entsprechenden Handler durchgereicht
         */
        ImageHandler.getParameters = function (iconUrl, width, height, isFileType) {
            // Neues Deferred, da .done() keine R�ckgabe erlaubt.
            // .done() ist hier notwendig, da RADApplication.menuSpriteLoaded schon fertig sein kann,
            // ein .then() w�rde dann nicht mehr ausgel�st werden
            var deferred = $.Deferred();
            // $.when muss hier genutzt werden, da menuSpriteLoaded noch nicht initialisiert sein kann
            $.when(RADApplication.menuSpriteLoaded).done(function () {
                var params = {
                    source: ImageHandler.convertSource(iconUrl, isFileType),
                    style: ''
                };
                if (width != null) {
                    params.style += "width: " + width + "px;";
                }
                if (height != null) {
                    params.style += "height: " + height + "px;";
                }
                if (RADApplication.menuSprite == null) {
                    deferred.resolve(params);
                    return;
                }
                var iconName = null;
                var split = params.source.split('?');
                if (split.length > 1) {
                    split = split[1].split('&');
                }
                for (var i in split) {
                    var thisParam = split[i].split('='), name_1 = thisParam[0], value = thisParam[1];
                    if (name_1 === 'ID' || name_1 === 'ImageName') {
                        iconName = value;
                    }
                }
                if (iconName == null) {
                    deferred.resolve(params);
                    return;
                }
                var imageMapEntry = RADApplication.menuSprite.imageMap[iconName];
                var svgMapEntry = RADApplication.menuSprite.svgMap[iconName];
                var cachedSvgSource = ImageHandler.svgSources[iconName];
                // Bild wurde nicht gefunden
                if (imageMapEntry == null) {
                    // SVG wurde auch nicht gefunden
                    if (svgMapEntry == null) {
                        if (cachedSvgSource == null) {
                            // Bild laden, um ein SVG einbetten zu k�nnen
                            $.get(params.source, null, function (data, status, request) {
                                var mimeType = request.getResponseHeader('content-type');
                                if (mimeType === 'image/svg+xml') {
                                    params.source = null;
                                    params.svgSource = request.responseText;
                                    ImageHandler.svgSources[iconName] = params.svgSource;
                                }
                                deferred.resolve(params);
                            });
                        }
                        else {
                            params.source = null;
                            params.svgSource = cachedSvgSource;
                            deferred.resolve(params);
                        }
                    }
                    else {
                        params.source = null;
                        params.svgSource = svgMapEntry;
                        deferred.resolve(params);
                    }
                    return;
                }
                var values = imageMapEntry.split(',').map(function (x) { return parseInt(x); }), source = ImageHandler.convertSource(RADApplication.menuSprite.spriteURL);
                if (width == null || width === values[2] && height == null || height === values[3]) {
                    var buildedSource = ImageHandler.buildSource('blank.png');
                    buildedSource = buildedSource.replace('$$DATA_HANDLER$$', RADApplication.radEnvironmentDataHandler);
                    buildedSource = buildedSource.replace('$$TEMPLATE_HANDLER$$', RADApplication.radEnvironmentTemplateHandler);
                    buildedSource = buildedSource.replace('$$TEMPLATE_STYLE$$', RADApplication.templateStyle() + '_HTML5');
                    params = {
                        source: buildedSource,
                        style: "background-image: url(" + source + "); background-position: -" + values[0] + "px -" + values[1] + "px; width: " + values[2] + "px; height: " + values[3] + "px;"
                    };
                }
                deferred.resolve(params);
            });
            return deferred;
        };
        ImageHandler.convertSource = function (source, isFileType) {
            source = source || '';
            if (source.indexOf('http://') === -1 && source.indexOf('$$TEMPLATE_HANDLER$$') === -1 && source.indexOf('TemplateHandler') === -1 && source.indexOf('$$DATA_HANDLER$$') === -1 && source.indexOf('DataHandler') === -1) {
                source = ImageHandler.buildSource(source, isFileType);
            }
            if (source.indexOf('BLANK') > -1) {
                source = ImageHandler.buildSource('blank.png');
            }
            source = source.replace('$$DATA_HANDLER$$', RADApplication.radEnvironmentDataHandler);
            source = source.replace('$$TEMPLATE_HANDLER$$', RADApplication.radEnvironmentTemplateHandler);
            source = source.replace('$$TEMPLATE_STYLE$$', RADApplication.templateStyle() + '_HTML5');
            return source;
        };
        ImageHandler.buildSource = function (image, isFileType) {
            var source = "$$TEMPLATE_HANDLER$$?TemplateName=$$TEMPLATE_STYLE$$&ImageName=" + image;
            if (isFileType === true) {
                source += '&IsFileType=true';
            }
            return source;
        };
        ImageHandler.parseHTML = function (html) {
            if (html != null) {
                return html.replace(/\$\$_IMAGES_\$\$\//g, RADApplication.radEnvironmentTemplateHandler + '?TemplateName=' + RADApplication.templateStyle() + '_HTML5&ImageName=');
            }
            return null;
        };
        ImageHandler.resolve = function (path) {
            return RADApplication.radEnvironmentTemplateHandler + '?TemplateName=' + RADApplication.templateStyle() + '_HTML5&ImageName=' + path;
        };
        /**
         * Gecachte SVG-Sourcen
         */
        ImageHandler.svgSources = {};
        return ImageHandler;
    }());
    return ImageHandler;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW1hZ2VIYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSW1hZ2VIYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDhDQUE4Qzs7O0lBSzlDO1FBQUE7UUErSkEsQ0FBQztRQXhKRzs7Ozs7O1dBTUc7UUFDVywwQkFBYSxHQUEzQixVQUE0QixPQUFlLEVBQUUsS0FBYSxFQUFFLE1BQWMsRUFBRSxVQUFtQjtZQUMzRixxREFBcUQ7WUFDckQseUZBQXlGO1lBQ3pGLHFEQUFxRDtZQUNyRCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFNUIsMEZBQTBGO1lBQzFGLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN6QyxJQUFJLE1BQU0sR0FBc0I7b0JBQzVCLE1BQU0sRUFBRSxZQUFZLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUM7b0JBQ3ZELEtBQUssRUFBRSxFQUFFO2lCQUNaLENBQUM7Z0JBRUYsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO29CQUNmLE1BQU0sQ0FBQyxLQUFLLElBQUksWUFBVSxLQUFLLFFBQUssQ0FBQztpQkFDeEM7Z0JBRUQsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO29CQUNoQixNQUFNLENBQUMsS0FBSyxJQUFJLGFBQVcsTUFBTSxRQUFLLENBQUM7aUJBQzFDO2dCQUVELElBQUksY0FBYyxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7b0JBQ25DLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3pCLE9BQU87aUJBQ1Y7Z0JBRUQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNwQixJQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFFckMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDbEIsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQy9CO2dCQUVELEtBQUssSUFBSSxDQUFDLElBQUksS0FBSyxFQUFFO29CQUNqQixJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUMvQixNQUFJLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUNuQixLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUV6QixJQUFJLE1BQUksS0FBSyxJQUFJLElBQUksTUFBSSxLQUFLLFdBQVcsRUFBRTt3QkFDdkMsUUFBUSxHQUFHLEtBQUssQ0FBQztxQkFDcEI7aUJBQ0o7Z0JBRUQsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNsQixRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUN6QixPQUFPO2lCQUNWO2dCQUVELElBQUksYUFBYSxHQUFXLGNBQWMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN6RSxJQUFJLFdBQVcsR0FBVyxjQUFjLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDckUsSUFBSSxlQUFlLEdBQVcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFaEUsNEJBQTRCO2dCQUM1QixJQUFJLGFBQWEsSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLGdDQUFnQztvQkFDaEMsSUFBSSxXQUFXLElBQUksSUFBSSxFQUFFO3dCQUNyQixJQUFJLGVBQWUsSUFBSSxJQUFJLEVBQUU7NEJBQ3pCLDZDQUE2Qzs0QkFDN0MsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxVQUFDLElBQVMsRUFBRSxNQUFxQyxFQUFFLE9BQXFCO2dDQUMvRixJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0NBQ3pELElBQUksUUFBUSxLQUFLLGVBQWUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0NBQ3JCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztvQ0FDeEMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO2lDQUN4RDtnQ0FFRCxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUM3QixDQUFDLENBQUMsQ0FBQzt5QkFDTjs2QkFDSTs0QkFDRCxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzs0QkFDckIsTUFBTSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUM7NEJBQ25DLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7eUJBQzVCO3FCQUNKO3lCQUNJO3dCQUNELE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUNyQixNQUFNLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQzt3QkFDL0IsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDNUI7b0JBRUQsT0FBTztpQkFDVjtnQkFFRCxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBWCxDQUFXLENBQUMsRUFDdkQsTUFBTSxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFFN0UsSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxJQUFJLElBQUksSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNoRixJQUFJLGFBQWEsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUUxRCxhQUFhLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxjQUFjLENBQUMseUJBQXlCLENBQUMsQ0FBQztvQkFDcEcsYUFBYSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsY0FBYyxDQUFDLDZCQUE2QixDQUFDLENBQUM7b0JBQzVHLGFBQWEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFFLGNBQWMsQ0FBQyxhQUFhLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQztvQkFDdkcsTUFBTSxHQUFHO3dCQUNMLE1BQU0sRUFBRSxhQUFhO3dCQUNyQixLQUFLLEVBQUUsMkJBQXlCLE1BQU0saUNBQTRCLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBTyxNQUFNLENBQUMsQ0FBQyxDQUFDLG1CQUFjLE1BQU0sQ0FBQyxDQUFDLENBQUMsb0JBQWUsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFLO3FCQUNsSixDQUFDO2lCQUNMO2dCQUVELFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDO1FBRWEsMEJBQWEsR0FBM0IsVUFBNEIsTUFBYyxFQUFFLFVBQW9CO1lBQzVELE1BQU0sR0FBRyxNQUFNLElBQUksRUFBRSxDQUFDO1lBRXRCLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNwTixNQUFNLEdBQUcsWUFBWSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDekQ7WUFFRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQzlCLE1BQU0sR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ2xEO1lBRUQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsY0FBYyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDdEYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsY0FBYyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDOUYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUUsY0FBYyxDQUFDLGFBQWEsRUFBRSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBRXpGLE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFYSx3QkFBVyxHQUF6QixVQUEwQixLQUFhLEVBQUUsVUFBb0I7WUFDekQsSUFBSSxNQUFNLEdBQUcsb0VBQWtFLEtBQU8sQ0FBQztZQUV2RixJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ3JCLE1BQU0sSUFBSSxrQkFBa0IsQ0FBQzthQUNoQztZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFYSxzQkFBUyxHQUF2QixVQUF3QixJQUFZO1lBQ2hDLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDZCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLDZCQUE2QixHQUFHLGdCQUFnQixHQUFHLGNBQWMsQ0FBQyxhQUFhLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO2FBQ3RLO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLG9CQUFPLEdBQXJCLFVBQXNCLElBQVk7WUFDOUIsT0FBTyxjQUFjLENBQUMsNkJBQTZCLEdBQUcsZ0JBQWdCLEdBQUcsY0FBYyxDQUFDLGFBQWEsRUFBRSxHQUFHLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUN6SSxDQUFDO1FBM0pEOztXQUVHO1FBQ1ksdUJBQVUsR0FBRyxFQUFFLENBQUM7UUEwSm5DLG1CQUFDO0tBQUEsQUEvSkQsSUErSkM7SUFFRCxPQUFTLFlBQVksQ0FBQyJ9