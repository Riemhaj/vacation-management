/// <amd-module name="Utility/Utilities" />
define("Utility/Utilities", ["require", "exports", "Management/ResourceManager"], function (require, exports, ResourceManager) {
    "use strict";
    var Utilities = /** @class */ (function () {
        function Utilities() {
        }
        Utilities.isBlank = function (value) {
            return !value || /^\s*$/.test(value);
        };
        Utilities.isNotBlank = function (value) {
            return !Utilities.isBlank(value);
        };
        Utilities.isInt = function (value) {
            return typeof value === 'number' &&
                isFinite(value) &&
                Math.floor(value) === value;
        };
        Utilities.equalsIgnoreCase = function (string1, string2) {
            if (string1 == string2) {
                return true;
            }
            if (null === string1 || undefined === string1) {
                return false;
            }
            if (null === string2 || undefined === string2) {
                return false;
            }
            return string1.toUpperCase() === string2.toUpperCase();
        };
        Utilities.contains = function (haystack, needle) {
            return haystack.indexOf(needle) !== -1;
        };
        Utilities.startsWith = function (str, word) {
            return !!str && !!word && !str.lastIndexOf(word, 0);
        };
        Utilities.endsWith = function (str, word) {
            var offset = word && str ? str.length - word.length : -1;
            return offset >= 0 && str.lastIndexOf(word, offset) === offset;
        };
        Utilities.findFirst = function (collection, test) {
            if (_.isEmpty(collection)) {
                return null;
            }
            for (var _i = 0, collection_1 = collection; _i < collection_1.length; _i++) {
                var item = collection_1[_i];
                if (test(item)) {
                    return item;
                }
            }
            return null;
        };
        Utilities.swap = function (collection, firstIndex, secondIndex) {
            if (_.isEmpty(collection) ||
                Utilities.isNullOrUndefined(firstIndex) ||
                Utilities.isNullOrUndefined(secondIndex)) {
                return;
            }
            if (firstIndex == secondIndex ||
                firstIndex < 0 || secondIndex < 0 ||
                firstIndex >= collection.length || secondIndex >= collection.length) {
                return;
            }
            var temp = collection[firstIndex];
            collection[firstIndex] = collection[secondIndex];
            collection[secondIndex] = temp;
        };
        Utilities.isNullOrUndefined = function (obj) {
            return obj === null || obj === undefined;
        };
        Utilities.encodeXmlChars = function (str) {
            return str.replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&apos;');
        };
        Utilities.getCurrentLanguageFloatingPrecision = function () {
            var language = ResourceManager.getLanguage();
            if (language) {
                return language.floatingPrecision;
            }
            else {
                return 0;
            }
        };
        Utilities.toLocaleNumber = function (number, floatingPrecision) {
            if (Utilities.isNullOrUndefined(number))
                return '';
            var decimals = !Utilities.isNullOrUndefined(floatingPrecision) ? floatingPrecision : Utilities.getCurrentLanguageFloatingPrecision();
            var formatedNumber = parseFloat((number + '').replace(/[^0-9+\-Ee.]/g, ''));
            var cultureInfo = ResourceManager.getLanguage() ? ResourceManager.getLanguage().cultureInfoName : 'en';
            var n = !isFinite(+formatedNumber) ? 0 : +formatedNumber, prec = !isFinite(+decimals) ? 0 : Math.abs(decimals), s = null, toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return (Math.round(n * k) / k);
            };
            s = (prec ? toFixedFix(n, prec) : Math.round(n));
            if (s === null) {
                return '';
            }
            return s.toLocaleString(cultureInfo, { minimumFractionDigits: prec });
        };
        Utilities.wrap = function (before, fn, after) {
            before && before();
            fn && fn();
            after && after();
        };
        return Utilities;
    }());
    return Utilities;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXRpbGl0aWVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiVXRpbGl0aWVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDJDQUEyQzs7O0lBSzNDO1FBQUE7UUErSEEsQ0FBQztRQTdIaUIsaUJBQU8sR0FBckIsVUFBc0IsS0FBYTtZQUMvQixPQUFPLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUVhLG9CQUFVLEdBQXhCLFVBQXlCLEtBQWE7WUFDbEMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUVhLGVBQUssR0FBbkIsVUFBb0IsS0FBVTtZQUMxQixPQUFPLE9BQU8sS0FBSyxLQUFLLFFBQVE7Z0JBQzVCLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLENBQUM7UUFDcEMsQ0FBQztRQUVhLDBCQUFnQixHQUE5QixVQUErQixPQUFlLEVBQUUsT0FBZTtZQUMzRCxJQUFJLE9BQU8sSUFBSSxPQUFPLEVBQUU7Z0JBQ3BCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLElBQUksS0FBSyxPQUFPLElBQUksU0FBUyxLQUFLLE9BQU8sRUFBRTtnQkFDM0MsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxJQUFJLElBQUksS0FBSyxPQUFPLElBQUksU0FBUyxLQUFLLE9BQU8sRUFBRTtnQkFDM0MsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxPQUFPLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDM0QsQ0FBQztRQUVhLGtCQUFRLEdBQXRCLFVBQXVCLFFBQWdCLEVBQUUsTUFBYztZQUNuRCxPQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDM0MsQ0FBQztRQUVhLG9CQUFVLEdBQXhCLFVBQXlCLEdBQVcsRUFBRSxJQUFZO1lBQzlDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUVhLGtCQUFRLEdBQXRCLFVBQXVCLEdBQVcsRUFBRSxJQUFZO1lBQzVDLElBQUksTUFBTSxHQUFHLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDeEQsT0FBTyxNQUFNLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxLQUFLLE1BQU0sQ0FBQTtRQUNsRSxDQUFDO1FBRWEsbUJBQVMsR0FBdkIsVUFBMkIsVUFBb0IsRUFBRSxJQUFlO1lBQzVELElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDdkIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELEtBQWlCLFVBQVUsRUFBVix5QkFBVSxFQUFWLHdCQUFVLEVBQVYsSUFBVTtnQkFBdEIsSUFBSSxJQUFJLG1CQUFBO2dCQUNULElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNaLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsY0FBSSxHQUFsQixVQUFtQixVQUFzQixFQUFFLFVBQWtCLEVBQUUsV0FBbUI7WUFDOUUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztnQkFDckIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztnQkFDdkMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUMxQyxPQUFPO2FBQ1Y7WUFFRCxJQUFJLFVBQVUsSUFBSSxXQUFXO2dCQUN6QixVQUFVLEdBQUcsQ0FBQyxJQUFJLFdBQVcsR0FBRyxDQUFDO2dCQUNqQyxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxXQUFXLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtnQkFDckUsT0FBTzthQUNWO1lBRUQsSUFBSSxJQUFJLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2xDLFVBQVUsQ0FBQyxVQUFVLENBQUMsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakQsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUNuQyxDQUFDO1FBRWEsMkJBQWlCLEdBQS9CLFVBQWdDLEdBQUc7WUFDL0IsT0FBTyxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLENBQUM7UUFDN0MsQ0FBQztRQUVhLHdCQUFjLEdBQTVCLFVBQTZCLEdBQVc7WUFDcEMsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7aUJBQzVCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO2lCQUNyQixPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQztpQkFDckIsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUM7aUJBQ3ZCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDakMsQ0FBQztRQUVjLDZDQUFtQyxHQUFsRDtZQUNJLElBQU0sUUFBUSxHQUFHLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUMvQyxJQUFJLFFBQVEsRUFBRTtnQkFDVixPQUFPLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQzthQUNyQztpQkFDSTtnQkFDRCxPQUFPLENBQUMsQ0FBQzthQUNaO1FBQ0wsQ0FBQztRQUVhLHdCQUFjLEdBQTVCLFVBQTZCLE1BQWMsRUFBRSxpQkFBMEI7WUFDbkUsSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDO2dCQUNuQyxPQUFPLEVBQUUsQ0FBQztZQUVkLElBQU0sUUFBUSxHQUFHLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsbUNBQW1DLEVBQUUsQ0FBQztZQUV2SSxJQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTlFLElBQUksV0FBVyxHQUFHLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3ZHLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLEVBQ3BELElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEVBQ3BELENBQUMsR0FBRyxJQUFJLEVBQ1IsVUFBVSxHQUFHLFVBQVUsQ0FBQyxFQUFFLElBQUk7Z0JBQzFCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMzQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbkMsQ0FBQyxDQUFDO1lBQ04sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUNaLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFDRCxPQUFPLENBQUMsQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLEVBQUUscUJBQXFCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUMxRSxDQUFDO1FBRWEsY0FBSSxHQUFsQixVQUFtQixNQUFPLEVBQUUsRUFBRyxFQUFFLEtBQU07WUFDbkMsTUFBTSxJQUFJLE1BQU0sRUFBRSxDQUFDO1lBQ25CLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQztZQUNYLEtBQUssSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUNyQixDQUFDO1FBQ0wsZ0JBQUM7SUFBRCxDQUFDLEFBL0hELElBK0hDO0lBRUQsT0FBUyxTQUFTLENBQUMifQ==