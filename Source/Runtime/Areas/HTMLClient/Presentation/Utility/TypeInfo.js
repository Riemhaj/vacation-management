/// <amd-module name="Utility/TypeInfo" />
define("Utility/TypeInfo", ["require", "exports"], function (require, exports) {
    "use strict";
    var TypeInfo;
    (function (TypeInfo) {
        function getType(func) {
            if (!_.isFunction(func)) {
                func = func.constructor;
            }
            if (func.name) {
                return func.name;
            }
            return (func + '').split(/\s|\(/)[1];
        }
        TypeInfo.getType = getType;
    })(TypeInfo || (TypeInfo = {}));
    return TypeInfo;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVHlwZUluZm8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJUeXBlSW5mby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwwQ0FBMEM7OztJQUUxQyxJQUFVLFFBQVEsQ0FhakI7SUFiRCxXQUFVLFFBQVE7UUFFZCxpQkFBd0IsSUFBSTtZQUN4QixJQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDckIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7YUFDM0I7WUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ3BCO1lBRUQsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekMsQ0FBQztRQVZlLGdCQUFPLFVBVXRCLENBQUE7SUFDTCxDQUFDLEVBYlMsUUFBUSxLQUFSLFFBQVEsUUFhakI7SUFFRCxPQUFTLFFBQVEsQ0FBQyJ9