/// <amd-module name="Utility/LoggingManager" />
define("Utility/LoggingManager", ["require", "exports", "Communication/RADEnvironmentProxy", "Management/RADApplication"], function (require, exports, RADEnvironmentProxy, RADApplication) {
    "use strict";
    var LoggingManager;
    (function (LoggingManager) {
        /*
         * Methode zum loggen einer Nachricht
         */
        function logMessage(level, message, location) {
            if (location != null) {
                message = 'Location: ' + location + '\n' + message;
            }
            write(level, message);
        }
        LoggingManager.logMessage = logMessage;
        /*
         * Methode zum loggen eines Fehlers
         */
        function logError(level, error, location) {
            var message;
            message = getErrorInformation(error);
            if (location != null) {
                message = 'Location: ' + location + '\n' + message;
            }
            write(level, message);
        }
        LoggingManager.logError = logError;
        /*
         * Objekt mit vorhandenen Log-Levels
         */
        LoggingManager.LogLevel = {
            Debug: 0,
            Info: 1,
            Warn: 2,
            Error: 3,
            Fatal: 4
        };
        function getProxy() {
            if (_.keys(RADEnvironmentProxy['__proto__']).length <= 0) {
                RADEnvironmentProxy = requirejs('Communication/RADEnvironmentProxy');
            }
            return RADEnvironmentProxy;
        }
        /*
         * Methode zum Auslesen des aktuellen Benutzernamen
         */
        function getCurrentUserName() {
            if (RADApplication.user != null && RADApplication.user.isLoggedIn()) {
                return RADApplication.user.username();
            }
            return null;
        }
        /*
         * Liefert Informationen zu einem aufgetretenen Fehler
         */
        function getErrorInformation(error) {
            var stack = printStackTrace({ e: error });
            var result = 'Message: ' + error.message + '\n' + stack;
        }
        /*
         * Schreibt eine Nachricht in die Konsole und übermittelt sie an den Server
         */
        function write(level, message) {
            var username = getCurrentUserName();
            if (username != null) {
                message += '\n' + 'User: ' + username;
            }
            console.log('RADClient => ' + message);
            getProxy().outputMessage({
                message: message,
                priority: level
            });
        }
    })(LoggingManager || (LoggingManager = {}));
    return LoggingManager;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9nZ2luZ01hbmFnZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJMb2dnaW5nTWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnREFBZ0Q7OztJQU9oRCxJQUFVLGNBQWMsQ0FtRnZCO0lBbkZELFdBQVUsY0FBYztRQUVwQjs7V0FFRztRQUNILG9CQUEyQixLQUFLLEVBQUUsT0FBTyxFQUFFLFFBQVE7WUFDL0MsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUNsQixPQUFPLEdBQUcsWUFBWSxHQUFHLFFBQVEsR0FBRyxJQUFJLEdBQUcsT0FBTyxDQUFDO2FBQ3REO1lBRUQsS0FBSyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMxQixDQUFDO1FBTmUseUJBQVUsYUFNekIsQ0FBQTtRQUVEOztXQUVHO1FBQ0gsa0JBQXlCLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUTtZQUMzQyxJQUFJLE9BQXNCLENBQUM7WUFDM0IsT0FBTyxHQUFHLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXJDLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDbEIsT0FBTyxHQUFHLFlBQVksR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQzthQUN0RDtZQUVELEtBQUssQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQVRlLHVCQUFRLFdBU3ZCLENBQUE7UUFFRDs7V0FFRztRQUNRLHVCQUFRLEdBQUc7WUFDbEIsS0FBSyxFQUFFLENBQUM7WUFDUixJQUFJLEVBQUUsQ0FBQztZQUNQLElBQUksRUFBRSxDQUFDO1lBQ1AsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztTQUNYLENBQUE7UUFFRDtZQUNJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3JELG1CQUEyQixHQUFHLFNBQVMsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2FBQ2pGO1lBRUQsT0FBTyxtQkFBbUIsQ0FBQztRQUMvQixDQUFDO1FBRUQ7O1dBRUc7UUFDSDtZQUNJLElBQUksY0FBYyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtnQkFDakUsT0FBTyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3pDO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ0gsNkJBQTZCLEtBQUs7WUFDOUIsSUFBSSxLQUFLLEdBQUcsZUFBZSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDMUMsSUFBSSxNQUFNLEdBQUcsV0FBVyxHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUM1RCxDQUFDO1FBRUQ7O1dBRUc7UUFDSCxlQUFlLEtBQUssRUFBRSxPQUFPO1lBQ3pCLElBQUksUUFBUSxHQUFHLGtCQUFrQixFQUFFLENBQUM7WUFFcEMsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUNsQixPQUFPLElBQUksSUFBSSxHQUFHLFFBQVEsR0FBRyxRQUFRLENBQUM7YUFDekM7WUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsQ0FBQztZQUV2QyxRQUFRLEVBQUUsQ0FBQyxhQUFhLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixRQUFRLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDUCxDQUFDO0lBRUwsQ0FBQyxFQW5GUyxjQUFjLEtBQWQsY0FBYyxRQW1GdkI7SUFFRCxPQUFTLGNBQWMsQ0FBQyJ9