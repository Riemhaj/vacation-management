/// <amd-module name="Utility/Messenger" />
define("Utility/Messenger", ["require", "exports", "Management/ComponentManager", "Utility/TypeInfo"], function (require, exports, ComponentManager, TypeInfo) {
    "use strict";
    var Messenger = /** @class */ (function () {
        function Messenger() {
        }
        /*
         * Registriert einen Callback für eine Message
         */
        Messenger.registerCallbackForId = function (messageId, callback, callee) {
            Messenger.extendRegistrationIfNecessary(messageId, callee);
            Messenger.registrations[messageId].push(callback);
            var calleeRegistration = Messenger.callees.get(callee);
            calleeRegistration.push(callback);
            Messenger.callees.put(callee, calleeRegistration);
        };
        /*
         * Entfernt einen einzigen Callback zu einer Message aus der Registrierung
         */
        Messenger.unregisterCallbackFromMessage = function (messageId, callback) {
            Messenger.extendRegistrationIfNecessary(messageId);
            var index = Messenger.registrations[messageId].indexOf(callback);
            if (index > -1) {
                Messenger.registrations[messageId].splice(index, 1);
                return true;
            }
            return false;
        };
        /*
         * Entfernt alle Callbacks eines Callees aus der Registrierung
         */
        Messenger.unregisterCallbackFromCallee = function (messageId, callee) {
            Messenger.extendRegistrationIfNecessary(messageId, callee);
            var calleeRegistration = Messenger.callees.get(callee);
            var callbacksToRemove = [];
            for (var i = 0; i < calleeRegistration.length; i++) {
                var callback = calleeRegistration[i];
                if (Messenger.unregisterCallbackFromMessage(messageId, callback)) {
                    callbacksToRemove.push(callback);
                }
            }
            callbacksToRemove.forEach(function (callback) {
                Messenger.removeCallbackFromCallee(callback, calleeRegistration);
            });
            if (calleeRegistration.length > 0) {
                Messenger.callees.put(callee, calleeRegistration);
            }
            else {
                Messenger.callees.remove(callee);
            }
        };
        Messenger.removeCallbackFromCallee = function (callback, calleeRegistration) {
            var index = calleeRegistration.indexOf(callback);
            if (index > -1) {
                calleeRegistration.splice(index, 1);
            }
        };
        /*
         * Ruft alle registrierten Callbacks einer Message auf und übergibt die Message als Argument
         */
        Messenger.applyCallbacksForMessage = function (messageId, message) {
            Messenger.extendRegistrationIfNecessary(messageId);
            var callbacks = [];
            for (var i = 0; i < Messenger.registrations[messageId].length; i++) {
                var callback = Messenger.registrations[messageId][i];
                if (callback != null) {
                    callbacks.push(callback);
                }
            }
            callbacks.forEach(function (callback) {
                callback(message);
            });
            callbacks.length = 0;
        };
        /*
         * Erweitert das Registrierungs-Objekt um eine weitere Message-Id falls diese noch nicht im Objekt vorhanden ist
         */
        Messenger.extendRegistrationIfNecessary = function (messageId, callee) {
            if (Messenger.registrations[messageId] == null) {
                Messenger.registrations[messageId] = [];
            }
            if (callee != null && Messenger.callees.get(callee) == null) {
                Messenger.callees.put(callee, []);
            }
        };
        /*
         * Liefert eine Message anhand der Id
         */
        Messenger.getMessageById = function (messageId) {
            return ComponentManager.getMessage(messageId);
        };
        /*
         * Registriert einen Callback für eine Message
         */
        Messenger.register = function (Message, callback, callee) {
            var messageId = TypeInfo.getType(Message);
            Messenger.registerCallbackForId(messageId, callback, callee);
        };
        /*
         * Registriert einen Callback für eine Message und leitet die letzte Message direkt an das callback weiter
         */
        Messenger.registerAndReceive = function (Message, callback, callee) {
            Messenger.register(Message, callback, callee);
            var messageId = TypeInfo.getType(Message);
            var message = Messenger.messages[messageId];
            if (message != null && !message.isDisposed) {
                if (callback != null) {
                    callback(message);
                }
            }
        };
        /*
         * Registriert einen Callback für eine Message-Id
         */
        Messenger.registerForMessageId = function (messageId, callback, callee) {
            var Message = Messenger.getMessageById(messageId);
            if (Message != null) {
                Messenger.register(Message, callback, callee);
            }
        };
        /*
         * Registriert einen Callback für eine Message-Id und leitet die letzte Message direkt an das callback weiter
         */
        Messenger.registerForMessageIdAndReceive = function (messageId, callback, callee) {
            var Message = Messenger.getMessageById(messageId);
            if (Message != null) {
                Messenger.registerAndReceive(Message, callback, callee);
            }
        };
        /*
         * Entfernt einen einzigen Callback oder alle Callbacks zu einer Message aus der Registrierung
         */
        Messenger.unregister = function (Message, callback, callee) {
            var messageId = TypeInfo.getType(Message);
            if (callback != null) {
                Messenger.unregisterCallbackFromMessage(messageId, callback);
            }
            else if (callee != null) {
                Messenger.unregisterCallbackFromCallee(messageId, callee);
            }
        };
        /*
         * Entfernt einen einzigen Callback oder alle Callbacks zu einer Message aus der Registrierung
         */
        Messenger.unregisterForMessageId = function (messageId, callback, callee) {
            var Message = Messenger.getMessageById(messageId);
            if (Message == null) {
                throw 'TypeException: Es konnte keine Message zur angegebenen Id gefunden werden.';
            }
            Messenger.unregister(Message, callback, callee);
        };
        /*
         * Sendet die Message an alle registrierten Callbacks
         */
        Messenger.send = function (message) {
            var messageId = TypeInfo.getType(message);
            Messenger.applyCallbacksForMessage(messageId, message);
        };
        /*
         * Disposed eine zuvor erzeugte Message
         */
        Messenger.disposeMessage = function (messageId) {
            if (messageId in Messenger.messages) {
                Messenger.messages[messageId].dispose();
                Messenger.messages[messageId] = null;
                delete Messenger.messages[messageId];
            }
        };
        /*
         * Erstellt eine neue Message anhand der Id
         */
        Messenger.createMessage = function (messageId, params) {
            Messenger.disposeMessage(messageId);
            var Message = Messenger.getMessageById(messageId);
            if (Message == null) {
                return null;
            }
            var message = new Message(params);
            Messenger.messages[messageId] = message;
            return message;
        };
        /*
         * Erstellt eine neue Message anhand der Id und sendet diese an alle registrierten Callbacks
         */
        Messenger.createMessageAndSend = function (messageId, params) {
            var message = Messenger.createMessage(messageId, params);
            if (message != null) {
                Messenger.send(message);
            }
            return message;
        };
        /*
         * Ein Objekt welches die registrierten Callbacks beinhaltet
         */
        Messenger.registrations = {};
        /*
         * Ein Objekt welches die registrierten Callbacks beinhaltet
         */
        Messenger.callees = new Hashtable();
        /*
         * Ein Objekt mit erzeugten Nachrichten
         */
        Messenger.messages = {};
        return Messenger;
    }());
    return Messenger;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVzc2VuZ2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTWVzc2VuZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDJDQUEyQzs7O0lBTzNDO1FBQUE7UUF3UEEsQ0FBQztRQXZPRzs7V0FFRztRQUNZLCtCQUFxQixHQUFwQyxVQUFxQyxTQUFTLEVBQUUsUUFBUSxFQUFFLE1BQU07WUFDNUQsU0FBUyxDQUFDLDZCQUE2QixDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUUzRCxTQUFTLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUVsRCxJQUFJLGtCQUFrQixHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELGtCQUFrQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUN0RCxDQUFDO1FBRUQ7O1dBRUc7UUFDWSx1Q0FBNkIsR0FBNUMsVUFBNkMsU0FBUyxFQUFFLFFBQVE7WUFDNUQsU0FBUyxDQUFDLDZCQUE2QixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRW5ELElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRWpFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNaLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDcEQsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRDs7V0FFRztRQUNZLHNDQUE0QixHQUEzQyxVQUE0QyxTQUFTLEVBQUUsTUFBTTtZQUN6RCxTQUFTLENBQUMsNkJBQTZCLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRTNELElBQUksa0JBQWtCLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdkQsSUFBSSxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFFM0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDaEQsSUFBSSxRQUFRLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRXJDLElBQUksU0FBUyxDQUFDLDZCQUE2QixDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsRUFBRTtvQkFDOUQsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUNwQzthQUNKO1lBRUQsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBUTtnQkFDL0IsU0FBUyxDQUFDLHdCQUF3QixDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3JFLENBQUMsQ0FBQyxDQUFDO1lBR0gsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMvQixTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUNyRDtpQkFBTTtnQkFDSCxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNwQztRQUNMLENBQUM7UUFFYyxrQ0FBd0IsR0FBdkMsVUFBd0MsUUFBUSxFQUFFLGtCQUFrQjtZQUNoRSxJQUFJLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFakQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1osa0JBQWtCLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN2QztRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNZLGtDQUF3QixHQUF2QyxVQUF3QyxTQUFTLEVBQUUsT0FBTztZQUN0RCxTQUFTLENBQUMsNkJBQTZCLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkQsSUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBRXJCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDaEUsSUFBSSxRQUFRLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFckQsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNsQixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUM1QjthQUNKO1lBRUQsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVE7Z0JBQ3ZCLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLENBQUM7UUFFRDs7V0FFRztRQUNZLHVDQUE2QixHQUE1QyxVQUE2QyxTQUFTLEVBQUUsTUFBTztZQUMzRCxJQUFJLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUM1QyxTQUFTLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUMzQztZQUVELElBQUksTUFBTSxJQUFJLElBQUksSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3pELFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNyQztRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNZLHdCQUFjLEdBQTdCLFVBQThCLFNBQVM7WUFDbkMsT0FBUSxnQkFBd0IsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDM0QsQ0FBQztRQUdEOztXQUVHO1FBQ1csa0JBQVEsR0FBdEIsVUFBdUIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNO1lBQzVDLElBQUksU0FBUyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFMUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDakUsQ0FBQztRQUVEOztXQUVHO1FBQ1csNEJBQWtCLEdBQWhDLFVBQWlDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTTtZQUN0RCxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFOUMsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLElBQUksT0FBTyxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUU7Z0JBRXhDLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtvQkFDbEIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNyQjthQUNKO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csOEJBQW9CLEdBQWxDLFVBQW1DLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTTtZQUMxRCxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRWxELElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDakIsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ2pEO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csd0NBQThCLEdBQTVDLFVBQTZDLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTTtZQUNwRSxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRWxELElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDakIsU0FBUyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDM0Q7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxvQkFBVSxHQUF4QixVQUF5QixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU07WUFDOUMsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUUxQyxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ2xCLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDaEU7aUJBQU0sSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO2dCQUN2QixTQUFTLENBQUMsNEJBQTRCLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQzdEO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csZ0NBQXNCLEdBQXBDLFVBQXFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTTtZQUM1RCxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRWxELElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDakIsTUFBTSw0RUFBNEUsQ0FBQzthQUN0RjtZQUVELFNBQVMsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxjQUFJLEdBQWxCLFVBQW1CLE9BQU87WUFDdEIsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUUxQyxTQUFTLENBQUMsd0JBQXdCLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzNELENBQUM7UUFFRDs7V0FFRztRQUNXLHdCQUFjLEdBQTVCLFVBQTZCLFNBQVM7WUFDbEMsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtnQkFDakMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDeEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBQ3JDLE9BQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN4QztRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLHVCQUFhLEdBQTNCLFVBQTRCLFNBQVMsRUFBRSxNQUFNO1lBQ3pDLFNBQVMsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFcEMsSUFBSSxPQUFPLEdBQUcsU0FBUyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQVEsQ0FBQztZQUV6RCxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ2pCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNsQyxTQUFTLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztZQUV4QyxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyw4QkFBb0IsR0FBbEMsVUFBbUMsU0FBUyxFQUFFLE1BQU07WUFDaEQsSUFBSSxPQUFPLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFekQsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUNqQixTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzNCO1lBRUQsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQztRQXBQRDs7V0FFRztRQUNZLHVCQUFhLEdBQUcsRUFBRSxDQUFDO1FBRWxDOztXQUVHO1FBQ1ksaUJBQU8sR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO1FBRXpDOztXQUVHO1FBQ1ksa0JBQVEsR0FBRyxFQUFFLENBQUM7UUF5T2pDLGdCQUFDO0tBQUEsQUF4UEQsSUF3UEM7SUFFRCxPQUFTLFNBQVMsQ0FBQyJ9