/// <amd-module name="Utility/Convert" />
define("Utility/Convert", ["require", "exports", "Management/RADApplication"], function (require, exports, RADApplication) {
    "use strict";
    var Convert;
    (function (Convert) {
        function XMLStringToObject(string) {
            if (string) {
                //string = string.replace(/&#xD;?&#xA;/gm, '\\n');
                var xml = $.parseXML(string);
                var json = xml2json(xml, '');
                json = json.replace(/(\r\n|\n|\r)/gm, '');
                json = _.unescape(json);
                var object = JSON.parse(json);
                return object;
            }
            return null;
        }
        Convert.XMLStringToObject = XMLStringToObject;
        function ObjectToXMLString(object) {
            var xml = '<?xml version="1.0" encoding="utf-16" ?>' + json2xml(object, '');
            return xml;
        }
        Convert.ObjectToXMLString = ObjectToXMLString;
        function UpdateDataHandlerURL(url) {
            if (!url) {
                return null;
            }
            url = url.replace('$$DATA_HANDLER$$', RADApplication.radEnvironmentDataHandler);
            url += '&UserToken=' + RADApplication.token;
            return url;
        }
        Convert.UpdateDataHandlerURL = UpdateDataHandlerURL;
    })(Convert || (Convert = {}));
    return Convert;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udmVydC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnZlcnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEseUNBQXlDOzs7SUFPekMsSUFBVSxPQUFPLENBK0JoQjtJQS9CRCxXQUFVLE9BQU87UUFFYiwyQkFBa0MsTUFBYztZQUM1QyxJQUFJLE1BQU0sRUFBRTtnQkFDUixrREFBa0Q7Z0JBQ2xELElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdCLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzdCLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFOUIsT0FBTyxNQUFNLENBQUM7YUFDakI7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBYmUseUJBQWlCLG9CQWFoQyxDQUFBO1FBRUQsMkJBQWtDLE1BQVU7WUFDeEMsSUFBSSxHQUFHLEdBQUcsMENBQTBDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM1RSxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUM7UUFIZSx5QkFBaUIsb0JBR2hDLENBQUE7UUFFRCw4QkFBcUMsR0FBVztZQUM1QyxJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNOLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxjQUFjLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNoRixHQUFHLElBQUksYUFBYSxHQUFHLGNBQWMsQ0FBQyxLQUFLLENBQUM7WUFDNUMsT0FBTyxHQUFHLENBQUM7UUFDZixDQUFDO1FBUmUsNEJBQW9CLHVCQVFuQyxDQUFBO0lBQ0wsQ0FBQyxFQS9CUyxPQUFPLEtBQVAsT0FBTyxRQStCaEI7SUFFRCxPQUFTLE9BQU8sQ0FBQyJ9