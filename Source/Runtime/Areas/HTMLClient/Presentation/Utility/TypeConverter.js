/// <amd-module name="Utility/TypeConverter" />
define("Utility/TypeConverter", ["require", "exports", "Utility/Utilities"], function (require, exports, Utilities) {
    "use strict";
    var TypeConverter = /** @class */ (function () {
        function TypeConverter() {
        }
        TypeConverter.isValidDotNetType = function (value, dotNetTypeName) {
            if (!dotNetTypeName) {
                return true;
            }
            if (TypeConverter.typeIsArray(dotNetTypeName)) {
                return TypeConverter.isArrayValidDotNetType(value, dotNetTypeName, ',');
            }
            else {
                return TypeConverter.isValueValidDotNetType(value, dotNetTypeName);
            }
        };
        TypeConverter.parseIntoDotNetType = function (value, dotNetTypeName) {
            if (!TypeConverter.isValidDotNetType(value, dotNetTypeName)) {
                throw 'Cannot convert "' + value + '" into "' + dotNetTypeName + '"';
            }
        };
        /**
         * Prüft für jeden Wert eines Arrays, ob dieser in den dotNetType geparst werden kann
         */
        TypeConverter.isArrayValidDotNetType = function (value, dotNetTypeName, separator) {
            var result = true;
            var values = Utilities.isNullOrUndefined(value) ? [] : value.split(separator);
            for (var i = 0; i < values.length; i++) {
                if (!TypeConverter.isValueValidDotNetType(values[i], dotNetTypeName)) {
                    result = false;
                }
            }
            return result;
        };
        /**
         * Prüft einen Wert, ob dieser in den dotNetType geparst werden kann
         */
        TypeConverter.isValueValidDotNetType = function (value, dotNetTypeName) {
            if (Utilities.isNullOrUndefined(value) && !TypeConverter.typeIsNullable(dotNetTypeName)) {
                return false;
            }
            if (TypeConverter.typeIsBool(dotNetTypeName) && !TypeConverter.valueIsBool(value)) {
                return false;
            }
            if (TypeConverter.typeIsChar(dotNetTypeName) && !TypeConverter.valueIsChar(value)) {
                return false;
            }
            if (TypeConverter.typeIsNumber(dotNetTypeName) && !TypeConverter.valueIsNumber(value, dotNetTypeName)) {
                return false;
            }
            return true;
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Array-Typ ist
         */
        TypeConverter.typeIsArray = function (dotNetTypeName) {
            return dotNetTypeName.indexOf('[]') > -1;
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Nullable-Typ ist
         */
        TypeConverter.typeIsNullable = function (dotNetTypeName) {
            return dotNetTypeName.indexOf('System.Nullable') === 0 ||
                TypeConverter.typeIsString(dotNetTypeName);
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Char ist
         */
        TypeConverter.typeIsChar = function (dotNetTypeName) {
            return dotNetTypeName === 'System.Char';
        };
        /**
         * Prüft, ob ein übergebener string in ein char geparst werden kann
         */
        TypeConverter.valueIsChar = function (value) {
            return !Utilities.isNullOrUndefined(value) && value.length === 1;
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Char ist
         */
        TypeConverter.typeIsString = function (dotNetTypeName) {
            return dotNetTypeName === 'System.String';
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein DateTime ist
         */
        TypeConverter.typeIsDateTime = function (dotNetTypeName) {
            return dotNetTypeName === 'System.DateTime'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.DateTime');
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Boolean ist
         */
        TypeConverter.typeIsBool = function (dotNetTypeName) {
            return dotNetTypeName === 'System.Boolean';
        };
        /**
         * Prüft, ob ein übergebener string in ein bool geparst werden kann
         */
        TypeConverter.valueIsBool = function (value) {
            if (!Utilities.isNullOrUndefined(value)) {
                value = value.toLowerCase();
                return !Utilities.isNullOrUndefined(value) && (value === 'true' || value === 'false');
            }
            return false;
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein Byte ist
         */
        TypeConverter.typeIsByte = function (dotNetTypeName) {
            return dotNetTypeName === 'System.Byte';
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein numerischer Typ ist
         */
        TypeConverter.typeIsNumber = function (dotNetTypeName) {
            return TypeConverter.typeIsFloatingNumber(dotNetTypeName) || TypeConverter.typeIsNonFloatingNumber(dotNetTypeName);
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein numerischer gleitkomma-Typ ist
         */
        TypeConverter.typeIsFloatingNumber = function (dotNetTypeName) {
            if (Utilities.isNullOrUndefined(dotNetTypeName))
                return false;
            return dotNetTypeName === 'System.Single'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Single')
                || dotNetTypeName === 'System.Double'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Double')
                || dotNetTypeName === 'System.Decimal'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Decimal')
                || dotNetTypeName === 'System.Float'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Float')
                || dotNetTypeName === 'System.Short'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Short')
                || dotNetTypeName === 'System.Long'
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Long');
        };
        /**
         * Prüft, ob ein übergebener dotNetTypeName ein numerischer nicht-gleitkomma-Typ ist
         */
        TypeConverter.typeIsNonFloatingNumber = function (dotNetTypeName) {
            if (Utilities.isNullOrUndefined(dotNetTypeName))
                return false;
            return dotNetTypeName.startsWith('System.Int')
                || dotNetTypeName.startsWith('System.Nullable`1[[System.Int');
        };
        /**
         * Prüft, ob ein übergebener string in einen numerischen Typ geparst werden kann
         */
        TypeConverter.valueIsNumber = function (value, dotNetTypeName) {
            if (TypeConverter.typeIsFloatingNumber(dotNetTypeName)) {
                return TypeConverter.valueIsFloatingNumber(value);
            }
            if (TypeConverter.typeIsNonFloatingNumber(dotNetTypeName)) {
                return TypeConverter.valueIsNonFloatingNumber(value);
            }
            return false;
        };
        /**
         * Prüft, ob ein übergebener string in einen numerischen Gleitkomma-Typen geparst werden kann
         */
        TypeConverter.valueIsFloatingNumber = function (value) {
            return !Utilities.isNullOrUndefined(value) && !isNaN(value);
        };
        /**
         * Prüft, ob ein übergebener string in einen numerischen Nicht-Gleitkomma-Typen geparst werden kann
         */
        TypeConverter.valueIsNonFloatingNumber = function (value) {
            return !Utilities.isNullOrUndefined(value) && !isNaN(value) && !TypeConverter.valueHasDecimalSeparator(value);
        };
        /**
         * Prüft, ob ein übergebener string einen Decimal separator enthält
         */
        TypeConverter.valueHasDecimalSeparator = function (value) {
            var separator = TypeConverter.getDecimalSeparator();
            if (!Utilities.isNullOrUndefined(value)) {
                return value.indexOf(separator) > -1;
            }
            return false;
        };
        /**
         * Liefert den Decimal separator der aktuell ausgewählten Sprache
         */
        TypeConverter.getDecimalSeparator = function () {
            var ResourceManager = require('Management/ResourceManager');
            var language = ResourceManager.getLanguage();
            var currencyPattern = language.currencyPattern;
            var replaced = currencyPattern.replace(/#/g, '');
            var decimalSeparator = replaced[replaced.length - 1];
            return decimalSeparator;
        };
        return TypeConverter;
    }());
    return TypeConverter;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVHlwZUNvbnZlcnRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlR5cGVDb252ZXJ0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0NBQStDOzs7SUFJL0M7UUFBQTtRQWdPQSxDQUFDO1FBOU5pQiwrQkFBaUIsR0FBL0IsVUFBZ0MsS0FBSyxFQUFFLGNBQWM7WUFDakQsSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDakIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELElBQUksYUFBYSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDM0MsT0FBTyxhQUFhLENBQUMsc0JBQXNCLENBQUMsS0FBSyxFQUFFLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUMzRTtpQkFBTTtnQkFDSCxPQUFPLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLEVBQUUsY0FBYyxDQUFDLENBQUM7YUFDdEU7UUFDTCxDQUFDO1FBRWEsaUNBQW1CLEdBQWpDLFVBQWtDLEtBQUssRUFBRSxjQUFjO1lBQ25ELElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxFQUFFO2dCQUN6RCxNQUFNLGtCQUFrQixHQUFHLEtBQUssR0FBRyxVQUFVLEdBQUcsY0FBYyxHQUFHLEdBQUcsQ0FBQzthQUN4RTtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLG9DQUFzQixHQUFwQyxVQUFxQyxLQUFLLEVBQUUsY0FBYyxFQUFFLFNBQVM7WUFDakUsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRTlFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxjQUFjLENBQUMsRUFBRTtvQkFDbEUsTUFBTSxHQUFHLEtBQUssQ0FBQztpQkFDbEI7YUFDSjtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFRDs7V0FFRztRQUNXLG9DQUFzQixHQUFwQyxVQUFxQyxLQUFVLEVBQUUsY0FBc0I7WUFDbkUsSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUNyRixPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksYUFBYSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQy9FLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsSUFBSSxhQUFhLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDL0UsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxJQUFJLGFBQWEsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxjQUFjLENBQUMsRUFBRTtnQkFDbkcsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyx5QkFBVyxHQUF6QixVQUEwQixjQUFzQjtZQUM1QyxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDN0MsQ0FBQztRQUVEOztXQUVHO1FBQ1csNEJBQWMsR0FBNUIsVUFBNkIsY0FBc0I7WUFDL0MsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQztnQkFDbEQsYUFBYSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNuRCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx3QkFBVSxHQUF4QixVQUF5QixjQUFzQjtZQUMzQyxPQUFPLGNBQWMsS0FBSyxhQUFhLENBQUM7UUFDNUMsQ0FBQztRQUVEOztXQUVHO1FBQ1cseUJBQVcsR0FBekIsVUFBMEIsS0FBSztZQUMzQixPQUFPLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFRDs7V0FFRztRQUNXLDBCQUFZLEdBQTFCLFVBQTJCLGNBQXNCO1lBQzdDLE9BQU8sY0FBYyxLQUFLLGVBQWUsQ0FBQztRQUM5QyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyw0QkFBYyxHQUE1QixVQUE2QixjQUFzQjtZQUMvQyxPQUFPLGNBQWMsS0FBSyxpQkFBaUI7bUJBQ3BDLGNBQWMsQ0FBQyxVQUFVLENBQUMsb0NBQW9DLENBQUMsQ0FBQztRQUMzRSxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx3QkFBVSxHQUF4QixVQUF5QixjQUFzQjtZQUMzQyxPQUFPLGNBQWMsS0FBSyxnQkFBZ0IsQ0FBQztRQUMvQyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx5QkFBVyxHQUF6QixVQUEwQixLQUFVO1lBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3JDLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBRTVCLE9BQU8sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssTUFBTSxJQUFJLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQzthQUN6RjtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRDs7V0FFRztRQUNXLHdCQUFVLEdBQXhCLFVBQXlCLGNBQXNCO1lBQzNDLE9BQU8sY0FBYyxLQUFLLGFBQWEsQ0FBQztRQUM1QyxDQUFDO1FBRUQ7O1dBRUc7UUFDVywwQkFBWSxHQUExQixVQUEyQixjQUFzQjtZQUM3QyxPQUFPLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxhQUFhLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdkgsQ0FBQztRQUVEOztXQUVHO1FBQ1csa0NBQW9CLEdBQWxDLFVBQW1DLGNBQXNCO1lBQ3JELElBQUksU0FBUyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQztnQkFDM0MsT0FBTyxLQUFLLENBQUM7WUFFakIsT0FBTyxjQUFjLEtBQUssZUFBZTttQkFDbEMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxrQ0FBa0MsQ0FBQzttQkFDN0QsY0FBYyxLQUFLLGVBQWU7bUJBQ2xDLGNBQWMsQ0FBQyxVQUFVLENBQUMsa0NBQWtDLENBQUM7bUJBQzdELGNBQWMsS0FBSyxnQkFBZ0I7bUJBQ25DLGNBQWMsQ0FBQyxVQUFVLENBQUMsbUNBQW1DLENBQUM7bUJBQzlELGNBQWMsS0FBSyxjQUFjO21CQUNqQyxjQUFjLENBQUMsVUFBVSxDQUFDLGlDQUFpQyxDQUFDO21CQUM1RCxjQUFjLEtBQUssY0FBYzttQkFDakMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxpQ0FBaUMsQ0FBQzttQkFDNUQsY0FBYyxLQUFLLGFBQWE7bUJBQ2hDLGNBQWMsQ0FBQyxVQUFVLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztRQUN2RSxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxxQ0FBdUIsR0FBckMsVUFBc0MsY0FBc0I7WUFDeEQsSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDO2dCQUMzQyxPQUFPLEtBQUssQ0FBQztZQUVqQixPQUFPLGNBQWMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO21CQUN2QyxjQUFjLENBQUMsVUFBVSxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFDdEUsQ0FBQztRQUVEOztXQUVHO1FBQ1csMkJBQWEsR0FBM0IsVUFBNEIsS0FBVSxFQUFFLGNBQXNCO1lBQzFELElBQUksYUFBYSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUNwRCxPQUFPLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNyRDtZQUVELElBQUksYUFBYSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUN2RCxPQUFPLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN4RDtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRDs7V0FFRztRQUNXLG1DQUFxQixHQUFuQyxVQUFvQyxLQUFVO1lBQzFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEUsQ0FBQztRQUVEOztXQUVHO1FBQ1csc0NBQXdCLEdBQXRDLFVBQXVDLEtBQVU7WUFDN0MsT0FBTyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsSCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxzQ0FBd0IsR0FBdEMsVUFBdUMsS0FBYTtZQUNoRCxJQUFJLFNBQVMsR0FBRyxhQUFhLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztZQUVwRCxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNyQyxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDeEM7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyxpQ0FBbUIsR0FBakM7WUFDSSxJQUFJLGVBQWUsR0FBRyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUM1RCxJQUFJLFFBQVEsR0FBRyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDN0MsSUFBSSxlQUFlLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQztZQUUvQyxJQUFJLFFBQVEsR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNqRCxJQUFJLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBRXJELE9BQU8sZ0JBQWdCLENBQUM7UUFDNUIsQ0FBQztRQUNMLG9CQUFDO0lBQUQsQ0FBQyxBQWhPRCxJQWdPQztJQUVELE9BQVMsYUFBYSxDQUFDIn0=