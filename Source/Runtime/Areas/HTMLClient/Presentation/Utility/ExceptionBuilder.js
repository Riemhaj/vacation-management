/// <amd-module name="Utility/ExceptionBuilder" />
define("Utility/ExceptionBuilder", ["require", "exports", "Utility/Loader", "Management/RADApplication", "Management/ComponentManager", "Utility/LoggingManager"], function (require, exports, Loader, RADApplication, ComponentManager, LoggingManager) {
    "use strict";
    var ExceptionBuilder = /** @class */ (function () {
        function ExceptionBuilder() {
            window.onerror = ExceptionBuilder.throw;
        }
        ;
        ExceptionBuilder.getInstance = function () {
            if (ExceptionBuilder.instance == null)
                ExceptionBuilder.instance = new ExceptionBuilder();
            return ExceptionBuilder.instance;
        };
        ExceptionBuilder.throw = function (msg, script, line, column, err) {
            script = script.replace(RADApplication.serverUrl, '');
            script = script.replace(MODEL.HTMLClientResourceURL, '');
            var extensionIndex = script.indexOf('Extension/');
            if (extensionIndex > -1) {
                script = script.substring(extensionIndex + 10);
            }
            var message = msg + '\nScript: ' + script + '\nZeile: ' + line + ' Spalte: ' + column;
            var stack = ExceptionBuilder.getInstance().getStackFromError(err);
            ExceptionBuilder.getInstance().showError(message, stack, null);
            Loader.removeAll();
        };
        ExceptionBuilder.onCommunicationError = function () {
            if (ExceptionBuilder.hasCommunicationError === false) {
                ExceptionBuilder.hasCommunicationError = true;
                ExceptionBuilder.getInstance().showCommunicationError();
                Loader.removeAll();
            }
        };
        ExceptionBuilder.throwDotNet = function (operation, keys, responseText, args) {
            var data = {};
            for (var i = 0; i < keys.length; i++) {
                data[keys[i]] = args[keys[i]] || null;
            }
            if (responseText) {
                var builder = ExceptionBuilder.getInstance();
                var stack = builder.getStackFromException(responseText);
                builder.showError('Der Aufruf der Operation "' + operation + '" ist fehlgeschlagen.', stack, data, true);
            }
            Loader.removeAll();
        };
        ExceptionBuilder.prototype.showCommunicationError = function () {
            var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            var NavigationService = require('Management/NavigationService');
            if (MessageBoxViewModel != null) {
                RADApplication.showMessageBox(MessageBoxViewModel.createErrorMessageBox(null, 'COMMUNICATION_ERROR', null, null, function () {
                    NavigationService.reload();
                }));
            }
        };
        ExceptionBuilder.prototype.showError = function (message, stack, data, withoutErgNames) {
            var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            if (MessageBoxViewModel != null) {
                if (withoutErgNames) {
                    RADApplication.showMessageBox(MessageBoxViewModel.createErrorMessageBoxWithoutErgnames(message, stack, data));
                }
                else {
                    RADApplication.showMessageBox(MessageBoxViewModel.createErrorMessageBox(message, null, stack, data));
                }
                LoggingManager.logError(LoggingManager.LogLevel.Error, document.location.href, message + " \n\r " + stack);
            }
        };
        ExceptionBuilder.prototype.getStackFromError = function (err) {
            var stack = printStackTrace({ e: err });
            var clientStack = [];
            for (var i in stack) {
                var line = stack[i];
                var rest = 221 - line.length;
                var blank = '\u00A0';
                var replacement = '';
                for (var j = 1; j <= rest; j++) {
                    replacement += blank;
                }
                line = line.replace('@', replacement);
                if (line.indexOf('Scripts/') === -1) {
                    clientStack.push(line);
                }
            }
            return clientStack.join('\n');
        };
        ExceptionBuilder.prototype.getStackFromException = function (exc) {
            var stack = exc + '';
            stack = stack.replace(/\"/g, '');
            stack = stack.replace(/\s/g, '\u00A0');
            stack = stack.replace(/\\u003c/g, '<');
            stack = stack.replace(/\\u003e/g, '>');
            stack = stack.replace(/([^>\\r\\n]?)(\\r\\n|\\n\\r|\\r|\\n)/g, '$1\n');
            return stack;
        };
        ExceptionBuilder.hasCommunicationError = false;
        return ExceptionBuilder;
    }());
    return ExceptionBuilder;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXhjZXB0aW9uQnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkV4Y2VwdGlvbkJ1aWxkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsa0RBQWtEOzs7SUFVbEQ7UUFLSTtZQUNJLE1BQU0sQ0FBQyxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBQzVDLENBQUM7UUFMb0QsQ0FBQztRQU92Qyw0QkFBVyxHQUExQjtZQUNJLElBQUksZ0JBQWdCLENBQUMsUUFBUSxJQUFJLElBQUk7Z0JBQ2pDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7WUFFdkQsT0FBTyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7UUFDckMsQ0FBQztRQUVhLHNCQUFLLEdBQW5CLFVBQW9CLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHO1lBQzlDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdEQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLHFCQUFxQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRXpELElBQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEQsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3JCLE1BQU0sR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsQ0FBQzthQUNsRDtZQUVELElBQUksT0FBTyxHQUFHLEdBQUcsR0FBRyxZQUFZLEdBQUcsTUFBTSxHQUFHLFdBQVcsR0FBRyxJQUFJLEdBQUcsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUN0RixJQUFJLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVsRSxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvRCxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUVhLHFDQUFvQixHQUFsQztZQUNJLElBQUksZ0JBQWdCLENBQUMscUJBQXFCLEtBQUssS0FBSyxFQUFFO2dCQUNsRCxnQkFBZ0IsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7Z0JBRTlDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUN0QjtRQUNMLENBQUM7UUFFYSw0QkFBVyxHQUF6QixVQUEwQixTQUFTLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxJQUFJO1lBQ3pELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUVkLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQzthQUN6QztZQUVELElBQUksWUFBWSxFQUFFO2dCQUNkLElBQUksT0FBTyxHQUFHLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUM3QyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3hELE9BQU8sQ0FBQyxTQUFTLENBQUMsNEJBQTRCLEdBQUcsU0FBUyxHQUFHLHVCQUF1QixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDNUc7WUFFRCxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUVPLGlEQUFzQixHQUE5QjtZQUNJLElBQUksbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFRLENBQUM7WUFDdEYsSUFBSSxpQkFBaUIsR0FBRyxPQUFPLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUVoRSxJQUFJLG1CQUFtQixJQUFJLElBQUksRUFBRTtnQkFDN0IsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUscUJBQXFCLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRTtvQkFDN0csaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQy9CLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDUDtRQUNMLENBQUM7UUFFTyxvQ0FBUyxHQUFqQixVQUFrQixPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxlQUFnQjtZQUNwRCxJQUFJLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBUSxDQUFDO1lBRXRGLElBQUksbUJBQW1CLElBQUksSUFBSSxFQUFFO2dCQUM3QixJQUFJLGVBQWUsRUFBRTtvQkFDakIsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxvQ0FBb0MsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2pIO3FCQUNJO29CQUNELGNBQWMsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDeEc7Z0JBRUQsY0FBYyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxPQUFPLEdBQUcsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDO2FBQzlHO1FBQ0wsQ0FBQztRQUVPLDRDQUFpQixHQUF6QixVQUEwQixHQUFHO1lBQ3pCLElBQUksS0FBSyxHQUFHLGVBQWUsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ3hDLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUVyQixLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRTtnQkFDakIsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDN0IsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDO2dCQUNyQixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBRXJCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQzVCLFdBQVcsSUFBSSxLQUFLLENBQUM7aUJBQ3hCO2dCQUVELElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFFdEMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUNqQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUMxQjthQUNKO1lBRUQsT0FBTyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLENBQUM7UUFFTyxnREFBcUIsR0FBN0IsVUFBOEIsR0FBRztZQUM3QixJQUFJLEtBQUssR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDO1lBRXJCLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNqQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDdkMsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyx1Q0FBdUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUV2RSxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBbkhhLHNDQUFxQixHQUFZLEtBQUssQ0FBQztRQW9IekQsdUJBQUM7S0FBQSxBQXRIRCxJQXNIQztJQUVELE9BQVMsZ0JBQWdCLENBQUMifQ==