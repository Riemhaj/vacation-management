/// <amd-module name="Utility/LastResponseAcceptor" />
define("Utility/LastResponseAcceptor", ["require", "exports"], function (require, exports) {
    "use strict";
    //    /*
    //     * Erlaubt es asynchrone Requests durchzuführen und dabei nur die Response weiterzuleiten,
    //     * die vom letzten Request stammt. Wird also während der Dauer eines Requests ein weiterer Request gestellt,
    //     * so wird die erste Antwort verworfen.
    //     * Optional kann im Konstruktor ein Disposable übergeben werden, welches beim Eintreffen der Response noch nicht disposed sein darf.
    //     *
    //     * Rückgabe ist stets ein Promise.
    //     *
    //     * Anwendungsbeispiele:
    //     * var acceptor = new LastResponseAcceptor(self);
    //     * var yes = ... Neuste-Response-erhalten-Callback ...; 
    //     * var no  = ... Alte-Response-verworfen-Callback ...;
    //     *
    //     * // Deferred/Promise-Variante (gibt ein Promise zurück)
    //     * acceptor.query($.ajax(...)).done(yes).fail(no);
    //     *
    //     * // Beliebige Requests (am Beispiel eines Ajax-Requests):
    //     * acceptor.query(function(resolveIfLast, reject) {
    //     *     asyncOperation(function(data) {
    //     *         if (data.isOk) {
    //     *             resolveIfLast(data);   // Äußeres Promise rejecten wenn schon neue Requests vorliegen, ansonsten resolven
    //     *         }
    //     *         else {
    //     *             reject("error"); // Äußeres Promise rejecten
    //     *         }
    //     *     });
    //     * }).done(yes).fail(no);
    //     * 
    //     * 
    //     * 
    //     */
    var LastResponseAcceptor = /** @class */ (function () {
        function LastResponseAcceptor(optionalDisposable) {
            this.optionalDisposable = optionalDisposable;
            this.callId = 0;
        }
        LastResponseAcceptor.prototype.query = function (requester) {
            if (requester == null) {
                return;
            }
            if (typeof requester === 'function') {
                return this._request(requester);
            }
            if (typeof requester.done === 'function' && typeof requester.fail === 'function') {
                return this._promise(requester);
            }
            return null;
        };
        LastResponseAcceptor.prototype._promise = function (promise) {
            var myCallId = ++this.callId;
            var def = $.Deferred();
            var self = this;
            promise
                .fail(def.reject) // Reject directly if given promise fails
                .done(function () {
                if (myCallId === self.callId && (self.optionalDisposable == null || self.optionalDisposable.isDisposed === false)) {
                    def.resolve.apply(def, arguments); // Resolve if call is current and disposable is not disposed
                }
                else {
                    def.reject(null); // Reject if call is not current or disposable is disposed
                }
            });
            return def.promise();
        };
        LastResponseAcceptor.prototype._request = function (performRequest) {
            var def = $.Deferred();
            performRequest(def.resolve, def.reject);
            return this._promise(def.promise());
        };
        return LastResponseAcceptor;
    }());
    return LastResponseAcceptor;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGFzdFJlc3BvbnNlQWNjZXB0b3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJMYXN0UmVzcG9uc2VBY2NlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzREFBc0Q7OztJQUV0RCxRQUFRO0lBQ1IsZ0dBQWdHO0lBQ2hHLGtIQUFrSDtJQUNsSCw2Q0FBNkM7SUFDN0MsMElBQTBJO0lBQzFJLFFBQVE7SUFDUix3Q0FBd0M7SUFDeEMsUUFBUTtJQUNSLDZCQUE2QjtJQUM3Qix1REFBdUQ7SUFDdkQsOERBQThEO0lBQzlELDREQUE0RDtJQUM1RCxRQUFRO0lBQ1IsK0RBQStEO0lBQy9ELHdEQUF3RDtJQUN4RCxRQUFRO0lBQ1IsaUVBQWlFO0lBQ2pFLHlEQUF5RDtJQUN6RCw0Q0FBNEM7SUFDNUMsaUNBQWlDO0lBQ2pDLDhIQUE4SDtJQUM5SCxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLGlFQUFpRTtJQUNqRSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLCtCQUErQjtJQUMvQixTQUFTO0lBQ1QsU0FBUztJQUNULFNBQVM7SUFDVCxTQUFTO0lBQ1Q7UUFJSSw4QkFBb0Isa0JBQWtCO1lBQWxCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBQTtZQUNsQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBRU0sb0NBQUssR0FBWixVQUFhLFNBQVM7WUFDbEIsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUNuQixPQUFPO2FBQ1Y7WUFFRCxJQUFJLE9BQU8sU0FBUyxLQUFLLFVBQVUsRUFBRTtnQkFDakMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ25DO1lBRUQsSUFBSSxPQUFPLFNBQVMsQ0FBQyxJQUFJLEtBQUssVUFBVSxJQUFJLE9BQU8sU0FBUyxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7Z0JBQzlFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNuQztZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx1Q0FBUSxHQUFmLFVBQWdCLE9BQU87WUFDbkIsSUFBSSxRQUFRLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO1lBRTdCLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUV2QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFFaEIsT0FBTztpQkFDRixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFHLHlDQUF5QztpQkFDNUQsSUFBSSxDQUFDO2dCQUNGLElBQUksUUFBUSxLQUFLLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEtBQUssS0FBSyxDQUFDLEVBQUU7b0JBQy9HLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFHLDREQUE0RDtpQkFDcEc7cUJBQ0k7b0JBQ0QsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFvQiwwREFBMEQ7aUJBQ2xHO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFUCxPQUFPLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBRU0sdUNBQVEsR0FBZixVQUFnQixjQUFjO1lBQzFCLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUV2QixjQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFeEMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFDTCwyQkFBQztJQUFELENBQUMsQUFwREQsSUFvREM7SUFFRCxPQUFTLG9CQUFvQixDQUFDIn0=