/// <amd-module name="Utility/LocalizationHelper" />
define("Utility/LocalizationHelper", ["require", "exports"], function (require, exports) {
    "use strict";
    var LocalizationHelper;
    (function (LocalizationHelper) {
        function getLanguageFullName(name) {
            var lowerCaseName = name.toLowerCase();
            switch (lowerCaseName) {
                case 'de':
                    return 'Deutsch';
                case 'en':
                    return 'English';
                case 'fr':
                    return 'Français';
            }
            return name;
        }
        LocalizationHelper.getLanguageFullName = getLanguageFullName;
    })(LocalizationHelper || (LocalizationHelper = {}));
    return LocalizationHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9jYWxpemF0aW9uSGVscGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTG9jYWxpemF0aW9uSGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9EQUFvRDs7O0lBRXBELElBQVUsa0JBQWtCLENBbUIzQjtJQW5CRCxXQUFVLGtCQUFrQjtRQUV4Qiw2QkFBb0MsSUFBSTtZQUNwQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFdkMsUUFBUSxhQUFhLEVBQUU7Z0JBQ25CLEtBQUssSUFBSTtvQkFDTCxPQUFPLFNBQVMsQ0FBQztnQkFFckIsS0FBSyxJQUFJO29CQUNMLE9BQU8sU0FBUyxDQUFDO2dCQUVyQixLQUFLLElBQUk7b0JBQ0wsT0FBTyxVQUFVLENBQUM7YUFDekI7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBZmUsc0NBQW1CLHNCQWVsQyxDQUFBO0lBRUwsQ0FBQyxFQW5CUyxrQkFBa0IsS0FBbEIsa0JBQWtCLFFBbUIzQjtJQUVELE9BQVMsa0JBQWtCLENBQUMifQ==