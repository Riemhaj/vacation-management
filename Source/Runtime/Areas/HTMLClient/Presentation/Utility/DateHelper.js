/// <amd-module name="Utility/DateHelper" />
define("Utility/DateHelper", ["require", "exports", "Management/ResourceManager"], function (require, exports, ResourceManager) {
    "use strict";
    var DateHelper;
    (function (DateHelper) {
        function dateToTicks(date) {
            var ticks = (((date.getTime() * 10000) + 621355968000000000) - (date.getTimezoneOffset() * 600000000));
            return ticks;
        }
        DateHelper.dateToTicks = dateToTicks;
        function timeStampToString(dotnetTicks) {
            return ticksToString(dotnetTicks);
        }
        DateHelper.timeStampToString = timeStampToString;
        function ticksToString(dotnetTicks) {
            // convert the ticks into something javascript understands
            var ticksSinceEpoch = dotnetTicks - 621355968000000000;
            var millisecondsSinceEpoch = ticksSinceEpoch / 10000;
            // output the result in something the human understands
            var tickDate = new Date(millisecondsSinceEpoch);
            return moment(tickDate.toISOString(), 'YYYY-MM-DDTHH:mm:ss').format('YYYY-MM-DDTHH:mm:ss');
        }
        DateHelper.ticksToString = ticksToString;
        function parseDotNetDateString(dateString) {
            return parseDotNetDateTimeString(dateString);
        }
        DateHelper.parseDotNetDateString = parseDotNetDateString;
        function parseDotNetDateTimeString(dateString) {
            if (!dateString || !_.isString(dateString)) {
                return null;
            }
            var limitedString = dateString.substring(0, 19);
            return moment(limitedString + 'Z', 'YYYY-MM-DDTHH:mm:ss').toDate();
        }
        DateHelper.parseDotNetDateTimeString = parseDotNetDateTimeString;
        function isDotNetDatetimeString(dateString) {
            if (!dateString || !_.isString(dateString)) {
                return false;
            }
            var limitedString = dateString.substring(0, 19);
            return moment(limitedString, 'YYYY-MM-DDTHH:mm:ss', true).isValid();
        }
        DateHelper.isDotNetDatetimeString = isDotNetDatetimeString;
        function toString(date, showTime, showDate) {
            var language = ResourceManager.getLanguage();
            var format = '';
            if (showTime == null && showDate == null) {
                format = 'L LTS';
            }
            else {
                if (showDate == null || showDate === true) {
                    format += language.datePattern + ' ';
                }
                if (showTime == null || showTime === true) {
                    format += language.timePattern;
                }
                format = format.replace(/dd/g, 'DD');
                format = format.replace(/d/g, 'DD');
                format = format.replace(/yyyy/g, 'YYYY');
                format = format.replace(/tt/g, 'A');
            }
            if (!format) {
                return '';
            }
            var oldLocale = moment.locale();
            moment.locale(language.cultureInfoName);
            var result = moment(date).format(format);
            moment.locale(oldLocale);
            return result;
        }
        DateHelper.toString = toString;
        function toDotNetDateString(date) {
            if (date == null) {
                return null;
            }
            return [date.getFullYear(), padLeft(date.getMonth() + 1, 2, '0'), padLeft(date.getDate(), 2, '0')].join('-') + 'T00:00:00';
        }
        DateHelper.toDotNetDateString = toDotNetDateString;
        function toDotNetDateTimeString(date) {
            if (date == null) {
                return null;
            }
            return [date.getFullYear(), padLeft(date.getMonth() + 1, 2, '0'), padLeft(date.getDate(), 2, '0')].join('-') + 'T' + [padLeft(date.getHours(), 2, '0'), padLeft(date.getMinutes(), 2, '0'), padLeft(date.getSeconds(), 2, '0')].join(':');
        }
        DateHelper.toDotNetDateTimeString = toDotNetDateTimeString;
        function now() {
            return moment().toDate();
        }
        DateHelper.now = now;
        function center(dates) {
            var range = moment.range(dates);
            var center = moment(range.center()).toDate();
            return center;
        }
        DateHelper.center = center;
        function padLeft(number, digits, string) {
            return new Array(digits - String(number).length + 1).join(string || '0') + number;
        }
        DateHelper.padLeft = padLeft;
        function createDateFilter(fieldName, date, operator) {
            if (date == null) {
                return '';
            }
            var dimensions = ['year', 'month', 'day'];
            var field = 'this.' + fieldName;
            return createCompareDateByOperatorExpression(field, date, operator, dimensions);
        }
        DateHelper.createDateFilter = createDateFilter;
        function createTimeFilter(fieldName, date, operator) {
            if (date == null) {
                return '';
            }
            var dimensions = ['hour', 'minute', 'second'];
            var field = 'this.' + fieldName;
            return createCompareDateByOperatorExpression(field, date, operator, dimensions);
        }
        DateHelper.createTimeFilter = createTimeFilter;
        function createDateAndTimeFilter(fieldName, date, operator) {
            if (date == null) {
                return '';
            }
            var dimensions = ['year', 'month', 'day', 'hour', 'minute', 'second'];
            var field = 'this.' + fieldName;
            return createCompareDateByOperatorExpression(field, date, operator, dimensions);
        }
        DateHelper.createDateAndTimeFilter = createDateAndTimeFilter;
        function createCompareDateByOperatorExpression(field, date, operator, dimensions) {
            var expressionByOperator = {
                '<': createBeforeDateExpression(field, date, dimensions),
                '>': createAfterDateExpression(field, date, dimensions),
                '<=': createBeforeDateExpression(field, date, dimensions) + ' OR ' +
                    createDateEqualsExpression(field, date, dimensions),
                '>=': createAfterDateExpression(field, date, dimensions) + ' OR ' +
                    createDateEqualsExpression(field, date, dimensions),
                '=': createDateEqualsExpression(field, date, dimensions),
                '!=': 'not' + wrapExpression(createDateEqualsExpression(field, date, dimensions))
            };
            var expression = expressionByOperator[operator];
            var wrappedExpression = wrapExpression(expression);
            return wrappedExpression;
        }
        DateHelper.createCompareDateByOperatorExpression = createCompareDateByOperatorExpression;
        function createBeforeDateExpression(field, date, dimensions) {
            return createBeforeAfterDateExpression(field, date, dimensions, '<');
        }
        DateHelper.createBeforeDateExpression = createBeforeDateExpression;
        function createAfterDateExpression(field, date, dimensions) {
            return createBeforeAfterDateExpression(field, date, dimensions, '>');
        }
        DateHelper.createAfterDateExpression = createAfterDateExpression;
        function createDateEqualsExpression(field, date, dimensions) {
            var dimensionValues = splitDateByDimensions(date);
            var query = dimensions.map(function (dimension) {
                var value = dimensionValues[dimension];
                return createCompareFieldByDateDimensionExpression(dimension, field, '=', value);
            }).join(' AND ');
            return query;
        }
        DateHelper.createDateEqualsExpression = createDateEqualsExpression;
        function createBeforeAfterDateExpression(field, date, dimensions, operator) {
            var dimensionsCopy = dimensions.slice(0);
            var dimensionValues = splitDateByDimensions(date);
            var queries = [];
            while (dimensionsCopy.length > 1) {
                var smallestDimension = dimensionsCopy.pop();
                var smallestDimensionValue = dimensionValues[smallestDimension];
                var equalsQuery = createDateEqualsExpression(field, date, dimensionsCopy);
                var beforeQuery = createCompareFieldByDateDimensionExpression(smallestDimension, field, operator, smallestDimensionValue);
                queries.push(equalsQuery + ' AND ' + beforeQuery);
            }
            var lastDimension = dimensionsCopy[0];
            var lastDimensionValue = dimensionValues[lastDimension];
            queries.push(createCompareFieldByDateDimensionExpression(lastDimension, field, operator, lastDimensionValue));
            queries.reverse();
            var query = queries.join(' OR ');
            return query;
        }
        DateHelper.createBeforeAfterDateExpression = createBeforeAfterDateExpression;
        function createCompareFieldByDateDimensionExpression(dimension, field, operator, value) {
            return dimension + '(' + field + ') ' + operator + ' ' + value;
        }
        DateHelper.createCompareFieldByDateDimensionExpression = createCompareFieldByDateDimensionExpression;
        function wrapExpression(expression) {
            return '(' + expression + ')';
        }
        DateHelper.wrapExpression = wrapExpression;
        function splitDateByDimensions(date) {
            return {
                'year': year(date),
                'month': month(date),
                'day': day(date),
                'hour': hour(date),
                'minute': minute(date),
                'second': second(date)
            };
        }
        DateHelper.splitDateByDimensions = splitDateByDimensions;
        function year(date) {
            var year = date.getFullYear();
            return year > 99 ? year : (year + 2000);
        }
        DateHelper.year = year;
        function month(date) {
            return date.getMonth() + 1;
        }
        DateHelper.month = month;
        function day(date) {
            return date.getDate();
        }
        DateHelper.day = day;
        function hour(date) {
            return date.getHours();
        }
        DateHelper.hour = hour;
        function minute(date) {
            return date.getMinutes();
        }
        DateHelper.minute = minute;
        function second(date) {
            return date.getSeconds();
        }
        DateHelper.second = second;
    })(DateHelper || (DateHelper = {}));
    return DateHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0ZUhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkRhdGVIZWxwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNENBQTRDOzs7SUFNNUMsSUFBVSxVQUFVLENBeVBuQjtJQXpQRCxXQUFVLFVBQVU7UUFFaEIscUJBQTRCLElBQVU7WUFDbEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEtBQUssQ0FBQyxHQUFHLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3ZHLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFIZSxzQkFBVyxjQUcxQixDQUFBO1FBRUQsMkJBQWtDLFdBQWdCO1lBQzlDLE9BQU8sYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7UUFGZSw0QkFBaUIsb0JBRWhDLENBQUE7UUFFRCx1QkFBOEIsV0FBbUI7WUFDN0MsMERBQTBEO1lBQzFELElBQUksZUFBZSxHQUFHLFdBQVcsR0FBRyxrQkFBa0IsQ0FBQztZQUN2RCxJQUFJLHNCQUFzQixHQUFHLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFFckQsdURBQXVEO1lBQ3ZELElBQUksUUFBUSxHQUFHLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFaEQsT0FBTyxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDL0YsQ0FBQztRQVRlLHdCQUFhLGdCQVM1QixDQUFBO1FBRUQsK0JBQXNDLFVBQWtCO1lBQ3BELE9BQU8seUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakQsQ0FBQztRQUZlLGdDQUFxQix3QkFFcEMsQ0FBQTtRQUVELG1DQUEwQyxVQUFrQjtZQUN4RCxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDeEMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELElBQUksYUFBYSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2hELE9BQU8sTUFBTSxDQUFDLGFBQWEsR0FBRyxHQUFHLEVBQUUscUJBQXFCLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN2RSxDQUFDO1FBUGUsb0NBQXlCLDRCQU94QyxDQUFBO1FBRUQsZ0NBQXVDLFVBQWtCO1lBQ3JELElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUN4QyxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksYUFBYSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2hELE9BQU8sTUFBTSxDQUFDLGFBQWEsRUFBRSxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4RSxDQUFDO1FBUGUsaUNBQXNCLHlCQU9yQyxDQUFBO1FBRUQsa0JBQXlCLElBQVUsRUFBRSxRQUFrQixFQUFFLFFBQWtCO1lBQ3ZFLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM3QyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFFaEIsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ3RDLE1BQU0sR0FBRyxPQUFPLENBQUM7YUFDcEI7aUJBQ0k7Z0JBQ0QsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsS0FBSyxJQUFJLEVBQUU7b0JBQ3ZDLE1BQU0sSUFBSSxRQUFRLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztpQkFDeEM7Z0JBRUQsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsS0FBSyxJQUFJLEVBQUU7b0JBQ3ZDLE1BQU0sSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDO2lCQUNsQztnQkFFRCxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3JDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDcEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN6QyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDdkM7WUFFRCxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNULE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxJQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDaEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDeEMsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pCLE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUEvQmUsbUJBQVEsV0ErQnZCLENBQUE7UUFFRCw0QkFBbUMsSUFBVTtZQUN6QyxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ2QsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFdBQVcsQ0FBQztRQUMvSCxDQUFDO1FBTmUsNkJBQWtCLHFCQU1qQyxDQUFBO1FBRUQsZ0NBQXVDLElBQVU7WUFDN0MsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5TyxDQUFDO1FBTmUsaUNBQXNCLHlCQU1yQyxDQUFBO1FBRUQ7WUFDSSxPQUFPLE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzdCLENBQUM7UUFGZSxjQUFHLE1BRWxCLENBQUE7UUFFRCxnQkFBdUIsS0FBa0I7WUFDckMsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxJQUFJLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7WUFFN0MsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQztRQUxlLGlCQUFNLFNBS3JCLENBQUE7UUFFRCxpQkFBd0IsTUFBYyxFQUFFLE1BQWMsRUFBRSxNQUFjO1lBQ2xFLE9BQU8sSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7UUFDdEYsQ0FBQztRQUZlLGtCQUFPLFVBRXRCLENBQUE7UUFFRCwwQkFBaUMsU0FBaUIsRUFBRSxJQUFVLEVBQUUsUUFBZ0I7WUFDNUUsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxJQUFJLFVBQVUsR0FBRyxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDMUMsSUFBSSxLQUFLLEdBQUcsT0FBTyxHQUFHLFNBQVMsQ0FBQztZQUVoQyxPQUFPLHFDQUFxQyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7UUFUZSwyQkFBZ0IsbUJBUy9CLENBQUE7UUFFRCwwQkFBaUMsU0FBaUIsRUFBRSxJQUFVLEVBQUUsUUFBZ0I7WUFDNUUsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxJQUFJLFVBQVUsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDOUMsSUFBSSxLQUFLLEdBQUcsT0FBTyxHQUFHLFNBQVMsQ0FBQztZQUVoQyxPQUFPLHFDQUFxQyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7UUFUZSwyQkFBZ0IsbUJBUy9CLENBQUE7UUFFRCxpQ0FBd0MsU0FBaUIsRUFBRSxJQUFVLEVBQUUsUUFBZ0I7WUFDbkYsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxJQUFJLFVBQVUsR0FBRyxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDdEUsSUFBSSxLQUFLLEdBQUcsT0FBTyxHQUFHLFNBQVMsQ0FBQztZQUVoQyxPQUFPLHFDQUFxQyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7UUFUZSxrQ0FBdUIsMEJBU3RDLENBQUE7UUFFRCwrQ0FBc0QsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsVUFBVTtZQUNuRixJQUFJLG9CQUFvQixHQUFHO2dCQUN2QixHQUFHLEVBQUUsMEJBQTBCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUM7Z0JBRXhELEdBQUcsRUFBRSx5QkFBeUIsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQztnQkFFdkQsSUFBSSxFQUFFLDBCQUEwQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLEdBQUcsTUFBTTtvQkFDOUQsMEJBQTBCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUM7Z0JBRXZELElBQUksRUFBRSx5QkFBeUIsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxHQUFHLE1BQU07b0JBQzdELDBCQUEwQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDO2dCQUV2RCxHQUFHLEVBQUUsMEJBQTBCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxVQUFVLENBQUM7Z0JBRXhELElBQUksRUFBRSxLQUFLLEdBQUcsY0FBYyxDQUFDLDBCQUEwQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDcEYsQ0FBQztZQUVGLElBQUksVUFBVSxHQUFHLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2hELElBQUksaUJBQWlCLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ25ELE9BQU8saUJBQWlCLENBQUM7UUFDN0IsQ0FBQztRQXBCZSxnREFBcUMsd0NBb0JwRCxDQUFBO1FBRUQsb0NBQTJDLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVTtZQUM5RCxPQUFPLCtCQUErQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3pFLENBQUM7UUFGZSxxQ0FBMEIsNkJBRXpDLENBQUE7UUFFRCxtQ0FBMEMsS0FBSyxFQUFFLElBQUksRUFBRSxVQUFVO1lBQzdELE9BQU8sK0JBQStCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDekUsQ0FBQztRQUZlLG9DQUF5Qiw0QkFFeEMsQ0FBQTtRQUVELG9DQUEyQyxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVU7WUFDOUQsSUFBSSxlQUFlLEdBQUcscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEQsSUFBSSxLQUFLLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFNBQVM7Z0JBQ2pDLElBQUksS0FBSyxHQUFHLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDdkMsT0FBTywyQ0FBMkMsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNyRixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFakIsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQVRlLHFDQUEwQiw2QkFTekMsQ0FBQTtRQUVELHlDQUFnRCxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxRQUFRO1lBQzdFLElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxlQUFlLEdBQUcscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLE9BQU8sY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzlCLElBQUksaUJBQWlCLEdBQUcsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUM3QyxJQUFJLHNCQUFzQixHQUFHLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUVoRSxJQUFJLFdBQVcsR0FBRywwQkFBMEIsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRSxJQUFJLFdBQVcsR0FBRywyQ0FBMkMsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixDQUFDLENBQUM7Z0JBRTFILE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sR0FBRyxXQUFXLENBQUMsQ0FBQzthQUNyRDtZQUVELElBQUksYUFBYSxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLGtCQUFrQixHQUFHLGVBQWUsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4RCxPQUFPLENBQUMsSUFBSSxDQUFDLDJDQUEyQyxDQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixDQUFDLENBQUMsQ0FBQztZQUM5RyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFbEIsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqQyxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBdEJlLDBDQUErQixrQ0FzQjlDLENBQUE7UUFFRCxxREFBNEQsU0FBUyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsS0FBSztZQUN6RixPQUFPLFNBQVMsR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLElBQUksR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNuRSxDQUFDO1FBRmUsc0RBQTJDLDhDQUUxRCxDQUFBO1FBRUQsd0JBQStCLFVBQVU7WUFDckMsT0FBTyxHQUFHLEdBQUcsVUFBVSxHQUFHLEdBQUcsQ0FBQztRQUNsQyxDQUFDO1FBRmUseUJBQWMsaUJBRTdCLENBQUE7UUFFRCwrQkFBc0MsSUFBSTtZQUN0QyxPQUFPO2dCQUNILE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNsQixPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQztnQkFDcEIsS0FBSyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNsQixRQUFRLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDdEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUM7YUFDekIsQ0FBQztRQUNOLENBQUM7UUFUZSxnQ0FBcUIsd0JBU3BDLENBQUE7UUFFRCxjQUFxQixJQUFJO1lBQ3JCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QixPQUFPLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUhlLGVBQUksT0FHbkIsQ0FBQTtRQUVELGVBQXNCLElBQUk7WUFDdEIsT0FBTyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFGZSxnQkFBSyxRQUVwQixDQUFBO1FBRUQsYUFBb0IsSUFBSTtZQUNwQixPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxQixDQUFDO1FBRmUsY0FBRyxNQUVsQixDQUFBO1FBRUQsY0FBcUIsSUFBSTtZQUNyQixPQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQixDQUFDO1FBRmUsZUFBSSxPQUVuQixDQUFBO1FBRUQsZ0JBQXVCLElBQUk7WUFDdkIsT0FBTyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDN0IsQ0FBQztRQUZlLGlCQUFNLFNBRXJCLENBQUE7UUFFRCxnQkFBdUIsSUFBSTtZQUN2QixPQUFPLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUM3QixDQUFDO1FBRmUsaUJBQU0sU0FFckIsQ0FBQTtJQUNMLENBQUMsRUF6UFMsVUFBVSxLQUFWLFVBQVUsUUF5UG5CO0lBRUQsT0FBUyxVQUFVLENBQUMifQ==