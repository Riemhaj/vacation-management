/// <amd-module name="Utility/Loader" />
define("Utility/Loader", ["require", "exports"], function (require, exports) {
    "use strict";
    var Loader = /** @class */ (function () {
        function Loader() {
        }
        Loader.on = function (element, showLoader, showOverlay) {
            var loaderId = element.data('loader-id');
            var count = 0;
            if (loaderId == null) {
                loaderId = Loader.generateId();
            }
            count = Loader.increaseCounter(loaderId);
            Loader.resetTimer(loaderId);
            if (count === 0) {
                element.addClass(Loader.LOADER_CLASS);
                element.attr('data-loader-id', loaderId);
                if (element.css('position') != 'absolute') { // position:absolute darf nicht einfach auf relative geändert werden!
                    element.css('position', 'relative');
                }
                if (showOverlay !== false) {
                    Loader.overlay.clone().appendTo(element);
                }
                if (showLoader !== false) {
                    Loader.loader.clone().appendTo(element);
                }
                if (!element.hasClass('c-associationsingleautocompletebox')
                    && !element.hasClass('c-associationmultiautocompletebox')) {
                    Loader.addUserActionPrevention();
                }
            }
        };
        Loader.addUserActionPrevention = function () {
            Loader.disableTabbables();
            $(window).on('keypress', Loader.preventUserActions);
            $(window).on('click', Loader.preventUserActions);
        };
        Loader.removeUserActionPrevention = function () {
            Loader.restoreTabbables();
            $(window).off('keypress', Loader.preventUserActions);
            $(window).off('click', Loader.preventUserActions);
        };
        Loader.disableTabbables = function () {
            Loader.activeElement = document.activeElement;
            $(':tabbable').each(function (i, e) {
                var $element = $(e);
                Loader.$tabbables.push($element);
                $element['tabindexToRestore'] = $element.attr('tabindex');
                $element.attr('tabindex', -1);
            });
        };
        Loader.restoreTabbables = function () {
            Loader.$tabbables.forEach(function ($element) {
                if ($element['tabindexToRestore'] !== undefined) {
                    $element.attr('tabindex', $element['tabindexToRestore']);
                }
                else {
                    $element.removeAttr('tabindex');
                }
            });
            if (Loader.activeElement != null && $.isFunction(Loader.activeElement.focus))
                Loader.activeElement.focus();
            Loader.activeElement = null;
            Loader.$tabbables = [];
        };
        Loader.preventUserActions = function (e) {
            e.preventDefault();
            e.stopPropagation();
        };
        Loader.off = function (element) {
            var loaderId = element.data('loader-id');
            var count = Loader.decreaseCounter(loaderId);
            if (count === 0) {
                Loader.setTimer(loaderId, function () {
                    if (Loader.getCount(loaderId) <= 0) {
                        element.removeClass(Loader.LOADER_CLASS);
                        element.find('>.overlay').remove();
                        element.find('>.loader').remove();
                        if (!element.hasClass('c-associationsingleautocompletebox')
                            && !element.hasClass('c-associationmultiautocompletebox')) {
                            Loader.removeUserActionPrevention();
                        }
                    }
                });
            }
        };
        Loader.generateId = function () {
            var i = 0;
            while (i in Loader.counter) {
                i++;
            }
            return i;
        };
        Loader.setTimer = function (loaderId, callback) {
            Loader.hideTimers[loaderId] = window.setTimeout(function () {
                callback();
            }, 200);
        };
        Loader.resetTimer = function (loaderId) {
            if (Loader.hideTimers[loaderId]) {
                window.clearTimeout(Loader.hideTimers[loaderId]);
                Loader.hideTimers[loaderId] = null;
            }
        };
        Loader.increaseCounter = function (loaderId) {
            if (!(loaderId in Loader.counter)) {
                Loader.counter[loaderId] = -1;
            }
            return ++Loader.counter[loaderId];
        };
        Loader.decreaseCounter = function (loaderId) {
            if (loaderId === undefined) {
                return 0;
            }
            if (!(loaderId in Loader.counter)) {
                Loader.counter[loaderId] = 0;
            }
            if (Loader.counter[loaderId] <= -1) {
                Loader.counter[loaderId] = -1;
                return Loader.counter[loaderId];
            }
            return Loader.counter[loaderId]--;
        };
        Loader.getCount = function (id) {
            if (id === undefined) {
                return 0;
            }
            return Loader.counter[id];
        };
        Loader.getChildLoaders = function (element) {
            var parent = element.parent();
            var loaders = parent.find('.' + Loader.LOADER_CLASS).not(element);
            return loaders;
        };
        Loader.hasParentLoader = function (element) {
            var parent = element.parent();
            while (parent.length > 0) {
                if (parent.is('html')) {
                    return false;
                }
                if (parent.find('>.' + Loader.LOADER_CLASS).not(element).length > 0) {
                    return true;
                }
                parent = parent.parent();
            }
            return false;
        };
        Loader.while = function (element, callback) {
            if (callback == null && _.isFunction(element)) {
                callback = element;
                element = null;
            }
            if (callback != null) {
                Loader.show(element);
                callback();
                Loader.hide(element);
            }
        };
        Loader.show = function (element, showLoader, showOverlay) {
            element = element || $('body');
            if (!Loader.hasParentLoader(element)) {
                Loader.on(element, showLoader, showOverlay);
                Loader.getChildLoaders(element).each(function (index, node) {
                    Loader.hide($(node));
                });
            }
        };
        Loader.hide = function (element) {
            element = element || $('body');
            Loader.off(element);
        };
        Loader.removeAll = function () {
            window.setTimeout(function () {
                $('.overlay, .loader').remove();
            }, 100);
        };
        Loader.LOADER_CLASS = 'c-loader';
        Loader.counter = {};
        Loader.hideTimers = {};
        Loader.overlay = $('<div class="overlay"></div>');
        Loader.loader = $('<div class="loader"><div class="loader-inner"><div class="loader-animation"><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div><div class="loader-inner-bar"></div></div></div></div>');
        Loader.$tabbables = [];
        Loader.activeElement = null;
        return Loader;
    }());
    return Loader;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9hZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTG9hZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHdDQUF3Qzs7O0lBRXhDO1FBQUE7UUFzT0EsQ0FBQztRQTNOa0IsU0FBRSxHQUFqQixVQUFrQixPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVc7WUFDOUMsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7WUFFZCxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ2xCLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDbEM7WUFFRCxLQUFLLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTVCLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFDYixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDdEMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDekMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsRUFBRSxFQUFFLHFFQUFxRTtvQkFDOUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7aUJBQ3ZDO2dCQUVELElBQUksV0FBVyxLQUFLLEtBQUssRUFBRTtvQkFDdkIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzVDO2dCQUVELElBQUksVUFBVSxLQUFLLEtBQUssRUFBRTtvQkFDdEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzNDO2dCQUdELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLG9DQUFvQyxDQUFDO3VCQUNwRCxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsbUNBQW1DLENBQUMsRUFBRTtvQkFDM0QsTUFBTSxDQUFDLHVCQUF1QixFQUFFLENBQUM7aUJBQ3BDO2FBQ0o7UUFDTCxDQUFDO1FBRWMsOEJBQXVCLEdBQXRDO1lBQ0ksTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFFMUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDcEQsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDckQsQ0FBQztRQUVjLGlDQUEwQixHQUF6QztZQUNJLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBRTFCLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3JELENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3RELENBQUM7UUFFYyx1QkFBZ0IsR0FBL0I7WUFDSSxNQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7WUFDOUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNyQixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRXBCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUVqQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMxRCxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVjLHVCQUFnQixHQUEvQjtZQUNJLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBUTtnQkFDL0IsSUFBSSxRQUFRLENBQUMsbUJBQW1CLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQzdDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7aUJBQzVEO3FCQUNJO29CQUNELFFBQVEsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQ25DO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hFLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDakMsTUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDNUIsTUFBTSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDM0IsQ0FBQztRQUVjLHlCQUFrQixHQUFqQyxVQUFrQyxDQUFDO1lBQy9CLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixDQUFDLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsQ0FBQztRQUVjLFVBQUcsR0FBbEIsVUFBbUIsT0FBTztZQUN0QixJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3pDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFN0MsSUFBSSxLQUFLLEtBQUssQ0FBQyxFQUFFO2dCQUNiLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO29CQUN0QixJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUVoQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFFekMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFFbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsb0NBQW9DLENBQUM7K0JBQ3BELENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxtQ0FBbUMsQ0FBQyxFQUFFOzRCQUMzRCxNQUFNLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzt5QkFDdkM7cUJBQ0o7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUM7UUFFYyxpQkFBVSxHQUF6QjtZQUNJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVWLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hCLENBQUMsRUFBRSxDQUFDO2FBQ1A7WUFFRCxPQUFPLENBQUMsQ0FBQztRQUNiLENBQUM7UUFFYyxlQUFRLEdBQXZCLFVBQXdCLFFBQVEsRUFBRSxRQUFRO1lBQ3RDLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztnQkFDNUMsUUFBUSxFQUFFLENBQUM7WUFDZixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDO1FBRWMsaUJBQVUsR0FBekIsVUFBMEIsUUFBUTtZQUM5QixJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzthQUN0QztRQUNMLENBQUM7UUFFYyxzQkFBZSxHQUE5QixVQUErQixRQUFRO1lBQ25DLElBQUksQ0FBQyxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDakM7WUFFRCxPQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QyxDQUFDO1FBRWMsc0JBQWUsR0FBOUIsVUFBK0IsUUFBUTtZQUNuQyxJQUFJLFFBQVEsS0FBSyxTQUFTLEVBQUU7Z0JBQ3hCLE9BQU8sQ0FBQyxDQUFDO2FBQ1o7WUFFRCxJQUFJLENBQUMsQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUMvQixNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNoQztZQUVELElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDaEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ25DO1lBQ0QsT0FBTyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7UUFDdEMsQ0FBQztRQUVjLGVBQVEsR0FBdkIsVUFBd0IsRUFBRTtZQUN0QixJQUFJLEVBQUUsS0FBSyxTQUFTLEVBQUU7Z0JBQ2xCLE9BQU8sQ0FBQyxDQUFDO2FBQ1o7WUFFRCxPQUFPLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUVjLHNCQUFlLEdBQTlCLFVBQStCLE9BQU87WUFDbEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzlCLElBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFbEUsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQztRQUVjLHNCQUFlLEdBQTlCLFVBQStCLE9BQU87WUFDbEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBRTlCLE9BQU8sTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDbkIsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUVELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNqRSxPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFFRCxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzVCO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUVhLFlBQUssR0FBbkIsVUFBb0IsT0FBTyxFQUFFLFFBQVE7WUFDakMsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzNDLFFBQVEsR0FBRyxPQUFPLENBQUM7Z0JBQ25CLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDbEI7WUFFRCxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsRUFBRSxDQUFDO2dCQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDeEI7UUFDTCxDQUFDO1FBRWEsV0FBSSxHQUFsQixVQUFtQixPQUFPLEVBQUUsVUFBVyxFQUFFLFdBQVk7WUFDakQsT0FBTyxHQUFHLE9BQU8sSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFL0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ2xDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFFNUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFLLEVBQUUsSUFBSTtvQkFDN0MsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUM7UUFFYSxXQUFJLEdBQWxCLFVBQW1CLE9BQU87WUFDdEIsT0FBTyxHQUFHLE9BQU8sSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFL0IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBRWEsZ0JBQVMsR0FBdkI7WUFDSSxNQUFNLENBQUMsVUFBVSxDQUFDO2dCQUNkLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3BDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNaLENBQUM7UUFuT2MsbUJBQVksR0FBRyxVQUFVLENBQUM7UUFDMUIsY0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNiLGlCQUFVLEdBQUcsRUFBRSxDQUFDO1FBRWhCLGNBQU8sR0FBRyxDQUFDLENBQUMsNkJBQTZCLENBQUMsQ0FBQztRQUMzQyxhQUFNLEdBQUcsQ0FBQyxDQUFDLDRlQUE0ZSxDQUFDLENBQUM7UUFDemYsaUJBQVUsR0FBRyxFQUFFLENBQUM7UUFDaEIsb0JBQWEsR0FBRyxJQUFJLENBQUM7UUE2TnhDLGFBQUM7S0FBQSxBQXRPRCxJQXNPQztJQUVELE9BQVMsTUFBTSxDQUFDIn0=