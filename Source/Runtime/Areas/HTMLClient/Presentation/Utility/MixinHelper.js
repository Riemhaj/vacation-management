/// <amd-module name="Utility/MixinHelper" />
define("Utility/MixinHelper", ["require", "exports"], function (require, exports) {
    "use strict";
    var MixinHelper = /** @class */ (function () {
        function MixinHelper() {
        }
        // s. https://www.typescriptlang.org/docs/handbook/mixins.html
        MixinHelper.applyMixins = function (derivedCtor, baseCtors) {
            baseCtors.forEach(function (baseCtor) {
                Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
                    if (name != "constructor")
                        derivedCtor.prototype[name] = baseCtor.prototype[name];
                });
            });
        };
        return MixinHelper;
    }());
    return MixinHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWl4aW5IZWxwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJNaXhpbkhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw2Q0FBNkM7OztJQUU3QztRQUFBO1FBWUEsQ0FBQztRQVZHLDhEQUE4RDtRQUNoRCx1QkFBVyxHQUF6QixVQUEwQixXQUFnQixFQUFFLFNBQWdCO1lBQ3hELFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO2dCQUN0QixNQUFNLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ3ZELElBQUksSUFBSSxJQUFJLGFBQWE7d0JBQ3JCLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0QsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFTCxrQkFBQztJQUFELENBQUMsQUFaRCxJQVlDO0lBRUQsT0FBUyxXQUFXLENBQUMifQ==