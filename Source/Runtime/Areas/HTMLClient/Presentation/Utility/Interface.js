/// <amd-module name="Utility/Interface" />
define("Utility/Interface", ["require", "exports"], function (require, exports) {
    "use strict";
    var Interface = /** @class */ (function () {
        function Interface(name, methods) {
            this.name = name;
            this.methods = [];
            for (var i = 0, len = methods.length; i < len; i++) {
                this.methods.push(methods[i]);
            }
        }
        Interface.definitions = {
            IListViewContext: new Interface('IListViewContext', ['refresh', 'notifyListViewReady']),
            IDetailViewContext: new Interface('IDetailViewContext', ['refresh', 'callOperation', 'notifyUpdatedAssociation', 'notifyDetailViewReady', 'getCreatedTuple', 'save', 'cancel', 'apply'])
        };
        return Interface;
    }());
    function InterfaceViolation(violatingInterface, violatingMethod) {
        this.interface = violatingInterface;
        this.method = violatingMethod;
    }
    function findInterfaceViolation(object) {
        if (arguments.length < 2) {
            throw 'Interface check called with ' + arguments.length + ' arguments, but expected at least 2.';
        }
        for (var i = 1, len = arguments.length; i < len; i++) {
            var iface = arguments[i];
            if (iface.constructor !== Interface) {
                if (iface in Interface.definitions) {
                    iface = Interface.definitions[iface];
                }
                else {
                    throw 'Interface check expects arguments two and above to be defined in "Interface.definitions".';
                }
            }
            for (var j = 0, methodsLen = iface.methods.length; j < methodsLen; j++) {
                var method = iface.methods[j];
                if (!object[method]) {
                    return new InterfaceViolation(iface, method);
                }
            }
        }
        return null;
    }
    Interface.implements = function (object) {
        return findInterfaceViolation.apply(this, arguments) == null;
    };
    Interface.ensureImplements = function (object) {
        var violation = findInterfaceViolation.apply(this, arguments);
        if (violation != null) {
            throw 'Function Interface.ensureImplements: object does not implement the interface "' + violation.interface.name + '". Method "' + violation.method + '" was not found.';
        }
    };
    return Interface;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDJDQUEyQzs7O0lBRTNDO1FBT0ksbUJBQVksSUFBWSxFQUFFLE9BQXNCO1lBQzVDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBRWxCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pDO1FBQ0wsQ0FBQztRQUVhLHFCQUFXLEdBQUc7WUFDeEIsZ0JBQWdCLEVBQUUsSUFBSSxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUN2RixrQkFBa0IsRUFBRSxJQUFJLFNBQVMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxlQUFlLEVBQUUsMEJBQTBCLEVBQUUsdUJBQXVCLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUMzTCxDQUFBO1FBQ0wsZ0JBQUM7S0FBQSxBQXBCRCxJQW9CQztJQUVELDRCQUE0QixrQkFBa0IsRUFBRSxlQUFlO1FBQzNELElBQUksQ0FBQyxTQUFTLEdBQUcsa0JBQWtCLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUM7SUFDbEMsQ0FBQztJQUVELGdDQUFnQyxNQUFNO1FBQ2xDLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEIsTUFBTSw4QkFBOEIsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLHNDQUFzQyxDQUFDO1NBQ3BHO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsRCxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFekIsSUFBSSxLQUFLLENBQUMsV0FBVyxLQUFLLFNBQVMsRUFBRTtnQkFDakMsSUFBSSxLQUFLLElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRTtvQkFDaEMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3hDO3FCQUFNO29CQUNILE1BQU0sMkZBQTJGLENBQUM7aUJBQ3JHO2FBQ0o7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDcEUsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDakIsT0FBTyxJQUFJLGtCQUFrQixDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDaEQ7YUFDSjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELFNBQVMsQ0FBQyxVQUFVLEdBQUcsVUFBVSxNQUFNO1FBQ25DLE9BQU8sc0JBQXNCLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7SUFDakUsQ0FBQyxDQUFBO0lBRUQsU0FBUyxDQUFDLGdCQUFnQixHQUFHLFVBQVUsTUFBTTtRQUN6QyxJQUFJLFNBQVMsR0FBRyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzlELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtZQUNuQixNQUFNLGdGQUFnRixHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDO1NBQzdLO0lBQ0wsQ0FBQyxDQUFBO0lBRUQsT0FBUyxTQUFTLENBQUMifQ==