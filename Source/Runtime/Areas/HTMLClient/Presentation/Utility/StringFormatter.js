/// <amd-module name="Utility/StringFormatter" />
define("Utility/StringFormatter", ["require", "exports"], function (require, exports) {
    "use strict";
    var StringFormatter;
    (function (StringFormatter) {
        var htmlEscapeMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#47;',
            '`': '&#96;',
        };
        /**
        * Maskiert die für HTML relevanten Sonderzeichen in einem String. Das &-Zeichen kann ignoriert werden, da es ebenfalls in der kodierten
        * Darstellung auftritt (relevant wenn ein bereits maskierter String erneut maskiert wird).
        */
        function escapeHtml(originalText, ignoreAmpersand) {
            var _this = this;
            var regexText = '', prop, regex;
            for (prop in this.htmlEscapeMap) {
                if (this.htmlEscapeMap.hasOwnProperty(prop)) {
                    if (ignoreAmpersand && prop === '&') {
                        continue;
                    }
                    regexText += prop;
                }
            }
            regexText = '[' + regexText + ']';
            regex = new RegExp(regexText, 'g');
            return originalText.replace(regex, function (s) {
                return _this.htmlEscapeMap[s];
            });
        }
        StringFormatter.escapeHtml = escapeHtml;
        /*
         * Ersetzt Leerzeichen alternierend durch Leerzeichen und maskierte Leerzeichen. Dadurch ist es möglich in HTML mehrere Leerzeichen hinterienander
         * zu rendern, wobei Zeilenumbrüche trotzdem weiterhin automatisch an den echten Leerzeichen erfolgen können.
         */
        function escapeWhitespacesAlternating(originalText) {
            return originalText.replace(/  /g, ' \u00a0');
        }
        StringFormatter.escapeWhitespacesAlternating = escapeWhitespacesAlternating;
        /*
         * Ersetzt Zeilenumbrüche durch das entsprechende HTML-Tag.
         */
        function replaceNewLineWithHtmlTag(orignalText) {
            return orignalText.replace(/(?:\r\n|\r|\n)/g, '<br />');
        }
        StringFormatter.replaceNewLineWithHtmlTag = replaceNewLineWithHtmlTag;
    })(StringFormatter || (StringFormatter = {}));
    return StringFormatter;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RyaW5nRm9ybWF0dGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiU3RyaW5nRm9ybWF0dGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGlEQUFpRDs7O0lBRWpELElBQVUsZUFBZSxDQXFEeEI7SUFyREQsV0FBVSxlQUFlO1FBQ3JCLElBQUksYUFBYSxHQUFHO1lBQ2hCLEdBQUcsRUFBRSxPQUFPO1lBQ1osR0FBRyxFQUFFLE1BQU07WUFDWCxHQUFHLEVBQUUsTUFBTTtZQUNYLEdBQUcsRUFBRSxRQUFRO1lBQ2IsR0FBRyxFQUFFLE9BQU87WUFDWixHQUFHLEVBQUUsT0FBTztZQUNaLEdBQUcsRUFBRSxPQUFPO1NBQ2YsQ0FBQTtRQUVEOzs7VUFHRTtRQUNGLG9CQUEyQixZQUFZLEVBQUUsZUFBZTtZQUF4RCxpQkFxQkM7WUFuQkcsSUFBSSxTQUFTLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUM7WUFFaEMsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFFekMsSUFBSSxlQUFlLElBQUksSUFBSSxLQUFLLEdBQUcsRUFBRTt3QkFDakMsU0FBUztxQkFDWjtvQkFFRCxTQUFTLElBQUksSUFBSSxDQUFDO2lCQUNyQjthQUNKO1lBRUQsU0FBUyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQ2xDLEtBQUssR0FBRyxJQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFFbkMsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxVQUFDLENBQUM7Z0JBQ2pDLE9BQU8sS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFyQmUsMEJBQVUsYUFxQnpCLENBQUE7UUFFRDs7O1dBR0c7UUFDSCxzQ0FBOEMsWUFBWTtZQUN0RCxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELENBQUM7UUFGZSw0Q0FBNEIsK0JBRTNDLENBQUE7UUFFRDs7V0FFRztRQUNILG1DQUEyQyxXQUFXO1lBQ2xELE9BQU8sV0FBVyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM1RCxDQUFDO1FBRmUseUNBQXlCLDRCQUV4QyxDQUFBO0lBRUwsQ0FBQyxFQXJEUyxlQUFlLEtBQWYsZUFBZSxRQXFEeEI7SUFFRCxPQUFTLGVBQWUsQ0FBQyJ9