/// <amd-module name="Utility/ChangeHandler" />
define("Utility/ChangeHandler", ["require", "exports"], function (require, exports) {
    "use strict";
    var ChangeHandler = /** @class */ (function () {
        function ChangeHandler() {
        }
        ChangeHandler.getObjProperties = function (obj) {
            var objProperties = [];
            var val = ko.unwrap(obj);
            if (val !== null && typeof val === 'object') {
                for (var i in val) {
                    if (val.hasOwnProperty(i)) {
                        objProperties.push({ 'name': i, 'value': val[i] });
                    }
                }
            }
            return objProperties;
        };
        ChangeHandler.traverseObservables = function (obj, action) {
            ko.utils.arrayForEach(ChangeHandler.getObjProperties(obj), function (observable) {
                if (observable && observable.value && !observable.value.nodeType && ko.isObservable(observable.value)) {
                    action(observable);
                }
            });
        };
        ChangeHandler.applyChangeTrackingToObservable = function (observable) {
            if (observable && !observable.nodeType && !observable.refresh && ko.isObservable(observable)) {
                if (!observable.isDirty) {
                    observable.extend({ trackChange: true });
                }
            }
        };
        ChangeHandler.applyChangeTracking = function (obj) {
            var properties = ChangeHandler.getObjProperties(obj);
            ko.utils.arrayForEach(properties, function (property) {
                ChangeHandler.applyChangeTrackingToObservable(property.value);
            });
        };
        ChangeHandler.getChangesFromModel = function (obj) {
            var changes = {};
            var properties = ChangeHandler.getObjProperties(obj);
            ko.utils.arrayForEach(properties, function (property) {
                if (property.value != null && property.value.isDirty != null && property.value.isDirty()) {
                    changes[property.name] = property.value.getChanges();
                }
            });
            return changes;
        };
        ChangeHandler.commitChangesFromModel = function (obj, props) {
            var changes = ChangeHandler.getChangesFromModel(obj);
            for (var property in changes) {
                if (props == null || props.indexOf(property) > -1) {
                    obj[property].commit();
                }
            }
        };
        ChangeHandler.revertChangesFromModel = function (obj, props) {
            var changes = ChangeHandler.getChangesFromModel(obj);
            for (var property in changes) {
                if (props == null || props.indexOf(property) > -1) {
                    obj[property].revert();
                }
            }
        };
        return ChangeHandler;
    }());
    return ChangeHandler;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2hhbmdlSGFuZGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNoYW5nZUhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0NBQStDOzs7SUFFL0M7UUFBQTtRQXlFQSxDQUFDO1FBdkVrQiw4QkFBZ0IsR0FBL0IsVUFBZ0MsR0FBRztZQUMvQixJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDdkIsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUV6QixJQUFJLEdBQUcsS0FBSyxJQUFJLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUFFO2dCQUN6QyxLQUFLLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRTtvQkFDZixJQUFJLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3ZCLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3FCQUN0RDtpQkFDSjthQUNKO1lBRUQsT0FBTyxhQUFhLENBQUM7UUFDekIsQ0FBQztRQUVhLGlDQUFtQixHQUFqQyxVQUFrQyxHQUFHLEVBQUUsTUFBTTtZQUN6QyxFQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBQyxVQUFVO2dCQUNsRSxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ25HLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDdEI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw2Q0FBK0IsR0FBN0MsVUFBOEMsVUFBVTtZQUNwRCxJQUFJLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQzFGLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFO29CQUNyQixVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQzVDO2FBQ0o7UUFDTCxDQUFDO1FBRWEsaUNBQW1CLEdBQWpDLFVBQWtDLEdBQUc7WUFDakMsSUFBSSxVQUFVLEdBQUcsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JELEVBQUUsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxVQUFDLFFBQVE7Z0JBQ3ZDLGFBQWEsQ0FBQywrQkFBK0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsaUNBQW1CLEdBQWpDLFVBQWtDLEdBQUc7WUFDakMsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLElBQUksVUFBVSxHQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVyRCxFQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsVUFBQyxRQUFRO2dCQUN2QyxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFFO29CQUN0RixPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7aUJBQ3hEO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDO1FBRWEsb0NBQXNCLEdBQXBDLFVBQXFDLEdBQUcsRUFBRSxLQUFLO1lBQzNDLElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVyRCxLQUFLLElBQUksUUFBUSxJQUFJLE9BQU8sRUFBRTtnQkFDMUIsSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQy9DLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQkFDMUI7YUFDSjtRQUNMLENBQUM7UUFFYSxvQ0FBc0IsR0FBcEMsVUFBcUMsR0FBRyxFQUFFLEtBQUs7WUFDM0MsSUFBSSxPQUFPLEdBQUcsYUFBYSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXJELEtBQUssSUFBSSxRQUFRLElBQUksT0FBTyxFQUFFO2dCQUMxQixJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDL0MsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO2lCQUMxQjthQUNKO1FBQ0wsQ0FBQztRQUVMLG9CQUFDO0lBQUQsQ0FBQyxBQXpFRCxJQXlFQztJQUVELE9BQVMsYUFBYSxDQUFDIn0=