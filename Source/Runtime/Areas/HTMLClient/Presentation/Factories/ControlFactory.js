/// <amd-module name="Factories/ControlFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ControlFactory", ["require", "exports", "Management/ComponentManager", "Factories/Factory", "Factories/ControlFactoryTuple", "Management/ResourceManager", "Utility/TypeConverter"], function (require, exports, ComponentManager, Factory, ControlFactoryTuple, ResourceManager, TypeConverter) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller ControlFactories
    */
    var ControlFactory = /** @class */ (function (_super) {
        __extends(ControlFactory, _super);
        function ControlFactory(params) {
            var _this = _super.call(this, params) || this;
            _this.LinkViewModel = ComponentManager.getViewModel('LinkViewModel');
            _this.LabelViewModel = ComponentManager.getViewModel('LabelViewModel');
            _this.ImageViewModel = ComponentManager.getViewModel('ImageViewModel');
            _this.Link = ComponentManager.getControl('Link');
            _this.Label = ComponentManager.getControl('Label');
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ControlFactory);
            _this.viewModel = null;
            _this.control = null; // beinhaltet z.B. Instanz von TextBox(-Control)
            _this.isControlFactory = true;
            _this.detailViewItem = _this.paramOrDefault(params.detailViewItem, null);
            _this.context = _this.paramOrDefault(params.context, null);
            _this.layout = _this.paramOrDefault(params.layout, 'OneColumnLayout');
            _this.isDisplayed = _this.observableOrDefault(params.isDisplayed, true);
            _this.neighborOf = _this.paramOrDefault(params.neighborOf, null);
            _this.isFocusable = _this.observableOrDefault(params.isFocusable, true);
            _this.labelPosition = _this.paramOrDefault(params.labelPosition, 'Left');
            _this.colspan = _this.paramOrDefault(params.colspan, 'One');
            if (params.detailViewItem.hasOwnProperty("isVisible"))
                _this.isVisible = _this.observableOrDefault(params.detailViewItem.isVisible, true).extend({ notify: 'always' });
            else
                _this.isVisible = _this.observableOrDefault(params.isVisible, true).extend({ notify: 'always' });
            _this.isSummaryViewMode = _this.paramOrDefault(params.isSummaryViewMode, false);
            _this.isRequired = _this.observableOrDefault(params.detailViewItem.required, false);
            _this.isBusy = _this.observableOrDefault(params.isBusy, false);
            _this.invalidationText = _this.observableOrDefault(params.invalidationText, '');
            _this.isValid = _this.observableOrDefault(params.isValid, true);
            var _isEnabled = null;
            if (params.detailViewItem.hasOwnProperty("isEnabled"))
                _isEnabled = _this.observableOrDefault(ko.unwrap(params.detailViewItem.isEnabled), true);
            else
                _isEnabled = _this.observableOrDefault(ko.unwrap(params.isEnabled), true);
            _this.isEnabled = _this.definePureComputed({
                read: function () {
                    var ret = _isEnabled();
                    if (_this.context != null && _this.context.hasOwnProperty('isLocked')) {
                        ret = !_this.context.isLocked() && ret;
                    }
                    return !params.detailViewItem.guiReadOnly && ret;
                },
                write: function (value) {
                    _isEnabled(value);
                }
            });
            _this.isDisposable = true;
            _this.undisposeProperties.push('context');
            return _this;
        }
        /*
        * Registriert eine Standardvalidierung
        */
        ControlFactory.prototype.registerDefaultValidation = function (props) {
            var _this = this;
            if (this.context.onValidationError != null) {
                this.context.onValidationError.push(function (fieldName, message, valid) {
                    // Wenn das Control valide ist, wird dieses durch einen Reset im DetailViewContext ausgelöst
                    // In diesem Fall muss keine Namensprüfung durchgeführt werden
                    if (valid || props.indexOf(fieldName) !== -1) {
                        _this.isValid(valid);
                        _this.invalidationText(message);
                    }
                });
                this.context.genericObject.onChange(function () {
                    _this.isValid(true);
                    _this.invalidationText('');
                }, props, this);
            }
        };
        /*
         * Entfernt des Assoziationsnamen aus einem Order-Statement für die Verwendung in Standard-Iteratoren
         */
        ControlFactory.prototype.convertOrderStatementWithoutAssociation = function (orderBy) {
            if (orderBy == null) {
                return null;
            }
            return orderBy.replace(new RegExp('this.' + this.detailViewItem.listView.relationName + '.', 'g'), 'this.');
        };
        /*
         * Registriert eine Validierung auf Basis eines Observables
         */
        ControlFactory.prototype.registerObservableValidation = function (observable) {
            var _this = this;
            if (this.context.onValidationError != null) {
                this.context.onValidationError.push(function (fieldName, message, valid) {
                    if (valid || (_this.detailViewItem && fieldName === _this.detailViewItem.name)) {
                        _this.isValid(valid);
                        _this.invalidationText(message);
                    }
                });
                this.defineSubscription(observable, function () {
                    _this.isValid(true);
                    _this.invalidationText('');
                });
            }
        };
        /*
         * Registriert eine Typvalidierung auf dem Wert eines Observables, aus welchem normalerweise der Wert des Properties entsteht
         */
        ControlFactory.prototype.registerTypePreValidation = function (property, observable, validator) {
            var _this = this;
            var execValidator = function (value) {
                var isValid = validator(value, _this.detailViewItem.dotNetTypeName);
                if (isValid === true) {
                    _this.context.genericObject.deliverNotificationFor(property);
                    _this.isValid(true);
                    _this.invalidationText('');
                }
                else {
                    _this.context.genericObject.suppressNotificationFor(property);
                    _this.isValid(false);
                    _this.invalidationText(ResourceManager.get('PROPERTY_CONSTRAINT'));
                }
                return isValid;
            };
            this.defineSubscription(observable, function (value) {
                execValidator(value);
            });
            if (this.context._typeValidations != null) {
                this.context._typeValidations.push(function () {
                    var val = _this.context.genericObject.getValue(property);
                    var isValid = execValidator(val);
                    if (isValid === true) {
                        return null;
                    }
                    else {
                        return { property: property, ergName: ResourceManager.loadResource(_this.detailViewItem.ergName, false) };
                    }
                });
            }
        };
        /*
         * Registriert eine Typvalidierung auf dem Wert eines Properties
         */
        ControlFactory.prototype.registerTypeValidation = function (property, validator) {
            var _this = this;
            var execValidator = function (value) {
                var isValid = validator(value, _this.detailViewItem.dotNetTypeName);
                if (isValid === true) {
                    _this.context.genericObject.deliverNotificationFor(property);
                    _this.isValid(true);
                    _this.invalidationText('');
                }
                else {
                    _this.context.genericObject.suppressNotificationFor(property);
                    _this.isValid(false);
                    _this.invalidationText(ResourceManager.get('PROPERTY_CONSTRAINT'));
                }
                return isValid;
            };
            this.context.genericObject.onChange(function (prop, value) {
                execValidator(value);
            }, [property], this, true);
            if (this.context._typeValidations != null) {
                this.context._typeValidations.push(function () {
                    var val = _this.context.genericObject.getValue(property);
                    var isValid = execValidator(val);
                    if (isValid === true) {
                        return null;
                    }
                    else {
                        return { property: property, ergName: ResourceManager.loadResource(_this.detailViewItem.ergName, false) };
                    }
                });
            }
        };
        /*
         * Registriert eine Custom-Validierung
         */
        ControlFactory.prototype.registerCustomValidation = function (props, observable, func) {
            var _this = this;
            this.defineSubscription(observable, function (value) {
                var isValid = func(value);
                if (isValid !== true) {
                    _this.isValid(false);
                    _this.invalidationText(isValid || ResourceManager.get('PROPERTY_CONSTRAINT'));
                    _this.context.genericObject.commit(props);
                }
            });
            if ($.isFunction(this.context.forceCustomValidation)) {
                this.defineSubscription(this.context.forceCustomValidation, function () {
                    var isValid = func(observable());
                    if (isValid !== true) {
                        _this.isValid(false);
                        _this.invalidationText(isValid || ResourceManager.get('PROPERTY_CONSTRAINT'));
                        _this.context.genericObject.commit(props);
                    }
                });
            }
        };
        /*
         * Fügt ein Icon zur Wiederherstellung des Derived-Wertes hinzu, falls notwendig
         */
        ControlFactory.prototype.addOverwritableToControl = function (control, props, tooltipErgName) {
            var _this = this;
            if (this.detailViewItem.isDerivedOverwritable === true) {
                var locator = '[' + props.join('/') + '].revert';
                var viewModel = new this.LinkViewModel({
                    isEnabled: this.isEnabled,
                    parentLocator: this.locator,
                    locator: locator,
                    tooltip: new this.LabelViewModel({
                        ergName: tooltipErgName || 'REVERT_BUTTON_TOOLTIP'
                    }),
                    image: new this.ImageViewModel({
                        parentLocator: this.locator,
                        locator: locator + '.image',
                        source: 'revert_dark.png',
                        alternativeText: new this.LabelViewModel({
                            ergName: tooltipErgName || 'REVERT_BUTTON_TOOLTIP'
                        })
                    }),
                    isToolBarLink: true,
                    onClick: function () {
                        for (var i = 0; i < props.length; i++) {
                            _this.context.genericObject.getObservable(props[i])(null);
                        }
                        _this.context.updateProperties();
                    }
                });
                var link = new this.Link(viewModel);
                var tuple = link.getTuple();
                tuple.control.appendTo(control);
            }
        };
        /*
         * Standardimplementierung zum Auslesen der Default-Operators
         */
        ControlFactory.prototype.getAvailableOperators = function () {
            var result = [];
            var typeName = this.detailViewItem.dotNetTypeName;
            var isString = TypeConverter.typeIsChar(typeName) || TypeConverter.typeIsString(typeName);
            var isNumber = TypeConverter.typeIsNumber(typeName);
            var isDateTime = TypeConverter.typeIsDateTime(typeName);
            var isBoolean = TypeConverter.typeIsBool(typeName);
            var isByte = TypeConverter.typeIsByte(typeName);
            result.push({ ergName: 'EQUALS', value: '=' });
            if (!isBoolean)
                result.push({ ergName: 'NOT_EQUALS', value: '!=' });
            if (isString)
                result.push({ ergName: 'LIKE', value: 'LIKE' });
            if (isNumber || isDateTime || isByte)
                result.push({ ergName: 'GREATER_EQUALS', value: '>=' });
            if (isDateTime)
                result.push({ ergName: 'GREATER', value: '>' });
            if (isNumber || isDateTime || isByte)
                result.push({ ergName: 'LESS_EQUALS', value: '<=' });
            if (isDateTime)
                result.push({ ergName: 'LESS', value: '<' });
            return result;
        };
        /*
        * Standardimplementierung zum Auslesen der Filter-Expression
        */
        ControlFactory.prototype.getFilterExpression = function (operator) {
            return '';
        };
        ControlFactory.prototype.getValueForFilter = function (operator) {
            return '';
        };
        ControlFactory.prototype.getFilterInfo = function () {
            return {
                field: '',
                value: ''
            };
        };
        /*
        * Erzeugt das FactoryTuple und die Factory
        */
        ControlFactory.prototype.getTuple = function () {
            this.tuple = new ControlFactoryTuple(this);
            return this.tuple;
        };
        ControlFactory.prototype.getControl = function () {
            throw new Error('Die Funktion "getControl" wurde nicht implementiert');
        };
        /*
         * Standardimplementierung, welche bei Bedarf überschrieben werden kann
         */
        ControlFactory.prototype.getLabel = function () {
            var _this = this;
            var label = null;
            if (this.detailViewItem != null) {
                this.labelViewModel = new this.LabelViewModel({
                    ergName: this.detailViewItem.ergName,
                    parentLocator: this.locator,
                    locator: this.detailViewItem.name + '.default-label',
                    postfix: new this.LabelViewModel({
                        caption: this.defineComputed(function () {
                            if (_this.isRequired() === true && _this.isSummaryViewMode === false) {
                                return ' *';
                            }
                            return '';
                        })
                    })
                });
                if (this.control) {
                    var labelElement = $('<label>');
                    this.control.addLabelReference(labelElement);
                    this.label = new this.Label(this.labelViewModel, labelElement);
                }
                else
                    this.label = new this.Label(this.labelViewModel);
                var tuple = this.label.getTuple();
                label = tuple.control;
            }
            else {
                label = $();
            }
            return label;
        };
        /*
        * Disposed die ControlFactory
        */
        ControlFactory.prototype.dispose = function () {
            if (this.tuple != null && this.tuple.isDisposed === false) {
                this.tuple.dispose();
            }
            this.tuple = null;
            _super.prototype.dispose.call(this);
        };
        return ControlFactory;
    }(Factory));
    return ControlFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbEZhY3RvcnkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDb250cm9sRmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxrREFBa0Q7Ozs7Ozs7Ozs7Ozs7SUFvQmxEOztNQUVFO0lBQ0Y7UUFBNkIsa0NBQU87UUFpSGhDLHdCQUFZLE1BQU07WUFBbEIsWUFDSSxrQkFBTSxNQUFNLENBQUMsU0FrRGhCO1lBbEtPLG1CQUFhLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUEyQixlQUFlLENBQUMsQ0FBQztZQUN6RixvQkFBYyxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBNEIsZ0JBQWdCLENBQUMsQ0FBQztZQUM1RixvQkFBYyxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBNEIsZ0JBQWdCLENBQUMsQ0FBQztZQUM1RixVQUFJLEdBQUcsZ0JBQWdCLENBQUMsVUFBVSxDQUFrQixNQUFNLENBQUMsQ0FBQztZQUM1RCxXQUFLLEdBQUcsZ0JBQWdCLENBQUMsVUFBVSxDQUFtQixPQUFPLENBQUMsQ0FBQztZQThHbkUscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUU5QyxLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDLGdEQUFnRDtZQUNyRSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQzdCLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3ZFLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pELEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDcEUsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN0RSxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvRCxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3RFLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZFLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRTFELElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO2dCQUNqRCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQzs7Z0JBRTlHLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7WUFFbkcsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2xGLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDN0QsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUU5RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUM7Z0JBQ2pELFVBQVUsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDOztnQkFFeEYsVUFBVSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUU3RSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckMsSUFBSSxFQUFFO29CQUNGLElBQUksR0FBRyxHQUFHLFVBQVUsRUFBRSxDQUFDO29CQUV2QixJQUFJLEtBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxJQUFJLEtBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFO3dCQUNqRSxHQUFHLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLEdBQUcsQ0FBQztxQkFDekM7b0JBQ0QsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxJQUFJLEdBQUcsQ0FBQztnQkFDckQsQ0FBQztnQkFDRCxLQUFLLEVBQUUsVUFBQyxLQUFjO29CQUNsQixVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLENBQUM7YUFDSixDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDOztRQUM3QyxDQUFDO1FBRUQ7O1VBRUU7UUFDSyxrREFBeUIsR0FBaEMsVUFBaUMsS0FBb0I7WUFBckQsaUJBZ0JDO1lBZkcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixJQUFJLElBQUksRUFBRTtnQkFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFpQixFQUFFLE9BQWUsRUFBRSxLQUFjO29CQUNuRiw0RkFBNEY7b0JBQzVGLDhEQUE4RDtvQkFDOUQsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDMUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDcEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNsQztnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQ2hDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNuQjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLGdFQUF1QyxHQUE5QyxVQUErQyxPQUFlO1lBQzFELElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDakIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsWUFBWSxHQUFHLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNoSCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxxREFBNEIsR0FBbkMsVUFBb0MsVUFBbUM7WUFBdkUsaUJBY0M7WUFiRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLElBQUksSUFBSSxFQUFFO2dCQUN4QyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFDLFNBQWlCLEVBQUUsT0FBZSxFQUFFLEtBQWM7b0JBQ25GLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsSUFBSSxTQUFTLEtBQUssS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDMUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDcEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNsQztnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFO29CQUNoQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQixLQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzlCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxrREFBeUIsR0FBaEMsVUFBaUMsUUFBZ0IsRUFBRSxVQUFtQyxFQUFFLFNBQW1CO1lBQTNHLGlCQW1DQztZQWxDRyxJQUFJLGFBQWEsR0FBRyxVQUFDLEtBQUs7Z0JBRXRCLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFFbkUsSUFBSSxPQUFPLEtBQUssSUFBSSxFQUFFO29CQUNsQixLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFFNUQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUM3QjtxQkFBTTtvQkFDSCxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFFN0QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO2lCQUNyRTtnQkFDRCxPQUFPLE9BQU8sQ0FBQztZQUNuQixDQUFDLENBQUM7WUFFRixJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLFVBQUMsS0FBVTtnQkFDM0MsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7b0JBQy9CLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxPQUFPLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUVqQyxJQUFJLE9BQU8sS0FBSyxJQUFJLEVBQUU7d0JBQ2xCLE9BQU8sSUFBSSxDQUFDO3FCQUNmO3lCQUFNO3dCQUNILE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxlQUFlLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQUM7cUJBQzVHO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSwrQ0FBc0IsR0FBN0IsVUFBOEIsUUFBZ0IsRUFBRSxTQUFtQjtZQUFuRSxpQkFrQ0M7WUFqQ0csSUFBSSxhQUFhLEdBQUcsVUFBQyxLQUFLO2dCQUN0QixJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBRW5FLElBQUksT0FBTyxLQUFLLElBQUksRUFBRTtvQkFDbEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBRTVELEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDN0I7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBRTdELEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3BCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztpQkFDckU7Z0JBQ0QsT0FBTyxPQUFPLENBQUM7WUFDbkIsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFVBQUMsSUFBWSxFQUFFLEtBQVU7Z0JBQ3pELGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QixDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7b0JBQy9CLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxPQUFPLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUVqQyxJQUFJLE9BQU8sS0FBSyxJQUFJLEVBQUU7d0JBQ2xCLE9BQU8sSUFBSSxDQUFDO3FCQUNmO3lCQUFNO3dCQUNILE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxlQUFlLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQUM7cUJBQzVHO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxpREFBd0IsR0FBL0IsVUFBZ0MsS0FBb0IsRUFBRSxVQUFtQyxFQUFFLElBQWM7WUFBekcsaUJBd0JDO1lBdkJHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsVUFBQyxLQUFVO2dCQUMzQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRTFCLElBQUksT0FBTyxLQUFLLElBQUksRUFBRTtvQkFDbEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sSUFBSSxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztvQkFFN0UsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM1QztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQkFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUU7b0JBQ3hELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO29CQUVqQyxJQUFJLE9BQU8sS0FBSyxJQUFJLEVBQUU7d0JBQ2xCLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3BCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLElBQUksZUFBZSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7d0JBRTdFLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDNUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLGlEQUF3QixHQUEvQixVQUFnQyxPQUE0QixFQUFFLEtBQW9CLEVBQUUsY0FBc0I7WUFBMUcsaUJBaUNDO1lBaENHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsS0FBSyxJQUFJLEVBQUU7Z0JBQ3BELElBQUksT0FBTyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQztnQkFDakQsSUFBSSxTQUFTLEdBQUcsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDO29CQUNuQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0JBQ3pCLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTztvQkFDM0IsT0FBTyxFQUFFLE9BQU87b0JBQ2hCLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUM7d0JBQzdCLE9BQU8sRUFBRSxjQUFjLElBQUksdUJBQXVCO3FCQUNyRCxDQUFDO29CQUNGLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUM7d0JBQzNCLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTzt3QkFDM0IsT0FBTyxFQUFFLE9BQU8sR0FBRyxRQUFRO3dCQUMzQixNQUFNLEVBQUUsaUJBQWlCO3dCQUN6QixlQUFlLEVBQUUsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDOzRCQUNyQyxPQUFPLEVBQUUsY0FBYyxJQUFJLHVCQUF1Qjt5QkFDckQsQ0FBQztxQkFDTCxDQUFDO29CQUNGLGFBQWEsRUFBRSxJQUFJO29CQUNuQixPQUFPLEVBQUU7d0JBQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ25DLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDNUQ7d0JBRUQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNwQyxDQUFDO2lCQUNKLENBQUMsQ0FBQztnQkFFSCxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFFNUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDbkM7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSw4Q0FBcUIsR0FBNUI7WUFDSSxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7WUFDcEQsSUFBTSxRQUFRLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxhQUFhLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVGLElBQU0sUUFBUSxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEQsSUFBTSxVQUFVLEdBQUcsYUFBYSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxRCxJQUFNLFNBQVMsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3JELElBQU0sTUFBTSxHQUFHLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFbEQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFFL0MsSUFBSSxDQUFDLFNBQVM7Z0JBQ1YsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFFeEQsSUFBSSxRQUFRO2dCQUNSLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1lBRXBELElBQUksUUFBUSxJQUFJLFVBQVUsSUFBSSxNQUFNO2dCQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBRTVELElBQUksVUFBVTtnQkFDVixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUVwRCxJQUFJLFFBQVEsSUFBSSxVQUFVLElBQUksTUFBTTtnQkFDaEMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFFekQsSUFBSSxVQUFVO2dCQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBR2pELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFRDs7VUFFRTtRQUNLLDRDQUFtQixHQUExQixVQUEyQixRQUFnQjtZQUN2QyxPQUFPLEVBQUUsQ0FBQztRQUNkLENBQUM7UUFFTSwwQ0FBaUIsR0FBeEIsVUFBeUIsUUFBZ0I7WUFDckMsT0FBTyxFQUFFLENBQUM7UUFDZCxDQUFDO1FBRU0sc0NBQWEsR0FBcEI7WUFDSSxPQUFPO2dCQUNILEtBQUssRUFBRSxFQUFFO2dCQUNULEtBQUssRUFBRSxFQUFFO2FBQ1osQ0FBQztRQUNOLENBQUM7UUFFRDs7VUFFRTtRQUNLLGlDQUFRLEdBQWY7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7UUFFTSxtQ0FBVSxHQUFqQjtZQUNJLE1BQU0sSUFBSSxLQUFLLENBQUMscURBQXFELENBQUMsQ0FBQztRQUMzRSxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxpQ0FBUSxHQUFmO1lBQUEsaUJBa0NDO1lBakNHLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztZQUVqQixJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxFQUFFO2dCQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQztvQkFDMUMsT0FBTyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTztvQkFDcEMsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPO29CQUMzQixPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCO29CQUNwRCxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDO3dCQUM3QixPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQzs0QkFDekIsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssSUFBSSxJQUFJLEtBQUksQ0FBQyxpQkFBaUIsS0FBSyxLQUFLLEVBQUU7Z0NBQ2hFLE9BQU8sSUFBSSxDQUFDOzZCQUNmOzRCQUVELE9BQU8sRUFBRSxDQUFDO3dCQUNkLENBQUMsQ0FBQztxQkFDTCxDQUFDO2lCQUNMLENBQUMsQ0FBQztnQkFFSCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2QsSUFBSSxZQUFZLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUM3QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO2lCQUNsRTs7b0JBRUcsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUVyRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNsQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQzthQUN6QjtpQkFBTTtnQkFDSCxLQUFLLEdBQUcsQ0FBQyxFQUFFLENBQUM7YUFDZjtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRDs7VUFFRTtRQUNLLGdDQUFPLEdBQWQ7WUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtnQkFDdkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN4QjtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBRWxCLGlCQUFNLE9BQU8sV0FBRSxDQUFDO1FBQ3BCLENBQUM7UUFDTCxxQkFBQztJQUFELENBQUMsQUEvZEQsQ0FBNkIsT0FBTyxHQStkbkM7SUFFRCxPQUFTLGNBQWMsQ0FBQyJ9