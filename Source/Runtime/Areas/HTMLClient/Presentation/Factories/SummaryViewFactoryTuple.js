/// <amd-module name="Factories/SummaryViewFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/SummaryViewFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var SummaryViewFactoryTuple = /** @class */ (function (_super) {
        __extends(SummaryViewFactoryTuple, _super);
        function SummaryViewFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(SummaryViewFactoryTuple);
            _this.summaryView = factory.getSummaryView();
            _this.factory = factory;
            _this.isDisposable = true;
            return _this;
        }
        return SummaryViewFactoryTuple;
    }(Class));
    return SummaryViewFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VtbWFyeVZpZXdGYWN0b3J5VHVwbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJTdW1tYXJ5Vmlld0ZhY3RvcnlUdXBsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwyREFBMkQ7Ozs7Ozs7Ozs7Ozs7SUFLM0Q7UUFBc0MsMkNBQUs7UUFLdkMsaUNBQVksT0FBMkI7WUFBdkMsWUFDSSxpQkFBTyxTQVFWO1lBTkcscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBRXZELEtBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzVDLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDOztRQUM3QixDQUFDO1FBQ0wsOEJBQUM7SUFBRCxDQUFDLEFBZkQsQ0FBc0MsS0FBSyxHQWUxQztJQUVELE9BQVMsdUJBQXVCLENBQUMifQ==