/// <amd-module name="Factories/ViewFactory" /> 
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ViewFactory", ["require", "exports", "Factories/Factory", "Management/RADApplication", "Factories/ViewFactoryTuple"], function (require, exports, Factory, RADApplication, ViewFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller ViewFactories. Erzeugt das View-Bundle und verkünpft dieses
    */
    var ViewFactory = /** @class */ (function (_super) {
        __extends(ViewFactory, _super);
        function ViewFactory(params) {
            var _this = _super.call(this, params) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ViewFactory);
            _this.name = _this.paramOrDefault(params.name, null, _this.escapeValue.bind(_this));
            _this.id = _this.paramOrDefault(_.uniqueId(_this.name), null, _this.escapeValue.bind(_this));
            _this.viewId = _this.paramOrDefault(params.ViewId || params.viewId, '', _this.escapeValue.bind(_this));
            _this.viewName = _this.paramOrDefault(params.ViewName || params.viewName, '', _this.escapeValue.bind(_this));
            _this.factory = _this.paramOrDefault(params.Factory, '', _this.escapeValue.bind(_this));
            _this.ergName = _this.paramOrDefault(params.Title || params.ErgName || params.ergName, '', _this.escapeValue.bind(_this));
            _this.viewModel = _this.paramOrDefault(params.viewModel, null);
            _this.view = _this.paramOrDefault(params.view, null);
            _this.undisposeProperties.push('id', 'name');
            return _this;
        }
        /*
        *  Ermittelt, ob abhängige Factories bereits erzeugt wurden und erzeugt diese, falls nicht vorhanden
        */
        ViewFactory.prototype.resolveDependencies = function (dependencies) {
            for (var i = 0; i < dependencies.length; i++) {
                var element = $('[data-view="' + dependencies[i] + '"]');
                if (element.length === 0) {
                    var factory = Factory.createInstance(dependencies[i], { name: dependencies[i] });
                    var tuple = factory.getTuple();
                }
            }
        };
        /*
        * Setzt den Seitentitel des Browsers
        */
        ViewFactory.prototype.setPageTitle = function (value) {
            if (value) {
                value += ' - ';
            }
            value += RADApplication.appName();
            document.title = value;
        };
        /*
        * Erzeugt ein neues View-Element in einer Region
        */
        ViewFactory.prototype.createRootElement = function (region, view) {
            return $('<div data-view="' + view + '" data-factory="' + this.id + '">').appendTo('[data-region="' + region + '"]');
        };
        /*
        * Prüft, ob eine Weiterleitung notwendig ist, und gibt diese zurück
        */
        ViewFactory.prototype.getNecessaryRedirect = function () {
            var isLoggedIn = RADApplication.user && RADApplication.user.isLoggedIn();
            if (isLoggedIn) {
                return false;
            }
            return 'Login';
        };
        /*
        * IRefreshable-Implementierung bei Zurück-Navigation
        */
        ViewFactory.prototype.refresh = function () {
            return true;
        };
        /*
        * Disposed die ViewFactory
        */
        ViewFactory.prototype.dispose = function () {
            if (this.viewModel != null) {
                if (!this.viewModel.isDisposed) {
                    this.viewModel.dispose();
                }
                this.viewModel = null;
            }
            if (this.view != null) {
                if (!this.view.isDisposed) {
                    this.view.dispose();
                }
                this.view = null;
            }
            if (this.tuple != null) {
                if (!this.tuple.isDisposed) {
                    this.tuple.dispose();
                }
                this.tuple = null;
            }
            _super.prototype.dispose.call(this);
        };
        /*
        * Erzeugt das FactoryTuple und die Factory
        */
        ViewFactory.prototype.getTuple = function () {
            this.tuple = new ViewFactoryTuple(this);
            return this.tuple;
        };
        ViewFactory.prototype.getView = function () {
            throw new Error('Die Funktion "getView" wurde nicht implementiert');
        };
        /*
        * Escaped einen Wert
        */
        ViewFactory.prototype.escapeValue = function (value) {
            if (_.isObject(value)) {
                return value;
            }
            // Hier muss ggf. decodiert und NICHT encodiert werden, da dieser wert ggf. von der encodierten URL kommt!
            return decodeURIComponent(value);
        };
        return ViewFactory;
    }(Factory));
    return ViewFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld0ZhY3RvcnkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJWaWV3RmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnREFBZ0Q7Ozs7Ozs7Ozs7Ozs7SUFNaEQ7O01BRUU7SUFDRjtRQUEwQiwrQkFBTztRQTRDN0IscUJBQVksTUFBTTtZQUFsQixZQUNJLGtCQUFNLE1BQU0sQ0FBQyxTQWNoQjtZQVpHLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFM0MsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7WUFDaEYsS0FBSSxDQUFDLEVBQUUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkcsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUMsQ0FBQztZQUN6RyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUMsQ0FBQztZQUNwRixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEgsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDN0QsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDbkQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7O1FBQ2hELENBQUM7UUFFRDs7VUFFRTtRQUNLLHlDQUFtQixHQUExQixVQUEyQixZQUFZO1lBQ25DLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMxQyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFFekQsSUFBSSxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDdEIsSUFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQWdCLENBQUM7b0JBQ2hHLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDbEM7YUFDSjtRQUNMLENBQUM7UUFFRDs7VUFFRTtRQUNLLGtDQUFZLEdBQW5CLFVBQW9CLEtBQWE7WUFDN0IsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsS0FBSyxJQUFJLEtBQUssQ0FBQzthQUNsQjtZQUVELEtBQUssSUFBSSxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFbEMsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQztRQUVEOztVQUVFO1FBQ0ssdUNBQWlCLEdBQXhCLFVBQXlCLE1BQU0sRUFBRSxJQUFJO1lBQ2pDLE9BQU8sQ0FBQyxDQUFDLGtCQUFrQixHQUFHLElBQUksR0FBRyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDekgsQ0FBQztRQUVEOztVQUVFO1FBQ0ssMENBQW9CLEdBQTNCO1lBQ0ksSUFBSSxVQUFVLEdBQUcsY0FBYyxDQUFDLElBQUksSUFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBRXpFLElBQUksVUFBVSxFQUFFO2dCQUNaLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQztRQUVEOztVQUVFO1FBQ0ssNkJBQU8sR0FBZDtZQUNJLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRDs7VUFFRTtRQUNLLDZCQUFPLEdBQWQ7WUFDSSxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQzVCO2dCQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2FBQ3pCO1lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2lCQUN2QjtnQkFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUNwQjtZQUVELElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRTtvQkFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDeEI7Z0JBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDckI7WUFFRCxpQkFBTSxPQUFPLFdBQUUsQ0FBQztRQUNwQixDQUFDO1FBRUQ7O1VBRUU7UUFDSyw4QkFBUSxHQUFmO1lBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXhDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO1FBRU0sNkJBQU8sR0FBZDtZQUNJLE1BQU0sSUFBSSxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztRQUN4RSxDQUFDO1FBRUQ7O1VBRUU7UUFDTSxpQ0FBVyxHQUFuQixVQUFvQixLQUFLO1lBQ3JCLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbkIsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCwwR0FBMEc7WUFDMUcsT0FBTyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQ0wsa0JBQUM7SUFBRCxDQUFDLEFBdktELENBQTBCLE9BQU8sR0F1S2hDO0lBRUQsT0FBUyxXQUFXLENBQUMifQ==