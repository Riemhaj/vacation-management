/// <amd-module name="Factories/ViewExtensionFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ViewExtensionFactory", ["require", "exports", "Factories/Factory", "Management/ComponentManager", "Factories/ViewExtensionFactoryTuple"], function (require, exports, Factory, ComponentManager, ViewExtensionFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller ViewExtensionFactories
    */
    var ViewExtensionFactory = /** @class */ (function (_super) {
        __extends(ViewExtensionFactory, _super);
        function ViewExtensionFactory(params) {
            var _this = _super.call(this, params) || this;
            _this.ViewExtensionContext = ComponentManager.getViewModel('ViewExtensionContext');
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ViewExtensionFactory);
            _this.isViewExtensionFactory = true;
            _this.isDisposable = true;
            return _this;
        }
        /*
        * Erstellt einen neuen Kontext für das ViewExtension
        */
        ViewExtensionFactory.prototype.createContext = function (properties) {
            // return new this.ViewExtensionContext(properties || {});
            return null;
        };
        /*
        * Erzeugt das FactoryTuple und die Factory
        */
        ViewExtensionFactory.prototype.getTuple = function () {
            this.tuple = new ViewExtensionFactoryTuple(this);
            return this.tuple;
        };
        return ViewExtensionFactory;
    }(Factory));
    return ViewExtensionFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld0V4dGVuc2lvbkZhY3RvcnkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJWaWV3RXh0ZW5zaW9uRmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx3REFBd0Q7Ozs7Ozs7Ozs7Ozs7SUFNeEQ7O01BRUU7SUFDRjtRQUFtQyx3Q0FBTztRQVd0Qyw4QkFBWSxNQUFNO1lBQWxCLFlBQ0ksa0JBQU0sTUFBTSxDQUFDLFNBT2hCO1lBakJPLDBCQUFvQixHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBWWpGLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUVwRCxLQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1lBQ25DLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDOztRQUM3QixDQUFDO1FBRUQ7O1VBRUU7UUFDSyw0Q0FBYSxHQUFwQixVQUFxQixVQUFVO1lBQzNCLDBEQUEwRDtZQUMxRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7O1VBRUU7UUFDSyx1Q0FBUSxHQUFmO1lBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO1FBQ0wsMkJBQUM7SUFBRCxDQUFDLEFBcENELENBQW1DLE9BQU8sR0FvQ3pDO0lBRUQsT0FBUyxvQkFBb0IsQ0FBQyJ9