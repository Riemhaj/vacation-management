/// <amd-module name="Factories/GroupFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/GroupFactory", ["require", "exports", "Factories/Factory", "Factories/GroupFactoryTuple"], function (require, exports, Factory, GroupFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller GroupFactories
    */
    var GroupFactory = /** @class */ (function (_super) {
        __extends(GroupFactory, _super);
        function GroupFactory(params) {
            var _this = _super.call(this, params) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(GroupFactory);
            _this.detailViewGroup = _this.paramOrDefault(params.detailViewGroup, null);
            _this.layout = _this.paramOrDefault(params.layout, 'NoneLayout');
            _this.colspan = _this.paramOrDefault(params.colspan, 'One');
            _this.context = _this.paramOrDefault(params.context, null);
            _this.isVisible = _this.observableOrDefault(params.isVisible, true);
            _this.isSummaryViewMode = _this.paramOrDefault(params.isSummaryViewMode, false);
            _this.isDisposable = true;
            _this.undisposeProperties.push('context');
            return _this;
        }
        /*
        * Erzeugt das FactoryTuple und die Factory
        */
        GroupFactory.prototype.getTuple = function () {
            this.tuple = new GroupFactoryTuple(this);
            return this.tuple;
        };
        GroupFactory.prototype.getControl = function () {
            return null;
        };
        return GroupFactory;
    }(Factory));
    return GroupFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JvdXBGYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiR3JvdXBGYWN0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdEQUFnRDs7Ozs7Ozs7Ozs7OztJQU9oRDs7TUFFRTtJQUNGO1FBQTJCLGdDQUFPO1FBcUM5QixzQkFBWSxNQUFNO1lBQWxCLFlBQ0ksa0JBQU0sTUFBTSxDQUFDLFNBYWhCO1lBWEcscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUU1QyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6RSxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztZQUMvRCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUMxRCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2xFLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDOztRQUM3QyxDQUFDO1FBRUQ7O1VBRUU7UUFDSywrQkFBUSxHQUFmO1lBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO1FBRU0saUNBQVUsR0FBakI7WUFDSSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0wsbUJBQUM7SUFBRCxDQUFDLEFBaEVELENBQTJCLE9BQU8sR0FnRWpDO0lBRUQsT0FBUyxZQUFZLENBQUMifQ==