/// <amd-module name="Factories/LayoutFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/LayoutFactory", ["require", "exports", "Factories/Factory", "Factories/LayoutFactoryTuple"], function (require, exports, Factory, LayoutFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller LayoutFactories
    */
    var LayoutFactory = /** @class */ (function (_super) {
        __extends(LayoutFactory, _super);
        function LayoutFactory(params) {
            var _this = _super.call(this, params) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(LayoutFactory);
            _this.isLayoutFactory = true;
            _this.context = _this.paramOrDefault(params.context, null);
            _this.tuples = _this.paramOrDefault(params.tuples, []);
            _this.isDisposable = true;
            _this.undisposeProperties.push('context');
            return _this;
        }
        /*
         * Erzeugt das FactoryTuple und die Factory
         */
        LayoutFactory.prototype.getTuple = function () {
            this.tuple = new LayoutFactoryTuple(this);
            return this.tuple;
        };
        return LayoutFactory;
    }(Factory));
    return LayoutFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGF5b3V0RmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkxheW91dEZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsaURBQWlEOzs7Ozs7Ozs7Ozs7O0lBTWpEOztNQUVFO0lBQ0Y7UUFBNEIsaUNBQU87UUFtQi9CLHVCQUFZLE1BQU07WUFBbEIsWUFDSSxrQkFBTSxNQUFNLENBQUMsU0FVaEI7WUFSRyxxQ0FBcUM7WUFDckMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBRTdDLEtBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pELEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JELEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7O1FBQzdDLENBQUM7UUFFRDs7V0FFRztRQUNJLGdDQUFRLEdBQWY7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7UUFDTCxvQkFBQztJQUFELENBQUMsQUF2Q0QsQ0FBNEIsT0FBTyxHQXVDbEM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9