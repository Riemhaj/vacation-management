/// <amd-module name="Factories/GroupFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/GroupFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var GroupFactoryTuple = /** @class */ (function (_super) {
        __extends(GroupFactoryTuple, _super);
        function GroupFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(GroupFactoryTuple);
            _this.control = factory.getControl();
            _this.factory = factory;
            _this.undisposeProperties.push('factory');
            _this.isDisposable = true;
            return _this;
        }
        GroupFactoryTuple.prototype.getCreatedControlTuples = function () {
            var tuples = [];
            for (var i = 0; i < this.factory.detailViewGroup.items.length; i++) {
                var name_1 = this.factory.detailViewGroup.items[i].name;
                var tuple = this.factory.context.getCreatedTuple(name_1);
                if (tuple != null) {
                    tuples.push(tuple);
                }
            }
            return tuples;
        };
        return GroupFactoryTuple;
    }(Class));
    return GroupFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JvdXBGYWN0b3J5VHVwbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJHcm91cEZhY3RvcnlUdXBsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxxREFBcUQ7Ozs7Ozs7Ozs7Ozs7SUFRckQ7UUFBZ0MscUNBQUs7UUFLakMsMkJBQVksT0FBcUI7WUFBakMsWUFDSSxpQkFBTyxTQVNWO1lBUEcscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBRWpELEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1FBQzdCLENBQUM7UUFFTSxtREFBdUIsR0FBOUI7WUFDSSxJQUFJLE1BQU0sR0FBK0IsRUFBRSxDQUFDO1lBRTVDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNoRSxJQUFJLE1BQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN0RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsTUFBSSxDQUF3QixDQUFDO2dCQUU5RSxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDdEI7YUFDSjtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFDTCx3QkFBQztJQUFELENBQUMsQUEvQkQsQ0FBZ0MsS0FBSyxHQStCcEM7SUFFRCxPQUFTLGlCQUFpQixDQUFDIn0=