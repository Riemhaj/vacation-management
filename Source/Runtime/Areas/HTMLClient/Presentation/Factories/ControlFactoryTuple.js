/// <amd-module name="Factories/ControlFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ControlFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ControlFactoryTuple = /** @class */ (function (_super) {
        __extends(ControlFactoryTuple, _super);
        function ControlFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ControlFactoryTuple);
            _this.control = factory.getControl();
            _this.label = factory.getLabel();
            _this.factory = factory;
            _this.undisposeProperties.push('factory');
            _this.isDisposable = true;
            return _this;
        }
        return ControlFactoryTuple;
    }(Class));
    return ControlFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbEZhY3RvcnlUdXBsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnRyb2xGYWN0b3J5VHVwbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdURBQXVEOzs7Ozs7Ozs7Ozs7O0lBS3ZEO1FBQWtDLHVDQUFLO1FBTW5DLDZCQUFZLE9BQXVCO1lBQW5DLFlBQ0ksaUJBQU8sU0FVVjtZQVJHLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUVuRCxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNwQyxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQyxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pDLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDOztRQUM3QixDQUFDO1FBQ0wsMEJBQUM7SUFBRCxDQUFDLEFBbEJELENBQWtDLEtBQUssR0FrQnRDO0lBRUQsT0FBUyxtQkFBbUIsQ0FBQyJ9