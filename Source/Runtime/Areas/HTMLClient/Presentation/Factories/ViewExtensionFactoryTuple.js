/// <amd-module name="Factories/ViewExtensionFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ViewExtensionFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ViewExtensionFactoryTuple = /** @class */ (function (_super) {
        __extends(ViewExtensionFactoryTuple, _super);
        function ViewExtensionFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ViewExtensionFactoryTuple);
            _this.control = factory.getControl();
            _this.factory = factory;
            _this.isDisposable = true;
            return _this;
        }
        return ViewExtensionFactoryTuple;
    }(Class));
    return ViewExtensionFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld0V4dGVuc2lvbkZhY3RvcnlUdXBsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlZpZXdFeHRlbnNpb25GYWN0b3J5VHVwbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNkRBQTZEOzs7Ozs7Ozs7Ozs7O0lBSzdEO1FBQXdDLDZDQUFLO1FBS3pDLG1DQUFZLE9BQU87WUFBbkIsWUFDSSxpQkFBTyxTQVFWO1lBTkcscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBRXpELEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDOztRQUM3QixDQUFDO1FBQ0wsZ0NBQUM7SUFBRCxDQUFDLEFBZkQsQ0FBd0MsS0FBSyxHQWU1QztJQUVELE9BQVMseUJBQXlCLENBQUMifQ==