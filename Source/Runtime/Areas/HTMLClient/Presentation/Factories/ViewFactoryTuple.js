/// <amd-module name="Factories/ViewFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ViewFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ViewFactoryTuple = /** @class */ (function (_super) {
        __extends(ViewFactoryTuple, _super);
        function ViewFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ViewFactoryTuple);
            _this.view = factory.getView();
            _this.factory = factory;
            _this.isDisposable = true;
            return _this;
        }
        return ViewFactoryTuple;
    }(Class));
    return ViewFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld0ZhY3RvcnlUdXBsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlZpZXdGYWN0b3J5VHVwbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0RBQW9EOzs7Ozs7Ozs7Ozs7O0lBS3BEO1FBQStCLG9DQUFLO1FBS2hDLDBCQUFZLE9BQW9CO1lBQWhDLFlBQ0ksaUJBQU8sU0FRVjtZQU5HLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUVoRCxLQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM5QixLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzs7UUFDN0IsQ0FBQztRQUNMLHVCQUFDO0lBQUQsQ0FBQyxBQWZELENBQStCLEtBQUssR0FlbkM7SUFFRCxPQUFTLGdCQUFnQixDQUFDIn0=