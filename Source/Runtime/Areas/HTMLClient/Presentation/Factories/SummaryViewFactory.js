/// <amd-module name="Factories/SummaryViewFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/SummaryViewFactory", ["require", "exports", "Factories/Factory", "Factories/SummaryViewFactoryTuple"], function (require, exports, Factory, SummaryViewFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller SummaryViewFactories
    */
    var SummaryViewFactory = /** @class */ (function (_super) {
        __extends(SummaryViewFactory, _super);
        function SummaryViewFactory(params) {
            var _this = _super.call(this, params) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(SummaryViewFactory);
            _this.genericObject = _this.paramOrDefault(params.genericObject, null);
            _this.pdoId = _this.paramOrDefault(params.pdoId, null);
            _this.pdTypeName = _this.paramOrDefault(params.pdTypeName, null);
            _this.ergName = _this.paramOrDefault(params.ergName, null);
            _this.showAlways = _this.paramOrDefault(params.showAlways === true || params.showAlways === 'true', false);
            _this.showHistory = _this.paramOrDefault(params.showHistory === true || params.showHistory === 'true', false);
            _this.orientation = _this.paramOrDefault(params.orientation, 'Horizontal');
            _this.screenRatio = _this.paramOrDefault(params.screenRatio, 50);
            _this.isDisposable = true;
            return _this;
        }
        /*
        * Erzeugt das FactoryTuple und die Factory
        */
        SummaryViewFactory.prototype.getTuple = function () {
            this.tuple = new SummaryViewFactoryTuple(this);
            return this.tuple;
        };
        SummaryViewFactory.prototype.getSummaryView = function () {
            throw new Error('Die Funktion "getSummaryView" wurde nicht implementiert');
        };
        return SummaryViewFactory;
    }(Factory));
    return SummaryViewFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VtbWFyeVZpZXdGYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiU3VtbWFyeVZpZXdGYWN0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNEQUFzRDs7Ozs7Ozs7Ozs7OztJQU10RDs7TUFFRTtJQUNGO1FBQWlDLHNDQUFPO1FBNENwQyw0QkFBWSxNQUFNO1lBQWxCLFlBQ0ksa0JBQU0sTUFBTSxDQUFDLFNBY2hCO1lBWkcscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBRWxELEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3JELEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQy9ELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pELEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxNQUFNLENBQUMsVUFBVSxLQUFLLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUN6RyxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDNUcsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDekUsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDL0QsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1FBQzdCLENBQUM7UUFFRDs7VUFFRTtRQUNLLHFDQUFRLEdBQWY7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7UUFFTSwyQ0FBYyxHQUFyQjtZQUNJLE1BQU0sSUFBSSxLQUFLLENBQUMseURBQXlELENBQUMsQ0FBQztRQUMvRSxDQUFDO1FBQ0wseUJBQUM7SUFBRCxDQUFDLEFBeEVELENBQWlDLE9BQU8sR0F3RXZDO0lBRUQsT0FBUyxrQkFBa0IsQ0FBQyJ9