/// <amd-module name="Factories/ColumnFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ColumnFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ColumnFactoryTuple = /** @class */ (function (_super) {
        __extends(ColumnFactoryTuple, _super);
        function ColumnFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ColumnFactoryTuple);
            _this.value = factory.getValue();
            _this.factory = factory;
            _this.isDisposable = true;
            return _this;
        }
        return ColumnFactoryTuple;
    }(Class));
    return ColumnFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29sdW1uRmFjdG9yeVR1cGxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ29sdW1uRmFjdG9yeVR1cGxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNEQUFzRDs7Ozs7Ozs7Ozs7OztJQUt0RDtRQUFpQyxzQ0FBSztRQUtsQyw0QkFBWSxPQUFzQjtZQUFsQyxZQUNJLGlCQUFPLFNBUVY7WUFORyxxQ0FBcUM7WUFDckMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFFbEQsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7WUFDdkIsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1FBQzdCLENBQUM7UUFDTCx5QkFBQztJQUFELENBQUMsQUFmRCxDQUFpQyxLQUFLLEdBZXJDO0lBRUQsT0FBUyxrQkFBa0IsQ0FBQyJ9