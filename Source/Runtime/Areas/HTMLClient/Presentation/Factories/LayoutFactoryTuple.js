/// <amd-module name="Factories/LayoutFactoryTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/LayoutFactoryTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var LayoutFactoryTuple = /** @class */ (function (_super) {
        __extends(LayoutFactoryTuple, _super);
        function LayoutFactoryTuple(factory) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(LayoutFactoryTuple);
            _this.control = factory.getControl();
            _this.factory = factory;
            _this.isDisposable = true;
            return _this;
        }
        return LayoutFactoryTuple;
    }(Class));
    return LayoutFactoryTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGF5b3V0RmFjdG9yeVR1cGxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTGF5b3V0RmFjdG9yeVR1cGxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNEQUFzRDs7Ozs7Ozs7Ozs7OztJQUt0RDtRQUFpQyxzQ0FBSztRQU1sQyw0QkFBWSxPQUFPO1lBQW5CLFlBQ0ksaUJBQU8sU0FRVjtZQU5HLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUVsRCxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNwQyxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN2QixLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzs7UUFDN0IsQ0FBQztRQUNMLHlCQUFDO0lBQUQsQ0FBQyxBQWhCRCxDQUFpQyxLQUFLLEdBZ0JyQztJQUVELE9BQVMsa0JBQWtCLENBQUMifQ==