/// <amd-module name="Factories/ColumnFactory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/ColumnFactory", ["require", "exports", "Factories/Factory", "Factories/ColumnFactoryTuple"], function (require, exports, Factory, ColumnFactoryTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller ColumnFactories
    */
    var ColumnFactory = /** @class */ (function (_super) {
        __extends(ColumnFactory, _super);
        function ColumnFactory(params) {
            var _this = _super.call(this, params) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ColumnFactory);
            _this.raw = _this.paramOrDefault(params.raw, null);
            _this.isDisposable = true;
            return _this;
        }
        /*
         * Erzeugt das FactoryTuple und die Factory
         */
        ColumnFactory.prototype.getTuple = function () {
            this.tuple = new ColumnFactoryTuple(this);
            return this.tuple;
        };
        ColumnFactory.prototype.getValue = function () {
            throw new Error('Die Funktion "getValue" wurde nicht implementiert');
        };
        ColumnFactory.isPlainText = false;
        ColumnFactory.isJQueryElement = false;
        return ColumnFactory;
    }(Factory));
    return ColumnFactory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29sdW1uRmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbHVtbkZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsaURBQWlEOzs7Ozs7Ozs7Ozs7O0lBS2pEOztNQUVFO0lBQ0Y7UUFBNEIsaUNBQU87UUFZL0IsdUJBQVksTUFBVztZQUF2QixZQUNJLGtCQUFNLE1BQU0sQ0FBQyxTQU9oQjtZQUxHLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFFN0MsS0FBSSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDakQsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O1FBQzdCLENBQUM7UUFFRDs7V0FFRztRQUNJLGdDQUFRLEdBQWY7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7UUFFTSxnQ0FBUSxHQUFmO1lBQ0ksTUFBTSxJQUFJLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO1FBQ3pFLENBQUM7UUE5QmEseUJBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsNkJBQWUsR0FBRyxLQUFLLENBQUM7UUE4QjFDLG9CQUFDO0tBQUEsQUFqQ0QsQ0FBNEIsT0FBTyxHQWlDbEM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9