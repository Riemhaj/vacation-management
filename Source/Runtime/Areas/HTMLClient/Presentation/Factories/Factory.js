/// <amd-module name="Factories/Factory" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Factories/Factory", ["require", "exports", "Class", "Management/ComponentManager", "Utility/Dictionary"], function (require, exports, Class, ComponentManager, Dictionary) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller Factories und bietet statische Methoden zur Erzeugung von Factory-Instanzen
    */
    var Factory = /** @class */ (function (_super) {
        __extends(Factory, _super);
        function Factory(params) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entferne
            _this.applyPrototypeToInstance(Factory);
            _this.locator = _this.paramOrDefault(params.locator, null);
            return _this;
        }
        Factory.uncap = function (str) {
            return str.substr(0, 1).toLowerCase() + str.substr(1);
        };
        Factory.parseQueryString = function (query) {
            var result = new Dictionary();
            /*
             * [0] IsCollapsed=false&
             * [1] Layout=FlowLayout(?Mode=Default&Orientation=Horizontal)
             */
            var paramQueries = XRegExp.match(query, /([\w\d]+=[\w\d\,.\-\*',\s=]+(?:\(\?[\w\d\,.'&=]+\))?)(?:[&])?/g);
            for (var i in paramQueries) {
                /*
                 * ==> Layout=FlowLayout(?Mode=Default&Orientation=Horizontal)
                 */
                var thisParamQuery = paramQueries[i].replace(/&$/, '');
                var paramName = thisParamQuery;
                var paramValue = null;
                var paramParams = null;
                var valueIndex = thisParamQuery.indexOf('=');
                if (valueIndex > -1) {
                    /*
                     * ==> Layout
                     */
                    paramName = thisParamQuery.substring(0, valueIndex);
                    /*
                     * ==> FlowLayout(?Mode=Default&Orientation=Horizontal)
                     */
                    paramValue = thisParamQuery.substring(valueIndex + 1, thisParamQuery.length);
                    /*
                     * [0] FlowLayout
                     * [1] Mode=Default&Orientation=Horizontal
                     */
                    var subParam = XRegExp.match(paramValue, /([\w\d\,.\-',\s]+)\(\?([\w\d\,.'&=]+)?\)/);
                    if (subParam != null && subParam.length === 3) {
                        paramValue = subParam[1];
                        paramParams = Factory.parseQueryString(subParam[2]);
                    }
                    /*
                     * Falls der Wert ein boolscher Wert ist, muss dieser geparst werden
                     */
                    if (['true', 'false'].indexOf(paramValue.toLowerCase()) > -1) {
                        paramValue = JSON.parse(paramValue.toLowerCase());
                    }
                }
                if (paramParams != null) {
                    result.add(Factory.uncap(paramName), {
                        value: paramValue,
                        parameters: paramParams
                    });
                }
                else {
                    result.add(Factory.uncap(paramName), paramValue);
                }
            }
            return result;
        };
        /*
         * Parsed einen String im angegebenen Format rekursiv und wandelt ihn in ein Objekt um
         * Format: ExpanderGroup?IsCollapsed=false&Layout=FlowLayout(?Mode=Default&Orientation=Horizontal)
         */
        Factory.parseUIGeneratorQuery = function (query) {
            var uiGenerator = query;
            var parameters = null;
            var paramIndex = uiGenerator.indexOf('?');
            if (paramIndex > -1) {
                uiGenerator = query.substring(0, paramIndex);
                query = query.substring(paramIndex + 1, query.length);
                parameters = Factory.parseQueryString(query);
            }
            uiGenerator = uiGenerator.replace(/Factory$/, '');
            uiGenerator = uiGenerator.replace(/\($/, '');
            var result = {
                uiGenerator: uiGenerator,
                parameters: parameters
            };
            return result;
        };
        /*
         * Erzeugt eine neue Instanz einer Factory anhand eines QueryStrings
         */
        Factory.createInstance = function (queryString, params, fallback) {
            var parser = Factory.parseUIGeneratorQuery(queryString);
            var options = _.extend(params || {}, parser.parameters);
            var Component = ComponentManager.getFactory(parser.uiGenerator);
            if (Component == null && fallback != null) {
                Component = ComponentManager.getFactory(fallback);
                console.log('Factory "' + queryString + '" wurde nicht gefunden. Fallback "' + fallback + ' wird verwendet."');
            }
            if (Component == null) {
                throw 'Factory-Komponente "' + queryString + '" wurde nicht gefunden und es wurde kein Fallback definiert.';
            }
            if (options.detailViewItem == null) {
                options.detailViewItem = {
                    uiGenerator: queryString
                };
            }
            return new Component(options);
        };
        return Factory;
    }(Class));
    return Factory;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMkNBQTJDOzs7Ozs7Ozs7Ozs7O0lBUzNDOztNQUVFO0lBQ0Y7UUFBc0IsMkJBQUs7UUFNdkIsaUJBQVksTUFBTTtZQUFsQixZQUNJLGlCQUFPLFNBTVY7WUFKRyxvQ0FBb0M7WUFDcEMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXZDLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDOztRQUM3RCxDQUFDO1FBRWMsYUFBSyxHQUFwQixVQUFxQixHQUFXO1lBQzVCLE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRCxDQUFDO1FBRWMsd0JBQWdCLEdBQS9CLFVBQWdDLEtBQUs7WUFDakMsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQTZCLENBQUM7WUFFekQ7OztlQUdHO1lBQ0gsSUFBSSxZQUFZLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsZ0VBQWdFLENBQUMsQ0FBQztZQUUxRyxLQUFLLElBQUksQ0FBQyxJQUFJLFlBQVksRUFBRTtnQkFDeEI7O21CQUVHO2dCQUNILElBQUksY0FBYyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUV2RCxJQUFJLFNBQVMsR0FBRyxjQUFjLENBQUM7Z0JBQy9CLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixJQUFJLFVBQVUsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUU3QyxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDakI7O3VCQUVHO29CQUNILFNBQVMsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztvQkFFcEQ7O3VCQUVHO29CQUNILFVBQVUsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxDQUFDLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUU3RTs7O3VCQUdHO29CQUNILElBQUksUUFBUSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLDBDQUEwQyxDQUFDLENBQUM7b0JBRXJGLElBQUksUUFBUSxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDM0MsVUFBVSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekIsV0FBVyxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDdkQ7b0JBRUQ7O3VCQUVHO29CQUNILElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO3dCQUMxRCxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztxQkFDckQ7aUJBQ0o7Z0JBRUQsSUFBSSxXQUFXLElBQUksSUFBSSxFQUFFO29CQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2pDLEtBQUssRUFBRSxVQUFVO3dCQUNqQixVQUFVLEVBQUUsV0FBVztxQkFDMUIsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNO29CQUNILE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztpQkFDcEQ7YUFDSjtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFRDs7O1dBR0c7UUFDVyw2QkFBcUIsR0FBbkMsVUFBb0MsS0FBYTtZQUM3QyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksVUFBVSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFMUMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2pCLFdBQVcsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFFN0MsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3RELFVBQVUsR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEQ7WUFFRCxXQUFXLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDbEQsV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRTdDLElBQUksTUFBTSxHQUFHO2dCQUNULFdBQVcsRUFBRSxXQUFXO2dCQUN4QixVQUFVLEVBQUUsVUFBVTthQUN6QixDQUFDO1lBRUYsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQztRQUVEOztXQUVHO1FBQ1csc0JBQWMsR0FBNUIsVUFBNkIsV0FBbUIsRUFBRSxNQUFvQyxFQUFFLFFBQWdCO1lBQ3BHLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4RCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxFQUFFLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBRXhELElBQUksU0FBUyxHQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFRLENBQUM7WUFFdkUsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLFdBQVcsR0FBRyxvQ0FBb0MsR0FBRyxRQUFRLEdBQUcsbUJBQW1CLENBQUMsQ0FBQTthQUNqSDtZQUVELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDbkIsTUFBTSxzQkFBc0IsR0FBRyxXQUFXLEdBQUcsOERBQThELENBQUM7YUFDL0c7WUFFRCxJQUFJLE9BQU8sQ0FBQyxjQUFjLElBQUksSUFBSSxFQUFFO2dCQUNoQyxPQUFPLENBQUMsY0FBYyxHQUFHO29CQUNyQixXQUFXLEVBQUUsV0FBVztpQkFDM0IsQ0FBQzthQUNMO1lBRUQsT0FBTyxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBQ0wsY0FBQztJQUFELENBQUMsQUF2SUQsQ0FBc0IsS0FBSyxHQXVJMUI7SUFFRCxPQUFTLE9BQU8sQ0FBQyJ9