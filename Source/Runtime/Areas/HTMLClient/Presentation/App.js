var MODEL = JSON.parse($('#MODEL').html());
// RequireJS konfigurieren
require.config({
    baseUrl: MODEL.HTMLClientResourceURL + 'Presentation/',
    paths: {
        text: '../Scripts/require/text',
        ext: '../Scripts/require/ext'
    },
    // Ergänzung ab Kendo 2017 notwendig, da kendo.core in den Culture- und Message-Dateien von Kendo benötigt wird
    // und das Mapping an dieser Stelle nicht funktioniert hat
    map: {
        '*': {
            'kendo.core': MODEL.HTMLClientResourceURL + 'Scripts/kendo.ui.core/kendo.ui.core'
        }
    },
    waitSeconds: 0
});
// jquery auch als Module veröffentlichen
define("jquery", [], function () {
    return window.jQuery;
});
// Anwendungsstart einleiten
$(document).ready(function () {
    'use strict';
    // Browser-Vorraussetzungen prüfen
    var errors = [];
    var html = $('html'), body = $('body');
    var requirements = {
        history: {
            name: 'History API',
            description: 'Dynamische Änderung der Adressleiste und Unterstützung von DeepLinks.'
        },
        //localstorage: {
        //    name: 'LocalStorage',
        //    description: 'Lokales Speichern von benutzersepezifischen Einstellungen.'
        //},
        csscalc: {
            name: 'CSS Calc',
            description: 'Breiten-/Höhenberechnung auf Basis von Prozent- und Pixelwerten.'
        },
        flexbox: {
            name: 'CSS FlexBox',
            description: 'Berechnet Breiten und Höhen von Factories in Layouts.'
        }
    };
    for (var requirement in requirements) {
        if (html.hasClass('c-requirement-' + requirement) === false) {
            errors.push(requirements[requirement]);
        }
    }
    if (errors.length > 0) {
        var container = $('<div class="c-requirement-error"></div>').prependTo(body);
        $('<h1>Hinweis zur Browserkompatibilität</h1>').appendTo(container);
        $('<h2>Folgende Funktionen werden von Ihrem Browser nicht unterstützt:</h2>').appendTo(container);
        for (var i = 0; i < errors.length; i++) {
            var error = errors[i];
            container.append('<span class="c-requirement"><strong>' + error.name + '</strong>: ' + error.description + '</span>');
        }
        $('<h2>Bitte aktualisieren Sie Ihren Browser!</h2>').appendTo(container);
        throw 'Requirement error: Your browser does not support the following requirements: ' + _.map(errors, 'name').join(', ');
    }
    // Module laden
    require(['Utility/Loader'], function (Loader) {
        Loader.show();
        require(['Management/RADApplication', 'Management/ComponentManager', 'Management/NavigationService', 'Management/KeepAliveHandler', 'Utility/ExceptionBuilder',
            'jquery', 'Infrastructure/CustomBindings', 'Infrastructure/CustomExtenders', 'Infrastructure/KnockoutCustomization'
        ], function (RADApplication, ComponentManager, NavigationService, KeepAliveHandler, ExceptionBuilder, jq) {
            new RADApplication();
            new NavigationService();
            new KeepAliveHandler();
            new ExceptionBuilder();
            ComponentManager.init(function () {
                RADApplication.init();
                Loader.hide();
            });
        });
    });
});
// Unit-Test
var testModeEnabled = false;
var enableTestMode = function () {
    if (testModeEnabled)
        return;
    jQuery('body').addClass('unittestmode');
    testModeEnabled = true;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7QUFHM0MsMEJBQTBCO0FBQzFCLE9BQU8sQ0FBQyxNQUFNLENBQUM7SUFDWCxPQUFPLEVBQUUsS0FBSyxDQUFDLHFCQUFxQixHQUFHLGVBQWU7SUFDdEQsS0FBSyxFQUFFO1FBQ1QsSUFBSSxFQUFFLHlCQUF5QjtRQUN6QixHQUFHLEVBQUUsd0JBQXdCO0tBQ2hDO0lBQ0QsK0dBQStHO0lBQy9HLDBEQUEwRDtJQUMxRCxHQUFHLEVBQUU7UUFDRCxHQUFHLEVBQUU7WUFDRCxZQUFZLEVBQUUsS0FBSyxDQUFDLHFCQUFxQixHQUFHLHFDQUFxQztTQUNwRjtLQUNKO0lBQ0QsV0FBVyxFQUFFLENBQUM7Q0FDakIsQ0FBQyxDQUFDO0FBRUgseUNBQXlDO0FBQ3pDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFO0lBQ2pCLE9BQVEsTUFBYyxDQUFDLE1BQU0sQ0FBQztBQUNsQyxDQUFDLENBQUMsQ0FBQztBQUVILDRCQUE0QjtBQUM1QixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ2QsWUFBWSxDQUFDO0lBRWIsa0NBQWtDO0lBQ2xDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNoQixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QyxJQUFJLFlBQVksR0FBRztRQUNmLE9BQU8sRUFBRTtZQUNMLElBQUksRUFBRSxhQUFhO1lBQ25CLFdBQVcsRUFBRSx1RUFBdUU7U0FDdkY7UUFDRCxpQkFBaUI7UUFDakIsMkJBQTJCO1FBQzNCLCtFQUErRTtRQUMvRSxJQUFJO1FBQ0osT0FBTyxFQUFFO1lBQ0wsSUFBSSxFQUFFLFVBQVU7WUFDaEIsV0FBVyxFQUFFLGtFQUFrRTtTQUNsRjtRQUNELE9BQU8sRUFBRTtZQUNMLElBQUksRUFBRSxhQUFhO1lBQ25CLFdBQVcsRUFBRSx1REFBdUQ7U0FDdkU7S0FDSixDQUFDO0lBRUYsS0FBSyxJQUFJLFdBQVcsSUFBSSxZQUFZLEVBQUU7UUFDbEMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQzFDO0tBQ0o7SUFFRCxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1FBQ25CLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3RSxDQUFDLENBQUMsNENBQTRDLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDcEUsQ0FBQyxDQUFDLDBFQUEwRSxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRWxHLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV0QixTQUFTLENBQUMsTUFBTSxDQUFDLHNDQUFzQyxHQUFHLEtBQUssQ0FBQyxJQUFJLEdBQUcsYUFBYSxHQUFHLEtBQUssQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLENBQUM7U0FDekg7UUFFRCxDQUFDLENBQUMsaURBQWlELENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFekUsTUFBTSwrRUFBK0UsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDNUg7SUFFRCxlQUFlO0lBQ2YsT0FBTyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxVQUFDLE1BQU07UUFDL0IsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRWQsT0FBTyxDQUFDLENBQUMsMkJBQTJCLEVBQUUsNkJBQTZCLEVBQUUsOEJBQThCLEVBQUUsNkJBQTZCLEVBQUUsMEJBQTBCO1lBQzFKLFFBQVEsRUFBRSwrQkFBK0IsRUFBRSxnQ0FBZ0MsRUFBRSxzQ0FBc0M7U0FFdEgsRUFBRSxVQUFDLGNBQWMsRUFBRSxnQkFBZ0IsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxnQkFBZ0IsRUFBRSxFQUFFO1lBQzNGLElBQUksY0FBYyxFQUFFLENBQUM7WUFDckIsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ3hCLElBQUksZ0JBQWdCLEVBQUUsQ0FBQztZQUN2QixJQUFJLGdCQUFnQixFQUFFLENBQUM7WUFFdkIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO2dCQUNsQixjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3RCLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNsQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQztBQUVILFlBQVk7QUFDWixJQUFJLGVBQWUsR0FBRyxLQUFLLENBQUM7QUFDNUIsSUFBSSxjQUFjLEdBQUc7SUFDakIsSUFBSSxlQUFlO1FBQ2YsT0FBTztJQUVYLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDeEMsZUFBZSxHQUFHLElBQUksQ0FBQztBQUMzQixDQUFDLENBQUMifQ==