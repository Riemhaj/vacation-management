/// <amd-module name="ViewModels/ViewViewModel" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("ViewModels/ViewViewModel", ["require", "exports", "ViewModels/ViewModel"], function (require, exports, ViewModel) {
    "use strict";
    var ViewViewModel = /** @class */ (function (_super) {
        __extends(ViewViewModel, _super);
        function ViewViewModel(params) {
            var _this = _super.call(this, params) || this;
            _this.viewFactory = _this.paramOrDefault(params.viewFactory, null);
            _this.isBusy = _this.observableOrDefault(params.isBusy, false);
            _this.isDisposable = _this.paramOrDefault(true);
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(ViewViewModel);
            return _this;
        }
        return ViewViewModel;
    }(ViewModel));
    return ViewViewModel;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld1ZpZXdNb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlZpZXdWaWV3TW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsa0RBQWtEOzs7Ozs7Ozs7Ozs7O0lBTWxEO1FBQTRCLGlDQUFTO1FBWWpDLHVCQUFtQixNQUE4RDtZQUFqRixZQUNJLGtCQUFNLE1BQU0sQ0FBQyxTQVFoQjtZQU5HLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2pFLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDN0QsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTlDLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsYUFBYSxDQUFDLENBQUM7O1FBQ2pELENBQUM7UUFDTCxvQkFBQztJQUFELENBQUMsQUF0QkQsQ0FBNEIsU0FBUyxHQXNCcEM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9