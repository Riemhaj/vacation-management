/// <amd-module name="ViewModels/ViewModel" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("ViewModels/ViewModel", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ViewModel = /** @class */ (function (_super) {
        __extends(ViewModel, _super);
        function ViewModel(params) {
            var _this = _super.call(this) || this;
            _this.parentLocator = _this.paramOrDefault(params.parentLocator, null);
            _this.locator = _this.paramOrDefault(params.locator, null, function (value) {
                // todo erkennung von vorhandnene teilstrings
                var newlocator = null;
                if (_this.parentLocator) {
                    newlocator = _this.parentLocator;
                }
                if (value) {
                    if (newlocator) {
                        newlocator = ViewModel.mergeLocator(newlocator, value);
                    }
                    else {
                        newlocator = value;
                    }
                    return newlocator;
                }
                return null;
            });
            _this.applyPrototypeToInstance(ViewModel);
            return _this;
        }
        ViewModel.mergeLocator = function (parent, child) {
            var parents = parent.split(".");
            var children = child.split(".");
            var bestResult = -1;
            var bestResultIndex = null;
            var actualResult = 0;
            outer: for (var i = parents.length - 1; i >= 0; i--) {
                for (var j = 0; j < children.length; j++) {
                    if (children[j] != parents[(i + j)]) {
                        if (actualResult >= bestResult) {
                            bestResult = actualResult;
                            bestResultIndex = j;
                        }
                        if (parents[(i + j)] === undefined) {
                            break outer;
                        }
                        actualResult = 0;
                        break;
                    }
                    actualResult++;
                }
            }
            for (var i = bestResultIndex; i < children.length; i++) {
                parents.push(children[i]);
            }
            return parents.join(".");
        };
        return ViewModel;
    }(Class));
    return ViewModel;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld01vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiVmlld01vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDhDQUE4Qzs7Ozs7Ozs7Ozs7OztJQUs5QztRQUF3Qiw2QkFBSztRQVl6QixtQkFBbUIsTUFBMEQ7WUFBN0UsWUFDSSxpQkFBTyxTQXdCVjtZQXRCRyxLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUVyRSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsVUFBQyxLQUFLO2dCQUMzRCw2Q0FBNkM7Z0JBRTdDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxLQUFJLENBQUMsYUFBYSxFQUFFO29CQUNwQixVQUFVLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQztpQkFDbkM7Z0JBQ0QsSUFBSSxLQUFLLEVBQUU7b0JBQ1AsSUFBSSxVQUFVLEVBQUU7d0JBQ1osVUFBVSxHQUFHLFNBQVMsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO3FCQUMxRDt5QkFDSTt3QkFDRCxVQUFVLEdBQUcsS0FBSyxDQUFDO3FCQUN0QjtvQkFDRCxPQUFPLFVBQVUsQ0FBQztpQkFDckI7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLENBQUM7O1FBQzdDLENBQUM7UUFFYSxzQkFBWSxHQUExQixVQUEyQixNQUFjLEVBQUUsS0FBYTtZQUNwRCxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBRTNCLElBQUksWUFBWSxHQUFHLENBQUMsQ0FBQztZQUVyQixLQUFLLEVBQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMxQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFFdEMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ2pDLElBQUksWUFBWSxJQUFJLFVBQVUsRUFBRTs0QkFDNUIsVUFBVSxHQUFHLFlBQVksQ0FBQzs0QkFDMUIsZUFBZSxHQUFHLENBQUMsQ0FBQzt5QkFDdkI7d0JBRUQsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxTQUFTLEVBQUU7NEJBQ2hDLE1BQU0sS0FBSyxDQUFDO3lCQUNmO3dCQUVELFlBQVksR0FBRyxDQUFDLENBQUM7d0JBQ2pCLE1BQU07cUJBQ1Q7b0JBQ0QsWUFBWSxFQUFFLENBQUM7aUJBRWxCO2FBQ0o7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLGVBQWUsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDcEQsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM3QjtZQUNELE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3QixDQUFDO1FBQ0wsZ0JBQUM7SUFBRCxDQUFDLEFBM0VELENBQXdCLEtBQUssR0EyRTVCO0lBRUQsT0FBUyxTQUFTLENBQUMifQ==