/// <amd-module name="ViewModels/Context" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("ViewModels/Context", ["require", "exports", "ViewModels/ViewModel", "BindingContextMixin", "Utility/MixinHelper"], function (require, exports, ViewModel, BindingContextMixin, MixinHelper) {
    "use strict";
    var Context = /** @class */ (function (_super) {
        __extends(Context, _super);
        /* ====================================================== */
        function Context(params) {
            var _this = _super.call(this, params) || this;
            /* ==============  from BindingContextMixin============== */
            _this._registeredDisposables = [];
            /* ============================== INTERNAL ============================== */
            _this._createdItems = ko.observable({}).extend({ notify: 'always', rateLimit: { timeout: 200, method: 'notifyWhenChangesStop' } });
            _this._createdFactories = ko.observableArray([]).extend({ notify: 'always', rateLimit: { timeout: 200, method: 'notifyWhenChangesStop' } });
            /* ============================== PUBLIC ============================== */
            _this.isDisposable = _this.paramOrDefault(true);
            _this.undisposeProperties.push('viewModel');
            /* ======================== TS/JS Inheritance ========================= */
            _this.setCreatedTuple = Context.prototype.setCreatedTuple;
            _this.getCreatedTuples = Context.prototype.getCreatedTuples;
            _this.getCreatedTuple = Context.prototype.getCreatedTuple;
            _this.getCreatedGroups = Context.prototype.getCreatedGroups;
            _this.getCreatedItems = Context.prototype.getCreatedItems;
            _this.setCreatedFactory = Context.prototype.setCreatedFactory;
            _this.getCreatedFactories = Context.prototype.getCreatedFactories;
            _this.getUIFactory = Context.prototype.getUIFactory;
            _this.getUIControl = Context.prototype.getUIControl;
            return _this;
        }
        /**
         * Registriert ein erzeugtes Tuple im Context
         * @param {string} name - Der Name des Items
         * @param {FactoryTuple} tuple - Das erzeugte Control/GroupFactoryTuple
         */
        Context.prototype.setCreatedTuple = function (name, tuple) {
            if (name) {
                var createdItems = this._createdItems();
                createdItems[name] = tuple;
                this._createdItems(createdItems);
            }
        };
        /**
         * Liefert Tuples von erzeugten Items
         * @returns {[FactoryTuple]}
         */
        Context.prototype.getCreatedTuples = function (includeGroups, includeControls) {
            var result = [];
            var createdTuples = this._createdItems();
            for (var i in createdTuples) {
                var tuple = createdTuples[i];
                var type = tuple["type"]; // TODO typing for tuple is not correct, it seems
                if (type === 'GroupFactoryTuple' && includeGroups) {
                    result.push(tuple);
                }
                if (type === 'ControlFactoryTuple' && includeControls) {
                    result.push(tuple);
                }
            }
            return result;
        };
        /**
         * Liefert ein im Kontext erzeugtes Tuple
         * @param {string} name - Der Name des Items
         * @returns {FactoryTuple} - Das unter dem angegebenen Namen registrierte Tuple
         */
        Context.prototype.getCreatedTuple = function (name) {
            var createdItems = this._createdItems();
            if (name.indexOf('*') > -1) {
                var result = [];
                var regex = new RegExp('^' + name.replace('*', '.*') + '$');
                for (var i in createdItems) {
                    var matches = regex.test(i);
                    if (matches) {
                        result.push(createdItems[i]);
                    }
                }
                // Geändert: Wenn * im Bezeichner vorkommt, muss man damit rechnen und sich darauf verlassen können, dass ein Array zurückkommt
                //if (result.length > 0) {
                //    if (result.length > 1) {
                //        return result;
                //    } else {
                //        return result[0];
                //    }
                //}
                return result;
            }
            return createdItems[name];
        };
        /**
         * Liefert alle im Kontext erzeugten Group-Factory-Tuples
         */
        Context.prototype.getCreatedGroups = function () {
            return this.getCreatedTuples(true, false);
        };
        /**
         * Liefert alle im Kontext erzeugten Control-Factory-Tuples
         */
        Context.prototype.getCreatedItems = function () {
            return this.getCreatedTuples(false, true);
        };
        /**
         * Registriert eine erzeugte Factory im Kontext
         */
        Context.prototype.setCreatedFactory = function (factory) {
            this._createdFactories.push(factory);
        };
        /*
        * Liefert die vom Kontext erzeugten Factories
        */
        Context.prototype.getCreatedFactories = function () {
            return this._createdFactories();
        };
        /*
         * Liefert eine im Kontext erzeugte Factory
         */
        Context.prototype.getUIFactory = function (name) {
            var tuple = this.getCreatedTuple(name);
            if (tuple != null) {
                return tuple.factory;
            }
        };
        /**
         * Liefert ein im Kontext erzeugtes Control
         */
        Context.prototype.getUIControl = function (name) {
            var tuple = this.getCreatedTuple(name);
            if (tuple != null) {
                return tuple.control;
            }
        };
        return Context;
    }(ViewModel));
    MixinHelper.applyMixins(Context, [BindingContextMixin]);
    return Context;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udGV4dC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnRleHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNENBQTRDOzs7Ozs7Ozs7Ozs7O0lBYzVDO1FBQXNCLDJCQUFTO1FBb0IzQiw0REFBNEQ7UUFHNUQsaUJBQW1CLE1BQXdEO1lBQTNFLFlBQ0ksa0JBQU0sTUFBTSxDQUFDLFNBb0JoQjtZQTNCRCw0REFBNEQ7WUFDckQsNEJBQXNCLEdBQWUsRUFBRSxDQUFDO1lBUTNDLDRFQUE0RTtZQUM1RSxLQUFJLENBQUMsYUFBYSxHQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUF3RCxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsdUJBQXVCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDMUwsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSx1QkFBdUIsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUUzSSwwRUFBMEU7WUFDMUUsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFM0MsMEVBQTBFO1lBQzFFLEtBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUM7WUFDekQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUM7WUFDM0QsS0FBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQztZQUN6RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztZQUMzRCxLQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDO1lBQ3pELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDO1lBQzdELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDO1lBQ2pFLEtBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUM7WUFDbkQsS0FBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQzs7UUFDdkQsQ0FBQztRQUVEOzs7O1dBSUc7UUFDSSxpQ0FBZSxHQUF0QixVQUF1QixJQUFZLEVBQUUsS0FBVTtZQUMzQyxJQUFJLElBQUksRUFBRTtnQkFDTixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBRXhDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDcEM7UUFDTCxDQUFDO1FBRUQ7OztXQUdHO1FBQ0ssa0NBQWdCLEdBQXhCLFVBQXlCLGFBQWEsRUFBRSxlQUFlO1lBQ25ELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNoQixJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFFekMsS0FBSyxJQUFJLENBQUMsSUFBSSxhQUFhLEVBQUU7Z0JBQ3pCLElBQUksS0FBSyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsaURBQWlEO2dCQUUzRSxJQUFJLElBQUksS0FBSyxtQkFBbUIsSUFBSSxhQUFhLEVBQUU7b0JBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RCO2dCQUVELElBQUksSUFBSSxLQUFLLHFCQUFxQixJQUFJLGVBQWUsRUFBRTtvQkFDbkQsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDdEI7YUFDSjtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFRDs7OztXQUlHO1FBQ0ksaUNBQWUsR0FBdEIsVUFBdUIsSUFBWTtZQUMvQixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFFeEMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBQ2hCLElBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztnQkFFNUQsS0FBSyxJQUFJLENBQUMsSUFBSSxZQUFZLEVBQUU7b0JBQ3hCLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRTVCLElBQUksT0FBTyxFQUFFO3dCQUNULE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2hDO2lCQUNKO2dCQUVELCtIQUErSDtnQkFFL0gsMEJBQTBCO2dCQUMxQiw4QkFBOEI7Z0JBQzlCLHdCQUF3QjtnQkFDeEIsY0FBYztnQkFDZCwyQkFBMkI7Z0JBQzNCLE9BQU87Z0JBQ1AsR0FBRztnQkFFSCxPQUFPLE1BQU0sQ0FBQzthQUNqQjtZQUVELE9BQU8sWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFRDs7V0FFRztRQUNJLGtDQUFnQixHQUF2QjtZQUNJLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM5QyxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxpQ0FBZSxHQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QyxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxtQ0FBaUIsR0FBeEIsVUFBeUIsT0FBZ0I7WUFDckMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxDQUFDO1FBRUQ7O1VBRUU7UUFDSyxxQ0FBbUIsR0FBMUI7WUFDSSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3BDLENBQUM7UUFFRDs7V0FFRztRQUNJLDhCQUFZLEdBQW5CLFVBQStCLElBQVk7WUFDdkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV2QyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2YsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ksOEJBQVksR0FBbkIsVUFBb0IsSUFBWTtZQUM1QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXZDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDZixPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUM7YUFDeEI7UUFDTCxDQUFDO1FBQ0wsY0FBQztJQUFELENBQUMsQUF6S0QsQ0FBc0IsU0FBUyxHQXlLOUI7SUFFRCxXQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztJQUV4RCxPQUFTLE9BQU8sQ0FBQyJ9