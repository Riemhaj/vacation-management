/// <amd-module name="ViewModels/ControlViewModel" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("ViewModels/ControlViewModel", ["require", "exports", "ViewModels/ViewModel"], function (require, exports, ViewModel) {
    "use strict";
    var ControlViewModel = /** @class */ (function (_super) {
        __extends(ControlViewModel, _super);
        function ControlViewModel(params) {
            var _this = _super.call(this, params) || this;
            _this.isDisposable = _this.paramOrDefault(true);
            _this.factory = _this.paramOrDefault(params.factory, null);
            _this.isSummaryViewMode = _this.paramOrDefault(params.isSummaryViewMode, false);
            _this.tooltip = _this.paramOrDefault(params.tooltip, null);
            _this.technicalName = _this.paramOrDefault(params.technicalName, null);
            _this.isBusy = _this.observableOrDefault(params.isBusy, false);
            _this.isDisplayed = _this.observableOrDefault(params.isDisplayed, true);
            _this.isVisible = _this.observableOrDefault(params.isVisible, true);
            _this.isEnabled = _this.observableOrDefault(params.isEnabled, true);
            _this.isRequired = _this.observableOrDefault(params.isRequired, false);
            _this.isFocusable = _this.observableOrDefault(params.isFocusable, false);
            _this.invalidationText = _this.observableOrDefault(params.invalidationText, '');
            _this.isValid = _this.observableOrDefault(params.isValid, true);
            _this.animation = _this.observableOrDefault(false);
            _this.isActive = _this.observableOrDefault(params.isActive, false);
            _this.undisposeProperties.push('context');
            _this.applyPrototypeToInstance(ControlViewModel);
            return _this;
        }
        /**
         * Setzt den Focusable-State gleich dem Enabled-State
         */
        ControlViewModel.prototype.setFocusableToEnabledState = function () {
            this.isFocusable(this.isEnabled());
        };
        /**
         * Synchronisiert den Focusable-State (Slave) mit dem Enabled-State (Master).
         */
        ControlViewModel.prototype.syncFocusableWithEnabledState = function () {
            var _this = this;
            this.defineSubscription(this.isEnabled, function (isEnabled) {
                _this.setFocusableToEnabledState();
            })(this.isEnabled());
        };
        /**
         * Testet, ob alle in den Parametern übergebenen Properties im ViewModel vorhanden sind
         * Dient der frühzeitigen Erkennung von Programmierfehlern
         */
        ControlViewModel.checkProperties = function (instance, params) {
            for (var prop in params) {
                if (!(prop in instance)) {
                    throw 'Invalid Parameter Error: Der Parameter mit dem Namen "' + prop + '" ist nicht als Property im ViewModel "' + instance.type + '" enthalten';
                }
            }
        };
        return ControlViewModel;
    }(ViewModel));
    return ControlViewModel;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbFZpZXdNb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnRyb2xWaWV3TW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscURBQXFEOzs7Ozs7Ozs7Ozs7O0lBUXJEO1FBQStCLG9DQUFTO1FBNkVwQywwQkFBbUIsTUFBaUU7WUFBcEYsWUFDSSxrQkFBTSxNQUFNLENBQUMsU0FzQmhCO1lBcEJHLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFckUsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM3RCxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3RFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDbEUsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRSxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdkUsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM5RCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqRCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRWpFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFekMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLENBQUM7O1FBQ3BELENBQUM7UUFFRDs7V0FFRztRQUNJLHFEQUEwQixHQUFqQztZQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDdkMsQ0FBQztRQUVEOztXQUVHO1FBQ0ksd0RBQTZCLEdBQXBDO1lBQUEsaUJBSUM7WUFIRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFDLFNBQVM7Z0JBQzlDLEtBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ3pCLENBQUM7UUFFRDs7O1dBR0c7UUFDVyxnQ0FBZSxHQUE3QixVQUE4QixRQUEwQixFQUFFLE1BQWlFO1lBQ3ZILEtBQUssSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO2dCQUNyQixJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLEVBQUU7b0JBQ3JCLE1BQU0sd0RBQXdELEdBQUcsSUFBSSxHQUFHLHlDQUF5QyxHQUFHLFFBQVEsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO2lCQUNySjthQUNKO1FBQ0wsQ0FBQztRQUNMLHVCQUFDO0lBQUQsQ0FBQyxBQWpJRCxDQUErQixTQUFTLEdBaUl2QztJQUVELE9BQVMsZ0JBQWdCLENBQUMifQ==