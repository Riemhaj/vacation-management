/// <amd-module name="ViewModels/SummaryViewModel" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("ViewModels/SummaryViewModel", ["require", "exports", "ViewModels/ViewModel"], function (require, exports, ViewModel) {
    "use strict";
    var SummaryViewModel = /** @class */ (function (_super) {
        __extends(SummaryViewModel, _super);
        function SummaryViewModel(params) {
            var _this = _super.call(this, params) || this;
            _this.genericObject = _this.paramOrDefault(params.genericObject, null);
            _this.isBusy = _this.observableOrDefault(params.isBusy, false);
            _this.isDisposable = _this.paramOrDefault(true);
            return _this;
        }
        return SummaryViewModel;
    }(ViewModel));
    return SummaryViewModel;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VtbWFyeVZpZXdNb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlN1bW1hcnlWaWV3TW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscURBQXFEOzs7Ozs7Ozs7Ozs7O0lBTXJEO1FBQStCLG9DQUFTO1FBZXBDLDBCQUFZLE1BQWlFO1lBQTdFLFlBQ0ksa0JBQU0sTUFBTSxDQUFDLFNBT2hCO1lBTEcsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFckUsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUU3RCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7O1FBQ2xELENBQUM7UUFDTCx1QkFBQztJQUFELENBQUMsQUF4QkQsQ0FBK0IsU0FBUyxHQXdCdkM7SUFFRCxPQUFTLGdCQUFnQixDQUFDIn0=