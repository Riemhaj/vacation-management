/// <amd-module name="SummaryViews/SummaryView" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("SummaryViews/SummaryView", ["require", "exports", "BindingContext", "Utility/TypeInfo", "SummaryViews/SummaryViewTuple", "Management/RADApplication", "Management/ResourceManager"], function (require, exports, BindingContext, TypeInfo, SummaryViewTuple, RADApplication, ResourceManager) {
    "use strict";
    var SummaryView = /** @class */ (function (_super) {
        __extends(SummaryView, _super);
        function SummaryView() {
            var _this = _super.call(this) || this;
            _this.isDisposable = true;
            _this.disposeProperties = _this.paramOrDefault(true);
            _this.typeName = TypeInfo.getType(self);
            _this.identifier = _.uniqueId(_this.typeName + '_');
            _this.applyPrototypeToInstance(SummaryView);
            return _this;
        }
        SummaryView.prototype.getSummaryView = function () {
            throw new Error('Die Funktion "getSummaryView" wurde nicht implementiert');
        };
        /**
         * Erzeugt das SummaryViewTuple und die SummaryView
         */
        SummaryView.prototype.getTuple = function () {
            return new SummaryViewTuple(this);
        };
        /**
         * Wendet Bindings auf einem HTML-String an
         */
        SummaryView.prototype.applyViewModel = function (html, viewModel) {
            if (html) {
                var element = $(html);
                ko.applyBindings({
                    uiElement: this,
                    viewModel: viewModel,
                    RADApplication: RADApplication,
                    ResourceManager: ResourceManager
                }, element[0]);
                return element;
            }
        };
        /**
         * Wendet Bindings auf einem HTML-String an
         */
        SummaryView.prototype.applyBindings = function (element, bindings) {
            ko.applyBindingsToNode(element[0], $.extend({ uiElement: this }, bindings));
        };
        return SummaryView;
    }(BindingContext));
    return SummaryView;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VtbWFyeVZpZXcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJTdW1tYXJ5Vmlldy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxrREFBa0Q7Ozs7Ozs7Ozs7Ozs7SUFTbEQ7UUFBMEIsK0JBQWM7UUFZcEM7WUFBQSxZQUNJLGlCQUFPLFNBUVY7WUFORyxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFFbEQsS0FBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsQ0FBQyxDQUFDOztRQUMvQyxDQUFDO1FBRU0sb0NBQWMsR0FBckI7WUFDSSxNQUFNLElBQUksS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7UUFDL0UsQ0FBQztRQUVEOztXQUVHO1FBQ0ksOEJBQVEsR0FBZjtZQUNJLE9BQU8sSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxvQ0FBYyxHQUFyQixVQUFzQixJQUFZLEVBQUUsU0FBb0I7WUFDcEQsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QixFQUFFLENBQUMsYUFBYSxDQUFDO29CQUNiLFNBQVMsRUFBRSxJQUFJO29CQUNmLFNBQVMsRUFBRSxTQUFTO29CQUNwQixjQUFjLEVBQUUsY0FBYztvQkFDOUIsZUFBZSxFQUFFLGVBQWU7aUJBQ25DLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWYsT0FBTyxPQUFPLENBQUM7YUFDbEI7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxtQ0FBYSxHQUFwQixVQUFxQixPQUE0QixFQUFFLFFBQWE7WUFDNUQsRUFBRSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDaEYsQ0FBQztRQUVMLGtCQUFDO0lBQUQsQ0FBQyxBQTFERCxDQUEwQixjQUFjLEdBMER2QztJQUVELE9BQVMsV0FBVyxDQUFDIn0=