/// <amd-module name="SummaryViews/SummaryViewTuple" />
define("SummaryViews/SummaryViewTuple", ["require", "exports"], function (require, exports) {
    "use strict";
    var SummaryViewTuple = /** @class */ (function () {
        function SummaryViewTuple(summaryView) {
            this.summaryView = summaryView.getSummaryView();
        }
        return SummaryViewTuple;
    }());
    return SummaryViewTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VtbWFyeVZpZXdUdXBsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlN1bW1hcnlWaWV3VHVwbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdURBQXVEOzs7SUFJdkQ7UUFHSSwwQkFBWSxXQUF3QjtZQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwRCxDQUFDO1FBRUwsdUJBQUM7SUFBRCxDQUFDLEFBUEQsSUFPQztJQUVELE9BQVMsZ0JBQWdCLENBQUMifQ==