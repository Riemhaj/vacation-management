///<amd-module name="InheritanceHelper" />
define("InheritanceHelper", ["require", "exports"], function (require, exports) {
    "use strict";
    /**
     * Liefert Methoden, die bei der Vererbung von reinen JS Modulen an in TS entwickelten Modulen hilft.
     */
    var InheritanceHelper = /** @class */ (function () {
        function InheritanceHelper() {
        }
        /**
         * Durch den Aufruf nach dem super-Konstruktor können Methoden, die in TS über prototype überschrieben wurden, wieder in die Instanz geschrieben werden. Andernfalls werden diese bei ".call()" von der Basisimplementierung überschrieben.
         * @param CLASS Die erbende Klasse
         * @param INSTANCE Die gerade erstellte Instanz der erbenden Klasse (this)
         * @param PARENT Die Basisklasse von der geerbt wird
         * @param PARENT_ARGUMENTS Argumente die zum Erstellen der Basisinstanz notwendig sind.
         * * @returns a reference object of the base-class. this object can be used to call methods of the base-class. do not attempt to use variables of the base class.
         */
        InheritanceHelper.ReloadOverwrittenImplementations = function (CLASS, INSTANCE, PARENT, PARENT_ARGUMENTS) {
            console.warn("Deprecated use of InheritanceHelper, call \"super\" instead");
            // Zum setzen der Basisimplementierung in Prototype ist es notwendig eine Instanz der Basisklasse zu erstellen.
            var parent_instance = null;
            var parentRef = {};
            if (PARENT_ARGUMENTS) {
                parent_instance = {};
                try {
                    PARENT.apply(parent_instance, PARENT_ARGUMENTS);
                }
                catch (e) {
                    console.log("Fehler beim Erstellen der Basisinstanz");
                    parent_instance = null;
                }
            }
            // Überschriebene Methoden wiederholen
            for (var key in CLASS.prototype) {
                if (key != "constructor") {
                    if (parent_instance && parent_instance[key] != undefined && PARENT.prototype[key] == undefined) {
                        // Alte Implementierung in den Prototype des Parents schreiben. Ermöglicht den Aufruf der Basisimplementierung!
                        parentRef[key] = INSTANCE[key];
                    }
                    // Überschriebene Implementierung erneut in die Instanz schreiben und somit die durch .call() geerbe Basisimplementierung wieder überschreiben
                    INSTANCE[key] = CLASS.prototype[key];
                }
            }
            // ggf. die Basisinstanz wieder löschen
            if (parent_instance) {
                if ($.isFunction(parent_instance.dispose)) {
                    var instance_disp_1 = INSTANCE.dispose;
                    INSTANCE.dispose = function () {
                        if (instance_disp_1)
                            instance_disp_1.apply(INSTANCE);
                        if (parent_instance)
                            parent_instance.dispose();
                        parent_instance = undefined;
                    };
                }
                else {
                    parent_instance = undefined;
                }
            }
            return parentRef;
        };
        return InheritanceHelper;
    }());
    return InheritanceHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5oZXJpdGFuY2VIZWxwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJJbmhlcml0YW5jZUhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwwQ0FBMEM7OztJQUUxQzs7T0FFRztJQUNIO1FBRUk7UUFDQSxDQUFDO1FBRUQ7Ozs7Ozs7V0FPRztRQUNXLGtEQUFnQyxHQUE5QyxVQUFrRCxLQUFVLEVBQUUsUUFBYSxFQUFFLE1BQVcsRUFBRSxnQkFBc0I7WUFDNUcsT0FBTyxDQUFDLElBQUksQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO1lBQzVFLCtHQUErRztZQUMvRyxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksZ0JBQWdCLEVBQUU7Z0JBQ2xCLGVBQWUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUk7b0JBQ0EsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztpQkFDbkQ7Z0JBQ0QsT0FBTyxDQUFDLEVBQUU7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO29CQUN0RCxlQUFlLEdBQUcsSUFBSSxDQUFDO2lCQUMxQjthQUNKO1lBRUQsc0NBQXNDO1lBQ3RDLEtBQUssSUFBSSxHQUFHLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDN0IsSUFBSSxHQUFHLElBQUksYUFBYSxFQUFFO29CQUV0QixJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksU0FBUyxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksU0FBUyxFQUFFO3dCQUM1RiwrR0FBK0c7d0JBQy9HLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ2xDO29CQUVELDhJQUE4STtvQkFDOUksUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3hDO2FBQ0o7WUFHRCx1Q0FBdUM7WUFDdkMsSUFBSSxlQUFlLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ3ZDLElBQUksZUFBYSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7b0JBQ3JDLFFBQVEsQ0FBQyxPQUFPLEdBQUc7d0JBQ2YsSUFBSSxlQUFhOzRCQUNiLGVBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ2xDLElBQUksZUFBZTs0QkFDZixlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQzlCLGVBQWUsR0FBRyxTQUFTLENBQUM7b0JBQ2hDLENBQUMsQ0FBQztpQkFDTDtxQkFDSTtvQkFDRCxlQUFlLEdBQUcsU0FBUyxDQUFDO2lCQUMvQjthQUNKO1lBQ0QsT0FBTyxTQUFjLENBQUM7UUFDMUIsQ0FBQztRQUVMLHdCQUFDO0lBQUQsQ0FBQyxBQS9ERCxJQStEQztJQUVELE9BQVMsaUJBQWlCLENBQUMifQ==