/// <amd-module name="Infrastructure/CustomBindings/CheckableBinding" />
define("Infrastructure/CustomBindings/CheckableBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var CheckableBinding = /** @class */ (function () {
        function CheckableBinding() {
        }
        CheckableBinding.prototype.initBinding = function () {
            ko.bindingHandlers.checkable = {
                init: function (node, valueAccessor) {
                    var element = $(node);
                    var checkable = ko.unwrap(valueAccessor());
                    if (!checkable)
                        element.find('.k-checkbox-label').first().css('display', 'none');
                    else
                        element.find('.k-checkbox-label').first().css('display', '');
                }
            };
        };
        return CheckableBinding;
    }());
    return CheckableBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2hlY2thYmxlQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNoZWNrYWJsZUJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsd0VBQXdFOzs7SUFFeEU7UUFBQTtRQWVBLENBQUM7UUFiVSxzQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHO2dCQUMzQixJQUFJLEVBQUUsVUFBQyxJQUFpQixFQUFFLGFBQXdCO29CQUM5QyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztvQkFFM0MsSUFBSSxDQUFDLFNBQVM7d0JBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7O3dCQUVqRSxPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckUsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsdUJBQUM7SUFBRCxDQUFDLEFBZkQsSUFlQztJQUVELE9BQVMsZ0JBQWdCLENBQUMifQ==