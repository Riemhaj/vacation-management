/// <amd-module name="Infrastructure/CustomBindings/ControlsBinding" />
define("Infrastructure/CustomBindings/ControlsBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ControlsBinding = /** @class */ (function () {
        function ControlsBinding() {
        }
        ControlsBinding.prototype.initBinding = function () {
            ko.bindingHandlers.controls = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var controls = ko.unwrap(value);
                    if (!controls) {
                        return;
                    }
                    // Falls das Element noch keine ID besitzt, wird diese anhand des darüberliegenden Controls erzeugt
                    if (!controls.attr('id')) {
                        controls.attr('id', _.uniqueId());
                    }
                    element.removeAttr('aria-controls');
                    element.attr('aria-controls', controls.attr('id'));
                }
            };
        };
        return ControlsBinding;
    }());
    return ControlsBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbHNCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ29udHJvbHNCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHVFQUF1RTs7O0lBRXZFO1FBQUE7UUF1QkEsQ0FBQztRQXJCVSxxQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHO2dCQUMxQixNQUFNLEVBQUUsVUFBQyxJQUFpQixFQUFFLGFBQXdCO29CQUNoRCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM1QixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVoQyxJQUFJLENBQUMsUUFBUSxFQUFFO3dCQUNYLE9BQU87cUJBQ1Y7b0JBRUQsbUdBQW1HO29CQUNuRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdEIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7cUJBQ3BDO29CQUVELE9BQU8sQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3BDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdkQsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsc0JBQUM7SUFBRCxDQUFDLEFBdkJELElBdUJDO0lBRUQsT0FBUyxlQUFlLENBQUMifQ==