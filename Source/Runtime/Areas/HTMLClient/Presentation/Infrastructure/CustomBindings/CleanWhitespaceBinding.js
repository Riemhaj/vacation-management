/// <amd-module name="Infrastructure/CustomBindings/CleanWhitespaceBinding" />
define("Infrastructure/CustomBindings/CleanWhitespaceBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var CleanWhitespaceBinding = /** @class */ (function () {
        function CleanWhitespaceBinding() {
        }
        CleanWhitespaceBinding.prototype.initBinding = function () {
            ko.bindingHandlers.cleanWhitespace = {
                init: function (node) {
                    $(node).contents().filter(function (index, htmlElement) {
                        return (htmlElement.nodeType == 3 && !/\S/.test(htmlElement.nodeValue));
                    }).remove();
                }
            };
        };
        return CleanWhitespaceBinding;
    }());
    return CleanWhitespaceBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2xlYW5XaGl0ZXNwYWNlQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNsZWFuV2hpdGVzcGFjZUJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsOEVBQThFOzs7SUFFOUU7UUFBQTtRQVdBLENBQUM7UUFUVSw0Q0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsZUFBZSxHQUFHO2dCQUNqQyxJQUFJLEVBQUUsVUFBQyxJQUFpQjtvQkFDcEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFDLEtBQUssRUFBRSxXQUFXO3dCQUN6QyxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsNkJBQUM7SUFBRCxDQUFDLEFBWEQsSUFXQztJQUVELE9BQVMsc0JBQXNCLENBQUMifQ==