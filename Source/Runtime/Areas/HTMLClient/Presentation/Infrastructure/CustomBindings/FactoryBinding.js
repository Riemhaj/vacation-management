/// <amd-module name="Infrastructure/CustomBindings/FactoryBinding" />
define("Infrastructure/CustomBindings/FactoryBinding", ["require", "exports", "Factories/Factory", "Infrastructure/CustomBindingUtilities"], function (require, exports, Factory, Utils) {
    "use strict";
    var FactoryBinding = /** @class */ (function () {
        function FactoryBinding() {
        }
        FactoryBinding.prototype.initBinding = function () {
            ko.bindingHandlers.factory = {
                init: function (element, valueAccessor, bindings, view, bindingContext) {
                    var options = valueAccessor();
                    var wrapper = $(element);
                    var factory = Factory.createInstance(options.uiGenerator, options, 'DummyControl');
                    if (factory == null) {
                        return;
                    }
                    Utils.registerDisposable(factory, bindings, bindingContext);
                    factory.async(function () {
                        var tuple = factory.getTuple();
                        wrapper.wrapInner(tuple.control);
                        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                            if (!factory.isDisposed && !factory.__isDisposing) {
                                factory.dispose();
                            }
                            factory = null;
                        });
                    });
                }
            };
        };
        return FactoryBinding;
    }());
    return FactoryBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmFjdG9yeUJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJGYWN0b3J5QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQWV0RTtRQUFBO1FBNkJBLENBQUM7UUEzQlUsb0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsSUFBSSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUE0QixFQUFFLFFBQXFDLEVBQUUsSUFBUyxFQUFFLGNBQXNDO29CQUMvSSxJQUFJLE9BQU8sR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUV6QixJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsT0FBTyxFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUNuRixJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7d0JBQ2pCLE9BQU87cUJBQ1Y7b0JBRUQsS0FBSyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7b0JBRTVELE9BQU8sQ0FBQyxLQUFLLENBQUM7d0JBQ1YsSUFBSSxLQUFLLEdBQUksT0FBMkIsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDcEQsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBRWpDLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRTs0QkFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksQ0FBRSxPQUFlLENBQUMsYUFBYSxFQUFFO2dDQUN4RCxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7NkJBQ3JCOzRCQUNELE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBQ25CLENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHFCQUFDO0lBQUQsQ0FBQyxBQTdCRCxJQTZCQztJQUVELE9BQVMsY0FBYyxDQUFDIn0=