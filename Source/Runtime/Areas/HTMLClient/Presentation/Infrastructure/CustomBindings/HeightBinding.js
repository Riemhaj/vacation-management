/// <amd-module name="Infrastructure/CustomBindings/HeightBinding" />
define("Infrastructure/CustomBindings/HeightBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var HeightBinding = /** @class */ (function () {
        function HeightBinding() {
        }
        HeightBinding.prototype.initBinding = function () {
            ko.bindingHandlers.height = {
                update: function (element, valueAccessor) {
                    var height = ko.unwrap(valueAccessor());
                    $(element).height(height);
                }
            };
        };
        return HeightBinding;
    }());
    return HeightBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGVpZ2h0QmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkhlaWdodEJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFFckU7UUFBQTtRQVdBLENBQUM7UUFUVSxtQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHO2dCQUN4QixNQUFNLEVBQUUsVUFBQyxPQUFvQixFQUFFLGFBQXdCO29CQUNuRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBRXhDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzlCLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLG9CQUFDO0lBQUQsQ0FBQyxBQVhELElBV0M7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9