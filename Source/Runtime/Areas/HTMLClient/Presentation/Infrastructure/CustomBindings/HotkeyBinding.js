/// <amd-module name="Infrastructure/CustomBindings/HotkeyBinding" />
define("Infrastructure/CustomBindings/HotkeyBinding", ["require", "exports", "Management/NavigationService"], function (require, exports, NavigationService) {
    "use strict";
    var HotkeyBinding = /** @class */ (function () {
        function HotkeyBinding() {
        }
        HotkeyBinding.prototype.initBinding = function () {
            ko.bindingHandlers.hotkey = {
                init: function (node, valueAccessor) {
                    var value = valueAccessor();
                    var shortcut = value.shortcut || null;
                    var handler = value.handler || null;
                    var isGlobal = value.isGlobal !== false;
                    var element = $(node);
                    if (shortcut == null || handler == null) {
                        return;
                    }
                    if (typeof shortcut == "string") {
                        shortcut = [shortcut];
                    }
                    var mousetrap = isGlobal ? Mousetrap : Mousetrap(node);
                    var isolatedViewFactoryCount = NavigationService.getModalViewFactories();
                    mousetrap.bind(shortcut, function (e) {
                        if (value.suspendOnModal !== true || isolatedViewFactoryCount == NavigationService.getModalViewFactories()) {
                            var ret = handler(e);
                        }
                        if (ret === false || ret === true) {
                            return ret;
                        }
                        return false;
                    });
                    ko.utils.domNodeDisposal.addDisposeCallback(node, function () {
                        if (mousetrap != null) {
                            mousetrap.unbind(shortcut);
                            mousetrap = null;
                        }
                    });
                }
            };
        };
        HotkeyBinding.addHotkey = function (node, shortcut, handler) {
            ko.applyBindingsToNode(node, {
                hotkey: {
                    shortcut: shortcut,
                    handler: handler,
                    isGlobal: false
                }
            });
        };
        return HotkeyBinding;
    }());
    return HotkeyBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSG90a2V5QmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkhvdGtleUJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFJckU7UUFBQTtRQW9EQSxDQUFDO1FBbERVLG1DQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUc7Z0JBQ3hCLElBQUksRUFBRSxVQUFDLElBQWlCLEVBQUUsYUFBd0g7b0JBQzlJLElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM1QixJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztvQkFDdEMsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUM7b0JBQ3BDLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDO29CQUN4QyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRXRCLElBQUksUUFBUSxJQUFJLElBQUksSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO3dCQUNyQyxPQUFPO3FCQUNWO29CQUVELElBQUksT0FBTyxRQUFRLElBQUksUUFBUSxFQUFFO3dCQUM3QixRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDekI7b0JBRUQsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFdkQsSUFBSSx3QkFBd0IsR0FBRyxpQkFBaUIsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO29CQUV6RSxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLENBQXdCO3dCQUM5QyxJQUFJLEtBQUssQ0FBQyxjQUFjLEtBQUssSUFBSSxJQUFJLHdCQUF3QixJQUFJLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLEVBQUU7NEJBQ3hHLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDeEI7d0JBQ0QsSUFBSSxHQUFHLEtBQUssS0FBSyxJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7NEJBQy9CLE9BQU8sR0FBRyxDQUFDO3lCQUNkO3dCQUNELE9BQU8sS0FBSyxDQUFDO29CQUNqQixDQUFDLENBQUMsQ0FBQztvQkFFSCxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUU7d0JBQzlDLElBQUksU0FBUyxJQUFJLElBQUksRUFBRTs0QkFDbkIsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDM0IsU0FBUyxHQUFHLElBQUksQ0FBQzt5QkFDcEI7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBRWEsdUJBQVMsR0FBRyxVQUFDLElBQWlCLEVBQUUsUUFBZ0IsRUFBRSxPQUFpQjtZQUM3RSxFQUFFLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO2dCQUN6QixNQUFNLEVBQUU7b0JBQ0osUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLE9BQU8sRUFBRSxPQUFPO29CQUNoQixRQUFRLEVBQUUsS0FBSztpQkFDbEI7YUFDSixDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFDTixvQkFBQztLQUFBLEFBcERELElBb0RDO0lBRUQsT0FBUyxhQUFhLENBQUMifQ==