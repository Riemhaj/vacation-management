/// <amd-module name="Infrastructure/CustomBindings/HighlightTextBinding" />
define("Infrastructure/CustomBindings/HighlightTextBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var HighlightTextBinding = /** @class */ (function () {
        function HighlightTextBinding() {
        }
        HighlightTextBinding.prototype.initBinding = function () {
            ko.bindingHandlers.highlightText = {
                update: function (element, valueAccessor) {
                    var value = valueAccessor();
                    var text = value.text;
                    if (ko.isObservable(text)) {
                        text = text();
                    }
                    var words = value.keywords.split(" ");
                    if (value.keywords.length > 0) {
                        // entferne HTML-Tags
                        text = $("<div>" + text + "</div>").text();
                        var span = $('<span>' + text + '</span>');
                        words.sort(function (a, b) {
                            return b.length - a.length; // ASC -> a - b; DESC -> b - a
                        });
                        for (var i in words) {
                            var word = words[i];
                            $(span).highlight(word);
                        }
                        $(element).html($(span).html());
                    }
                    else {
                        $(element).text(text);
                    }
                }
            };
        };
        return HighlightTextBinding;
    }());
    return HighlightTextBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGlnaGxpZ2h0VGV4dEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJIaWdobGlnaHRUZXh0QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw0RUFBNEU7OztJQUU1RTtRQUFBO1FBaUNBLENBQUM7UUEvQlUsMENBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRztnQkFDL0IsTUFBTSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDbkQsSUFBSSxLQUFLLEdBQUcsYUFBYSxFQUFFLENBQUM7b0JBQzVCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7b0JBQ3RCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdkIsSUFBSSxHQUFHLElBQUksRUFBRSxDQUFDO3FCQUNqQjtvQkFDRCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFFdEMsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzNCLHFCQUFxQjt3QkFDckIsSUFBSSxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLFFBQVEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMzQyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksR0FBRyxTQUFTLENBQUMsQ0FBQzt3QkFFMUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDOzRCQUNaLE9BQU8sQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsOEJBQThCO3dCQUM5RCxDQUFDLENBQUMsQ0FBQzt3QkFFSCxLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRTs0QkFDakIsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNuQixDQUFDLENBQUMsSUFBSSxDQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNwQzt3QkFDRCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO3FCQUNuQzt5QkFDSTt3QkFDRCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUN6QjtnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCwyQkFBQztJQUFELENBQUMsQUFqQ0QsSUFpQ0M7SUFFRCxPQUFTLG9CQUFvQixDQUFDIn0=