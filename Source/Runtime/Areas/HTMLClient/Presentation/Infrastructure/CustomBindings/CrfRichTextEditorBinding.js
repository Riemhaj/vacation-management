/// <amd-module name="Infrastructure/CustomBindings/CrfRichTextEditorBinding" />
define("Infrastructure/CustomBindings/CrfRichTextEditorBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var CrfRichTextEditorBinding = /** @class */ (function () {
        function CrfRichTextEditorBinding() {
        }
        CrfRichTextEditorBinding.prototype.initBinding = function () {
            ko.bindingHandlers.cfrichtexteditor = {
                init: function (node, valueAccessor) {
                    var element = $(node);
                    var v = valueAccessor();
                    var isReadonly = v.readonly;
                    var value = v.value;
                    var language = v.language;
                    var widget = v.widget;
                    var subscriptions = [];
                    var interval = null;
                    var timeout = null;
                    var clearAll = function () {
                        clearInterval(interval);
                        clearTimeout(timeout);
                    };
                    var tryToInitEditor = function () {
                        if (document.body.contains(node)) {
                            clearAll();
                            var editor_1 = new CF1.RichTextEditor(node, { Value: value(), Language: language(), ReadOnly: isReadonly() });
                            $(node).on('TextChanged', function (ev, newVal) {
                                value(newVal);
                            });
                            if (value && $.isFunction(value.subscribe)) {
                                subscriptions.push(value.subscribe(function (newvalue) {
                                    editor_1.SetValue(newvalue);
                                }));
                            }
                            if (language && $.isFunction(language.subscribe)) {
                                subscriptions.push(language.subscribe(function (newlang) {
                                    editor_1.SetLanguage(newlang);
                                }));
                            }
                            if (isReadonly && $.isFunction(isReadonly.subscribe)) {
                                subscriptions.push(isReadonly.subscribe(function (isReadonly) {
                                    editor_1.SetReadOnly(isReadonly);
                                }));
                            }
                            widget(editor_1);
                        }
                    };
                    interval = setInterval(tryToInitEditor, 300);
                    setTimeout(function () {
                        clearAll();
                    }, 5000);
                    ko.utils.domNodeDisposal.addDisposeCallback(node, function () {
                        for (var i in subscriptions) {
                            subscriptions[i].dispose();
                            subscriptions[i] = null;
                        }
                        subscriptions = [];
                        clearAll();
                    });
                }
            };
        };
        return CrfRichTextEditorBinding;
    }());
    return CrfRichTextEditorBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3JmUmljaFRleHRFZGl0b3JCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ3JmUmljaFRleHRFZGl0b3JCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdGQUFnRjs7O0lBSWhGO1FBQUE7UUFzRUEsQ0FBQztRQXBFVSw4Q0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEdBQUc7Z0JBQ2xDLElBQUksRUFBRSxVQUFDLElBQUksRUFBRSxhQUFhO29CQUN0QixJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUN4QixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDO29CQUM1QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO29CQUNwQixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDO29CQUMxQixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO29CQUN0QixJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDcEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNuQixJQUFJLFFBQVEsR0FBRzt3QkFDWCxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3hCLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUIsQ0FBQyxDQUFBO29CQUdELElBQUksZUFBZSxHQUFHO3dCQUNsQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUM5QixRQUFRLEVBQUUsQ0FBQzs0QkFDWCxJQUFJLFFBQU0sR0FBRyxJQUFJLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDOzRCQUU1RyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxVQUFDLEVBQUUsRUFBRSxNQUFNO2dDQUNqQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ2xCLENBQUMsQ0FBQyxDQUFDOzRCQUVILElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dDQUN4QyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBQyxRQUFRO29DQUN4QyxRQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dDQUM5QixDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNQOzRCQUVELElBQUksUUFBUSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dDQUM5QyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQyxPQUFPO29DQUMxQyxRQUFNLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dDQUNoQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNQOzRCQUVELElBQUksVUFBVSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dDQUNsRCxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQyxVQUFVO29DQUMvQyxRQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dDQUNuQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNQOzRCQUdELE1BQU0sQ0FBQyxRQUFNLENBQUMsQ0FBQzt5QkFDbEI7b0JBQ0wsQ0FBQyxDQUFBO29CQUVELFFBQVEsR0FBRyxXQUFXLENBQUMsZUFBZSxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUM3QyxVQUFVLENBQUM7d0JBQ1AsUUFBUSxFQUFFLENBQUM7b0JBQ2YsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUdULEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRTt3QkFDOUMsS0FBSyxJQUFJLENBQUMsSUFBSSxhQUFhLEVBQUU7NEJBQ3pCLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzs0QkFDM0IsYUFBYSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQzt5QkFDM0I7d0JBQ0QsYUFBYSxHQUFHLEVBQUUsQ0FBQzt3QkFFbkIsUUFBUSxFQUFFLENBQUM7b0JBQ2YsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsK0JBQUM7SUFBRCxDQUFDLEFBdEVELElBc0VDO0lBRUQsT0FBUyx3QkFBd0IsQ0FBQyJ9