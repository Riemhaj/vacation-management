/// <amd-module name="Infrastructure/CustomBindings/OptionsLandschaftsGroupsBinding" />
define("Infrastructure/CustomBindings/OptionsLandschaftsGroupsBinding", ["require", "exports", "Management/ComponentManager"], function (require, exports, ComponentManager) {
    "use strict";
    var OptionsLandschaftsGroupsBinding = /** @class */ (function () {
        function OptionsLandschaftsGroupsBinding() {
        }
        OptionsLandschaftsGroupsBinding.prototype.initBinding = function () {
            ko.bindingHandlers.optionsLandschaftGroups = {
                update: function (node, valueAccessor) {
                    var OptionsLandschaftGroup = ComponentManager.getControl('OptionsLandschaftGroup');
                    var optionsLandschaft = $(node);
                    var options = valueAccessor();
                    var groups = options.groups();
                    if (options.parent.groups != null) {
                        for (var i = 0; i < options.parent.groups.length; i++) {
                            options.parent.groups[i].dispose();
                            options.parent.groups[i] = null;
                        }
                    }
                    optionsLandschaft.empty();
                    options.parent.groups = [];
                    for (var i = 0; i < groups.length; i++) {
                        var optionsLandschaftGroup = new OptionsLandschaftGroup(groups[i], $('<div>').appendTo(optionsLandschaft));
                        var tuple = optionsLandschaftGroup.getTuple();
                        options.parent.groups.push(optionsLandschaftGroup);
                    }
                }
            };
        };
        return OptionsLandschaftsGroupsBinding;
    }());
    return OptionsLandschaftsGroupsBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3B0aW9uc0xhbmRzY2hhZnRzR3JvdXBzQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk9wdGlvbnNMYW5kc2NoYWZ0c0dyb3Vwc0JpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdUZBQXVGOzs7SUFLdkY7UUFBQTtRQThCQSxDQUFDO1FBNUJVLHFEQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyx1QkFBdUIsR0FBRztnQkFDekMsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBSSxzQkFBc0IsR0FBRyxnQkFBZ0IsQ0FBQyxVQUFVLENBQWlDLHdCQUF3QixDQUFDLENBQUM7b0JBRW5ILElBQUksaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNoQyxJQUFJLE9BQU8sR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUU5QixJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTt3QkFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDbkQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7NEJBQ25DLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQzt5QkFDbkM7cUJBQ0o7b0JBRUQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzFCLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztvQkFFM0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ3BDLElBQUksc0JBQXNCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7d0JBQzNHLElBQUksS0FBSyxHQUFHLHNCQUFzQixDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUU5QyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztxQkFDdEQ7Z0JBQ0wsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsc0NBQUM7SUFBRCxDQUFDLEFBOUJELElBOEJDO0lBRUQsT0FBUywrQkFBK0IsQ0FBQyJ9