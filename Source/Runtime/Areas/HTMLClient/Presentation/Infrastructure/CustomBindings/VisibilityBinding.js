/// <amd-module name="Infrastructure/CustomBindings/VisibilityBinding" />
define("Infrastructure/CustomBindings/VisibilityBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var VisibilityBinding = /** @class */ (function () {
        function VisibilityBinding() {
        }
        VisibilityBinding.prototype.initBinding = function () {
            ko.bindingHandlers.visibility = {
                update: function (element, valueAccessor) {
                    var value = ko.utils.unwrapObservable(valueAccessor());
                    var isCurrentlyVisible = !(element.style.visibility == 'hidden');
                    if (value && !isCurrentlyVisible) {
                        element.style.visibility = 'visible';
                        element.style.opacity = '1';
                    }
                    else if ((!value) && isCurrentlyVisible) {
                        element.style.visibility = 'hidden';
                        element.style.opacity = '0';
                    }
                }
            };
        };
        return VisibilityBinding;
    }());
    return VisibilityBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlzaWJpbGl0eUJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJWaXNpYmlsaXR5QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx5RUFBeUU7OztJQUV6RTtRQUFBO1FBa0JBLENBQUM7UUFoQlUsdUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRztnQkFDNUIsTUFBTSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDbkQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUN2RCxJQUFJLGtCQUFrQixHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsQ0FBQztvQkFFakUsSUFBSSxLQUFLLElBQUksQ0FBQyxrQkFBa0IsRUFBRTt3QkFDOUIsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO3dCQUNyQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7cUJBQy9CO3lCQUFNLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLGtCQUFrQixFQUFFO3dCQUN2QyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7d0JBQ3BDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztxQkFDL0I7Z0JBQ0wsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsd0JBQUM7SUFBRCxDQUFDLEFBbEJELElBa0JDO0lBRUQsT0FBUyxpQkFBaUIsQ0FBQyJ9