/// <amd-module name="Infrastructure/CustomBindings/CallFktBinding" />
define("Infrastructure/CustomBindings/CallFktBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var CallFktBinding = /** @class */ (function () {
        function CallFktBinding() {
        }
        CallFktBinding.prototype.initBinding = function () {
            ko.bindingHandlers.callFkt = {
                init: function (element, valueAccessor) {
                    var value = valueAccessor();
                    if ($.isFunction(value)) {
                        value();
                    }
                }
            };
        };
        return CallFktBinding;
    }());
    return CallFktBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FsbEZrdEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDYWxsRmt0QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQUV0RTtRQUFBO1FBYUEsQ0FBQztRQVhVLG9DQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUc7Z0JBQ3pCLElBQUksRUFBRSxVQUFDLE9BQW9CLEVBQUUsYUFBMkI7b0JBQ3BELElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUU1QixJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ3JCLEtBQUssRUFBRSxDQUFDO3FCQUNYO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHFCQUFDO0lBQUQsQ0FBQyxBQWJELElBYUM7SUFFRCxPQUFTLGNBQWMsQ0FBQyJ9