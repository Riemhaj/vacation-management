/// <amd-module name="Infrastructure/CustomBindings/InteractBinding" />
define("Infrastructure/CustomBindings/InteractBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var InteractBinding = /** @class */ (function () {
        function InteractBinding() {
        }
        InteractBinding.prototype.initBinding = function () {
            ko.bindingHandlers.interact = {
                update: function (element, valueAccessor) {
                    var interactable = ko.unwrap(valueAccessor());
                    var overlay = $(element).find('.interact-overlay');
                    if (interactable === false) {
                        if (overlay.length === 0) {
                            overlay = $('<div class="interact-overlay">').appendTo($(element));
                        }
                        $(element).css('position', 'relative');
                    }
                    else {
                        overlay.remove();
                    }
                }
            };
        };
        return InteractBinding;
    }());
    return InteractBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW50ZXJhY3RCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSW50ZXJhY3RCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHVFQUF1RTs7O0lBRXZFO1FBQUE7UUFvQkEsQ0FBQztRQWxCVSxxQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHO2dCQUMxQixNQUFNLEVBQUUsVUFBQyxPQUFvQixFQUFFLGFBQXdCO29CQUNuRCxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQzlDLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFFbkQsSUFBSSxZQUFZLEtBQUssS0FBSyxFQUFFO3dCQUN4QixJQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOzRCQUN0QixPQUFPLEdBQUcsQ0FBQyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3lCQUN0RTt3QkFFRCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztxQkFDMUM7eUJBQU07d0JBQ0gsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO3FCQUNwQjtnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCxzQkFBQztJQUFELENBQUMsQUFwQkQsSUFvQkM7SUFFRCxPQUFTLGVBQWUsQ0FBQyJ9