/// <amd-module name="Infrastructure/CustomBindings/HighlightTextArrayBinding" />
define("Infrastructure/CustomBindings/HighlightTextArrayBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var HighlightTextArrayBinding = /** @class */ (function () {
        function HighlightTextArrayBinding() {
        }
        HighlightTextArrayBinding.prototype.initBinding = function () {
            ko.bindingHandlers.highlightTextArray = {
                update: function (element, valueAccessor) {
                    var value = valueAccessor();
                    var text = value.text;
                    if (ko.isObservable(text)) {
                        text = text();
                    }
                    var words = value.keywords;
                    if (value.keywords.length > 0) {
                        // entferne HTML-Tags
                        text = $("<div>" + text + "</div>").text();
                        var span = $('<span>' + text + '</span>');
                        words.sort(function (a, b) {
                            return b.length - a.length; // ASC -> a - b; DESC -> b - a
                        });
                        for (var i in words) {
                            var word = words[i];
                            $(span).highlight(word);
                        }
                        $(element).html($(span).html());
                    }
                    else {
                        $(element).text(text);
                    }
                }
            };
        };
        return HighlightTextArrayBinding;
    }());
    return HighlightTextArrayBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGlnaGxpZ2h0VGV4dEFycmF5QmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkhpZ2hsaWdodFRleHRBcnJheUJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsaUZBQWlGOzs7SUFFakY7UUFBQTtRQWlDQSxDQUFDO1FBL0JVLCtDQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsR0FBRztnQkFDcEMsTUFBTSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDbkQsSUFBSSxLQUFLLEdBQUcsYUFBYSxFQUFFLENBQUM7b0JBQzVCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7b0JBQ3RCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdkIsSUFBSSxHQUFHLElBQUksRUFBRSxDQUFDO3FCQUNqQjtvQkFDRCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUUzQixJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDM0IscUJBQXFCO3dCQUNyQixJQUFJLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLEdBQUcsUUFBUSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzNDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLFNBQVMsQ0FBQyxDQUFDO3dCQUUxQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQ1osT0FBTyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyw4QkFBOEI7d0JBQzlELENBQUMsQ0FBQyxDQUFDO3dCQUVILEtBQUssSUFBSSxDQUFDLElBQUksS0FBSyxFQUFFOzRCQUNqQixJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ25CLENBQUMsQ0FBQyxJQUFJLENBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQ3BDO3dCQUNELENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7cUJBQ25DO3lCQUNJO3dCQUNELENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3pCO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLGdDQUFDO0lBQUQsQ0FBQyxBQWpDRCxJQWlDQztJQUVELE9BQVMseUJBQXlCLENBQUMifQ==