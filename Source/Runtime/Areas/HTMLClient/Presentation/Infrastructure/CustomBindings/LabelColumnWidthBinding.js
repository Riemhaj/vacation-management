/// <amd-module name="Infrastructure/CustomBindings/LabelColumnWidthBinding" />
define("Infrastructure/CustomBindings/LabelColumnWidthBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var LabelColumnWidthBinding = /** @class */ (function () {
        function LabelColumnWidthBinding() {
        }
        LabelColumnWidthBinding.prototype.initBinding = function () {
            ko.bindingHandlers.labelColumnWidth = {
                init: function (node) {
                    var element = $(node);
                    var caption = element.find('> .c-caption');
                    caption.addClass('c-labelcolumn');
                },
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var width = ko.unwrap(value);
                    var caption = element.find('> .c-caption');
                    if (caption.length && caption.text().length) {
                        caption.css('flex-basis', width);
                    }
                }
            };
        };
        return LabelColumnWidthBinding;
    }());
    return LabelColumnWidthBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGFiZWxDb2x1bW5XaWR0aEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJMYWJlbENvbHVtbldpZHRoQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwrRUFBK0U7OztJQUUvRTtRQUFBO1FBc0JBLENBQUM7UUFwQlUsNkNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLGdCQUFnQixHQUFHO2dCQUNsQyxJQUFJLEVBQUUsVUFBQyxJQUFpQjtvQkFDcEIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUUzQyxPQUFPLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN0QyxDQUFDO2dCQUNELE1BQU0sRUFBRSxVQUFDLElBQWlCLEVBQUUsYUFBd0I7b0JBQ2hELElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdEIsSUFBSSxLQUFLLEdBQUcsYUFBYSxFQUFFLENBQUM7b0JBQzVCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzdCLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBRTNDLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFO3dCQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztxQkFDcEM7Z0JBQ0wsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsOEJBQUM7SUFBRCxDQUFDLEFBdEJELElBc0JDO0lBRUQsT0FBUyx1QkFBdUIsQ0FBQyJ9