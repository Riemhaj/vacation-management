define("Infrastructure/CustomBindings/CoreFrameGridBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var CoreFrameGridBinding = /** @class */ (function () {
        function CoreFrameGridBinding() {
        }
        CoreFrameGridBinding.prototype.initBinding = function () {
            ko.bindingHandlers.coreFrameGrid = {
                init: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var columns = value.columns;
                    var data = value.data;
                    var height = value.height;
                    var editable = value.editable;
                    var editMode = value.editMode;
                    var doubleClickToEdit = value.doubleClickToEdit;
                    var applyOnLostFocus = value.applyOnLostFocus;
                    var rowIdName = value.rowIdName;
                    var onBeforeEdit = value.onBeforeEdit;
                    var onSortChange = value.onSortChange;
                    var onDblClick = value.onDblClick;
                    var onSave = value.onSave;
                    var onDelete = value.onDelete;
                    var canAdd = value.canAdd;
                    var onAdd = value.onAdd;
                    var onKeyDown = value.onKeyDown;
                    var fireEditWithoutClick = value.fireEditWithoutClick;
                    var isEnabled = value.enabled;
                    var paging = value.paging;
                    var summaryRow = value.summaryRow;
                    var selected = value.selected;
                    var showHeader = value.showHeader;
                    var locator = value.locator;
                    var selectionMode = value.selectionMode;
                    var onSelectionChange = value.onSelectionChange;
                    var onColumnResized = value.onColumnResized;
                    var onMouseOver = value.onMouseOver;
                    var onMouseDown = value.onMouseDown;
                    var onBeforeRender = value.onBeforeRender;
                    var onAfterRender = value.onAfterRender;
                    var orderBy = value.orderBy;
                    var makeFunction = function (candidate, defaultValue) {
                        if ($.isFunction(candidate)) {
                            return candidate;
                        }
                        return function () { return candidate || defaultValue; };
                    };
                    rowIdName = makeFunction(rowIdName);
                    canAdd = makeFunction(canAdd, false);
                    isEnabled = makeFunction(isEnabled, true);
                    paging = makeFunction(paging, undefined);
                    selected = makeFunction(selected, undefined);
                    selectionMode = makeFunction(selectionMode, undefined);
                    height = makeFunction(height, undefined);
                    editable = makeFunction(editable, false);
                    orderBy = makeFunction(orderBy, undefined);
                    var subscriptions = [];
                    var grid = new CF.CoreFrameGrid({
                        element: element,
                        columns: columns(),
                        data: data(),
                        onSelectionChange: onSelectionChange,
                        selectionMode: selectionMode(),
                        height: height(),
                        onSortChange: onSortChange,
                        onDblClick: onDblClick,
                        onMouseOver: onMouseOver,
                        onMouseDown: onMouseDown,
                        isEditable: editable(),
                        editMode: editMode,
                        canAdd: canAdd(),
                        rowIdName: rowIdName(),
                        onBeforeEdit: onBeforeEdit,
                        doubleClickToEdit: doubleClickToEdit,
                        applyOnLostFocus: applyOnLostFocus,
                        onSave: onSave,
                        onDelete: onDelete,
                        onAdd: onAdd,
                        onKeyDown: onKeyDown,
                        isEnabled: isEnabled(),
                        paging: paging(),
                        summaryRow: summaryRow,
                        selected: selected(),
                        showHeader: showHeader,
                        locator: locator,
                        onColumnResized: onColumnResized,
                        onBeforeRender: onBeforeRender,
                        onAfterRender: onAfterRender,
                        orderBy: orderBy()
                    });
                    grid.init();
                    var redirectSubscription = function (subscribable, callback) {
                        if (subscribable && $.isFunction(subscribable.subscribe)) {
                            subscriptions.push(subscribable.subscribe(callback));
                        }
                    };
                    redirectSubscription(data, function (newData) { return grid.SetData(newData); });
                    redirectSubscription(selected, function (newSelected) { return grid.SetSelectedIds(newSelected); });
                    redirectSubscription(editable, function (editable) { return grid.setIsEditable(editable); });
                    redirectSubscription(columns, function (newColummns) { return grid.setColumns(newColummns); });
                    redirectSubscription(height, function (newHeight) { return grid.setHeight(newHeight); });
                    redirectSubscription(isEnabled, function (isEnabled) { return grid.setIsEnabled(isEnabled); });
                    redirectSubscription(canAdd, function (canAdd) { return grid.setCanAdd(canAdd); });
                    redirectSubscription(fireEditWithoutClick, function (rowId) { return grid.EnterEditModeById(rowId, true); });
                    redirectSubscription(paging, function (newPaging) { return grid.updatePaging(newPaging); });
                    redirectSubscription(orderBy, function (newOrderBy) { return grid.setSortExpression(newOrderBy); });
                    value.widget(grid);
                    ko.utils.domNodeDisposal.addDisposeCallback(node, function () {
                        subscriptions.forEach(function (s) { return s.dispose(); });
                        subscriptions = [];
                        grid.dispose();
                    });
                }
            };
        };
        return CoreFrameGridBinding;
    }());
    return CoreFrameGridBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29yZUZyYW1lR3JpZEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDb3JlRnJhbWVHcmlkQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztJQUtBO1FBQUE7UUE0SEEsQ0FBQztRQTFIVSwwQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHO2dCQUMvQixJQUFJLEVBQUUsVUFBQyxJQUFpQixFQUFFLGFBQXFDO29CQUMzRCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM1QixJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO29CQUM1QixJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO29CQUV0QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO29CQUMxQixJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUM5QixJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUM5QixJQUFJLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztvQkFDaEQsSUFBSSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsZ0JBQWdCLENBQUM7b0JBQzlDLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUM7b0JBQ2hDLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7b0JBQ3RDLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7b0JBQ3RDLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7b0JBQ2xDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7b0JBQzFCLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7b0JBQzlCLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7b0JBQzFCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7b0JBQ3hCLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUM7b0JBQ2hDLElBQUksb0JBQW9CLEdBQUcsS0FBSyxDQUFDLG9CQUFvQixDQUFDO29CQUN0RCxJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO29CQUM5QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO29CQUMxQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUNsQyxJQUFJLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUM5QixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUNsQyxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO29CQUM1QixJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDO29CQUN4QyxJQUFJLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztvQkFDaEQsSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQztvQkFDNUMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQztvQkFDcEMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQztvQkFDcEMsSUFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztvQkFDMUMsSUFBSSxhQUFhLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQztvQkFDeEMsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztvQkFFNUIsSUFBSSxZQUFZLEdBQUcsVUFBQyxTQUFjLEVBQUUsWUFBa0I7d0JBQ2xELElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRTs0QkFDekIsT0FBTyxTQUFTLENBQUM7eUJBQ3BCO3dCQUVELE9BQU8sY0FBTSxPQUFBLFNBQVMsSUFBSSxZQUFZLEVBQXpCLENBQXlCLENBQUM7b0JBQzNDLENBQUMsQ0FBQztvQkFHRixTQUFTLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUNwQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDckMsU0FBUyxHQUFHLFlBQVksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzFDLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLEdBQUcsWUFBWSxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDN0MsYUFBYSxHQUFHLFlBQVksQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBQ3ZELE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLEdBQUcsWUFBWSxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDekMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBRTNDLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztvQkFFdkIsSUFBSSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsYUFBYSxDQUFDO3dCQUM1QixPQUFPLEVBQUUsT0FBTzt3QkFDaEIsT0FBTyxFQUFFLE9BQU8sRUFBRTt3QkFDbEIsSUFBSSxFQUFFLElBQUksRUFBRTt3QkFFWixpQkFBaUIsRUFBRSxpQkFBaUI7d0JBQ3BDLGFBQWEsRUFBRSxhQUFhLEVBQUU7d0JBQzlCLE1BQU0sRUFBRSxNQUFNLEVBQUU7d0JBQ2hCLFlBQVksRUFBRSxZQUFZO3dCQUMxQixVQUFVLEVBQUUsVUFBVTt3QkFDdEIsV0FBVyxFQUFFLFdBQVc7d0JBQ3hCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixVQUFVLEVBQUUsUUFBUSxFQUFFO3dCQUN0QixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsTUFBTSxFQUFFLE1BQU0sRUFBRTt3QkFDaEIsU0FBUyxFQUFFLFNBQVMsRUFBRTt3QkFDdEIsWUFBWSxFQUFFLFlBQVk7d0JBQzFCLGlCQUFpQixFQUFFLGlCQUFpQjt3QkFDcEMsZ0JBQWdCLEVBQUUsZ0JBQWdCO3dCQUNsQyxNQUFNLEVBQUUsTUFBTTt3QkFDZCxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsS0FBSyxFQUFFLEtBQUs7d0JBQ1osU0FBUyxFQUFFLFNBQVM7d0JBQ3BCLFNBQVMsRUFBRSxTQUFTLEVBQUU7d0JBQ3RCLE1BQU0sRUFBRSxNQUFNLEVBQUU7d0JBQ2hCLFVBQVUsRUFBRSxVQUFVO3dCQUN0QixRQUFRLEVBQUUsUUFBUSxFQUFFO3dCQUNwQixVQUFVLEVBQUUsVUFBVTt3QkFDdEIsT0FBTyxFQUFFLE9BQU87d0JBQ2hCLGVBQWUsRUFBRSxlQUFlO3dCQUNoQyxjQUFjLEVBQUUsY0FBYzt3QkFDOUIsYUFBYSxFQUFFLGFBQWE7d0JBQzVCLE9BQU8sRUFBRSxPQUFPLEVBQUU7cUJBQ0gsQ0FBQyxDQUFDO29CQUNyQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBRVosSUFBSSxvQkFBb0IsR0FBRyxVQUFDLFlBQWlCLEVBQUUsUUFBaUM7d0JBQzVFLElBQUksWUFBWSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFOzRCQUN0RCxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt5QkFDeEQ7b0JBQ0wsQ0FBQyxDQUFDO29CQUVGLG9CQUFvQixDQUFDLElBQUksRUFBRSxVQUFBLE9BQU8sSUFBSSxPQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQXJCLENBQXFCLENBQUMsQ0FBQztvQkFDN0Qsb0JBQW9CLENBQUMsUUFBUSxFQUFFLFVBQUEsV0FBVyxJQUFJLE9BQUEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO29CQUNoRixvQkFBb0IsQ0FBQyxRQUFRLEVBQUUsVUFBQSxRQUFRLElBQUksT0FBQSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUM7b0JBQ3pFLG9CQUFvQixDQUFDLE9BQU8sRUFBRSxVQUFBLFdBQVcsSUFBSSxPQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEVBQTVCLENBQTRCLENBQUMsQ0FBQztvQkFDM0Usb0JBQW9CLENBQUMsTUFBTSxFQUFFLFVBQUEsU0FBUyxJQUFJLE9BQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO29CQUNyRSxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsVUFBQSxTQUFTLElBQUksT0FBQSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUM7b0JBQzNFLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxVQUFBLE1BQU0sSUFBSSxPQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQXRCLENBQXNCLENBQUMsQ0FBQztvQkFDL0Qsb0JBQW9CLENBQUMsb0JBQW9CLEVBQUUsVUFBQSxLQUFLLElBQUksT0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFuQyxDQUFtQyxDQUFDLENBQUM7b0JBQ3pGLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxVQUFBLFNBQVMsSUFBSSxPQUFBLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQTVCLENBQTRCLENBQUMsQ0FBQztvQkFDeEUsb0JBQW9CLENBQUMsT0FBTyxFQUFFLFVBQUEsVUFBVSxJQUFJLE9BQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxFQUFsQyxDQUFrQyxDQUFDLENBQUM7b0JBRWhGLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRW5CLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRTt3QkFDOUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBWCxDQUFXLENBQUMsQ0FBQzt3QkFDeEMsYUFBYSxHQUFHLEVBQUUsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUNuQixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCwyQkFBQztJQUFELENBQUMsQUE1SEQsSUE0SEM7SUF1Q0QsT0FBUyxvQkFBb0IsQ0FBQyJ9