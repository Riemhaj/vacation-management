/// <amd-module name="Infrastructure/CustomBindings/DescribedByBinding" />
define("Infrastructure/CustomBindings/DescribedByBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var DescribedByBinding = /** @class */ (function () {
        function DescribedByBinding() {
        }
        DescribedByBinding.prototype.initBinding = function () {
            ko.bindingHandlers.describedBy = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var describedBy = ko.unwrap(value);
                    if (!describedBy) {
                        return;
                    }
                    // Falls das Element noch keine ID besitzt, wird diese anhand des darüberliegenden Controls erzeugt
                    if (!describedBy.attr('id')) {
                        var parentControl = describedBy.parents('.c-control').first();
                        describedBy.attr('id', parentControl.data('control') + '-description');
                    }
                    element.removeAttr('aria-describedby');
                    element.attr('aria-describedby', describedBy.attr('id'));
                }
            };
        };
        return DescribedByBinding;
    }());
    return DescribedByBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGVzY3JpYmVkQnlCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRGVzY3JpYmVkQnlCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBFQUEwRTs7O0lBRTFFO1FBQUE7UUF3QkEsQ0FBQztRQXRCVSx3Q0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHO2dCQUM3QixNQUFNLEVBQUUsVUFBQyxJQUFpQixFQUFFLGFBQXdCO29CQUNoRCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM1QixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVuQyxJQUFJLENBQUMsV0FBVyxFQUFFO3dCQUNkLE9BQU87cUJBQ1Y7b0JBRUQsbUdBQW1HO29CQUNuRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDekIsSUFBSSxhQUFhLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDOUQsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxjQUFjLENBQUMsQ0FBQTtxQkFDekU7b0JBRUQsT0FBTyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2QyxPQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDN0QsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wseUJBQUM7SUFBRCxDQUFDLEFBeEJELElBd0JDO0lBRUQsT0FBUyxrQkFBa0IsQ0FBQyJ9