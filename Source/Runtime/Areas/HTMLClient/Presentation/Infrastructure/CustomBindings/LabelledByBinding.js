/// <amd-module name="Infrastructure/CustomBindings/LabelledByBinding" />
define("Infrastructure/CustomBindings/LabelledByBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var LabelledByBinding = /** @class */ (function () {
        function LabelledByBinding() {
        }
        LabelledByBinding.prototype.initBinding = function () {
            ko.bindingHandlers.labelledBy = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var labelledBy = ko.unwrap(value);
                    if (!labelledBy)
                        return;
                    // Falls das Element noch keine ID besitzt, wird diese anhand des darüberliegenden Controls erzeugt
                    if (!labelledBy.attr('id')) {
                        var parentControl = labelledBy.parents('.c-control').first();
                        labelledBy.attr('id', parentControl.data('control') + '-label-' + _.uniqueId());
                    }
                    element.removeAttr('aria-labelledby');
                    element.attr('aria-labelledby', labelledBy.attr('id'));
                }
            };
        };
        return LabelledByBinding;
    }());
    return LabelledByBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGFiZWxsZWRCeUJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJMYWJlbGxlZEJ5QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx5RUFBeUU7OztJQUV6RTtRQUFBO1FBdUJBLENBQUM7UUFyQlUsdUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRztnQkFDNUIsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLEtBQUssR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDNUIsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFbEMsSUFBSSxDQUFDLFVBQVU7d0JBQ1gsT0FBTztvQkFFWCxtR0FBbUc7b0JBQ25HLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUN4QixJQUFJLGFBQWEsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUM3RCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLFNBQVMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQTtxQkFDbEY7b0JBRUQsT0FBTyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDM0QsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsd0JBQUM7SUFBRCxDQUFDLEFBdkJELElBdUJDO0lBRUQsT0FBUyxpQkFBaUIsQ0FBQyJ9