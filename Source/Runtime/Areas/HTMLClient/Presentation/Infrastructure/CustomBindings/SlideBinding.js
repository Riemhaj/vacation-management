/// <amd-module name="Infrastructure/CustomBindings/SlideBinding" />
define("Infrastructure/CustomBindings/SlideBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var SlideBinding = /** @class */ (function () {
        function SlideBinding() {
        }
        SlideBinding.slideCallback = function (callback, element) {
            if (callback) {
                callback($(element));
            }
        };
        SlideBinding.prototype.initBinding = function () {
            ko.bindingHandlers.slide = {
                init: function (element, valueAccessor) {
                    var params = ko.unwrap(valueAccessor());
                    var value = ko.unwrap(params.value);
                    $(element).toggle(value);
                },
                update: function (element, valueAccessor) {
                    var params = ko.unwrap(valueAccessor());
                    var value = ko.unwrap(params.value);
                    value ? $(element).slideDown(120, function () {
                        SlideBinding.slideCallback(params.onSlideDown, element);
                    }) : $(element).slideUp(120, function () {
                        SlideBinding.slideCallback(params.onSlideUp, element);
                    });
                }
            };
        };
        return SlideBinding;
    }());
    return SlideBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2xpZGVCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiU2xpZGVCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9FQUFvRTs7O0lBRXBFO1FBQUE7UUE0QkEsQ0FBQztRQTFCa0IsMEJBQWEsR0FBNUIsVUFBNkIsUUFBUSxFQUFFLE9BQU87WUFDMUMsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQztRQUVNLGtDQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUc7Z0JBQ3ZCLElBQUksRUFBRSxVQUFDLE9BQU8sRUFBRSxhQUFhO29CQUN6QixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQ3hDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVwQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixDQUFDO2dCQUNELE1BQU0sRUFBRSxVQUFDLE9BQU8sRUFBRSxhQUFhO29CQUMzQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQ3hDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUVwQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO3dCQUM5QixZQUFZLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQzVELENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRTt3QkFDekIsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUMxRCxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCxtQkFBQztJQUFELENBQUMsQUE1QkQsSUE0QkM7SUFFRCxPQUFTLFlBQVksQ0FBQyJ9