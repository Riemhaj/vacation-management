/// <amd-module name="Infrastructure/CustomBindings/ControlIdentBinding" />
define("Infrastructure/CustomBindings/ControlIdentBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ControlIdentBinding = /** @class */ (function () {
        function ControlIdentBinding() {
        }
        ControlIdentBinding.prototype.initBinding = function () {
            ko.bindingHandlers.controlIdent = {
                init: function (element, valueAccessor) {
                    var ident = ko.unwrap(valueAccessor());
                    $(element).attr('data-control', ident);
                }
            };
        };
        return ControlIdentBinding;
    }());
    return ControlIdentBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbElkZW50QmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnRyb2xJZGVudEJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMkVBQTJFOzs7SUFFM0U7UUFBQTtRQVdBLENBQUM7UUFUVSx5Q0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsWUFBWSxHQUFHO2dCQUM5QixJQUFJLEVBQUUsVUFBQyxPQUFvQixFQUFFLGFBQXdCO29CQUNqRCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBRXZDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUMzQyxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCwwQkFBQztJQUFELENBQUMsQUFYRCxJQVdDO0lBRUQsT0FBUyxtQkFBbUIsQ0FBQyJ9