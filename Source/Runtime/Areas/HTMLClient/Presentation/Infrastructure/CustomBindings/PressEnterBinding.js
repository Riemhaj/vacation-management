/// <amd-module name="Infrastructure/CustomBindings/PressEnterBinding" />
define("Infrastructure/CustomBindings/PressEnterBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var PressEnterBinding = /** @class */ (function () {
        function PressEnterBinding() {
        }
        PressEnterBinding.prototype.initBinding = function () {
            ko.bindingHandlers.pressEnter = {
                init: function (element, valueAccessor) {
                    var val = valueAccessor();
                    if (!$.isFunction(val)) {
                        return;
                    }
                    $(element).keyup(function (event) {
                        if (event.keyCode == 13) {
                            val("", event);
                        }
                    });
                }
            };
        };
        return PressEnterBinding;
    }());
    return PressEnterBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJlc3NFbnRlckJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJQcmVzc0VudGVyQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx5RUFBeUU7OztJQUV6RTtRQUFBO1FBa0JBLENBQUM7UUFoQlUsdUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRztnQkFDNUIsSUFBSSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDakQsSUFBSSxHQUFHLEdBQUcsYUFBYSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUNwQixPQUFPO3FCQUNWO29CQUVELENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxLQUFLO3dCQUNuQixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksRUFBRSxFQUFFOzRCQUNyQixHQUFHLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNsQjtvQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0osQ0FBQTtRQUNMLENBQUM7UUFDTCx3QkFBQztJQUFELENBQUMsQUFsQkQsSUFrQkM7SUFFRCxPQUFTLGlCQUFpQixDQUFDIn0=