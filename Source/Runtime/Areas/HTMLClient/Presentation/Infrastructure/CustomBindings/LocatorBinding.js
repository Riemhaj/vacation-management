/// <amd-module name="Infrastructure/CustomBindings/LocatorBinding" />
define("Infrastructure/CustomBindings/LocatorBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var LocatorBinding = /** @class */ (function () {
        function LocatorBinding() {
        }
        LocatorBinding.prototype.initBinding = function () {
            ko.bindingHandlers.locator = {
                init: function (element, valueAccessor, bindings, view, bindingContext) {
                    var name = ko.unwrap(valueAccessor());
                    if (name != null && typeof name == 'object') {
                        var parent_1 = name.parent;
                        name = name.name;
                        if (name != null && name != '' && parent_1 != null && parent_1 != '') {
                            name = parent_1 + '.' + name;
                        }
                    }
                    if (name == null || name == '') {
                        return;
                    }
                    $(element).attr('data-locator', name);
                }
            };
        };
        return LocatorBinding;
    }());
    return LocatorBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9jYXRvckJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJMb2NhdG9yQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQUV0RTtRQUFBO1FBdUJBLENBQUM7UUFyQlUsb0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsSUFBSSxFQUFFLFVBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWM7b0JBQ3pELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztvQkFFdEMsSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLE9BQU8sSUFBSSxJQUFJLFFBQVEsRUFBRTt3QkFDekMsSUFBSSxRQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQzt3QkFDekIsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ2pCLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksRUFBRSxJQUFJLFFBQU0sSUFBSSxJQUFJLElBQUksUUFBTSxJQUFJLEVBQUUsRUFBRTs0QkFDOUQsSUFBSSxHQUFHLFFBQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDO3lCQUM5QjtxQkFDSjtvQkFFRCxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUUsRUFBRTt3QkFDNUIsT0FBTztxQkFDVjtvQkFFRCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDMUMsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wscUJBQUM7SUFBRCxDQUFDLEFBdkJELElBdUJDO0lBRUQsT0FBUyxjQUFjLENBQUMifQ==