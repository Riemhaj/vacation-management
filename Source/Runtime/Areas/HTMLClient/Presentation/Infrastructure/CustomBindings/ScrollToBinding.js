/// <amd-module name="Infrastructure/CustomBindings/ScrollToBinding" />
define("Infrastructure/CustomBindings/ScrollToBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ScrollToBinding = /** @class */ (function () {
        function ScrollToBinding() {
        }
        ScrollToBinding.prototype.initBinding = function () {
            ko.bindingHandlers.scrollTo = {
                update: function (element, valueAccessor) {
                    var value = ko.utils.unwrapObservable(valueAccessor());
                    if (!isNaN(value)) {
                        $(element).animate({
                            scrollTop: value + 'px'
                        }, 'fast');
                    }
                }
            };
        };
        return ScrollToBinding;
    }());
    return ScrollToBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2Nyb2xsVG9CaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiU2Nyb2xsVG9CaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHVFQUF1RTs7O0lBRXZFO1FBQUE7UUFlQSxDQUFDO1FBYlUscUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFFBQVEsR0FBRztnQkFDMUIsTUFBTSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDbkQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUV2RCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNmLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUM7NEJBQ2YsU0FBUyxFQUFFLEtBQUssR0FBRyxJQUFJO3lCQUMxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO3FCQUNkO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHNCQUFDO0lBQUQsQ0FBQyxBQWZELElBZUM7SUFFRCxPQUFTLGVBQWUsQ0FBQyJ9