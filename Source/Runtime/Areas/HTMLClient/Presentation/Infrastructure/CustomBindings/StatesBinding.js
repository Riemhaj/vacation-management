/// <amd-module name="Infrastructure/CustomBindings/StatesBinding" />
define("Infrastructure/CustomBindings/StatesBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var StatesBinding = /** @class */ (function () {
        function StatesBinding() {
        }
        StatesBinding.prototype.initBinding = function () {
            ko.bindingHandlers.states = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var states = ko.unwrap(value);
                    element.alterClass('c-state-*');
                    for (var i = 0; i < states.length; i++) {
                        element.addClass('c-state-' + states[i]);
                    }
                }
            };
        };
        return StatesBinding;
    }());
    return StatesBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RhdGVzQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlN0YXRlc0JpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFFckU7UUFBQTtRQWlCQSxDQUFDO1FBZlUsbUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRztnQkFDeEIsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLEtBQUssR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDNUIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFN0IsT0FBZSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFekMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ3BDLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUM1QztnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCxvQkFBQztJQUFELENBQUMsQUFqQkQsSUFpQkM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9