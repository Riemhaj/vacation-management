/// <amd-module name="Infrastructure/CustomBindings/ClassesBinding" />
define("Infrastructure/CustomBindings/ClassesBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ClassesBinding = /** @class */ (function () {
        function ClassesBinding() {
        }
        ClassesBinding.prototype.initBinding = function () {
            ko.bindingHandlers.classes = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var classes = ko.unwrap(value);
                    for (var i = 0; i < classes.length; i++) {
                        element.addClass(classes[i]);
                    }
                }
            };
        };
        return ClassesBinding;
    }());
    return ClassesBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2xhc3Nlc0JpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDbGFzc2VzQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQUV0RTtRQUFBO1FBZUEsQ0FBQztRQWJVLG9DQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUc7Z0JBQ3pCLE1BQU0sRUFBRSxVQUFDLElBQWlCLEVBQUUsYUFBd0I7b0JBQ2hELElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdEIsSUFBSSxLQUFLLEdBQUcsYUFBYSxFQUFFLENBQUM7b0JBQzVCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBRS9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUNyQyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNoQztnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCxxQkFBQztJQUFELENBQUMsQUFmRCxJQWVDO0lBRUQsT0FBUyxjQUFjLENBQUMifQ==