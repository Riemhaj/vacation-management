/// <amd-module name="Infrastructure/CustomBindings/ErgNameBinding" />
define("Infrastructure/CustomBindings/ErgNameBinding", ["require", "exports", "Management/ResourceManager"], function (require, exports, ResourceManager) {
    "use strict";
    var ErgNameBinding = /** @class */ (function () {
        function ErgNameBinding() {
        }
        ErgNameBinding.prototype.initBinding = function () {
            ko.bindingHandlers.ergName = {
                init: function (node, valueAccessor) {
                    var element = $(node);
                    var ergName = ko.unwrap(valueAccessor());
                    var resource = ResourceManager.loadResource(ergName, true);
                    if (_.isString(resource)) {
                        element.text(resource);
                    }
                    else {
                        var deferred = resource;
                        deferred.then(function (resource) {
                            element.text(resource);
                        });
                    }
                }
            };
        };
        return ErgNameBinding;
    }());
    return ErgNameBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXJnTmFtZUJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJFcmdOYW1lQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQUl0RTtRQUFBO1FBcUJBLENBQUM7UUFuQlUsb0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsSUFBSSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDOUMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQ3pDLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUUzRCxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQzFCO3lCQUFNO3dCQUNILElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQzt3QkFFeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVE7NEJBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQzNCLENBQUMsQ0FBQyxDQUFDO3FCQUNOO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHFCQUFDO0lBQUQsQ0FBQyxBQXJCRCxJQXFCQztJQUVELE9BQVMsY0FBYyxDQUFDIn0=