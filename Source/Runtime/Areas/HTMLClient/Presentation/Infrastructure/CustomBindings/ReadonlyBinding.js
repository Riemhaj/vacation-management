/// <amd-module name="Infrastructure/CustomBindings/ReadonlyBinding" />
define("Infrastructure/CustomBindings/ReadonlyBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ReadonlyBinding = /** @class */ (function () {
        function ReadonlyBinding() {
        }
        ReadonlyBinding.prototype.initBinding = function () {
            ko.bindingHandlers.readonly = {
                update: function (element, valueAccessor) {
                    var value = ko.utils.unwrapObservable(valueAccessor());
                    if (!value && element.readOnly)
                        element.readOnly = false;
                    else if (value && !element.readOnly)
                        element.readOnly = true;
                }
            };
        };
        return ReadonlyBinding;
    }());
    return ReadonlyBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVhZG9ubHlCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUmVhZG9ubHlCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHVFQUF1RTs7O0lBRXZFO1FBQUE7UUFhQSxDQUFDO1FBWFUscUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFFBQVEsR0FBRztnQkFDMUIsTUFBTSxFQUFFLFVBQUMsT0FBeUIsRUFBRSxhQUF3QjtvQkFDeEQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUN2RCxJQUFJLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxRQUFRO3dCQUMxQixPQUFPLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzt5QkFDeEIsSUFBSSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUTt3QkFDL0IsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHNCQUFDO0lBQUQsQ0FBQyxBQWJELElBYUM7SUFFRCxPQUFTLGVBQWUsQ0FBQyJ9