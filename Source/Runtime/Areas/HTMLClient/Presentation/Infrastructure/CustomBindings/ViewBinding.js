/// <amd-module name="Infrastructure/CustomBindings/ViewBinding" />
define("Infrastructure/CustomBindings/ViewBinding", ["require", "exports", "Factories/Factory", "Infrastructure/CustomBindingUtilities"], function (require, exports, Factory, Utils) {
    "use strict";
    var ViewBinding = /** @class */ (function () {
        function ViewBinding() {
        }
        ViewBinding.prototype.initBinding = function () {
            ko.bindingHandlers.view = {
                init: function (element, valueAccessor, bindings, view, bindingContext) {
                    var options = valueAccessor();
                    var wrapper = $(element);
                    var allBindings = bindings();
                    var factory = Factory.createInstance(options.uiGenerator, options);
                    if (factory == null) {
                        return;
                    }
                    Utils.registerDisposable(factory, bindings, bindingContext);
                    factory.async(function () {
                        var tuple = factory.getTuple();
                        var newBindings = jQuery.extend({}, allBindings, { uiElement: factory });
                        delete newBindings['view'];
                        ko.applyBindingsToNode(tuple.view[0], newBindings);
                        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                            if (!factory.isDisposed && !factory.__isDisposing) {
                                factory.dispose();
                            }
                            factory = null;
                        });
                    });
                }
            };
        };
        return ViewBinding;
    }());
    return ViewBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld0JpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJWaWV3QmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRUFBbUU7OztJQU1uRTtRQUFBO1FBa0NBLENBQUM7UUFoQ1UsaUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLElBQUksR0FBRztnQkFDdEIsSUFBSSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QixFQUFFLFFBQXFDLEVBQUUsSUFBUyxFQUFFLGNBQXNDO29CQUMzSSxJQUFJLE9BQU8sR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN6QixJQUFJLFdBQVcsR0FBRyxRQUFRLEVBQUUsQ0FBQztvQkFFN0IsSUFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBZ0IsQ0FBQztvQkFDbEYsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO3dCQUNqQixPQUFPO3FCQUNWO29CQUVELEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUU1RCxPQUFPLENBQUMsS0FBSyxDQUFDO3dCQUVWLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDL0IsSUFBSSxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7d0JBQ3pFLE9BQU8sV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUUzQixFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQzt3QkFFbkQsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFOzRCQUNqRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFFLE9BQWUsQ0FBQyxhQUFhLEVBQUU7Z0NBQ3hELE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQzs2QkFDckI7NEJBQ0QsT0FBTyxHQUFHLElBQUksQ0FBQzt3QkFDbkIsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsa0JBQUM7SUFBRCxDQUFDLEFBbENELElBa0NDO0lBRUQsT0FBUyxXQUFXLENBQUMifQ==