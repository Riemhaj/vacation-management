/// <amd-module name="Infrastructure/CustomBindings/HasFocusBinding" />
define("Infrastructure/CustomBindings/HasFocusBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var HasFocusBinding = /** @class */ (function () {
        function HasFocusBinding() {
        }
        HasFocusBinding.prototype.initBinding = function () {
            // Überschreibung von hasFocus
            var koHasFocusUpdateFunction = ko.bindingHandlers.hasfocus.update;
            ko.bindingHandlers.hasfocus.update = function (element, valueAccessor, bindings, view, bindingContext) {
                if ($(element).is(":visible")) {
                    koHasFocusUpdateFunction(element, valueAccessor, bindings, view, bindingContext);
                }
            };
        };
        return HasFocusBinding;
    }());
    return HasFocusBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGFzRm9jdXNCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSGFzRm9jdXNCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHVFQUF1RTs7O0lBRXZFO1FBQUE7UUFhQSxDQUFDO1FBWFUscUNBQVcsR0FBbEI7WUFFSSw4QkFBOEI7WUFDOUIsSUFBSSx3QkFBd0IsR0FBRyxFQUFFLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFFbEUsRUFBRSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLFVBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWM7Z0JBQ3hGLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRTtvQkFDM0Isd0JBQXdCLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO2lCQUNwRjtZQUNMLENBQUMsQ0FBQztRQUNOLENBQUM7UUFDTCxzQkFBQztJQUFELENBQUMsQUFiRCxJQWFDO0lBRUQsT0FBUyxlQUFlLENBQUMifQ==