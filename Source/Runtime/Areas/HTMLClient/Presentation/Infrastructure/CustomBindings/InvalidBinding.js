/// <amd-module name="Infrastructure/CustomBindings/InvalidBinding" />
define("Infrastructure/CustomBindings/InvalidBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var InvalidBinding = /** @class */ (function () {
        function InvalidBinding() {
        }
        InvalidBinding.prototype.initBinding = function () {
            ko.bindingHandlers.invalid = {
                update: function (element, valueAccessor, bindings, view, bindingContext) {
                    var text = ko.unwrap(valueAccessor());
                    var controlContainerElement = $(element).closest('.c-control');
                    if (text) {
                        if ($(element).children('.invalid-corner').length === 0) {
                            $('<div class="invalid-corner">').appendTo($(element)).hover(function (event) {
                                var it = $(element).children('.invalid-text');
                                if (it.length === 0) {
                                    $('<div class="invalid-text">').appendTo($(element));
                                    it = $(element).children('.invalid-text');
                                }
                                it.text(text);
                                it.css({ visibility: 'hidden', display: 'block', 'white-space': 'nowrap' });
                                var width = it.width();
                                var refLeft;
                                var ref = $(element).closest('[data-role=window]').first();
                                if (ref.length === 0) {
                                    ref = $(element).closest('[data-view]');
                                }
                                if (ref.length === 0) {
                                    ref = $(document);
                                    refLeft = 0;
                                }
                                else {
                                    refLeft = ref.offset().left;
                                }
                                var refWidth = ref.width();
                                var itLeft = it.offset().left;
                                var itRel = itLeft - refLeft;
                                var maxWidth = (refWidth - itRel - 16);
                                if (maxWidth < 120) {
                                    var posLeft = $(this).position().left;
                                    maxWidth = $(this).offset().left - refLeft - 16;
                                    it.css({ right: Math.max(0, $(this).offsetParent().width() - posLeft) + $(this).width(), left: 'auto' });
                                }
                                if (width > maxWidth) {
                                    it.css({ width: maxWidth + "px", "white-space": 'normal' });
                                }
                                it.css({ visibility: '', display: '' });
                                it.fadeIn('fast');
                                controlContainerElement.trigger('invalidation-text-shown');
                                controlContainerElement.data('invalidation-text-shown', true);
                            }, function () {
                                $(element).children('.invalid-text').remove();
                                controlContainerElement.removeData('invalidation-text-shown');
                            });
                        }
                        $(element).css('position', 'relative');
                    }
                    else {
                        $(element).children('.invalid-text').remove();
                        $(element).children('.invalid-corner').remove();
                    }
                }
            };
        };
        return InvalidBinding;
    }());
    return InvalidBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW52YWxpZEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJJbnZhbGlkQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQUV0RTtRQUFBO1FBcUVBLENBQUM7UUFuRVUsb0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsTUFBTSxFQUFFLFVBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWM7b0JBQzNELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztvQkFDdEMsSUFBSSx1QkFBdUIsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUUvRCxJQUFJLElBQUksRUFBRTt3QkFDTixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOzRCQUNyRCxDQUFDLENBQUMsOEJBQThCLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSztnQ0FDeEUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQ0FDOUMsSUFBSSxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQ0FDakIsQ0FBQyxDQUFDLDRCQUE0QixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29DQUNyRCxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztpQ0FDN0M7Z0NBQ0QsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDZCxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dDQUM1RSxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7Z0NBRXZCLElBQUksT0FBTyxDQUFDO2dDQUNaLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDM0QsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQ0FDbEIsR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7aUNBQzNDO2dDQUNELElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0NBQ2xCLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0NBQ2xCLE9BQU8sR0FBRyxDQUFDLENBQUM7aUNBQ2Y7cUNBQ0k7b0NBQ0QsT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7aUNBQy9CO2dDQUVELElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDM0IsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztnQ0FDOUIsSUFBSSxLQUFLLEdBQUcsTUFBTSxHQUFHLE9BQU8sQ0FBQztnQ0FFN0IsSUFBSSxRQUFRLEdBQUcsQ0FBQyxRQUFRLEdBQUcsS0FBSyxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dDQUV2QyxJQUFJLFFBQVEsR0FBRyxHQUFHLEVBQUU7b0NBQ2hCLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0NBQ3RDLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUM7b0NBQ2hELEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDLEtBQUssRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztpQ0FDNUc7Z0NBRUQsSUFBSSxLQUFLLEdBQUcsUUFBUSxFQUFFO29DQUNsQixFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsR0FBRyxJQUFJLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7aUNBQy9EO2dDQUNELEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dDQUV4QyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUVsQix1QkFBdUIsQ0FBQyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQztnQ0FDM0QsdUJBQXVCLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUVsRSxDQUFDLEVBQUU7Z0NBQ0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQ0FDOUMsdUJBQXVCLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUM7NEJBQ2xFLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUVELENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO3FCQUMxQzt5QkFBTTt3QkFDSCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUM5QyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7cUJBQ25EO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHFCQUFDO0lBQUQsQ0FBQyxBQXJFRCxJQXFFQztJQUVELE9BQVMsY0FBYyxDQUFDIn0=