/// <amd-module name="Infrastructure/CustomBindings/TooltipBinding" />
define("Infrastructure/CustomBindings/TooltipBinding", ["require", "exports", "Utility/StringFormatter", "Management/RADApplication", "Utility/Utilities"], function (require, exports, StringFormatter, RADApplication, Utilities) {
    "use strict";
    var TooltipBinding = /** @class */ (function () {
        function TooltipBinding() {
        }
        TooltipBinding.prototype.initBinding = function () {
            var tooltipRemovers = [];
            var clearTooltips = function () {
                for (var i = 0; i < tooltipRemovers.length; i++) {
                    tooltipRemovers[i]();
                }
                tooltipRemovers = [];
            };
            // Wenn irgendwo eine Maustaste gedrückt oder die Tastatur verwendet wird,
            // so schließen wir direkt alle Tooltips; dies erspart manuelles Schließen des Tooltips an vielen Stellen
            // (z.B. Klick auf einen Menüpunkt sollte auch das Tooltip verschwinden lassen etc.)
            $(document.body).on('mousedown keydown', clearTooltips);
            ko.bindingHandlers.tooltip = {
                update: function (node, valueAccessor) {
                    var GUI_TOOLTIP_DELAY = RADApplication.getSystemParam('Gui.TooltipDelay', parseInt);
                    var GUI_TOOLTIP_CLOSING_DELAY = RADApplication.getSystemParam('Gui.TooltipClosingDelay', parseInt);
                    var value = valueAccessor();
                    var element = $(node);
                    var tooltip;
                    var timeout;
                    var closeTimeout;
                    var controlContainerElement = element.closest('.c-control');
                    var delay = Utilities.isInt(GUI_TOOLTIP_DELAY) ? GUI_TOOLTIP_DELAY : 500; // Verzögerung bis Tooltip angezeigt wird
                    var closeTooltipDelay = Utilities.isInt(GUI_TOOLTIP_CLOSING_DELAY) ? GUI_TOOLTIP_CLOSING_DELAY : 5000;
                    var moveTooltip = true; // Soll der Tooltip mit dem Cursor mitbewegt werden?
                    // Der Tooltip wird erst delay ms nach dem Hover angezeigt.
                    // Bewegt man die Maus in der Zeit weiter, so sorgt der positionUpdater dafür,
                    // das der Tooltip dann an der letzten bekannten Position angezeigt wird, statt an dem Punkt des Hover-Eintritts.
                    // Falls moveTooltip=true ist und der Tooltip schon angezeigt wird, wird zudem direkt die reale Position des Tooltips angepasst.
                    var positionUpdater = null;
                    var text = null;
                    if (typeof (value) === 'string') {
                        text = value;
                    }
                    else {
                        // Achtung: Hier entsteht eine Dependency; ändert sich dieser Text, so wird update erneut aufgerufen
                        text = value != null ? value.caption() : null;
                    }
                    if (text == null || text.trim() == '') {
                        // Einen leeren Text wollen wir nicht in einem Tooltip darstellen
                        if (element.data('destroyTooltip') != null) {
                            element.data('destroyTooltip')();
                        }
                        return;
                    }
                    if (element.data('reinitializeOpenedTooltip') != null) {
                        // Tooltip ist gerade offen und kann aktualisiert werden!
                        element.data('reinitializeOpenedTooltip')();
                        return;
                    }
                    else if (element.data('destroyTooltip') != null) {
                        // Wenn der Tooltip nicht geöffnet ist, aber schonmal initialisiert wurde,
                        // so müssen wir verhindern, dass die Hover-Callbacks doppelt registriert werden; dies verhindern wir hier,
                        // indem wir den Tooltip erstmal zerstören.
                        element.data('destroyTooltip')();
                    }
                    var showTooltip = function () {
                        if (tooltip) {
                            window.clearTimeout(timeout);
                            window.clearTimeout(closeTimeout);
                            // Erst nach delay ms soll der Tooltip eingeblendet werden
                            timeout = window.setTimeout(function () {
                                tooltip.show();
                                closeTimeout = window.setTimeout(function () {
                                    hideTooltip();
                                }, closeTooltipDelay);
                            }, delay);
                        }
                    };
                    var hideTooltip = function () {
                        if (tooltip) {
                            window.clearTimeout(timeout);
                            window.clearTimeout(closeTimeout);
                            tooltip.hide();
                        }
                    };
                    // Entfernt den hier gebundenen Tooltip
                    var removeTooltip = function () {
                        window.clearTimeout(timeout);
                        window.clearTimeout(closeTimeout);
                        if (tooltip != null) {
                            tooltip.remove();
                            element.removeAttr('aria-describedby');
                            tooltip = null;
                        }
                        if (positionUpdater != null) {
                            element.off('mousemove', positionUpdater);
                            positionUpdater = null;
                        }
                        element.removeData('reinitializeOpenedTooltip');
                    };
                    // Beim Verlassen des zu hoverenden Elements mit der Maus, schließen wir auch das Tooltip wieder
                    var onMouseLeave = function () {
                        // Titelattribut zurücksetzen
                        var title = element.data('title');
                        element.attr('title', title);
                        element.removeData('title');
                        removeTooltip();
                        var idx = tooltipRemovers.indexOf(removeTooltip);
                        if (idx >= 0) {
                            tooltipRemovers.splice(idx, 1);
                        }
                    };
                    // Beim Hovern des Elementes mit der Maus lösen wir einen Timeout zur Anzeige des Tooltips aus
                    var onMouseEnter = function (event) {
                        // Titelattribut löschen und zwischenspeichern, um browsereigenen Tooltip zu unterbinden
                        var title = element.attr('title');
                        element.data('title', title);
                        element.removeAttr('title');
                        clearTooltips(); // Alle bestehenden Tooltips schließen
                        tooltipRemovers.push(removeTooltip); // Neues Tooltip registrieren (es soll durch andere Tooltip-Aufrufe entfernt werden können)
                        var top, left; // Dynamisch aktualisierte Position des Tooltips
                        var width, height; // Einmalig berechnte Breite und Höhe des Tooltips
                        var heightFromTop, widthFromLeft; // Einmalig berechnte Größen des Fensters, um Kollision/Overflow damit zu vermeiden
                        // Merkt sich bei Mausereignissen die aktuelle Position und wenn Tooltip-Bewegungen erwünscht ist und das Tooltip existiert
                        // so wird auch gleich die Position auf das Tooltip übertragen
                        positionUpdater = function (event) {
                            if (moveTooltip && tooltip != null && tooltip.length > 0) {
                                top = event.pageY + TooltipBinding.staticOffsetTop;
                                left = event.pageX + TooltipBinding.staticOffsetLeft;
                                TooltipBinding.positionTooltip(tooltip, top, left, height, width, heightFromTop, widthFromLeft);
                            }
                            // Wenn gerade der Invalidierungstext angezeigt wird, öffnen wir nicht den Tooltip
                            if (controlContainerElement.data('invalidation-text-shown')) {
                                hideTooltip();
                            }
                            else {
                                showTooltip();
                            }
                        };
                        element.on('mousemove', positionUpdater); // Bei allen zukünftigen Mausbewegungen ebenfalls die Position verarbeiten
                        var initializeTooltip = function () {
                            if (!moveTooltip && positionUpdater != null) {
                                // Wenn der Tooltip nicht kontinierlich mitbewegt werden soll, dann können wir positionUpdater nun deregistrieren
                                element.off('mousemove', positionUpdater);
                                positionUpdater = null;
                            }
                            if (value.isDisposed) {
                                // Falls das ViewModel schon disposed ist, so schließen wir auch den Tooltip direkt
                                if (tooltip != null) {
                                    tooltip.remove();
                                }
                                tooltip = null;
                                return;
                            }
                            // Falls der Observable-Text aktualisiert wird während der Tooltip geöffnet ist, so wird nur diese Methode aufgerufen
                            element.data('reinitializeOpenedTooltip', initializeTooltip);
                            var tooltipText = text || "";
                            tooltipText = StringFormatter.escapeHtml(tooltipText, true);
                            tooltipText = StringFormatter.escapeWhitespacesAlternating(tooltipText);
                            var tooltipLines = tooltipText.split(/\r?\n/);
                            var rootTooltip = $('<div>');
                            jQuery.each(tooltipLines, function (i, line) {
                                var currentLine = $('<div>').html(line);
                                rootTooltip.append(currentLine);
                            });
                            // Tooltip-Element initialisieren
                            tooltip = $('body').children('.c-tooltip');
                            if (tooltip.length === 0) {
                                var tooltipID = _.uniqueId('tooltip');
                                tooltip = $('<div id="' + tooltipID + '" class="c-tooltip">').append(rootTooltip)
                                    .css({ top: top, left: left })
                                    .appendTo('body')
                                    .hide();
                                element.attr('aria-describedby', tooltipID);
                                ko.applyBindingsToNode(tooltip[0], {
                                    attr: {
                                        role: 'tooltip'
                                    }
                                });
                            }
                            else {
                                tooltip.empty();
                                tooltip.append(rootTooltip).css({ top: top, left: left }).show();
                            }
                            // Initiale Größenmessungen durchführen
                            var w = $(window);
                            heightFromTop = w.height() + w.scrollTop();
                            widthFromLeft = w.width() + w.scrollLeft();
                            width = tooltip.outerWidth();
                            height = tooltip.outerHeight();
                            // Tooltip erstmalig positionieren
                            TooltipBinding.positionTooltip(tooltip, top, left, height, width, heightFromTop, widthFromLeft);
                        };
                        initializeTooltip();
                        positionUpdater(event); // Initial die Position des Hover-Ereignisses merken
                    };
                    var destroyTooltip = function () {
                        if (element.data('destroyTooltip') == null) {
                            return;
                        }
                        onMouseLeave();
                        element.off('mouseenter', onMouseEnter).off('mouseleave', onMouseLeave);
                        controlContainerElement.off('invalidation-text-shown');
                        element.removeData('destroyTooltip');
                    };
                    element
                        .on('mouseenter', onMouseEnter)
                        .on('mouseleave', onMouseLeave);
                    element.data('destroyTooltip', destroyTooltip);
                    controlContainerElement.on('invalidation-text-shown', hideTooltip);
                    ko.utils.domNodeDisposal.addDisposeCallback(node, destroyTooltip);
                }
            };
        };
        TooltipBinding.positionTooltip = function (tooltip, top, left, height, width, heightFromTop, widthFromLeft) {
            if (top + height >= heightFromTop) {
                top = top - (height + 0.5 * TooltipBinding.staticOffsetTop);
            }
            if (left + width >= widthFromLeft) {
                left = left - (width + 1.5 * TooltipBinding.staticOffsetLeft);
            }
            tooltip.css({ top: top, left: left });
        };
        /*
         * Verschiebungen des Tooltips gegenüber dem Mauszeiger
         */
        TooltipBinding.staticOffsetLeft = 10;
        /*
         * Verschiebungen des Tooltips gegenüber dem Mauszeiger
         */
        TooltipBinding.staticOffsetTop = 10;
        return TooltipBinding;
    }());
    return TooltipBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVG9vbHRpcEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJUb29sdGlwQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQU10RTtRQUFBO1FBK1FBLENBQUM7UUFuUVUsb0NBQVcsR0FBbEI7WUFDSSxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxhQUFhLEdBQUc7Z0JBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUM3QyxlQUFlLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDeEI7Z0JBQ0QsZUFBZSxHQUFHLEVBQUUsQ0FBQztZQUN6QixDQUFDLENBQUM7WUFFRiwwRUFBMEU7WUFDMUUseUdBQXlHO1lBQ3pHLG9GQUFvRjtZQUNwRixDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUV4RCxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBTSxpQkFBaUIsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLGtCQUFrQixFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUN0RixJQUFNLHlCQUF5QixHQUFHLGNBQWMsQ0FBQyxjQUFjLENBQUMseUJBQXlCLEVBQUUsUUFBUSxDQUFDLENBQUM7b0JBQ3JHLElBQUksS0FBSyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUU1QixJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksT0FBNEIsQ0FBQztvQkFDakMsSUFBSSxPQUFlLENBQUM7b0JBQ3BCLElBQUksWUFBb0IsQ0FBQztvQkFDekIsSUFBSSx1QkFBdUIsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUU1RCxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBWSx5Q0FBeUM7b0JBQzlILElBQUksaUJBQWlCLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUN0RyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBSyxvREFBb0Q7b0JBRWhGLDJEQUEyRDtvQkFDM0QsOEVBQThFO29CQUM5RSxpSEFBaUg7b0JBQ2pILGdJQUFnSTtvQkFDaEksSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO29CQUUzQixJQUFJLElBQUksR0FBVyxJQUFJLENBQUM7b0JBQ3hCLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFFBQVEsRUFBRTt3QkFDN0IsSUFBSSxHQUFHLEtBQUssQ0FBQztxQkFDaEI7eUJBQU07d0JBQ0gsb0dBQW9HO3dCQUNwRyxJQUFJLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7cUJBQ2pEO29CQUVELElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO3dCQUNuQyxpRUFBaUU7d0JBQ2pFLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksRUFBRTs0QkFDeEMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUM7eUJBQ3BDO3dCQUNELE9BQU87cUJBQ1Y7b0JBRUQsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksSUFBSSxFQUFFO3dCQUNuRCx5REFBeUQ7d0JBQ3pELE9BQU8sQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsRUFBRSxDQUFDO3dCQUM1QyxPQUFPO3FCQUNWO3lCQUNJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksRUFBRTt3QkFDN0MsMEVBQTBFO3dCQUMxRSwyR0FBMkc7d0JBQzNHLDJDQUEyQzt3QkFDM0MsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUM7cUJBQ3BDO29CQUVELElBQUksV0FBVyxHQUFHO3dCQUNkLElBQUksT0FBTyxFQUFFOzRCQUNULE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBRWxDLDBEQUEwRDs0QkFDMUQsT0FBTyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0NBQ3hCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDZixZQUFZLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztvQ0FDN0IsV0FBVyxFQUFFLENBQUM7Z0NBQ2xCLENBQUMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDOzRCQUMxQixDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ2I7b0JBQ0wsQ0FBQyxDQUFBO29CQUVELElBQUksV0FBVyxHQUFHO3dCQUNkLElBQUksT0FBTyxFQUFFOzRCQUNULE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBRWxDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQzt5QkFDbEI7b0JBQ0wsQ0FBQyxDQUFBO29CQUVELHVDQUF1QztvQkFDdkMsSUFBSSxhQUFhLEdBQUc7d0JBQ2hCLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBRWxDLElBQUksT0FBTyxJQUFJLElBQUksRUFBRTs0QkFDakIsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDOzRCQUNqQixPQUFPLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUM7NEJBQ3ZDLE9BQU8sR0FBRyxJQUFJLENBQUM7eUJBQ2xCO3dCQUVELElBQUksZUFBZSxJQUFJLElBQUksRUFBRTs0QkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7NEJBQzFDLGVBQWUsR0FBRyxJQUFJLENBQUM7eUJBQzFCO3dCQUVELE9BQU8sQ0FBQyxVQUFVLENBQUMsMkJBQTJCLENBQUMsQ0FBQztvQkFDcEQsQ0FBQyxDQUFDO29CQUVGLGdHQUFnRztvQkFDaEcsSUFBSSxZQUFZLEdBQUc7d0JBQ2YsNkJBQTZCO3dCQUM3QixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBVyxDQUFDO3dCQUM1QyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDN0IsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFFNUIsYUFBYSxFQUFFLENBQUM7d0JBQ2hCLElBQUksR0FBRyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQ2pELElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTs0QkFDVixlQUFlLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQzt5QkFDbEM7b0JBQ0wsQ0FBQyxDQUFDO29CQUVGLDhGQUE4RjtvQkFDOUYsSUFBSSxZQUFZLEdBQUcsVUFBQyxLQUFLO3dCQUVyQix3RkFBd0Y7d0JBQ3hGLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ2xDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO3dCQUM3QixPQUFPLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUU1QixhQUFhLEVBQUUsQ0FBQyxDQUFDLHNDQUFzQzt3QkFDdkQsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLDJGQUEyRjt3QkFFaEksSUFBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQW9CLGdEQUFnRDt3QkFDbEYsSUFBSSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQWdCLGtEQUFrRDt3QkFDcEYsSUFBSSxhQUFhLEVBQUUsYUFBYSxDQUFDLENBQUMsbUZBQW1GO3dCQUVySCwySEFBMkg7d0JBQzNILDhEQUE4RDt3QkFDOUQsZUFBZSxHQUFHLFVBQUMsS0FBSzs0QkFDcEIsSUFBSSxXQUFXLElBQUksT0FBTyxJQUFJLElBQUksSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQ0FDdEQsR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDLGVBQWUsQ0FBQztnQ0FDbkQsSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDLGdCQUFnQixDQUFDO2dDQUVyRCxjQUFjLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDOzZCQUNuRzs0QkFFRCxrRkFBa0Y7NEJBQ2xGLElBQUksdUJBQXVCLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLEVBQUU7Z0NBQ3pELFdBQVcsRUFBRSxDQUFDOzZCQUNqQjtpQ0FBTTtnQ0FDSCxXQUFXLEVBQUUsQ0FBQzs2QkFDakI7d0JBQ0wsQ0FBQyxDQUFDO3dCQUVGLE9BQU8sQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUUsMEVBQTBFO3dCQUVySCxJQUFJLGlCQUFpQixHQUFHOzRCQUNwQixJQUFJLENBQUMsV0FBVyxJQUFJLGVBQWUsSUFBSSxJQUFJLEVBQUU7Z0NBQ3pDLGlIQUFpSDtnQ0FDakgsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7Z0NBQzFDLGVBQWUsR0FBRyxJQUFJLENBQUM7NkJBQzFCOzRCQUVELElBQUksS0FBSyxDQUFDLFVBQVUsRUFBRTtnQ0FDbEIsbUZBQW1GO2dDQUNuRixJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7b0NBQ2pCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQ0FDcEI7Z0NBQ0QsT0FBTyxHQUFHLElBQUksQ0FBQztnQ0FDZixPQUFPOzZCQUNWOzRCQUVELHFIQUFxSDs0QkFDckgsT0FBTyxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDOzRCQUU3RCxJQUFJLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDOzRCQUM3QixXQUFXLEdBQUcsZUFBZSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQzVELFdBQVcsR0FBRyxlQUFlLENBQUMsNEJBQTRCLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBRXhFLElBQUksWUFBWSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQzlDLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFFN0IsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQ3BCLFVBQUMsQ0FBQyxFQUFFLElBQUk7Z0NBQ0osSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDeEMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQzs0QkFDcEMsQ0FBQyxDQUFDLENBQUM7NEJBRVAsaUNBQWlDOzRCQUNqQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDM0MsSUFBSSxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQ0FDdEIsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQ0FDdEMsT0FBTyxHQUFHLENBQUMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxHQUFHLHNCQUFzQixDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztxQ0FDNUUsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUM7cUNBQzdCLFFBQVEsQ0FBQyxNQUFNLENBQUM7cUNBQ2hCLElBQUksRUFBRSxDQUFDO2dDQUNaLE9BQU8sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0NBQzVDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0NBQy9CLElBQUksRUFBRTt3Q0FDRixJQUFJLEVBQUUsU0FBUztxQ0FDbEI7aUNBQ0osQ0FBQyxDQUFDOzZCQUNOO2lDQUFNO2dDQUNILE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDaEIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDOzZCQUNwRTs0QkFFRCx1Q0FBdUM7NEJBQ3ZDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDbEIsYUFBYSxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7NEJBQzNDLGFBQWEsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUMzQyxLQUFLLEdBQUcsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUM3QixNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDOzRCQUUvQixrQ0FBa0M7NEJBQ2xDLGNBQWMsQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsYUFBYSxDQUFDLENBQUM7d0JBQ3BHLENBQUMsQ0FBQzt3QkFFRixpQkFBaUIsRUFBRSxDQUFDO3dCQUVwQixlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxvREFBb0Q7b0JBQ2hGLENBQUMsQ0FBQztvQkFFRixJQUFJLGNBQWMsR0FBRzt3QkFDakIsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksSUFBSSxFQUFFOzRCQUN4QyxPQUFPO3lCQUNWO3dCQUVELFlBQVksRUFBRSxDQUFDO3dCQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUM7d0JBQ3hFLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO3dCQUV2RCxPQUFPLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQ3pDLENBQUMsQ0FBQztvQkFFRixPQUFPO3lCQUNGLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO3lCQUM5QixFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDO29CQUVwQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUUvQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMseUJBQXlCLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBRW5FLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztnQkFDdEUsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBRWMsOEJBQWUsR0FBOUIsVUFBK0IsT0FBNEIsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsYUFBYSxFQUFFLGFBQWE7WUFDL0csSUFBSSxHQUFHLEdBQUcsTUFBTSxJQUFJLGFBQWEsRUFBRTtnQkFDL0IsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQy9EO1lBRUQsSUFBSSxJQUFJLEdBQUcsS0FBSyxJQUFJLGFBQWEsRUFBRTtnQkFDL0IsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDakU7WUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUMxQyxDQUFDO1FBNVFEOztXQUVHO1FBQ3FCLCtCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUU5Qzs7V0FFRztRQUNxQiw4QkFBZSxHQUFHLEVBQUUsQ0FBQztRQXFRakQscUJBQUM7S0FBQSxBQS9RRCxJQStRQztJQUVELE9BQVMsY0FBYyxDQUFDIn0=