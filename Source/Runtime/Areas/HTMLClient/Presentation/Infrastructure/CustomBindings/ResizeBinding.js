/// <amd-module name="Infrastructure/CustomBindings/ResizeBinding" />
define("Infrastructure/CustomBindings/ResizeBinding", ["require", "exports", "Utility/ElementResizeDetector"], function (require, exports, ElementResizeDetector) {
    "use strict";
    var ResizeBinding = /** @class */ (function () {
        function ResizeBinding() {
        }
        ResizeBinding.prototype.initBinding = function () {
            ko.bindingHandlers.resize = {
                init: function (node, valueAccessor) {
                    var options = valueAccessor();
                    var method = options.method || 'throttle';
                    var delay = options.delay || 100;
                    var delayed;
                    if (method != 'none') {
                        delayed = _[method](function () {
                            options.callback(node);
                        }, delay);
                    }
                    else {
                        delayed = function () {
                            options.callback(node);
                        };
                    }
                    ElementResizeDetector.listenTo(node, delayed);
                    ko.utils.domNodeDisposal.addDisposeCallback(node, function () {
                        ElementResizeDetector.uninstall(node);
                    });
                }
            };
        };
        return ResizeBinding;
    }());
    return ResizeBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVzaXplQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlJlc2l6ZUJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFJckU7UUFBQTtRQTZCQSxDQUFDO1FBM0JVLG1DQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUc7Z0JBQ3hCLElBQUksRUFBRSxVQUFDLElBQWlCLEVBQUUsYUFBZ0M7b0JBQ3RELElBQUksT0FBTyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM5QixJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQztvQkFDMUMsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssSUFBSSxHQUFHLENBQUM7b0JBQ2pDLElBQUksT0FBTyxDQUFDO29CQUVaLElBQUksTUFBTSxJQUFJLE1BQU0sRUFBRTt3QkFDbEIsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDaEIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDM0IsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO3FCQUNiO3lCQUNJO3dCQUNELE9BQU8sR0FBRzs0QkFDTixPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUMzQixDQUFDLENBQUM7cUJBQ0w7b0JBRUQscUJBQXFCLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFFOUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFO3dCQUM5QyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzFDLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLG9CQUFDO0lBQUQsQ0FBQyxBQTdCRCxJQTZCQztJQVFELE9BQVMsYUFBYSxDQUFDIn0=