/// <amd-module name="Infrastructure/CustomBindings/ControlNameBinding" />
define("Infrastructure/CustomBindings/ControlNameBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var ControlNameBinding = /** @class */ (function () {
        function ControlNameBinding() {
        }
        ControlNameBinding.prototype.initBinding = function () {
            ko.bindingHandlers.controlName = {
                init: function (element, valueAccessor) {
                    var name = ko.unwrap(valueAccessor());
                    $(element).addClass('c-control');
                    $(element).addClass('c-' + name);
                }
            };
        };
        return ControlNameBinding;
    }());
    return ControlNameBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbE5hbWVCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ29udHJvbE5hbWVCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBFQUEwRTs7O0lBRTFFO1FBQUE7UUFZQSxDQUFDO1FBVlUsd0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRztnQkFDN0IsSUFBSSxFQUFFLFVBQUMsT0FBb0IsRUFBRSxhQUF3QjtvQkFDakQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUV0QyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNqQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDckMsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wseUJBQUM7SUFBRCxDQUFDLEFBWkQsSUFZQztJQUVELE9BQVMsa0JBQWtCLENBQUMifQ==