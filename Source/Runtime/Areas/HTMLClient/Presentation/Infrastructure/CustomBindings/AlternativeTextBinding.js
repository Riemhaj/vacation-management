/// <amd-module name="Infrastructure/CustomBindings/AlternativeTextBinding" />
define("Infrastructure/CustomBindings/AlternativeTextBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var AlternativeTextBinding = /** @class */ (function () {
        function AlternativeTextBinding() {
        }
        AlternativeTextBinding.prototype.initBinding = function () {
            ko.bindingHandlers.alternativeText = {
                init: function (node, valueAccessor) {
                    if ($.isFunction(valueAccessor)) {
                        var label = valueAccessor();
                        if (label && $.isFunction(label.caption)) {
                            ko.applyBindingsToNode(node, {
                                attr: {
                                    alt: label.caption
                                }
                            });
                        }
                    }
                }
            };
        };
        return AlternativeTextBinding;
    }());
    return AlternativeTextBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWx0ZXJuYXRpdmVUZXh0QmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFsdGVybmF0aXZlVGV4dEJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsOEVBQThFOzs7SUFJOUU7UUFBQTtRQW1CQSxDQUFDO1FBakJVLDRDQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEdBQUc7Z0JBQ2pDLElBQUksRUFBRSxVQUFDLElBQWlCLEVBQUUsYUFBbUM7b0JBQ3pELElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsRUFBRTt3QkFDN0IsSUFBSSxLQUFLLEdBQUcsYUFBYSxFQUFFLENBQUM7d0JBRTVCLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFFOzRCQUN0QyxFQUFFLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO2dDQUN6QixJQUFJLEVBQUU7b0NBQ0YsR0FBRyxFQUFFLEtBQUssQ0FBQyxPQUFPO2lDQUNyQjs2QkFDSixDQUFDLENBQUM7eUJBQ047cUJBQ0o7Z0JBQ0wsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsNkJBQUM7SUFBRCxDQUFDLEFBbkJELElBbUJDO0lBRUQsT0FBUyxzQkFBc0IsQ0FBQyJ9