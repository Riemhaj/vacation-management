/// <amd-module name="Infrastructure/CustomBindings/FocusBinding" />
define("Infrastructure/CustomBindings/FocusBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var FocusBinding = /** @class */ (function () {
        function FocusBinding() {
        }
        FocusBinding.prototype.initBinding = function () {
            ko.bindingHandlers.focus = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var focusable = ko.unwrap(valueAccessor());
                    if (focusable)
                        element.attr('tabindex', 0);
                    else
                        element.attr('tabindex', -1);
                }
            };
        };
        return FocusBinding;
    }());
    return FocusBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9jdXNCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRm9jdXNCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9FQUFvRTs7O0lBRXBFO1FBQUE7UUFlQSxDQUFDO1FBYlUsa0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRztnQkFDdkIsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBRTNDLElBQUksU0FBUzt3QkFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQzs7d0JBRTVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLG1CQUFDO0lBQUQsQ0FBQyxBQWZELElBZUM7SUFFRCxPQUFTLFlBQVksQ0FBQyJ9