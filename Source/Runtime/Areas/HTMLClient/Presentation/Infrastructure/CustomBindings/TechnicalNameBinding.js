/// <amd-module name="Infrastructure/CustomBindings/TechnicalNameBinding" />
define("Infrastructure/CustomBindings/TechnicalNameBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var TechnicalNameBinding = /** @class */ (function () {
        function TechnicalNameBinding() {
        }
        TechnicalNameBinding.prototype.initBinding = function () {
            ko.bindingHandlers.technicalName = {
                init: function (element, valueAccessor) {
                    var name = ko.unwrap(valueAccessor());
                    if (name == null || name == '') {
                        return;
                    }
                    $(element).attr('data-technical-name', name);
                }
            };
        };
        return TechnicalNameBinding;
    }());
    return TechnicalNameBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVGVjaG5pY2FsTmFtZUJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJUZWNobmljYWxOYW1lQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw0RUFBNEU7OztJQUU1RTtRQUFBO1FBY0EsQ0FBQztRQVpVLDBDQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUc7Z0JBQy9CLElBQUksRUFBRSxVQUFDLE9BQW9CLEVBQUUsYUFBMkI7b0JBQ3BELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztvQkFDdEMsSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFLEVBQUU7d0JBQzVCLE9BQU87cUJBQ1Y7b0JBRUQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDakQsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsMkJBQUM7SUFBRCxDQUFDLEFBZEQsSUFjQztJQUVELE9BQVMsb0JBQW9CLENBQUMifQ==