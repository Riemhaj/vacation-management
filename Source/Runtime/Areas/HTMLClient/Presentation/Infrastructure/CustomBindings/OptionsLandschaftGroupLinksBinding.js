/// <amd-module name="Infrastructure/CustomBindings/OptionsLandschaftGroupLinksBinding" />
define("Infrastructure/CustomBindings/OptionsLandschaftGroupLinksBinding", ["require", "exports", "Management/ComponentManager"], function (require, exports, ComponentManager) {
    "use strict";
    var OptionsLandschaftGroupLinksBinding = /** @class */ (function () {
        function OptionsLandschaftGroupLinksBinding() {
        }
        OptionsLandschaftGroupLinksBinding.prototype.initBinding = function () {
            ko.bindingHandlers.optionsLandschaftGroupLinks = {
                init: function (node, valueAccessor, bindings, view, bindingContext) {
                    var Link = ComponentManager.getControl('Link');
                    var optionsLandschaftGroup = $(node);
                    var options = valueAccessor();
                    var links = options.links();
                    options.parent.links = [];
                    for (var i = 0; i < links.length; i++) {
                        var link = new Link(links[i], $('<div>').appendTo(optionsLandschaftGroup));
                        var tuple = link.getTuple();
                        options.parent.links.push(link);
                    }
                }
            };
        };
        return OptionsLandschaftGroupLinksBinding;
    }());
    return OptionsLandschaftGroupLinksBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3B0aW9uc0xhbmRzY2hhZnRHcm91cExpbmtzQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk9wdGlvbnNMYW5kc2NoYWZ0R3JvdXBMaW5rc0JpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMEZBQTBGOzs7SUFLMUY7UUFBQTtRQXNCQSxDQUFDO1FBcEJVLHdEQUFXLEdBQWxCO1lBQ0ksRUFBRSxDQUFDLGVBQWUsQ0FBQywyQkFBMkIsR0FBRztnQkFDN0MsSUFBSSxFQUFFLFVBQUMsSUFBSSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWM7b0JBQ3RELElBQUksSUFBSSxHQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBZSxNQUFNLENBQUMsQ0FBQztvQkFFN0QsSUFBSSxzQkFBc0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3JDLElBQUksT0FBTyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUM5QixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBRTVCLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFFMUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ25DLElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQzt3QkFDM0UsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUU1QixPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ25DO2dCQUNMLENBQUM7YUFDSixDQUFDO1FBQ04sQ0FBQztRQUNMLHlDQUFDO0lBQUQsQ0FBQyxBQXRCRCxJQXNCQztJQUVELE9BQVMsa0NBQWtDLENBQUMifQ==