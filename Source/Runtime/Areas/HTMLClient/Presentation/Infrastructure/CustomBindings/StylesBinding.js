/// <amd-module name="Infrastructure/CustomBindings/StylesBinding" />
define("Infrastructure/CustomBindings/StylesBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var StylesBinding = /** @class */ (function () {
        function StylesBinding() {
        }
        StylesBinding.prototype.initBinding = function () {
            ko.bindingHandlers.styles = {
                update: function (node, valueAccessor) {
                    var element = $(node);
                    var value = valueAccessor();
                    var styles = ko.unwrap(value);
                    element.alterClass('c-style-*');
                    for (var i = 0; i < styles.length; i++) {
                        element.addClass('c-style-' + styles[i]);
                    }
                }
            };
        };
        return StylesBinding;
    }());
    return StylesBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3R5bGVzQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlN0eWxlc0JpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFFckU7UUFBQTtRQWlCQSxDQUFDO1FBZlUsbUNBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRztnQkFDeEIsTUFBTSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUF3QjtvQkFDaEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLEtBQUssR0FBRyxhQUFhLEVBQUUsQ0FBQztvQkFDNUIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFN0IsT0FBZSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFekMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ3BDLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUM1QztnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCxvQkFBQztJQUFELENBQUMsQUFqQkQsSUFpQkM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9