/// <amd-module name="Infrastructure/CustomBindings/LoaderBinding" />
define("Infrastructure/CustomBindings/LoaderBinding", ["require", "exports", "Utility/Loader"], function (require, exports, Loader) {
    "use strict";
    var LoaderBinding = /** @class */ (function () {
        function LoaderBinding() {
        }
        LoaderBinding.prototype.initBinding = function () {
            ko.bindingHandlers.loader = {
                update: function (node, valueAccessor, bindings, view, bindingContext) {
                    var element = $(node);
                    var isBusy = ko.unwrap(valueAccessor());
                    var func = isBusy ? 'show' : 'hide';
                    Loader[func](element);
                }
            };
        };
        return LoaderBinding;
    }());
    return LoaderBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9hZGVyQmluZGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkxvYWRlckJpbmRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUVBQXFFOzs7SUFJckU7UUFBQTtRQWFBLENBQUM7UUFYVSxtQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHO2dCQUN4QixNQUFNLEVBQUUsVUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsY0FBYztvQkFDeEQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQ3hDLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBRXBDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUIsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wsb0JBQUM7SUFBRCxDQUFDLEFBYkQsSUFhQztJQUVELE9BQVMsYUFBYSxDQUFDIn0=