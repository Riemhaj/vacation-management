/// <amd-module name="Infrastructure/CustomBindings/ControlBinding" />
define("Infrastructure/CustomBindings/ControlBinding", ["require", "exports", "Controls/Control", "Infrastructure/CustomBindingUtilities"], function (require, exports, Control, Utils) {
    "use strict";
    var ControlBinding = /** @class */ (function () {
        function ControlBinding() {
        }
        ControlBinding.prototype.initBinding = function () {
            ko.bindingHandlers.control = {
                init: function (node, valueAccessor, bindings, view, bindingContext) {
                    var element = $(node);
                    var options = valueAccessor();
                    var uiGenerator = options.uiGenerator;
                    var viewModel = options.viewModel;
                    var defaultDisposingHandler = options.useDefaultDisposing !== false;
                    if (bindingContext.$root != null) {
                        var rootVM = bindingContext.$root.viewModel || bindingContext.$root.uiElement.context;
                        // wenn das HauptViewModel aus dem Context bereits disposed wurde, dann muss auch kein Control mehr gerendert werden!
                        if (rootVM == null || rootVM.isDisposed) {
                            rootVM = null;
                            return;
                        }
                    }
                    if (viewModel == null) {
                        throw 'ViewModel for Control "' + options.uiGenerator + '" is not defined';
                    }
                    if (_.isFunction(viewModel)) {
                        viewModel = viewModel();
                    }
                    var control = Control.createInstance(uiGenerator, viewModel, element);
                    if (control == null) {
                        return;
                    }
                    // e.g. "subcontrol: 'fancyTreeControl'" sets control to the variable 'fancyTreeControl' in the base-control (e.g. MGSelectorControl)
                    // this mechanism enables that a base-control can influence its sub-controls
                    if (options.hasOwnProperty('subcontrol')) {
                        bindingContext.$root.uiElement[options.subcontrol] = control;
                    }
                    if (defaultDisposingHandler) {
                        Utils.registerDisposable(control, bindings, bindingContext);
                    }
                    control.getTuple();
                    ko.utils.domNodeDisposal.addDisposeCallback(node, function () {
                        if (!control.isDisposed && !control.__isDisposing) {
                            control.dispose();
                        }
                        // Hier muss die Referenz auch entfernt werden, wenn das Control bereits disposed wurde!
                        control = null;
                    });
                }
            };
        };
        ;
        return ControlBinding;
    }());
    return ControlBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbEJpbmRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDb250cm9sQmluZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxzRUFBc0U7OztJQWF0RTtRQUFBO1FBeURBLENBQUM7UUF2RFUsb0NBQVcsR0FBbEI7WUFDSSxFQUFFLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRztnQkFDekIsSUFBSSxFQUFFLFVBQUMsSUFBaUIsRUFBRSxhQUE0QixFQUFFLFFBQXFDLEVBQUUsSUFBUyxFQUFFLGNBQXNDO29CQUM1SSxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RCLElBQUksT0FBTyxHQUFHLGFBQWEsRUFBRSxDQUFDO29CQUU5QixJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO29CQUN0QyxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO29CQUNsQyxJQUFJLHVCQUF1QixHQUFHLE9BQU8sQ0FBQyxtQkFBbUIsS0FBSyxLQUFLLENBQUM7b0JBR3BFLElBQUksY0FBYyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7d0JBQzlCLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLGNBQWMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQzt3QkFDdEYscUhBQXFIO3dCQUNySCxJQUFJLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxDQUFDLFVBQVUsRUFBRTs0QkFDckMsTUFBTSxHQUFHLElBQUksQ0FBQzs0QkFDZCxPQUFPO3lCQUNWO3FCQUNKO29CQUVELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTt3QkFDbkIsTUFBTSx5QkFBeUIsR0FBRyxPQUFPLENBQUMsV0FBVyxHQUFHLGtCQUFrQixDQUFDO3FCQUM5RTtvQkFFRCxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ3pCLFNBQVMsR0FBRyxTQUFTLEVBQUUsQ0FBQztxQkFDM0I7b0JBRUQsSUFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUN0RSxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7d0JBQ2pCLE9BQU87cUJBQ1Y7b0JBRUQscUlBQXFJO29CQUNySSw0RUFBNEU7b0JBQzVFLElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsRUFBRTt3QkFDdEMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztxQkFDaEU7b0JBRUQsSUFBSSx1QkFBdUIsRUFBRTt3QkFDekIsS0FBSyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7cUJBQy9EO29CQUVELE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFFbkIsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFO3dCQUM5QyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFFLE9BQWUsQ0FBQyxhQUFhLEVBQUU7NEJBQ3hELE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQzt5QkFDckI7d0JBQ0Qsd0ZBQXdGO3dCQUN4RixPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNuQixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0osQ0FBQTtRQUNMLENBQUM7UUFBQSxDQUFDO1FBQ04scUJBQUM7SUFBRCxDQUFDLEFBekRELElBeURDO0lBRUQsT0FBUyxjQUFjLENBQUMifQ==