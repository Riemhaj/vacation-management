/// <amd-module name="Infrastructure/CustomBindings/AutoLocatorBinding" />
define("Infrastructure/CustomBindings/AutoLocatorBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var AutoLocatorBinding = /** @class */ (function () {
        function AutoLocatorBinding() {
        }
        AutoLocatorBinding.prototype.initBinding = function () {
            ko.bindingHandlers.autoLocator = {
                init: function (element, valueAccessor, bindings, view, bindingContext) {
                    var name = ko.unwrap(valueAccessor());
                    if (name != null && typeof name == 'object') {
                        var parent_1 = name.parent;
                        name = name.name;
                        if (name != null && name != '' && parent_1 != null && parent_1 != '') {
                            name = parent_1 + '.' + name;
                        }
                    }
                    if (name == null || name == '') {
                        return;
                    }
                    var $element = $(element);
                    $element.attr('data-locator', name);
                    var $alreadyAssignedAndDescendants = $element.find('[data-locator]').find('*').addBack();
                    $element.find('*').not($alreadyAssignedAndDescendants).each(function (i, child) {
                        $(child).attr('data-locator', name + '@' + i);
                    });
                }
            };
        };
        return AutoLocatorBinding;
    }());
    return AutoLocatorBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0b0xvY2F0b3JCaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQXV0b0xvY2F0b3JCaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBFQUEwRTs7O0lBRTFFO1FBQUE7UUErQkEsQ0FBQztRQTdCVSx3Q0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHO2dCQUM3QixJQUFJLEVBQUUsVUFBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsY0FBYztvQkFDekQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUV0QyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksT0FBTyxJQUFJLElBQUksUUFBUSxFQUFFO3dCQUN6QyxJQUFJLFFBQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUN6QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDakIsSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFLElBQUksUUFBTSxJQUFJLElBQUksSUFBSSxRQUFNLElBQUksRUFBRSxFQUFFOzRCQUM5RCxJQUFJLEdBQUcsUUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7eUJBQzlCO3FCQUNKO29CQUVELElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksRUFBRSxFQUFFO3dCQUM1QixPQUFPO3FCQUNWO29CQUVELElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFFMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBRXBDLElBQUksOEJBQThCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFFekYsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLEVBQUUsS0FBSzt3QkFDakUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDbEQsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQzthQUNKLENBQUM7UUFDTixDQUFDO1FBQ0wseUJBQUM7SUFBRCxDQUFDLEFBL0JELElBK0JDO0lBRUQsT0FBUyxrQkFBa0IsQ0FBQyJ9