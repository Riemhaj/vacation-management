/// <amd-module name="Infrastructure/CustomBindings/MenuItemIsOpenBinding" />
define("Infrastructure/CustomBindings/MenuItemIsOpenBinding", ["require", "exports"], function (require, exports) {
    "use strict";
    var MenuItemIsOpenBinding = /** @class */ (function () {
        function MenuItemIsOpenBinding() {
        }
        // Der von knockout-kendo beim kendoMenuItem-Binding mitgebrachte Parameter isOpen ist ein One-Way-Binding,
        // d.h. die durch das Widget selbst verursachten Wechsel des Geöffnet-Zustands werden dort nicht an das ViewModel zurückübertragen.
        // Das Two-Way-Binding wird hingegen durch dieses eigene Binding realisiert.
        MenuItemIsOpenBinding.prototype.initBinding = function () {
            ko.bindingHandlers.menuItemIsOpen = {
                init: function (element, valueAccessor) {
                    ko.utils.domData.set(element, 'isOpen', false);
                    var options = ko.unwrap(valueAccessor());
                    var widget = ko.unwrap(options.widget);
                    var state = options.state;
                    widget.bind('open', function (e) {
                        if (e.item == element) {
                            ko.utils.domData.set(element, 'isOpen', true);
                            state(true);
                        }
                    });
                    widget.bind('close', function (e) {
                        if (e.item == element) {
                            ko.utils.domData.set(element, 'isOpen', false);
                            state(false);
                        }
                    });
                },
                update: function (element, valueAccessor) {
                    var itemIsOpen = ko.utils.domData.get(element, 'isOpen');
                    var options = ko.unwrap(valueAccessor());
                    var state = ko.unwrap(options.state);
                    var widget = ko.unwrap(options.widget);
                    if (state != itemIsOpen) {
                        widget[state ? 'open' : 'close'](element);
                    }
                }
            };
        };
        return MenuItemIsOpenBinding;
    }());
    return MenuItemIsOpenBinding;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVudUl0ZW1Jc09wZW5CaW5kaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiTWVudUl0ZW1Jc09wZW5CaW5kaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDZFQUE2RTs7O0lBRTdFO1FBQUE7UUEwQ0EsQ0FBQztRQXhDRywyR0FBMkc7UUFDM0csbUlBQW1JO1FBQ25JLDRFQUE0RTtRQUVyRSwyQ0FBVyxHQUFsQjtZQUNJLEVBQUUsQ0FBQyxlQUFlLENBQUMsY0FBYyxHQUFHO2dCQUNoQyxJQUFJLEVBQUUsVUFBQyxPQUFvQixFQUFFLGFBQXdCO29CQUNqRCxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO29CQUN6QyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdkMsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztvQkFFMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBQyxDQUFDO3dCQUNsQixJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksT0FBTyxFQUFFOzRCQUNuQixFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDOUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNmO29CQUNMLENBQUMsQ0FBQyxDQUFDO29CQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLE9BQU8sRUFBRTs0QkFDbkIsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7NEJBQy9DLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDaEI7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBR1AsQ0FBQztnQkFDRCxNQUFNLEVBQUUsVUFBQyxPQUFvQixFQUFFLGFBQXdCO29CQUNuRCxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUN6RCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7b0JBQ3pDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFFdkMsSUFBSSxLQUFLLElBQUksVUFBVSxFQUFFO3dCQUNyQixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUM3QztnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFDTCw0QkFBQztJQUFELENBQUMsQUExQ0QsSUEwQ0M7SUFFRCxPQUFTLHFCQUFxQixDQUFDIn0=