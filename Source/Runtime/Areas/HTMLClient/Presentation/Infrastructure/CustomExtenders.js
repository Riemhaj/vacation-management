/// <amd-module name="Infrastructure/CustomExtenders" />
define("Infrastructure/CustomExtenders", ["require", "exports", "Infrastructure/CustomExtenders/AsyncExtender", "Infrastructure/CustomExtenders/OverwritableExtender", "Infrastructure/CustomExtenders/PublisherExtender", "Infrastructure/CustomExtenders/TrackChangeExtender", "Infrastructure/CustomExtenders/WaiterExtender"], function (require, exports, Async, Overwritable, Publisher, TrackChange, Waiter) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var customExtenders = [
        new Async(),
        new Overwritable(),
        new Publisher(),
        new TrackChange(),
        new Waiter(),
    ];
    customExtenders.forEach(function (extender) { return extender.AddToKnockout(); });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tRXh0ZW5kZXJzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ3VzdG9tRXh0ZW5kZXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHdEQUF3RDs7OztJQVF4RCxJQUFJLGVBQWUsR0FBRztRQUNsQixJQUFJLEtBQUssRUFBRTtRQUNYLElBQUksWUFBWSxFQUFFO1FBQ2xCLElBQUksU0FBUyxFQUFFO1FBQ2YsSUFBSSxXQUFXLEVBQUU7UUFDakIsSUFBSSxNQUFNLEVBQUU7S0FFVSxDQUFDO0lBRTNCLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsYUFBYSxFQUFFLEVBQXhCLENBQXdCLENBQUMsQ0FBQyJ9