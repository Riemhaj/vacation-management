/// <amd-module name="Infrastructure/CustomBindingUtilities" />
define("Infrastructure/CustomBindingUtilities", ["require", "exports"], function (require, exports) {
    "use strict";
    var CustomBindingUtilities = /** @class */ (function () {
        function CustomBindingUtilities() {
        }
        CustomBindingUtilities.registerDisposable = function (disposable, bindings, bindingContext) {
            var lookup = [bindings()];
            var bc = bindingContext;
            do {
                lookup.push(bc);
                lookup.push(bc.$data);
                bc = bc.$parentContext;
            } while (bc != null);
            var uiElement = null;
            for (var i = 0; i < lookup.length; i++) {
                var scope = lookup[i];
                if (scope == null)
                    continue;
                if (scope.uiElement != null && $.isFunction(scope.uiElement.registerDisposable)) {
                    uiElement = scope.uiElement;
                    break;
                }
            }
            if (uiElement != null) {
                uiElement.registerDisposable(disposable);
            }
            else {
                console.log("Konnte zu folgendem Objekt kein verantwortliches Eltern-Element finden:");
                console.log(disposable);
            }
        };
        return CustomBindingUtilities;
    }());
    return CustomBindingUtilities;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tQmluZGluZ1V0aWxpdGllcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkN1c3RvbUJpbmRpbmdVdGlsaXRpZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0RBQStEOzs7SUFFL0Q7UUFBQTtRQWtDQSxDQUFDO1FBaENpQix5Q0FBa0IsR0FBaEMsVUFBaUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxjQUFjO1lBQ2pFLElBQUksTUFBTSxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUUxQixJQUFJLEVBQUUsR0FBRyxjQUFjLENBQUM7WUFDeEIsR0FBRztnQkFDQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFdEIsRUFBRSxHQUFHLEVBQUUsQ0FBQyxjQUFjLENBQUM7YUFDMUIsUUFDTSxFQUFFLElBQUksSUFBSSxFQUFFO1lBRW5CLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQztZQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDcEMsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLEtBQUssSUFBSSxJQUFJO29CQUFFLFNBQVM7Z0JBRTVCLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7b0JBQzdFLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDO29CQUM1QixNQUFNO2lCQUNUO2FBQ0o7WUFFRCxJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQ25CLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQTthQUMzQztpQkFDSTtnQkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLHlFQUF5RSxDQUFDLENBQUM7Z0JBQ3ZGLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDM0I7UUFDTCxDQUFDO1FBRUwsNkJBQUM7SUFBRCxDQUFDLEFBbENELElBa0NDO0lBR0QsT0FBUyxzQkFBc0IsQ0FBQyJ9