/// <amd-module name="Infrastructure/CustomExtenders/AsyncExtender" />
define("Infrastructure/CustomExtenders/AsyncExtender", ["require", "exports"], function (require, exports) {
    "use strict";
    var AsyncExtender = /** @class */ (function () {
        function AsyncExtender() {
        }
        AsyncExtender.prototype.AddToKnockout = function () {
            var extenders = ko.extenders;
            extenders.async = function (target, initial) {
                var type = _.isArray(initial) ? 'observableArray' : 'observable';
                var plainObservable = ko[type](initial), currentDeferred;
                plainObservable.inProgress = ko.observable(false);
                ko.computed(function () {
                    if (currentDeferred) {
                        currentDeferred.reject();
                        currentDeferred = null;
                    }
                    var newDeferred = target() || initial;
                    if (newDeferred && _.isFunction(newDeferred.done)) {
                        plainObservable.inProgress(true);
                        currentDeferred = $.Deferred().done(function (data) {
                            plainObservable.inProgress(false);
                            plainObservable(data || initial);
                        });
                        newDeferred.done(currentDeferred.resolve);
                    }
                    else {
                        plainObservable(newDeferred);
                    }
                });
                return plainObservable;
            };
        };
        return AsyncExtender;
    }());
    return AsyncExtender;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXN5bmNFeHRlbmRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFzeW5jRXh0ZW5kZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsc0VBQXNFOzs7SUFFdEU7UUFBQTtRQXFDQSxDQUFDO1FBbkNVLHFDQUFhLEdBQXBCO1lBQ0ksSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLFNBQXdDLENBQUM7WUFFNUQsU0FBUyxDQUFDLEtBQUssR0FBRyxVQUFDLE1BQU0sRUFBRSxPQUFPO2dCQUM5QixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO2dCQUNqRSxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsZUFBZSxDQUFDO2dCQUV6RCxlQUFlLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRWxELEVBQUUsQ0FBQyxRQUFRLENBQUM7b0JBQ1IsSUFBSSxlQUFlLEVBQUU7d0JBQ2pCLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDekIsZUFBZSxHQUFHLElBQUksQ0FBQztxQkFDMUI7b0JBRUQsSUFBSSxXQUFXLEdBQUcsTUFBTSxFQUFFLElBQUksT0FBTyxDQUFDO29CQUV0QyxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDL0MsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFFakMsZUFBZSxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJOzRCQUNyQyxlQUFlLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNsQyxlQUFlLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxDQUFDO3dCQUNyQyxDQUFDLENBQUMsQ0FBQzt3QkFFSCxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDN0M7eUJBQU07d0JBQ0gsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUNoQztnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFFSCxPQUFPLGVBQWUsQ0FBQztZQUMzQixDQUFDLENBQUM7UUFDTixDQUFDO1FBRUwsb0JBQUM7SUFBRCxDQUFDLEFBckNELElBcUNDO0lBRUQsT0FBUyxhQUFhLENBQUMifQ==