/// <amd-module name="Infrastructure/CustomExtenders/TrackChangeExtender" />
define("Infrastructure/CustomExtenders/TrackChangeExtender", ["require", "exports", "Utility/ChangeHandler"], function (require, exports, ChangeHandler) {
    "use strict";
    var TrackChangeExtender = /** @class */ (function () {
        function TrackChangeExtender() {
        }
        TrackChangeExtender.prototype.AddToKnockout = function () {
            var extenders = ko.extenders;
            extenders.trackChange = function (target, track) {
                if (track) {
                    target.hasValueChanged = ko.observable(false);
                    target.hasDirtyProperties = ko.observable(false);
                    target.isDirty = ko.computed(function () {
                        return target.hasValueChanged() || target.hasDirtyProperties();
                    });
                    var unwrapped = target();
                    if (unwrapped instanceof Object) {
                        ChangeHandler.traverseObservables(unwrapped, function (obj) {
                            ChangeHandler.applyChangeTrackingToObservable(obj.value);
                            obj.value.isDirty.subscribe(function (isdirty) {
                                if (isdirty) {
                                    target.hasDirtyProperties(true);
                                }
                            });
                        });
                    }
                    target.originalValue = target();
                    target.subscribe(function (newValue) {
                        target.hasValueChanged(newValue != target.originalValue);
                        target.hasValueChanged.valueHasMutated();
                    });
                    if (!target.getChanges) {
                        target.getChanges = function (newObject) {
                            var obj = target();
                            if (obj instanceof Object) {
                                if (target.hasValueChanged()) {
                                    return ko["mapping"].toJS(obj);
                                }
                                return ChangeHandler.getChangesFromModel(obj);
                            }
                            return target();
                        };
                    }
                    if (!target.onChange) {
                        target.onChange = function () {
                            target.hasValueChanged(true);
                        };
                    }
                    if (!target.commit) {
                        target.commit = function () {
                            target.originalValue = target();
                            target.hasValueChanged(false);
                            target.hasDirtyProperties(false);
                        };
                    }
                    if (!target.revert) {
                        target.revert = function () {
                            target(target.originalValue);
                            target.hasValueChanged(false);
                            target.hasDirtyProperties(false);
                        };
                    }
                }
                return target;
            };
        };
        return TrackChangeExtender;
    }());
    return TrackChangeExtender;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVHJhY2tDaGFuZ2VFeHRlbmRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlRyYWNrQ2hhbmdlRXh0ZW5kZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEVBQTRFOzs7SUFJNUU7UUFBQTtRQTRFQSxDQUFDO1FBMUVVLDJDQUFhLEdBQXBCO1lBQ0ksSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLFNBQXdDLENBQUM7WUFFNUQsU0FBUyxDQUFDLFdBQVcsR0FBRyxVQUFDLE1BQStCLEVBQUUsS0FBYztnQkFDcEUsSUFBSSxLQUFLLEVBQUU7b0JBQ1AsTUFBTSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUM5QyxNQUFNLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFakQsTUFBTSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO3dCQUN6QixPQUFPLE1BQU0sQ0FBQyxlQUFlLEVBQUUsSUFBSSxNQUFNLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztvQkFDbkUsQ0FBQyxDQUFDLENBQUM7b0JBRUgsSUFBSSxTQUFTLEdBQUcsTUFBTSxFQUFFLENBQUM7b0JBQ3pCLElBQUksU0FBUyxZQUFZLE1BQU0sRUFBRTt3QkFDN0IsYUFBYSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxVQUFDLEdBQUc7NEJBQzdDLGFBQWEsQ0FBQywrQkFBK0IsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRXpELEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxVQUFDLE9BQU87Z0NBQ2hDLElBQUksT0FBTyxFQUFFO29DQUNULE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDbkM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBRUQsTUFBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLEVBQUUsQ0FBQztvQkFDaEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQVE7d0JBQ3RCLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDekQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDN0MsQ0FBQyxDQUFDLENBQUM7b0JBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUU7d0JBQ3BCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsVUFBQyxTQUFTOzRCQUMxQixJQUFJLEdBQUcsR0FBRyxNQUFNLEVBQUUsQ0FBQzs0QkFDbkIsSUFBSSxHQUFHLFlBQVksTUFBTSxFQUFFO2dDQUN2QixJQUFJLE1BQU0sQ0FBQyxlQUFlLEVBQUUsRUFBRTtvQ0FDMUIsT0FBTyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lDQUNsQztnQ0FDRCxPQUFPLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQzs2QkFDakQ7NEJBRUQsT0FBTyxNQUFNLEVBQUUsQ0FBQzt3QkFDcEIsQ0FBQyxDQUFDO3FCQUNMO29CQUVELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO3dCQUNsQixNQUFNLENBQUMsUUFBUSxHQUFHOzRCQUNkLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2pDLENBQUMsQ0FBQztxQkFDTDtvQkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTt3QkFDaEIsTUFBTSxDQUFDLE1BQU0sR0FBRzs0QkFDWixNQUFNLENBQUMsYUFBYSxHQUFHLE1BQU0sRUFBRSxDQUFDOzRCQUVoQyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUM5QixNQUFNLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JDLENBQUMsQ0FBQztxQkFDTDtvQkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTt3QkFDaEIsTUFBTSxDQUFDLE1BQU0sR0FBRzs0QkFDWixNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUU3QixNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUM5QixNQUFNLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JDLENBQUMsQ0FBQztxQkFDTDtpQkFDSjtnQkFFRCxPQUFPLE1BQU0sQ0FBQztZQUNsQixDQUFDLENBQUM7UUFDTixDQUFDO1FBRUwsMEJBQUM7SUFBRCxDQUFDLEFBNUVELElBNEVDO0lBRUQsT0FBUyxtQkFBbUIsQ0FBQyJ9