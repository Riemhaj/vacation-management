/// <amd-module name="Infrastructure/CustomExtenders/WaiterExtender" />
define("Infrastructure/CustomExtenders/WaiterExtender", ["require", "exports"], function (require, exports) {
    "use strict";
    var WaiterExtender = /** @class */ (function () {
        function WaiterExtender() {
        }
        WaiterExtender.prototype.AddToKnockout = function () {
            var extenders = ko.extenders;
            extenders.waiter = function (target, timeout) {
                var timer = null;
                var currentValue = target();
                var interceptor;
                if (ko.isComputed(target) && !ko.isWriteableObservable(target)) {
                    var constructor = target["push"] != null ? 'observableArray' : 'observable';
                    interceptor = ko[constructor](currentValue).extend({ waiter: timeout });
                    target["subscribe"](interceptor);
                    return interceptor;
                }
                return ko.computed({
                    read: target,
                    write: function (value) {
                        if (timer != null) {
                            window.clearTimeout(timer);
                        }
                        timer = window.setTimeout(function () {
                            target(value);
                        }, timeout);
                    }
                });
            };
        };
        return WaiterExtender;
    }());
    ;
    return WaiterExtender;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2FpdGVyRXh0ZW5kZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJXYWl0ZXJFeHRlbmRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1RUFBdUU7OztJQUV2RTtRQUFBO1FBaUNBLENBQUM7UUEvQlUsc0NBQWEsR0FBcEI7WUFDSSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsU0FBd0MsQ0FBQztZQUU1RCxTQUFTLENBQUMsTUFBTSxHQUFHLFVBQUMsTUFBTSxFQUFFLE9BQU87Z0JBQy9CLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDakIsSUFBSSxZQUFZLEdBQUcsTUFBTSxFQUFFLENBQUM7Z0JBQzVCLElBQUksV0FBVyxDQUFDO2dCQUVoQixJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzVELElBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7b0JBRTVFLFdBQVcsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7b0JBQ3hFLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakMsT0FBTyxXQUFXLENBQUM7aUJBQ3RCO2dCQUVELE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQztvQkFDZixJQUFJLEVBQUUsTUFBTTtvQkFDWixLQUFLLEVBQUUsVUFBQyxLQUFLO3dCQUNULElBQUksS0FBSyxJQUFJLElBQUksRUFBRTs0QkFDZixNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUM5Qjt3QkFFRCxLQUFLLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQzs0QkFDdEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNsQixDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0osQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUVMLHFCQUFDO0lBQUQsQ0FBQyxBQWpDRCxJQWlDQztJQUFBLENBQUM7SUFFRixPQUFTLGNBQWMsQ0FBQyJ9