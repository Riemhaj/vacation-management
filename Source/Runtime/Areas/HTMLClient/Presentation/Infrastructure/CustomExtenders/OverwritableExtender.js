/// <amd-module name="Infrastructure/CustomExtenders/OverwritableExtender" />
define("Infrastructure/CustomExtenders/OverwritableExtender", ["require", "exports"], function (require, exports) {
    "use strict";
    var OverwritableExtender = /** @class */ (function () {
        function OverwritableExtender() {
        }
        OverwritableExtender.prototype.AddToKnockout = function () {
            var extenders = ko.extenders;
            extenders.overwritable = function (target) {
                if (ko.isWriteableObservable(target)) {
                    return target;
                }
                var overwrittenValue = ko.observable();
                return ko.pureComputed({
                    read: function () {
                        var computed = target();
                        var overwritten = overwrittenValue();
                        if (overwritten != null) {
                            return overwritten;
                        }
                        else {
                            return computed;
                        }
                    },
                    write: function (value) {
                        overwrittenValue(value);
                    }
                });
            };
        };
        return OverwritableExtender;
    }());
    ;
    return OverwritableExtender;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3ZlcndyaXRhYmxlRXh0ZW5kZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJPdmVyd3JpdGFibGVFeHRlbmRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw2RUFBNkU7OztJQUU3RTtRQUFBO1FBOEJBLENBQUM7UUE1QlcsNENBQWEsR0FBckI7WUFDSSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsU0FBd0MsQ0FBQztZQUU1RCxTQUFTLENBQUMsWUFBWSxHQUFHLFVBQUMsTUFBTTtnQkFDNUIsSUFBSSxFQUFFLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2xDLE9BQU8sTUFBTSxDQUFDO2lCQUNqQjtnQkFFRCxJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFFdkMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUNuQixJQUFJLEVBQUU7d0JBQ0YsSUFBSSxRQUFRLEdBQUksTUFBYyxFQUFFLENBQUM7d0JBQ2pDLElBQUksV0FBVyxHQUFHLGdCQUFnQixFQUFFLENBQUM7d0JBRXJDLElBQUksV0FBVyxJQUFJLElBQUksRUFBRTs0QkFDckIsT0FBTyxXQUFXLENBQUM7eUJBQ3RCOzZCQUFNOzRCQUNILE9BQU8sUUFBUSxDQUFDO3lCQUNuQjtvQkFDTCxDQUFDO29CQUNELEtBQUssRUFBRSxVQUFDLEtBQUs7d0JBQ1QsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzVCLENBQUM7aUJBQ0osQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUVMLDJCQUFDO0lBQUQsQ0FBQyxBQTlCRCxJQThCQztJQUFBLENBQUM7SUFFRixPQUFTLG9CQUFvQixDQUFDIn0=