/// <amd-module name="Infrastructure/CustomExtenders/PublisherExtender" />
define("Infrastructure/CustomExtenders/PublisherExtender", ["require", "exports"], function (require, exports) {
    "use strict";
    var PublisherExtender = /** @class */ (function () {
        function PublisherExtender() {
        }
        PublisherExtender.prototype.AddToKnockout = function () {
            var extenders = ko.extenders;
            extenders.publisher = function (target) {
                target.args = [];
                target.notifySubscribers = function () {
                    target.args = arguments;
                };
                target.publish = function () {
                    ko.subscribable.fn.notifySubscribers.apply(target, target.args);
                };
                return target;
            };
        };
        return PublisherExtender;
    }());
    return PublisherExtender;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHVibGlzaGVyRXh0ZW5kZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJQdWJsaXNoZXJFeHRlbmRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwwRUFBMEU7OztJQUUxRTtRQUFBO1FBb0JBLENBQUM7UUFsQlUseUNBQWEsR0FBcEI7WUFDSSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsU0FBd0MsQ0FBQztZQUU1RCxTQUFTLENBQUMsU0FBUyxHQUFHLFVBQVUsTUFBTTtnQkFDbEMsTUFBTSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBRWpCLE1BQU0sQ0FBQyxpQkFBaUIsR0FBRztvQkFDdkIsTUFBTSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7Z0JBQzVCLENBQUMsQ0FBQztnQkFFRixNQUFNLENBQUMsT0FBTyxHQUFHO29CQUNiLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwRSxDQUFDLENBQUM7Z0JBRUYsT0FBTyxNQUFNLENBQUM7WUFDbEIsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUVMLHdCQUFDO0lBQUQsQ0FBQyxBQXBCRCxJQW9CQztJQUVELE9BQVMsaUJBQWlCLENBQUMifQ==