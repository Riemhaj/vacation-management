/// <amd-module name="Infrastructure/CustomBindings" />
define("Infrastructure/CustomBindings", ["require", "exports", "Infrastructure/CustomBindings/HotkeyBinding", "Infrastructure/CustomBindings/AlternativeTextBinding", "Infrastructure/CustomBindings/CoreFrameGridBinding", "Infrastructure/CustomBindings/CleanWhitespaceBinding", "Infrastructure/CustomBindings/ErgNameBinding", "Infrastructure/CustomBindings/TechnicalNameBinding", "Infrastructure/CustomBindings/VisibilityBinding", "Infrastructure/CustomBindings/ResizeBinding", "Infrastructure/CustomBindings/ControlBinding", "Infrastructure/CustomBindings/ViewBinding", "Infrastructure/CustomBindings/FactoryBinding", "Infrastructure/CustomBindings/HeightBinding", "Infrastructure/CustomBindings/ControlNameBinding", "Infrastructure/CustomBindings/ControlIdentBinding", "Infrastructure/CustomBindings/ScrollToBinding", "Infrastructure/CustomBindings/CallFktBinding", "Infrastructure/CustomBindings/HighlightTextBinding", "Infrastructure/CustomBindings/HighlightTextArrayBinding", "Infrastructure/CustomBindings/ReadonlyBinding", "Infrastructure/CustomBindings/PressEnterBinding", "Infrastructure/CustomBindings/InteractBinding", "Infrastructure/CustomBindings/StylesBinding", "Infrastructure/CustomBindings/StatesBinding", "Infrastructure/CustomBindings/ClassesBinding", "Infrastructure/CustomBindings/FocusBinding", "Infrastructure/CustomBindings/InvalidBinding", "Infrastructure/CustomBindings/LabelledByBinding", "Infrastructure/CustomBindings/ControlsBinding", "Infrastructure/CustomBindings/DescribedByBinding", "Infrastructure/CustomBindings/CheckableBinding", "Infrastructure/CustomBindings/LabelColumnWidthBinding", "Infrastructure/CustomBindings/MenuItemIsOpenBinding", "Infrastructure/CustomBindings/OptionsLandschaftsGroupsBinding", "Infrastructure/CustomBindings/OptionsLandschaftGroupLinksBinding", "Infrastructure/CustomBindings/CrfRichTextEditorBinding", "Infrastructure/CustomBindings/SlideBinding", "Infrastructure/CustomBindings/HasFocusBinding", "Infrastructure/CustomBindings/LocatorBinding", "Infrastructure/CustomBindings/AutoLocatorBinding", "Infrastructure/CustomBindings/TooltipBinding", "Infrastructure/CustomBindings/LoaderBinding"], function (require, exports, HotkeyBinding, AlternativeTextBinding, CoreFrameGridBinding, CleanWhitespaceBinding, ErgNameBinding, TechnicalNameBinding, VisibilityBinding, ResizeBinding, ControlBinding, ViewBinding, FactoryBinding, HeightBinding, ControlNameBinding, ControlIdentBinding, ScrollToBinding, CallFktBinding, HighlightTextBinding, HighlightTextArrayBinding, ReadonlyBinding, PressEnterBinding, InteractBinding, StylesBinding, StatesBinding, ClassesBinding, FocusBinding, InvalidBinding, LabelledByBinding, ControlsBinding, DescribedByBinding, CheckableBinding, LabelColumnWidthBinding, MenuItemIsOpenBinding, OptionsLandschaftsGroupsBinding, OptionsLandschaftGroupLinksBinding, CrfRichTextEditorBinding, SlideBinding, HasFocusBinding, LocatorBinding, AutoLocatorBinding, TooltipBinding, LoaderBinding) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    [
        new AlternativeTextBinding(),
        new HotkeyBinding(),
        new CoreFrameGridBinding(),
        new CleanWhitespaceBinding(),
        new ErgNameBinding(),
        new TechnicalNameBinding(),
        new VisibilityBinding(),
        new ResizeBinding(),
        new ControlBinding(),
        new ViewBinding(),
        new FactoryBinding(),
        new HeightBinding(),
        new ControlNameBinding(),
        new ControlIdentBinding(),
        new ScrollToBinding(),
        new CallFktBinding(),
        new HighlightTextBinding(),
        new HighlightTextArrayBinding(),
        new ReadonlyBinding(),
        new PressEnterBinding(),
        new InteractBinding(),
        new StylesBinding(),
        new StatesBinding(),
        new ClassesBinding(),
        new FocusBinding(),
        new InvalidBinding(),
        new LabelledByBinding(),
        new ControlsBinding(),
        new DescribedByBinding(),
        new CheckableBinding(),
        new LabelColumnWidthBinding(),
        new MenuItemIsOpenBinding(),
        new OptionsLandschaftsGroupsBinding(),
        new OptionsLandschaftGroupLinksBinding(),
        new CrfRichTextEditorBinding(),
        new SlideBinding(),
        new HasFocusBinding(),
        new LocatorBinding(),
        new AutoLocatorBinding(),
        new TooltipBinding(),
        new LoaderBinding()
    ]
        .forEach(function (binding) { return binding.initBinding(); });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tQmluZGluZ3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDdXN0b21CaW5kaW5ncy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1REFBdUQ7Ozs7SUE0Q3ZEO1FBQ0ksSUFBSSxzQkFBc0IsRUFBRTtRQUM1QixJQUFJLGFBQWEsRUFBRTtRQUNuQixJQUFJLG9CQUFvQixFQUFFO1FBQzFCLElBQUksc0JBQXNCLEVBQUU7UUFDNUIsSUFBSSxjQUFjLEVBQUU7UUFDcEIsSUFBSSxvQkFBb0IsRUFBRTtRQUMxQixJQUFJLGlCQUFpQixFQUFFO1FBQ3ZCLElBQUksYUFBYSxFQUFFO1FBQ25CLElBQUksY0FBYyxFQUFFO1FBQ3BCLElBQUksV0FBVyxFQUFFO1FBQ2pCLElBQUksY0FBYyxFQUFFO1FBQ3BCLElBQUksYUFBYSxFQUFFO1FBQ25CLElBQUksa0JBQWtCLEVBQUU7UUFDeEIsSUFBSSxtQkFBbUIsRUFBRTtRQUN6QixJQUFJLGVBQWUsRUFBRTtRQUNyQixJQUFJLGNBQWMsRUFBRTtRQUNwQixJQUFJLG9CQUFvQixFQUFFO1FBQzFCLElBQUkseUJBQXlCLEVBQUU7UUFDL0IsSUFBSSxlQUFlLEVBQUU7UUFDckIsSUFBSSxpQkFBaUIsRUFBRTtRQUN2QixJQUFJLGVBQWUsRUFBRTtRQUNyQixJQUFJLGFBQWEsRUFBRTtRQUNuQixJQUFJLGFBQWEsRUFBRTtRQUNuQixJQUFJLGNBQWMsRUFBRTtRQUNwQixJQUFJLFlBQVksRUFBRTtRQUNsQixJQUFJLGNBQWMsRUFBRTtRQUNwQixJQUFJLGlCQUFpQixFQUFFO1FBQ3ZCLElBQUksZUFBZSxFQUFFO1FBQ3JCLElBQUksa0JBQWtCLEVBQUU7UUFDeEIsSUFBSSxnQkFBZ0IsRUFBRTtRQUN0QixJQUFJLHVCQUF1QixFQUFFO1FBQzdCLElBQUkscUJBQXFCLEVBQUU7UUFDM0IsSUFBSSwrQkFBK0IsRUFBRTtRQUNyQyxJQUFJLGtDQUFrQyxFQUFFO1FBQ3hDLElBQUksd0JBQXdCLEVBQUU7UUFDOUIsSUFBSSxZQUFZLEVBQUU7UUFDbEIsSUFBSSxlQUFlLEVBQUU7UUFDckIsSUFBSSxjQUFjLEVBQUU7UUFDcEIsSUFBSSxrQkFBa0IsRUFBRTtRQUN4QixJQUFJLGNBQWMsRUFBRTtRQUNwQixJQUFJLGFBQWEsRUFBRTtLQUN0QjtTQUNJLE9BQU8sQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDIn0=