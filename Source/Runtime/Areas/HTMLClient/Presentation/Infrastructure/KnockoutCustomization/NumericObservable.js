/// <amd-module name="Infrastructure/KnockoutCustomization/NumericObservable" />
define("Infrastructure/KnockoutCustomization/NumericObservable", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyCustomization() {
        var customKnockout = ko;
        customKnockout.numericObservable = function (initialValue) {
            var _actual = ko.observable(initialValue);
            var result = ko.computed({
                read: function () {
                    return _actual();
                },
                write: function (newValue) {
                    var parsedValue = parseFloat(newValue);
                    _actual(isNaN(parsedValue) ? newValue : parsedValue);
                }
            });
            return result;
        };
    }
    exports.applyCustomization = applyCustomization;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTnVtZXJpY09ic2VydmFibGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJOdW1lcmljT2JzZXJ2YWJsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRkFBZ0Y7Ozs7SUFFaEY7UUFDSSxJQUFJLGNBQWMsR0FBRyxFQUE4QixDQUFDO1FBRXBELGNBQWMsQ0FBQyxpQkFBaUIsR0FBRyxVQUFDLFlBQVk7WUFDNUMsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUUxQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNyQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxPQUFPLEVBQUUsQ0FBQztnQkFDckIsQ0FBQztnQkFDRCxLQUFLLEVBQUUsVUFBQyxRQUFRO29CQUNaLElBQUksV0FBVyxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDdkMsT0FBTyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekQsQ0FBQzthQUNKLENBQUMsQ0FBQztZQUVILE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFsQkQsZ0RBa0JDO0lBQUEsQ0FBQyJ9