/// <amd-module name="Infrastructure/KnockoutCustomization/ObservableArray" />
define("Infrastructure/KnockoutCustomization/ObservableArray", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyCustomization() {
        ko.observableArray.fn.swap = function (index1, index2) {
            this.valueWillMutate();
            var temp = this()[index1];
            this()[index1] = this()[index2];
            this()[index2] = temp;
            this.valueHasMutated();
        };
    }
    exports.applyCustomization = applyCustomization;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT2JzZXJ2YWJsZUFycmF5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiT2JzZXJ2YWJsZUFycmF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDhFQUE4RTs7OztJQUU5RTtRQUVJLEVBQUUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxVQUFVLE1BQU0sRUFBRSxNQUFNO1lBQ2pELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUV2QixJQUFJLElBQUksR0FBRyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxQixJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7WUFFdEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFYRCxnREFXQztJQUFBLENBQUMifQ==