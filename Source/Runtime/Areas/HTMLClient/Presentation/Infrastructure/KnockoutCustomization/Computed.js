/// <amd-module name="Infrastructure/KnockoutCustomization/Computed" />
define("Infrastructure/KnockoutCustomization/Computed", ["require", "exports", "Infrastructure/KnockoutCustomization/Utilities"], function (require, exports, Utilities_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyCustomization() {
        ko.computed.fn.sync = function (target, func) {
            var primary = this;
            var secondary = target;
            Utilities_1.subscribeObservables(primary, secondary, func);
        };
        ko.computed.fn.subscribeOnce = Utilities_1.subscribeOnce;
    }
    exports.applyCustomization = applyCustomization;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29tcHV0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDb21wdXRlZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1RUFBdUU7Ozs7SUFJdkU7UUFHSSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsVUFBVSxNQUFNLEVBQUUsSUFBSTtZQUN4QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDO1lBRXZCLGdDQUFvQixDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQyxDQUFDO1FBRUYsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsYUFBYSxHQUFHLHlCQUFhLENBQUM7SUFHakQsQ0FBQztJQWJELGdEQWFDO0lBQUEsQ0FBQyJ9