/// <amd-module name="Infrastructure/KnockoutCustomization/AsyncComputed" />
define("Infrastructure/KnockoutCustomization/AsyncComputed", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyCustomization() {
        var customKnockout = ko;
        customKnockout.asyncComputed = function (readFunc, initialValue, suppressInitialExecution) {
            var isInitial = true;
            var trigger = ko.observable().extend({ notify: 'always' });
            var target = ko.computed(function () {
                trigger();
                if (!suppressInitialExecution) {
                    return readFunc();
                }
                else {
                    if (isInitial) {
                        isInitial = false;
                        return initialValue;
                    }
                    else {
                        return readFunc();
                    }
                }
            }).extend({ async: initialValue });
            target.evaluate = function () {
                trigger.valueHasMutated();
            };
            return target;
        };
    }
    exports.applyCustomization = applyCustomization;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXN5bmNDb21wdXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFzeW5jQ29tcHV0ZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEVBQTRFOzs7O0lBRTVFO1FBQ0ksSUFBSSxjQUFjLEdBQUcsRUFBOEIsQ0FBQztRQUVwRCxjQUFjLENBQUMsYUFBYSxHQUFHLFVBQUMsUUFBbUIsRUFBRSxZQUFrQixFQUFFLHdCQUFrQztZQUV2RyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDckIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQzNELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtvQkFDM0IsT0FBTyxRQUFRLEVBQUUsQ0FBQztpQkFDckI7cUJBQ0k7b0JBQ0QsSUFBSSxTQUFTLEVBQUU7d0JBQ1gsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFDbEIsT0FBTyxZQUFZLENBQUM7cUJBQ3ZCO3lCQUNJO3dCQUNELE9BQU8sUUFBUSxFQUFFLENBQUM7cUJBQ3JCO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7WUFFbkMsTUFBTSxDQUFDLFFBQVEsR0FBRztnQkFDZCxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDOUIsQ0FBQyxDQUFDO1lBRUYsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQTdCRCxnREE2QkMifQ==