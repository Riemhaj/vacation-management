/// <amd-module name="Infrastructure/KnockoutCustomization/Observable" />
define("Infrastructure/KnockoutCustomization/Observable", ["require", "exports", "Infrastructure/KnockoutCustomization/Utilities"], function (require, exports, Utilities_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyCustomization() {
        ko.observable.fn.equalityComparer = Utilities_1.compare;
        ko.observable.fn.toggle = function () {
            var value = this();
            this(!value);
            return this;
        };
        ko.observable.fn.sync = function (target, func) {
            var primary = this;
            var secondary = target;
            Utilities_1.subscribeObservables(primary, secondary, func);
        };
        ko.observable.fn.withPausing = function () {
            this.notifySubscribers = function () {
                if (!this.pauseNotifications) {
                    ko.subscribable.fn.notifySubscribers.apply(this, arguments);
                }
            };
            this.sneakyUpdate = function (newValue) {
                this.pauseNotifications = true;
                this(newValue);
                this.pauseNotifications = false;
            };
            return this;
        };
        ko.observable.fn.subscribeOnce = Utilities_1.subscribeOnce;
        ko.observable.fn.defer = function () {
            var deferred = $.Deferred();
            this.subscribeOnce(function (value) {
                deferred.resolve(value);
            });
            return deferred;
        };
    }
    exports.applyCustomization = applyCustomization;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT2JzZXJ2YWJsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk9ic2VydmFibGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEseUVBQXlFOzs7O0lBSXpFO1FBQ0ksRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEdBQUcsbUJBQU8sQ0FBQztRQUU1QyxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEdBQUc7WUFDdEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFYixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUM7UUFFRixFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsVUFBVSxNQUFNLEVBQUUsSUFBSTtZQUMxQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDO1lBRXZCLGdDQUFvQixDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQyxDQUFDO1FBRUYsRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsV0FBVyxHQUFHO1lBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRztnQkFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtvQkFDMUIsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztpQkFDL0Q7WUFDTCxDQUFDLENBQUM7WUFFRixJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsUUFBUTtnQkFDbEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDcEMsQ0FBQyxDQUFDO1lBRUYsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDO1FBRUYsRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsYUFBYSxHQUFHLHlCQUFhLENBQUM7UUFFL0MsRUFBRSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxHQUFHO1lBQ3JCLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUU1QixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsS0FBSztnQkFDOUIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUMzQixDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQztJQUNOLENBQUM7SUE1Q0QsZ0RBNENDO0lBQUEsQ0FBQyJ9