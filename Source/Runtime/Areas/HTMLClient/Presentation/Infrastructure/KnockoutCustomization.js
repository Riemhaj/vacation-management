/// <amd-module name="Infrastructure/KnockoutCustomization" />
define("Infrastructure/KnockoutCustomization", ["require", "exports", "Infrastructure/KnockoutCustomization/Observable", "Infrastructure/KnockoutCustomization/ObservableArray", "Infrastructure/KnockoutCustomization/Computed", "Infrastructure/KnockoutCustomization/AsyncComputed", "Infrastructure/KnockoutCustomization/NumericObservable"], function (require, exports, Observable, ObservableArray, Computed, AsyncComputed, NumericObservable) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    Observable.applyCustomization();
    ObservableArray.applyCustomization();
    Computed.applyCustomization();
    NumericObservable.applyCustomization();
    AsyncComputed.applyCustomization();
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiS25vY2tvdXRDdXN0b21pemF0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiS25vY2tvdXRDdXN0b21pemF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDhEQUE4RDs7OztJQVE5RCxVQUFVLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUNoQyxlQUFlLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUNyQyxRQUFRLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM5QixpQkFBaUIsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQ3ZDLGFBQWEsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDIn0=