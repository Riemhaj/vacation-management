/// <amd-module name="Class" />
define("Class", ["require", "exports", "Management/RADApplication", "Management/ResourceManager", "Management/ComponentManager", "Utility/TypeInfo", "Utility/Interface", "Utility/Messenger", "PrototypeToInstance"], function (require, exports, RADApplication, ResourceManager, ComponentManager, TypeInfo, Interface, Messanger, PrototypeToInstance_1) {
    "use strict";
    var Class = /** @class */ (function () {
        function Class() {
            //#region MESSENGER
            this.__registeredMessageIds = [];
            /*
             * Generelles Disposing
             */
            this.__isDisposing = false;
            // TODO: Nach TS-Portierung entfernen
            this.applyPrototypeToInstance = PrototypeToInstance_1.applyPrototypeToInstance.bind(this);
            this.applyPrototypeToInstance(Class);
            this.isDisposed = false;
            this.isDisposable = false;
            this.disposeProperties = true;
            this.blacklist = ['blacklist', 'type', 'identifier', 'isDisposed', '__isDisposing', 'dispose', 'isDisposable', 'disposeProperties', 'undisposeProperties', 'removeProperty', 'disposeObject', 'validateStatus'];
            this.undisposeProperties = ['type', 'identifier', 'isDisposed', 'isDisposable', '_dispose', 'removeProperty', 'disposeProperties', 'undisposeProperties', 'validateStatus', 'subscriptionCollection'];
            this.triggers = {};
            this.type = TypeInfo.getType(this);
            this.identifier = _.uniqueId(this.type);
            this.subscriptionCollection = [];
            if (RADApplication.profiler != null) {
                RADApplication.profiler.onInstanceCreated(this);
            }
        }
        /**
         * Gibt den übergebenen Parameter oder einen Default Wert zurück
         */
        Class.prototype.paramOrDefault = function (param, def, parsingFunc) {
            if (param != null && parsingFunc) {
                return parsingFunc(param, def);
            }
            if (param != null) {
                return param;
            }
            if (_.isFunction(def)) {
                return def();
            }
            return def;
        };
        /**
         * Prüft, ob dieses Objekt Interfaces mit den angegebenen Namen implementiert.
         * Die Namen müssen so in Interface.definitions auftauchen!
         */
        Class.prototype.implements = function () {
            var args = [this].concat(Array.prototype.slice.call(arguments));
            return Interface.implements.apply(Interface, args);
        };
        /**
         * Validiert den Status von DTO-Objekten
         */
        Class.prototype.validateStatus = function (dto, showMessageBox) {
            if (showMessageBox === void 0) { showMessageBox = true; }
            if (this.isDisposed || !dto) {
                return false;
            }
            var result = true;
            var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            switch (dto.status) {
                case 'Warning':
                    if (showMessageBox) {
                        RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(dto.errorMessage));
                    }
                    result = false;
                    break;
                case 'Error':
                    if (showMessageBox) {
                        RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(dto.errorMessage));
                    }
                    result = false;
                    break;
                case 'Exception':
                    if (showMessageBox) {
                        RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(dto.errorMessage));
                    }
                    result = false;
                    break;
                case 'NoLegal':
                    if (showMessageBox) {
                        RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(ResourceManager.get('LEGAL_ERROR')));
                    }
                    result = false;
                    break;
                case 'SaveRequired':
                    if (showMessageBox) {
                        RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(dto.errorMessage));
                    }
                    result = false;
                    break;
                case 'IsLockedByOther':
                    if (showMessageBox) {
                        if (dto.lockedBy != null) {
                            RADApplication.showMessageBox(MessageBoxViewModel.createLockedByUserMessageBox(dto.lockedBy, dto.lockedObjectIdent));
                        }
                        else {
                            RADApplication.showMessageBox(MessageBoxViewModel.createWarningMessageBox(dto.errorMessage));
                        }
                    }
                    result = false;
                    break;
            }
            if (dto.lockedByUser != null && showMessageBox) {
                RADApplication.showMessageBox(MessageBoxViewModel.createLockedByUserMessageBox(dto.lockedByUser, dto.lockedObjectIdent));
            }
            return result;
        };
        /**
         * Definiert eine asynchrone Ausführung unter Berücksichtigung des Disposing-Mechanismus.
         * Returns the timerId returned from window.setTimeout, which is used in the triggers-dictionary (above).
         */
        Class.prototype.async = function (func, timeout) {
            if (_.isFunction(func)) {
                var dfd_1 = $.Deferred();
                var self_1 = this;
                var timer_1 = window.setTimeout(function () {
                    if (self_1.isDisposed === false) {
                        func.apply(self_1, arguments);
                        dfd_1.resolve();
                        window.clearTimeout(timer_1);
                    }
                }, timeout);
                this.triggers[timer_1] = dfd_1;
                return timer_1;
            }
            return -1;
        };
        /**
         * Wandelt einen übergebenen Parameter in ein Observable um oder liefert ein Observable mit dem Default-Wert
         */
        Class.prototype.observableOrDefault = function (value, def, parsingFunc) {
            if (value != null) {
                if (_.isFunction(value['done'])) {
                    var observable = ko.asyncComputed(function () {
                        return value;
                    }, def);
                    return observable;
                }
                else if (ko.isObservable(value)) {
                    if (ko.isComputed(value)) {
                        return value.extend({ overwritable: true });
                    }
                    else {
                        return value;
                    }
                }
                else {
                    if (parsingFunc) {
                        value = parsingFunc(value, def);
                    }
                    if (_.isArray(value)) {
                        return ko.observableArray(value);
                    }
                    else if (_.isFunction(value)) {
                        return ko.computed(value);
                    }
                    else {
                        return ko.observable(value);
                    }
                }
            }
            else {
                if (_.isArray(def)) {
                    return ko.observableArray(def);
                }
                else if (_.isFunction(def)) {
                    return ko.computed(def);
                }
                else {
                    return ko.observable(def);
                }
            }
        };
        /**
         * Evaluiert den Rückgabewert einer Funktion
         */
        Class.prototype.evaluate = function (value) {
            var deferred = $.Deferred();
            if (value && _.isFunction(value.done)) {
                value.done(function (value) {
                    deferred.resolve(value);
                });
            }
            else {
                deferred.resolve(value);
            }
            return deferred;
        };
        /*
         * Erzeugt einen Default-Wert von einem Typ
         */
        Class.prototype.defaultFromType = function (type) {
            switch (type) {
                case 'string':
                case 'String':
                    return '';
                case 'bool':
                case 'Boolean':
                    return false;
                case 'int':
                case 'Integer':
                case 'long':
                case 'float':
                case 'double':
                    return 0;
                case 'array':
                    return [];
                case 'object':
                case 'dictionary':
                    return {};
                default:
                    return null;
            }
        };
        /*
         * Gibt den übergebenen Handler oder eine leere Funktion zurück
         */
        Class.prototype.handlerOrDefault = function (func) {
            if (func != null) {
                return func;
            }
            else {
                var emptyFunc = function () { };
                emptyFunc["isEmpty"] = true;
                return emptyFunc;
            }
        };
        Class.prototype.synchronizeComputed = function (computed, sync) {
            if (sync == null || !ko.isObservable(sync)) {
                return;
            }
            if (ko.isWriteableObservable(computed)) {
                // Als erstes den Wert in das Ziel-computed schreiben
                // Grund: Diese Methode wird oft benutzt, um ein neues Computed mit einem GenericObject-Observable (sync) in Verbindung zu bringen
                //        Falls nun computed aus irgendeinem Grund nach dem Schreiben des Wertes beim Lesen nicht den gleichen Wert wieder zurückgibt,
                //        so soll das GenericObject davon nicht direkt beeinflusst werden.
                //        Grundsätzlich könnte man auch voraussetzen, dass computed(sync()) == sync() ist und daher ein _vorhergehendes_ subscribe unnötig ist.
                computed(sync());
                // Zudem weitere Änderungen an sync ab jetzt ebenfalls an Ziel-computed weiterleiten
                var syncSub = sync.subscribe(function (value) {
                    var current = computed();
                    if (value !== current) {
                        computed(value);
                    }
                });
                this.subscriptionCollection.push(syncSub);
            }
            if (ko.isWriteableObservable(sync)) {
                // Änderungen am Ziel-Computed zurück an sync schicken
                var compSub = computed.subscribe(function (value) {
                    var current = sync();
                    if (value !== current) {
                        sync(value);
                    }
                });
                this.subscriptionCollection.push(compSub);
            }
        };
        /**
         * Definiert ein neues Computed-Property und registriert es in der computedCollection
         */
        Class.prototype.defineComputed = function (func, sync) {
            var computed = ko.computed(func);
            this.subscriptionCollection.push(computed);
            this.synchronizeComputed(computed, sync);
            return computed;
        };
        Class.prototype.addComputed = function (computed) {
            if (computed && this.subscriptionCollection.indexOf(computed) == -1) {
                this.subscriptionCollection.push(computed);
            }
        };
        /*
         * Definiert ein neues PureComputed-Property und registriert es in der computedCollection
         */
        Class.prototype.definePureComputed = function (obj, sync) {
            var computed = ko.pureComputed(obj);
            this.subscriptionCollection.push(computed);
            this.synchronizeComputed(computed, sync);
            return computed;
        };
        /*
         * Definiert ein neues Subscription-Property und registriert es in der subscriptionCollection.
         * Gibt als die Funktion zurück, die auch durch die Subscription aufgerufen wird (dies erlaubt
         * die Schreibweise this.doIt = this.defineSubscription(observable, function(val) { ... });
         * mit späterem Aufruf this.doIt(customValue);
         */
        Class.prototype.defineSubscription = function (observable, func, disposeImmediately, eventType) {
            var _this = this;
            if (func != null) {
                if (eventType == undefined) {
                    eventType = 'change';
                }
                var subscription_1 = observable.subscribe(function (value) {
                    if (disposeImmediately === true) {
                        subscription_1.dispose();
                        var idx = _this.subscriptionCollection.indexOf(subscription_1);
                        if (idx > -1) {
                            _this.subscriptionCollection.splice(idx, 1);
                        }
                    }
                    func(value);
                }, eventType);
                this.subscriptionCollection.push(subscription_1);
                return func;
            }
            return null;
        };
        /**
         * Erzeugt einen Operationsparameter
         */
        Class.prototype.buildOperationParameter = function (dotNetTypeName, value) {
            return 'DotNetTypeName:' + dotNetTypeName + ';Value:' + value;
        };
        /**
         * Liefert ein vergleichbares Objekt der aktuellen Instanz
         */
        Class.prototype.equalityComparer = function () {
            if (this.equalityProperties == null) {
                return null;
            }
            var comparable = {};
            var properties = this.equalityProperties.concat(['type']);
            for (var i = 0; i < properties.length; i++) {
                var propValue = this[properties[i]];
                if (_.isArray(propValue)) {
                    var newProp = [];
                    for (var j = 0; j < propValue.length; j++) {
                        if (propValue[j] && propValue[j].equalityComparer != null) {
                            newProp[j] = propValue[j].equalityComparer();
                        }
                    }
                    comparable[properties[i]] = newProp;
                }
                else {
                    var newProp = ko.toJS(propValue);
                    if (newProp && newProp["equalityComparer"] != null) {
                        newProp = newProp["equalityComparer"]();
                    }
                    comparable[properties[i]] = newProp;
                }
            }
            return comparable;
        };
        /**
         * Registriert das Objekt für eine Message. Diese wird vom Disposing automatisch deregistriert.
         * @param messageId ID der Message
         * @param callback Wird aufgerufen, bei dem empfangen einer Message
         * @param receiveLastMessageImmediately Wenn TRUE, wird die zuletzt gesendete Message direkt empfangen, auch wenn diese bereits vor dem registrieren gesendet wurde
         */
        Class.prototype.registerForMessageId = function (messageId, callback, receiveLastMessageImmediately) {
            if (receiveLastMessageImmediately === void 0) { receiveLastMessageImmediately = false; }
            if (this.__registeredMessageIds.indexOf(messageId) < 0) {
                this.__registeredMessageIds.push(messageId);
            }
            if (receiveLastMessageImmediately) {
                Messanger.registerForMessageIdAndReceive(messageId, callback, this);
            }
            else {
                Messanger.registerForMessageId(messageId, callback, this);
            }
        };
        /**
         * Deregistriert sich für eine bestimmte MessageId
         * @param messageId ID der Message
         * @param callback OPTIONAL: Wenn nicht angegeben werden alle Callbacks dieser MessageId entfernt
         */
        Class.prototype.unregisterForMessageId = function (messageId, callback) {
            if (callback === void 0) { callback = null; }
            var firstIndex = this.__registeredMessageIds.indexOf(messageId);
            if (firstIndex >= 0) {
                this.__registeredMessageIds.splice(firstIndex, 1);
            }
            Messanger.unregisterForMessageId(messageId, callback, this);
        };
        Class.prototype.unregisterAllMessageIds = function () {
            var ids = _.uniq(this.__registeredMessageIds);
            for (var i in ids) {
                this.unregisterForMessageId(ids[i]);
            }
            this.__registeredMessageIds = [];
        };
        //#endregion
        /*
         * Disposed ein beliebiges Objekt
         */
        Class.prototype.disposeObject = function (value, disposePlain) {
            if (value != null) {
                if (_.isArray(value)) {
                    for (var i = 0; i < value.length; i++) {
                        this.disposeObject(value[i]);
                    }
                }
                else if (ko.isObservable(value)) {
                    var realValue = value.peek();
                    this.disposeObject(realValue);
                    for (var event_1 in value._subscriptions) {
                        for (var k = 0; k < value._subscriptions[event_1].length; k++) {
                            value._subscriptions[event_1][k].dispose();
                        }
                    }
                }
                else if (value.isDisposable === true && value.dispose != null && !value.isDisposed && !value.__isDisposing) {
                    value.dispose();
                }
                else if (value.destroy != null && value.element != null) {
                    try {
                        value.destroy();
                    }
                    catch (e) { }
                }
                else if (value instanceof jQuery || value instanceof HTMLElement) {
                    if (value instanceof HTMLElement) {
                        ko.cleanNode(value);
                    }
                    else {
                        value.each(function (index, elem) {
                            ko.cleanNode(elem);
                        });
                        value.remove();
                    }
                }
                else if (disposePlain !== false && jQuery.isPlainObject(value)) {
                    for (var key in value) {
                        this.disposeObject(value[key]);
                    }
                }
            }
        };
        /*
         * Entfernt ein Property aus der Instanz
         */
        Class.prototype.removeProperty = function (property) {
            if (this.blacklist.indexOf(property) === -1) {
                var propertyDescriptor = Object.getOwnPropertyDescriptor(this, property);
                // Überprüfen, ob Getter und Setter genutzt wurden und nur ein Getter vorhanden ist
                if (propertyDescriptor && propertyDescriptor.get && !propertyDescriptor.set) {
                    delete this[property];
                }
                // ansonsten: Überprüfen, ob das Property gesetzt werden kann
                else if (propertyDescriptor && !propertyDescriptor.writable) {
                    // Wenn nicht, muss das Property kurzzeitig auf schreibbar gestellt werden
                    propertyDescriptor.writable = true;
                    this[property] = null;
                    // Wieder zurücksetzen
                    propertyDescriptor.writable = false;
                }
                // Standardfall
                else if (propertyDescriptor) {
                    this[property] = null;
                }
            }
        };
        /*
         * Hebt alle  bisher registrierten Subscriptions wieder auf.
         */
        Class.prototype.disposeSubscriptions = function () {
            if (this.subscriptionCollection == null) {
                return;
            }
            for (var i = 0; i < this.subscriptionCollection.length; i++) {
                var subscription = this.subscriptionCollection[i];
                if (subscription["_state"] && subscription["_state"].isSleeping) {
                    subscription["_state"].isSleeping = false;
                }
                if ($.isFunction(subscription.dispose))
                    subscription.dispose();
            }
            this.subscriptionCollection = [];
        };
        Class.prototype._dispose = function () {
            if (this.__isDisposing) {
                return;
            }
            this.__isDisposing = true;
            this.unregisterAllMessageIds();
            this.disposeSubscriptions();
            if (this.disposeProperties !== false) {
                for (var property in this) {
                    if ((this.disposeProperties === true || this.disposeProperties.indexOf(property) > -1)
                        && this.undisposeProperties.indexOf(property) === -1
                        && !property.startsWith("__")) {
                        this.disposeObject(this[property], false);
                    }
                    this.removeProperty(property);
                }
            }
            this.isDisposed = true;
            this.__isDisposing = null;
            if (RADApplication.profiler != null) {
                RADApplication.profiler.onInstanceDisposed(this);
            }
        };
        /*
        * Generelles Disposing
        */
        Class.prototype.dispose = function () {
            if (this.isDisposed || this.__isDisposing) {
                return;
            }
            this._dispose();
        };
        return Class;
    }());
    return Class;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2xhc3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDbGFzcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwrQkFBK0I7OztJQWUvQjtRQXFESTtZQXdZQSxtQkFBbUI7WUFDWCwyQkFBc0IsR0FBa0IsRUFBRSxDQUFDO1lBb0luRDs7ZUFFRztZQUNLLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1lBL2dCMUIscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyw4Q0FBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXJDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztZQUNoTixJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxNQUFNLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLGdCQUFnQixFQUFFLHdCQUF3QixDQUFDLENBQUM7WUFDdE0sSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztZQUVqQyxJQUFJLGNBQWMsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUNqQyxjQUFjLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ25EO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ksOEJBQWMsR0FBckIsVUFBeUIsS0FBUSxFQUFFLEdBQU8sRUFBRSxXQUF1QztZQUMvRSxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksV0FBVyxFQUFFO2dCQUM5QixPQUFPLFdBQVcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDbEM7WUFFRCxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2YsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ25CLE9BQU8sR0FBRyxFQUFFLENBQUM7YUFDaEI7WUFFRCxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUM7UUFFRDs7O1dBR0c7UUFDSSwwQkFBVSxHQUFqQjtZQUNJLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLE9BQU8sU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELENBQUM7UUFFRDs7V0FFRztRQUNJLDhCQUFjLEdBQXJCLFVBQXNCLEdBQVEsRUFBRSxjQUE4QjtZQUE5QiwrQkFBQSxFQUFBLHFCQUE4QjtZQUMxRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBQ3pCLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUE4QixxQkFBcUIsQ0FBQyxDQUFDO1lBRTVHLFFBQVEsR0FBRyxDQUFDLE1BQU0sRUFBRTtnQkFDaEIsS0FBSyxTQUFTO29CQUNWLElBQUksY0FBYyxFQUFFO3dCQUNoQixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3FCQUNoRztvQkFFRCxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNmLE1BQU07Z0JBRVYsS0FBSyxPQUFPO29CQUNSLElBQUksY0FBYyxFQUFFO3dCQUNoQixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3FCQUNoRztvQkFFRCxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNmLE1BQU07Z0JBRVYsS0FBSyxXQUFXO29CQUNaLElBQUksY0FBYyxFQUFFO3dCQUNoQixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3FCQUNoRztvQkFFRCxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNmLE1BQU07Z0JBRVYsS0FBSyxTQUFTO29CQUNWLElBQUksY0FBYyxFQUFFO3dCQUNoQixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNsSDtvQkFFRCxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNmLE1BQU07Z0JBRVYsS0FBSyxjQUFjO29CQUNmLElBQUksY0FBYyxFQUFFO3dCQUNoQixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3FCQUNoRztvQkFFRCxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNmLE1BQU07Z0JBRVYsS0FBSyxpQkFBaUI7b0JBQ2xCLElBQUksY0FBYyxFQUFFO3dCQUNoQixJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFOzRCQUN0QixjQUFjLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLDRCQUE0QixDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzt5QkFDeEg7NkJBQU07NEJBQ0gsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt5QkFDaEc7cUJBQ0o7b0JBRUQsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDZixNQUFNO2FBQ2I7WUFFRCxJQUFJLEdBQUcsQ0FBQyxZQUFZLElBQUksSUFBSSxJQUFJLGNBQWMsRUFBRTtnQkFDNUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyw0QkFBNEIsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7YUFDNUg7WUFFRCxPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBRUQ7OztXQUdHO1FBQ0kscUJBQUssR0FBWixVQUFhLElBQWMsRUFBRSxPQUFnQjtZQUN6QyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksS0FBRyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixJQUFJLE9BQUssR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO29CQUMxQixJQUFJLE1BQUksQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO3dCQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQUksRUFBRSxTQUFTLENBQUMsQ0FBQzt3QkFDNUIsS0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUNkLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBSyxDQUFDLENBQUM7cUJBQzlCO2dCQUNMLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFFWixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQUssQ0FBQyxHQUFHLEtBQUcsQ0FBQztnQkFFM0IsT0FBTyxPQUFLLENBQUM7YUFDaEI7WUFFRCxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2QsQ0FBQztRQUVEOztXQUVHO1FBQ0ksbUNBQW1CLEdBQTFCLFVBQThCLEtBQWdDLEVBQUUsR0FBTyxFQUFFLFdBQXVDO1lBQzVHLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDZixJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUU7b0JBQzdCLElBQUksVUFBVSxHQUFJLEVBQStCLENBQUMsYUFBYSxDQUFDO3dCQUM1RCxPQUFPLEtBQUssQ0FBQztvQkFDakIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUVSLE9BQU8sVUFBVSxDQUFDO2lCQUNyQjtxQkFDSSxJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQzdCLElBQUksRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDdEIsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7cUJBQy9DO3lCQUFNO3dCQUNILE9BQU8sS0FBSyxDQUFDO3FCQUNoQjtpQkFDSjtxQkFDSTtvQkFDRCxJQUFJLFdBQVcsRUFBRTt3QkFDYixLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztxQkFDbkM7b0JBRUQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNsQixPQUFPLEVBQUUsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUE0QixDQUFDO3FCQUMvRDt5QkFBTSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQzVCLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFZLENBQUMsQ0FBQztxQkFDcEM7eUJBQU07d0JBQ0gsT0FBTyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQjtpQkFDSjthQUNKO2lCQUNJO2dCQUNELElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDaEIsT0FBTyxFQUFFLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBNEIsQ0FBQztpQkFDN0Q7cUJBQU0sSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUMxQixPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsR0FBVSxDQUFDLENBQUM7aUJBQ2xDO3FCQUFNO29CQUNILE9BQU8sRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDN0I7YUFDSjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLHdCQUFRLEdBQWYsVUFBbUIsS0FBVTtZQUN6QixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFNUIsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ25DLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFLO29CQUNiLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzVCLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMzQjtZQUVELE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7UUFFRDs7V0FFRztRQUNJLCtCQUFlLEdBQXRCLFVBQXVCLElBQWM7WUFDakMsUUFBUSxJQUFJLEVBQUU7Z0JBQ1YsS0FBSyxRQUFRLENBQUM7Z0JBQUMsS0FBSyxRQUFRO29CQUN4QixPQUFPLEVBQUUsQ0FBQztnQkFFZCxLQUFLLE1BQU0sQ0FBQztnQkFBQyxLQUFLLFNBQVM7b0JBQ3ZCLE9BQU8sS0FBSyxDQUFDO2dCQUVqQixLQUFLLEtBQUssQ0FBQztnQkFBQyxLQUFLLFNBQVMsQ0FBQztnQkFBQyxLQUFLLE1BQU0sQ0FBQztnQkFBQyxLQUFLLE9BQU8sQ0FBQztnQkFBQyxLQUFLLFFBQVE7b0JBQ2hFLE9BQU8sQ0FBQyxDQUFDO2dCQUViLEtBQUssT0FBTztvQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFFZCxLQUFLLFFBQVEsQ0FBQztnQkFBQyxLQUFLLFlBQVk7b0JBQzVCLE9BQU8sRUFBRSxDQUFDO2dCQUVkO29CQUNJLE9BQU8sSUFBSSxDQUFDO2FBQ25CO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ksZ0NBQWdCLEdBQXZCLFVBQTJCLElBQU87WUFDOUIsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0gsSUFBSSxTQUFTLEdBQUcsY0FBUSxDQUFDLENBQUM7Z0JBQzFCLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBRTVCLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1FBQ0wsQ0FBQztRQUVPLG1DQUFtQixHQUEzQixVQUE0QixRQUFRLEVBQUUsSUFBSTtZQUN0QyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN4QyxPQUFPO2FBQ1Y7WUFFRCxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEMscURBQXFEO2dCQUNyRCxrSUFBa0k7Z0JBQ2xJLHNJQUFzSTtnQkFDdEksMEVBQTBFO2dCQUMxRSwrSUFBK0k7Z0JBRS9JLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUVqQixvRkFBb0Y7Z0JBQ3BGLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFLO29CQUMvQixJQUFJLE9BQU8sR0FBRyxRQUFRLEVBQUUsQ0FBQztvQkFFekIsSUFBSSxLQUFLLEtBQUssT0FBTyxFQUFFO3dCQUNuQixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ25CO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFFN0M7WUFFRCxJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEMsc0RBQXNEO2dCQUN0RCxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBSztvQkFDbkMsSUFBSSxPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUM7b0JBRXJCLElBQUksS0FBSyxLQUFLLE9BQU8sRUFBRTt3QkFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNmO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDN0M7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSw4QkFBYyxHQUFyQixVQUFzQixJQUFTLEVBQUUsSUFBOEI7WUFDM0QsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTNDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFekMsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQztRQUVNLDJCQUFXLEdBQWxCLFVBQW1CLFFBQStCO1lBQzlDLElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQ2pFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDOUM7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSxrQ0FBa0IsR0FBekIsVUFBMEIsR0FBUSxFQUFFLElBQThCO1lBQzlELElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUUzQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRXpDLE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7UUFFRDs7Ozs7V0FLRztRQUNJLGtDQUFrQixHQUF6QixVQUEwQixVQUFtQyxFQUFFLElBQTJCLEVBQUUsa0JBQTRCLEVBQUUsU0FBZTtZQUF6SSxpQkEwQkM7WUF6QkcsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUVkLElBQUksU0FBUyxJQUFJLFNBQVMsRUFBRTtvQkFDeEIsU0FBUyxHQUFHLFFBQVEsQ0FBQztpQkFDeEI7Z0JBRUQsSUFBSSxjQUFZLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQUs7b0JBQzFDLElBQUksa0JBQWtCLEtBQUssSUFBSSxFQUFFO3dCQUM3QixjQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBRXZCLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsY0FBWSxDQUFDLENBQUM7d0JBQzVELElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFOzRCQUNWLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO3lCQUM5QztxQkFDSjtvQkFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFFZCxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGNBQVksQ0FBQyxDQUFDO2dCQUUvQyxPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ0ksdUNBQXVCLEdBQTlCLFVBQStCLGNBQXNCLEVBQUUsS0FBVTtZQUM3RCxPQUFPLGlCQUFpQixHQUFHLGNBQWMsR0FBRyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xFLENBQUM7UUFFRDs7V0FFRztRQUNJLGdDQUFnQixHQUF2QjtZQUNJLElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksRUFBRTtnQkFDakMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUUxRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDeEMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVwQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7b0JBQ3RCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFFakIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ3ZDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7NEJBQ3ZELE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzt5QkFDaEQ7cUJBQ0o7b0JBRUQsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztpQkFDdkM7cUJBQU07b0JBQ0gsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFFakMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLGtCQUFrQixDQUFDLElBQUksSUFBSSxFQUFFO3dCQUNoRCxPQUFPLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQztxQkFDM0M7b0JBRUQsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztpQkFDdkM7YUFDSjtZQUVELE9BQU8sVUFBVSxDQUFDO1FBQ3RCLENBQUM7UUFJRDs7Ozs7V0FLRztRQUNJLG9DQUFvQixHQUEzQixVQUFnRCxTQUFpQixFQUFFLFFBQTBCLEVBQUUsNkJBQThDO1lBQTlDLDhDQUFBLEVBQUEscUNBQThDO1lBQ3pJLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDL0M7WUFFRCxJQUFJLDZCQUE2QixFQUFFO2dCQUMvQixTQUFTLENBQUMsOEJBQThCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUN2RTtpQkFDSTtnQkFDRCxTQUFTLENBQUMsb0JBQW9CLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM3RDtRQUNMLENBQUM7UUFFRDs7OztXQUlHO1FBQ0ksc0NBQXNCLEdBQTdCLFVBQWtELFNBQWlCLEVBQUUsUUFBaUM7WUFBakMseUJBQUEsRUFBQSxlQUFpQztZQUNsRyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2hFLElBQUksVUFBVSxJQUFJLENBQUMsRUFBRTtnQkFDakIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDckQ7WUFDRCxTQUFTLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNoRSxDQUFDO1FBRU8sdUNBQXVCLEdBQS9CO1lBQ0ksSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUM5QyxLQUFLLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRTtnQkFDZixJQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLENBQUM7UUFDRCxZQUFZO1FBR1o7O1dBRUc7UUFDSSw2QkFBYSxHQUFwQixVQUFxQixLQUFVLEVBQUUsWUFBc0I7WUFDbkQsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUNmLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2hDO2lCQUNKO3FCQUFNLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDL0IsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUU5QixLQUFLLElBQUksT0FBSyxJQUFJLEtBQUssQ0FBQyxjQUFjLEVBQUU7d0JBQ3BDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDekQsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt5QkFDNUM7cUJBQ0o7aUJBQ0o7cUJBQU0sSUFBSSxLQUFLLENBQUMsWUFBWSxLQUFLLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFO29CQUMxRyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQ25CO3FCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7b0JBQ3ZELElBQUk7d0JBQ0EsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO3FCQUNuQjtvQkFBQyxPQUFPLENBQUMsRUFBRSxHQUFHO2lCQUNsQjtxQkFBTSxJQUFJLEtBQUssWUFBWSxNQUFNLElBQUksS0FBSyxZQUFZLFdBQVcsRUFBRTtvQkFFaEUsSUFBSSxLQUFLLFlBQVksV0FBVyxFQUFFO3dCQUM5QixFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2Qjt5QkFDSTt3QkFDRCxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBYSxFQUFFLElBQWlCOzRCQUN4QyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN2QixDQUFDLENBQUMsQ0FBQzt3QkFFSCxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7cUJBQ2xCO2lCQUNKO3FCQUFNLElBQUksWUFBWSxLQUFLLEtBQUssSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUM5RCxLQUFLLElBQUksR0FBRyxJQUFJLEtBQUssRUFBRTt3QkFDbkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztxQkFDbEM7aUJBQ0o7YUFDSjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLDhCQUFjLEdBQXJCLFVBQXNCLFFBQWdCO1lBQ2xDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksa0JBQWtCLEdBQUcsTUFBTSxDQUFDLHdCQUF3QixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFFekUsbUZBQW1GO2dCQUNuRixJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRTtvQkFDekUsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3pCO2dCQUNELDZEQUE2RDtxQkFDeEQsSUFBSSxrQkFBa0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRTtvQkFDekQsMEVBQTBFO29CQUMxRSxrQkFBa0IsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNuQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUN0QixzQkFBc0I7b0JBQ3RCLGtCQUFrQixDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7aUJBQ3ZDO2dCQUNELGVBQWU7cUJBQ1YsSUFBSSxrQkFBa0IsRUFBRTtvQkFDekIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDekI7YUFDSjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLG9DQUFvQixHQUEzQjtZQUNJLElBQUksSUFBSSxDQUFDLHNCQUFzQixJQUFJLElBQUksRUFBRTtnQkFDckMsT0FBTzthQUNWO1lBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsRUFBRTtvQkFDN0QsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7aUJBQzdDO2dCQUNELElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO29CQUNsQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLENBQUM7UUFNTSx3QkFBUSxHQUFmO1lBQ0ksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwQixPQUFPO2FBQ1Y7WUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztZQUUxQixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUU1QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxLQUFLLEVBQUU7Z0JBQ2xDLEtBQUssSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksSUFBSyxJQUFJLENBQUMsaUJBQXlCLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzJCQUN4RixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzsyQkFDakQsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztxQkFDN0M7b0JBRUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDakM7YUFDSjtZQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBRTFCLElBQUksY0FBYyxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUU7Z0JBQ2pDLGNBQWMsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDcEQ7UUFDTCxDQUFDO1FBRUQ7O1VBRUU7UUFDSyx1QkFBTyxHQUFkO1lBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3ZDLE9BQU87YUFDVjtZQUNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVwQixDQUFDO1FBQ0wsWUFBQztJQUFELENBQUMsQUE3bUJELElBNm1CQztJQUVELE9BQVMsS0FBSyxDQUFDIn0=