/// <amd-module name="Views/View" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Views/View", ["require", "exports", "BindingContext", "Management/ComponentManager", "Management/RADApplication", "Management/ResourceManager", "Utility/StorageHelper", "Utility/ImageHandler", "Views/ViewTuple"], function (require, exports, BindingContext, ComponentManager, RADApplication, ResourceManager, StorageHelper, ImageHandler, ViewTuple) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller Views
    */
    var View = /** @class */ (function (_super) {
        __extends(View, _super);
        function View() {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(View);
            return _this;
        }
        View.prototype.getView = function () {
            throw new Error('Die Funktion "getView" wurde nicht implementiert');
        };
        /*
        * Erzeugt das ViewTuple und die View
        */
        View.prototype.getTuple = function () {
            this.tuple = new ViewTuple(this);
            return this.tuple;
        };
        /*
         * Wendet Bindings auf ein Template an
         */
        View.prototype.applyBindings = function (element, viewModel, html) {
            var parsedHTML = ImageHandler.parseHTML(html);
            var view = element.append($(parsedHTML));
            ko.applyBindings({
                uiElement: this,
                viewModel: viewModel,
                RADApplication: RADApplication,
                ResourceManager: ResourceManager,
                StorageHelper: StorageHelper
            }, view[0]);
            ko.applyBindingsToNode(view[0], {
                loader: viewModel.isBusy
            });
            return view;
        };
        /*
         * Erzeugt eine neue Instanz einer View anhand einer Komponentenbezeichnung
         */
        View.createInstance = function (component, viewModel, element) {
            var Component = ComponentManager.getView(component);
            if (Component == null) {
                throw 'View-Komponente "' + component + '" wurde nicht gefunden.';
            }
            return new Component(viewModel, element);
        };
        return View;
    }(BindingContext));
    return View;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlldy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlZpZXcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQW9DOzs7Ozs7Ozs7Ozs7O0lBV3BDOztNQUVFO0lBQ0Y7UUFBbUIsd0JBQWM7UUFJN0I7WUFBQSxZQUNJLGlCQUFPLFNBSVY7WUFGRyxxQ0FBcUM7WUFDckMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxDQUFDOztRQUN4QyxDQUFDO1FBRU0sc0JBQU8sR0FBZDtZQUNJLE1BQU0sSUFBSSxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztRQUN4RSxDQUFDO1FBRUQ7O1VBRUU7UUFDSyx1QkFBUSxHQUFmO1lBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQztRQUVEOztXQUVHO1FBQ0ksNEJBQWEsR0FBcEIsVUFBcUIsT0FBNEIsRUFBRSxTQUF3QixFQUFFLElBQVk7WUFDckYsSUFBSSxVQUFVLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBRXpDLEVBQUUsQ0FBQyxhQUFhLENBQUM7Z0JBQ2IsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLGNBQWMsRUFBRSxjQUFjO2dCQUM5QixlQUFlLEVBQUUsZUFBZTtnQkFDaEMsYUFBYSxFQUFFLGFBQWE7YUFDL0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVaLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzVCLE1BQU0sRUFBRSxTQUFTLENBQUMsTUFBTTthQUMzQixDQUFDLENBQUM7WUFFSCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyxtQkFBYyxHQUE1QixVQUE2QixTQUFpQixFQUFFLFNBQXdCLEVBQUUsT0FBNEI7WUFDbEcsSUFBSSxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBUSxDQUFDO1lBRTNELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDbkIsTUFBTSxtQkFBbUIsR0FBRyxTQUFTLEdBQUcseUJBQXlCLENBQUM7YUFDckU7WUFFRCxPQUFPLElBQUksU0FBUyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUM3QyxDQUFDO1FBQ0wsV0FBQztJQUFELENBQUMsQUF6REQsQ0FBbUIsY0FBYyxHQXlEaEM7SUFFRCxPQUFTLElBQUksQ0FBQyJ9