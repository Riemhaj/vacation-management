/// <amd-module name="Views/ViewTuple" />
define("Views/ViewTuple", ["require", "exports"], function (require, exports) {
    "use strict";
    var ViewTuple = /** @class */ (function () {
        function ViewTuple(view) {
            this.view = view.getView();
        }
        return ViewTuple;
    }());
    return ViewTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVmlld1R1cGxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiVmlld1R1cGxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHlDQUF5Qzs7O0lBSXpDO1FBSUksbUJBQVksSUFBVTtZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMvQixDQUFDO1FBQ0wsZ0JBQUM7SUFBRCxDQUFDLEFBUEQsSUFPQztJQUVELE9BQVMsU0FBUyxDQUFDIn0=