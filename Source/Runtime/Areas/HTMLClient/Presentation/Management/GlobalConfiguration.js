///<amd-module name="Management/GlobalConfiguration" />
define("Management/GlobalConfiguration", ["require", "exports"], function (require, exports) {
    "use strict";
    return {
        visitedModules: ["Base"],
        listView: {
            ShowToolbarText: true,
            ShowCountInHeader: true,
            filterConfiguration: {}
        },
        detailView: {
            AlwaysCloseDetailViewContext: false,
            ShowNoLastSavedViewAfterDVC: false
        },
        viewExtension: {
            ShowContextHeader: true
        },
        dataGrid: {
            ShowCountInFooter: false,
            MinHeightForAssociationGridInDV: 185,
            HeightForAssociationGridInDV: '185px'
        },
        multiLineTextBox: {
            MinHeight: 120,
            DefaultHeight: 120
        },
        summaryView: {},
        inlineImages: {
            FILTER: 'funnel.svg',
            FILTER_OFF: 'c_funnel_off.svg',
            OBJ_SELECT: 'link.svg',
            OBJ_CREATE: 'add.svg',
            OBJ_DELETE: 'garbage.svg',
            OBJ_EDIT: 'pencil.svg',
            OBJ_DISCONNECT: 'link_broken.svg',
            MOVE_UP: 'arrow_up.svg',
            MOVE_DOWN: 'arrow_down.svg',
            MLSTRING_EDIT: 'message.svg',
            LV_FIND: 'lv_find.png',
            LV_TABLE: 'table_selection_column.svg',
            REFRESH: 'arrow_circle2.svg',
            MEDIUM_UPLOAD: 'sspick.png',
            MEDIUM_DOWNLOAD: 'ssopen.png',
            MEDIUM_DELETE: 'ssdisc.png'
        },
        window: {
            ShowMessageWindowCrossButton: false
        }
    };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR2xvYmFsQ29uZmlndXJhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkdsb2JhbENvbmZpZ3VyYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdURBQXVEOzs7SUFFdkQsT0FBUztRQUNMLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQztRQUN4QixRQUFRLEVBQUU7WUFDTixlQUFlLEVBQUUsSUFBSTtZQUNyQixpQkFBaUIsRUFBRSxJQUFJO1lBRXZCLG1CQUFtQixFQUFFLEVBRXBCO1NBQ0o7UUFDRCxVQUFVLEVBQUU7WUFDUiw0QkFBNEIsRUFBRSxLQUFLO1lBQ25DLDJCQUEyQixFQUFFLEtBQUs7U0FDckM7UUFDRCxhQUFhLEVBQUU7WUFDWCxpQkFBaUIsRUFBRSxJQUFJO1NBQzFCO1FBQ0QsUUFBUSxFQUFFO1lBQ04saUJBQWlCLEVBQUUsS0FBSztZQUN4QiwrQkFBK0IsRUFBRSxHQUFHO1lBQ3BDLDRCQUE0QixFQUFFLE9BQU87U0FDeEM7UUFDRCxnQkFBZ0IsRUFBRTtZQUNkLFNBQVMsRUFBRSxHQUFHO1lBQ2QsYUFBYSxFQUFFLEdBQUc7U0FDckI7UUFDRCxXQUFXLEVBQUUsRUFDWjtRQUNELFlBQVksRUFBRTtZQUNWLE1BQU0sRUFBRSxZQUFZO1lBQ3BCLFVBQVUsRUFBRSxrQkFBa0I7WUFDOUIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsVUFBVSxFQUFFLFNBQVM7WUFDckIsVUFBVSxFQUFFLGFBQWE7WUFDekIsUUFBUSxFQUFFLFlBQVk7WUFDdEIsY0FBYyxFQUFFLGlCQUFpQjtZQUVqQyxPQUFPLEVBQUUsY0FBYztZQUN2QixTQUFTLEVBQUUsZ0JBQWdCO1lBRTNCLGFBQWEsRUFBRSxhQUFhO1lBRTVCLE9BQU8sRUFBRSxhQUFhO1lBQ3RCLFFBQVEsRUFBRSw0QkFBNEI7WUFFdEMsT0FBTyxFQUFFLG1CQUFtQjtZQUU1QixhQUFhLEVBQUUsWUFBWTtZQUMzQixlQUFlLEVBQUUsWUFBWTtZQUM3QixhQUFhLEVBQUUsWUFBWTtTQUM5QjtRQUNELE1BQU0sRUFBRTtZQUNKLDRCQUE0QixFQUFFLEtBQUs7U0FDdEM7S0FDSixDQUFDIn0=