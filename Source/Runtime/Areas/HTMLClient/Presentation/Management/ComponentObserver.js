///<amd-module name="Management/ComponentObserver" />
define("Management/ComponentObserver", ["require", "exports"], function (require, exports) {
    "use strict";
    var ComponentObserver = /** @class */ (function () {
        function ComponentObserver() {
        }
        ComponentObserver.onControlInit = function (identifier) {
            ComponentObserver.createSubscription();
            ComponentObserver.controls.push(identifier);
        };
        ComponentObserver.onControlReady = function (identifier) {
            ComponentObserver.controls.splice(ComponentObserver.controls.indexOf(identifier), 1);
        };
        ComponentObserver.createSubscription = function () {
            if (ComponentObserver.subscription == undefined)
                ComponentObserver.subscription = ComponentObserver.computed.subscribe(ComponentObserver.resolveAllDeferreds);
        };
        ComponentObserver.resolveAllDeferreds = function () {
            ComponentObserver.defs.forEach(function (d) { return d.resolve(); });
            ComponentObserver.defs = [];
        };
        ComponentObserver.whenAllComponentsAreReady = function () {
            var d = $.Deferred();
            ComponentObserver.defs.push(d);
            return d.promise();
        };
        ComponentObserver.controls = ko.observableArray([]);
        ComponentObserver.computed = ko.computed(function () { return ComponentObserver.controls(); }).extend({ throttle: 100 });
        ComponentObserver.defs = [];
        return ComponentObserver;
    }());
    return ComponentObserver;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29tcG9uZW50T2JzZXJ2ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJDb21wb25lbnRPYnNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxxREFBcUQ7OztJQUVyRDtRQUFBO1FBaUNBLENBQUM7UUExQmlCLCtCQUFhLEdBQTNCLFVBQTRCLFVBQWtCO1lBQzFDLGlCQUFpQixDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDdkMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBRWEsZ0NBQWMsR0FBNUIsVUFBNkIsVUFBa0I7WUFDM0MsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3pGLENBQUM7UUFFYyxvQ0FBa0IsR0FBakM7WUFDSSxJQUFJLGlCQUFpQixDQUFDLFlBQVksSUFBSSxTQUFTO2dCQUMzQyxpQkFBaUIsQ0FBQyxZQUFZLEdBQUcsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3JILENBQUM7UUFFYyxxQ0FBbUIsR0FBbEM7WUFDSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFYLENBQVcsQ0FBQyxDQUFDO1lBQ25ELGlCQUFpQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDaEMsQ0FBQztRQUVhLDJDQUF5QixHQUF2QztZQUNJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUVyQixpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRS9CLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUE3QmMsMEJBQVEsR0FBb0MsRUFBRSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuRSwwQkFBUSxHQUFvQyxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQVEsT0FBTyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2xJLHNCQUFJLEdBQStCLEVBQUUsQ0FBQztRQTRCekQsd0JBQUM7S0FBQSxBQWpDRCxJQWlDQztJQUNELE9BQVMsaUJBQWlCLENBQUEifQ==