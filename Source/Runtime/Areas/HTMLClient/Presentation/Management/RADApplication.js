///<amd-module name="Management/RADApplication" />
define("Management/RADApplication", ["require", "exports", "Utility/Loader", "Utility/Profiler", "Management/ComponentManager"], function (require, exports, Loader, Profiler, ComponentManager) {
    "use strict";
    /**
     * Repräsentiert die RAD-Anwendung
     * Initialisiert wichtige Komponenten wie ResourceManager und NavigationService und beinhaltet Informationen über die Anwendung
     */
    var RADApplication = /** @class */ (function () {
        function RADApplication() {
            RADApplication.appName = ko.observable();
            RADApplication.description = ko.observable();
            RADApplication.ergName = ko.observable();
            RADApplication.rootLayoutVM = null;
            RADApplication.isSystemLocked = ko.observable(false);
            RADApplication.lastCacheUpdate = ko.observable();
            RADApplication.lastRightCacheUpdate = ko.observable();
            RADApplication.principalTypeName = ko.observable();
            RADApplication.systemLockMessage = ko.observable();
            RADApplication.isForceLogout = ko.observable(false);
            RADApplication.isChangePrincipalEnabled = ko.observable(true);
            RADApplication._templateStyle = ko.observable(MODEL.Style + '_HTML5');
            RADApplication.hasShowLicenceWarning = false;
            RADApplication.templateStyle = (function () {
                return ko.pureComputed({
                    read: function () {
                        var value = RADApplication._templateStyle();
                        $('html').addClass('styleTemplate-' + value.replace('_HTML5', ''));
                        return value.replace('_HTML5', '');
                    },
                    write: function (value) {
                        var originalValue = value;
                        value = value + '_HTML5';
                        // BugFix für FF: veränderliches Token in URL sorgt dafür, dass der Stil auf jeden Fall neu geladen wird
                        var oldSource = $('#styles').attr('href');
                        var newSource = RADApplication.radEnvironmentTemplateHandler + '?TemplateName=' + value + '&StylePath=Styles&_ts=' + (new Date().getTime());
                        $('html').removeClass('styleTemplate-' + RADApplication._templateStyle().replace('_HTML5', ''));
                        $('html').addClass('styleTemplate-' + value.replace('_HTML5', ''));
                        RADApplication._templateStyle(value);
                        Loader.while(function () {
                            // BugFix für IE: Beim Anwenden des Styles kam es zu Phantom-Hover-Effekten; hierdurch werden diese verhindert
                            document.body.style.pointerEvents = 'none';
                            $('#styles').attr('href', newSource);
                            // Wir wollen ein Callback für das Ereignis "CSS geladen" haben; leider funktioniert dies nicht browser-übergreifend
                            // HACK:
                            // Der einfachste Trick ist: Einfach ein Bild erzeugen, das die CSS-Daten als Quelle hat; wenn die Daten ankommen, merkt der
                            // Browser, dass sie ungültig sind und löst onerror aus. Zu dem Zeitpunkt sollte das CSS definitiv im Style gelandet und verarbeitet sein.
                            var img = new Image();
                            img.onerror = function () {
                                window.setTimeout(function () {
                                    // Der IE aktualisiert einige Layouts nicht nach dem Stil-Update;
                                    // dies forciert einige Re-Layouts
                                    $(document.body).css('display', 'none').css('display', '');
                                    // Der FF aktualisiert einige SVG-Farbverläufe nicht nach dem Stil-Update
                                    $("svg [fill]").each(function () {
                                        var fill = $(this).attr("fill");
                                        $(this).attr("fill", "").attr("fill", fill);
                                    });
                                    // Dies ermöglicht ggf. komponentenspezifische Fixes (z.B. mittels Reinitialisierung/Rendering) für das Stil-Update
                                    var Messenger = requirejs('Utility/Messenger');
                                    Messenger.createMessageAndSend('TemplateStyleChangedMessage', { style: originalValue });
                                    document.body.style.pointerEvents = ''; // Maus-Ereignisse wieder erlauben
                                }, 10);
                            };
                            img.src = newSource;
                        });
                    }
                });
            })();
            RADApplication._iconUrl = ko.observable($('#favicon').attr('href'));
            RADApplication.iconUrl = (function () {
                return ko.pureComputed({
                    read: RADApplication._iconUrl,
                    write: function (value) {
                        value = RADApplication.templateStyle() + '_HTML5';
                        var oldSource = $('#favicon').attr('href');
                        var newSource = RADApplication.radEnvironmentTemplateHandler + '?TemplateName=' + value + '&ImageName=favicon.ico&_ts=' + Date.now();
                        if (oldSource !== newSource) {
                            $('#favicon').attr('href', newSource);
                            RADApplication._iconUrl(newSource);
                        }
                    }
                });
            })();
            RADApplication.tooltip = ko.observable();
            RADApplication.configuration = null;
            RADApplication.menuSprite = null;
            RADApplication.menuSpriteLoaded = null;
            RADApplication.user = null;
            RADApplication.principal = null;
            RADApplication.deployedModules = ko.observable();
            RADApplication.pdApplicationChangedTime = ko.observable();
            RADApplication.licensedModules = ko.observable();
            RADApplication.isRADlicensed = ko.computed(function () {
                var licensedModules = RADApplication.licensedModules();
                if (!licensedModules) {
                    return false;
                }
                return licensedModules.indexOf('RAD') !== -1;
            });
            RADApplication.expiredModules = ko.observable();
            RADApplication.modules = ko.observable();
            RADApplication.currentListViewContext = ko.observable(null);
            RADApplication.currentSummaryViewContext = ko.observable(null);
            RADApplication.currentDetailViewContext = ko.observable(null);
            RADApplication.currentViewFactory = ko.observable(null);
            RADApplication.defaultPersistenceContextId = ko.observable(null);
            RADApplication.uiInterceptors = [];
            RADApplication.isInitialised = false;
            if (MODEL.ServerURL == '') {
                var browserUrl = window.location.href;
                if (MODEL.StartViewName && MODEL.StartViewName != '') {
                    var idxStartView = browserUrl.lastIndexOf('/' + MODEL.StartViewName);
                    if (idxStartView > -1) {
                        browserUrl = browserUrl.substr(0, idxStartView);
                    }
                }
                //Endet die URL mit einem "/" ?
                if (browserUrl.indexOf('/', browserUrl.length - 1) == -1) {
                    browserUrl += "/";
                }
                RADApplication.serverUrl = browserUrl + MODEL.HTMLClientBaseURL;
            }
            else {
                RADApplication.serverUrl = MODEL.ServerURL + MODEL.HTMLClientBaseURL;
            }
            MODEL.ServerURL = RADApplication.serverUrl;
            RADApplication.radEnvironmentServiceUrl = MODEL.HTMLClientBaseURL + MODEL.RADEnvironmentServiceURL;
            RADApplication.radEnvironmentTemplateHandler = MODEL.HTMLClientBaseURL + MODEL.RADEnvironmentTemplateServiceURL;
            RADApplication.radEnvironmentDataHandler = MODEL.HTMLClientBaseURL + MODEL.RADEnvironmentDataServiceURL;
            RADApplication.radEnvironmentMediumHandler = MODEL.HTMLClientBaseURL + MODEL.RADEnvironmentMediumServiceURL;
            RADApplication.radEnvironmentViewExtensionHandler = MODEL.HTMLClientBaseURL + MODEL.RADEnvironmentViewExtensionServiceURL;
            RADApplication.token = MODEL.Token;
            RADApplication.xcsrfToken = MODEL.XCSRFToken;
            RADApplication.ssoUserName = MODEL.SSOUserName;
            RADApplication.profiler = null;
            RADApplication.modalWindowOpenCounter = 0;
            RADApplication.showingUnauthorizedRequestCallback = false;
            RADApplication.showingNoConnectionToServerCallback = false;
        } // end of constructor
        /**
         * URL für den View-Extension-Serivces des RAD-Environments
         * Verweist intern auf den ResourceManager welcher die aktuelle Sprache verwaltet
         */
        RADApplication.currentLanguage = function () {
            var ResourceManager = requirejs('Management/ResourceManager');
            return ko.toJS(ResourceManager.language.value);
        };
        /**
         * Beinhalte alle übersetzten standard ResourceItems für eine RAD-Anwendung
         * Verweist intern auf den ResourceManager welcher die Resource-Elemente verwaltet
         */
        RADApplication.resourceItemValues = function () {
            var ResourceManager = requirejs('Management/ResourceManager');
            return ko.toJS(ResourceManager.resources);
        };
        /**
         * Öffnet eine MessageBox
         */
        RADApplication.isModalWindowOpen = function () {
            return RADApplication.modalWindowOpenCounter > 0;
        };
        RADApplication.showMessageBox = function (messageBoxViewModel, isNavigatable) {
            isNavigatable = !!isNavigatable;
            var NavigationService = requirejs('Management/NavigationService');
            // Die Erzeugung einer modalen MessageBox soll immer isoliert (also ohne Abhängigkeiten) zum NavigationService geschehen.
            // Somit bleibt beim Öffnen der MessageBox die aktive ViewFactory erhalten und wird nicht durch die MessageBox überschrieben.
            var messageBoxFactory = NavigationService.loadViewFactoryIsolated('MessageBox', {
                viewModel: messageBoxViewModel
            }, true);
            if (messageBoxViewModel.window.isModal() && !isNavigatable) {
                RADApplication.modalWindowOpenCounter++;
            }
            var disposeBox = function () {
                if (!isNavigatable) {
                    RADApplication.modalWindowOpenCounter--;
                }
                NavigationService.removeViewFactoryIsolated(messageBoxFactory);
                window.setTimeout(function () {
                    if (!messageBoxViewModel.isDisposed) {
                        messageBoxViewModel.dispose();
                    }
                    messageBoxViewModel = null;
                }, 1); // dies sofort auszuführen kann registrierte close-Handler stören
            };
            var onClose;
            var focusedElementBefore = document.activeElement;
            if (messageBoxViewModel.window.onClose != null) {
                var originalOnClose_1 = messageBoxViewModel.window.onClose;
                onClose = function () {
                    originalOnClose_1();
                    disposeBox();
                    $(focusedElementBefore).focus();
                };
            }
            else {
                onClose = function () {
                    disposeBox();
                    $(focusedElementBefore).focus();
                };
            }
            messageBoxViewModel.window.onClose = onClose;
        };
        /**
         * Wird aufgerufen, nachdem die Anwendung gestartet wurde.
         * Hinweis: Die Function 'callback' muss zwingend aufgerufen werden.
         */
        RADApplication.onAfterStartup = function (callback) {
            var countInterceptors = RADApplication.uiInterceptors.length;
            if (countInterceptors === 0) {
                callback();
                return;
            }
            var continueCounter = 0;
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].afterStartup(function () {
                    if (++continueCounter == countInterceptors) {
                        callback();
                    }
                });
            }
        };
        /**
         * Wird aufgerufen, nachdem ein Benutzer angemeldet wurde.
         * Hinweis: Die Function 'callback' muss zwingend aufgerufen werden.
         */
        RADApplication.onAfterLogin = function (callback) {
            var countInterceptors = RADApplication.uiInterceptors.length;
            if (countInterceptors === 0) {
                callback();
                return;
            }
            var continueCounter = 0;
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].afterLogin(function () {
                    if (++continueCounter == countInterceptors) {
                        callback();
                    }
                });
            }
        };
        /**
         * Wird aufgerufen, bevor das Rootlayout geladen oder aktualisiert wird.
         */
        RADApplication.onBeforeLoadRootLayout = function () {
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].beforeLoadRootLayout();
            }
        };
        /**
         * Wird aufgerufen, sobald das Rootlayout geladen wurde.
         */
        RADApplication.onAfterLoadRootLayout = function () {
            // das RootLayoutViewModel herranziehen, wenn möglich
            var wireframeDOM = $('[data-view=Wireframe]')[0];
            if (wireframeDOM) {
                var koData = ko.dataFor(wireframeDOM);
                if (koData && koData.viewModel) {
                    RADApplication.rootLayoutVM = koData.viewModel;
                }
            }
            RADApplication.showExpiredModulesMessage();
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].afterLoadRootLayout();
            }
        };
        /**
         * Wird aufgerufen bevor der Menübaum geladen wird.
         */
        RADApplication.onBeforeLoadMenuTree = function (rootNode) {
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].beforeLoadMenuTree(rootNode);
            }
        };
        /**
         * Wird aufgerufen bevor der Navigationsbaum geladen wird.
         */
        RADApplication.onBeforeLoadNavigationTree = function (rootNode) {
            for (var i = 0; i < RADApplication.uiInterceptors.length; i++) {
                RADApplication.uiInterceptors[i].beforeLoadNavigationTree(rootNode);
            }
        };
        /**
         * Methode, welche nach einem Login die Login-Informationen setzt
         */
        RADApplication.afterLogin = function (loginDTO, principalDTO) {
            // Die Sprache muss direkt nach dem Login gesetzt werden,
            // damit bei einem direkten CallOperation des Clients auch die ClientInfo bereits über die ausgewählte Sprache informiert ist.
            var ResourceManager = requirejs('Management/ResourceManager');
            ResourceManager.setLanguage();
            RADApplication.onAfterLogin(function () {
                var KeepAliveHandler = requirejs('Management/KeepAliveHandler');
                var NavigationService = requirejs('Management/NavigationService');
                var FrontController = requirejs('Management/FrontController');
                RADApplication.principal.setPrincipalInfo(principalDTO);
                RADApplication.user.setLoginInfo(loginDTO);
                RADApplication.lastRightCacheUpdate(loginDTO.rightCacheCreated);
                KeepAliveHandler.startWatchDog();
                FrontController.init();
                NavigationService.navigateToRedirectingAddress();
                if (loginDTO.clientInfo.user.changePasswordWithNextLogin === true) {
                    NavigationService.loadViewFactory('ChangePassword', { allowClose: false });
                }
            });
        };
        /**
         * Methode, welche nach dem Logout zum Login navigiert
         */
        RADApplication.afterLogout = function () {
            var KeepAliveHandler = requirejs('Management/KeepAliveHandler');
            var NavigationService = requirejs('Management/NavigationService');
            if (RADApplication.user != null && RADApplication.user.isLoggedIn !== undefined)
                RADApplication.user.isLoggedIn(false);
            if (KeepAliveHandler != null && KeepAliveHandler.stopWatchDog !== undefined)
                KeepAliveHandler.stopWatchDog();
            if (NavigationService != null && NavigationService.reload !== undefined) {
                //Nun wird der Client neugeladen..
                NavigationService.reload();
            }
        };
        /**
         * Gibt an, ob eine genannte Systemvariable gesetzt und auf True gestellt ist
         */
        RADApplication.isSystemParamSetAndTrue = function (name) {
            if (name in RADApplication.configuration.systemProperties) {
                var param = RADApplication.configuration.systemProperties[name];
                return param === true || param === 'True' || param === 'true';
            }
            return false;
        };
        /**
         * Liefert den Wert einer Systemvariablen
         */
        RADApplication.getSystemParam = function (name, parsingFunc) {
            if (name in RADApplication.configuration.systemProperties) {
                var param = RADApplication.configuration.systemProperties[name];
                if (parsingFunc) {
                    param = parsingFunc(param);
                }
                return param;
            }
            return null;
        };
        /**
         * Registrert Keyboard-Events
         */
        RADApplication.manageKeyboard = function () {
            $(document).bind('keyup', function (event) {
                var templateStyle = RADApplication.templateStyle();
                if (event.keyCode === kendo.keys.F10) {
                    RADApplication.templateStyle(null);
                    RADApplication.templateStyle(templateStyle);
                }
                if (event.keyCode === kendo.keys.F2) {
                    RADApplication.toggleProfiler();
                }
            });
            // unterbindet das markieren des gesamten textinhalts in der anwendung
            // davon ausgeschlossen sind jedoch inputfelder
            Mousetrap.bind(['mod+a'], function (e) {
                return false;
            });
        };
        /**
         * Initialisiert den Profiler
         */
        RADApplication.toggleProfiler = function () {
            var NavigationService = requirejs('Management/NavigationService');
            if (RADApplication.profiler == null) {
                RADApplication.profiler = new Profiler();
                NavigationService.loadViewFactoryIsolated('Debug', null, true);
            }
            else {
                RADApplication.profiler = null;
                NavigationService.cleanRegion('Debug');
            }
        };
        /**
         * Führt ein Cache-Update durch
         */
        RADApplication.updateCache = function () {
            var RADEnvironmentProxy = requirejs('Communication/RADEnvironmentProxy');
            var Messenger = requirejs('Utility/Messenger');
            return RADEnvironmentProxy.updateClientCache({
                level: 'Full'
            }).then(function (clientUpdate) {
                Loader.show();
                if (clientUpdate.clientInfo != null) {
                    RADApplication.user.setClientInfo(clientUpdate.clientInfo);
                }
                if (clientUpdate.applicationMenus != null) {
                    if (clientUpdate.applicationMenus.userNavigationTree != null) {
                        RADApplication.onBeforeLoadNavigationTree(clientUpdate.applicationMenus.userNavigationTree);
                    }
                    if (clientUpdate.applicationMenus.userMenuTree != null) {
                        RADApplication.onBeforeLoadMenuTree(clientUpdate.applicationMenus.userMenuTree);
                    }
                    RADApplication.user.menus(clientUpdate.applicationMenus);
                }
                if (clientUpdate.configuration != null) {
                    RADApplication.configuration = clientUpdate.configuration;
                }
                if (clientUpdate.principalInfo != null) {
                    RADApplication.principal.setPrincipalInfo(clientUpdate.principalInfo);
                }
                RADApplication.onBeforeLoadRootLayout();
                Messenger.createMessageAndSend('RefreshRootLayoutMessage');
                RADApplication.onAfterLoadRootLayout();
                Messenger.createMessageAndSend('CacheUpdatedMessage');
                Loader.hide();
                return;
            });
        };
        RADApplication.updateAllResourceItems = function () {
            var ResourceManager = requirejs('Management/ResourceManager');
            return ResourceManager.loadLanguageArtfacts();
        };
        RADApplication.unauthorizedRequestCallback = function () {
            if (RADApplication.showingUnauthorizedRequestCallback === true) {
                return;
            }
            RADApplication.isForceLogout(true);
            RADApplication.showingUnauthorizedRequestCallback = true;
            var msgVM = {};
            var ComponentManager = requirejs('Management/ComponentManager');
            var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            var ResourceManager = requirejs('Management/ResourceManager');
            //So verhindert wir, dass Loader hängen bleiben und kein Logout stattfindet
            Loader.removeAll();
            msgVM = MessageBoxViewModel.createWarningMessageBox(ResourceManager.get('SESSION_ERROR'), function () {
                RADApplication.afterLogout();
            }, {});
            RADApplication.showMessageBox(msgVM);
        };
        RADApplication.noConnectionToServerCallback = function () {
            if (RADApplication.showingNoConnectionToServerCallback === true) {
                return;
            }
            RADApplication.isForceLogout(true);
            RADApplication.showingNoConnectionToServerCallback = true;
            var msgVM = {};
            var ComponentManager = requirejs('Management/ComponentManager');
            var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            var ResourceManager = requirejs('Management/ResourceManager');
            //So verhindert wir, dass Loader hängen bleiben und kein Logout stattfindet
            Loader.removeAll();
            var msg = ResourceManager.get('COMMUNICATION_ERROR');
            if (msg == 'COMMUNICATION_ERROR')
                msg = "The service is temporarily unavailable. Please try again later.";
            msgVM = MessageBoxViewModel.createWarningMessageBox(msg, function () {
                RADApplication.afterLogout();
            }, {});
            RADApplication.showMessageBox(msgVM);
        };
        /**
         * Instanziiert alle geladenen UIInterceptoren
         */
        RADApplication.loadUIInterceptors = function () {
            RADApplication.uiInterceptors = [];
            var ComponentManager = requirejs('Management/ComponentManager');
            var uiInterceptors = ComponentManager.getUIInterceptors();
            for (var i = 0; i < uiInterceptors.length; i++) {
                RADApplication.uiInterceptors.push(new uiInterceptors[i]());
            }
        };
        /**
         * Initialisiert die Anwendung
         */
        RADApplication.init = function () {
            requirejs(['Communication/RADEnvironmentProxy'], function (RADEnvironmentProxy) {
                RADApplication.loadUIInterceptors();
                RADEnvironmentProxy.getApplication().then(function (applicationDTO) {
                    RADApplication.configuration = applicationDTO.configuration;
                    RADApplication.pdApplicationChangedTime(applicationDTO.pdApplicationChangedTime);
                    RADApplication.deployedModules(applicationDTO.deployedModules);
                    RADApplication.manageKeyboard();
                    RADApplication.description(applicationDTO.description);
                    RADApplication.ergName(applicationDTO.ergName);
                    RADApplication.expiredModules(applicationDTO.expiredModules);
                    RADApplication.iconUrl(applicationDTO.iconUrl);
                    RADApplication.isSystemLocked(applicationDTO.isSystemLocked);
                    RADApplication.lastCacheUpdate(applicationDTO.lastCacheUpdate);
                    RADApplication.licensedModules(applicationDTO.licensedModules);
                    RADApplication.modules(applicationDTO.modules);
                    RADApplication.appName(applicationDTO.name);
                    RADApplication.principalTypeName(applicationDTO.principalTypeName);
                    RADApplication.systemLockMessage(applicationDTO.systemLockMessage);
                    RADApplication.tooltip(applicationDTO.tooltip);
                    requirejs(['Model/User', 'Model/Principal', 'Management/NavigationService', 'Management/ResourceManager', 'Utility/StorageHelper'], function (User, Principal, NavigationService, ResourceManager, StorageHelper) {
                        RADApplication.user = new User();
                        RADApplication.principal = new Principal();
                        var reloadWithoutCache = StorageHelper.checkLocalStorageRefresh();
                        if (reloadWithoutCache === true) {
                            document.location.reload(true);
                        }
                        else {
                            ResourceManager.init().then(function () {
                                RADApplication.onAfterStartup(function () {
                                    RADApplication.isInitialised = true;
                                    NavigationService.init();
                                });
                            });
                        }
                    });
                }).fail(function (param) {
                    console.log("An error has occurred while getting the application. Please contact the administrator.");
                });
            });
        };
        /**
         * Liefert zu einem Mandanten-Filter-Modus den entsprechend Client-Wert.
         */
        RADApplication.getServerValueForPrincipalFilterMode = function (keyOrValue) {
            // handelt es sich dabei um den Key
            var principalFilterMode = RADApplication.principalFilterModesClientToServer[keyOrValue];
            // wenn nicht handelt es sich vielleicht schon um das Value
            if (!principalFilterMode && _.values(RADApplication.principalFilterModesClientToServer).indexOf(keyOrValue) > 0) {
                principalFilterMode = keyOrValue;
            }
            return principalFilterMode;
        };
        /*
        * Liefert zu einem Mandanten-Filter-Modus den entsprechend Server-Wert.
        */
        RADApplication.getClientValueForPrincipalFilterMode = function (keyOrValue) {
            for (var prop in RADApplication.principalFilterModesClientToServer) {
                if (RADApplication.principalFilterModesClientToServer.hasOwnProperty(prop)) {
                    if (RADApplication.principalFilterModesClientToServer[prop] === keyOrValue) {
                        return prop;
                    }
                    else if (prop === keyOrValue) {
                        return prop;
                    }
                }
            }
            return null;
        };
        RADApplication.showExpiredModulesMessage = function () {
            var ResourceManager, MessageBoxViewModel, expiredText, expiredModulesText, msgVM;
            if (!RADApplication.hasShowLicenceWarning && RADApplication.expiredModules() && RADApplication.expiredModules().length > 0) {
                ResourceManager = requirejs('Management/ResourceManager');
                MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
                expiredText = ResourceManager.get('LICENSE_EXPIRED_MODULES');
                expiredModulesText = RADApplication.expiredModules().join(', ');
                msgVM = MessageBoxViewModel.createWarningMessageBox(expiredText + ' ' + expiredModulesText, {});
                RADApplication.showMessageBox(msgVM);
                RADApplication.hasShowLicenceWarning = true;
            }
        };
        // Mapping des Mandanten Filter-Modus
        RADApplication.principalFilterModesClientToServer = {
            'PRINCIPAL_SELECTOR_ONLY_CURRENT': 'Default',
            'PRINCIPAL_SELECTOR_ONLY_SUB': 'OnlySubPrincipals',
            'PRINCIPAL_SELECTOR_CURRENT_AND_SUB': 'WithSubPrincipals',
            // Aktuell werden nur die obigen 3 Modi in der GUI Unterstützt; der Vollständigkeit halber werden noch weitere angeführt:
            'PRINCIPAL_SELECTOR_WITH_BASE': 'WithBasePrincipals',
            'PRINCIPAL_SELECTOR_ALL': 'WithAllPrincipals'
        };
        return RADApplication;
    }());
    return RADApplication;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUkFEQXBwbGljYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJSQURBcHBsaWNhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxrREFBa0Q7OztJQVNsRDs7O09BR0c7SUFDSDtRQXlPSTtZQUNJLGNBQWMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3pDLGNBQWMsQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzdDLGNBQWMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3pDLGNBQWMsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ25DLGNBQWMsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyRCxjQUFjLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNqRCxjQUFjLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RELGNBQWMsQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbkQsY0FBYyxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNuRCxjQUFjLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEQsY0FBYyxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUQsY0FBYyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUM7WUFDdEUsY0FBYyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztZQUM3QyxjQUFjLENBQUMsYUFBYSxHQUFHLENBQUM7Z0JBQzVCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDbkIsSUFBSSxFQUFFO3dCQUNGLElBQUksS0FBSyxHQUFHLGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzt3QkFDNUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUN2QyxDQUFDO29CQUNELEtBQUssRUFBRSxVQUFDLEtBQUs7d0JBQ1QsSUFBSSxhQUFhLEdBQUcsS0FBSyxDQUFDO3dCQUMxQixLQUFLLEdBQUcsS0FBSyxHQUFHLFFBQVEsQ0FBQzt3QkFFekIsd0dBQXdHO3dCQUV4RyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUMxQyxJQUFJLFNBQVMsR0FBRyxjQUFjLENBQUMsNkJBQTZCLEdBQUcsZ0JBQWdCLEdBQUcsS0FBSyxHQUFHLHdCQUF3QixHQUFHLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO3dCQUM1SSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxDQUFDLGdCQUFnQixHQUFHLGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ2hHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQzt3QkFFbkUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFFckMsTUFBTSxDQUFDLEtBQUssQ0FBQzs0QkFDVCw4R0FBOEc7NEJBQzlHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7NEJBQzNDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDOzRCQUVyQyxvSEFBb0g7NEJBQ3BILFFBQVE7NEJBQ1IsNEhBQTRIOzRCQUM1SCwwSUFBMEk7NEJBQzFJLElBQUksR0FBRyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7NEJBQ3RCLEdBQUcsQ0FBQyxPQUFPLEdBQUc7Z0NBQ1YsTUFBTSxDQUFDLFVBQVUsQ0FBQztvQ0FDZCxpRUFBaUU7b0NBQ2pFLGtDQUFrQztvQ0FDbEMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7b0NBRTNELHlFQUF5RTtvQ0FDekUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQzt3Q0FDakIsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3Q0FDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztvQ0FDaEQsQ0FBQyxDQUFDLENBQUM7b0NBRUgsbUhBQW1IO29DQUNuSCxJQUFJLFNBQVMsR0FBRyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQ0FDL0MsU0FBUyxDQUFDLG9CQUFvQixDQUFDLDZCQUE2QixFQUFFLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7b0NBRXhGLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxrQ0FBa0M7Z0NBQzlFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzs0QkFDWCxDQUFDLENBQUM7NEJBQ0YsR0FBRyxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUM7d0JBQ3hCLENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUM7aUJBQ0osQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNMLGNBQWMsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDcEUsY0FBYyxDQUFDLE9BQU8sR0FBRyxDQUFDO2dCQUN0QixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ25CLElBQUksRUFBRSxjQUFjLENBQUMsUUFBUTtvQkFDN0IsS0FBSyxFQUFFLFVBQUMsS0FBVTt3QkFDZCxLQUFLLEdBQUcsY0FBYyxDQUFDLGFBQWEsRUFBRSxHQUFHLFFBQVEsQ0FBQzt3QkFFbEQsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDM0MsSUFBSSxTQUFTLEdBQUcsY0FBYyxDQUFDLDZCQUE2QixHQUFHLGdCQUFnQixHQUFHLEtBQUssR0FBRyw2QkFBNkIsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBRXJJLElBQUksU0FBUyxLQUFLLFNBQVMsRUFBRTs0QkFDekIsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7NEJBQ3RDLGNBQWMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQ3RDO29CQUNMLENBQUM7aUJBQ0osQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNMLGNBQWMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3pDLGNBQWMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3BDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLGNBQWMsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDdkMsY0FBYyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDM0IsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDaEMsY0FBYyxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDakQsY0FBYyxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUMxRCxjQUFjLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNqRCxjQUFjLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZDLElBQUksZUFBZSxHQUFHLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFFdkQsSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDbEIsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUVELE9BQU8sZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztZQUNILGNBQWMsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hELGNBQWMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3pDLGNBQWMsQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVELGNBQWMsQ0FBQyx5QkFBeUIsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9ELGNBQWMsQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlELGNBQWMsQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hELGNBQWMsQ0FBQywyQkFBMkIsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pFLGNBQWMsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ25DLGNBQWMsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBR3JDLElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7Z0JBQ3ZCLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUV0QyxJQUFJLEtBQUssQ0FBQyxhQUFhLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxFQUFFLEVBQUU7b0JBQ2xELElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDckUsSUFBSSxZQUFZLEdBQUcsQ0FBQyxDQUFDLEVBQUU7d0JBQ25CLFVBQVUsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQztxQkFDbkQ7aUJBQ0o7Z0JBRUQsK0JBQStCO2dCQUMvQixJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7b0JBQ3RELFVBQVUsSUFBSSxHQUFHLENBQUM7aUJBQ3JCO2dCQUNELGNBQWMsQ0FBQyxTQUFTLEdBQUcsVUFBVSxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQzthQUNuRTtpQkFDSTtnQkFDRCxjQUFjLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLGlCQUFpQixDQUFDO2FBQ3hFO1lBQ0QsS0FBSyxDQUFDLFNBQVMsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDO1lBRTNDLGNBQWMsQ0FBQyx3QkFBd0IsR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLHdCQUF3QixDQUFDO1lBQ25HLGNBQWMsQ0FBQyw2QkFBNkIsR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLGdDQUFnQyxDQUFDO1lBQ2hILGNBQWMsQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLDRCQUE0QixDQUFDO1lBQ3hHLGNBQWMsQ0FBQywyQkFBMkIsR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLDhCQUE4QixDQUFDO1lBQzVHLGNBQWMsQ0FBQyxrQ0FBa0MsR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLHFDQUFxQyxDQUFDO1lBRTFILGNBQWMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNuQyxjQUFjLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7WUFDN0MsY0FBYyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDO1lBQy9DLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBRS9CLGNBQWMsQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7WUFHMUMsY0FBYyxDQUFDLGtDQUFrQyxHQUFHLEtBQUssQ0FBQztZQUMxRCxjQUFjLENBQUMsbUNBQW1DLEdBQUcsS0FBSyxDQUFDO1FBQy9ELENBQUMsQ0FBQyxxQkFBcUI7UUFFdkI7OztXQUdHO1FBQ1csOEJBQWUsR0FBN0I7WUFDSSxJQUFJLGVBQWUsR0FBRyxTQUFTLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUM5RCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuRCxDQUFDO1FBRUQ7OztXQUdHO1FBQ1csaUNBQWtCLEdBQWhDO1lBQ0ksSUFBSSxlQUFlLEdBQUcsU0FBUyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDOUQsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxnQ0FBaUIsR0FBL0I7WUFDSSxPQUFPLGNBQWMsQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7UUFDckQsQ0FBQztRQUVhLDZCQUFjLEdBQTVCLFVBQTZCLG1CQUFtQixFQUFFLGFBQXVCO1lBQ3JFLGFBQWEsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1lBRWhDLElBQUksaUJBQWlCLEdBQUcsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUM7WUFFbEUseUhBQXlIO1lBQ3pILDZIQUE2SDtZQUM3SCxJQUFJLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDLHVCQUF1QixDQUFDLFlBQVksRUFBRTtnQkFDNUUsU0FBUyxFQUFFLG1CQUFtQjthQUNqQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRVQsSUFBSSxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3hELGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2FBQzNDO1lBRUQsSUFBSSxVQUFVLEdBQUc7Z0JBQ2IsSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDaEIsY0FBYyxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQzNDO2dCQUVELGlCQUFpQixDQUFDLHlCQUF5QixDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBRS9ELE1BQU0sQ0FBQyxVQUFVLENBQUM7b0JBQ2QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRTt3QkFDakMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7cUJBQ2pDO29CQUNELG1CQUFtQixHQUFHLElBQUksQ0FBQztnQkFDL0IsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsaUVBQWlFO1lBQzVFLENBQUMsQ0FBQztZQUVGLElBQUksT0FBTyxDQUFDO1lBQ1osSUFBSSxvQkFBb0IsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO1lBQ2xELElBQUksbUJBQW1CLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQzVDLElBQUksaUJBQWUsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUN6RCxPQUFPLEdBQUc7b0JBQ04saUJBQWUsRUFBRSxDQUFDO29CQUNsQixVQUFVLEVBQUUsQ0FBQztvQkFDYixDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDcEMsQ0FBQyxDQUFDO2FBQ0w7aUJBQ0k7Z0JBQ0QsT0FBTyxHQUFHO29CQUNOLFVBQVUsRUFBRSxDQUFDO29CQUNiLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNwQyxDQUFDLENBQUM7YUFDTDtZQUNELG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ2pELENBQUM7UUFFRDs7O1dBR0c7UUFDVyw2QkFBYyxHQUE1QixVQUE2QixRQUFRO1lBQ2pDLElBQUksaUJBQWlCLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFFN0QsSUFBSSxpQkFBaUIsS0FBSyxDQUFDLEVBQUU7Z0JBQ3pCLFFBQVEsRUFBRSxDQUFDO2dCQUNYLE9BQU87YUFDVjtZQUVELElBQUksZUFBZSxHQUFHLENBQUMsQ0FBQztZQUV4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNELGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO29CQUMxQyxJQUFJLEVBQUUsZUFBZSxJQUFJLGlCQUFpQixFQUFFO3dCQUN4QyxRQUFRLEVBQUUsQ0FBQztxQkFDZDtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQztRQUVEOzs7V0FHRztRQUNXLDJCQUFZLEdBQTFCLFVBQTJCLFFBQVE7WUFDL0IsSUFBSSxpQkFBaUIsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUU3RCxJQUFJLGlCQUFpQixLQUFLLENBQUMsRUFBRTtnQkFDekIsUUFBUSxFQUFFLENBQUM7Z0JBQ1gsT0FBTzthQUNWO1lBRUQsSUFBSSxlQUFlLEdBQUcsQ0FBQyxDQUFDO1lBRXhCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0QsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7b0JBQ3hDLElBQUksRUFBRSxlQUFlLElBQUksaUJBQWlCLEVBQUU7d0JBQ3hDLFFBQVEsRUFBRSxDQUFDO3FCQUNkO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxxQ0FBc0IsR0FBcEM7WUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNELGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLEVBQUUsQ0FBQzthQUMzRDtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLG9DQUFxQixHQUFuQztZQUVJLHFEQUFxRDtZQUNyRCxJQUFJLFlBQVksR0FBRyxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRCxJQUFJLFlBQVksRUFBRTtnQkFDZCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO29CQUM1QixjQUFjLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7aUJBQ2xEO2FBQ0o7WUFFRCxjQUFjLENBQUMseUJBQXlCLEVBQUUsQ0FBQztZQUUzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNELGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzthQUMxRDtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLG1DQUFvQixHQUFsQyxVQUFtQyxRQUFRO1lBQ3ZDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0QsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNqRTtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLHlDQUEwQixHQUF4QyxVQUF5QyxRQUFRO1lBQzdDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0QsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN2RTtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLHlCQUFVLEdBQXhCLFVBQXlCLFFBQVEsRUFBRSxZQUFZO1lBQzNDLHlEQUF5RDtZQUN6RCw4SEFBOEg7WUFDOUgsSUFBSSxlQUFlLEdBQUcsU0FBUyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDOUQsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRTlCLGNBQWMsQ0FBQyxZQUFZLENBQUM7Z0JBQ3hCLElBQUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLDZCQUE2QixDQUFDLENBQUM7Z0JBQ2hFLElBQUksaUJBQWlCLEdBQUcsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUM7Z0JBQ2xFLElBQUksZUFBZSxHQUFHLFNBQVMsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO2dCQUU5RCxjQUFjLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN4RCxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0MsY0FBYyxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUVoRSxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDakMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUV2QixpQkFBaUIsQ0FBQyw0QkFBNEIsRUFBRSxDQUFDO2dCQUVqRCxJQUFJLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLDJCQUEyQixLQUFLLElBQUksRUFBRTtvQkFDL0QsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7aUJBQzlFO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQ7O1dBRUc7UUFDVywwQkFBVyxHQUF6QjtZQUNJLElBQUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDaEUsSUFBSSxpQkFBaUIsR0FBRyxTQUFTLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUVsRSxJQUFJLGNBQWMsQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVM7Z0JBQzNFLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTFDLElBQUksZ0JBQWdCLElBQUksSUFBSSxJQUFJLGdCQUFnQixDQUFDLFlBQVksS0FBSyxTQUFTO2dCQUN2RSxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUVwQyxJQUFJLGlCQUFpQixJQUFJLElBQUksSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLEtBQUssU0FBUyxFQUFFO2dCQUNyRSxrQ0FBa0M7Z0JBQ2xDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzlCO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csc0NBQXVCLEdBQXJDLFVBQXNDLElBQUk7WUFDdEMsSUFBSSxJQUFJLElBQUksY0FBYyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDdkQsSUFBSSxLQUFLLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDaEUsT0FBTyxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssS0FBSyxNQUFNLElBQUksS0FBSyxLQUFLLE1BQU0sQ0FBQzthQUNqRTtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRDs7V0FFRztRQUNXLDZCQUFjLEdBQTVCLFVBQTZCLElBQUksRUFBRSxXQUFXO1lBQzFDLElBQUksSUFBSSxJQUFJLGNBQWMsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3ZELElBQUksS0FBSyxHQUFHLGNBQWMsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRWhFLElBQUksV0FBVyxFQUFFO29CQUNiLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzlCO2dCQUVELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ1csNkJBQWMsR0FBNUI7WUFDSSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFDLEtBQUs7Z0JBQzVCLElBQUksYUFBYSxHQUFHLGNBQWMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFFbkQsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO29CQUNsQyxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQyxjQUFjLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUMvQztnQkFFRCxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUU7b0JBQ2pDLGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDbkM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILHNFQUFzRTtZQUN0RSwrQ0FBK0M7WUFDL0MsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFVBQUMsQ0FBQztnQkFDeEIsT0FBTyxLQUFLLENBQUM7WUFDakIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyw2QkFBYyxHQUE1QjtZQUNJLElBQUksaUJBQWlCLEdBQUcsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUM7WUFFbEUsSUFBSSxjQUFjLENBQUMsUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDakMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO2dCQUN6QyxpQkFBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ2xFO2lCQUNJO2dCQUNELGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUMvQixpQkFBaUIsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDMUM7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDVywwQkFBVyxHQUF6QjtZQUNJLElBQUksbUJBQW1CLEdBQUcsU0FBUyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDekUsSUFBSSxTQUFTLEdBQUcsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFFL0MsT0FBTyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDekMsS0FBSyxFQUFFLE1BQU07YUFDaEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFlBQVk7Z0JBQ2pCLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDZCxJQUFJLFlBQVksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO29CQUNqQyxjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQzlEO2dCQUVELElBQUksWUFBWSxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtvQkFDdkMsSUFBSSxZQUFZLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLElBQUksSUFBSSxFQUFFO3dCQUMxRCxjQUFjLENBQUMsMEJBQTBCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUM7cUJBQy9GO29CQUVELElBQUksWUFBWSxDQUFDLGdCQUFnQixDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7d0JBQ3BELGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7cUJBQ25GO29CQUVELGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2lCQUM1RDtnQkFFRCxJQUFJLFlBQVksQ0FBQyxhQUFhLElBQUksSUFBSSxFQUFFO29CQUNwQyxjQUFjLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUM7aUJBQzdEO2dCQUVELElBQUksWUFBWSxDQUFDLGFBQWEsSUFBSSxJQUFJLEVBQUU7b0JBQ3BDLGNBQWMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUN6RTtnQkFFRCxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDeEMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLDBCQUEwQixDQUFDLENBQUM7Z0JBQzNELGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUV2QyxTQUFTLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNkLE9BQU87WUFDWCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxxQ0FBc0IsR0FBcEM7WUFDSSxJQUFJLGVBQWUsR0FBRyxTQUFTLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUU5RCxPQUFPLGVBQWUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQ2xELENBQUM7UUFFYSwwQ0FBMkIsR0FBekM7WUFDSSxJQUFJLGNBQWMsQ0FBQyxrQ0FBa0MsS0FBSyxJQUFJLEVBQUU7Z0JBQzVELE9BQU87YUFDVjtZQUNELGNBQWMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkMsY0FBYyxDQUFDLGtDQUFrQyxHQUFHLElBQUksQ0FBQztZQUN6RCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7WUFFZixJQUFJLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBQ2hFLElBQUksbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsSUFBSSxlQUFlLEdBQUcsU0FBUyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFFOUQsMkVBQTJFO1lBQzNFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUVuQixLQUFLLEdBQUcsbUJBQW1CLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBRTtnQkFDdEYsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2pDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUVQLGNBQWMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUVhLDJDQUE0QixHQUExQztZQUNJLElBQUksY0FBYyxDQUFDLG1DQUFtQyxLQUFLLElBQUksRUFBRTtnQkFDN0QsT0FBTzthQUNWO1lBQ0QsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQyxjQUFjLENBQUMsbUNBQW1DLEdBQUcsSUFBSSxDQUFDO1lBQzFELElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUVmLElBQUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDaEUsSUFBSSxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvRSxJQUFJLGVBQWUsR0FBRyxTQUFTLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUU5RCwyRUFBMkU7WUFDM0UsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBRW5CLElBQUksR0FBRyxHQUFHLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNyRCxJQUFJLEdBQUcsSUFBSSxxQkFBcUI7Z0JBQzVCLEdBQUcsR0FBRyxpRUFBaUUsQ0FBQztZQUU1RSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsdUJBQXVCLENBQUMsR0FBRyxFQUFFO2dCQUNyRCxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDakMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRVAsY0FBYyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxpQ0FBa0IsR0FBaEM7WUFDSSxjQUFjLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUVuQyxJQUFJLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBQ2hFLElBQUksY0FBYyxHQUFHLGdCQUFnQixDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFFMUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzVDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMvRDtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNXLG1CQUFJLEdBQWxCO1lBQ0ksU0FBUyxDQUFDLENBQUMsbUNBQW1DLENBQUMsRUFBRSxVQUFDLG1CQUFtQjtnQkFDakUsY0FBYyxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBRXBDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLGNBQWM7b0JBQ3JELGNBQWMsQ0FBQyxhQUFhLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQztvQkFFNUQsY0FBYyxDQUFDLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO29CQUNqRixjQUFjLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDL0QsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUNoQyxjQUFjLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDdkQsY0FBYyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQy9DLGNBQWMsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUM3RCxjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDL0MsY0FBYyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQzdELGNBQWMsQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUMvRCxjQUFjLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDL0QsY0FBYyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQy9DLGNBQWMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM1QyxjQUFjLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQ25FLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBQztvQkFDbkUsY0FBYyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBRS9DLFNBQVMsQ0FBQyxDQUFDLFlBQVksRUFBRSxpQkFBaUIsRUFBRSw4QkFBOEIsRUFBRSw0QkFBNEIsRUFBRSx1QkFBdUIsQ0FBQyxFQUM5SCxVQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsZUFBZSxFQUFFLGFBQWE7d0JBRS9ELGNBQWMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQzt3QkFDakMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO3dCQUUzQyxJQUFJLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO3dCQUVsRSxJQUFJLGtCQUFrQixLQUFLLElBQUksRUFBRTs0QkFDN0IsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQ2xDOzZCQUFNOzRCQUVILGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0NBQ3hCLGNBQWMsQ0FBQyxjQUFjLENBQUM7b0NBQzFCLGNBQWMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO29DQUVwQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDN0IsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQyxDQUFDLENBQUM7eUJBQ047b0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSztvQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLHdGQUF3RixDQUFDLENBQUM7Z0JBQzFHLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxtREFBb0MsR0FBbEQsVUFBbUQsVUFBVTtZQUN6RCxtQ0FBbUM7WUFDbkMsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsa0NBQWtDLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEYsMkRBQTJEO1lBQzNELElBQUksQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdHLG1CQUFtQixHQUFHLFVBQVUsQ0FBQzthQUNwQztZQUVELE9BQU8sbUJBQW1CLENBQUM7UUFDL0IsQ0FBQztRQUVEOztVQUVFO1FBQ1ksbURBQW9DLEdBQWxELFVBQW1ELFVBQVU7WUFDekQsS0FBSyxJQUFJLElBQUksSUFBSSxjQUFjLENBQUMsa0NBQWtDLEVBQUU7Z0JBQ2hFLElBQUksY0FBYyxDQUFDLGtDQUFrQyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDeEUsSUFBSSxjQUFjLENBQUMsa0NBQWtDLENBQUMsSUFBSSxDQUFDLEtBQUssVUFBVSxFQUFFO3dCQUN4RSxPQUFPLElBQUksQ0FBQztxQkFDZjt5QkFDSSxJQUFJLElBQUksS0FBSyxVQUFVLEVBQUU7d0JBQzFCLE9BQU8sSUFBSSxDQUFDO3FCQUNmO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWMsd0NBQXlCLEdBQXhDO1lBQ0ksSUFBSSxlQUFlLEVBQUUsbUJBQW1CLEVBQUUsV0FBVyxFQUFFLGtCQUFrQixFQUFFLEtBQUssQ0FBQztZQUVqRixJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixJQUFJLGNBQWMsQ0FBQyxjQUFjLEVBQUUsSUFBSSxjQUFjLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEgsZUFBZSxHQUFHLFNBQVMsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO2dCQUMxRCxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFFM0UsV0FBVyxHQUFHLGVBQWUsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztnQkFDN0Qsa0JBQWtCLEdBQUcsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFaEUsS0FBSyxHQUFHLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLFdBQVcsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2hHLGNBQWMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRXJDLGNBQWMsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDL0M7UUFDTCxDQUFDO1FBbDNCRCxxQ0FBcUM7UUFDdEIsaURBQWtDLEdBQUc7WUFDaEQsaUNBQWlDLEVBQUUsU0FBUztZQUM1Qyw2QkFBNkIsRUFBRSxtQkFBbUI7WUFDbEQsb0NBQW9DLEVBQUUsbUJBQW1CO1lBRXpELHlIQUF5SDtZQUN6SCw4QkFBOEIsRUFBRSxvQkFBb0I7WUFDcEQsd0JBQXdCLEVBQUUsbUJBQW1CO1NBQ2hELENBQUM7UUEwMkJOLHFCQUFDO0tBQUEsQUFyM0JELElBcTNCQztJQUVELE9BQVMsY0FBYyxDQUFDIn0=