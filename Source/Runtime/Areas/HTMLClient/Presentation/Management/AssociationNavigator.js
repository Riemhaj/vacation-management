///<amd-module name="Management/AssociationNavigator" />
define("Management/AssociationNavigator", ["require", "exports", "PrototypeToInstance"], function (require, exports, PrototypeToInstance_1) {
    "use strict";
    /**
     * Diese abstrakte Klasse ist Basisklasse für alle konkreten AssociationNavigator-Klassen.
     * Ein AssociationNavigator wird vom System aufgerufen, wenn über eine Assoziation navigiert werden soll.
     * Das anzunavigierende Objekt wird hier als assoziiertes Objekt bezeichnet, das Objekt von dem die Navigation ausgeht, wird assoziierendes Objekt genannt.
     *
     * Verwendet wird die Methode navigateAssociation mit folgenden Parametern:
     *
     *  - typeName: Typ des assoziierten Objekts
     *  - pdoId: ID des assoziierten Objekts
     *  - optionalParams: Weitere Parameter, im Framework bekannt sind mindestens:
     *      - contextSource: null, 'ALV' oder 'SV': Die Art des Kontextes aus dem heraus die Navigation stattfindet
     *      - contextObject: ID des assoziierenden Objekts
     *      - contextTypeName: Typ des assoziierenden Objekts
     *      - contextRelation: Name der Assoziation am assoziierenden Objekt über die navigiert wird
     *
     *      - onNavigationNotPossible: Falls eine Navigation nicht möglich ist, wird dieses Callback aufgerufen
     *      - onNavigated            : Kurz vor der eigentlichen Navigation wird dieses Callback aufgerufen
     *
     * Konkrete Unterklassen können die Parameter (inkl. optionalParams) nutzen, um die Navigation genauer zu steuern.
     */
    var AssociationNavigator = /** @class */ (function () {
        function AssociationNavigator() {
            PrototypeToInstance_1.applyPrototypeToInstance.call(this, AssociationNavigator);
        }
        AssociationNavigator.prototype.prepareParamsForDetailView = function (pdoId, params) {
            params = $.extend({}, params, { pdoId: pdoId });
            delete params.contextSource;
            return params;
        };
        AssociationNavigator.prototype.prepareParamsForListView = function (pdoId, params) {
            params = $.extend({}, params, { selectedPDO: pdoId });
            delete params.contextSource;
            return params;
        };
        AssociationNavigator.prototype.getListViewNameForType = function (typeName) {
            return typeName + '_LV';
        };
        AssociationNavigator.prototype.navigateAssociation = function (typeName, pdoId, optionalParams, openInNewWindow) {
            throw 'Not yet implemented: navigateToAssociation';
        };
        return AssociationNavigator;
    }());
    return AssociationNavigator;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXNzb2NpYXRpb25OYXZpZ2F0b3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJBc3NvY2lhdGlvbk5hdmlnYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx3REFBd0Q7OztJQUt4RDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztPQW1CRztJQUNIO1FBRUk7WUFDSSw4Q0FBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDN0QsQ0FBQztRQUVNLHlEQUEwQixHQUFqQyxVQUFrQyxLQUFLLEVBQUUsTUFBTTtZQUMzQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDaEQsT0FBTyxNQUFNLENBQUMsYUFBYSxDQUFDO1lBQzVCLE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFTSx1REFBd0IsR0FBL0IsVUFBZ0MsS0FBSyxFQUFFLE1BQU07WUFDekMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ3RELE9BQU8sTUFBTSxDQUFDLGFBQWEsQ0FBQztZQUM1QixPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBRU0scURBQXNCLEdBQTdCLFVBQThCLFFBQVE7WUFDbEMsT0FBTyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQzVCLENBQUM7UUFFTSxrREFBbUIsR0FBMUIsVUFBMkIsUUFBUSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsZUFBd0I7WUFDaEYsTUFBTSw0Q0FBNEMsQ0FBQztRQUN2RCxDQUFDO1FBQ0wsMkJBQUM7SUFBRCxDQUFDLEFBekJELElBeUJDO0lBRUQsT0FBUyxvQkFBb0IsQ0FBQyJ9