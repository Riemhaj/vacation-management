///<amd-module name="Management/FrontController" />
define("Management/FrontController", ["require", "exports", "Utility/Messenger", "Constants", "Management/ComponentManager", "Management/NavigationService", "Management/NavigationHelper", "Management/RADApplication", "Model/Address", "Communication/RADEnvironmentProxy", "Utility/Loader"], function (require, exports, Messenger, Constants, ComponentManager, NavigationService, NavigationHelper, RADApplication, Address, RADEnvironmentProxy, Loader) {
    "use strict";
    var FrontController = /** @class */ (function () {
        function FrontController() {
        }
        /**
         * Entfernt einen Call aus der Liste
         */
        FrontController.removeCall = function (id) {
            if (id in FrontController.calls) {
                delete FrontController.calls[id];
            }
        };
        /**
         * Liefert einen Call anhand der Id oder Null
         */
        FrontController.getCall = function (id) {
            if (id in FrontController.calls) {
                return FrontController.calls[id];
            }
            return null;
        };
        /**
         * Erzeugt einen neuen Operation-Call und ruft diesen mit den übergebenen Parametern auf
         */
        FrontController.managedCallStaticOperation = function (pdTypeName, operationName, operationParameters, contextParameters, onSuccess, onError) {
            var managedStaticOperationCall = new ManagedStaticOperationCall(pdTypeName, operationName, contextParameters, onSuccess, onError);
            FrontController.calls[managedStaticOperationCall.id] = managedStaticOperationCall;
            managedStaticOperationCall.call(operationParameters);
        };
        /**
         * Initialisiert den FrontController
         * z.B. Messages werden registriert
         */
        FrontController.init = function () {
            Messenger.registerForMessageId('DetailViewExitedMessage', function (message) {
                if (message.context != null && message.context._callId in FrontController.calls) {
                    FrontController.calls[message.context._callId].onDetailViewClosed(message);
                }
            }, FrontController);
        };
        FrontController.evaluateDetailViewContextAfterCallOperation = function (context, result) {
            var parameter = context.getContextParameters();
            if (parameter.hasOwnProperty(Constants.CALL_ENVELOPE_DV_FORCE_CLOSING_ACTION)) {
                var value = parameter[Constants.CALL_ENVELOPE_DV_FORCE_CLOSING_ACTION];
                //parameter.remove(Constants.CALL_ENVELOPE_DV_FORCE_CLOSING_ACTION);
                if (value === 'SAVE') {
                    context.save();
                }
                else if (value === 'CANCEL') {
                    context.cancel();
                }
            }
            else if (parameter.hasOwnProperty(Constants.CALL_ENVELOPE_DV_FORCE_APPLY_ACTION)) {
                var value = parameter[Constants.CALL_ENVELOPE_DV_FORCE_APPLY_ACTION];
                if (value === 'APPLY') {
                    context.apply();
                }
            }
        };
        /**
         * Enthält eine Liste von Operations-Aufrufen
         */
        FrontController.calls = {};
        return FrontController;
    }()); // end of class FrontController
    var ManagedStaticOperationCall = /** @class */ (function () {
        function ManagedStaticOperationCall(pdTypeName, operationName, contextParameters, onSuccess, onError) {
            this.MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
            this.id = _.uniqueId(operationName + '_');
            this.pdTypeName = pdTypeName;
            this.operationName = operationName;
            this.contextNeedsRefresh = true;
            this.contextParameters = contextParameters || {};
            this.onSuccess = onSuccess || this.onDefaultSuccess.bind(this);
            this.onError = onError || this.onDefaultError.bind(this);
            this.isClosed = false;
        }
        ManagedStaticOperationCall.prototype.evalContextNeedsRefresh = function () {
            if (!!this.contextParameters && _.has(this.contextParameters, Constants.CALL_ENVELOPE_CONTEXT_NEEDS_REFRESH)) {
                var sbool = this.contextParameters[Constants.CALL_ENVELOPE_CONTEXT_NEEDS_REFRESH];
                this.contextNeedsRefresh = sbool.toLowerCase() == true.toString().toLowerCase();
            }
        };
        /*
         * Der Default-Callback nach einem erfolgreichen Aufruf der Operation
         */
        ManagedStaticOperationCall.prototype.onDefaultSuccess = function (managedStaticOperationCall, operationResultDTO) {
            var action = this.contextParameters[Constants.CALL_ENVELOPE_CLIENT_ACTION_STRING];
            var pdoId = this.contextParameters[Constants.CALL_ENVELOPE_SELECT_OBJECT_IN_LV_ACTION_STRING];
            var url = this.contextParameters[Constants.CALL_ENVELOPE_OPEN_WEBPAGE];
            var openInNewWindow = this.contextParameters['openInNewWindow'];
            if (action != null) {
                // wozu war diese Ersetzungen?
                // verursacht Probleme, wenn z.B. in der action ein Filter-String der Form this.PDO_ID IN (...) vorkommt
                // action = action.replace(/\s/g, '');
                // Die vom Server erzeugten URLs enthalten die Schreibung PdoID statt PdoID; in diesem Client wird (in den Properties) aber PdoId benutzt.
                action = action.replace('PdoID=', 'PdoId=');
                var address = new Address(action);
                address.setParameter('ContextID', this.id);
                if (openInNewWindow)
                    NavigationService.navigateToAddress(address, null, null, null, openInNewWindow);
                else
                    NavigationService.navigateToAddress(address);
            }
            else if (pdoId != null) {
                var lv = RADApplication.currentListViewContext();
                if (lv != null && lv.dataGrid != null) {
                    lv.dataGrid.selected([pdoId]);
                    lv.refreshPersistenceContext();
                }
            }
            else if (url != null) {
                NavigationHelper.openWebPage(url, '_blank');
                this.close();
            }
            else {
                this.close();
            }
        };
        /**
         * Der Default-Callback nach einem fehlerhaften Aufruf der Operation
         */
        ManagedStaticOperationCall.prototype.onDefaultError = function (managedStaticOperationCall, operationResultDTO) {
            RADApplication.showMessageBox(this.MessageBoxViewModel.createErrorMessageBox(operationResultDTO.errorMessage));
            this.close();
        };
        /**
         * Wird aufgerufen sobald die DetailView verlassen wird
         */
        ManagedStaticOperationCall.prototype.onDetailViewClosed = function (message) {
            var _this = this;
            var action = this.contextParameters[Constants.CALL_ENVELOPE_DV_ACTION_CLOSE];
            if (action != null) {
                delete this.contextParameters[Constants.CALL_ENVELOPE_DV_ACTION_CLOSE];
                var address = new Address(action);
                var name_1 = address.getName();
                if (name_1 === 'CallStaticOperation') {
                    this.contextParameters[Constants.CALL_ENVELOPE_DV_HAS_OBJECT_CHANGED] = message.hasObjectChanged;
                    this.contextParameters[Constants.CALL_ENVELOPE_DV_LEAVING_ACTION] = message.leavingAction;
                    this.contextParameters[Constants.CALL_ENVELOPE_DV_HAS_PENDING_DVC] = message.hasPendingDVC;
                    var pdTypeName = address.getParameter('TypeName');
                    var operationName = address.getParameter('OperationName');
                    if (pdTypeName != null && operationName != null) {
                        Loader.show();
                        RADEnvironmentProxy.callStaticOperation({
                            pdTypeName: pdTypeName,
                            operationName: operationName,
                            operationParameters: null,
                            contextParameters: this.contextParameters
                        }).then(function (operationResultDTO) {
                            _this.contextParameters = operationResultDTO.contextParameters;
                            if (operationResultDTO.status == 'Successfull') {
                                if (operationResultDTO.contextParameters[Constants.CALL_ENVELOPE_SHOW_RESULT_IN_GUI] === true) {
                                    RADApplication.showMessageBox(_this.MessageBoxViewModel.createNoticeMessageBox(operationResultDTO.returnValue + ''));
                                }
                                _this.onSuccess(_this, operationResultDTO);
                            }
                            else {
                                _this.onError(_this, operationResultDTO);
                            }
                        }).always(function () {
                            Loader.hide();
                        });
                    }
                }
            }
        };
        /*
         * Ruft die Operation über den RADEnvironmentProxy auf
         */
        ManagedStaticOperationCall.prototype.call = function (operationParameters) {
            var _this = this;
            Loader.show();
            RADEnvironmentProxy.callStaticOperation({
                pdTypeName: this.pdTypeName,
                operationName: this.operationName,
                operationParameters: operationParameters,
                contextParameters: this.contextParameters
            }).then(function (operationResultDTO) {
                _this.contextParameters = $.extend({}, _this.contextParameters, operationResultDTO.contextParameters);
                if (operationResultDTO.status == 'Successfull') {
                    if (operationResultDTO.contextParameters[Constants.CALL_ENVELOPE_SHOW_RESULT_IN_GUI] === true) {
                        RADApplication.showMessageBox(_this.MessageBoxViewModel.createNoticeMessageBox(operationResultDTO.returnValue + ''));
                    }
                    _this.onSuccess(_this, operationResultDTO);
                }
                else {
                    _this.onError(_this, operationResultDTO);
                }
            }).always(function () {
                Loader.hide();
            });
        };
        ManagedStaticOperationCall.prototype.close = function () {
            this.evalContextNeedsRefresh();
            FrontController.removeCall(this.id);
            this.isClosed = true;
        };
        return ManagedStaticOperationCall;
    }());
    return FrontController;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRnJvbnRDb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRnJvbnRDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG1EQUFtRDs7O0lBWW5EO1FBQUE7UUFxRUEsQ0FBQztRQTlERzs7V0FFRztRQUNXLDBCQUFVLEdBQXhCLFVBQXlCLEVBQVU7WUFDL0IsSUFBSSxFQUFFLElBQUksZUFBZSxDQUFDLEtBQUssRUFBRTtnQkFDN0IsT0FBTyxlQUFlLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csdUJBQU8sR0FBckIsVUFBc0IsRUFBVTtZQUM1QixJQUFJLEVBQUUsSUFBSSxlQUFlLENBQUMsS0FBSyxFQUFFO2dCQUM3QixPQUFPLGVBQWUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDcEM7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7O1dBRUc7UUFDVywwQ0FBMEIsR0FBeEMsVUFBeUMsVUFBa0IsRUFBRSxhQUFxQixFQUFFLG1CQUF3QixFQUFFLGlCQUFzQixFQUFFLFNBQVUsRUFBRSxPQUFRO1lBQ3RKLElBQUksMEJBQTBCLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxVQUFVLEVBQUUsYUFBYSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsSSxlQUFlLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLEVBQUUsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO1lBQ2xGLDBCQUEwQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3pELENBQUM7UUFFRDs7O1dBR0c7UUFDVyxvQkFBSSxHQUFsQjtZQUNJLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsRUFBRSxVQUFDLE9BQU87Z0JBQzlELElBQUksT0FBTyxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksZUFBZSxDQUFDLEtBQUssRUFBRTtvQkFDN0UsZUFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUM5RTtZQUNMLENBQUMsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBRWEsMkRBQTJDLEdBQXpELFVBQTBELE9BQU8sRUFBRSxNQUFNO1lBQ3JFLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQy9DLElBQUksU0FBUyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMscUNBQXFDLENBQUMsRUFBRTtnQkFDM0UsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO2dCQUN2RSxvRUFBb0U7Z0JBRXBFLElBQUksS0FBSyxLQUFLLE1BQU0sRUFBRTtvQkFDbEIsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNsQjtxQkFDSSxJQUFJLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQ3pCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQkFDcEI7YUFDSjtpQkFDSSxJQUFJLFNBQVMsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLG1DQUFtQyxDQUFDLEVBQUU7Z0JBQzlFLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsbUNBQW1DLENBQUMsQ0FBQztnQkFDckUsSUFBSSxLQUFLLEtBQUssT0FBTyxFQUFFO29CQUNuQixPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQ25CO2FBQ0o7UUFDTCxDQUFDO1FBbEVEOztXQUVHO1FBQ1kscUJBQUssR0FBRyxFQUFFLENBQUM7UUFnRTlCLHNCQUFDO0tBQUEsQUFyRUQsSUFxRUMsQ0FBQywrQkFBK0I7SUFHakM7UUF5Q0ksb0NBQVksVUFBVSxFQUFFLGFBQWEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsT0FBTztZQUN4RSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztZQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztZQUNuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsSUFBSSxFQUFFLENBQUM7WUFDakQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV6RCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUMxQixDQUFDO1FBRU0sNERBQXVCLEdBQTlCO1lBQ0ksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxtQ0FBbUMsQ0FBQyxFQUFFO2dCQUMxRyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ25GO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0kscURBQWdCLEdBQXZCLFVBQXdCLDBCQUEwQixFQUFFLGtCQUFrQjtZQUNsRSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7WUFDbEYsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1lBQzlGLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUN2RSxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUVoRSxJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQ2hCLDhCQUE4QjtnQkFDOUIsd0dBQXdHO2dCQUN4RyxzQ0FBc0M7Z0JBRXRDLDBJQUEwSTtnQkFDMUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUU1QyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEMsT0FBTyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUUzQyxJQUFJLGVBQWU7b0JBQ2YsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDOztvQkFFaEYsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDcEQ7aUJBQU0sSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUN0QixJQUFJLEVBQUUsR0FBRyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDakQsSUFBSSxFQUFFLElBQUksSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNuQyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQzlCLEVBQUUsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO2lCQUNsQzthQUNKO2lCQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDcEIsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2hCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNoQjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLG1EQUFjLEdBQXJCLFVBQXNCLDBCQUEwQixFQUFFLGtCQUFrQjtZQUNoRSxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9HLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixDQUFDO1FBRUQ7O1dBRUc7UUFDSSx1REFBa0IsR0FBekIsVUFBMEIsT0FBTztZQUFqQyxpQkEyQ0M7WUExQ0csSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBRTdFLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLDZCQUE2QixDQUFDLENBQUM7Z0JBRXZFLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLE1BQUksR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBRTdCLElBQUksTUFBSSxLQUFLLHFCQUFxQixFQUFFO29CQUNoQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLG1DQUFtQyxDQUFDLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDO29CQUNqRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLCtCQUErQixDQUFDLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQztvQkFDMUYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxnQ0FBZ0MsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUM7b0JBRTNGLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFXLENBQUM7b0JBQzVELElBQUksYUFBYSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFXLENBQUM7b0JBRXBFLElBQUksVUFBVSxJQUFJLElBQUksSUFBSSxhQUFhLElBQUksSUFBSSxFQUFFO3dCQUM3QyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBRWQsbUJBQW1CLENBQUMsbUJBQW1CLENBQUM7NEJBQ3BDLFVBQVUsRUFBRSxVQUFVOzRCQUN0QixhQUFhLEVBQUUsYUFBYTs0QkFDNUIsbUJBQW1CLEVBQUUsSUFBSTs0QkFDekIsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjt5QkFDNUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLGtCQUFrQjs0QkFDdkIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDOzRCQUU5RCxJQUFJLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxhQUFhLEVBQUU7Z0NBQzVDLElBQUksa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLGdDQUFnQyxDQUFDLEtBQUssSUFBSSxFQUFFO29DQUMzRixjQUFjLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztpQ0FDdkg7Z0NBRUQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzs2QkFDNUM7aUNBQU07Z0NBQ0gsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzs2QkFDMUM7d0JBQ0wsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDOzRCQUNOLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDbEIsQ0FBQyxDQUFDLENBQUM7cUJBQ047aUJBQ0o7YUFDSjtRQUNMLENBQUM7UUFHRDs7V0FFRztRQUNJLHlDQUFJLEdBQVgsVUFBWSxtQkFBbUI7WUFBL0IsaUJBd0JDO1lBdkJHLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVkLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDO2dCQUNwQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7Z0JBQzNCLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYTtnQkFDakMsbUJBQW1CLEVBQUUsbUJBQW1CO2dCQUN4QyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2FBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxrQkFBa0I7Z0JBQ3ZCLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFJLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFFcEcsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLElBQUksYUFBYSxFQUFFO29CQUM1QyxJQUFJLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxnQ0FBZ0MsQ0FBQyxLQUFLLElBQUksRUFBRTt3QkFDM0YsY0FBYyxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsc0JBQXNCLENBQUMsa0JBQWtCLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBQ3ZIO29CQUVELEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxFQUFFLGtCQUFrQixDQUFDLENBQUM7aUJBQzVDO3FCQUNJO29CQUNELEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSSxFQUFFLGtCQUFrQixDQUFDLENBQUM7aUJBQzFDO1lBQ0wsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUNOLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNsQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFTSwwQ0FBSyxHQUFaO1lBQ0ksSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7WUFFL0IsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDekIsQ0FBQztRQUNMLGlDQUFDO0lBQUQsQ0FBQyxBQS9MRCxJQStMQztJQUVELE9BQVMsZUFBZSxDQUFDIn0=