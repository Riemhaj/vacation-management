///<amd-module name="Management/NavigationHelper" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Management/NavigationHelper", ["require", "exports", "Management/AssociationNavigator", "Management/RADApplication", "Model/Address", "Utility/DateHelper", "Management/NavigationService"], function (require, exports, AssociationNavigator, RADApplication, Address, DateHelper, NavigationService) {
    "use strict";
    var DefaultAssociationNavigator = /** @class */ (function (_super) {
        __extends(DefaultAssociationNavigator, _super);
        function DefaultAssociationNavigator() {
            return _super.call(this) || this;
        }
        DefaultAssociationNavigator.prototype.navigateAssociation = function (typeName, selectedPDO, optionalParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            if (selectedPDO === null || selectedPDO === undefined) {
                return;
            }
            if (optionalParams != null && optionalParams.contextSource === 'SV') {
                if (RADApplication.isSystemParamSetAndTrue('OpenDetailViewFromSummary')) {
                    var params = this.prepareParamsForDetailView(selectedPDO, optionalParams);
                    NavigationHelper.openDetailView('EDIT', typeName, params, openInNewWindow);
                }
                else {
                    var params = this.prepareParamsForListView(selectedPDO, optionalParams);
                    NavigationHelper.openListViewByType(typeName, params, openInNewWindow);
                }
            }
            else {
                var params = this.prepareParamsForListView(selectedPDO, optionalParams);
                NavigationHelper.openListViewByType(typeName, params, openInNewWindow);
            }
        };
        return DefaultAssociationNavigator;
    }(AssociationNavigator)); // end of class DefaultAssociationNavigator
    /**
     * Bietet Methoden, die die Bedienung des NavigationService erleichtern.
     */
    var NavigationHelper = /** @class */ (function () {
        function NavigationHelper() {
        }
        NavigationHelper.setAssociationNavigator = function (_associationNavigator) {
            NavigationHelper.associationNavigator = _associationNavigator;
        };
        /**
         * Schnittstelle, um assoziative Listen anzunavigieren.
         */
        NavigationHelper.navigateAssociation = function () {
            var args = Array.prototype.slice.call(arguments);
            NavigationHelper.associationNavigator.navigateAssociation.apply(NavigationHelper.associationNavigator, args);
        };
        NavigationHelper.addListViewParameters = function (addr, params) {
            if (params == null) {
                return;
            }
            addr.onNavigated(params != null ? params.onNavigated : null);
            if (!String.isNullOrEmpty(params.preFilter)) {
                addr.setParameter('PreFilter', params.preFilter);
            }
            if (!String.isNullOrEmpty(params.filter)) {
                addr.setParameter('Filter', params.filter);
            }
            if (!String.isNullOrEmpty(params.serializedFilter)) {
                addr.setParameter('SerializedFilter', params.serializedFilter);
            }
            if (params.forceSelectingLVByName === true || params.forceSelectingLVByName === false) {
                addr.setHiddenParameter('forceSelectingLVByName', params.forceSelectingLVByName);
            }
            if (!isNaN(params.selectedPDO) && params.selectedPDO > 0) {
                addr.setParameter('SelectedPDO', params.selectedPDO);
            }
            if (!String.isNullOrEmpty(params.contextTypeName)
                && params.contextObject != undefined && params.contextObject > 0
                && !String.isNullOrEmpty(params.contextRelation)) {
                addr.setHiddenParameter('ContextTypeName', params.contextTypeName);
                addr.setHiddenParameter('ContextObject', params.contextObject);
                addr.setHiddenParameter('Assoziation', params.contextRelation);
            }
            if (!String.isNullOrEmpty(params.titleExtension)) {
                addr.setParameter('TitleExtension', params.titleExtension);
            }
            if (!String.isNullOrEmpty(params.principalFilterMode)) {
                var principalFilterMode = RADApplication.getServerValueForPrincipalFilterMode(params.principalFilterMode);
                if (principalFilterMode != undefined && principalFilterMode != 'Default') {
                    addr.setHiddenParameter('PrincipalMode', principalFilterMode);
                }
            }
            if (params.showPrincipalFilter === false) {
                addr.setHiddenParameter('ShowPrincipalButton', false);
            }
            if (params.historyDate != undefined && params.historyDate != '') {
                var historyDateTicks = -1;
                if (params.historyDate instanceof Date) {
                    historyDateTicks = DateHelper.dateToTicks(params.historyDate);
                }
                else if (typeof params.historyDate == 'number') {
                    historyDateTicks = params.historyDate;
                }
                // Nur für .Net-Date-Strings!
                else if (typeof params.historyDate == "string") {
                    historyDateTicks = DateHelper.dateToTicks(DateHelper.parseDotNetDateString(params.historyDate));
                }
                if (historyDateTicks > 0) {
                    addr.setParameter('HistoryDate', historyDateTicks);
                }
            }
        };
        NavigationHelper.openListViewById = function (viewPDO_ID, optionalParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            if (viewPDO_ID <= 0)
                return;
            var addr = new Address();
            addr.setName('ListView');
            addr.setParameter('ViewId', viewPDO_ID);
            NavigationHelper.addListViewParameters(addr, optionalParams);
            NavigationService.navigateToAddress(addr, null, null, null, openInNewWindow);
        };
        ;
        NavigationHelper.openListViewByType = function (typeName, optionalParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            if (String.isNullOrEmpty(typeName)) {
                return;
            }
            NavigationHelper.openListViewByName(typeName + '_LV', optionalParams, openInNewWindow);
        };
        NavigationHelper.openListViewByName = function (listViewName, optionalParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            if (String.isNullOrEmpty(listViewName)) {
                return;
            }
            var addr = new Address();
            addr.setName('ListView');
            addr.setParameter('ViewName', listViewName);
            NavigationHelper.addListViewParameters(addr, optionalParams);
            NavigationService.navigateToAddress(addr, null, null, null, openInNewWindow);
        };
        NavigationHelper.addDetailViewParameters = function (addr, params) {
            if (params == null) {
                return '';
            }
            addr.onNavigated(params != null ? params.onNavigated : null);
            if (!isNaN(params.pdoId) && params.pdoId > 0) {
                addr.setParameter('PdoId', params.pdoId);
            }
            if (!String.isNullOrEmpty(params.contextId)) {
                addr.setHiddenParameter('ContextID', params.contextId);
            }
            if (!isNaN(params.contextObject) && params.contextObject > 0) {
                addr.setHiddenParameter('ContextObject', params.contextObject);
            }
            if (!String.isNullOrEmpty(params.contextTypeName)) {
                addr.setHiddenParameter('ContextTypeName', params.contextTypeName);
            }
            if (!String.isNullOrEmpty(params.contextRelation)) {
                addr.setHiddenParameter('ContextRelation', params.contextRelation);
            }
            if (!String.isNullOrEmpty(params.listViewId)) {
                addr.setParameter('ViewID', params.listViewId);
            }
        };
        NavigationHelper.openDetailView = function (action, typeName, optionalParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            var addr = new Address();
            addr.setName('DetailView');
            addr.setParameters({ Action: action, TypeName: typeName });
            NavigationHelper.addDetailViewParameters(addr, optionalParams);
            NavigationService.navigateToAddress(addr, null, null, null, openInNewWindow);
        };
        NavigationHelper.openDetailViewAndConnect = function (action, typeName, toConnectPdoId, toConnectTypeName, contextRelation) {
            return NavigationHelper.openDetailView(action, typeName, {
                pdoId: -1,
                contextObject: toConnectPdoId,
                contextTypeName: toConnectTypeName,
                contextRelation: contextRelation
            });
        };
        NavigationHelper.addExtensionViewParameters = function (addr, params) {
            if (params == null) {
                return '';
            }
            addr.onNavigated(params != null ? params.onNavigated : null);
            params = $.extend(true, {}, params);
            delete params.navigateTo;
            addr.setParameters(params);
        };
        NavigationHelper.openExtensionView = function (viewExtensionFactory, title, customParams, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            var addr = new Address();
            addr.setName('ExtensionView');
            addr.setParameters({ Factory: viewExtensionFactory, Title: title });
            NavigationHelper.addExtensionViewParameters(addr, customParams);
            if (openInNewWindow)
                NavigationService.navigateToAddress(addr, null, null, null, openInNewWindow);
            else
                NavigationService.navigateToAddress(addr);
        };
        NavigationHelper.openWebPage = function (url, target) {
            if (String.isNullOrEmpty(target)) {
                target = '_self';
            }
            window.open(url, target);
        };
        NavigationHelper.openSearch = function (query, pageSize, page, openinNewWindow) {
            var addr = new Address('Search');
            if (!String.isNullOrEmpty(query)) {
                addr.setParameter('Query', query);
            }
            else {
                addr.setParameter('Query', '');
            }
            if (pageSize != null && pageSize > 0) {
                addr.setParameter('PageSize', pageSize);
            }
            if (page != null && page >= 0) {
                addr.setParameter('Page', page);
            }
            addr.onNavigated(NavigationService.notifyNavigationReset);
            if (openinNewWindow)
                NavigationService.navigateToAddress(addr, null, null, null, openinNewWindow);
            else
                NavigationService.navigateToAddress(addr);
        };
        NavigationHelper.associationNavigator = new DefaultAssociationNavigator();
        return NavigationHelper;
    }());
    return NavigationHelper;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTmF2aWdhdGlvbkhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk5hdmlnYXRpb25IZWxwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0RBQW9EOzs7Ozs7Ozs7Ozs7O0lBUXBEO1FBQTBDLCtDQUFvQjtRQUUxRDttQkFDSSxpQkFBTztRQUNYLENBQUM7UUFFTSx5REFBbUIsR0FBMUIsVUFBMkIsUUFBUSxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsZUFBZ0M7WUFBaEMsZ0NBQUEsRUFBQSx1QkFBZ0M7WUFDOUYsSUFBSSxXQUFXLEtBQUssSUFBSSxJQUFJLFdBQVcsS0FBSyxTQUFTLEVBQUU7Z0JBQ25ELE9BQU87YUFDVjtZQUdELElBQUksY0FBYyxJQUFJLElBQUksSUFBSSxjQUFjLENBQUMsYUFBYSxLQUFLLElBQUksRUFBRTtnQkFDakUsSUFBSSxjQUFjLENBQUMsdUJBQXVCLENBQUMsMkJBQTJCLENBQUMsRUFBRTtvQkFDckUsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDMUUsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2lCQUM5RTtxQkFDSTtvQkFDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUN4RSxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2lCQUMxRTthQUNKO2lCQUNJO2dCQUNELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQ3hFLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsZUFBZSxDQUFDLENBQUM7YUFDMUU7UUFDTCxDQUFDO1FBQ0wsa0NBQUM7SUFBRCxDQUFDLEFBM0JELENBQTBDLG9CQUFvQixHQTJCN0QsQ0FBQywyQ0FBMkM7SUFHN0M7O09BRUc7SUFDSDtRQUFBO1FBNE5BLENBQUM7UUF4TmlCLHdDQUF1QixHQUFyQyxVQUFzQyxxQkFBcUI7WUFDdkQsZ0JBQWdCLENBQUMsb0JBQW9CLEdBQUcscUJBQXFCLENBQUM7UUFDbEUsQ0FBQztRQUVEOztXQUVHO1FBQ1csb0NBQW1CLEdBQWpDO1lBQ0ksSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2pELGdCQUFnQixDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqSCxDQUFDO1FBRWEsc0NBQXFCLEdBQW5DLFVBQW9DLElBQUksRUFBRSxNQUFNO1lBQzVDLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUU3RCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwRDtZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlDO1lBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDbEU7WUFFRCxJQUFJLE1BQU0sQ0FBQyxzQkFBc0IsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLHNCQUFzQixLQUFLLEtBQUssRUFBRTtnQkFDbkYsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixFQUFFLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2FBQ3BGO1lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksTUFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUN4RDtZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUM7bUJBQzFDLE1BQU0sQ0FBQyxhQUFhLElBQUksU0FBUyxJQUFJLE1BQU0sQ0FBQyxhQUFhLEdBQUcsQ0FBQzttQkFDN0QsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFDbEQ7Z0JBQ0UsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDbkUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ2xFO1lBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUM5RDtZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO2dCQUNuRCxJQUFJLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxvQ0FBb0MsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDMUcsSUFBSSxtQkFBbUIsSUFBSSxTQUFTLElBQUksbUJBQW1CLElBQUksU0FBUyxFQUFFO29CQUN0RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLG1CQUFtQixDQUFDLENBQUM7aUJBQ2pFO2FBQ0o7WUFFRCxJQUFJLE1BQU0sQ0FBQyxtQkFBbUIsS0FBSyxLQUFLLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN6RDtZQUVELElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxTQUFTLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFLEVBQUU7Z0JBQzdELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksTUFBTSxDQUFDLFdBQVcsWUFBWSxJQUFJLEVBQUU7b0JBQ3BDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUNqRTtxQkFDSSxJQUFJLE9BQU8sTUFBTSxDQUFDLFdBQVcsSUFBSSxRQUFRLEVBQUU7b0JBQzVDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7aUJBQ3pDO2dCQUNELDZCQUE2QjtxQkFDeEIsSUFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLElBQUksUUFBUSxFQUFFO29CQUM1QyxnQkFBZ0IsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztpQkFDbkc7Z0JBRUQsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLGdCQUFnQixDQUFDLENBQUM7aUJBQ3REO2FBQ0o7UUFDTCxDQUFDO1FBRWEsaUNBQWdCLEdBQTlCLFVBQStCLFVBQVUsRUFBRSxjQUFjLEVBQUUsZUFBZ0M7WUFBaEMsZ0NBQUEsRUFBQSx1QkFBZ0M7WUFDdkYsSUFBSSxVQUFVLElBQUksQ0FBQztnQkFBRSxPQUFPO1lBRTVCLElBQUksSUFBSSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUN4QyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7WUFDN0QsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ2pGLENBQUM7UUFBQSxDQUFDO1FBRVksbUNBQWtCLEdBQWhDLFVBQWlDLFFBQVEsRUFBRSxjQUFjLEVBQUUsZUFBZ0M7WUFBaEMsZ0NBQUEsRUFBQSx1QkFBZ0M7WUFDdkYsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNoQyxPQUFPO2FBQ1Y7WUFFRCxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEdBQUcsS0FBSyxFQUFFLGNBQWMsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUMzRixDQUFDO1FBRWEsbUNBQWtCLEdBQWhDLFVBQWlDLFlBQVksRUFBRSxjQUFjLEVBQUUsZUFBZ0M7WUFBaEMsZ0NBQUEsRUFBQSx1QkFBZ0M7WUFDM0YsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUNwQyxPQUFPO2FBQ1Y7WUFFRCxJQUFJLElBQUksR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDNUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQzdELGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUNqRixDQUFDO1FBRWEsd0NBQXVCLEdBQXJDLFVBQXNDLElBQUksRUFBRSxNQUFNO1lBQzlDLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsT0FBTyxFQUFFLENBQUM7YUFDYjtZQUVELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFN0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQzFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM1QztZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDekMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDMUQ7WUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBRTtnQkFDMUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDbEU7WUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDdEU7WUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDdEU7WUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQzFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNsRDtRQUNMLENBQUM7UUFFYSwrQkFBYyxHQUE1QixVQUE2QixNQUFNLEVBQUUsUUFBUSxFQUFFLGNBQWMsRUFBRSxlQUFnQztZQUFoQyxnQ0FBQSxFQUFBLHVCQUFnQztZQUMzRixJQUFJLElBQUksR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7WUFDM0QsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQy9ELGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUNqRixDQUFDO1FBRWEseUNBQXdCLEdBQXRDLFVBQXVDLE1BQU0sRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLGlCQUFpQixFQUFFLGVBQWU7WUFDdkcsT0FBTyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRTtnQkFDckQsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDVCxhQUFhLEVBQUUsY0FBYztnQkFDN0IsZUFBZSxFQUFFLGlCQUFpQjtnQkFDbEMsZUFBZSxFQUFFLGVBQWU7YUFDbkMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDJDQUEwQixHQUF4QyxVQUF5QyxJQUFJLEVBQUUsTUFBTTtZQUNqRCxJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQ2hCLE9BQU8sRUFBRSxDQUFDO2FBQ2I7WUFFRCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTdELE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEMsT0FBTyxNQUFNLENBQUMsVUFBVSxDQUFDO1lBRXpCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUVhLGtDQUFpQixHQUEvQixVQUFnQyxvQkFBb0IsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLGVBQWdDO1lBQWhDLGdDQUFBLEVBQUEsdUJBQWdDO1lBQ3ZHLElBQUksSUFBSSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUM5QixJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ3BFLGdCQUFnQixDQUFDLDBCQUEwQixDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztZQUNoRSxJQUFJLGVBQWU7Z0JBQ2YsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDOztnQkFFN0UsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEQsQ0FBQztRQUVhLDRCQUFXLEdBQXpCLFVBQTBCLEdBQUcsRUFBRSxNQUFNO1lBQ2pDLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDOUIsTUFBTSxHQUFHLE9BQU8sQ0FBQzthQUNwQjtZQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLENBQUM7UUFFYSwyQkFBVSxHQUF4QixVQUF5QixLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxlQUFlO1lBQzNELElBQUksSUFBSSxHQUFHLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNyQztpQkFDSTtnQkFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNsQztZQUVELElBQUksUUFBUSxJQUFJLElBQUksSUFBSSxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUMzQztZQUNELElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFO2dCQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNuQztZQUVELElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUUxRCxJQUFHLGVBQWU7Z0JBQ2QsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDOztnQkFFN0UsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEQsQ0FBQztRQXpOYyxxQ0FBb0IsR0FBRyxJQUFJLDJCQUEyQixFQUFFLENBQUM7UUEwTjVFLHVCQUFDO0tBQUEsQUE1TkQsSUE0TkM7SUFFRCxPQUFTLGdCQUFnQixDQUFDIn0=