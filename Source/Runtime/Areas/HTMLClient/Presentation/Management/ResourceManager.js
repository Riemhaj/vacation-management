///<amd-module name="Management/ResourceManager" />
define("Management/ResourceManager", ["require", "exports", "Utility/LocalizationHelper", "Utility/ImageHandler", "Utility/StorageHelper", "Communication/RADEnvironmentProxy"], function (require, exports, LocalizationHelper, ImageHandler, StorageHelper, RADEnvironmentProxy) {
    "use strict";
    /**
     * Repräsentiert eine Einheit zur Verwaltung der Sprachvariaten und zum Wechsel der Sprache
     */
    var ResourceManager = /** @class */ (function () {
        function ResourceManager() {
        }
        /**
         * Liefert das DTO der aktuell ausgewählten Sprache
         * @returns {LanguageDTO|null} Das Language-DTO der ausgewählten Sprache oder null
         */
        ResourceManager.getLanguage = function () {
            var RADApplication = requirejs('Management/RADApplication');
            var value = ResourceManager.language.value();
            for (var i in RADApplication.configuration.languages) {
                var language = RADApplication.configuration.languages[i];
                if (language.iso6391 === value || language.cultureInfoName === value) {
                    return language;
                }
            }
            return null;
        };
        /**
         * Liefert die Ressource zu einem ergonomischen Bezeichner
         */
        ResourceManager.get = function (ergName) {
            var resources = ResourceManager.resources();
            if (ergName != null) {
                if (ergName in resources) {
                    return resources[ergName];
                }
                if (ergName.toUpperCase() in resources) {
                    return resources[ergName.toUpperCase()];
                }
            }
            return ergName;
        };
        ResourceManager.loadResource = function (ergName, tryToLoad) {
            var language = ResourceManager.language.value();
            var resources = ResourceManager.resources();
            var resource = null;
            if (ergName) {
                if (ergName in resources) {
                    resource = resources[ergName];
                }
                else {
                    var upperErgName = ergName.toUpperCase();
                    if (upperErgName in resources) {
                        resource = resources[upperErgName];
                    }
                    else if (tryToLoad === true) {
                        return RADEnvironmentProxy.getResourceItem({
                            lang: language,
                            resourceName: upperErgName
                        }).then(function (data) {
                            resources[ergName] = data;
                            return data;
                        });
                    }
                }
                if (resource == null) {
                    resource = ergName;
                }
            }
            return resource;
        };
        ResourceManager.setLanguage = function (callback) {
            RADEnvironmentProxy.changeLanguage({
                lang: ResourceManager.language.value()
            }).then(callback);
        };
        ResourceManager.loadLanguageArtfacts = function (language) {
            var reload = false;
            if (!language) {
                language = ResourceManager.language.value();
                reload = true;
            }
            var lang = language.toLowerCase();
            if (lang != ResourceManager.currentLoadedLang || reload) {
                ResourceManager.currentLoadedLang = lang;
                var cultureSource = MODEL.HTMLClientResourceURL + 'Scripts/kendo/kendo.culture.' + lang;
                var messageSource = MODEL.HTMLClientResourceURL + 'Scripts/kendo/kendo.messages.' + lang;
                requirejs([cultureSource, messageSource], function () {
                    kendo.culture(lang);
                });
                var dfd = RADEnvironmentProxy.getAllResourceItems({
                    lang: language
                }).then(function (data) {
                    ResourceManager.resources(data);
                });
                return dfd;
            }
            else {
                var dfd = $.Deferred().resolve();
                return dfd.promise();
            }
        };
        ResourceManager.init = function () {
            var _this = this;
            var RADApplication = requirejs('Management/RADApplication');
            /* Die Liste der verfügbaren und die aktuell ausgewählte Sprache */
            if (!ResourceManager.language.value()) {
                ResourceManager.language.value(RADApplication.configuration.defaultLanguage.iso6391);
            }
            var dfd = this.loadLanguageArtfacts(ResourceManager.language.value());
            /*
             * Aktualisiert die Liste verfügbarer Sprachen anhand Informationen aus der RADApplication
             */
            ko.computed(function () {
                var data = [];
                for (var i in RADApplication.configuration.languages) {
                    var language = RADApplication.configuration.languages[i];
                    language.fullName = LocalizationHelper.getLanguageFullName(language.cultureInfoName);
                    language.iconUrl = ImageHandler.convertSource(language.iconUrl);
                    data.push(language);
                }
                ResourceManager.language.data(data);
            });
            /*
             * Läd Ressource-Elemente und Kendo-Sprachdateien sobald die ausgewählte Sprache gewechselt wird
             */
            ko.computed(function () {
                var language = ResourceManager.language.value();
                // Accessibility: Aktuelle Sprache setzen
                $('html').attr('lang', language);
                if (language != null) {
                    if (RADApplication.user.isLoggedIn() === true) {
                        _this.setLanguage();
                    }
                    _this.loadLanguageArtfacts(language);
                }
            });
            return dfd;
        };
        ResourceManager.getTranslationInMLString = function (mlstring, language) {
            if (mlstring.indexOf(ResourceManager.MLSTRING_SEPARATOR) >= -1) {
                var languages = mlstring.split(ResourceManager.MLSTRING_SEPARATOR);
                var translation_1 = mlstring;
                languages.forEach(function (l) {
                    if (l.indexOf(language + ':') == 0)
                        translation_1 = l.substring(language.length + 1);
                });
                return translation_1;
            }
            else {
                return mlstring;
            }
        };
        /**
         * Ressource-Elemente der aktuellen Sprachvariante
         */
        ResourceManager.resources = ko.observable({});
        /**
         * Liste aller aktiven Sprachen für die Anwendung
         */
        ResourceManager.language = {
            data: ko.observableArray(),
            value: StorageHelper.bindGlobal('lastLoggedInLanguage', null)
        };
        ResourceManager.currentLoadedLang = '';
        ResourceManager.MLSTRING_SEPARATOR = '!#/sp[$)';
        return ResourceManager;
    }());
    return ResourceManager;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVzb3VyY2VNYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUmVzb3VyY2VNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG1EQUFtRDs7O0lBVW5EOztPQUVHO0lBQ0g7UUFBQTtRQWdNQSxDQUFDO1FBN0tHOzs7V0FHRztRQUNXLDJCQUFXLEdBQXpCO1lBQ0ksSUFBSSxjQUFjLEdBQUcsU0FBUyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDNUQsSUFBSSxLQUFLLEdBQUcsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUU3QyxLQUFLLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFO2dCQUNsRCxJQUFJLFFBQVEsR0FBRyxjQUFjLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFekQsSUFBSSxRQUFRLENBQUMsT0FBTyxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsZUFBZSxLQUFLLEtBQUssRUFBRTtvQkFDbEUsT0FBTyxRQUFRLENBQUM7aUJBQ25CO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyxtQkFBRyxHQUFqQixVQUFrQixPQUFlO1lBQzdCLElBQUksU0FBUyxHQUFHLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUU1QyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ2pCLElBQUksT0FBTyxJQUFJLFNBQVMsRUFBRTtvQkFDdEIsT0FBTyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzdCO2dCQUVELElBQUksT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLFNBQVMsRUFBRTtvQkFDcEMsT0FBTyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7aUJBQzNDO2FBQ0o7WUFFRCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDO1FBRWEsNEJBQVksR0FBMUIsVUFBMkIsT0FBZSxFQUFFLFNBQW1CO1lBQzNELElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEQsSUFBSSxTQUFTLEdBQUcsZUFBZSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzVDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztZQUVwQixJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFJLE9BQU8sSUFBSSxTQUFTLEVBQUU7b0JBQ3RCLFFBQVEsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2pDO3FCQUFNO29CQUNILElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFFekMsSUFBSSxZQUFZLElBQUksU0FBUyxFQUFFO3dCQUMzQixRQUFRLEdBQUcsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUN0Qzt5QkFBTSxJQUFJLFNBQVMsS0FBSyxJQUFJLEVBQUU7d0JBQzNCLE9BQU8sbUJBQW1CLENBQUMsZUFBZSxDQUFDOzRCQUN2QyxJQUFJLEVBQUUsUUFBUTs0QkFDZCxZQUFZLEVBQUUsWUFBWTt5QkFDN0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7NEJBQ1QsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQzs0QkFDMUIsT0FBTyxJQUFJLENBQUM7d0JBQ2hCLENBQUMsQ0FBQyxDQUFDO3FCQUNOO2lCQUNKO2dCQUVELElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtvQkFDbEIsUUFBUSxHQUFHLE9BQU8sQ0FBQztpQkFDdEI7YUFDSjtZQUVELE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7UUFFYSwyQkFBVyxHQUF6QixVQUEwQixRQUFjO1lBQ3BDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQztnQkFDL0IsSUFBSSxFQUFFLGVBQWUsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO2FBQ3pDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsQ0FBQztRQUVhLG9DQUFvQixHQUFsQyxVQUFtQyxRQUFnQjtZQUMvQyxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDWCxRQUFRLEdBQUcsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNqQjtZQUVELElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNsQyxJQUFJLElBQUksSUFBSSxlQUFlLENBQUMsaUJBQWlCLElBQUksTUFBTSxFQUFFO2dCQUNyRCxlQUFlLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2dCQUV6QyxJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMscUJBQXFCLEdBQUcsOEJBQThCLEdBQUcsSUFBSSxDQUFDO2dCQUN4RixJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMscUJBQXFCLEdBQUcsK0JBQStCLEdBQUcsSUFBSSxDQUFDO2dCQUV6RixTQUFTLENBQUMsQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDLEVBQUU7b0JBQ3RDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksR0FBRyxHQUFHLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDO29CQUM5QyxJQUFJLEVBQUUsUUFBUTtpQkFDakIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7b0JBQ1QsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsT0FBTyxHQUFHLENBQUM7YUFDZDtpQkFDSTtnQkFDRCxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2pDLE9BQU8sR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQztRQUVhLG9CQUFJLEdBQWxCO1lBQUEsaUJBOENDO1lBN0NHLElBQUksY0FBYyxHQUFHLFNBQVMsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBRTVELG1FQUFtRTtZQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDbkMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDeEY7WUFFRCxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBRXRFOztlQUVHO1lBQ0gsRUFBRSxDQUFDLFFBQVEsQ0FBQztnQkFDUixJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBRWQsS0FBSyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRTtvQkFDbEQsSUFBSSxRQUFRLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pELFFBQVEsQ0FBQyxRQUFRLEdBQUcsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUNyRixRQUFRLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUVoRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QjtnQkFFRCxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUVIOztlQUVHO1lBQ0gsRUFBRSxDQUFDLFFBQVEsQ0FBQztnQkFDUixJQUFJLFFBQVEsR0FBRyxlQUFlLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUVoRCx5Q0FBeUM7Z0JBQ3pDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUVqQyxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7b0JBQ2xCLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSyxJQUFJLEVBQUU7d0JBQzNDLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztxQkFDdEI7b0JBRUQsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxHQUFHLENBQUM7UUFDZixDQUFDO1FBR2Esd0NBQXdCLEdBQXRDLFVBQXVDLFFBQWdCLEVBQUUsUUFBZ0I7WUFDckUsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUM1RCxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUNuRSxJQUFJLGFBQVcsR0FBRyxRQUFRLENBQUM7Z0JBQzNCLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO29CQUNoQixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7d0JBQzlCLGFBQVcsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELENBQUMsQ0FBQyxDQUFDO2dCQUVILE9BQU8sYUFBVyxDQUFDO2FBQ3RCO2lCQUNJO2dCQUNELE9BQU8sUUFBUSxDQUFDO2FBQ25CO1FBQ0wsQ0FBQztRQTVMRDs7V0FFRztRQUNXLHlCQUFTLEdBQTJCLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFcEU7O1dBRUc7UUFDVyx3QkFBUSxHQUFxQjtZQUN2QyxJQUFJLEVBQUUsRUFBRSxDQUFDLGVBQWUsRUFBRTtZQUMxQixLQUFLLEVBQUUsYUFBYSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUM7U0FDaEUsQ0FBQztRQUVZLGlDQUFpQixHQUFXLEVBQUUsQ0FBQztRQUN0QixrQ0FBa0IsR0FBVyxVQUFVLENBQUM7UUFnTG5FLHNCQUFDO0tBQUEsQUFoTUQsSUFnTUM7SUFFRCxPQUFTLGVBQWUsQ0FBQyJ9