///<amd-module name="Management/NavigationService" />
define("Management/NavigationService", ["require", "exports", "Model/Address", "Management/RADApplication", "Management/ComponentManager", "Utility/Loader", "Utility/Messenger", "Communication/RADEnvironmentProxy"], function (require, exports, Address, RADApplication, ComponentManager, Loader, Messenger, RADEnvironmentProxy) {
    "use strict";
    /**
     * Analysiert die URL-Anfragen an den Browser und erzeugt die registrierte ViewFactory
     */
    var NavigationService = /** @class */ (function () {
        function NavigationService() {
            NavigationService.login = new Address('Login');
            NavigationService.logout = new Address('Logout');
            NavigationService.changePassword = new Address('ChangePassword');
            NavigationService.home = null;
            NavigationService.start = null;
            NavigationService.baseUrl = NavigationService.getBaseUrl();
            NavigationService._onNavigateBack = null;
            NavigationService._forceNavigate = false;
            NavigationService.activeFactories = {};
            NavigationService.lastFactories = {};
            NavigationService.visibleFactory = null;
            NavigationService.redirectingAddress = null;
            NavigationService._isNavigatingBack = false;
            NavigationService.stateCounter = 0;
            NavigationService.historyStateStack = [];
            NavigationService.viewFactories = [];
        }
        /**
         * Versucht eine zuletzt aufgerufene Factory anhand des Namen zu finden,
         * welche zudem nicht die derzeit sichtbare Factory ist
         */
        NavigationService.findLastFactory = function (name) {
            var lastFactory = NavigationService.lastFactories[name];
            // Falls wir auf eine inzwischen disposed-e Factory stoßen, räumen wir noch mal auf
            if (lastFactory != null && lastFactory.isDisposed) {
                delete NavigationService.lastFactories[name];
                lastFactory = null;
            }
            if (lastFactory != null) {
                if (lastFactory !== NavigationService.visibleFactory) {
                    return lastFactory;
                }
            }
            return null;
        };
        /**
         * Entfernt eine aktive Factory indem diese zunächst disposed wird
         * und anschließend aus dem Objekt entfernt wird
         */
        NavigationService.removeActiveFactory = function (name) {
            var activeFactory = null;
            for (var id in NavigationService.activeFactories) {
                if (NavigationService.activeFactories[id].name === name) {
                    activeFactory = NavigationService.activeFactories[id];
                    break;
                }
            }
            NavigationService.removeViewFactory(activeFactory);
        };
        /**
         * Registriert eine neue aktive Factory
         * indem diese ins Objekt eingetragen wird
         */
        NavigationService.registerActiveFactory = function (factory) {
            NavigationService.activeFactories[factory.id] = factory;
        };
        /**
         * Registriert eine neue zuletzt aufgerufene Factory
         * indem diese ins Objekt eingetragen wird
         */
        NavigationService.registerLastFactory = function (factory, altName) {
            if (factory.name != 'ListView' && factory.name != 'DetailView') {
                NavigationService.lastFactories[altName] = factory;
            }
            else {
                NavigationService.lastFactories[factory.name] = factory;
            }
        };
        /**
         * Registriert eine neue zuletzt aufgerufene Factory
         * indem diese ins Objekt eingetragen wird
         */
        NavigationService.registerVisibleFactory = function (factory) {
            NavigationService.visibleFactory = factory;
        };
        /**
         * Läd eine ViewFactory
         */
        NavigationService._loadViewFactory = function (name, viewFactory, tryToReuse, disposeOther, address) {
            if (disposeOther !== false && tryToReuse !== true) {
                NavigationService.removeActiveFactory(viewFactory.name);
            }
            NavigationService.registerActiveFactory(viewFactory);
            NavigationService.registerLastFactory(viewFactory, address ? address.toString() : viewFactory.name);
            NavigationService.registerVisibleFactory(viewFactory);
            viewFactory.getTuple();
        };
        /**
         * Berechnung der Home-Adresse
         */
        NavigationService.getHomeAddress = function () {
            return new Address('/Home');
        };
        NavigationService.getMappedHomeAddress = function () {
            var factoryModel = '';
            if (!String.isNullOrEmpty(RADApplication.configuration.startpageUIFactory)) {
                if (ComponentManager.getFactory(RADApplication.configuration.startpageUIFactory) != null) {
                    return new Address(RADApplication.configuration.startpageUIFactory);
                }
                factoryModel = RADApplication.configuration.startpageUIFactory;
            }
            else {
                factoryModel = 'StartpageViewFactory';
            }
            return new Address('/ExtensionView?Factory=' + factoryModel);
        };
        /**
         * Berechnung der Start-Adresse
         */
        NavigationService.getStartAddress = function () {
            if (MODEL.StartViewName) {
                var browserPath = window.location.pathname.toLowerCase();
                var startpage = MODEL.StartViewName.toLowerCase();
                var idx = browserPath.indexOf(startpage, browserPath.length - startpage.length);
                if (idx > -1) {
                    var queryParams = window.location.search ;
                    if(queryParams.indexOf("scope")>-1){
                        //Edited by P-cation 20201230
                        queryParams=queryParams.substring(0,queryParams.indexOf("scope")-1);
                        var ad= new Address('Home');
                        ad.start='Home';
                        ad.url="/Home";
                        ad.setName('Home');
                      var parameters ={};
                        var nameValuePairs = queryParams.split('?')[1].split('&');
                        if (nameValuePairs.length > 0) {
                            for (var i = 0; i < nameValuePairs.length; i++) {
                                var nameValuePairStr = nameValuePairs[i];
                                var eqIdx = nameValuePairStr.indexOf('=');
                                var key = nameValuePairStr.substring(0, eqIdx);
                                var val = nameValuePairStr.substring(eqIdx + 1);
                               if (key.toLowerCase() == 'filter' && val.length >= 2 && val.charAt(0) == "'" && val.charAt(val.length - 1) == "'") {
                                    val = val.substring(1, val.length - 1);
                                }
                                try {
                                    val = decodeURIComponent(val);
                                }
                                catch (e) {
                                    
                                }
                                parameters[key] = val;
                            }
                            ad.setParameters(parameters);
                        }
                        return ad;
                    }
                    
    
                    return new Address(window.location.pathname + queryParams);
                }
            }
            return new Address('');
        };
        /**
         * Berechnung der BaseURL
         */
        NavigationService.getBaseUrl = function () {
            var baseUrl = window.location.pathname;
            if (MODEL.StartViewName) {
                var browserPath = window.location.pathname.toLowerCase();
              
                var startpage = MODEL.StartViewName.toLowerCase();
                var idx = browserPath.indexOf(startpage, browserPath.length - startpage.length);
                if (idx > -1) {
                    baseUrl = window.location.pathname.substr(0, idx);
                }
            }
            if (baseUrl.indexOf('/', baseUrl.length - 1) == -1) {
                baseUrl += '/';
            }
            return baseUrl;
        };
        NavigationService.isNavigatingBack = function () {
            return NavigationService._isNavigatingBack;
        };
        /**
         * Liefert die derzeitig öffentlich sichtbare Adresse
         * (enthält nicht die ggf. versteckten Parameter)
         */
        NavigationService.getAddress = function () {
         
           var queryParams = window.location.search ;
           queryParams=queryParams.substring(0,queryParams.indexOf("scope"));
            return new Address(window.location.pathname + queryParams);
        };
        /**
         * Setzt / Ändert einen Hidden-Parameter der aktuellen Adresse
         */
        NavigationService.setHiddenParameterToAddress = function (parameterName, value) {
            var addr = NavigationService.getStatefulAddress();
            addr.setHiddenParameter(parameterName, value);
            var state = NavigationService.convertAddressToHistoryState(addr);
            _.last(NavigationService.historyStateStack).state = state;
            addr = null;
        };
        /**
         * Liefert die letzte zur Navigation verwendete Addresse
         * (inklusive ggf. versteckter Parameter)
         */
        NavigationService.getStatefulAddress = function () {
            var state = _.last(NavigationService.historyStateStack);
            return NavigationService.convertHistoryStateToAddress(state.state);
        };
        NavigationService.convertHistoryStateToAddress = function (state) {
            var address = new Address();
            address.setName(state.name);
            address.setParameters(state.parameters);
            address.setHiddenNames(state.hiddenNames);
            return address;
        };
        NavigationService.convertAddressToHistoryState = function (address) {
            return {
                counter: Date.now(),
                name: address.getName(),
                parameters: address.getParameters(),
                hiddenNames: address.getHiddenNames()
            };
        };
        NavigationService.go = function (state, title, url) {
            if (NavigationService.isNewState(state, title, url)) {
                NavigationService.historyStateStack.push({ state: state, title: title, url: url });
                history.pushState(state.counter, title, url);
            }
        };
        NavigationService.isNewState = function (state, title, url) {
            var last = _.last(NavigationService.historyStateStack);
            if (last
                && _.isMatch(state.parameters, last.state.parameters)
                && state.name == last.state.name
                && last.title == title)
                return false;
            return true;
        };
        NavigationService.revertNavigation = function () {
            var previous = _.last(NavigationService.historyStateStack);
            history.pushState(Date.now(), previous.title, previous.url);
        };
        /**
         * Bei einer Rückwärtsnavigation wird immer replace aufgerufen
         */
        NavigationService.back = function () {
            NavigationService.historyStateStack.pop();
            var previous = _.last(NavigationService.historyStateStack);
            if (previous) {
                history.pushState(Date.now(), previous.title, previous.url);
            }
        };
        /**
         * Sucht die zu einem DOM-Element gehörende ViewFactory
         */
        NavigationService.findFactory = function (element) {
            var factoryId = element.closest('[data-factory]').data('factory');
            if (factoryId != null) {
                return NavigationService.activeFactories[factoryId];
            }
            return null;
        };
        /**
         * Läd eine ViewFactory anhand ihres Namen
         */
        NavigationService.loadViewFactory = function (name, params, tryToReuse, address, disposeOther) {
            Loader.show();
            Messenger.createMessageAndSend('ViewFactoryLoadingMessage', { name: name });
            var viewFactory = null;
            if (tryToReuse === true) {
                if (name != 'ListView' && name != 'DetailView') {
                    viewFactory = NavigationService.findLastFactory(address.toString());
                }
                else {
                    viewFactory = NavigationService.findLastFactory(name);
                }
            }
            if (viewFactory != null) {
                // Jetzt Refresh der Factory durchführen; wenn dieser nicht erfolgreich ist, so bauen wir die Factory neu auf
                // Es gibt noch alte ViewFactories, die gar nichts zurückgeben; für diese soll auch das alte Verhalten bestehen bleiben, d.h.
                // wir prüfen nur, ob explizit false zurückgegeben wird.
                if (viewFactory.refresh() === false) {
                    // refresh war nicht erfolgreich; baue Factory neu auf
                    if (viewFactory != null && !viewFactory.isDisposed) {
                        viewFactory.dispose();
                    }
                    viewFactory = null;
                }
            }
            if (viewFactory == null) {
                viewFactory = NavigationService.loadViewFactoryIsolated(name, params, false);
            }
            if (viewFactory != null) {
                var redirect = viewFactory.getNecessaryRedirect();
                if (redirect) {
                    NavigationService.removeViewFactory(viewFactory);
                    NavigationService.redirectingAddress = address;
                    NavigationService.navigateToAddress(new Address(redirect));
                }
                else {
                    NavigationService._loadViewFactory(name, viewFactory, tryToReuse, disposeOther, address);
                }
                Loader.hide();
                return viewFactory;
            }
            else {
                Loader.hide();
                throw 'ViewFactory "' + name + '" could not be found.';
            }
        };
        /**
         * Entfernt eine ViewFactory.
         */
        NavigationService.removeViewFactory = function (viewFactory) {
            if (viewFactory == null) {
                return;
            }
            delete NavigationService.activeFactories[viewFactory.id];
            delete NavigationService.lastFactories[viewFactory.name];
            if (viewFactory == NavigationService.visibleFactory) {
                NavigationService.visibleFactory = null;
            }
            NavigationService.removeViewFactoryIsolated(viewFactory);
        };
        NavigationService.getModalViewFactories = function () {
            return NavigationService.viewFactories
                .filter(function (vf) {
                if (vf.isDisposed) {
                    return false;
                }
                return vf.tuple && vf.viewModel && vf.viewModel.window &&
                    vf.viewModel.window.isModal !== undefined &&
                    (vf.viewModel.window.isModal === true || vf.viewModel.window.isModal() === true);
            })
                .length;
        };
        /**
         * Lädt eine ViewFactory isoliert, d.h. ohne Abhängigkeiten zu setzen oder Registrierungen durchzuführen.
         */
        NavigationService.loadViewFactoryIsolated = function (name, params, loadTuple) {
            var viewFactory = null;
            var options = _.extend({ name: name }, params);
            var ViewFactory = ComponentManager.getFactory(name);
            if (ViewFactory != null) {
                viewFactory = new ViewFactory(options);
                if (loadTuple) {
                    viewFactory.getTuple();
                }
                NavigationService.viewFactories.push(viewFactory);
            }
            return viewFactory;
        };
        /**
         * Löscht eine ViewFactory ohne ggf. abhängige Registrierungen o.Ä. zu beachten.
         */
        NavigationService.removeViewFactoryIsolated = function (viewFactory) {
            if (viewFactory.isDisposed === false) {
                if (viewFactory.view != null && viewFactory.view.tuple != null && viewFactory.view.tuple.view != null) {
                    viewFactory.view.tuple.view.detach();
                }
                viewFactory.dispose();
                var i = _.findIndex(NavigationService.viewFactories, function (vf) {
                    return vf.id == viewFactory.id;
                });
                NavigationService.viewFactories.splice(i, 1);
                viewFactory = null;
            }
        };
        /**
         * Entfernt ein Element
         */
        NavigationService.removeNode = function (node) {
            var element = $(node);
            element.find('*').each(function () {
                $(this).unbind();
            });
            kendo.unbind(element);
            kendo.destroy(element);
            ko.removeNode(node);
            element.remove();
        };
        /**
         * Säubert eine Region
         */
        NavigationService.cleanRegion = function (name) {
            Messenger.createMessageAndSend('RegionCleaningMessage', { name: name });
            var self = this;
            $('[data-region="' + name + '"]').children().each(function () {
                var node = this;
                var view = $(node);
                view.detach();
                var id = view.data('factory');
                var viewFactory = self.activeFactories[id];
                if (viewFactory != null && viewFactory.view != null) {
                    viewFactory.view.dispose();
                    viewFactory.view = null;
                }
                if (view.is('[data-view]')) {
                    window.setTimeout(function () {
                        self.removeNode(node);
                    }, 60);
                }
                else {
                    self.removeNode(node);
                }
            });
        };
        /**
         * Navigiert zur vorherigen Seite zurück
         */
        NavigationService.navigateBack = function (onNavigated) {
            NavigationService._onNavigateBack = onNavigated;
            window.history.back();
        };
        NavigationService.forceNavigateBack = function (onNavigated) {
            NavigationService._forceNavigate = true;
            NavigationService._onNavigateBack = onNavigated;
            window.history.back();
        };
        /**
         * Aktualisiert die aktuelle ViewFactory
         */
        NavigationService.refresh = function () {
            var address = NavigationService.getAddress();
            NavigationService.navigateToAddress(address, false, true, false);
        };
        NavigationService.continueNavigation = function (navigationMessage) {
            if (navigationMessage.isDisposed && navigationMessage.address == null) {
                return;
            }
            var address = navigationMessage.address;
            var pushState = navigationMessage.pushState;
            var sendMessage = false; // Die Nachricht wurde ja schon geworfen
            var tryToReuse = navigationMessage.tryToReuse;
            NavigationService._isNavigatingBack = navigationMessage.isNavigatingBack;
            NavigationService.navigateToAddress(address, pushState, sendMessage, tryToReuse);
            NavigationService._isNavigatingBack = false;
        };
        /**
         * Startet die Navigation zu einer Adresse
         */
        NavigationService.navigateToAddress = function (address, pushState, sendMessage, tryToReuse, openInNewWindow) {
            if (openInNewWindow) {
                window.open(NavigationService.baseUrl + address.getPublicURL());
                return true;
            }
            var message = null;
            if (sendMessage == null || sendMessage === true) {
                message = Messenger.createMessageAndSend('NavigationStartedMessage', { address: address, pushState: pushState, tryToReuse: tryToReuse, isNavigatingBack: NavigationService._isNavigatingBack });
            }
            if (message == null || !message.isCanceled()) {
                Messenger.createMessageAndSend('OnBeforeNavigationMessage', {});
                var state = NavigationService.convertAddressToHistoryState(address);
                var title = RADApplication.appName();
                var url = NavigationService.baseUrl + address.getPublicURL();
                if (!NavigationService._isNavigatingBack) {
                    NavigationService.go(state, title, url);
                }
                else {
                    NavigationService.back();
                }
                address.navigated(true);
                NavigationService.loadViewFactory(address.getName(), address.getParameters(), tryToReuse, address);
                return true;
            }
            else {
                return false;
            }
        };
        NavigationService.notifyNavigationReset = function () {
            Messenger.createMessageAndSend('NavigationResetMessage');
        };
        /**
         * Navigiert zu der zuletzt weiterleitenden Adresse
         */
        NavigationService.navigateToRedirectingAddress = function () {
            if (NavigationService.redirectingAddress != null) {
                NavigationService.navigateToAddress(NavigationService.redirectingAddress, true, true, false);
            }
        };
        /**
         * Startet die Navigation zu einer URL
         */
        NavigationService.navigateToUrl = function (url, onNavigated, pushState, sendMessage, tryToReuse, openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            if (url && url[0] === '/') {
                url = url.substring(1);
            }
            if (url.indexOf("coreframe:") == 0) {
                NavigationService.handlCoreframeUrl(url);
                return;
            }
            var address = new Address(url);
            if ($.isFunction(onNavigated)) {
                address.onNavigated(onNavigated);
            }
            NavigationService.navigateToAddress(address, pushState, sendMessage, tryToReuse, openInNewWindow);
        };
        /**
         * Verarbeitet eine CoreFrame-URL und führt die hinterlegte Method aus
         */
        NavigationService.handlCoreframeUrl = function (url) {
            var coreframeUrl = url.replace("coreframe:", "");
            coreframeUrl = coreframeUrl.substr(0, coreframeUrl.indexOf("&"));
            var params = coreframeUrl.split("|");
            if (params.length != 2)
                return;
            var module = params[0];
            var method = params[1];
            var resolvedModule = require(module);
            resolvedModule[method]();
        };
        /**
         * Startet die Navigation zum Login
         */
        NavigationService.navigateToLogin = function () {
            NavigationService.navigateToAddress(NavigationService.login);
        };
        NavigationService.navigateToChangePassword = function () {
            var viewFactory = NavigationService.loadViewFactoryIsolated(NavigationService.changePassword.getName(), {
                onClose: function () {
                    NavigationService.removeViewFactoryIsolated(viewFactory);
                    viewFactory = null;
                }
            }, true);
        };
        /**
         * Startet die Navigation zum Logout
         */
        NavigationService.navigateToLogout = function () {
            NavigationService.navigateToAddress(NavigationService.logout);
        };
        /**
         * Startet die Navigation zur initial aufgerufenen Adresse
         */
        NavigationService.navigateToStart = function () {
            NavigationService.navigateToAddress(NavigationService.start);
        };
        /**
         * Startet die Navigation zur eingerichteten Startseite
         */
        NavigationService.navigateToHome = function (openInNewWindow) {
            if (openInNewWindow === void 0) { openInNewWindow = false; }
            NavigationService.navigateToAddress(NavigationService.home, null, null, null, openInNewWindow);
        };
        /**
         * Läd die Basis-Seite der SPA neu
         */
        NavigationService.reload = function () {
            window.location.href = NavigationService.baseUrl;
        };
        /**
         * Initialisiert den NavigationService und leitet initial an das Login-Modul weiter
         * Registriert eine Funktion an den Browserevents "Vor" und "Zurück" um vorherigen Zustand wiederherstellen zu können
         */
        NavigationService.init = function () {
            NavigationService.home = NavigationService.getHomeAddress();
            NavigationService.start = NavigationService.getStartAddress();
            if (NavigationService.start.isEqualTo([NavigationService.login, NavigationService.logout])) {
                NavigationService.start = NavigationService.home;
            }
            NavigationService.start.onNavigated(NavigationService.notifyNavigationReset);
            if (NavigationService.home != NavigationService.start) {
                NavigationService.home.onNavigated(NavigationService.notifyNavigationReset);
            }
            // Bei jeder durch Browser-Buttons verursachten Änderung des Browser-History-Stacks wird diese Methode aufgerufen
            // Ein manuelles Aufrufen von pushState (wie in self.setState) löst dies nicht aus.
            window.onpopstate = function (event) {
                var now = Date.now();
                var eventState = event.state;
                var fallbackToHome = eventState == null || (eventState < now && NavigationService.historyStateStack.length < 2);
                if (fallbackToHome) {
                    NavigationService.historyStateStack = [];
                    NavigationService.navigateToHome();
                    return;
                }
                var isBackwardNavigation = eventState < now;
                //stateCounter = eventState.counter;
                // hier muss die adresse von dem eigenen stack kommen!
                if (isBackwardNavigation && NavigationService.historyStateStack.length > 1) {
                    var navPoint = NavigationService.historyStateStack.slice(-2, -1);
                    eventState = navPoint[0].state;
                }
                var address = NavigationService.convertHistoryStateToAddress(eventState);
                if (isBackwardNavigation) {
                    var _userOnNavigateBack = NavigationService._onNavigateBack;
                    NavigationService._onNavigateBack = null;
                    // Wenn wir zurücknavigieren, dann soll die jetzt sichtbare ViewFactory komplett zerstört werden;
                    var onNavigated = function () {
                        if (NavigationService.visibleFactory != null) {
                            NavigationService.removeViewFactory(NavigationService.visibleFactory);
                        }
                        if ($.isFunction(_userOnNavigateBack)) {
                            _userOnNavigateBack();
                        }
                    };
                    address.onNavigated(onNavigated);
                }
                else {
                    // Vorwärts navigation
                    var lastState = _.last(NavigationService.historyStateStack);
                    history.replaceState(lastState.state, lastState.title, lastState.url);
                    return;
                }
                if (address.isEqualTo([NavigationService.login, NavigationService.logout])) {
                    address = NavigationService.home;
                }
                NavigationService._isNavigatingBack = isBackwardNavigation;
                // Bei einer Navigation über die Pfeiltasten(Browser) ist tryToReuse niemals auf True.
                // Weil die Identifizierung der Wiederherstellbarkeit allein über den Typ der Ansicht erfolgt.
                // Wird also von einer LV zu LV navigiert, wird die falsche LV zur Wiederherstellung verwendet.
                if (NavigationService._forceNavigate || !RADApplication.isModalWindowOpen()) {
                    NavigationService._forceNavigate = false;
                    var hasNavigated = self.navigateToAddress(address, false, true, true);
                    if (!hasNavigated)
                        NavigationService.revertNavigation();
                }
                else {
                    NavigationService.revertNavigation();
                }
                NavigationService._isNavigatingBack = false;
            };
            window.onbeforeunload = function () {
                var currentPage = NavigationService.getAddress();
                //Auf der Loginseite muss kein Logout gefeuert werden.
                if (currentPage === null || !currentPage.isEqualTo([NavigationService.login])) {
                    RADEnvironmentProxy.autoLogout({
                        _async: false
                    });
                }
            };
            var self = this;
            $('body').on('click', 'a', function (event) {
                var $this = $(this);
                var href = $this.attr('href');
                if (!$this.data('ignoreByRAD') && !(this.target == '_blank') && (href && href.indexOf('javascript:') != 0)) {
                    event.preventDefault();
                    if (href && href !== '#') {
                        var address = new Address(href);
                        self.navigateToAddress(address);
                    }
                }
            });
            NavigationService.navigateToAddress(NavigationService.start);
            //Nach dem der HTML-Client gestartet wurde, werden alle historyState geleert, sodass BACK nur noch zu Home führen kann
            NavigationService.historyStateStack = [];
        };
        /**
         * Liefert den Identifier der sichtbaren ViewFactory
         */
        NavigationService.getVisibleViewIdentifier = function () {
            if (NavigationService.visibleFactory) {
                return NavigationService.visibleFactory.identifier;
            }
            return null;
        };
        return NavigationService;
    }());
    return NavigationService;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTmF2aWdhdGlvblNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJOYXZpZ2F0aW9uU2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxxREFBcUQ7OztJQVlyRDs7T0FFRztJQUNIO1FBb0RJO1lBQ0ksaUJBQWlCLENBQUMsS0FBSyxHQUFHLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRCxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNqRSxpQkFBaUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQzlCLGlCQUFpQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDL0IsaUJBQWlCLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzNELGlCQUFpQixDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDekMsaUJBQWlCLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUN6QyxpQkFBaUIsQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1lBQ3ZDLGlCQUFpQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDckMsaUJBQWlCLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUN4QyxpQkFBaUIsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDNUMsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQzVDLGlCQUFpQixDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7WUFDbkMsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQ3pDLGlCQUFpQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDekMsQ0FBQztRQUVEOzs7V0FHRztRQUNZLGlDQUFlLEdBQTlCLFVBQStCLElBQUk7WUFDL0IsSUFBSSxXQUFXLEdBQUcsaUJBQWlCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXhELG1GQUFtRjtZQUNuRixJQUFJLFdBQVcsSUFBSSxJQUFJLElBQUksV0FBVyxDQUFDLFVBQVUsRUFBRTtnQkFDL0MsT0FBTyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzdDLFdBQVcsR0FBRyxJQUFJLENBQUM7YUFDdEI7WUFFRCxJQUFJLFdBQVcsSUFBSSxJQUFJLEVBQUU7Z0JBQ3JCLElBQUksV0FBVyxLQUFLLGlCQUFpQixDQUFDLGNBQWMsRUFBRTtvQkFDbEQsT0FBTyxXQUFXLENBQUM7aUJBQ3RCO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7OztXQUdHO1FBQ1kscUNBQW1CLEdBQWxDLFVBQW1DLElBQUk7WUFDbkMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBRXpCLEtBQUssSUFBSSxFQUFFLElBQUksaUJBQWlCLENBQUMsZUFBZSxFQUFFO2dCQUM5QyxJQUFJLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO29CQUNyRCxhQUFhLEdBQUcsaUJBQWlCLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUN0RCxNQUFNO2lCQUNUO2FBQ0o7WUFFRCxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN2RCxDQUFDO1FBRUQ7OztXQUdHO1FBQ1ksdUNBQXFCLEdBQXBDLFVBQXFDLE9BQU87WUFDeEMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUM7UUFDNUQsQ0FBQztRQUVEOzs7V0FHRztRQUNZLHFDQUFtQixHQUFsQyxVQUFtQyxPQUFPLEVBQUUsT0FBTztZQUMvQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksVUFBVSxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksWUFBWSxFQUFFO2dCQUM1RCxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEdBQUcsT0FBTyxDQUFDO2FBQ3REO2lCQUFNO2dCQUNILGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDO2FBQzNEO1FBQ0wsQ0FBQztRQUVEOzs7V0FHRztRQUNZLHdDQUFzQixHQUFyQyxVQUFzQyxPQUFPO1lBQ3pDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUM7UUFDL0MsQ0FBQztRQUVEOztXQUVHO1FBQ1ksa0NBQWdCLEdBQS9CLFVBQWdDLElBQUksRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxPQUFPO1lBQ2hGLElBQUksWUFBWSxLQUFLLEtBQUssSUFBSSxVQUFVLEtBQUssSUFBSSxFQUFFO2dCQUMvQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDM0Q7WUFFRCxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNyRCxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwRyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUV0RCxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0IsQ0FBQztRQUVEOztXQUVHO1FBQ1ksZ0NBQWMsR0FBN0I7WUFDSSxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFFYyxzQ0FBb0IsR0FBbkM7WUFDSSxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO2dCQUN4RSxJQUFJLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksSUFBSSxFQUFFO29CQUN0RixPQUFPLElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQztpQkFDdkU7Z0JBRUQsWUFBWSxHQUFHLGNBQWMsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUM7YUFDbEU7aUJBQ0k7Z0JBQ0QsWUFBWSxHQUFHLHNCQUFzQixDQUFDO2FBQ3pDO1lBRUQsT0FBTyxJQUFJLE9BQU8sQ0FBQyx5QkFBeUIsR0FBRyxZQUFZLENBQUMsQ0FBQztRQUNqRSxDQUFDO1FBRUQ7O1dBRUc7UUFDWSxpQ0FBZSxHQUE5QjtZQUNJLElBQUksS0FBSyxDQUFDLGFBQWEsRUFBRTtnQkFDckIsSUFBSSxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3pELElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2xELElBQUksR0FBRyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUVoRixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDVixPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3pFO2FBQ0o7WUFFRCxPQUFPLElBQUksT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzNCLENBQUM7UUFFRDs7V0FFRztRQUNZLDRCQUFVLEdBQXpCO1lBQ0ksSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFFdkMsSUFBSSxLQUFLLENBQUMsYUFBYSxFQUFFO2dCQUNyQixJQUFJLFdBQVcsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDekQsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDbEQsSUFBSSxHQUFHLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRWhGLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUNWLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUNyRDthQUNKO1lBRUQsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUNoRCxPQUFPLElBQUksR0FBRyxDQUFDO2FBQ2xCO1lBRUQsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQztRQUVhLGtDQUFnQixHQUE5QjtZQUNJLE9BQU8saUJBQWlCLENBQUMsaUJBQWlCLENBQUM7UUFDL0MsQ0FBQztRQUVEOzs7V0FHRztRQUNXLDRCQUFVLEdBQXhCO1lBQ0ksT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFFLENBQUM7UUFFRDs7V0FFRztRQUNXLDZDQUEyQixHQUF6QyxVQUEwQyxhQUFhLEVBQUUsS0FBSztZQUMxRCxJQUFJLElBQUksR0FBRyxpQkFBaUIsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ2xELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDOUMsSUFBSSxLQUFLLEdBQUcsaUJBQWlCLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEUsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkUsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQ7OztXQUdHO1FBQ1csb0NBQWtCLEdBQWhDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBUSxDQUFDO1lBQy9ELE9BQU8saUJBQWlCLENBQUMsNEJBQTRCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZFLENBQUM7UUFFYyw4Q0FBNEIsR0FBM0MsVUFBNEMsS0FBSztZQUM3QyxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hDLE9BQU8sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUM7UUFFYyw4Q0FBNEIsR0FBM0MsVUFBNEMsT0FBTztZQUMvQyxPQUFPO2dCQUNILE9BQU8sRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNuQixJQUFJLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRTtnQkFDdkIsVUFBVSxFQUFFLE9BQU8sQ0FBQyxhQUFhLEVBQUU7Z0JBQ25DLFdBQVcsRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFO2FBQ3hDLENBQUE7UUFDTCxDQUFDO1FBRWMsb0JBQUUsR0FBakIsVUFBa0IsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHO1lBQy9CLElBQUksaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDbkYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNoRDtRQUNMLENBQUM7UUFFYyw0QkFBVSxHQUF6QixVQUEwQixLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUc7WUFDdkMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBUSxDQUFDO1lBRTlELElBQUksSUFBSTttQkFDRCxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7bUJBQ2xELEtBQUssQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO21CQUM3QixJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUs7Z0JBQ3RCLE9BQU8sS0FBSyxDQUFDO1lBRWpCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYyxrQ0FBZ0IsR0FBL0I7WUFDSSxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFRLENBQUM7WUFFbEUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEUsQ0FBQztRQUdEOztXQUVHO1FBQ1ksc0JBQUksR0FBbkI7WUFDSSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUMxQyxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFRLENBQUM7WUFDbEUsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDL0Q7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyw2QkFBVyxHQUF6QixVQUEwQixPQUFPO1lBQzdCLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFbEUsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUNuQixPQUFPLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN2RDtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRDs7V0FFRztRQUNXLGlDQUFlLEdBQTdCLFVBQThCLElBQUksRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxZQUFhO1lBQzFFLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVkLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQywyQkFBMkIsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBRTVFLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztZQUV2QixJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ3JCLElBQUksSUFBSSxJQUFJLFVBQVUsSUFBSSxJQUFJLElBQUksWUFBWSxFQUFFO29CQUM1QyxXQUFXLEdBQUcsaUJBQWlCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2lCQUN2RTtxQkFBTTtvQkFDSCxXQUFXLEdBQUcsaUJBQWlCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN6RDthQUNKO1lBRUQsSUFBSSxXQUFXLElBQUksSUFBSSxFQUFFO2dCQUNyQiw2R0FBNkc7Z0JBQzdHLDZIQUE2SDtnQkFDN0gsd0RBQXdEO2dCQUN4RCxJQUFJLFdBQVcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxLQUFLLEVBQUU7b0JBQ2pDLHNEQUFzRDtvQkFDdEQsSUFBSSxXQUFXLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRTt3QkFDaEQsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO3FCQUN6QjtvQkFDRCxXQUFXLEdBQUcsSUFBSSxDQUFDO2lCQUN0QjthQUNKO1lBRUQsSUFBSSxXQUFXLElBQUksSUFBSSxFQUFFO2dCQUNyQixXQUFXLEdBQUcsaUJBQWlCLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNoRjtZQUVELElBQUksV0FBVyxJQUFJLElBQUksRUFBRTtnQkFDckIsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixFQUFFLENBQUM7Z0JBRWxELElBQUksUUFBUSxFQUFFO29CQUNWLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNqRCxpQkFBaUIsQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7b0JBRS9DLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7aUJBQzlEO3FCQUNJO29CQUNELGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxPQUFPLENBQUMsQ0FBQztpQkFDNUY7Z0JBRUQsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUVkLE9BQU8sV0FBVyxDQUFDO2FBQ3RCO2lCQUNJO2dCQUNELE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFFZCxNQUFNLGVBQWUsR0FBRyxJQUFJLEdBQUcsdUJBQXVCLENBQUM7YUFDMUQ7UUFDTCxDQUFDO1FBWUQ7O1dBRUc7UUFDVyxtQ0FBaUIsR0FBL0IsVUFBZ0MsV0FBVztZQUV2QyxJQUFJLFdBQVcsSUFBSSxJQUFJLEVBQUU7Z0JBQ3JCLE9BQU87YUFDVjtZQUVELE9BQU8saUJBQWlCLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN6RCxPQUFPLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekQsSUFBSSxXQUFXLElBQUksaUJBQWlCLENBQUMsY0FBYyxFQUFFO2dCQUNqRCxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzNDO1lBRUQsaUJBQWlCLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0QsQ0FBQztRQUVhLHVDQUFxQixHQUFuQztZQUNJLE9BQU8saUJBQWlCLENBQUMsYUFBYTtpQkFDakMsTUFBTSxDQUFDLFVBQUMsRUFBRTtnQkFDUCxJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUU7b0JBQ2YsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUVELE9BQU8sRUFBRSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTTtvQkFDbEQsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxLQUFLLFNBQVM7b0JBQ3pDLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxJQUFJLENBQUMsQ0FBQTtZQUN4RixDQUFDLENBQUM7aUJBQ0QsTUFBTSxDQUFDO1FBQ2hCLENBQUM7UUFFRDs7V0FFRztRQUNXLHlDQUF1QixHQUFyQyxVQUFzQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFNBQVM7WUFDekQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBRXZCLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDL0MsSUFBSSxXQUFXLEdBQUcsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBUSxDQUFDO1lBRTNELElBQUksV0FBVyxJQUFJLElBQUksRUFBRTtnQkFDckIsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUV2QyxJQUFJLFNBQVMsRUFBRTtvQkFDWCxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQzFCO2dCQUVELGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDckQ7WUFFRCxPQUFPLFdBQVcsQ0FBQztRQUN2QixDQUFDO1FBR0Q7O1dBRUc7UUFDVywyQ0FBeUIsR0FBdkMsVUFBd0MsV0FBVztZQUMvQyxJQUFJLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO2dCQUNsQyxJQUFJLFdBQVcsQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO29CQUNuRyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ3hDO2dCQUNELFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFFdEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsVUFBQyxFQUFPO29CQUN6RCxPQUFPLEVBQUUsQ0FBQyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsaUJBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBRzdDLFdBQVcsR0FBRyxJQUFJLENBQUM7YUFDdEI7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDWSw0QkFBVSxHQUF6QixVQUEwQixJQUFJO1lBQzFCLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV0QixPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0QixLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXZCLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3JCLENBQUM7UUFFRDs7V0FFRztRQUNXLDZCQUFXLEdBQXpCLFVBQTBCLElBQUk7WUFDMUIsU0FBUyxDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFFeEUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWhCLENBQUMsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUM5QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUVkLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRTNDLElBQUksV0FBVyxJQUFJLElBQUksSUFBSSxXQUFXLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtvQkFDakQsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDM0IsV0FBVyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7aUJBQzNCO2dCQUVELElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsRUFBRTtvQkFDeEIsTUFBTSxDQUFDLFVBQVUsQ0FBQzt3QkFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxQixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7aUJBQ1Y7cUJBQ0k7b0JBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFRDs7V0FFRztRQUNXLDhCQUFZLEdBQTFCLFVBQTJCLFdBQVc7WUFDbEMsaUJBQWlCLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQztZQUNoRCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLENBQUM7UUFFYSxtQ0FBaUIsR0FBL0IsVUFBZ0MsV0FBVztZQUN2QyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQ3hDLGlCQUFpQixDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUM7WUFDaEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyx5QkFBTyxHQUFyQjtZQUNJLElBQUksT0FBTyxHQUFHLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzdDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFYSxvQ0FBa0IsR0FBaEMsVUFBaUMsaUJBQWlCO1lBQzlDLElBQUksaUJBQWlCLENBQUMsVUFBVSxJQUFJLGlCQUFpQixDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ25FLE9BQU87YUFDVjtZQUNELElBQUksT0FBTyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQztZQUN4QyxJQUFJLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxTQUFTLENBQUM7WUFDNUMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLENBQUMsd0NBQXdDO1lBQ2pFLElBQUksVUFBVSxHQUFHLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztZQUU5QyxpQkFBaUIsQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQztZQUN6RSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNqRixpQkFBaUIsQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDaEQsQ0FBQztRQUdEOztXQUVHO1FBQ1csbUNBQWlCLEdBQS9CLFVBQWdDLE9BQU8sRUFBRSxTQUFVLEVBQUUsV0FBcUIsRUFBRSxVQUFvQixFQUFFLGVBQXlCO1lBQ3ZILElBQUksZUFBZSxFQUFFO2dCQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztnQkFDaEUsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQztZQUVuQixJQUFJLFdBQVcsSUFBSSxJQUFJLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtnQkFDN0MsT0FBTyxHQUFHLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQywwQkFBMEIsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFFLGlCQUFpQixDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQzthQUNuTTtZQUVELElBQUksT0FBTyxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRTtnQkFDMUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUVoRSxJQUFJLEtBQUssR0FBRyxpQkFBaUIsQ0FBQyw0QkFBNEIsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxLQUFLLEdBQUcsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNyQyxJQUFJLEdBQUcsR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUc3RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3RDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUMzQztxQkFDSTtvQkFDRCxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDNUI7Z0JBRUQsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFeEIsaUJBQWlCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxPQUFPLENBQUMsYUFBYSxFQUFFLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUVuRyxPQUFPLElBQUksQ0FBQzthQUNmO2lCQUNJO2dCQUNELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1FBQ0wsQ0FBQztRQUVhLHVDQUFxQixHQUFuQztZQUNJLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQzdELENBQUM7UUFFRDs7V0FFRztRQUNXLDhDQUE0QixHQUExQztZQUNJLElBQUksaUJBQWlCLENBQUMsa0JBQWtCLElBQUksSUFBSSxFQUFFO2dCQUM5QyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ2hHO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csK0JBQWEsR0FBM0IsVUFBNEIsR0FBRyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxlQUF1QjtZQUF2QixnQ0FBQSxFQUFBLHVCQUF1QjtZQUNyRyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO2dCQUN2QixHQUFHLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMxQjtZQUVELElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QyxPQUFPO2FBQ1Y7WUFFRCxJQUFJLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDcEM7WUFFRCxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDdEcsQ0FBQztRQUVEOztXQUVHO1FBQ1csbUNBQWlCLEdBQS9CLFVBQWdDLEdBQUc7WUFDL0IsSUFBSSxZQUFZLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDakQsWUFBWSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNqRSxJQUFJLE1BQU0sR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXJDLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNsQixPQUFPO1lBRVgsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV2QixJQUFJLGNBQWMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7UUFDN0IsQ0FBQztRQUVEOztXQUVHO1FBQ1csaUNBQWUsR0FBN0I7WUFDSSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqRSxDQUFDO1FBRWEsMENBQXdCLEdBQXRDO1lBQ0ksSUFBSSxXQUFXLEdBQUcsaUJBQWlCLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNwRyxPQUFPLEVBQUU7b0JBQ0wsaUJBQWlCLENBQUMseUJBQXlCLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3pELFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLENBQUM7YUFDSixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2IsQ0FBQztRQUVEOztXQUVHO1FBQ1csa0NBQWdCLEdBQTlCO1lBQ0ksaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEUsQ0FBQztRQUVEOztXQUVHO1FBQ1csaUNBQWUsR0FBN0I7WUFDSSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqRSxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxnQ0FBYyxHQUE1QixVQUE2QixlQUFnQztZQUFoQyxnQ0FBQSxFQUFBLHVCQUFnQztZQUN6RCxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDbkcsQ0FBQztRQUVEOztXQUVHO1FBQ1csd0JBQU0sR0FBcEI7WUFDSSxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUM7UUFDckQsQ0FBQztRQUVEOzs7V0FHRztRQUNXLHNCQUFJLEdBQWxCO1lBQ0ksaUJBQWlCLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzVELGlCQUFpQixDQUFDLEtBQUssR0FBRyxpQkFBaUIsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUU5RCxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRTtnQkFDeEYsaUJBQWlCLENBQUMsS0FBSyxHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQzthQUNwRDtZQUVELGlCQUFpQixDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxJQUFJLGlCQUFpQixDQUFDLElBQUksSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEVBQUU7Z0JBQ25ELGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQzthQUMvRTtZQUVELGlIQUFpSDtZQUNqSCxtRkFBbUY7WUFDbkYsTUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFDLEtBQUs7Z0JBQ3RCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDckIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFFN0IsSUFBSSxjQUFjLEdBQUcsVUFBVSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLGNBQWMsRUFBRTtvQkFDaEIsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO29CQUN6QyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDbkMsT0FBTztpQkFDVjtnQkFFRCxJQUFJLG9CQUFvQixHQUFHLFVBQVUsR0FBRyxHQUFHLENBQUM7Z0JBRzVDLG9DQUFvQztnQkFDcEMsc0RBQXNEO2dCQUN0RCxJQUFJLG9CQUFvQixJQUFJLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3hFLElBQUksUUFBUSxHQUFHLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxVQUFVLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQkFDbEM7Z0JBRUQsSUFBSSxPQUFPLEdBQUcsaUJBQWlCLENBQUMsNEJBQTRCLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRXpFLElBQUksb0JBQW9CLEVBQUU7b0JBQ3RCLElBQUksbUJBQW1CLEdBQUcsaUJBQWlCLENBQUMsZUFBZSxDQUFDO29CQUM1RCxpQkFBaUIsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO29CQUV6QyxpR0FBaUc7b0JBQ2pHLElBQUksV0FBVyxHQUFHO3dCQUNkLElBQUksaUJBQWlCLENBQUMsY0FBYyxJQUFJLElBQUksRUFBRTs0QkFDMUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUM7eUJBQ3pFO3dCQUVELElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFOzRCQUNuQyxtQkFBbUIsRUFBRSxDQUFDO3lCQUN6QjtvQkFDTCxDQUFDLENBQUM7b0JBRUYsT0FBTyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDcEM7cUJBQ0k7b0JBQ0Qsc0JBQXNCO29CQUN0QixJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFRLENBQUM7b0JBQ25FLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdEUsT0FBTztpQkFDVjtnQkFFRCxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsaUJBQWlCLENBQUMsTUFBTSxDQUFRLENBQUMsRUFBRTtvQkFDL0UsT0FBTyxHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQztpQkFDcEM7Z0JBR0QsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsb0JBQW9CLENBQUM7Z0JBQzNELHNGQUFzRjtnQkFDdEYsOEZBQThGO2dCQUM5RiwrRkFBK0Y7Z0JBQy9GLElBQUksaUJBQWlCLENBQUMsY0FBYyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLEVBQUU7b0JBQ3pFLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7b0JBQ3pDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDdEUsSUFBSSxDQUFDLFlBQVk7d0JBQ2IsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDNUM7cUJBQ0k7b0JBQ0QsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDeEM7Z0JBRUQsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQ2hELENBQUMsQ0FBQztZQUVGLE1BQU0sQ0FBQyxjQUFjLEdBQUc7Z0JBQ3BCLElBQUksV0FBVyxHQUFHLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNqRCxzREFBc0Q7Z0JBQ3RELElBQUksV0FBVyxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQVEsQ0FBQyxFQUFFO29CQUNqRixtQkFBbUIsQ0FBQyxVQUFrQixDQUFDO3dCQUNwQyxNQUFNLEVBQUUsS0FBSztxQkFDaEIsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQyxDQUFDO1lBR0YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEtBQUs7Z0JBQ3RDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFcEIsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFFLElBQTBCLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7b0JBRS9ILEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFFdkIsSUFBSSxJQUFJLElBQUksSUFBSSxLQUFLLEdBQUcsRUFBRTt3QkFDdEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2hDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDbkM7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUdILGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdELHNIQUFzSDtZQUN0SCxpQkFBaUIsQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDN0MsQ0FBQztRQTVhRDs7V0FFRztRQUNXLDBDQUF3QixHQUFHO1lBQ3JDLElBQUksaUJBQWlCLENBQUMsY0FBYyxFQUFFO2dCQUNsQyxPQUFPLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7YUFDdEQ7WUFDRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUE7UUFxYUwsd0JBQUM7S0FBQSxBQW55QkQsSUFteUJDO0lBRUQsT0FBUyxpQkFBaUIsQ0FBQyJ9