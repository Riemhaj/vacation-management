///<amd-module name="Management/ComponentManager" />
define("Management/ComponentManager", ["require", "exports"], function (require, exports) {
    "use strict";
    /**
     * Der ComponentManager läd alle externen Komponenten vor dem Start der Anwendung und
     * verwaltet Mappings von Aliases auf die jeweilige Komponente
     */
    var ComponentManager = /** @class */ (function () {
        function ComponentManager() {
            var _this = this;
            this.extensionParameterAreaIdx = 0;
            this.priorizedExtensions = (function () {
                var priorization = [];
                if (MODEL.Extension) {
                    var extension = MODEL.Extension.split(',');
                    for (var i = extension.length - 1; i >= 0; i--) {
                        priorization.splice(0, 0, extension[i]);
                    }
                    _this.extensionParameterAreaIdx = priorization.length;
                }
                return priorization;
            })();
            this.components = { models: {}, controls: {}, factories: {}, messages: {}, summaryViews: {}, uiListener: {}, uiInterceptors: {}, viewModels: {}, views: {}, utilities: {} };
        }
        ComponentManager.getInstance = function () {
            if (ComponentManager.instance == null)
                ComponentManager.instance = new ComponentManager();
            return ComponentManager.instance;
        };
        /**
         * Priorisiert eine Erweiterung über einer anderen
         */
        ComponentManager.addPriorization = function (name) {
            var manager = ComponentManager.getInstance();
            if (!(name in manager.priorizedExtensions)) {
                manager.priorizedExtensions.splice(manager.extensionParameterAreaIdx, 0, name);
            }
        };
        /**
         * Eine interne Funktion zur Registierung einer neuen Komponente
         * Alle Pfade zu einem Alias werden in einem Array gespeichert. Alle Implementierungen sind daher immer erreichbar.
         */
        ComponentManager.setComponent = function (object, alias, path) {
            if (object == null) {
                throw 'Parameter Error: object is null';
            }
            if (alias == null) {
                throw 'Parameter Error: alias is null';
            }
            if (path == null) {
                throw 'Parameter Error: path is null';
            }
            if (object[alias] == null) {
                object[alias] = [];
            }
            object[alias].push(path);
        };
        /**
         * Eine interne Funktion zum Auslesen einer Komponente zu einem Alias
         * Falls mehrere Komponenten unter einem Alias registriert sind, wird die zuletzt registrierte zurück geliefert.
         */
        ComponentManager.getComponent = function (object, alias) {
            if (object == null) {
                throw 'Parameter Error: object is null';
            }
            if (alias == null) {
                throw 'Parameter Error: alias is null';
            }
            if (!(alias in object)) {
                for (var prop in object) {
                    var lowerCaseProp = prop.toLowerCase();
                    var lowerCaseAlias = alias.toLowerCase();
                    if (lowerCaseProp === lowerCaseAlias || lowerCaseProp + 'factory' === lowerCaseAlias) {
                        alias = prop;
                        break;
                    }
                }
            }
            if (object[alias] == null) {
                console.log('ComponentManager: ' + alias + ' could not be found.');
                return null;
            }
            var index = null;
            if (object[alias].length > 1) {
                var manager = ComponentManager.getInstance();
                for (var j = 0; j < manager.priorizedExtensions.length; j++) {
                    if (index == null) {
                        for (var i = 0; i < object[alias].length; i++) {
                            if (object[alias][i].indexOf(manager.priorizedExtensions[j]) === 0) {
                                index = i;
                            }
                        }
                    }
                }
                if (index == null) {
                    index = object[alias].length - 1;
                }
            }
            else {
                index = 0;
            }
            return require(object[alias][index]);
        };
        /*
         * Eine interne Funktion zum Auslesen aller Komponenten eines Typs
         */
        ComponentManager.getComponents = function (object) {
            if (object == null) {
                throw 'Parameter Error: object is null';
            }
            var components = [];
            var manager = ComponentManager.getInstance();
            for (var alias in object) {
                var index = null;
                for (var j = 0; j < manager.priorizedExtensions.length; j++) {
                    if (index == null) {
                        for (var i = 0; i < object[alias].length; i++) {
                            if (object[alias][i].indexOf(manager.priorizedExtensions[j]) === 0) {
                                index = i;
                            }
                        }
                    }
                }
                if (index == null) {
                    index = object[alias].length - 1;
                }
                var component = require(object[alias][index]);
                components.push(component);
            }
            return components;
        };
        /*
         * Ruft über eine Server-Methode alle verfügbaren Komponenten ab
         * Es muss sichergestellt sein, dass als erstes die DefaultViewExtensions geladen werden damit diese bei Bedarf durch andere Plugins überschrieben werden können
         */
        ComponentManager.loadComponents = function (callback) {
            $.ajax({
                url: MODEL.HTMLClientBaseURL + 'HTMLClient/Extension/All',
                type: 'GET',
                cache: false,
                async: true,
                dataType: 'json',
                success: function (orderedExtensions) {
                    var length = orderedExtensions.length;
                    var paths = [];
                    var components = [];
                    var namespaces = [];
                    //
                    // Im alten Code wurde pro Extension einmal requireComponents aufgerufen.
                    //
                    // Bug:
                    //      Haben sich die Komponenten der Extensions aufeinander bezogen,
                    //      so wurden die Inter-Extension-Abhängigkeiten durch mehrere requireComponents-Aufrufe ggf. nicht korrekt aufgelöst.
                    //      Beobachtet wurde das Verhalten allerdings bisher nur, wenn Bundling mit mind. zwei Extensions aktiv war:
                    //       - RADPresentation.HTMLClient.DefaultViewExtensions
                    //       - Extension X
                    //      Extension X war klein und hat einige Klassen aus den DefaultViewExtension erweitert.
                    //
                    // Erklärungsansatz für das Phänomen:
                    //      - Wenn Bundling inaktiv ist, stauen sich die vielen Requests, sodass wenn die Komponenten aus der späteren Extension (X) geladen werden,
                    //        die Requests der früheren DefaultViewExtensions schon fertig sind und die Abhängigkeiten somit bereitstehen. -> Kein Fehler
                    //      - Wenn Bundling inaktiv ist, können alle (im Bsp. beide) Requests sofort losgeschickt werden und werden typischerweise
                    //        früher beantwortet, wenn die zu übertragende Datenmenge nur gering ist. Require.js bemerkt dann für die spätere/kleinere Extension (X)
                    //        die fehlenden Abhängigkeiten und versucht diese selbst nochmal zu laden, obwohl sie ja später durch die DefaultViewExtensions
                    //        abgedeckt würden (was require an der Stelle nicht wissen kann).
                    //
                    // BugFix:
                    //      Es werden nun alle Daten gesammelt und dann zusammen an require.js übergeben.
                    //      Dann nämlich wartet require.js ab, bis ALLE Extensions geladen sind und fängt erst dann mit dem Auflösen der Abhängigkeiten an.
                    //
                    for (var i = 0; i < length; i++) {
                        (function (extensions) {
                            for (var namespace in extensions) {
                                ComponentManager.addPriorization(namespace);
                                namespaces.push(namespace); // Wird nur benutzt, wenn Bundling aktiv ist
                                var _loop_1 = function (i_1) {
                                    var extension = extensions[namespace][i_1];
                                    var isFileTypeJS = extension.split('.').pop() === 'js';
                                    var isPlugin = extension.indexOf('Plugins') > -1;
                                    if (isFileTypeJS === true && isPlugin === false) {
                                        var component = namespace + '/' + extension.replace(namespace + '.', '').replace('.js', '').replace(/\./g, '/');
                                        var ns = (function () {
                                            var split = extension.split('.');
                                            split.splice(split.length - 2, 2);
                                            return split.join('.');
                                        })();
                                        var file = (function () {
                                            var split = extension.split('.');
                                            split.splice(0, split.length - 2);
                                            return split.join('.');
                                        })();
                                        var path = MODEL.HTMLClientBaseURL + 'HTMLClient/Extension/' + ns + '/' + file; // Wird nur benutzt, wenn Bundling inaktiv ist
                                        components.push(component);
                                        paths.push(path);
                                    }
                                };
                                for (var i_1 = 0; i_1 < extensions[namespace].length; i_1++) {
                                    _loop_1(i_1);
                                }
                            }
                        })(orderedExtensions[i]);
                    }
                    ComponentManager.requireComponents(components, paths, namespaces, callback);
                }
            });
        };
        ComponentManager.requireComponents = function (components, paths, namespaces, callback) {
            if (components.length > 0) {
                var requiredModules = namespaces.map(function (namespace) {
                    return MODEL.HTMLClientBaseURL + 'HTMLClient/Extension/Module/' + namespace + '.js';
                });
                if (MODEL.HTMLClientBundling === false) {
                    requiredModules = paths;
                }
                require(requiredModules, function () {
                    require(components, function () {
                        for (var j = 0; j < arguments.length; j++) {
                            var Component = arguments[j];
                            if (Component != null && Component.register != null) {
                                Component.register(components[j]);
                            }
                        }
                        callback();
                    });
                });
            }
            else {
                callback();
            }
        };
        /**
         * Registriert eine Komponente vom Typ "Model"
         */
        ComponentManager.setModel = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.models, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "Control"
         */
        ComponentManager.setControl = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.controls, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "Factory"
         */
        ComponentManager.setFactory = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.factories, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "Message"
         */
        ComponentManager.setMessage = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.messages, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "SummaryView"
         */
        ComponentManager.setSummaryView = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.summaryViews, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "UIListener"
         */
        ComponentManager.setUIListener = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.uiListener, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "UIInterceptor"
         */
        ComponentManager.setUIInterceptor = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.uiInterceptors, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "ViewModel"
         */
        ComponentManager.setViewModel = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.viewModels, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "View"
         */
        ComponentManager.setView = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.views, alias, path);
        };
        /**
         * Registriert eine Komponente vom Typ "Utility"
         */
        ComponentManager.setUtility = function (alias, path) {
            ComponentManager.setComponent(ComponentManager.getInstance().components.utilities, alias, path);
        };
        /**
         * Liefert die zum Alias registrierte "Model"-Komponente
         */
        ComponentManager.getModel = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.models, alias);
        };
        /**
         * Liefert die zum Alias registrierte "Control"-Komponente
         */
        ComponentManager.getControl = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.controls, alias);
        };
        /**
         * Liefert die zum Alias registrierte "Factory"-Komponente
         */
        ComponentManager.getFactory = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.factories, alias);
        };
        /**
         * Liefert alle registrierten "Factory"-Komponenten
         */
        ComponentManager.getFactories = function () {
            return ComponentManager.getInstance().components.factories;
        };
        /**
         * Liefert die zum Alias registrierte "Message"-Komponente
         */
        ComponentManager.getMessage = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.messages, alias);
        };
        /**
         * Liefert die zum Alias registrierte "SummaryView"-Komponente
         */
        ComponentManager.getSummaryView = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.summaryViews, alias);
        };
        /**
         * Liefert die zum Alias registrierte "UIListener"-Komponente
         */
        ComponentManager.getUIListener = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.uiListener, alias);
        };
        /**
         * Liefert die zum Alias registrierte "UIInterceptor"-Komponente
         */
        ComponentManager.getUIInterceptor = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.uiInterceptors, alias);
        };
        /**
         * Liefert alle "UIInterceptor"-Komponenten
         */
        ComponentManager.getUIInterceptors = function () {
            return ComponentManager.getComponents(ComponentManager.getInstance().components.uiInterceptors);
        };
        /**
         * Liefert die zum Alias registrierte "ViewModel"-Komponente
         */
        ComponentManager.getViewModel = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.viewModels, alias);
        };
        /**
         * Liefert die zum Alias registrierte "ViewModel"-Komponente
         */
        ComponentManager.getViewModelByInterface = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.viewModels, alias);
        };
        /**
         * Liefert die zum Alias registrierte "View"-Komponente
         */
        ComponentManager.getView = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.views, alias);
        };
        /**
         * Liefert die zum Alias registrierte "Utility"-Komponente
         */
        ComponentManager.getUtility = function (alias) {
            return ComponentManager.getComponent(ComponentManager.getInstance().components.utilities, alias);
        };
        /**
         * Initialisiert den ComponentManager und macht ihn global über das Window-Objekt verfügbar
         * Komponenten aus Plugins werden geladen und registriert
         */
        ComponentManager.init = function (callback) {
            ComponentManager.loadComponents(callback);
        };
        return ComponentManager;
    }());
    return ComponentManager;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29tcG9uZW50TWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbXBvbmVudE1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0RBQW9EOzs7SUFJcEQ7OztPQUdHO0lBQ0g7UUFrQkk7WUFBQSxpQkFvQkM7WUFuQkcsSUFBSSxDQUFDLHlCQUF5QixHQUFHLENBQUMsQ0FBQztZQUVuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQztnQkFDeEIsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO2dCQUV0QixJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7b0JBQ2pCLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUUzQyxLQUFLLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQzVDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDM0M7b0JBRUQsS0FBSSxDQUFDLHlCQUF5QixHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7aUJBQ3hEO2dCQUVELE9BQU8sWUFBWSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFFTCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUUsY0FBYyxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQ2hMLENBQUM7UUFFYyw0QkFBVyxHQUExQjtZQUNJLElBQUksZ0JBQWdCLENBQUMsUUFBUSxJQUFJLElBQUk7Z0JBQ2pDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7WUFFdkQsT0FBTyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7UUFDckMsQ0FBQztRQUVEOztXQUVHO1FBQ1ksZ0NBQWUsR0FBOUIsVUFBK0IsSUFBSTtZQUMvQixJQUFJLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUU3QyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7Z0JBQ3hDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNsRjtRQUNMLENBQUM7UUFFRDs7O1dBR0c7UUFDWSw2QkFBWSxHQUEzQixVQUE0QixNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUk7WUFDM0MsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO2dCQUNoQixNQUFNLGlDQUFpQyxDQUFDO2FBQzNDO1lBRUQsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUNmLE1BQU0sZ0NBQWdDLENBQUM7YUFDMUM7WUFFRCxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ2QsTUFBTSwrQkFBK0IsQ0FBQzthQUN6QztZQUVELElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDdkIsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUN0QjtZQUVELE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQztRQUVEOzs7V0FHRztRQUNZLDZCQUFZLEdBQTNCLFVBQTRCLE1BQU0sRUFBRSxLQUFLO1lBQ3JDLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsTUFBTSxpQ0FBaUMsQ0FBQzthQUMzQztZQUVELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDZixNQUFNLGdDQUFnQyxDQUFDO2FBQzFDO1lBRUQsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLE1BQU0sQ0FBQyxFQUFFO2dCQUNwQixLQUFLLElBQUksSUFBSSxJQUFJLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN2QyxJQUFJLGNBQWMsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBRXpDLElBQUksYUFBYSxLQUFLLGNBQWMsSUFBSSxhQUFhLEdBQUcsU0FBUyxLQUFLLGNBQWMsRUFBRTt3QkFDbEYsS0FBSyxHQUFHLElBQUksQ0FBQzt3QkFDYixNQUFNO3FCQUNUO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxHQUFHLHNCQUFzQixDQUFDLENBQUE7Z0JBQ2xFLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7WUFFakIsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBRTdDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUN6RCxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7d0JBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQzNDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0NBQ2hFLEtBQUssR0FBRyxDQUFDLENBQUM7NkJBQ2I7eUJBQ0o7cUJBQ0o7aUJBQ0o7Z0JBRUQsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO29CQUNmLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztpQkFDcEM7YUFDSjtpQkFBTTtnQkFDSCxLQUFLLEdBQUcsQ0FBQyxDQUFDO2FBQ2I7WUFFRCxPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN6QyxDQUFDO1FBRUQ7O1dBRUc7UUFDWSw4QkFBYSxHQUE1QixVQUE2QixNQUFNO1lBQy9CLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsTUFBTSxpQ0FBaUMsQ0FBQzthQUMzQztZQUVELElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVwQixJQUFJLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUU3QyxLQUFLLElBQUksS0FBSyxJQUFJLE1BQU0sRUFBRTtnQkFDdEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUVqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDekQsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO3dCQUNmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUMzQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUNoRSxLQUFLLEdBQUcsQ0FBQyxDQUFDOzZCQUNiO3lCQUNKO3FCQUNKO2lCQUNKO2dCQUVELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDZixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7aUJBQ3BDO2dCQUVELElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDOUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM5QjtZQUVELE9BQU8sVUFBVSxDQUFDO1FBQ3RCLENBQUM7UUFFRDs7O1dBR0c7UUFDWSwrQkFBYyxHQUE3QixVQUE4QixRQUFRO1lBQ2xDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ0gsR0FBRyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsR0FBRywwQkFBMEI7Z0JBQ3pELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssRUFBRSxLQUFLO2dCQUNaLEtBQUssRUFBRSxJQUFJO2dCQUNYLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixPQUFPLEVBQUUsVUFBQyxpQkFBaUI7b0JBQ3ZCLElBQUksTUFBTSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztvQkFFdEMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNmLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDO29CQUVwQixFQUFFO29CQUNGLHlFQUF5RTtvQkFDekUsRUFBRTtvQkFDRixPQUFPO29CQUNQLHNFQUFzRTtvQkFDdEUsMEhBQTBIO29CQUMxSCxnSEFBZ0g7b0JBQ2hILDJEQUEyRDtvQkFDM0Qsc0JBQXNCO29CQUN0Qiw0RkFBNEY7b0JBQzVGLEVBQUU7b0JBQ0YscUNBQXFDO29CQUNyQyxnSkFBZ0o7b0JBQ2hKLHFJQUFxSTtvQkFDckksOEhBQThIO29CQUM5SCxnSkFBZ0o7b0JBQ2hKLHVJQUF1STtvQkFDdkkseUVBQXlFO29CQUN6RSxFQUFFO29CQUNGLFVBQVU7b0JBQ1YscUZBQXFGO29CQUNyRix1SUFBdUk7b0JBQ3ZJLEVBQUU7b0JBRUYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDN0IsQ0FBQyxVQUFDLFVBQVU7NEJBQ1IsS0FBSyxJQUFJLFNBQVMsSUFBSSxVQUFVLEVBQUU7Z0NBQzlCLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQ0FDNUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLDRDQUE0Qzt3REFFL0QsR0FBQztvQ0FDTixJQUFJLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBQyxDQUFDLENBQUM7b0NBQ3pDLElBQUksWUFBWSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEtBQUssSUFBSSxDQUFDO29DQUN2RCxJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29DQUVqRCxJQUFJLFlBQVksS0FBSyxJQUFJLElBQUksUUFBUSxLQUFLLEtBQUssRUFBRTt3Q0FDN0MsSUFBSSxTQUFTLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO3dDQUNoSCxJQUFJLEVBQUUsR0FBRyxDQUFDOzRDQUNOLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7NENBQ2pDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7NENBRWxDLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3Q0FDM0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3Q0FDTCxJQUFJLElBQUksR0FBRyxDQUFDOzRDQUNSLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7NENBQ2pDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7NENBRWxDLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3Q0FDM0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3Q0FFTCxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsdUJBQXVCLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyw4Q0FBOEM7d0NBRTlILFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0NBQzNCLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUNBQ3BCO2dDQUNMLENBQUM7Z0NBekJELEtBQUssSUFBSSxHQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxFQUFFLEdBQUMsRUFBRTs0Q0FBNUMsR0FBQztpQ0F5QlQ7NkJBRUo7d0JBQ0wsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDNUI7b0JBRUQsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBRWhGLENBQUM7YUFDSixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWMsa0NBQWlCLEdBQWhDLFVBQWlDLFVBQVUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLFFBQVE7WUFDcEUsSUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkIsSUFBSSxlQUFlLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFNBQVM7b0JBQzNDLE9BQU8sS0FBSyxDQUFDLGlCQUFpQixHQUFHLDhCQUE4QixHQUFHLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3hGLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksS0FBSyxDQUFDLGtCQUFrQixLQUFLLEtBQUssRUFBRTtvQkFDcEMsZUFBZSxHQUFHLEtBQUssQ0FBQztpQkFDM0I7Z0JBRUQsT0FBTyxDQUFDLGVBQWUsRUFBRTtvQkFDckIsT0FBTyxDQUFDLFVBQVUsRUFBRTt3QkFDaEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3ZDLElBQUksU0FBUyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFFN0IsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO2dDQUNqRCxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNyQzt5QkFDSjt3QkFFRCxRQUFRLEVBQUUsQ0FBQztvQkFDZixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFFBQVEsRUFBRSxDQUFDO2FBQ2Q7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx5QkFBUSxHQUF0QixVQUF1QixLQUFLLEVBQUUsSUFBSTtZQUM5QixnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakcsQ0FBQztRQUVEOztXQUVHO1FBQ1csMkJBQVUsR0FBeEIsVUFBeUIsS0FBSyxFQUFFLElBQUk7WUFDaEMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ25HLENBQUM7UUFFRDs7V0FFRztRQUNXLDJCQUFVLEdBQXhCLFVBQXlCLEtBQUssRUFBRSxJQUFJO1lBQ2hDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNwRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVywyQkFBVSxHQUF4QixVQUF5QixLQUFLLEVBQUUsSUFBSTtZQUNoQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkcsQ0FBQztRQUVEOztXQUVHO1FBQ1csK0JBQWMsR0FBNUIsVUFBNkIsS0FBSyxFQUFFLElBQUk7WUFDcEMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZHLENBQUM7UUFFRDs7V0FFRztRQUNXLDhCQUFhLEdBQTNCLFVBQTRCLEtBQUssRUFBRSxJQUFJO1lBQ25DLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyxpQ0FBZ0IsR0FBOUIsVUFBK0IsS0FBSyxFQUFFLElBQUk7WUFDdEMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3pHLENBQUM7UUFFRDs7V0FFRztRQUNXLDZCQUFZLEdBQTFCLFVBQTJCLEtBQUssRUFBRSxJQUFJO1lBQ2xDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx3QkFBTyxHQUFyQixVQUFzQixLQUFLLEVBQUUsSUFBSTtZQUM3QixnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDaEcsQ0FBQztRQUVEOztXQUVHO1FBQ1csMkJBQVUsR0FBeEIsVUFBeUIsS0FBSyxFQUFFLElBQUk7WUFDaEMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BHLENBQUM7UUFFRDs7V0FFRztRQUNXLHlCQUFRLEdBQXRCLFVBQXVCLEtBQUs7WUFDeEIsT0FBTyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNsRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVywyQkFBVSxHQUF4QixVQUF5QixLQUFLO1lBQzFCLE9BQU8sZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDcEcsQ0FBQztRQUVEOztXQUVHO1FBQ1csMkJBQVUsR0FBeEIsVUFBeUIsS0FBSztZQUMxQixPQUFPLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3JHLENBQUM7UUFFRDs7V0FFRztRQUNXLDZCQUFZLEdBQTFCO1lBQ0ksT0FBTyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQy9ELENBQUM7UUFFRDs7V0FFRztRQUNXLDJCQUFVLEdBQXhCLFVBQXlCLEtBQUs7WUFDMUIsT0FBTyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNwRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVywrQkFBYyxHQUE1QixVQUE2QixLQUFLO1lBQzlCLE9BQU8sZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDeEcsQ0FBQztRQUVEOztXQUVHO1FBQ1csOEJBQWEsR0FBM0IsVUFBNEIsS0FBSztZQUM3QixPQUFPLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3RHLENBQUM7UUFFRDs7V0FFRztRQUNXLGlDQUFnQixHQUE5QixVQUErQixLQUFLO1lBQ2hDLE9BQU8sZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUcsQ0FBQztRQUVEOztXQUVHO1FBQ1csa0NBQWlCLEdBQS9CO1lBQ0ksT0FBTyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BHLENBQUM7UUFFRDs7V0FFRztRQUNXLDZCQUFZLEdBQTFCLFVBQTJCLEtBQUs7WUFDNUIsT0FBTyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0RyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyx3Q0FBdUIsR0FBckMsVUFBeUMsS0FBYTtZQUNsRCxPQUFPLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3RHLENBQUM7UUFFRDs7V0FFRztRQUNXLHdCQUFPLEdBQXJCLFVBQXNCLEtBQUs7WUFDdkIsT0FBTyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNqRyxDQUFDO1FBRUQ7O1dBRUc7UUFDVywyQkFBVSxHQUF4QixVQUF5QixLQUFLO1lBQzFCLE9BQU8sZ0JBQWdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckcsQ0FBQztRQUVEOzs7V0FHRztRQUNXLHFCQUFJLEdBQWxCLFVBQW1CLFFBQVE7WUFDdkIsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFDTCx1QkFBQztJQUFELENBQUMsQUF0Y0QsSUFzY0M7SUFFRCxPQUFTLGdCQUFnQixDQUFDIn0=