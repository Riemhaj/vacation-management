///<amd-module name="Management/ObjectPool" />
define("Management/ObjectPool", ["require", "exports", "Utility/TypeInfo"], function (require, exports, TypeInfo) {
    "use strict";
    var ObjectPool;
    (function (ObjectPool) {
        var pool = {};
        function get(Type) {
            var instance = getInstance(Type);
            return function () {
                instance.constructor.apply(instance, arguments);
                return instance;
            };
        }
        ObjectPool.get = get;
        function getInstance(Type) {
            var type = TypeInfo.getType(Type);
            return readManagedInstance(type) || createManagedInstance(Type, type);
        }
        function createManagedInstance(Type, type) {
            var instance = instantiate(Type, type);
            addToPool(instance);
            return instance;
        }
        function readManagedInstance(type) {
            if (type in pool) {
                var poolOfType = pool[type];
                for (var i = 0; i < poolOfType.length; i++) {
                    var instance = poolOfType[i];
                    if (instance.isDisposed === true) {
                        return instance;
                    }
                }
            }
        }
        function instantiate(Type, type) {
            var instance = construct(Type);
            instance.type = type;
            return instance;
        }
        function construct(Type) {
            var EmptyFunction = function () { };
            EmptyFunction.prototype = Type.prototype;
            return new EmptyFunction();
        }
        function addToPool(instance) {
            if (!(instance.type in pool)) {
                pool[instance.type] = [];
            }
            pool[instance.type].push(instance);
        }
    })(ObjectPool || (ObjectPool = {}));
    return ObjectPool;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT2JqZWN0UG9vbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIk9iamVjdFBvb2wudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsOENBQThDOzs7SUFJOUMsSUFBVSxVQUFVLENBNERuQjtJQTVERCxXQUFVLFVBQVU7UUFDaEIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBRWQsYUFBb0IsSUFBSTtZQUNwQixJQUFJLFFBQVEsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFakMsT0FBTztnQkFDSCxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQ2hELE9BQU8sUUFBUSxDQUFDO1lBQ3BCLENBQUMsQ0FBQztRQUNOLENBQUM7UUFQZSxjQUFHLE1BT2xCLENBQUE7UUFFRCxxQkFBcUIsSUFBSTtZQUNyQixJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xDLE9BQU8sbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUkscUJBQXFCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFFLENBQUM7UUFFRCwrQkFBK0IsSUFBSSxFQUFFLElBQUk7WUFDckMsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN2QyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFcEIsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQztRQUVELDZCQUE2QixJQUFJO1lBQzdCLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDZCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRTVCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUN4QyxJQUFJLFFBQVEsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRTdCLElBQUksUUFBUSxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7d0JBQzlCLE9BQU8sUUFBUSxDQUFDO3FCQUNuQjtpQkFDSjthQUNKO1FBQ0wsQ0FBQztRQUVELHFCQUFxQixJQUFJLEVBQUUsSUFBSTtZQUMzQixJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFFckIsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQztRQUVELG1CQUFtQixJQUFJO1lBQ25CLElBQUksYUFBYSxHQUFHLGNBQVEsQ0FBQyxDQUFDO1lBQzlCLGFBQWEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUV6QyxPQUFPLElBQUksYUFBYSxFQUFFLENBQUM7UUFDL0IsQ0FBQztRQUVELG1CQUFtQixRQUFRO1lBQ3ZCLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQzVCO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkMsQ0FBQztJQUVMLENBQUMsRUE1RFMsVUFBVSxLQUFWLFVBQVUsUUE0RG5CO0lBRUQsT0FBUyxVQUFVLENBQUMifQ==