///<amd-module name="Management/KeepAliveHandler" />
define("Management/KeepAliveHandler", ["require", "exports", "Communication/RADEnvironmentProxy", "Management/RADApplication", "Management/ComponentManager", "Management/NavigationService", "Utility/DateHelper", "Utility/StorageHelper"], function (require, exports, RADEnvironmentProxy, RADApplication, ComponentManager, NavigationService, DateHelper, StorageHelper) {
    "use strict";
    var KeepAliveHandler = /** @class */ (function () {
        function KeepAliveHandler() {
            KeepAliveHandler.FORCE_LOGOUT_NOTIFICATION_INTERVAL = 1000 * 60 * 5;
            KeepAliveHandler.INITIAL_KEEP_ALIVE = 1000 * 5;
            KeepAliveHandler.KEEP_ALIVE_INTERVAL = 1000 * 60 * 2;
            KeepAliveHandler.IDLE_TIMESPAN = 1000 * 60 * 5;
            KeepAliveHandler.MAX_IDLE_TIME = 1000 * 60 * 60;
            KeepAliveHandler.SESSION_VALID_TIME = 1000 * 60 * 20;
            KeepAliveHandler.now = function () { return moment.utc(); };
            KeepAliveHandler.keepAliveTimer = null;
            KeepAliveHandler.autoLogoutTimer = null;
            KeepAliveHandler.forceLogoutTimer = null;
            KeepAliveHandler.clientServerDuration = null;
            KeepAliveHandler.idleDetected = false;
            KeepAliveHandler.systemLockDetected = false;
            KeepAliveHandler.clientActionSubscription = null;
            KeepAliveHandler.scheduledLogoutTime = null;
            KeepAliveHandler.systemLockStartTime = null;
            KeepAliveHandler.lastForceLogoutNotification = null;
            KeepAliveHandler.lastClientAction = null;
            KeepAliveHandler.lastKeepAliveUtcSendTime = null;
            KeepAliveHandler.lastKeepAliveUtcReceiveTime = null;
            KeepAliveHandler.logoutNotificationCreated = false;
            KeepAliveHandler.forceLogoutNotificationCreated = false;
            KeepAliveHandler.userNotificationCreated = false;
        }
        // ================================== KeepAliveInterval ==================================
        KeepAliveHandler.startKeepAliveInterval = function () {
            var _this = this;
            KeepAliveHandler.keepAliveTimer = window.setTimeout(function () {
                KeepAliveHandler.keepAliveTimerTick();
                KeepAliveHandler.keepAliveTimer = window.setInterval(KeepAliveHandler.keepAliveTimerTick.bind(_this), KeepAliveHandler.KEEP_ALIVE_INTERVAL);
            }, KeepAliveHandler.INITIAL_KEEP_ALIVE);
        };
        KeepAliveHandler.stopKeepAliveInterval = function () {
            window.clearInterval(KeepAliveHandler.keepAliveTimer);
        };
        KeepAliveHandler.keepAliveTimerTick = function () {
            var difference = KeepAliveHandler.lastKeepAliveUtcReceiveTime.diff(KeepAliveHandler.lastKeepAliveUtcSendTime, 'seconds') * 1000;
            if (difference <= KeepAliveHandler.KEEP_ALIVE_INTERVAL) {
                KeepAliveHandler.lastKeepAliveUtcSendTime = KeepAliveHandler.now();
                var clientTime = KeepAliveHandler.lastKeepAliveUtcSendTime.format();
                RADEnvironmentProxy.keepAlive({
                    clientTime: clientTime
                }).then(function (keepAliveDTO) {
                    var utcNow = moment.utc(keepAliveDTO.utcNow);
                    var difference = utcNow.diff(KeepAliveHandler.lastKeepAliveUtcSendTime);
                    KeepAliveHandler.clientServerDuration = moment.duration(difference);
                    KeepAliveHandler.lastKeepAliveUtcReceiveTime = KeepAliveHandler.now();
                    KeepAliveHandler.updateCache(keepAliveDTO);
                    KeepAliveHandler.idleDetection();
                    KeepAliveHandler.notificationHandling(keepAliveDTO.notifications);
                }).fail(function (status, message) {
                    KeepAliveHandler.showSessionTimeoutNotification();
                });
            }
            else {
                KeepAliveHandler.showSessionTimeoutNotification();
            }
        };
        KeepAliveHandler.idleDetection = function () {
            var idleTime = KeepAliveHandler.now().diff(KeepAliveHandler.getLastClientAction());
            if (idleTime > KeepAliveHandler.MAX_IDLE_TIME) {
                if (KeepAliveHandler.idleDetected === false && KeepAliveHandler.forceLogoutTimer == null) {
                    KeepAliveHandler.startAutoLogoutTimer();
                }
            }
            else {
                KeepAliveHandler.stopAutoLogoutTimer();
            }
        };
        KeepAliveHandler.updateCache = function (keepAliveDTO) {
            var lastReceivedCacheUpdate = RADApplication.lastCacheUpdate();
            var lastReceivedRightCacheUpdate = RADApplication.lastRightCacheUpdate();
            if (lastReceivedCacheUpdate < keepAliveDTO.lastCacheUpdate
                || lastReceivedRightCacheUpdate < keepAliveDTO.lastRightCacheUpdate) {
                RADApplication.updateCache();
                RADApplication.lastCacheUpdate(keepAliveDTO.lastCacheUpdate);
                RADApplication.lastRightCacheUpdate(keepAliveDTO.lastRightCacheUpdate);
            }
        };
        KeepAliveHandler.notificationHandling = function (notifications) {
            var hasSystemLockNotification = false;
            for (var i in notifications) {
                var notification = notifications[i];
                if (notification._type === 'systemLockNotificationDTO') {
                    hasSystemLockNotification = true;
                    if (KeepAliveHandler.systemLockDetected === false) {
                        KeepAliveHandler.startForceLogoutTimer(notification);
                    }
                }
                else {
                    KeepAliveHandler.showUserNotification(notification);
                }
            }
            if (hasSystemLockNotification === false && KeepAliveHandler.forceLogoutTimer != null) {
                KeepAliveHandler.lastForceLogoutNotification = moment.utc(0);
                KeepAliveHandler.stopForceLogoutTimer();
            }
        };
        // ================================== AutoLogoutTimer ==================================
        KeepAliveHandler.startAutoLogoutTimer = function () {
            KeepAliveHandler.idleDetected = true;
            var scheduledLogoutTime = KeepAliveHandler.now().add(KeepAliveHandler.IDLE_TIMESPAN);
            KeepAliveHandler.autoLogoutTimer = window.setInterval(function () {
                var difference = scheduledLogoutTime.diff(KeepAliveHandler.now());
                var remaining = moment.duration(difference);
                KeepAliveHandler.showLogoutNotification(remaining);
                if (remaining.asMilliseconds() <= 0) {
                    KeepAliveHandler.stopAutoLogoutTimer();
                    KeepAliveHandler.forceLogout();
                }
            }, 1000);
        };
        KeepAliveHandler.stopAutoLogoutTimer = function () {
            KeepAliveHandler.idleDetected = false;
            KeepAliveHandler.hideLogoutNotification();
            window.clearTimeout(KeepAliveHandler.autoLogoutTimer);
            KeepAliveHandler.autoLogoutTimer = null;
        };
        // ================================== ListenClientAction ==================================
        KeepAliveHandler.checkKeepaliveActivity = function () {
            var difference = KeepAliveHandler.now().diff(KeepAliveHandler.lastKeepAliveUtcReceiveTime, 'seconds') * 1000;
            if (difference > KeepAliveHandler.SESSION_VALID_TIME) {
                KeepAliveHandler.showSessionTimeoutNotification();
            }
        };
        KeepAliveHandler.startListenClientAction = function () {
            KeepAliveHandler.clientActionSubscription = ko.computed(function () {
                var clientAction = RADEnvironmentProxy.clientAction();
                KeepAliveHandler.setLastClientAction(KeepAliveHandler.now());
                KeepAliveHandler.checkKeepaliveActivity();
            });
        };
        KeepAliveHandler.stopListenClientAction = function () {
            if (KeepAliveHandler.clientActionSubscription != null) {
                KeepAliveHandler.clientActionSubscription.dispose();
                KeepAliveHandler.clientActionSubscription = null;
            }
        };
        // ================================== ForceLogoutTimer ==================================
        KeepAliveHandler.startForceLogoutTimer = function (systemLockNotificationDTO) {
            if (KeepAliveHandler.forceLogoutTimer != null) {
                KeepAliveHandler.stopForceLogoutTimer();
                return;
            }
            if (systemLockNotificationDTO.isUserOnExceptionList === true) {
                KeepAliveHandler.stopForceLogoutTimer();
                return;
            }
            KeepAliveHandler.systemLockDetected = true;
            KeepAliveHandler.systemLockStartTime = moment.utc(systemLockNotificationDTO.lockStart);
            KeepAliveHandler.stopAutoLogoutTimer();
            KeepAliveHandler.forceLogoutTimer = window.setInterval(function () {
                var difference = KeepAliveHandler.systemLockStartTime.diff(KeepAliveHandler.now());
                var remaining = moment.duration(difference);
                if (systemLockNotificationDTO.preNotification === true) {
                    KeepAliveHandler.showForceLogoutNotification(remaining, systemLockNotificationDTO);
                }
                if (remaining.asMilliseconds() <= 0) {
                    KeepAliveHandler.stopForceLogoutTimer();
                    KeepAliveHandler.forceLogout();
                }
            }, 1000);
        };
        KeepAliveHandler.stopForceLogoutTimer = function () {
            KeepAliveHandler.systemLockDetected = false;
            KeepAliveHandler.hideForceLogoutNotification();
            window.clearInterval(KeepAliveHandler.forceLogoutTimer);
            KeepAliveHandler.forceLogoutTimer = null;
        };
        // ================================== SessionTimeoutNotification ==================================
        KeepAliveHandler.showSessionTimeoutNotification = function () {
            RADApplication.unauthorizedRequestCallback();
        };
        // ================================== LogoutNotification ==================================
        KeepAliveHandler.createLogoutNotification = function () {
            if (KeepAliveHandler.logoutNotificationCreated === false) {
                var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
                KeepAliveHandler.logoutNotificationViewModel = MessageBoxViewModel.createLogoutNotificationMessageBox(function () {
                    KeepAliveHandler.setLastClientAction(KeepAliveHandler.now());
                    KeepAliveHandler.hideForceLogoutNotification();
                    KeepAliveHandler.stopAutoLogoutTimer();
                });
                RADApplication.showMessageBox(KeepAliveHandler.logoutNotificationViewModel);
                KeepAliveHandler.logoutNotificationCreated = true;
            }
        };
        KeepAliveHandler.showLogoutNotification = function (remaining) {
            KeepAliveHandler.createLogoutNotification();
            var headlinePostfix = ' (' + KeepAliveHandler.pad(remaining.minutes(), '0', 2) + ':' + KeepAliveHandler.pad(remaining.seconds(), '0', 2) + ')';
            var textrep = {
                minuten: remaining.minutes() > 0 ? remaining.minutes() : 1
            };
            if (KeepAliveHandler.logoutNotificationViewModel.isDisposed === true) {
                KeepAliveHandler.logoutNotificationCreated = false;
                return;
            }
            KeepAliveHandler.logoutNotificationViewModel.window.headline.postfix.caption(headlinePostfix);
            KeepAliveHandler.logoutNotificationViewModel.label.replacements = textrep;
            KeepAliveHandler.logoutNotificationViewModel.label.ergName.valueHasMutated();
            if (!KeepAliveHandler.logoutNotificationViewModel.window.isOpen()) {
                KeepAliveHandler.logoutNotificationViewModel.window.isOpen(true);
            }
        };
        KeepAliveHandler.hideLogoutNotification = function () {
            if (KeepAliveHandler.logoutNotificationCreated === true) {
                if (KeepAliveHandler.logoutNotificationViewModel.isDisposed === false) {
                    KeepAliveHandler.logoutNotificationViewModel.window.isOpen(false);
                }
                else {
                    KeepAliveHandler.logoutNotificationCreated = false;
                }
            }
        };
        // ================================== ForceLogoutNotification ==================================
        KeepAliveHandler.createForceLogoutNotification = function () {
            if (KeepAliveHandler.forceLogoutNotificationCreated === false) {
                var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
                KeepAliveHandler.forceLogoutNotificationViewModel = MessageBoxViewModel.createForceLogoutNotificationMessageBox(function () {
                    KeepAliveHandler.forceLogout();
                }, function () {
                    KeepAliveHandler.setLastClientAction(KeepAliveHandler.now());
                    KeepAliveHandler.hideForceLogoutNotification();
                    KeepAliveHandler.stopForceLogoutTimer();
                });
                RADApplication.showMessageBox(KeepAliveHandler.forceLogoutNotificationViewModel);
                KeepAliveHandler.forceLogoutNotificationCreated = true;
            }
        };
        KeepAliveHandler.showForceLogoutNotification = function (remaining, systemLockNotificationDTO) {
            KeepAliveHandler.createForceLogoutNotification();
            var headline = " (" + KeepAliveHandler.pad(remaining.minutes(), '0', 2) + ":" + KeepAliveHandler.pad(remaining.seconds(), '0', 2) + ")";
            var textrep = {
                minuten: remaining.minutes() > 0 ? remaining.minutes() : 1,
                lockTime: DateHelper.toString(moment.utc(systemLockNotificationDTO.lockStart).toDate(), true),
                lockReason: systemLockNotificationDTO.notificationMessage
            };
            if (KeepAliveHandler.forceLogoutNotificationViewModel.isDisposed === true) {
                KeepAliveHandler.forceLogoutNotificationCreated = false;
                return;
            }
            KeepAliveHandler.forceLogoutNotificationViewModel.window.headline.postfix.caption(headline);
            KeepAliveHandler.forceLogoutNotificationViewModel.label.replacements = textrep;
            KeepAliveHandler.forceLogoutNotificationViewModel.label.ergName.valueHasMutated();
            var timeTillForceLogoutNotification = KeepAliveHandler.now().diff(KeepAliveHandler.lastForceLogoutNotification);
            var isOpen = KeepAliveHandler.forceLogoutNotificationViewModel.window.isOpen();
            if (isOpen === false && timeTillForceLogoutNotification > KeepAliveHandler.FORCE_LOGOUT_NOTIFICATION_INTERVAL) {
                KeepAliveHandler.forceLogoutNotificationViewModel.window.isOpen(true);
            }
        };
        KeepAliveHandler.hideForceLogoutNotification = function () {
            if (KeepAliveHandler.forceLogoutNotificationCreated === true) {
                if (KeepAliveHandler.forceLogoutNotificationViewModel.isDisposed === false) {
                    KeepAliveHandler.forceLogoutNotificationViewModel.window.isOpen(false);
                }
                else {
                    KeepAliveHandler.forceLogoutNotificationCreated = false;
                }
            }
            KeepAliveHandler.lastForceLogoutNotification = KeepAliveHandler.now();
        };
        // ================================== UserNotification ==================================
        KeepAliveHandler.createUserNotification = function () {
            if (KeepAliveHandler.userNotificationCreated === false) {
                var MessageBoxViewModel = ComponentManager.getViewModel('MessageBoxViewModel');
                KeepAliveHandler.userNotificationViewModel = MessageBoxViewModel.createNoticeMessageBox(null, null, KeepAliveHandler.hideUserNotification.bind(this));
                KeepAliveHandler.userNotificationViewModel.window.isOpen(false);
                RADApplication.showMessageBox(KeepAliveHandler.userNotificationViewModel);
                KeepAliveHandler.userNotificationCreated = true;
            }
        };
        KeepAliveHandler.showUserNotification = function (notificationDTO) {
            KeepAliveHandler.createUserNotification();
            if (KeepAliveHandler.userNotificationViewModel.isDisposed === true) {
                KeepAliveHandler.userNotificationCreated = false;
                return;
            }
            KeepAliveHandler.userNotificationViewModel.label.caption(notificationDTO.notificationMessage);
            var isOpen = KeepAliveHandler.userNotificationViewModel.window.isOpen();
            if (isOpen === false && KeepAliveHandler.userNotificationViewModel.window.isOpen() === false) {
                KeepAliveHandler.userNotificationViewModel.window.isOpen(true);
            }
        };
        KeepAliveHandler.hideUserNotification = function (notificationDTO) {
            if (KeepAliveHandler.userNotificationCreated === true) {
                if (KeepAliveHandler.userNotificationViewModel.isDisposed === false) {
                    KeepAliveHandler.userNotificationViewModel.window.isOpen(false);
                }
                else {
                    KeepAliveHandler.userNotificationCreated = false;
                }
            }
        };
        // ======================================================================================
        KeepAliveHandler.pad = function (number, char, count) {
            var result = number + '';
            while (result.length < count) {
                result = char + result;
            }
            return result;
        };
        KeepAliveHandler.reset = function () {
            KeepAliveHandler.keepAliveTimer = null;
            KeepAliveHandler.autoLogoutTimer = null;
            KeepAliveHandler.forceLogoutTimer = null;
            KeepAliveHandler.clientServerDuration = null;
            KeepAliveHandler.idleDetected = false;
            KeepAliveHandler.systemLockDetected = false;
            KeepAliveHandler.clientActionSubscription = null;
            KeepAliveHandler.scheduledLogoutTime = KeepAliveHandler.now();
            KeepAliveHandler.systemLockStartTime = KeepAliveHandler.now();
            KeepAliveHandler.lastForceLogoutNotification = moment.utc(0);
            KeepAliveHandler.setLastClientAction(KeepAliveHandler.now());
            KeepAliveHandler.lastKeepAliveUtcSendTime = KeepAliveHandler.now();
            KeepAliveHandler.lastKeepAliveUtcReceiveTime = KeepAliveHandler.now();
            KeepAliveHandler.KEEP_ALIVE_INTERVAL = 1000 * 60 * RADApplication.getSystemParam('Gui.KeepAliveTime', parseInt);
            KeepAliveHandler.IDLE_TIMESPAN = 1000 * 60 * RADApplication.getSystemParam('Gui.IdleWarningTimeout', parseInt);
            KeepAliveHandler.MAX_IDLE_TIME = 1000 * 60 * RADApplication.getSystemParam('Gui.MaxIdleTime', parseInt);
            KeepAliveHandler.SESSION_VALID_TIME = 1000 * 60 * RADApplication.getSystemParam('Gui.SessionValidTime', parseInt);
        };
        KeepAliveHandler.forceLogout = function () {
            RADApplication.isForceLogout(true);
            NavigationService.navigateToLogout();
        };
        KeepAliveHandler.startWatchDog = function () {
            KeepAliveHandler.reset();
            KeepAliveHandler.startListenClientAction();
            KeepAliveHandler.startKeepAliveInterval();
        };
        KeepAliveHandler.stopWatchDog = function () {
            KeepAliveHandler.stopListenClientAction();
            KeepAliveHandler.stopKeepAliveInterval();
        };
        KeepAliveHandler.getLastClientAction = function () {
            if (RADApplication.isSystemParamSetAndTrue('Gui.UseAutoLogin')) {
                return StorageHelper.get('KeepAliveHandler.lastClientAction');
            }
            else
                return KeepAliveHandler.lastClientAction;
        };
        KeepAliveHandler.setLastClientAction = function (newValue) {
            if (RADApplication.isSystemParamSetAndTrue('Gui.UseAutoLogin')) {
                StorageHelper.set('KeepAliveHandler.lastClientAction', KeepAliveHandler.now(), false);
            }
            else
                KeepAliveHandler.lastClientAction = newValue;
        };
        return KeepAliveHandler;
    }());
    return KeepAliveHandler;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiS2VlcEFsaXZlSGFuZGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIktlZXBBbGl2ZUhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0RBQW9EOzs7SUFXcEQ7UUFnSEk7WUFDSSxnQkFBZ0IsQ0FBQyxrQ0FBa0MsR0FBRyxJQUFJLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNwRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQy9DLGdCQUFnQixDQUFDLG1CQUFtQixHQUFHLElBQUksR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3JELGdCQUFnQixDQUFDLGFBQWEsR0FBRyxJQUFJLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUMvQyxnQkFBZ0IsQ0FBQyxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFDaEQsZ0JBQWdCLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFDckQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLGNBQVEsT0FBTyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEQsZ0JBQWdCLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUN2QyxnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQ3hDLGdCQUFnQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7WUFDN0MsZ0JBQWdCLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN0QyxnQkFBZ0IsQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDNUMsZ0JBQWdCLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDO1lBQ2pELGdCQUFnQixDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUM1QyxnQkFBZ0IsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7WUFDNUMsZ0JBQWdCLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDO1lBQ3BELGdCQUFnQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUM7WUFDakQsZ0JBQWdCLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDO1lBRXBELGdCQUFnQixDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztZQUNuRCxnQkFBZ0IsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7WUFDeEQsZ0JBQWdCLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDO1FBQ3JELENBQUM7UUFFRCwwRkFBMEY7UUFFM0UsdUNBQXNCLEdBQXJDO1lBQUEsaUJBS0M7WUFKRyxnQkFBZ0IsQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztnQkFDaEQsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQkFDdEMsZ0JBQWdCLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxFQUFFLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDL0ksQ0FBQyxFQUFFLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUVjLHNDQUFxQixHQUFwQztZQUNJLE1BQU0sQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUQsQ0FBQztRQUVjLG1DQUFrQixHQUFqQztZQUNJLElBQUksVUFBVSxHQUFHLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsRUFBRSxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7WUFFaEksSUFBSSxVQUFVLElBQUksZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3BELGdCQUFnQixDQUFDLHdCQUF3QixHQUFHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUVuRSxJQUFJLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFFcEUsbUJBQW1CLENBQUMsU0FBUyxDQUFDO29CQUMxQixVQUFVLEVBQUUsVUFBVTtpQkFDekIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFlBQVk7b0JBQ2pCLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUM3QyxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDLENBQUM7b0JBRXhFLGdCQUFnQixDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3BFLGdCQUFnQixDQUFDLDJCQUEyQixHQUFHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUV0RSxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQzNDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUNqQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3RFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBRSxPQUFPO29CQUNwQixnQkFBZ0IsQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO2dCQUN0RCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILGdCQUFnQixDQUFDLDhCQUE4QixFQUFFLENBQUM7YUFDckQ7UUFDTCxDQUFDO1FBRWMsOEJBQWEsR0FBNUI7WUFDSSxJQUFJLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDO1lBRW5GLElBQUksUUFBUSxHQUFHLGdCQUFnQixDQUFDLGFBQWEsRUFBRTtnQkFDM0MsSUFBSSxnQkFBZ0IsQ0FBQyxZQUFZLEtBQUssS0FBSyxJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtvQkFDdEYsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztpQkFDM0M7YUFDSjtpQkFBTTtnQkFDSCxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQzFDO1FBQ0wsQ0FBQztRQUVjLDRCQUFXLEdBQTFCLFVBQTJCLFlBQVk7WUFDbkMsSUFBSSx1QkFBdUIsR0FBRyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDL0QsSUFBSSw0QkFBNEIsR0FBRyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUV6RSxJQUFJLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxlQUFlO21CQUNuRCw0QkFBNEIsR0FBRyxZQUFZLENBQUMsb0JBQW9CLEVBQUU7Z0JBRXJFLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDN0IsY0FBYyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzdELGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQzthQUMxRTtRQUNMLENBQUM7UUFFYyxxQ0FBb0IsR0FBbkMsVUFBb0MsYUFBYTtZQUM3QyxJQUFJLHlCQUF5QixHQUFHLEtBQUssQ0FBQztZQUV0QyxLQUFLLElBQUksQ0FBQyxJQUFJLGFBQWEsRUFBRTtnQkFDekIsSUFBSSxZQUFZLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVwQyxJQUFJLFlBQVksQ0FBQyxLQUFLLEtBQUssMkJBQTJCLEVBQUU7b0JBQ3BELHlCQUF5QixHQUFHLElBQUksQ0FBQztvQkFFakMsSUFBSSxnQkFBZ0IsQ0FBQyxrQkFBa0IsS0FBSyxLQUFLLEVBQUU7d0JBQy9DLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUN4RDtpQkFDSjtxQkFBTTtvQkFDSCxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDdkQ7YUFDSjtZQUVELElBQUkseUJBQXlCLEtBQUssS0FBSyxJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDbEYsZ0JBQWdCLENBQUMsMkJBQTJCLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0QsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQzthQUMzQztRQUNMLENBQUM7UUFFRCx3RkFBd0Y7UUFFekUscUNBQW9CLEdBQW5DO1lBQ0ksZ0JBQWdCLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUVyQyxJQUFJLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUVyRixnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDbEQsSUFBSSxVQUFVLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ2xFLElBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRTVDLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUVuRCxJQUFJLFNBQVMsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ2pDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUM7b0JBQ3ZDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUNsQztZQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNiLENBQUM7UUFFYyxvQ0FBbUIsR0FBbEM7WUFDSSxnQkFBZ0IsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBRXRDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDMUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN0RCxnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVDLENBQUM7UUFFRCwyRkFBMkY7UUFFNUUsdUNBQXNCLEdBQXJDO1lBQ0ksSUFBSSxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDJCQUEyQixFQUFFLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQztZQUM3RyxJQUFJLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDbEQsZ0JBQWdCLENBQUMsOEJBQThCLEVBQUUsQ0FBQzthQUNyRDtRQUNMLENBQUM7UUFFYyx3Q0FBdUIsR0FBdEM7WUFDSSxnQkFBZ0IsQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNwRCxJQUFJLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDdEQsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFFN0QsZ0JBQWdCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUM5QyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYyx1Q0FBc0IsR0FBckM7WUFDSSxJQUFJLGdCQUFnQixDQUFDLHdCQUF3QixJQUFJLElBQUksRUFBRTtnQkFDbkQsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3BELGdCQUFnQixDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQzthQUNwRDtRQUNMLENBQUM7UUFFRCx5RkFBeUY7UUFFMUUsc0NBQXFCLEdBQXBDLFVBQXFDLHlCQUF5QjtZQUMxRCxJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDM0MsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1lBRUQsSUFBSSx5QkFBeUIsQ0FBQyxxQkFBcUIsS0FBSyxJQUFJLEVBQUU7Z0JBQzFELGdCQUFnQixDQUFDLG9CQUFvQixFQUFFLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtZQUVELGdCQUFnQixDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMzQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZGLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFFdkMsZ0JBQWdCLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDbkQsSUFBSSxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ25GLElBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRTVDLElBQUkseUJBQXlCLENBQUMsZUFBZSxLQUFLLElBQUksRUFBRTtvQkFDcEQsZ0JBQWdCLENBQUMsMkJBQTJCLENBQUMsU0FBUyxFQUFFLHlCQUF5QixDQUFDLENBQUM7aUJBQ3RGO2dCQUVELElBQUksU0FBUyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRTtvQkFDakMsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztvQkFDeEMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ2xDO1lBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2IsQ0FBQztRQUVjLHFDQUFvQixHQUFuQztZQUNJLGdCQUFnQixDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUU1QyxnQkFBZ0IsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1lBQy9DLE1BQU0sQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUN4RCxnQkFBZ0IsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0MsQ0FBQztRQUVELG1HQUFtRztRQUVwRiwrQ0FBOEIsR0FBN0M7WUFDSSxjQUFjLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztRQUNqRCxDQUFDO1FBRUQsMkZBQTJGO1FBRTVFLHlDQUF3QixHQUF2QztZQUNJLElBQUksZ0JBQWdCLENBQUMseUJBQXlCLEtBQUssS0FBSyxFQUFFO2dCQUN0RCxJQUFJLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBUSxDQUFDO2dCQUV0RixnQkFBZ0IsQ0FBQywyQkFBMkIsR0FBRyxtQkFBbUIsQ0FBQyxrQ0FBa0MsQ0FBQztvQkFDbEcsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztvQkFDN0QsZ0JBQWdCLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztvQkFDL0MsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0MsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsY0FBYyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2dCQUM1RSxnQkFBZ0IsQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7YUFDckQ7UUFDTCxDQUFDO1FBRWMsdUNBQXNCLEdBQXJDLFVBQXNDLFNBQVM7WUFDM0MsZ0JBQWdCLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztZQUU1QyxJQUFJLGVBQWUsR0FBRyxJQUFJLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztZQUUvSSxJQUFJLE9BQU8sR0FBRztnQkFDVixPQUFPLEVBQUUsU0FBUyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzdELENBQUM7WUFFRixJQUFJLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ2xFLGdCQUFnQixDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztnQkFDbkQsT0FBTzthQUNWO1lBRUQsZ0JBQWdCLENBQUMsMkJBQTJCLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzlGLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDO1lBQzFFLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDN0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDL0QsZ0JBQWdCLENBQUMsMkJBQTJCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNwRTtRQUNMLENBQUM7UUFFYyx1Q0FBc0IsR0FBckM7WUFDSSxJQUFJLGdCQUFnQixDQUFDLHlCQUF5QixLQUFLLElBQUksRUFBRTtnQkFDckQsSUFBSSxnQkFBZ0IsQ0FBQywyQkFBMkIsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUNuRSxnQkFBZ0IsQ0FBQywyQkFBMkIsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyRTtxQkFBTTtvQkFDSCxnQkFBZ0IsQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7aUJBQ3REO2FBQ0o7UUFDTCxDQUFDO1FBRUQsZ0dBQWdHO1FBQ2pGLDhDQUE2QixHQUE1QztZQUNJLElBQUksZ0JBQWdCLENBQUMsOEJBQThCLEtBQUssS0FBSyxFQUFFO2dCQUMzRCxJQUFJLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBUSxDQUFDO2dCQUV0RixnQkFBZ0IsQ0FBQyxnQ0FBZ0MsR0FBRyxtQkFBbUIsQ0FBQyx1Q0FBdUMsQ0FBQztvQkFDNUcsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ25DLENBQUMsRUFBRTtvQkFDQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO29CQUM3RCxnQkFBZ0IsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO29CQUMvQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUM1QyxDQUFDLENBQVEsQ0FBQztnQkFFVixjQUFjLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLENBQUM7Z0JBQ2pGLGdCQUFnQixDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQzthQUMxRDtRQUNMLENBQUM7UUFFYyw0Q0FBMkIsR0FBMUMsVUFBMkMsU0FBUyxFQUFFLHlCQUF5QjtZQUMzRSxnQkFBZ0IsQ0FBQyw2QkFBNkIsRUFBRSxDQUFDO1lBRWpELElBQUksUUFBUSxHQUFHLE9BQUssZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLFNBQUksZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLE1BQUcsQ0FBQztZQUU5SCxJQUFJLE9BQU8sR0FBRztnQkFDVixPQUFPLEVBQUUsU0FBUyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRCxRQUFRLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksQ0FBQztnQkFDN0YsVUFBVSxFQUFFLHlCQUF5QixDQUFDLG1CQUFtQjthQUM1RCxDQUFDO1lBR0YsSUFBSSxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO2dCQUN2RSxnQkFBZ0IsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7Z0JBQ3hELE9BQU87YUFDVjtZQUVELGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM1RixnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQztZQUMvRSxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBRWxGLElBQUksK0JBQStCLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDaEgsSUFBSSxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsZ0NBQWdDLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBRS9FLElBQUksTUFBTSxLQUFLLEtBQUssSUFBSSwrQkFBK0IsR0FBRyxnQkFBZ0IsQ0FBQyxrQ0FBa0MsRUFBRTtnQkFDM0csZ0JBQWdCLENBQUMsZ0NBQWdDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN6RTtRQUNMLENBQUM7UUFFYyw0Q0FBMkIsR0FBMUM7WUFDSSxJQUFJLGdCQUFnQixDQUFDLDhCQUE4QixLQUFLLElBQUksRUFBRTtnQkFDMUQsSUFBSSxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUN4RSxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMxRTtxQkFBTTtvQkFDSCxnQkFBZ0IsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7aUJBQzNEO2FBQ0o7WUFFRCxnQkFBZ0IsQ0FBQywyQkFBMkIsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMxRSxDQUFDO1FBRUQseUZBQXlGO1FBRTFFLHVDQUFzQixHQUFyQztZQUNJLElBQUksZ0JBQWdCLENBQUMsdUJBQXVCLEtBQUssS0FBSyxFQUFFO2dCQUNwRCxJQUFJLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBUSxDQUFDO2dCQUV0RixnQkFBZ0IsQ0FBQyx5QkFBeUIsR0FBRyxtQkFBbUIsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN0SixnQkFBZ0IsQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVoRSxjQUFjLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLHlCQUF5QixDQUFDLENBQUM7Z0JBQzFFLGdCQUFnQixDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQzthQUNuRDtRQUNMLENBQUM7UUFFYyxxQ0FBb0IsR0FBbkMsVUFBb0MsZUFBZTtZQUMvQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBRzFDLElBQUksZ0JBQWdCLENBQUMseUJBQXlCLENBQUMsVUFBVSxLQUFLLElBQUksRUFBRTtnQkFDaEUsZ0JBQWdCLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDO2dCQUNqRCxPQUFPO2FBQ1Y7WUFFRCxnQkFBZ0IsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBRTlGLElBQUksTUFBTSxHQUFHLGdCQUFnQixDQUFDLHlCQUF5QixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUV4RSxJQUFJLE1BQU0sS0FBSyxLQUFLLElBQUksZ0JBQWdCLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLEtBQUssRUFBRTtnQkFDMUYsZ0JBQWdCLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNsRTtRQUNMLENBQUM7UUFFYyxxQ0FBb0IsR0FBbkMsVUFBb0MsZUFBZTtZQUMvQyxJQUFJLGdCQUFnQixDQUFDLHVCQUF1QixLQUFLLElBQUksRUFBRTtnQkFDbkQsSUFBSSxnQkFBZ0IsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUNqRSxnQkFBZ0IsQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNuRTtxQkFBTTtvQkFDSCxnQkFBZ0IsQ0FBQyx1QkFBdUIsR0FBRyxLQUFLLENBQUM7aUJBQ3BEO2FBQ0o7UUFDTCxDQUFDO1FBRUQseUZBQXlGO1FBRTFFLG9CQUFHLEdBQWxCLFVBQW1CLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSztZQUNsQyxJQUFJLE1BQU0sR0FBRyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBRXpCLE9BQU8sTUFBTSxDQUFDLE1BQU0sR0FBRyxLQUFLLEVBQUU7Z0JBQzFCLE1BQU0sR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDO2FBQzFCO1lBRUQsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQztRQUVjLHNCQUFLLEdBQXBCO1lBQ0ksZ0JBQWdCLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUN2QyxnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQ3hDLGdCQUFnQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7WUFDN0MsZ0JBQWdCLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN0QyxnQkFBZ0IsQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDNUMsZ0JBQWdCLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDO1lBRWpELGdCQUFnQixDQUFDLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzlELGdCQUFnQixDQUFDLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzlELGdCQUFnQixDQUFDLDJCQUEyQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0QsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUM3RCxnQkFBZ0IsQ0FBQyx3QkFBd0IsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNuRSxnQkFBZ0IsQ0FBQywyQkFBMkIsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUV0RSxnQkFBZ0IsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLEdBQUcsRUFBRSxHQUFHLGNBQWMsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDaEgsZ0JBQWdCLENBQUMsYUFBYSxHQUFHLElBQUksR0FBRyxFQUFFLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUMvRyxnQkFBZ0IsQ0FBQyxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQUUsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3hHLGdCQUFnQixDQUFDLGtCQUFrQixHQUFHLElBQUksR0FBRyxFQUFFLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN0SCxDQUFDO1FBRWMsNEJBQVcsR0FBMUI7WUFDSSxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25DLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDekMsQ0FBQztRQUVhLDhCQUFhLEdBQTNCO1lBQ0ksZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDekIsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztZQUMzQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlDLENBQUM7UUFFYSw2QkFBWSxHQUExQjtZQUNJLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDMUMsZ0JBQWdCLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUM3QyxDQUFDO1FBRWMsb0NBQW1CLEdBQWxDO1lBQ0ksSUFBSSxjQUFjLENBQUMsdUJBQXVCLENBQUMsa0JBQWtCLENBQUMsRUFBRTtnQkFDNUQsT0FBTyxhQUFhLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7YUFDakU7O2dCQUNHLE9BQU8sZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUM7UUFDakQsQ0FBQztRQUVjLG9DQUFtQixHQUFsQyxVQUFtQyxRQUFhO1lBQzVDLElBQUksY0FBYyxDQUFDLHVCQUF1QixDQUFDLGtCQUFrQixDQUFDLEVBQUU7Z0JBQzVELGFBQWEsQ0FBQyxHQUFHLENBQUMsbUNBQW1DLEVBQUUsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDekY7O2dCQUNHLGdCQUFnQixDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztRQUNyRCxDQUFDO1FBQ0wsdUJBQUM7SUFBRCxDQUFDLEFBNWhCRCxJQTRoQkM7SUFFRCxPQUFTLGdCQUFnQixDQUFDIn0=