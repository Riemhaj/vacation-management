/// <amd-module name="BindingContext" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("BindingContext", ["require", "exports", "Class", "BindingContextMixin", "Utility/MixinHelper", "Utility/Utilities"], function (require, exports, Class, BindingContextMixin, MixinHelper, Utilities) {
    "use strict";
    /*
    * Dies ist eine Hilfsklasse für Control, View, Factory etc.
    * Wann immer eine Klasse dazu in der Lage ist Knockout-Bindings ausführen, sollte sie einen BindingContext
    * als Property haben oder hier von erben. Bei jedem ko.applyBindings(...) welches potentiell Controls, Factories etc. initialisiert, muss dieser mit übergeben werden.
    * Alle so erzeugten Controls, Factories werden dann in diesem BindingContext hinterlegt, um beim Disposing des einbettenden
    * Objekts mit-disposed zu werden.
    */
    var BindingContext = /** @class */ (function (_super) {
        __extends(BindingContext, _super);
        function BindingContext() {
            var _this = _super.call(this) || this;
            _this._registeredDisposables = [];
            _this.unregisterEventHandlerCallbacks = [];
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(BindingContext);
            return _this;
        }
        /*
         * Registriert einen Event-Handler und sorgt dafür, dass dieser beim Disposing deregistriert wird
         */
        BindingContext.prototype.addEventHandler = function (eventType, element, callback, selector) {
            if (selector === void 0) { selector = null; }
            var eventNamespace = eventType + '.' + this.identifier;
            if (element instanceof jQuery) {
                element.on(eventNamespace, selector, null, callback);
                this.unregisterEventHandlerCallbacks.push(function () {
                    element.off(eventNamespace);
                });
            }
        };
        BindingContext.prototype.removeEventHandler = function (eventType, element) {
            var eventNamespace = eventType + '.' + this.identifier;
            element.off(eventNamespace);
        };
        BindingContext.prototype.dispose = function () {
            //  Ruft die Funktionen zum Deregistieren aller Event-Handler auf, die von diesem Control erzeugt wurden
            if (!Utilities.isNullOrUndefined(this.unregisterEventHandlerCallbacks))
                this.unregisterEventHandlerCallbacks.forEach(function (unreg) {
                    if ($.isFunction(unreg))
                        unreg();
                });
            this.unregisterEventHandlerCallbacks = null;
            _super.prototype.dispose.call(this);
        };
        return BindingContext;
    }(Class));
    MixinHelper.applyMixins(BindingContext, [BindingContextMixin]);
    return BindingContext;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQmluZGluZ0NvbnRleHQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJCaW5kaW5nQ29udGV4dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx3Q0FBd0M7Ozs7Ozs7Ozs7Ozs7SUFPeEM7Ozs7OztNQU1FO0lBQ0Y7UUFBNkIsa0NBQUs7UUFZOUI7WUFBQSxZQUNJLGlCQUFPLFNBTVY7WUFMRyxLQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1lBQ2pDLEtBQUksQ0FBQywrQkFBK0IsR0FBRyxFQUFFLENBQUM7WUFFMUMscUNBQXFDO1lBQ3JDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsQ0FBQzs7UUFDbEQsQ0FBQztRQUVEOztXQUVHO1FBQ0ksd0NBQWUsR0FBdEIsVUFBdUIsU0FBcUIsRUFBRSxPQUE0QixFQUFFLFFBQWtCLEVBQUUsUUFBdUI7WUFBdkIseUJBQUEsRUFBQSxlQUF1QjtZQUNuSCxJQUFJLGNBQWMsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDdkQsSUFBSSxPQUFPLFlBQVksTUFBTSxFQUFFO2dCQUMxQixPQUFrQixDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxRQUFlLENBQUMsQ0FBQztnQkFFeEUsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQztvQkFDckMsT0FBa0IsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxDQUFBO2FBQ0w7UUFDTCxDQUFDO1FBRU0sMkNBQWtCLEdBQXpCLFVBQTBCLFNBQXFCLEVBQUUsT0FBNEI7WUFDekUsSUFBSSxjQUFjLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVNLGdDQUFPLEdBQWQ7WUFDSSx3R0FBd0c7WUFDeEcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUM7Z0JBQ2xFLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO29CQUMvQyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO3dCQUNuQixLQUFLLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLENBQUM7WUFDUCxJQUFJLENBQUMsK0JBQStCLEdBQUcsSUFBSSxDQUFDO1lBRTVDLGlCQUFNLE9BQU8sV0FBRSxDQUFDO1FBQ3BCLENBQUM7UUFDTCxxQkFBQztJQUFELENBQUMsQUFuREQsQ0FBNkIsS0FBSyxHQW1EakM7SUFFRCxXQUFXLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztJQUUvRCxPQUFTLGNBQWMsQ0FBQyJ9