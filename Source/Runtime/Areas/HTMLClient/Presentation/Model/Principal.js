/// <amd-module name="Model/Principal" />
define("Model/Principal", ["require", "exports"], function (require, exports) {
    "use strict";
    var Principal = /** @class */ (function () {
        function Principal() {
            this.pdoId = ko.observable();
            this.name = ko.observable();
            this.fullName = ko.observable();
            this.logoUrl = ko.observable();
            this.defaultCurrencyId = ko.observable();
        }
        /**
         * Setzt Mandanteninformationen
         */
        Principal.prototype.setPrincipalInfo = function (principalInfo) {
            if (principalInfo == null) {
                principalInfo = { pdoId: 0, name: "", fullName: "WithoutPrincipal", logoUrl: "", defaultCurrencyID: 0 };
            }
            this.pdoId(principalInfo.pdoId);
            this.name(principalInfo.name);
            this.fullName(principalInfo.fullName);
            this.logoUrl(principalInfo.logoUrl);
            this.defaultCurrencyId(principalInfo.defaultCurrencyID);
        };
        return Principal;
    }());
    return Principal;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJpbmNpcGFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUHJpbmNpcGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHlDQUF5Qzs7O0lBRXpDO1FBUUk7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzdDLENBQUM7UUFFRDs7V0FFRztRQUNJLG9DQUFnQixHQUF2QixVQUF3QixhQUFhO1lBQ2pDLElBQUksYUFBYSxJQUFJLElBQUksRUFBRTtnQkFDdkIsYUFBYSxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLGlCQUFpQixFQUFFLENBQUMsRUFBRSxDQUFDO2FBQzNHO1lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzVELENBQUM7UUFDTCxnQkFBQztJQUFELENBQUMsQUE5QkQsSUE4QkM7SUFFRCxPQUFTLFNBQVMsQ0FBQyJ9