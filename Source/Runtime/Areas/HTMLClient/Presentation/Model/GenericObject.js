/// <amd-module name="Model/GenericObject" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Model/GenericObject", ["require", "exports", "Class", "Utility/ChangeHandler", "Communication/RADEnvironmentProxy"], function (require, exports, Class, ChangeHandler, RADEnvironmentProxy) {
    "use strict";
    var GenericObject = /** @class */ (function (_super) {
        __extends(GenericObject, _super);
        function GenericObject(subject, delay) {
            var _this = _super.call(this) || this;
            _this.subject = subject;
            _this.pdoId = _this.paramOrDefault(subject.pdoId, -1);
            _this.pdTypeName = _this.paramOrDefault(subject.pdTypeName, null);
            _this.principalName = _this.paramOrDefault(subject.principalName);
            _this.objectIdentValue = _this.paramOrDefault(subject.objectIdentValue);
            _this.delay = _this.paramOrDefault(delay, 1000);
            _this.values = _this.paramOrDefault(subject.values, {}, function (values) {
                for (var key in values) {
                    values[key] = _this.observableOrDefault(values[key], null);
                }
                ChangeHandler.applyChangeTracking(values);
                return values;
            });
            // Historisierungsdaten
            _this.historizedAssemblyVersion = _this.paramOrDefault(subject.historizedAssemblyVersion, null);
            _this.historizationDate = _this.paramOrDefault(subject.historizationDate, null);
            _this.isHistorized = _this.paramOrDefault(subject.isHistorized, false); // TODO fix /evaluate type in subject
            var _currentHistoryPDO_ID = subject.currentHistoryPDO_ID == undefined ? subject["currentHistoryPDOID"] : subject.currentHistoryPDO_ID;
            _this.currentHistoryPDO_ID = _this.paramOrDefault(_currentHistoryPDO_ID, null);
            _this.isSingleton = _this.paramOrDefault(subject.isSingleton, false);
            _this.isPrincipalIndependent = _this.paramOrDefault(subject.isPrincipalIndependent, false);
            _this.suppressedNotifications = [];
            _this.reevaluateChanges = _this.observableOrDefault(new Date().toString());
            _this.changes = _this.defineComputed(function () {
                var force = _this.reevaluateChanges();
                var values = ko.toJS(_this.values);
                var changes = ChangeHandler.getChangesFromModel(_this.values);
                changes = _.pick(changes, function (value, prop) {
                    return _this.suppressedNotifications.indexOf(prop) === -1;
                });
                return changes;
            });
            _this.delayedChanges = _this.defineComputed(function () {
                return _this.changes();
            }).extend({ waiter: _this.delay });
            _this.isModified = _this.observableOrDefault(subject.isModified, false).extend({ notify: 'always' });
            _this.isForcedToUpdate = false;
            _this.hasPendingModifications = _this.observableOrDefault(subject.hasPendingModifications, false);
            _this.defineSubscription(_this.changes, function (changes) {
                changes = _.keys(changes);
                if (changes.length > 0) {
                    if (!_this.isForcedToUpdate) { //Bei einem Force-Update wird der isModified-Zustand nicht geändert
                        _this.isModified(true);
                    }
                }
            });
            _this.defineSubscription(_this.isModified, function (isModified) {
                _this.hasPendingModifications(isModified);
            });
            _this.isDisposable = _this.paramOrDefault(true);
            _this.applyPrototypeToInstance(GenericObject);
            return _this;
        }
        /**
         * Liefert das Observable zu einem Property im GenericObject
         */
        GenericObject.prototype.getObservable = function (propertyName, defaultValue) {
            if (!(propertyName in this.values)) {
                this.values[propertyName] = ko.observable();
                ChangeHandler.applyChangeTrackingToObservable(this.values[propertyName]);
            }
            var observable = this.values[propertyName];
            if (defaultValue != null && observable.peek() == null) {
                observable(defaultValue);
            }
            return observable;
        };
        /**
         * Bietet die Möglichkeit, das Lesen und Schreiben eines Observables über ein Computed zu definieren
         */
        GenericObject.prototype.getComputed = function (propertyName, params) {
            var _this = this;
            var observable = this.getObservable(propertyName);
            if (params.timeout != null) {
                if (params.hash != null) {
                    var timer_1 = null;
                    var oldHash_1 = null;
                    var pusher_1 = null;
                    var computed = this.defineComputed(function () {
                        var value = observable();
                        var converted = null;
                        ko.ignoreDependencies(function () {
                            if (params.read) {
                                converted = params.read(value);
                            }
                            oldHash_1 = params.hash(converted);
                        });
                        if (pusher_1 != null) {
                            pusher_1.dispose();
                        }
                        pusher_1 = _this.defineComputed(function () {
                            var newHash = params.hash(converted);
                            if (newHash != oldHash_1) {
                                if (timer_1 != null) {
                                    window.clearTimeout(timer_1);
                                }
                                timer_1 = window.setTimeout(function () {
                                    if (params.write) {
                                        params.write(observable, converted);
                                    }
                                }, params.timeout);
                            }
                        });
                        return converted;
                    });
                    return computed;
                }
                else {
                    var value_1 = params.read(observable);
                    var waiter = this.defineComputed(function () {
                        ko.toJS(value_1);
                        return value_1;
                    }).extend({ waiter: params.timeout });
                    this.defineSubscription(waiter, function (value) {
                        if (value != null) {
                            params.write(observable, value);
                        }
                    });
                    return value_1;
                }
            }
            else {
                var computed = this.definePureComputed({
                    read: function () {
                        if (params.read) {
                            return params.read(observable);
                        }
                        return null;
                    },
                    write: function (value) {
                        if (params.write) {
                            params.write(observable, value);
                        }
                    }
                });
                return computed;
            }
        };
        ;
        GenericObject.prototype.hasValue = function (propertyName) {
            if (!(propertyName in this.values)) {
                return false;
            }
            return true;
        };
        /**
         * Liefert den aktuellen Wert eines Properties
         */
        GenericObject.prototype.getValue = function (propertyName) {
            if (!this.hasValue(propertyName)) {
                this.values[propertyName] = ko.observable();
                ChangeHandler.applyChangeTrackingToObservable(this.values[propertyName]);
            }
            return this.values[propertyName]();
        };
        GenericObject.prototype.getValueReturnNullIfNotExists = function (propertyName) {
            if (this.hasValue(propertyName)) {
                return this.values[propertyName]();
            }
            return null;
        };
        /**
         * Verhindert Change-Notifications für ein Property
         */
        GenericObject.prototype.suppressNotificationFor = function (property) {
            var index = this.suppressedNotifications.indexOf(property);
            if (index === -1) {
                this.suppressedNotifications.push(property);
                this.reevaluateChanges(new Date().toString());
            }
        };
        /**
         * Erlaubt Change-Notifications für ein Property
         */
        GenericObject.prototype.deliverNotificationFor = function (property) {
            var index = this.suppressedNotifications.indexOf(property);
            if (index > -1) {
                this.suppressedNotifications.splice(index, 1);
                this.reevaluateChanges(new Date().getTime());
            }
        };
        /**
         * Registriert einen Callback für sämtliche Änderungen im GenericObject
         * Die Subscription wird dabei im Callee registriert um ein automatisches Disposing zu gewährleisten
         */
        GenericObject.prototype.onChange = function (callback, props, callee, deliverSuppressed) {
            var _this = this;
            var _loop_1 = function (prop) {
                if (props == null || props.indexOf(prop) > -1) {
                    callee.defineSubscription(this_1.values[prop], function (value) {
                        if (deliverSuppressed === true || _this.suppressedNotifications.indexOf(prop) === -1) {
                            callback(prop, value);
                        }
                    });
                }
            };
            var this_1 = this;
            for (var prop in this.values) {
                _loop_1(prop);
            }
        };
        /**
         * Registriert einen Callback für verzögerte Änderungen.
         * Die Subscription wird dabei im Callee registriert um ein automatisches Disposing zu gewährleisten
         */
        GenericObject.prototype.onDelayedChange = function (callback, callee) {
            var _this = this;
            callee.defineSubscription(this.delayedChanges, function (changes) {
                changes = _.pick(changes, function (value, prop) {
                    return _this.suppressedNotifications.indexOf(prop) === -1;
                });
                var hasChanges = _.keys(changes).length > 0 || _this.isForcedToUpdate === true;
                if (hasChanges === true) {
                    callback(changes);
                }
            });
        };
        GenericObject.prototype.forceUpdateProperties = function () {
            this.isForcedToUpdate = true;
            this.reevaluateChanges(new Date().toString());
        };
        GenericObject.prototype.refreshProperties = function (properties) {
            var _this = this;
            RADEnvironmentProxy.getProperties({
                pdoID: this.pdoId,
                properties: properties
            }).then(function (resultDTO) {
                if (_this.validateStatus(resultDTO)) {
                    for (var key in resultDTO.returnValue) {
                        if (key in _this.values) {
                            _this.values[key](resultDTO.returnValue[key]);
                        }
                        else {
                            _this.values[key] = _this.observableOrDefault(_this.subject.values[key]);
                            ChangeHandler.applyChangeTrackingToObservable(_this.values[key]);
                        }
                    }
                    _this.commit();
                }
            });
        };
        /**
         * Aktualisiert das GenericObject mit einem SubjectDTO
         */
        GenericObject.prototype.refresh = function (subject) {
            this.pdoId = subject.pdoId;
            this.pdTypeName = subject.pdTypeName;
            this.principalName = subject.principalName;
            this.objectIdentValue = subject.objectIdentValue;
            for (var key in subject.values) {
                var value = subject.values[key];
                if (typeof value == "function")
                    value = value();
                if (key in this.values) {
                    this.values[key](value);
                }
                else {
                    this.values[key] = this.observableOrDefault(value);
                    ChangeHandler.applyChangeTrackingToObservable(this.values[key]);
                }
            }
            this.commit();
        };
        /**
         * Bestätigt die ausstehenden Änderungen und markiert diese als unverändert
         * 1. Kann ohne Parameter aufgerufen werden => Commited alle Werte
         * 2. Kann mit einem String als Parameter aufgerufen werden => Commited nur den Wert mit dem übergebenen Property-Namen
         * 3. Kann mit einem String-Array als Parameter aufgerufen werden => Commited alle Werte der übergebenen Property-Namen
         */
        GenericObject.prototype.commit = function (propertyNames) {
            if (propertyNames != null && !_.isArray(propertyNames)) {
                propertyNames = _.keys(propertyNames);
            }
            ChangeHandler.commitChangesFromModel(this.values, propertyNames);
        };
        /**
         * Setzt ausstehende Änderungen zurück
         * 1. Kann ohne Parameter aufgerufen werden => Reverted alle Werte
         * 2. Kann mit einem String als Parameter aufgerufen werden => Reverted nur den Wert mit dem übergebenen Property-Namen
         * 3. Kann mit einem String-Array als Parameter aufgerufen werden => Reverted alle Werte der übergebenen Property-Namen
         */
        GenericObject.prototype.revert = function (propertyNames) {
            if (!_.isArray(propertyNames)) {
                propertyNames = _.keys(propertyNames);
            }
            ChangeHandler.revertChangesFromModel(this.values, propertyNames);
        };
        return GenericObject;
    }(Class));
    return GenericObject;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR2VuZXJpY09iamVjdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkdlbmVyaWNPYmplY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNkNBQTZDOzs7Ozs7Ozs7Ozs7O0lBTzdDO1FBQTRCLGlDQUFLO1FBeUU3Qix1QkFBb0IsT0FBK0QsRUFBRSxLQUFjO1lBQW5HLFlBQ0ksaUJBQU8sU0FxRVY7WUF0RW1CLGFBQU8sR0FBUCxPQUFPLENBQXdEO1lBRy9FLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEQsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEUsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNoRSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUN0RSxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRTlDLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxVQUFDLE1BQU07Z0JBQ3pELEtBQUssSUFBSSxHQUFHLElBQUksTUFBTSxFQUFFO29CQUNwQixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDN0Q7Z0JBRUQsYUFBYSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUUxQyxPQUFPLE1BQU0sQ0FBQztZQUNsQixDQUFDLENBQUMsQ0FBQztZQUVILHVCQUF1QjtZQUN2QixLQUFJLENBQUMseUJBQXlCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDOUYsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMscUNBQXFDO1lBQzNHLElBQUkscUJBQXFCLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQztZQUN0SSxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUU3RSxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuRSxLQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFFekYsS0FBSSxDQUFDLHVCQUF1QixHQUFHLEVBQUUsQ0FBQztZQUNsQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUV6RSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQy9CLElBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUNyQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxPQUFPLEdBQUcsYUFBYSxDQUFDLG1CQUFtQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFN0QsT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQUMsS0FBSyxFQUFFLElBQUk7b0JBQ2xDLE9BQU8sS0FBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDN0QsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsT0FBTyxPQUFPLENBQUM7WUFDbkIsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQ3RDLE9BQU8sS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUVsQyxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ25HLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDOUIsS0FBSSxDQUFDLHVCQUF1QixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFHaEcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUUsVUFBQyxPQUFPO2dCQUMxQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFFMUIsSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLG1FQUFtRTt3QkFDN0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDekI7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFFLFVBQUMsVUFBVTtnQkFDaEQsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTlDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7UUFDakQsQ0FBQztRQUVEOztXQUVHO1FBQ0kscUNBQWEsR0FBcEIsVUFBcUIsWUFBb0IsRUFBRSxZQUFrQjtZQUN6RCxJQUFJLENBQUMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDNUMsYUFBYSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUM1RTtZQUVELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFM0MsSUFBSSxZQUFZLElBQUksSUFBSSxJQUFJLFVBQVUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxJQUFJLEVBQUU7Z0JBQ25ELFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUM1QjtZQUVELE9BQU8sVUFBVSxDQUFDO1FBQ3RCLENBQUM7UUFFRDs7V0FFRztRQUNJLG1DQUFXLEdBQWxCLFVBQW1CLFlBQW9CLEVBQUUsTUFBVztZQUFwRCxpQkErRUM7WUE5RUcsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUVsRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN4QixJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO29CQUNyQixJQUFJLE9BQUssR0FBRyxJQUFJLENBQUM7b0JBQ2pCLElBQUksU0FBTyxHQUFHLElBQUksQ0FBQztvQkFDbkIsSUFBSSxRQUFNLEdBQUcsSUFBSSxDQUFDO29CQUVsQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO3dCQUMvQixJQUFJLEtBQUssR0FBRyxVQUFVLEVBQUUsQ0FBQzt3QkFDekIsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDO3dCQUVyQixFQUFFLENBQUMsa0JBQWtCLENBQUM7NEJBQ2xCLElBQUksTUFBTSxDQUFDLElBQUksRUFBRTtnQ0FDYixTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDbEM7NEJBRUQsU0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ3JDLENBQUMsQ0FBQyxDQUFDO3dCQUVILElBQUksUUFBTSxJQUFJLElBQUksRUFBRTs0QkFDaEIsUUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO3lCQUNwQjt3QkFFRCxRQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQzs0QkFDekIsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFFckMsSUFBSSxPQUFPLElBQUksU0FBTyxFQUFFO2dDQUNwQixJQUFJLE9BQUssSUFBSSxJQUFJLEVBQUU7b0NBQ2YsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFLLENBQUMsQ0FBQztpQ0FDOUI7Z0NBRUQsT0FBSyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7b0NBQ3RCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTt3Q0FDZCxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztxQ0FDdkM7Z0NBQ0wsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQzs2QkFDdEI7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBRUgsT0FBTyxTQUFTLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxDQUFDO29CQUVILE9BQU8sUUFBUSxDQUFDO2lCQUNuQjtxQkFBTTtvQkFDSCxJQUFJLE9BQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUVwQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO3dCQUM3QixFQUFFLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO3dCQUNmLE9BQU8sT0FBSyxDQUFDO29CQUNqQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7b0JBRXRDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsVUFBQyxLQUFLO3dCQUNsQyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7NEJBQ2YsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ25DO29CQUNMLENBQUMsQ0FBQyxDQUFDO29CQUVILE9BQU8sT0FBSyxDQUFDO2lCQUNoQjthQUNKO2lCQUFNO2dCQUNILElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztvQkFDbkMsSUFBSSxFQUFFO3dCQUNGLElBQUksTUFBTSxDQUFDLElBQUksRUFBRTs0QkFDYixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQ2xDO3dCQUVELE9BQU8sSUFBSSxDQUFDO29CQUNoQixDQUFDO29CQUNELEtBQUssRUFBRSxVQUFDLEtBQUs7d0JBQ1QsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFOzRCQUNkLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNuQztvQkFDTCxDQUFDO2lCQUNKLENBQUMsQ0FBQztnQkFFSCxPQUFPLFFBQVEsQ0FBQzthQUNuQjtRQUNMLENBQUM7UUFBQSxDQUFDO1FBRUssZ0NBQVEsR0FBZixVQUFnQixZQUFZO1lBQ3hCLElBQUksQ0FBQyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2hDLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ0ksZ0NBQVEsR0FBZixVQUFnQixZQUFZO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDNUMsYUFBYSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUM1RTtZQUVELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDO1FBQ3ZDLENBQUM7UUFFTSxxREFBNkIsR0FBcEMsVUFBcUMsWUFBWTtZQUM3QyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDO2FBQ3RDO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ0ksK0NBQXVCLEdBQTlCLFVBQStCLFFBQVE7WUFDbkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUUzRCxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDZCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2FBQ2pEO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ksOENBQXNCLEdBQTdCLFVBQThCLFFBQVE7WUFDbEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUUzRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDWixJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQzthQUNoRDtRQUNMLENBQUM7UUFFRDs7O1dBR0c7UUFDSSxnQ0FBUSxHQUFmLFVBQWdCLFFBQVEsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGlCQUFpQjtZQUExRCxpQkFVQztvQ0FUWSxJQUFJO2dCQUNULElBQUksS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUMzQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsT0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsVUFBQyxLQUFLO3dCQUMvQyxJQUFJLGlCQUFpQixLQUFLLElBQUksSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFOzRCQUNqRixRQUFRLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUN6QjtvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUM7O1lBUkQsS0FBSyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTTt3QkFBbkIsSUFBSTthQVFaO1FBQ0wsQ0FBQztRQUVEOzs7V0FHRztRQUNJLHVDQUFlLEdBQXRCLFVBQXVCLFFBQVEsRUFBRSxNQUFNO1lBQXZDLGlCQVlDO1lBWEcsTUFBTSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsVUFBQyxPQUFPO2dCQUNuRCxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBQyxLQUFLLEVBQUUsSUFBSTtvQkFDbEMsT0FBTyxLQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM3RCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSSxDQUFDLGdCQUFnQixLQUFLLElBQUksQ0FBQztnQkFFOUUsSUFBSSxVQUFVLEtBQUssSUFBSSxFQUFFO29CQUNyQixRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3JCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRU0sNkNBQXFCLEdBQTVCO1lBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELENBQUM7UUFFTSx5Q0FBaUIsR0FBeEIsVUFBeUIsVUFBVTtZQUFuQyxpQkFrQkM7WUFqQkcsbUJBQW1CLENBQUMsYUFBYSxDQUFDO2dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQWU7Z0JBQzNCLFVBQVUsRUFBRSxVQUFVO2FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTO2dCQUNkLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsRUFBRTtvQkFDaEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO3dCQUNuQyxJQUFJLEdBQUcsSUFBSSxLQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt5QkFDaEQ7NkJBQU07NEJBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs0QkFDdEUsYUFBYSxDQUFDLCtCQUErQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt5QkFDbkU7cUJBQ0o7b0JBRUQsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2lCQUNqQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVEOztXQUVHO1FBQ0ksK0JBQU8sR0FBZCxVQUFlLE9BQU87WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztZQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUM7WUFDM0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQztZQUVqRCxLQUFLLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUU7Z0JBQzVCLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksT0FBTyxLQUFLLElBQUksVUFBVTtvQkFDMUIsS0FBSyxHQUFHLEtBQUssRUFBRSxDQUFDO2dCQUVwQixJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMzQjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDbkQsYUFBYSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDbkU7YUFDSjtZQUVELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNsQixDQUFDO1FBRUQ7Ozs7O1dBS0c7UUFDSSw4QkFBTSxHQUFiLFVBQWMsYUFBYztZQUN4QixJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUNwRCxhQUFhLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN6QztZQUVELGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFRDs7Ozs7V0FLRztRQUNJLDhCQUFNLEdBQWIsVUFBYyxhQUFhO1lBQ3ZCLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUMzQixhQUFhLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN6QztZQUVELGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFTCxvQkFBQztJQUFELENBQUMsQUExWkQsQ0FBNEIsS0FBSyxHQTBaaEM7SUFFRCxPQUFTLGFBQWEsQ0FBQyJ9