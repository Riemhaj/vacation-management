/// <amd-module name="Model/User" />
define("Model/User", ["require", "exports", "Management/RADApplication", "Communication/RADEnvironmentProxy", "Utility/Messenger", "Utility/StorageHelper"], function (require, exports, RADApplication, RADEnvironmentProxy, Messenger, StorageHelper) {
    "use strict";
    var User = /** @class */ (function () {
        function User() {
            var _this = this;
            this.taskInformationFailingCounter = 0;
            var username = StorageHelper.getGlobal('lastLoggedInUserName') || '';
            var principal = StorageHelper.getGlobal('lastLoggedInPrincipal');
            var password = '';
            this.pdoId = ko.observable(-1);
            this.isSingleSignOn = ko.observable(RADApplication.ssoUserName != null);
            this.username = ko.computed({
                write: function (value) {
                    var storageObs = StorageHelper.bindGlobal('lastLoggedInUserName', username);
                    storageObs(value);
                },
                read: function () {
                    var storageObs = StorageHelper.bindGlobal('lastLoggedInUserName', username);
                    var isSingleSignOn = _this.isSingleSignOn();
                    if (isSingleSignOn) {
                        return RADApplication.ssoUserName;
                    }
                    return storageObs();
                }
            });
            this.password = ko.observable(password);
            this.principal = StorageHelper.bindGlobal('lastLoggedInPrincipal', principal);
            this.principalList = ko.observableArray([]);
            this.taskInformation = ko.observable();
            this.menus = ko.observable();
            this.isLoggedIn = ko.observable(false);
            this.firstName = ko.observable();
            this.lastName = ko.observable();
            this.isSuperUser = ko.observable(false);
            this.userSpecificData = {};
            this.additionalLoginInformation = ko.observable({});
            // Wechselt den Mandanten sobald ein anderer Mandant ausgewählt wird
            this.principal.subscribe(function (newPrincipal) {
                var isLoggedIn = _this.isLoggedIn();
                if (isLoggedIn) {
                    RADEnvironmentProxy.changePrincipal({
                        principal: newPrincipal
                    }).then(function (principalDto) {
                        if (newPrincipal != principalDto.name) {
                            // Wechsel hat nicht richtig funktioniert
                            _this.principal(principalDto.name);
                            return;
                        }
                        RADApplication.principal.setPrincipalInfo(principalDto);
                        Messenger.createMessageAndSend('PrincipalChangedMessage', { principal: principalDto, principalName: newPrincipal });
                        RADApplication.updateCache();
                    }).fail(function () {
                        if (RADApplication.principal) {
                            _this.principal(RADApplication.principal.name());
                        }
                    });
                }
            });
            // Aktualisiert die Liste der verfügbaren Mandanten anhand des Benutzernamen
            ko.computed(function () {
                var un = _this.username();
                var showOap = RADApplication.isSystemParamSetAndTrue('Login.ShowOnlyAssignedPrincipals');
                if (showOap === true) {
                    ko.ignoreDependencies(function () {
                        _this.refreshPrincipalList(un);
                    });
                }
            });
            /*
             * Aktualisiert die Liste der verfügbaren Mandanten anhand des Login-Status
             */
            this.isLoggedIn.subscribe(function (isLoggedIn) {
                if (isLoggedIn === true) {
                    RADEnvironmentProxy.getAssignedPrincipals({
                        username: _this.username()
                    }).then(function (data) {
                        if (data.length > 0) {
                            _this.principalList(_this.sortPrincipals(data));
                        }
                    });
                }
            });
            // Läd die Liste der initial verfügbaren Mandanten
            var showOnlyAssignedPrincipals = RADApplication.isSystemParamSetAndTrue('Login.ShowOnlyAssignedPrincipals');
            if (showOnlyAssignedPrincipals === false) {
                RADEnvironmentProxy.getPrincipals().then(function (data) {
                    data.splice(0, 0, { name: '' });
                    _this.principalList(_this.sortPrincipals(data));
                    _this.principal(data[0].name);
                });
            }
            if (Messenger != null) {
                Messenger.registerForMessageId('RefreshPrincipalListMessage', function () { _this.refreshPrincipalList(); }, this);
            }
        }
        /*
         * Setzt Benutzerinformationen anhand eines LoginDTO's
         */
        User.prototype.setLoginInfo = function (loginDto) {
            this.isLoggedIn(loginDto.isAuthorized);
            this.setClientInfo(loginDto.clientInfo);
            this.manageTaskInformation(loginDto.taskinformation);
            if (loginDto.menus.userNavigationTree != null) {
                RADApplication.onBeforeLoadNavigationTree(loginDto.menus.userNavigationTree);
            }
            if (loginDto.menus.userMenuTree != null) {
                RADApplication.onBeforeLoadMenuTree(loginDto.menus.userMenuTree);
            }
            this.menus(loginDto.menus);
            if (loginDto.userSpecificData != null) {
                for (var key in loginDto.userSpecificData) {
                    if (loginDto.userSpecificData.hasOwnProperty(key)) {
                        try {
                            var value = jQuery.parseJSON(loginDto.userSpecificData[key]);
                            this.setUserSpecificData(key, value, true);
                        }
                        catch (e) {
                            var value = loginDto.userSpecificData[key];
                            this.setUserSpecificData(key, value, true);
                        }
                    }
                }
            }
            this.principal = StorageHelper.bindGlobal('lastLoggedInPrincipal', loginDto.clientInfo.user.principal);
        };
        /**
         * Setzt Benutzerinformationen anhand eines ClientInfoDTO's
         */
        User.prototype.setClientInfo = function (clientInfoDto) {
            this.pdoId(clientInfoDto.user.pdoId);
            this.firstName(clientInfoDto.user.firstName);
            this.lastName(clientInfoDto.user.lastName);
            this.isSuperUser(clientInfoDto.user.isSuperUser);
            this.additionalLoginInformation(clientInfoDto.user.additionalLoginInformations);
            this.roles = clientInfoDto.user.roles;
        };
        /**
         * Ändert den Wert einer UserSpecificData
         */
        User.prototype.setUserSpecificData = function (key, value, withoutUpdate) {
            if (withoutUpdate) {
                this.addUserSpecificData(key, value);
            }
            else {
                var userSpecificData = this.addUserSpecificData(key);
                userSpecificData(value);
            }
        };
        /**
         * Liefert das Observable einer UserSpecificData
         */
        User.prototype.getUserSpecificData = function (key, defaultValue) {
            return this.addUserSpecificData(key, defaultValue);
        };
        /**
         * Prüft ob für ein key UserSpecificData vorhanden sind
         */
        User.prototype.existUserSpecificData = function (key) {
            return (key in this.userSpecificData);
        };
        /**
         * Fügt neue UserSpecificData hinzu, falls diese nicht existieren
         */
        User.prototype.addUserSpecificData = function (key, defaultValue) {
            var _this = this;
            if (!this.existUserSpecificData(key)) {
                this.userSpecificData[key] = ko.observable(defaultValue).extend({ notify: 'always' });
                this.userSpecificData[key].subscribe(function (value) {
                    var json;
                    if (typeof (value) === 'string' || value === null) {
                        json = value;
                    }
                    else {
                        json = window['JSON'].stringify(value);
                    }
                    RADEnvironmentProxy.updateUserSpecificData({
                        key: key,
                        value: json
                    });
                    if (value === null) {
                        delete _this.userSpecificData[key];
                    }
                });
            }
            return this.userSpecificData[key];
        };
        /**
         * Ruft die aktuellen Task-Informationen ab und setzt diese im aktuellen User
         */
        User.prototype.refreshTaskInformation = function () {
            var _this = this;
            RADEnvironmentProxy.getCurrentUserTaskInfo().then(function (taskInformation) {
                _this.setTaskInformation(taskInformation);
                _this.taskInformationFailingCounter = 0;
            }).fail(function () {
                _this.taskInformationFailingCounter++;
                if (_this.taskInformationFailingCounter > 5) {
                    _this.stopTaskInformation();
                    console.log('Refreshing task-informations is stopped.');
                }
            });
        };
        /**
         * Setzt die Task-Informationen neu, falls diese sich geändert haben
         */
        User.prototype.setTaskInformation = function (newTaskInformation) {
            var oldTaskInformation = this.taskInformation.peek();
            if (JSON.stringify(oldTaskInformation) !== JSON.stringify(newTaskInformation)) {
                this.taskInformation(newTaskInformation);
            }
        };
        /**
         * Verwaltet die Informationen über die UserTasks
         */
        User.prototype.manageTaskInformation = function (taskInformation) {
            this.setTaskInformation(taskInformation);
            var tasksKeepAliveTime = RADApplication.getSystemParam('Gui.TasksKeepAliveTime', parseInt);
            if (RADApplication.isSystemParamSetAndTrue('Gui.ShowTasks')) {
                if (tasksKeepAliveTime != null) {
                    var intervalTime = tasksKeepAliveTime * 1000;
                    this.userTaskInterval = window.setInterval(this.refreshTaskInformation.bind(this), intervalTime);
                }
                if (Messenger != null) {
                    Messenger.registerForMessageId('RefreshUserTasksMessage', this.refreshTaskInformation.bind(this), this);
                    Messenger.registerForMessageId('PrincipalChangedMessage', this.refreshTaskInformation.bind(this), this);
                }
            }
        };
        /*
         * Verwaltet die Informationen über die UserTasks
         */
        User.prototype.stopTaskInformation = function () {
            Messenger.unregisterForMessageId('RefreshUserTasksMessage', this.refreshTaskInformation.bind(this), this);
            Messenger.unregisterForMessageId('PrincipalChangedMessage', this.refreshTaskInformation.bind(this), this);
            if (this.userTaskInterval != null) {
                window.clearInterval(this.userTaskInterval);
            }
            this.userTaskInterval = null;
            this.taskInformationFailingCounter = 0;
        };
        /**
         * Aktualisiert die Liste der Mandanten für einen Benutzer
         * @param forUserName Falls nicht gesetzt, wird der aktuelle Benutzter gewählt
         */
        User.prototype.refreshPrincipalList = function (forUserName) {
            var _this = this;
            if (forUserName == null) {
                forUserName = this.username.peek();
            }
            //vorausgewählten Mandanten ermitteln
            var preselectPdoId = null;
            var preselectPrincipalName = null;
            if (this.principal() != null && this.principalList().length > 1) {
                var preselectPrincipal = _.find(this.principalList(), function (d) { return d.name === _this.principal(); });
                if (preselectPrincipal) {
                    preselectPdoId = preselectPrincipal.pdoId;
                }
            }
            else if (this.principal()) {
                preselectPrincipalName = this.principal();
            }
            RADEnvironmentProxy.getAssignedPrincipals({
                username: forUserName
            }).then(function (data) {
                if (data.length > 0) {
                    _this.principalList(_this.sortPrincipals(data));
                    var preselect = preselectPdoId ? _.find(data, function (p) { return p.pdoId === preselectPdoId; }) : null;
                    if (!preselect && preselectPrincipalName) {
                        preselect = _.find(data, function (p) { return p.name === preselectPrincipalName; });
                    }
                    if (preselect != null) {
                        _this.principal(preselect.name);
                    }
                    else {
                        _this.principal(data[0].name);
                    }
                }
                else {
                    _this.principalList([]);
                    _this.principal('');
                }
            });
        };
        /**
         *  Sortiert die Liste an Mandanten alphabetisch nach ihrem Namen
         * @param principalList
         */
        User.prototype.sortPrincipals = function (principalList) {
            return _.sortBy(principalList, function (p) { return p.name.toLowerCase(); });
        };
        return User;
    }());
    return User;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlVzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQW9DOzs7SUFRcEM7UUF5Qkk7WUFBQSxpQkEwR0M7WUE5R08sa0NBQTZCLEdBQUcsQ0FBQyxDQUFDO1lBS3RDLElBQU0sUUFBUSxHQUFHLGFBQWEsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkUsSUFBTSxTQUFTLEdBQUcsYUFBYSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ25FLElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUVwQixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUV4RSxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3hCLEtBQUssRUFBRSxVQUFDLEtBQUs7b0JBQ1QsSUFBTSxVQUFVLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDOUUsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixDQUFDO2dCQUNELElBQUksRUFBRTtvQkFDRixJQUFNLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDLHNCQUFzQixFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUM5RSxJQUFNLGNBQWMsR0FBRyxLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQzdDLElBQUksY0FBYyxFQUFFO3dCQUNoQixPQUFPLGNBQWMsQ0FBQyxXQUFXLENBQUM7cUJBQ3JDO29CQUNELE9BQU8sVUFBVSxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7YUFDSixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDLHVCQUF1QixFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQzlFLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUU1QyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN2QyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUU3QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFcEQsb0VBQW9FO1lBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFVBQUMsWUFBWTtnQkFDbEMsSUFBTSxVQUFVLEdBQUcsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUVyQyxJQUFJLFVBQVUsRUFBRTtvQkFDWixtQkFBbUIsQ0FBQyxlQUFlLENBQUM7d0JBQ2hDLFNBQVMsRUFBRSxZQUFZO3FCQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsWUFBWTt3QkFDakIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLElBQUksRUFBRTs0QkFDbkMseUNBQXlDOzRCQUN6QyxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDbEMsT0FBTzt5QkFDVjt3QkFFRCxjQUFjLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN4RCxTQUFTLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLEVBQUUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO3dCQUVwSCxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ2pDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDSixJQUFJLGNBQWMsQ0FBQyxTQUFTLEVBQUU7NEJBQzFCLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO3lCQUNuRDtvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRUgsNEVBQTRFO1lBQzVFLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ1IsSUFBTSxFQUFFLEdBQUcsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUMzQixJQUFNLE9BQU8sR0FBRyxjQUFjLENBQUMsdUJBQXVCLENBQUMsa0NBQWtDLENBQUMsQ0FBQztnQkFFM0YsSUFBSSxPQUFPLEtBQUssSUFBSSxFQUFFO29CQUNsQixFQUFFLENBQUMsa0JBQWtCLENBQUM7d0JBQ2xCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLENBQUM7aUJBQ047WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVIOztlQUVHO1lBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQyxVQUFVO2dCQUNqQyxJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7b0JBQ3JCLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDO3dCQUN0QyxRQUFRLEVBQUUsS0FBSSxDQUFDLFFBQVEsRUFBRTtxQkFDNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7d0JBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDakIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7eUJBQ2pEO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxrREFBa0Q7WUFDbEQsSUFBTSwwQkFBMEIsR0FBRyxjQUFjLENBQUMsdUJBQXVCLENBQUMsa0NBQWtDLENBQUMsQ0FBQztZQUU5RyxJQUFJLDBCQUEwQixLQUFLLEtBQUssRUFBRTtnQkFFdEMsbUJBQW1CLENBQUMsYUFBYSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtvQkFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBUyxDQUFDLENBQUM7b0JBRXZDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUM5QyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakMsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUVELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDbkIsU0FBUyxDQUFDLG9CQUFvQixDQUFDLDZCQUE2QixFQUFFLGNBQVEsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDL0c7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSSwyQkFBWSxHQUFuQixVQUFvQixRQUFhO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7WUFFckQsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLGtCQUFrQixJQUFJLElBQUksRUFBRTtnQkFDM0MsY0FBYyxDQUFDLDBCQUEwQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUNoRjtZQUVELElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFO2dCQUNyQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNwRTtZQUVELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTNCLElBQUksUUFBUSxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDbkMsS0FBSyxJQUFJLEdBQUcsSUFBSSxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7b0JBQ3ZDLElBQUksUUFBUSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDL0MsSUFBSTs0QkFDQSxJQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUMvRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQzt5QkFDOUM7d0JBQUMsT0FBTyxDQUFDLEVBQUU7NEJBQ1IsSUFBTSxLQUFLLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDOzRCQUM3QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQzt5QkFDOUM7cUJBQ0o7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsRUFBRSxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzRyxDQUFDO1FBRUQ7O1dBRUc7UUFDSSw0QkFBYSxHQUFwQixVQUFxQixhQUFrQjtZQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzFDLENBQUM7UUFFRDs7V0FFRztRQUNJLGtDQUFtQixHQUExQixVQUEyQixHQUFXLEVBQUUsS0FBVSxFQUFFLGFBQXNCO1lBQ3RFLElBQUksYUFBYSxFQUFFO2dCQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0gsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZELGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzNCO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0ksa0NBQW1CLEdBQTFCLFVBQTJCLEdBQVcsRUFBRSxZQUFpQjtZQUNyRCxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDdkQsQ0FBQztRQUVEOztXQUVHO1FBQ0ksb0NBQXFCLEdBQTVCLFVBQTZCLEdBQVc7WUFDcEMsT0FBTyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMxQyxDQUFDO1FBRUQ7O1dBRUc7UUFDSyxrQ0FBbUIsR0FBM0IsVUFBNEIsR0FBVyxFQUFFLFlBQWtCO1lBQTNELGlCQXVCQztZQXRCRyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztnQkFFdEYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQUs7b0JBQ3ZDLElBQUksSUFBWSxDQUFDO29CQUNqQixJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTt3QkFDL0MsSUFBSSxHQUFHLEtBQUssQ0FBQztxQkFDaEI7eUJBQ0k7d0JBQ0QsSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQzFDO29CQUNELG1CQUFtQixDQUFDLHNCQUFzQixDQUFDO3dCQUN2QyxHQUFHLEVBQUUsR0FBRzt3QkFDUixLQUFLLEVBQUUsSUFBSTtxQkFDZCxDQUFDLENBQUM7b0JBQ0gsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFO3dCQUNoQixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDckM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUVELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7UUFFRDs7V0FFRztRQUNLLHFDQUFzQixHQUE5QjtZQUFBLGlCQVdDO1lBVkcsbUJBQW1CLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxlQUFlO2dCQUM5RCxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyw2QkFBNkIsR0FBRyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDO2dCQUNyQyxJQUFJLEtBQUksQ0FBQyw2QkFBNkIsR0FBRyxDQUFDLEVBQUU7b0JBQ3hDLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO29CQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7aUJBQzNEO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQ7O1dBRUc7UUFDSyxpQ0FBa0IsR0FBMUIsVUFBMkIsa0JBQXVCO1lBQzlDLElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV2RCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7Z0JBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUM1QztRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLG9DQUFxQixHQUE1QixVQUE2QixlQUFvQjtZQUM3QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFFekMsSUFBTSxrQkFBa0IsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLHdCQUF3QixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBRTdGLElBQUksY0FBYyxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUN6RCxJQUFJLGtCQUFrQixJQUFJLElBQUksRUFBRTtvQkFDNUIsSUFBTSxZQUFZLEdBQUcsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO29CQUMvQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDO2lCQUNwRztnQkFFRCxJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7b0JBQ25CLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUN4RyxTQUFTLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDM0c7YUFDSjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNJLGtDQUFtQixHQUExQjtZQUNJLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzFHLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRTFHLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDL0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUMvQztZQUVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFFN0IsSUFBSSxDQUFDLDZCQUE2QixHQUFHLENBQUMsQ0FBQztRQUMzQyxDQUFDO1FBRUQ7OztXQUdHO1FBQ0ssbUNBQW9CLEdBQTVCLFVBQTZCLFdBQW9CO1lBQWpELGlCQXVDQztZQXRDRyxJQUFJLFdBQVcsSUFBSSxJQUFJLEVBQUU7Z0JBQ3JCLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3RDO1lBRUQscUNBQXFDO1lBQ3JDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQztZQUMxQixJQUFJLHNCQUFzQixHQUFHLElBQUksQ0FBQztZQUNsQyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzdELElBQU0sa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUUsVUFBQyxDQUFDLElBQU8sT0FBTyxDQUFDLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQSxDQUFDLENBQUMsQ0FBa0IsQ0FBQztnQkFDeEgsSUFBSSxrQkFBa0IsRUFBRTtvQkFDcEIsY0FBYyxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQztpQkFDN0M7YUFDSjtpQkFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRTtnQkFDekIsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQzdDO1lBRUQsbUJBQW1CLENBQUMscUJBQXFCLENBQUM7Z0JBQ3RDLFFBQVEsRUFBRSxXQUFXO2FBQ3hCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJO2dCQUNULElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2pCLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUU5QyxJQUFJLFNBQVMsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQUMsQ0FBQyxJQUFPLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxjQUFjLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUNuRyxJQUFJLENBQUMsU0FBUyxJQUFJLHNCQUFzQixFQUFFO3dCQUN0QyxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBQyxDQUFDLElBQU8sT0FBTyxDQUFDLENBQUMsSUFBSSxLQUFLLHNCQUFzQixDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2pGO29CQUVELElBQUksU0FBUyxJQUFJLElBQUksRUFBRTt3QkFDbkIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2xDO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNoQztpQkFDSjtxQkFDSTtvQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUN2QixLQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUN0QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVEOzs7V0FHRztRQUNLLDZCQUFjLEdBQXRCLFVBQXVCLGFBQW1DO1lBQ3RELE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsVUFBQyxDQUFnQixJQUFPLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNGLENBQUM7UUFDTCxXQUFDO0lBQUQsQ0FBQyxBQTlWRCxJQThWQztJQUVELE9BQVMsSUFBSSxDQUFBIn0=