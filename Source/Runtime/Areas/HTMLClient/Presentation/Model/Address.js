/// <amd-module name="Model/Address" />
define("Model/Address", ["require", "exports"], function (require, exports) {
    "use strict";
    var Address = /** @class */ (function () {
        function Address(url) {
            this.url = url;
            this.name = '';
            this.parameters = {};
            this.hiddenNames = ['Filter', 'SerializedFilter'];
            this.navigated = ko.observable(false).extend({ notify: 'always' });
            if (url != null) {
                this.init();
            }
        }
        Address.prototype.onNavigated = function (navigatedCallback) {
            if (navigatedCallback == null || typeof navigatedCallback != "function") {
                return;
            }
            this.navigated.subscribe(navigatedCallback);
        };
        Address.prototype.init = function () {
            if (_.isString(this.url)) {
                var split = this.url.split('/');
                this.name = split[split.length - 1];
            }
            if (!this.name) {
                this.name = 'Home';
            }
            if (this.name.indexOf('?') > -1) {
                var split = this.name.split('?');
                var uri = split[1];
                this.name = split[0];
                var nameValuePairs = uri.split('&');
                if (nameValuePairs.length > 0) {
                    for (var i = 0; i < nameValuePairs.length; i++) {
                        var nameValuePairStr = nameValuePairs[i];
                        // let nameValuePair = nameValuePairStr.split('='); // funktioniert nicht, wenn in dem String ein '=' vorkommt
                        var eqIdx = nameValuePairStr.indexOf('=');
                        var key = nameValuePairStr.substring(0, eqIdx);
                        var val = nameValuePairStr.substring(eqIdx + 1);
                        // Filter werden meistens in einfachen Anf�hrungszeichen in die Adresse gelegt
                        // Notiz: decodeURIComponent/encodeURIComponent wird in diesem Projekt noch nicht verwendet
                        // Entspricht Spezialbehandlung aus SL-Code NavigationService.GetQueryStringParameterValue
                        if (key.toLowerCase() == 'filter' && val.length >= 2 && val.charAt(0) == "'" && val.charAt(val.length - 1) == "'") {
                            val = val.substring(1, val.length - 1);
                        }
                        try {
                            val = decodeURIComponent(val);
                        }
                        catch (e) {
                            // okay, wir haben eine URL bekommen, die % enth�lt, aber eigentlich gar nicht URI-encoded ist
                        }
                        this.parameters[key] = val;
                    }
                }
            }
        };
        Address.prototype.getName = function () {
            return this.name + '';
        };
        Address.prototype.setName = function (parameter) {
            this.name = parameter;
        };
        Address.prototype.getParameters = function () {
            return jQuery.extend(true, {}, this.parameters);
        };
        Address.prototype.getParameter = function (parameter) {
            if (parameter in this.parameters) {
                return this.parameters[parameter];
            }
            else {
                return null;
            }
        };
        Address.prototype.setParameter = function (parameter, value) {
            if (typeof value == 'function') {
                return;
            }
            this.parameters[parameter] = value;
        };
        Address.prototype.setParameters = function (list) {
            if (_.isArray(list)) {
                for (var i = 0; i < list.length; i++) {
                    this.setParameter(list[i].name, list[i].value);
                }
            }
            else {
                for (var name_1 in list) {
                    this.setParameter(name_1, list[name_1]);
                }
            }
        };
        Address.prototype.setHiddenParameter = function (parameter, value) {
            this.setParameter(parameter, value);
            this.setHiddenName(parameter);
        };
        Address.prototype.setHiddenParameters = function (list) {
            if (list == null) {
                return;
            }
            this.setParameters(list);
            this.setHiddenNames(list);
        };
        Address.prototype.getHiddenNames = function () {
            return this.hiddenNames.slice(0);
        };
        Address.prototype.setHiddenNames = function (names) {
            if (names == null) {
                return;
            }
            if (_.isArray(names)) {
                for (var i = 0; i < names.length; i++) {
                    var name_2 = names[i];
                    if (typeof name_2 == 'string') {
                        this.setHiddenName(name_2);
                    }
                    else if (name_2 != null && name_2.name != null && typeof name_2.name == 'string') {
                        this.setHiddenName(name_2);
                    }
                }
            }
            else {
                for (var name_3 in names) {
                    this.setHiddenName(name_3);
                }
            }
        };
        Address.prototype.setHiddenName = function (name) {
            if (this.hiddenNames.indexOf(name) < 0) {
                this.hiddenNames.push(name);
            }
        };
        Address.prototype.getQueryString = function (onlyPublic) {
            var _this = this;
            var result = '';
            if (_.keys(this.parameters).length > 0) {
                var parameterArray_1 = [];
                _.each(this.parameters, function (value, key) {
                    if (onlyPublic && _this.hiddenNames.indexOf(key) >= 0) {
                        return;
                    }
                    if (value == null) {
                        return;
                    }
                    if (key.toLowerCase() == 'query' || key.toLowerCase() == 'titleextension') {
                        value = encodeURIComponent(value);
                    }
                    parameterArray_1.push(key + '=' + value);
                });
                result += '?' + parameterArray_1.join('&');
            }
            return result;
        };
        Address.prototype.getPublicURL = function () {
            var queryString = this.getQueryString(true);
            var result = this.name + queryString;
            return result;
        };
        Address.prototype.toString = function () {
            var queryString = this.getQueryString(false);
            var result = this.name + queryString;
            return result;
        };
        Address.prototype.equalsDeeply = function (address) {
            if (this.name !== address.getName()) {
                return false;
            }
            if (!_.isEqual(this.parameters, address.getParameters())) {
                return false;
            }
            if (!_.isEqual(this.hiddenNames, address.getHiddenNames())) {
                return false;
            }
            return true;
        };
        Address.prototype.isEqualTo = function (address) {
            if (_.isArray(address)) {
                for (var i = 0; i < address.length; i++) {
                    if (this.isEqualTo(address[i])) {
                        return true;
                    }
                }
                return false;
            }
            else {
                var ad = address.getName();
                if (!this.name && !ad) {
                    return true;
                }
                else if (!this.name && ad) {
                    return false;
                }
                else if (this.name && !ad) {
                    return false;
                }
                else {
                    return this.name.toLowerCase() === ad.toLowerCase();
                }
            }
        };
        Address.prototype.clone = function () {
            var address = new Address();
            address.setName(this.getName());
            address.setParameters(this.getParameters());
            address.setHiddenNames(this.getHiddenNames());
            return address;
        };
        return Address;
    }());
    return Address;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWRkcmVzcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFkZHJlc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsdUNBQXVDOzs7SUFFdkM7UUFRSSxpQkFBb0IsR0FBWTtZQUFaLFFBQUcsR0FBSCxHQUFHLENBQVM7WUFDNUIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLENBQUM7WUFFbEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBRW5FLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDYixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDZjtRQUNMLENBQUM7UUFFTSw2QkFBVyxHQUFsQixVQUFtQixpQkFBOEM7WUFDN0QsSUFBSSxpQkFBaUIsSUFBSSxJQUFJLElBQUksT0FBTyxpQkFBaUIsSUFBSSxVQUFVLEVBQUU7Z0JBQ3JFLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUVNLHNCQUFJLEdBQVg7WUFDSSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzthQUN2QztZQUVELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNaLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2FBQ3RCO1lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDN0IsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFbkIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRXJCLElBQUksY0FBYyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRXBDLElBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUM1QyxJQUFJLGdCQUFnQixHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekMsOEdBQThHO3dCQUM5RyxJQUFJLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzFDLElBQUksR0FBRyxHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7d0JBQy9DLElBQUksR0FBRyxHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBRWhELDhFQUE4RTt3QkFDOUUsMkZBQTJGO3dCQUMzRiwwRkFBMEY7d0JBQzFGLElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLFFBQVEsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFOzRCQUMvRyxHQUFHLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzt5QkFDMUM7d0JBRUQsSUFBSTs0QkFDQSxHQUFHLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7eUJBQ2pDO3dCQUNELE9BQU8sQ0FBQyxFQUFFOzRCQUNOLDhGQUE4Rjt5QkFDakc7d0JBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7cUJBQzlCO2lCQUNKO2FBQ0o7UUFDTCxDQUFDO1FBRU0seUJBQU8sR0FBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDMUIsQ0FBQztRQUVNLHlCQUFPLEdBQWQsVUFBZSxTQUFpQjtZQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBRU0sK0JBQWEsR0FBcEI7WUFDSSxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsQ0FBQztRQUdNLDhCQUFZLEdBQW5CLFVBQW9CLFNBQWlCO1lBQ2pDLElBQUksU0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQzlCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNyQztpQkFDSTtnQkFDRCxPQUFPLElBQUksQ0FBQzthQUNmO1FBQ0wsQ0FBQztRQUVNLDhCQUFZLEdBQW5CLFVBQW9CLFNBQWlCLEVBQUUsS0FBYTtZQUNoRCxJQUFJLE9BQU8sS0FBSyxJQUFJLFVBQVUsRUFBRTtnQkFDNUIsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDdkMsQ0FBQztRQUdNLCtCQUFhLEdBQXBCLFVBQXFCLElBQVM7WUFDMUIsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDbEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbEQ7YUFDSjtpQkFDSTtnQkFDRCxLQUFLLElBQUksTUFBSSxJQUFJLElBQUksRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFJLEVBQUUsSUFBSSxDQUFDLE1BQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0o7UUFDTCxDQUFDO1FBRU0sb0NBQWtCLEdBQXpCLFVBQTBCLFNBQWlCLEVBQUUsS0FBYTtZQUN0RCxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xDLENBQUM7UUFFTSxxQ0FBbUIsR0FBMUIsVUFBMkIsSUFBUztZQUNoQyxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ2QsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFTSxnQ0FBYyxHQUFyQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUVNLGdDQUFjLEdBQXJCLFVBQXNCLEtBQVU7WUFDNUIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUNmLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25DLElBQUksTUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEIsSUFBSSxPQUFPLE1BQUksSUFBSSxRQUFRLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBSSxDQUFDLENBQUM7cUJBQzVCO3lCQUNJLElBQUksTUFBSSxJQUFJLElBQUksSUFBSSxNQUFJLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxPQUFPLE1BQUksQ0FBQyxJQUFJLElBQUksUUFBUSxFQUFFO3dCQUN4RSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQUksQ0FBQyxDQUFDO3FCQUM1QjtpQkFDSjthQUNKO2lCQUNJO2dCQUNELEtBQUssSUFBSSxNQUFJLElBQUksS0FBSyxFQUFFO29CQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQUksQ0FBQyxDQUFDO2lCQUM1QjthQUNKO1FBQ0wsQ0FBQztRQUVNLCtCQUFhLEdBQXBCLFVBQXFCLElBQVk7WUFDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQy9CO1FBQ0wsQ0FBQztRQUVPLGdDQUFjLEdBQXRCLFVBQXVCLFVBQW1CO1lBQTFDLGlCQXlCQztZQXhCRyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFFaEIsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLGdCQUFjLEdBQUcsRUFBRSxDQUFDO2dCQUV4QixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBQyxLQUFhLEVBQUUsR0FBVztvQkFDL0MsSUFBSSxVQUFVLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNsRCxPQUFPO3FCQUNWO29CQUVELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTt3QkFDZixPQUFPO3FCQUNWO29CQUVELElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLE9BQU8sSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFLElBQUksZ0JBQWdCLEVBQUU7d0JBQ3ZFLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDckM7b0JBQ0QsZ0JBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFDM0MsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsTUFBTSxJQUFJLEdBQUcsR0FBRyxnQkFBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM1QztZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFTSw4QkFBWSxHQUFuQjtZQUNJLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUM7WUFFckMsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQztRQUVNLDBCQUFRLEdBQWY7WUFDSSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO1lBRXJDLE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFTSw4QkFBWSxHQUFuQixVQUFvQixPQUFPO1lBQ3ZCLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2pDLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRTtnQkFDdEQsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxFQUFFO2dCQUN4RCxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTSwyQkFBUyxHQUFoQixVQUFpQixPQUFnQjtZQUM3QixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNyQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQzVCLE9BQU8sSUFBSSxDQUFDO3FCQUNmO2lCQUNKO2dCQUVELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO2lCQUNJO2dCQUNELElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFFM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7b0JBQ25CLE9BQU8sSUFBSSxDQUFDO2lCQUNmO3FCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRTtvQkFDdkIsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO3FCQUNJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtvQkFDdkIsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO3FCQUNJO29CQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3ZEO2FBQ0o7UUFDTCxDQUFDO1FBRU0sdUJBQUssR0FBWjtZQUNJLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7WUFDNUIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUNoQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO1lBQzVDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7WUFDOUMsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQztRQUNMLGNBQUM7SUFBRCxDQUFDLEFBalFELElBaVFDO0lBRUQsT0FBUyxPQUFPLENBQUMifQ==