/// <amd-module name="Communication/RADEnvironmentProxyInterceptor" />
define("Communication/RADEnvironmentProxyInterceptor", ["require", "exports", "Management/RADApplication"], function (require, exports, RADApplication) {
    "use strict";
    var RADEnvironmentProxyInterceptor = /** @class */ (function () {
        function RADEnvironmentProxyInterceptor() {
        }
        RADEnvironmentProxyInterceptor._args = function (keys, args) {
            var result = { token: RADApplication.token };
            for (var i = 0; i < keys.length; i++) {
                result[keys[i]] = args[0][keys[i]];
            }
            return result;
        };
        RADEnvironmentProxyInterceptor.intercept = function (originalProxy) {
            var _orginalCallOperationFromDVContext = originalProxy.__proto__.constructor.callOperationFromDVContext.bind(originalProxy);
            originalProxy.__proto__.constructor.callOperationFromDVContext = function () {
                var allArguments = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    allArguments[_i] = arguments[_i];
                }
                if (!allArguments[0].hasOwnProperty('withValidation')) {
                    allArguments[0]['withValidation'] = true;
                }
                return _orginalCallOperationFromDVContext(allArguments[0]);
            };
            originalProxy.__proto__.constructor.outputMessage = function () {
                var allArguments = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    allArguments[_i] = arguments[_i];
                }
                var args = RADEnvironmentProxyInterceptor._args(['message', 'priority'], allArguments);
                var deferred = $.ajax({
                    url: MODEL.HTMLClientBaseURL + 'HTMLClient/Json/' + 'outputMessage',
                    type: 'POST',
                    headers: { '__RequestVerificationToken': RADApplication.xcsrfToken },
                    cache: false,
                    async: true,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(args)
                });
                return deferred;
            };
        };
        return RADEnvironmentProxyInterceptor;
    }());
    return RADEnvironmentProxyInterceptor;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUkFERW52aXJvbm1lbnRQcm94eUludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUkFERW52aXJvbm1lbnRQcm94eUludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNFQUFzRTs7O0lBTXRFO1FBQUE7UUEwQ0EsQ0FBQztRQXhDa0Isb0NBQUssR0FBcEIsVUFBcUIsSUFBSSxFQUFFLElBQUk7WUFDM0IsSUFBSSxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRTdDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNsQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3RDO1lBRUQsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQztRQUVhLHdDQUFTLEdBQXZCLFVBQXdCLGFBQWtCO1lBQ3RDLElBQUksa0NBQWtDLEdBQUcsYUFBYSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBRTVILGFBQWEsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLDBCQUEwQixHQUFHO2dCQUFDLHNCQUEyQjtxQkFBM0IsVUFBMkIsRUFBM0IscUJBQTJCLEVBQTNCLElBQTJCO29CQUEzQixpQ0FBMkI7O2dCQUN6RixJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO29CQUNuRCxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxJQUFJLENBQUM7aUJBQzVDO2dCQUVELE9BQU8sa0NBQWtDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFL0QsQ0FBQyxDQUFDO1lBRUYsYUFBYSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsYUFBYSxHQUFHO2dCQUFDLHNCQUEyQjtxQkFBM0IsVUFBMkIsRUFBM0IscUJBQTJCLEVBQTNCLElBQTJCO29CQUEzQixpQ0FBMkI7O2dCQUM1RSxJQUFJLElBQUksR0FBRyw4QkFBOEIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBRXZGLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQ2xCLEdBQUcsRUFBRSxLQUFLLENBQUMsaUJBQWlCLEdBQUcsa0JBQWtCLEdBQUcsZUFBZTtvQkFDbkUsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLEVBQUUsNEJBQTRCLEVBQUUsY0FBYyxDQUFDLFVBQVUsRUFBRTtvQkFDcEUsS0FBSyxFQUFFLEtBQUs7b0JBQ1osS0FBSyxFQUFFLElBQUk7b0JBQ1gsV0FBVyxFQUFFLGtCQUFrQjtvQkFDL0IsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztpQkFDN0IsQ0FBQyxDQUFDO2dCQUVILE9BQU8sUUFBUSxDQUFDO1lBQ3BCLENBQUMsQ0FBQztRQUNOLENBQUM7UUFFTCxxQ0FBQztJQUFELENBQUMsQUExQ0QsSUEwQ0M7SUFFRCxPQUFTLDhCQUE4QixDQUFDIn0=