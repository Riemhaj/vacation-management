/// <amd-module name="Communication/RADGeneratedModels" />
define("Communication/RADGeneratedModels", ["require", "exports"], function (require, exports) {
    "use strict";
    /*
     * ATTENTION: This file was automatically created by RADPresentation.HTMLClient.Generator
     */
    var RADGeneratedModels = /** @class */ (function () {
        function RADGeneratedModels() {
        }
        RADGeneratedModels.statusEnumDTO = function (index) {
            var values = [];
            values.push('Successfull');
            values.push('Warning');
            values.push('Error');
            values.push('Exception');
            values.push('NoLegal');
            values.push('IsLockedByOther');
            values.push('SaveRequired');
            return values[index];
        };
        RADGeneratedModels.changeObjectDTO = function (changeObjectDTO) {
            var type = {};
            type._type = 'changeObjectDTO';
            type.errorMessage = changeObjectDTO.ErrorMessage;
            type.isModified = changeObjectDTO.IsModified;
            type.lockedBy = changeObjectDTO.LockedBy;
            type.lockedObjectIdent = changeObjectDTO.LockedObjectIdent;
            type.returnType = changeObjectDTO.ReturnType;
            type.returnValue = changeObjectDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(changeObjectDTO.Status);
            return type;
        };
        RADGeneratedModels.currencyDTO = function (currencyDTO) {
            var type = {};
            type._type = 'currencyDTO';
            type.isActive = currencyDTO.IsActive;
            type.isO4217NumberCode = currencyDTO.ISO4217NumberCode;
            type.rate = currencyDTO.Rate;
            type.scale = currencyDTO.Scale;
            type.shortname = currencyDTO.Shortname;
            type.subcurrencies = [];
            if (currencyDTO.Subcurrencies != null) {
                for (var i2 in currencyDTO.Subcurrencies) {
                    var typeName = 'currencyDTO';
                    type.subcurrencies.push(RADGeneratedModels[typeName](currencyDTO.Subcurrencies[i2]));
                }
            }
            return type;
        };
        RADGeneratedModels.languageDTO = function (languageDTO) {
            var type = {};
            type._type = 'languageDTO';
            type.cultureInfoName = languageDTO.CultureInfoName;
            type.currencyPattern = languageDTO.CurrencyPattern;
            type.datePattern = languageDTO.DatePattern;
            type.displayName = languageDTO.DisplayName;
            type.floatingPrecision = languageDTO.FloatingPrecision;
            type.iconUrl = languageDTO.IconUrl;
            type.iso6391 = languageDTO.ISO6391;
            type.timePattern = languageDTO.TimePattern;
            return type;
        };
        RADGeneratedModels.configurationDTO = function (configurationDTO) {
            var type = {};
            type._type = 'configurationDTO';
            type.currencies = {};
            if (configurationDTO.Currencies != null) {
                for (var i1 in configurationDTO.Currencies) {
                    if (configurationDTO.Currencies.hasOwnProperty(i1)) {
                        type.currencies[i1] = RADGeneratedModels.currencyDTO(configurationDTO.Currencies[i1]);
                    }
                }
            }
            type.currencyExchangePaths = {};
            if (configurationDTO.CurrencyExchangePaths != null) {
                for (var i3 in configurationDTO.CurrencyExchangePaths) {
                    if (configurationDTO.CurrencyExchangePaths.hasOwnProperty(i3)) {
                        type.currencyExchangePaths[i3] = configurationDTO.CurrencyExchangePaths[i3];
                    }
                }
            }
            type.defaultCurrencyID = configurationDTO.DefaultCurrencyID;
            if (configurationDTO.DefaultLanguage != null) {
                var typeName = 'languageDTO';
                type.defaultLanguage = RADGeneratedModels[typeName](configurationDTO.DefaultLanguage);
            }
            else {
                type.defaultLanguage = null;
            }
            type.languages = [];
            if (configurationDTO.Languages != null) {
                for (var i4 in configurationDTO.Languages) {
                    var typeName = 'languageDTO';
                    type.languages.push(RADGeneratedModels[typeName](configurationDTO.Languages[i4]));
                }
            }
            type.possibleAssoziationListViewPages = [];
            if (configurationDTO.PossibleAssoziationListViewPages != null) {
                for (var i5 in configurationDTO.PossibleAssoziationListViewPages) {
                    type.possibleAssoziationListViewPages.push(configurationDTO.PossibleAssoziationListViewPages[i5]);
                }
            }
            type.possibleListViewPages = [];
            if (configurationDTO.PossibleListViewPages != null) {
                for (var i6 in configurationDTO.PossibleListViewPages) {
                    type.possibleListViewPages.push(configurationDTO.PossibleListViewPages[i6]);
                }
            }
            type.startpageUIFactory = configurationDTO.StartpageUIFactory;
            type.systemProperties = {};
            if (configurationDTO.SystemProperties != null) {
                for (var i7 in configurationDTO.SystemProperties) {
                    if (configurationDTO.SystemProperties.hasOwnProperty(i7)) {
                        type.systemProperties[i7] = configurationDTO.SystemProperties[i7];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.typeMetaDTO = function (typeMetaDTO) {
            var type = {};
            type._type = 'typeMetaDTO';
            type.ergName = typeMetaDTO.ErgName;
            type.iconUrl = typeMetaDTO.IconUrl;
            type.name = typeMetaDTO.Name;
            type.pdoId = typeMetaDTO.PDO_ID;
            type.tooltip = typeMetaDTO.Tooltip;
            return type;
        };
        RADGeneratedModels.extensionModuleDTO = function (extensionModuleDTO) {
            var type = {};
            type._type = 'extensionModuleDTO';
            type.alias = extensionModuleDTO.Alias;
            type.ergName = extensionModuleDTO.ErgName;
            type.title = extensionModuleDTO.Title;
            type.types = [];
            if (extensionModuleDTO.Types != null) {
                for (var i12 in extensionModuleDTO.Types) {
                    var typeName = 'typeMetaDTO';
                    type.types.push(RADGeneratedModels[typeName](extensionModuleDTO.Types[i12]));
                }
            }
            return type;
        };
        RADGeneratedModels.applicationDTO = function (applicationDTO) {
            var type = {};
            type._type = 'applicationDTO';
            if (applicationDTO.Configuration != null) {
                var typeName = 'configurationDTO';
                type.configuration = RADGeneratedModels[typeName](applicationDTO.Configuration);
            }
            else {
                type.configuration = null;
            }
            type.deployedModules = [];
            if (applicationDTO.DeployedModules != null) {
                for (var i8 in applicationDTO.DeployedModules) {
                    type.deployedModules.push(applicationDTO.DeployedModules[i8]);
                }
            }
            type.description = applicationDTO.Description;
            type.ergName = applicationDTO.ErgName;
            type.expiredModules = [];
            if (applicationDTO.ExpiredModules != null) {
                for (var i9 in applicationDTO.ExpiredModules) {
                    type.expiredModules.push(applicationDTO.ExpiredModules[i9]);
                }
            }
            type.iconUrl = applicationDTO.IconUrl;
            type.isSystemLocked = applicationDTO.IsSystemLocked;
            type.lastCacheUpdate = applicationDTO.LastCacheUpdate;
            type.licensedModules = [];
            if (applicationDTO.LicensedModules != null) {
                for (var i10 in applicationDTO.LicensedModules) {
                    type.licensedModules.push(applicationDTO.LicensedModules[i10]);
                }
            }
            type.modules = {};
            if (applicationDTO.Modules != null) {
                for (var i11 in applicationDTO.Modules) {
                    if (applicationDTO.Modules.hasOwnProperty(i11)) {
                        type.modules[i11] = RADGeneratedModels.extensionModuleDTO(applicationDTO.Modules[i11]);
                    }
                }
            }
            type.name = applicationDTO.Name;
            type.pdApplicationChangedTime = applicationDTO.PDApplicationChangedTime;
            type.pdEnumerationValues = {};
            if (applicationDTO.PDEnumerationValues != null) {
                for (var i13 in applicationDTO.PDEnumerationValues) {
                    if (applicationDTO.PDEnumerationValues.hasOwnProperty(i13)) {
                        type.pdEnumerationValues[i13] = applicationDTO.PDEnumerationValues[i13];
                    }
                }
            }
            type.principalTypeName = applicationDTO.PrincipalTypeName;
            type.systemLockMessage = applicationDTO.SystemLockMessage;
            type.templateStyle = applicationDTO.TemplateStyle;
            type.tooltip = applicationDTO.Tooltip;
            return type;
        };
        RADGeneratedModels.operationDTO = function (operationDTO) {
            var type = {};
            type._type = 'operationDTO';
            type.ergName = operationDTO.ErgName;
            type.hasRight = operationDTO.HasRight;
            type.iconUrl = operationDTO.IconUrl;
            type.isNeedingParameters = operationDTO.IsNeedingParameters;
            type.name = operationDTO.Name;
            type.operationGUID = operationDTO.OperationGUID;
            type.showResult = operationDTO.ShowResult;
            type.tooltip = operationDTO.Tooltip;
            return type;
        };
        RADGeneratedModels.columnDTO = function (columnDTO) {
           
            var type = {};
            type._type = 'columnDTO';
            type.dotNetTypeName = columnDTO.DotNetTypeName;
            type.isFilterable = columnDTO.IsFilterable;
            type.isVisible = columnDTO.IsVisible;
            type.listViewColumnFactory = columnDTO.ListViewColumnFactory;
            type.listViewPDOID = columnDTO.ListViewPDOID;
            type.mlsErgName = {};
            if (columnDTO.MLSErgName != null) {
                for (var i21 in columnDTO.MLSErgName) {
                    if (columnDTO.MLSErgName.hasOwnProperty(i21)) {
                        type.mlsErgName[i21] = columnDTO.MLSErgName[i21];
                    }
                }
            }
            type.name = columnDTO.Name;
            type.possiblePDEnumerationValues = {};
            if (columnDTO.PossiblePDEnumerationValues != null) {
                for (var i22 in columnDTO.PossiblePDEnumerationValues) {
                    if (columnDTO.PossiblePDEnumerationValues.hasOwnProperty(i22)) {
                        type.possiblePDEnumerationValues[i22] = columnDTO.PossiblePDEnumerationValues[i22];
                    }
                }
            }
            type.sortable = columnDTO.Sortable;
            type.uiGenerator = columnDTO.UIGenerator;
            type.unitNames = {};
            if (columnDTO.UnitNames != null) {
                for (var i23 in columnDTO.UnitNames) {
                    if (columnDTO.UnitNames.hasOwnProperty(i23)) {
                        type.unitNames[i23] = columnDTO.UnitNames[i23];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.listViewDTO = function (listViewDTO) {
            var type = {};
            type._type = 'listViewDTO';
            type.assoziationListView = listViewDTO.AssoziationListView;
            type.columns = [];
            if (listViewDTO.Columns != null) {
                for (var i20 in listViewDTO.Columns) {
                    var typeName = 'columnDTO';
                    type.columns.push(RADGeneratedModels[typeName](listViewDTO.Columns[i20]));
                }
            }
            type.defaultOrder = listViewDTO.DefaultOrder;
            type.ergName = listViewDTO.ErgName;
            type.ergNamePluralized = listViewDTO.ErgNamePluralized;
            type.filter = listViewDTO.Filter;
            type.iconUrl = listViewDTO.IconUrl;
            type.includeAllPrincipals = listViewDTO.IncludeAllPrincipals;
            type.includeBasePrincipals = listViewDTO.IncludeBasePrincipals;
            type.includeSubPrincipals = listViewDTO.IncludeSubPrincipals;
            type.isPrincipalIndependent = listViewDTO.IsPrincipalIndependent;
            type.listViewID = listViewDTO.ListViewID;
            type.multiColumnSort = listViewDTO.MultiColumnSort;
            type.name = listViewDTO.Name;
            type.operations = [];
            if (listViewDTO.Operations != null) {
                for (var i24 in listViewDTO.Operations) {
                    var typeName = 'operationDTO';
                    type.operations.push(RADGeneratedModels[typeName](listViewDTO.Operations[i24]));
                }
            }
            type.relationName = listViewDTO.RelationName;
            type.showButtonCreate = listViewDTO.ShowButtonCreate;
            type.showButtonCSVExport = listViewDTO.ShowButtonCSVExport;
            type.showButtonDelete = listViewDTO.ShowButtonDelete;
            type.showButtonDisconnect = listViewDTO.ShowButtonDisconnect;
            type.showButtonEdit = listViewDTO.ShowButtonEdit;
            type.showButtonSelect = listViewDTO.ShowButtonSelect;
            type.showButtonXLSExport = listViewDTO.ShowButtonXLSExport;
            type.showFilter = listViewDTO.ShowFilter;
            type.showMoveButtons = listViewDTO.ShowMoveButtons;
            type.staticFilterExpression = listViewDTO.StaticFilterExpression;
            type.summaryViewAutoGenerating = listViewDTO.SummaryViewAutoGenerating;
            type.summaryViewParameters = listViewDTO.SummaryViewParameters;
            type.summaryViewUIFactory = listViewDTO.SummaryViewUIFactory;
            type.tooltip = listViewDTO.Tooltip;
            type.typeName = listViewDTO.TypeName;
            type.types = [];
            if (listViewDTO.Types != null) {
                for (var i25 in listViewDTO.Types) {
                    var typeName = 'typeMetaDTO';
                    type.types.push(RADGeneratedModels[typeName](listViewDTO.Types[i25]));
                }
            }
            type.uiListener = listViewDTO.UIListener;
            return type;
        };
        RADGeneratedModels.dvgItemDTO = function (dvgItemDTO) {
            var type = {};
            type._type = 'dvgItemDTO';
            type.children = [];
            if (dvgItemDTO.Children != null) {
                for (var i19 in dvgItemDTO.Children) {
                    var typeName = 'dvgItemDTO';
                    type.children.push(RADGeneratedModels[typeName](dvgItemDTO.Children[i19]));
                }
            }
            type.disableNavigation = dvgItemDTO.DisableNavigation;
            type.dotNetTypeName = dvgItemDTO.DotNetTypeName;
            type.ergName = dvgItemDTO.ErgName;
            type.guiReadOnly = dvgItemDTO.GUIReadOnly;
            type.iconUrl = dvgItemDTO.IconUrl;
            type.isDerived = dvgItemDTO.IsDerived;
            type.isDerivedOverwritable = dvgItemDTO.IsDerivedOverwritable;
            type.isMultiSelectablePDEnumeration = dvgItemDTO.IsMultiSelectablePDEnumeration;
            type.isPDEmbedded = dvgItemDTO.IsPDEmbedded;
            type.isPDEmbeddedMedium = dvgItemDTO.IsPDEmbeddedMedium;
            type.isPDEnumeration = dvgItemDTO.IsPDEnumeration;
            type.isToManyAssoziation = dvgItemDTO.IsToManyAssoziation;
            type.length = dvgItemDTO.Length;
            if (dvgItemDTO.ListView != null) {
                var typeName = 'listViewDTO';
                type.listView = RADGeneratedModels[typeName](dvgItemDTO.ListView);
            }
            else {
                type.listView = null;
            }
            type.name = dvgItemDTO.Name;
            type.possiblePDEnumerationValues = {};
            if (dvgItemDTO.PossiblePDEnumerationValues != null) {
                for (var i26 in dvgItemDTO.PossiblePDEnumerationValues) {
                    if (dvgItemDTO.PossiblePDEnumerationValues.hasOwnProperty(i26)) {
                        type.possiblePDEnumerationValues[i26] = dvgItemDTO.PossiblePDEnumerationValues[i26];
                    }
                }
            }
            type.required = dvgItemDTO.Required;
            type.tooltip = dvgItemDTO.Tooltip;
            type.uiGenerator = dvgItemDTO.UIGenerator;
            type.unitErgName = dvgItemDTO.UnitErgName;
            return type;
        };
        RADGeneratedModels.detailViewGroupDTO = function (detailViewGroupDTO) {
            var type = {};
            type._type = 'detailViewGroupDTO';
            type.detailViewGroups = [];
            if (detailViewGroupDTO.DetailViewGroups != null) {
                for (var i17 in detailViewGroupDTO.DetailViewGroups) {
                    var typeName = 'detailViewGroupDTO';
                    type.detailViewGroups.push(RADGeneratedModels[typeName](detailViewGroupDTO.DetailViewGroups[i17]));
                }
            }
            type.ergName = detailViewGroupDTO.ErgName;
            type.iconUrl = detailViewGroupDTO.IconUrl;
            type.items = [];
            if (detailViewGroupDTO.Items != null) {
                for (var i18 in detailViewGroupDTO.Items) {
                    var typeName = 'dvgItemDTO';
                    type.items.push(RADGeneratedModels[typeName](detailViewGroupDTO.Items[i18]));
                }
            }
            type.name = detailViewGroupDTO.Name;
            type.tooltip = detailViewGroupDTO.Tooltip;
            type.uiGenerator = detailViewGroupDTO.UIGenerator;
            return type;
        };
        RADGeneratedModels.detailViewDTO = function (detailViewDTO) {
            var type = {};
            type._type = 'detailViewDTO';
            type.autoUpdateDerivedProperties = detailViewDTO.AutoUpdateDerivedProperties;
            type.derivedProperties = [];
            if (detailViewDTO.DerivedProperties != null) {
                for (var i15 in detailViewDTO.DerivedProperties) {
                    type.derivedProperties.push(detailViewDTO.DerivedProperties[i15]);
                }
            }
            type.detailViewGroups = [];
            if (detailViewDTO.DetailViewGroups != null) {
                for (var i16 in detailViewDTO.DetailViewGroups) {
                    var typeName = 'detailViewGroupDTO';
                    type.detailViewGroups.push(RADGeneratedModels[typeName](detailViewDTO.DetailViewGroups[i16]));
                }
            }
            type.detailViewID = detailViewDTO.DetailViewID;
            type.ergName = detailViewDTO.ErgName;
            type.iconUrl = detailViewDTO.IconUrl;
            type.name = detailViewDTO.Name;
            type.neededProperties = [];
            if (detailViewDTO.NeededProperties != null) {
                for (var i27 in detailViewDTO.NeededProperties) {
                    type.neededProperties.push(detailViewDTO.NeededProperties[i27]);
                }
            }
            type.operations = [];
            if (detailViewDTO.Operations != null) {
                for (var i28 in detailViewDTO.Operations) {
                    var typeName = 'operationDTO';
                    type.operations.push(RADGeneratedModels[typeName](detailViewDTO.Operations[i28]));
                }
            }
            type.tooltip = detailViewDTO.Tooltip;
            type.typeName = detailViewDTO.TypeName;
            type.uiListener = detailViewDTO.UIListener;
            return type;
        };
        RADGeneratedModels.viewDTO = function (viewDTO) {
            var type = {};
            type._type = 'viewDTO';
            type.ergName = viewDTO.ErgName;
            type.iconUrl = viewDTO.IconUrl;
            type.name = viewDTO.Name;
            type.operations = [];
            if (viewDTO.Operations != null) {
                for (var i14 in viewDTO.Operations) {
                    var typeName = 'operationDTO';
                    type.operations.push(RADGeneratedModels[typeName](viewDTO.Operations[i14]));
                }
            }
            type.tooltip = viewDTO.Tooltip;
            type.typeName = viewDTO.TypeName;
            type.uiListener = viewDTO.UIListener;
            return type;
        };
        RADGeneratedModels.navigationNodeDTO = function (navigationNodeDTO) {
            var type = {};
            type._type = 'navigationNodeDTO';
            type.action = navigationNodeDTO.Action;
            type.childNodes = [];
            if (navigationNodeDTO.ChildNodes != null) {
                for (var i33 in navigationNodeDTO.ChildNodes) {
                    var typeName = 'navigationNodeDTO';
                    type.childNodes.push(RADGeneratedModels[typeName](navigationNodeDTO.ChildNodes[i33]));
                }
            }
            type.ergName = navigationNodeDTO.ErgName;
            type.iconUrl = navigationNodeDTO.IconUrl;
            type.isListView = navigationNodeDTO.IsListView;
            type.name = navigationNodeDTO.Name;
            type.pdTypeName = navigationNodeDTO.PDTypeName;
            type.tooltip = navigationNodeDTO.Tooltip;
            type.url = navigationNodeDTO.Url;
            type.viewExtensionFactory = navigationNodeDTO.ViewExtensionFactory;
            type.viewPDOID = navigationNodeDTO.ViewPDO_ID;
            return type;
        };
        RADGeneratedModels.deletionItemDTO = function (deletionItemDTO) {
            var type = {};
            type._type = 'deletionItemDTO';
            type.ergName = deletionItemDTO.ErgName;
            type.ergNamePluralized = deletionItemDTO.ErgNamePluralized;
            type.iconUrl = deletionItemDTO.IconUrl;
            type.name = deletionItemDTO.Name;
            type.objects = [];
            if (deletionItemDTO.Objects != null) {
                for (var i34 in deletionItemDTO.Objects) {
                    type.objects.push(deletionItemDTO.Objects[i34]);
                }
            }
            type.tooltip = deletionItemDTO.Tooltip;
            return type;
        };
        RADGeneratedModels.metadataDTO = function (metadataDTO) {
            var type = {};
            type._type = 'metadataDTO';
            type.ergName = metadataDTO.ErgName;
            type.iconUrl = metadataDTO.IconUrl;
            type.name = metadataDTO.Name;
            type.tooltip = metadataDTO.Tooltip;
            return type;
        };
        RADGeneratedModels.menuDTO = function (menuDTO) {
            var type = {};
            type._type = 'menuDTO';
            if (menuDTO.UserFavoriteTree != null) {
                var typeName = 'navigationNodeDTO';
                type.userFavoriteTree = RADGeneratedModels[typeName](menuDTO.UserFavoriteTree);
            }
            else {
                type.userFavoriteTree = null;
            }
            if (menuDTO.UserMenuTree != null) {
                var typeName = 'navigationNodeDTO';
                type.userMenuTree = RADGeneratedModels[typeName](menuDTO.UserMenuTree);
            }
            else {
                type.userMenuTree = null;
            }
            if (menuDTO.UserNavigationTree != null) {
                var typeName = 'navigationNodeDTO';
                type.userNavigationTree = RADGeneratedModels[typeName](menuDTO.UserNavigationTree);
            }
            else {
                type.userNavigationTree = null;
            }
            return type;
        };
        RADGeneratedModels.userDTO = function (userDTO) {
            var type = {};
            type._type = 'userDTO';
            type.additionalLoginInformations = {};
            if (userDTO.AdditionalLoginInformations != null) {
                for (var i36 in userDTO.AdditionalLoginInformations) {
                    if (userDTO.AdditionalLoginInformations.hasOwnProperty(i36)) {
                        type.additionalLoginInformations[i36] = userDTO.AdditionalLoginInformations[i36];
                    }
                }
            }
            type.changePasswordWithNextLogin = userDTO.ChangePasswordWithNextLogin;
            type.firstName = userDTO.FirstName;
            type.isLoginWithoutPrincipal = userDTO.IsLoginWithoutPrincipal;
            type.isSuperUser = userDTO.IsSuperUser;
            type.languageKey = userDTO.LanguageKey;
            type.lastName = userDTO.LastName;
            type.pdoId = userDTO.PDO_ID;
            type.principal = userDTO.Principal;
            type.roles = [];
            if (userDTO.Roles != null) {
                for (var i37 in userDTO.Roles) {
                    type.roles.push(userDTO.Roles[i37]);
                }
            }
            return type;
        };
        RADGeneratedModels.clientInfoDTO = function (clientInfoDTO) {
            var type = {};
            type._type = 'clientInfoDTO';
            type.id = clientInfoDTO.ID;
            type.language = clientInfoDTO.Language;
            if (clientInfoDTO.User != null) {
                var typeName = 'userDTO';
                type.user = RADGeneratedModels[typeName](clientInfoDTO.User);
            }
            else {
                type.user = null;
            }
            return type;
        };
        RADGeneratedModels.clientUpdateLevelDTO = function (index) {
            var values = [];
            values.push('Full');
            values.push('OnlyMenus');
            values.push('OnlyUserInformation');
            values.push('OnlyApplicationInformation');
            return values[index];
        };
        RADGeneratedModels.principalDTO = function (principalDTO) {
            var type = {};
            type._type = 'principalDTO';
            type.defaultCurrencyID = principalDTO.DefaultCurrencyID;
            type.fullName = principalDTO.FullName;
            type.logoUrl = principalDTO.LogoUrl;
            type.name = principalDTO.Name;
            type.pdoId = principalDTO.PDO_ID;
            return type;
        };
        RADGeneratedModels.clientUpdateDTO = function (clientUpdateDTO) {
            var type = {};
            type._type = 'clientUpdateDTO';
            if (clientUpdateDTO.ApplicationMenus != null) {
                var typeName = 'menuDTO';
                type.applicationMenus = RADGeneratedModels[typeName](clientUpdateDTO.ApplicationMenus);
            }
            else {
                type.applicationMenus = null;
            }
            if (clientUpdateDTO.ClientInfo != null) {
                var typeName = 'clientInfoDTO';
                type.clientInfo = RADGeneratedModels[typeName](clientUpdateDTO.ClientInfo);
            }
            else {
                type.clientInfo = null;
            }
            if (clientUpdateDTO.Configuration != null) {
                var typeName = 'configurationDTO';
                type.configuration = RADGeneratedModels[typeName](clientUpdateDTO.Configuration);
            }
            else {
                type.configuration = null;
            }
            type.errorMessage = clientUpdateDTO.ErrorMessage;
            type.isModified = clientUpdateDTO.IsModified;
            type.level = RADGeneratedModels.clientUpdateLevelDTO(clientUpdateDTO.Level);
            if (clientUpdateDTO.PrincipalInfo != null) {
                var typeName = 'principalDTO';
                type.principalInfo = RADGeneratedModels[typeName](clientUpdateDTO.PrincipalInfo);
            }
            else {
                type.principalInfo = null;
            }
            type.returnType = clientUpdateDTO.ReturnType;
            type.returnValue = clientUpdateDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(clientUpdateDTO.Status);
            return type;
        };
        RADGeneratedModels.searchObjectPositionResponseDTO = function (searchObjectPositionResponseDTO) {
            var type = {};
            type._type = 'searchObjectPositionResponseDTO';
            type.pageID = searchObjectPositionResponseDTO.PageID;
            type.rowNumber = searchObjectPositionResponseDTO.RowNumber;
            return type;
        };
        RADGeneratedModels.iteratorDTO = function (iteratorDTO) {
            var type = {};
            type._type = 'iteratorDTO';
            type.errorMessage = iteratorDTO.ErrorMessage;
            type.extentCount = iteratorDTO.ExtentCount;
            type.id = iteratorDTO.ID;
            type.isModified = iteratorDTO.IsModified;
            type.pdTypeName = iteratorDTO.PDTypeName;
            type.returnHistoricalData = iteratorDTO.ReturnHistoricalData;
            type.returnType = iteratorDTO.ReturnType;
            type.returnValue = iteratorDTO.ReturnValue;
            if (iteratorDTO.SearchObjectPositionResponse != null) {
                var typeName = 'searchObjectPositionResponseDTO';
                type.searchObjectPositionResponse = RADGeneratedModels[typeName](iteratorDTO.SearchObjectPositionResponse);
            }
            else {
                type.searchObjectPositionResponse = null;
            }
            type.status = RADGeneratedModels.statusEnumDTO(iteratorDTO.Status);
            return type;
        };
        RADGeneratedModels.genericDTO = function (genericDTO) {
            var type = {};
            type._type = 'genericDTO';
            type.currentHistoryPDOID = genericDTO.CurrentHistoryPDO_ID;
            type.errorMessage = genericDTO.ErrorMessage;
            type.historizationDate = genericDTO.HistorizationDate;
            type.historizedAssemblyVersion = genericDTO.HistorizedAssemblyVersion;
            type.isHistorized = genericDTO.IsHistorized;
            type.isModified = genericDTO.IsModified;
            type.isPrincipalIndependent = genericDTO.IsPrincipalIndependent;
            type.isSingleton = genericDTO.IsSingleton;
            type.objectIdentValue = genericDTO.ObjectIdentValue;
            type.pdOChanged = genericDTO.PDO_Changed;
            type.pdOCreated = genericDTO.PDO_Created;
            type.pdoId = genericDTO.PDO_ID;
            type.pdTypeName = genericDTO.PDTypeName;
            type.principalName = genericDTO.PrincipalName;
            type.returnType = genericDTO.ReturnType;
            type.returnValue = genericDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(genericDTO.Status);
            type.values = {};
            if (genericDTO.Values != null) {
                for (var i41 in genericDTO.Values) {
                    if (genericDTO.Values.hasOwnProperty(i41)) {
                        type.values[i41] = genericDTO.Values[i41];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.detailViewContextDTO = function (detailViewContextDTO) {
            var type = {};
            type._type = 'detailViewContextDTO';
            type.assoziationGridIterators = {};
            if (detailViewContextDTO.AssoziationGridIterators != null) {
                for (var i38 in detailViewContextDTO.AssoziationGridIterators) {
                    if (detailViewContextDTO.AssoziationGridIterators.hasOwnProperty(i38)) {
                        type.assoziationGridIterators[i38] = RADGeneratedModels.iteratorDTO(detailViewContextDTO.AssoziationGridIterators[i38]);
                    }
                }
            }
            type.contextParameters = {};
            if (detailViewContextDTO.ContextParameters != null) {
                for (var i39 in detailViewContextDTO.ContextParameters) {
                    if (detailViewContextDTO.ContextParameters.hasOwnProperty(i39)) {
                        type.contextParameters[i39] = detailViewContextDTO.ContextParameters[i39];
                    }
                }
            }
            if (detailViewContextDTO.DetailView != null) {
                var typeName = 'detailViewDTO';
                type.detailView = RADGeneratedModels[typeName](detailViewContextDTO.DetailView);
            }
            else {
                type.detailView = null;
            }
            type.errorMessage = detailViewContextDTO.ErrorMessage;
            type.hasWriteRight = detailViewContextDTO.HasWriteRight;
            type.id = detailViewContextDTO.ID;
            type.isModified = detailViewContextDTO.IsModified;
            type.isNewObject = detailViewContextDTO.IsNewObject;
            type.lockAquired = detailViewContextDTO.LockAquired;
            type.lockedByUser = detailViewContextDTO.LockedByUser;
            type.lockedObjectIdent = detailViewContextDTO.LockedObjectIdent;
            type.lockGroupID = detailViewContextDTO.LockGroupID;
            type.nestedDetailViewContexts = {};
            if (detailViewContextDTO.NestedDetailViewContexts != null) {
                for (var i40 in detailViewContextDTO.NestedDetailViewContexts) {
                    if (detailViewContextDTO.NestedDetailViewContexts.hasOwnProperty(i40)) {
                        type.nestedDetailViewContexts[i40] = RADGeneratedModels.detailViewContextDTO(detailViewContextDTO.NestedDetailViewContexts[i40]);
                    }
                }
            }
            type.persistenceContextID = detailViewContextDTO.PersistenceContextID;
            type.relationNameFromPreviousObject = detailViewContextDTO.RelationNameFromPreviousObject;
            type.returnType = detailViewContextDTO.ReturnType;
            type.returnValue = detailViewContextDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(detailViewContextDTO.Status);
            if (detailViewContextDTO.Subject != null) {
                var typeName = 'genericDTO';
                type.subject = RADGeneratedModels[typeName](detailViewContextDTO.Subject);
            }
            else {
                type.subject = null;
            }
            return type;
        };
        RADGeneratedModels.imageSpriteDTO = function (imageSpriteDTO) {
            var type = {};
            type._type = 'imageSpriteDTO';
            type.errorMessage = imageSpriteDTO.ErrorMessage;
            type.imageMap = {};
            if (imageSpriteDTO.ImageMap != null) {
                for (var i42 in imageSpriteDTO.ImageMap) {
                    if (imageSpriteDTO.ImageMap.hasOwnProperty(i42)) {
                        type.imageMap[i42] = imageSpriteDTO.ImageMap[i42];
                    }
                }
            }
            type.isModified = imageSpriteDTO.IsModified;
            type.returnType = imageSpriteDTO.ReturnType;
            type.returnValue = imageSpriteDTO.ReturnValue;
            type.spriteURL = imageSpriteDTO.SpriteURL;
            type.status = RADGeneratedModels.statusEnumDTO(imageSpriteDTO.Status);
            type.svgMap = {};
            if (imageSpriteDTO.SvgMap != null) {
                for (var i43 in imageSpriteDTO.SvgMap) {
                    if (imageSpriteDTO.SvgMap.hasOwnProperty(i43)) {
                        type.svgMap[i43] = imageSpriteDTO.SvgMap[i43];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.inlineEditingContextDTO = function (inlineEditingContextDTO) {
            var type = {};
            type._type = 'inlineEditingContextDTO';
            if (inlineEditingContextDTO.DetailView != null) {
                var typeName = 'detailViewDTO';
                type.detailView = RADGeneratedModels[typeName](inlineEditingContextDTO.DetailView);
            }
            else {
                type.detailView = null;
            }
            type.detailViewContextID = inlineEditingContextDTO.DetailViewContextID;
            type.errorMessage = inlineEditingContextDTO.ErrorMessage;
            type.hasCreateRight = inlineEditingContextDTO.HasCreateRight;
            type.hasWriteRight = inlineEditingContextDTO.HasWriteRight;
            type.isModified = inlineEditingContextDTO.IsModified;
            type.lockGroupID = inlineEditingContextDTO.LockGroupID;
            type.persistenceContextID = inlineEditingContextDTO.PersistenceContextID;
            type.returnType = inlineEditingContextDTO.ReturnType;
            type.returnValue = inlineEditingContextDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(inlineEditingContextDTO.Status);
            type.subjects = [];
            if (inlineEditingContextDTO.Subjects != null) {
                for (var i44 in inlineEditingContextDTO.Subjects) {
                    var typeName = 'genericDTO';
                    type.subjects.push(RADGeneratedModels[typeName](inlineEditingContextDTO.Subjects[i44]));
                }
            }
            return type;
        };
        RADGeneratedModels.operationResultDTO = function (operationResultDTO) {
            var type = {};
            type._type = 'operationResultDTO';
            type.contextParameters = {};
            if (operationResultDTO.ContextParameters != null) {
                for (var i46 in operationResultDTO.ContextParameters) {
                    if (operationResultDTO.ContextParameters.hasOwnProperty(i46)) {
                        type.contextParameters[i46] = operationResultDTO.ContextParameters[i46];
                    }
                }
            }
            type.errorMessage = operationResultDTO.ErrorMessage;
            type.isModified = operationResultDTO.IsModified;
            type.isValid = operationResultDTO.IsValid;
            type.returnType = operationResultDTO.ReturnType;
            type.returnValue = operationResultDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(operationResultDTO.Status);
            if (operationResultDTO.Subject != null) {
                var typeName = 'genericDTO';
                type.subject = RADGeneratedModels[typeName](operationResultDTO.Subject);
            }
            else {
                type.subject = null;
            }
            type.validationMessages = {};
            if (operationResultDTO.ValidationMessages != null) {
                for (var i47 in operationResultDTO.ValidationMessages) {
                    if (operationResultDTO.ValidationMessages.hasOwnProperty(i47)) {
                        type.validationMessages[i47] = operationResultDTO.ValidationMessages[i47];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.saveResultDTO = function (saveResultDTO) {
            var type = {};
            type._type = 'saveResultDTO';
            type.errorMessage = saveResultDTO.ErrorMessage;
            type.isModified = saveResultDTO.IsModified;
            type.isValid = saveResultDTO.IsValid;
            type.returnType = saveResultDTO.ReturnType;
            type.returnValue = saveResultDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(saveResultDTO.Status);
            type.validationMessages = {};
            if (saveResultDTO.ValidationMessages != null) {
                for (var i48 in saveResultDTO.ValidationMessages) {
                    if (saveResultDTO.ValidationMessages.hasOwnProperty(i48)) {
                        type.validationMessages[i48] = saveResultDTO.ValidationMessages[i48];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.updateDTO = function (updateDTO) {
            var type = {};
            type._type = 'updateDTO';
            type.errorMessage = updateDTO.ErrorMessage;
            type.isModified = updateDTO.IsModified;
            type.isValid = updateDTO.IsValid;
            type.refreshProperties = {};
            if (updateDTO.RefreshProperties != null) {
                for (var i49 in updateDTO.RefreshProperties) {
                    if (updateDTO.RefreshProperties.hasOwnProperty(i49)) {
                        type.refreshProperties[i49] = updateDTO.RefreshProperties[i49];
                    }
                }
            }
            type.returnType = updateDTO.ReturnType;
            type.returnValue = updateDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(updateDTO.Status);
            type.validationMessages = {};
            if (updateDTO.ValidationMessages != null) {
                for (var i50 in updateDTO.ValidationMessages) {
                    if (updateDTO.ValidationMessages.hasOwnProperty(i50)) {
                        type.validationMessages[i50] = updateDTO.ValidationMessages[i50];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.validationDTO = function (validationDTO) {
            var type = {};
            type._type = 'validationDTO';
            type.errorMessage = validationDTO.ErrorMessage;
            type.isModified = validationDTO.IsModified;
            type.isValid = validationDTO.IsValid;
            type.returnType = validationDTO.ReturnType;
            type.returnValue = validationDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(validationDTO.Status);
            type.validationMessages = {};
            if (validationDTO.ValidationMessages != null) {
                for (var i45 in validationDTO.ValidationMessages) {
                    if (validationDTO.ValidationMessages.hasOwnProperty(i45)) {
                        type.validationMessages[i45] = validationDTO.ValidationMessages[i45];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.historizationPointDTO = function (historizationPointDTO) {
            var type = {};
            type._type = 'historizationPointDTO';
            type.historizationDate = historizationPointDTO.HistorizationDate;
            type.pdoId = historizationPointDTO.PDO_ID;
            type.title = historizationPointDTO.Title;
            return type;
        };
        RADGeneratedModels.historyInformationDTO = function (historyInformationDTO) {
            var type = {};
            type._type = 'historyInformationDTO';
            type.historizableObjectPDOID = historyInformationDTO.HistorizableObjectPDO_ID;
            if (historyInformationDTO.HistoryDate != null) {
                var typeName = 'historizationPointDTO';
                type.historyDate = RADGeneratedModels[typeName](historyInformationDTO.HistoryDate);
            }
            else {
                type.historyDate = null;
            }
            return type;
        };
        RADGeneratedModels.objectHistoryInformationsDTO = function (objectHistoryInformationsDTO) {
            var type = {};
            type._type = 'objectHistoryInformationsDTO';
            type.errorMessage = objectHistoryInformationsDTO.ErrorMessage;
            type.historyInformation = [];
            if (objectHistoryInformationsDTO.HistoryInformation != null) {
                for (var i53 in objectHistoryInformationsDTO.HistoryInformation) {
                    var typeName = 'historyInformationDTO';
                    type.historyInformation.push(RADGeneratedModels[typeName](objectHistoryInformationsDTO.HistoryInformation[i53]));
                }
            }
            type.isCurrentDeleted = objectHistoryInformationsDTO.IsCurrentDeleted;
            type.isHistorizableType = objectHistoryInformationsDTO.IsHistorizableType;
            type.isModified = objectHistoryInformationsDTO.IsModified;
            type.isTargetHistoryObject = objectHistoryInformationsDTO.IsTargetHistoryObject;
            type.returnType = objectHistoryInformationsDTO.ReturnType;
            type.returnValue = objectHistoryInformationsDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(objectHistoryInformationsDTO.Status);
            type.targetHistoriziationPointPdoID = objectHistoryInformationsDTO.TargetHistoriziationPointPdoID;
            return type;
        };
        RADGeneratedModels.summaryViewContextDTO = function (summaryViewContextDTO) {
            var type = {};
            type._type = 'summaryViewContextDTO';
            if (summaryViewContextDTO.DetailViewContext != null) {
                var typeName = 'detailViewContextDTO';
                type.detailViewContext = RADGeneratedModels[typeName](summaryViewContextDTO.DetailViewContext);
            }
            else {
                type.detailViewContext = null;
            }
            type.errorMessage = summaryViewContextDTO.ErrorMessage;
            type.isModified = summaryViewContextDTO.IsModified;
            if (summaryViewContextDTO.ObjectHistoryInformations != null) {
                var typeName = 'objectHistoryInformationsDTO';
                type.objectHistoryInformations = RADGeneratedModels[typeName](summaryViewContextDTO.ObjectHistoryInformations);
            }
            else {
                type.objectHistoryInformations = null;
            }
            type.returnType = summaryViewContextDTO.ReturnType;
            type.returnValue = summaryViewContextDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(summaryViewContextDTO.Status);
            return type;
        };
        RADGeneratedModels.tryDeletionDTO = function (tryDeletionDTO) {
            var type = {};
            type._type = 'tryDeletionDTO';
            type.errorMessage = tryDeletionDTO.ErrorMessage;
            type.isModified = tryDeletionDTO.IsModified;
            type.objectsToDelete = [];
            if (tryDeletionDTO.ObjectsToDelete != null) {
                for (var i54 in tryDeletionDTO.ObjectsToDelete) {
                    var typeName = 'deletionItemDTO';
                    type.objectsToDelete.push(RADGeneratedModels[typeName](tryDeletionDTO.ObjectsToDelete[i54]));
                }
            }
            type.returnType = tryDeletionDTO.ReturnType;
            type.returnValue = tryDeletionDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(tryDeletionDTO.Status);
            return type;
        };
        RADGeneratedModels.updateAndValidateDTO = function (updateAndValidateDTO) {
            var type = {};
            type._type = 'updateAndValidateDTO';
            type.errorMessage = updateAndValidateDTO.ErrorMessage;
            type.isModified = updateAndValidateDTO.IsModified;
            type.returnType = updateAndValidateDTO.ReturnType;
            type.returnValue = updateAndValidateDTO.ReturnValue;
            if (updateAndValidateDTO.SaveResultDTO != null) {
                var typeName = 'saveResultDTO';
                type.saveResultDTO = RADGeneratedModels[typeName](updateAndValidateDTO.SaveResultDTO);
            }
            else {
                type.saveResultDTO = null;
            }
            type.status = RADGeneratedModels.statusEnumDTO(updateAndValidateDTO.Status);
            if (updateAndValidateDTO.UpdateDTO != null) {
                var typeName = 'updateDTO';
                type.updateDTO = RADGeneratedModels[typeName](updateAndValidateDTO.UpdateDTO);
            }
            else {
                type.updateDTO = null;
            }
            return type;
        };
        RADGeneratedModels.resultDTO = function (resultDTO) {
            var type = {};
            type._type = 'resultDTO';
            type.errorMessage = resultDTO.ErrorMessage;
            type.isModified = resultDTO.IsModified;
            type.returnType = resultDTO.ReturnType;
            type.returnValue = resultDTO.ReturnValue;
            type.status = RADGeneratedModels.statusEnumDTO(resultDTO.Status);
            return type;
        };
        RADGeneratedModels.changePasswordResponseDTO = function (changePasswordResponseDTO) {
            var type = {};
            type._type = 'changePasswordResponseDTO';
            type.messages = {};
            if (changePasswordResponseDTO.Messages != null) {
                for (var i56 in changePasswordResponseDTO.Messages) {
                    if (changePasswordResponseDTO.Messages.hasOwnProperty(i56)) {
                        type.messages[i56] = changePasswordResponseDTO.Messages[i56];
                    }
                }
            }
            type.successfull = changePasswordResponseDTO.Successfull;
            return type;
        };
        RADGeneratedModels.searchObjectPositionRequestDTO = function (searchObjectPositionRequestDTO) {
            var type = {};
            type._type = 'searchObjectPositionRequestDTO';
            type.currentPagingSize = searchObjectPositionRequestDTO.CurrentPagingSize;
            type.pdoIDToFind = searchObjectPositionRequestDTO.PdoIDToFind;
            return type;
        };
        RADGeneratedModels.pdIteratorOperatorDTO = function (pdIteratorOperatorDTO) {
            var type = {};
            type._type = 'pdIteratorOperatorDTO';
            type.disablePrincipalFilter = pdIteratorOperatorDTO.DisablePrincipalFilter;
            type.historizationDates = [];
            if (pdIteratorOperatorDTO.HistorizationDates != null) {
                for (var i57 in pdIteratorOperatorDTO.HistorizationDates) {
                    type.historizationDates.push(pdIteratorOperatorDTO.HistorizationDates[i57]);
                }
            }
            type.includeAllPrincipals = pdIteratorOperatorDTO.IncludeAllPrincipals;
            type.includeBasePrincipals = pdIteratorOperatorDTO.IncludeBasePrincipals;
            type.includePrincipalFromSubject = pdIteratorOperatorDTO.IncludePrincipalFromSubject;
            type.includeSubPrincipals = pdIteratorOperatorDTO.IncludeSubPrincipals;
            type.memoryOptimizationEnabled = pdIteratorOperatorDTO.MemoryOptimizationEnabled;
            type.onlySubPrincipals = pdIteratorOperatorDTO.OnlySubPrincipals;
            type.principalFilter = pdIteratorOperatorDTO.PrincipalFilter;
            if (pdIteratorOperatorDTO.SearchObjectPositionRequest != null) {
                var typeName = 'searchObjectPositionRequestDTO';
                type.searchObjectPositionRequest = RADGeneratedModels[typeName](pdIteratorOperatorDTO.SearchObjectPositionRequest);
            }
            else {
                type.searchObjectPositionRequest = null;
            }
            type.showCurrentObjects = pdIteratorOperatorDTO.ShowCurrentObjects;
            type.showDeletedObjects = pdIteratorOperatorDTO.ShowDeletedObjects;
            type.showHistoricedObjects = pdIteratorOperatorDTO.ShowHistoricedObjects;
            type.showObjectsWithoutPrincipal = pdIteratorOperatorDTO.ShowObjectsWithoutPrincipal;
            type.visibleDeletedObjects = [];
            if (pdIteratorOperatorDTO.VisibleDeletedObjects != null) {
                for (var i58 in pdIteratorOperatorDTO.VisibleDeletedObjects) {
                    type.visibleDeletedObjects.push(pdIteratorOperatorDTO.VisibleDeletedObjects[i58]);
                }
            }
            return type;
        };
        RADGeneratedModels.exportType = function (index) {
            var values = [];
            values.push('XLS');
            values.push('CSV');
            values.push('PDF');
            return values[index];
        };
        RADGeneratedModels.searchParameterDTO = function (searchParameterDTO) {
            var type = {};
            type._type = 'searchParameterDTO';
            type.extensionModules = [];
            if (searchParameterDTO.ExtensionModules != null) {
                for (var i59 in searchParameterDTO.ExtensionModules) {
                    var typeName = 'extensionModuleDTO';
                    type.extensionModules.push(RADGeneratedModels[typeName](searchParameterDTO.ExtensionModules[i59]));
                }
            }
            type.historizationPoints = [];
            if (searchParameterDTO.HistorizationPoints != null) {
                for (var i60 in searchParameterDTO.HistorizationPoints) {
                    var typeName = 'historizationPointDTO';
                    type.historizationPoints.push(RADGeneratedModels[typeName](searchParameterDTO.HistorizationPoints[i60]));
                }
            }
            return type;
        };
        RADGeneratedModels.taskDTO = function (taskDTO) {
            var type = {};
            type._type = 'taskDTO';
            type.description = taskDTO.Description;
            type.dueDate = taskDTO.DueDate;
            type.pdoId = taskDTO.PDO_ID;
            type.pdTypeName = taskDTO.PDTypeName;
            type.title = taskDTO.Title;
            return type;
        };
        RADGeneratedModels.taskInformationDTO = function (taskInformationDTO) {
            var type = {};
            type._type = 'taskInformationDTO';
            type.allAssignedTasksCount = taskInformationDTO.AllAssignedTasksCount;
            type.allTasksCount = taskInformationDTO.AllTasksCount;
            type.assignedTopTasks = [];
            if (taskInformationDTO.AssignedTopTasks != null) {
                for (var i61 in taskInformationDTO.AssignedTopTasks) {
                    var typeName = 'taskDTO';
                    type.assignedTopTasks.push(RADGeneratedModels[typeName](taskInformationDTO.AssignedTopTasks[i61]));
                }
            }
            type.taskListViewId = taskInformationDTO.TaskListViewId;
            return type;
        };
        RADGeneratedModels.searchEntryDTO = function (searchEntryDTO) {
            var type = {};
            type._type = 'searchEntryDTO';
            type.alias = searchEntryDTO.Alias;
            type.historizationDate = searchEntryDTO.HistorizationDate;
            type.historizationGroupPDO_ID = searchEntryDTO.HistorizationGroup_PDO_ID;
            type.isHistorized = searchEntryDTO.IsHistorized;
            type.objectIdentValue = searchEntryDTO.ObjectIdentValue;
            type.pdOChanged = searchEntryDTO.PDO_Changed;
            type.pdOChangedBy = searchEntryDTO.PDO_ChangedBy;
            type.pdOCreated = searchEntryDTO.PDO_Created;
            type.pdOCreatedBy = searchEntryDTO.PDO_CreatedBy;
            type.pdoId = searchEntryDTO.PDO_ID;
            type.principal = searchEntryDTO.Principal;
            type.typeErgName = searchEntryDTO.TypeErgName;
            type.typeName = searchEntryDTO.TypeName;
            return type;
        };
        RADGeneratedModels.searchResultDTO = function (searchResultDTO) {
            var type = {};
            type._type = 'searchResultDTO';
            type.entries = [];
            if (searchResultDTO.Entries != null) {
                for (var i62 in searchResultDTO.Entries) {
                    var typeName = 'searchEntryDTO';
                    type.entries.push(RADGeneratedModels[typeName](searchResultDTO.Entries[i62]));
                }
            }
            type.extentCount = searchResultDTO.ExtentCount;
            return type;
        };
        RADGeneratedModels.searchQueryDTO = function (searchQueryDTO) {
            var type = {};
            type._type = 'searchQueryDTO';
            type.extensionModules = [];
            if (searchQueryDTO.ExtensionModules != null) {
                for (var i63 in searchQueryDTO.ExtensionModules) {
                    var typeName = 'extensionModuleDTO';
                    type.extensionModules.push(RADGeneratedModels[typeName](searchQueryDTO.ExtensionModules[i63]));
                }
            }
            type.from = searchQueryDTO.From;
            type.historizationPointPDOID = searchQueryDTO.HistorizationPointPDO_ID;
            type.includeHistory = searchQueryDTO.IncludeHistory;
            type.pageSize = searchQueryDTO.PageSize;
            type.query = searchQueryDTO.Query;
            type.reverseSort = searchQueryDTO.ReverseSort;
            type.searchInAllPrincipals = searchQueryDTO.SearchInAllPrincipals;
            if (searchQueryDTO.SelectedType != null) {
                var typeName = 'typeMetaDTO';
                type.selectedType = RADGeneratedModels[typeName](searchQueryDTO.SelectedType);
            }
            else {
                type.selectedType = null;
            }
            type.sortField = searchQueryDTO.SortField;
            type.to = searchQueryDTO.To;
            return type;
        };
        RADGeneratedModels.systemLockNotificationDTO = function (systemLockNotificationDTO) {
            var type = {};
            type._type = 'systemLockNotificationDTO';
            type.isUserOnExceptionList = systemLockNotificationDTO.IsUserOnExceptionList;
            type.lockEnd = systemLockNotificationDTO.LockEnd;
            type.lockStart = systemLockNotificationDTO.LockStart;
            type.notificationPDOID = systemLockNotificationDTO.Notification_PDOID;
            type.notificationMessage = systemLockNotificationDTO.NotificationMessage;
            type.preNotification = systemLockNotificationDTO.PreNotification;
            return type;
        };
        RADGeneratedModels.notificationDTO = function (notificationDTO) {
            var type = {};
            type._type = 'notificationDTO';
            type.notificationPDOID = notificationDTO.Notification_PDOID;
            type.notificationMessage = notificationDTO.NotificationMessage;
            return type;
        };
        RADGeneratedModels.keepAliveDTO = function (keepAliveDTO) {
            var type = {};
            type._type = 'keepAliveDTO';
            type.lastCacheUpdate = keepAliveDTO.LastCacheUpdate;
            type.lastRightCacheUpdate = keepAliveDTO.LastRightCacheUpdate;
            type.notifications = [];
            if (keepAliveDTO.Notifications != null) {
                for (var i64 in keepAliveDTO.Notifications) {
                    var typeName = 'notificationDTO';
                    var hasAllProperties = true;
                    for (var property in keepAliveDTO.Notifications[i64]) {
                        if (RADGeneratedModels.notificationDTOProperties.indexOf(property) < 0) {
                            hasAllProperties = false;
                            break;
                        }
                    }
                    if (hasAllProperties === false) {
                        if (hasAllProperties === false) {
                            var thisHasAllProperties = true;
                            for (var property in keepAliveDTO.Notifications[i64]) {
                                if (RADGeneratedModels.systemLockNotificationDTOProperties.indexOf(property) < 0) {
                                    thisHasAllProperties = false;
                                    break;
                                }
                            }
                            if (thisHasAllProperties === true) {
                                typeName = 'systemLockNotificationDTO';
                            }
                            hasAllProperties = thisHasAllProperties;
                        }
                    }
                    type.notifications.push(RADGeneratedModels[typeName](keepAliveDTO.Notifications[i64]));
                }
            }
            type.utcNow = keepAliveDTO.UtcNow;
            return type;
        };
        RADGeneratedModels.loginDTO = function (loginDTO) {
            var type = {};
            type._type = 'loginDTO';
            if (loginDTO.ClientInfo != null) {
                var typeName = 'clientInfoDTO';
                type.clientInfo = RADGeneratedModels[typeName](loginDTO.ClientInfo);
            }
            else {
                type.clientInfo = null;
            }
            type.defaultPersistenceContext = loginDTO.DefaultPersistenceContext;
            type.isAuthorized = loginDTO.IsAuthorized;
            if (loginDTO.Menus != null) {
                var typeName = 'menuDTO';
                type.menus = RADGeneratedModels[typeName](loginDTO.Menus);
            }
            else {
                type.menus = null;
            }
            type.resultErgName = loginDTO.ResultErgName;
            type.rightCacheCreated = loginDTO.RightCacheCreated;
            if (loginDTO.Taskinformation != null) {
                var typeName = 'taskInformationDTO';
                type.taskinformation = RADGeneratedModels[typeName](loginDTO.Taskinformation);
            }
            else {
                type.taskinformation = null;
            }
            type.userSpecificData = {};
            if (loginDTO.UserSpecificData != null) {
                for (var i65 in loginDTO.UserSpecificData) {
                    if (loginDTO.UserSpecificData.hasOwnProperty(i65)) {
                        type.userSpecificData[i65] = loginDTO.UserSpecificData[i65];
                    }
                }
            }
            return type;
        };
        RADGeneratedModels.resultDTOProperties = ['ErrorMessage', 'IsModified', 'ReturnType', 'ReturnValue', 'Status'];
        RADGeneratedModels.changeObjectDTOProperties = ['ErrorMessage', 'IsModified', 'LockedBy', 'LockedObjectIdent', 'ReturnType', 'ReturnValue', 'Status'];
        RADGeneratedModels.metadataDTOProperties = ['ErgName', 'IconUrl', 'Name', 'Tooltip'];
        RADGeneratedModels.currencyDTOProperties = ['IsActive', 'ISO4217NumberCode', 'Rate', 'Scale', 'Shortname', 'Subcurrencies'];
        RADGeneratedModels.languageDTOProperties = ['CultureInfoName', 'CurrencyPattern', 'DatePattern', 'DisplayName', 'FloatingPrecision', 'IconUrl', 'ISO6391', 'TimePattern'];
        RADGeneratedModels.configurationDTOProperties = ['Currencies', 'CurrencyExchangePaths', 'DefaultCurrencyID', 'DefaultLanguage', 'Languages', 'PossibleAssoziationListViewPages', 'PossibleListViewPages', 'StartpageUIFactory', 'SystemProperties'];
        RADGeneratedModels.typeMetaDTOProperties = ['ErgName', 'IconUrl', 'Name', 'PDO_ID', 'Tooltip'];
        RADGeneratedModels.extensionModuleDTOProperties = ['Alias', 'ErgName', 'Title', 'Types'];
        RADGeneratedModels.applicationDTOProperties = ['Configuration', 'DeployedModules', 'Description', 'ErgName', 'ExpiredModules', 'IconUrl', 'IsSystemLocked', 'LastCacheUpdate', 'LicensedModules', 'Modules', 'Name', 'PDApplicationChangedTime', 'PDEnumerationValues', 'PrincipalTypeName', 'SystemLockMessage', 'TemplateStyle', 'Tooltip'];
        RADGeneratedModels.operationDTOProperties = ['ErgName', 'HasRight', 'IconUrl', 'IsNeedingParameters', 'Name', 'OperationGUID', 'ShowResult', 'Tooltip'];
        RADGeneratedModels.viewDTOProperties = ['ErgName', 'IconUrl', 'Name', 'Operations', 'Tooltip', 'TypeName', 'UIListener'];
        RADGeneratedModels.columnDTOProperties = ['DotNetTypeName', 'IsFilterable', 'IsVisible', 'ListViewColumnFactory', 'ListViewPDOID', 'MLSErgName', 'Name', 'PossiblePDEnumerationValues', 'Sortable', 'UIGenerator', 'UnitNames'];
        RADGeneratedModels.listViewDTOProperties = ['AssoziationListView', 'Columns', 'DefaultOrder', 'ErgName', 'ErgNamePluralized', 'Filter', 'IconUrl', 'IncludeAllPrincipals', 'IncludeBasePrincipals', 'IncludeSubPrincipals', 'IsPrincipalIndependent', 'ListViewID', 'MultiColumnSort', 'Name', 'Operations', 'RelationName', 'ShowButtonCreate', 'ShowButtonCSVExport', 'ShowButtonDelete', 'ShowButtonDisconnect', 'ShowButtonEdit', 'ShowButtonSelect', 'ShowButtonXLSExport', 'ShowFilter', 'ShowMoveButtons', 'StaticFilterExpression', 'SummaryViewAutoGenerating', 'SummaryViewParameters', 'SummaryViewUIFactory', 'Tooltip', 'TypeName', 'Types', 'UIListener'];
        RADGeneratedModels.dvgItemDTOProperties = ['Children', 'DisableNavigation', 'DotNetTypeName', 'ErgName', 'GUIReadOnly', 'IconUrl', 'IsDerived', 'IsDerivedOverwritable', 'IsMultiSelectablePDEnumeration', 'IsPDEmbedded', 'IsPDEmbeddedMedium', 'IsPDEnumeration', 'IsToManyAssoziation', 'Length', 'ListView', 'Name', 'PossiblePDEnumerationValues', 'Required', 'Tooltip', 'UIGenerator', 'UnitErgName'];
        RADGeneratedModels.detailViewGroupDTOProperties = ['DetailViewGroups', 'ErgName', 'IconUrl', 'Items', 'Name', 'Tooltip', 'UIGenerator'];
        RADGeneratedModels.detailViewDTOProperties = ['AutoUpdateDerivedProperties', 'DerivedProperties', 'DetailViewGroups', 'DetailViewID', 'ErgName', 'IconUrl', 'Name', 'NeededProperties', 'Operations', 'Tooltip', 'TypeName', 'UIListener'];
        RADGeneratedModels.navigationNodeDTOProperties = ['Action', 'ChildNodes', 'ErgName', 'IconUrl', 'IsListView', 'Name', 'PDTypeName', 'Tooltip', 'Url', 'ViewExtensionFactory', 'ViewPDO_ID'];
        RADGeneratedModels.deletionItemDTOProperties = ['ErgName', 'ErgNamePluralized', 'IconUrl', 'Name', 'Objects', 'Tooltip'];
        RADGeneratedModels.menuDTOProperties = ['UserFavoriteTree', 'UserMenuTree', 'UserNavigationTree'];
        RADGeneratedModels.userDTOProperties = ['AdditionalLoginInformations', 'ChangePasswordWithNextLogin', 'FirstName', 'IsLoginWithoutPrincipal', 'IsSuperUser', 'LanguageKey', 'LastName', 'PDO_ID', 'Principal', 'Roles'];
        RADGeneratedModels.clientInfoDTOProperties = ['ID', 'Language', 'User'];
        RADGeneratedModels.principalDTOProperties = ['DefaultCurrencyID', 'FullName', 'LogoUrl', 'Name', 'PDO_ID'];
        RADGeneratedModels.clientUpdateDTOProperties = ['ApplicationMenus', 'ClientInfo', 'Configuration', 'ErrorMessage', 'IsModified', 'Level', 'PrincipalInfo', 'ReturnType', 'ReturnValue', 'Status'];
        RADGeneratedModels.searchObjectPositionResponseDTOProperties = ['PageID', 'RowNumber'];
        RADGeneratedModels.iteratorDTOProperties = ['ErrorMessage', 'ExtentCount', 'ID', 'IsModified', 'PDTypeName', 'ReturnHistoricalData', 'ReturnType', 'ReturnValue', 'SearchObjectPositionResponse', 'Status'];
        RADGeneratedModels.genericDTOProperties = ['CurrentHistoryPDO_ID', 'ErrorMessage', 'HistorizationDate', 'HistorizedAssemblyVersion', 'IsHistorized', 'IsModified', 'IsPrincipalIndependent', 'IsSingleton', 'ObjectIdentValue', 'PDO_Changed', 'PDO_Created', 'PDO_ID', 'PDTypeName', 'PrincipalName', 'ReturnType', 'ReturnValue', 'Status', 'Values'];
        RADGeneratedModels.detailViewContextDTOProperties = ['AssoziationGridIterators', 'ContextParameters', 'DetailView', 'ErrorMessage', 'HasWriteRight', 'ID', 'IsModified', 'IsNewObject', 'LockAquired', 'LockedByUser', 'LockedObjectIdent', 'LockGroupID', 'NestedDetailViewContexts', 'PersistenceContextID', 'RelationNameFromPreviousObject', 'ReturnType', 'ReturnValue', 'Status', 'Subject'];
        RADGeneratedModels.imageSpriteDTOProperties = ['ErrorMessage', 'ImageMap', 'IsModified', 'ReturnType', 'ReturnValue', 'SpriteURL', 'Status', 'SvgMap'];
        RADGeneratedModels.inlineEditingContextDTOProperties = ['DetailView', 'DetailViewContextID', 'ErrorMessage', 'HasCreateRight', 'HasWriteRight', 'IsModified', 'LockGroupID', 'PersistenceContextID', 'ReturnType', 'ReturnValue', 'Status', 'Subjects'];
        RADGeneratedModels.validationDTOProperties = ['ErrorMessage', 'IsModified', 'IsValid', 'ReturnType', 'ReturnValue', 'Status', 'ValidationMessages'];
        RADGeneratedModels.operationResultDTOProperties = ['ContextParameters', 'ErrorMessage', 'IsModified', 'IsValid', 'ReturnType', 'ReturnValue', 'Status', 'Subject', 'ValidationMessages'];
        RADGeneratedModels.saveResultDTOProperties = ['ErrorMessage', 'IsModified', 'IsValid', 'ReturnType', 'ReturnValue', 'Status', 'ValidationMessages'];
        RADGeneratedModels.updateDTOProperties = ['ErrorMessage', 'IsModified', 'IsValid', 'RefreshProperties', 'ReturnType', 'ReturnValue', 'Status', 'ValidationMessages'];
        RADGeneratedModels.historizationPointDTOProperties = ['HistorizationDate', 'PDO_ID', 'Title'];
        RADGeneratedModels.historyInformationDTOProperties = ['HistorizableObjectPDO_ID', 'HistoryDate'];
        RADGeneratedModels.objectHistoryInformationsDTOProperties = ['ErrorMessage', 'HistoryInformation', 'IsCurrentDeleted', 'IsHistorizableType', 'IsModified', 'IsTargetHistoryObject', 'ReturnType', 'ReturnValue', 'Status', 'TargetHistoriziationPointPdoID'];
        RADGeneratedModels.summaryViewContextDTOProperties = ['DetailViewContext', 'ErrorMessage', 'IsModified', 'ObjectHistoryInformations', 'ReturnType', 'ReturnValue', 'Status'];
        RADGeneratedModels.tryDeletionDTOProperties = ['ErrorMessage', 'IsModified', 'ObjectsToDelete', 'ReturnType', 'ReturnValue', 'Status'];
        RADGeneratedModels.updateAndValidateDTOProperties = ['ErrorMessage', 'IsModified', 'ReturnType', 'ReturnValue', 'SaveResultDTO', 'Status', 'UpdateDTO'];
        RADGeneratedModels.changePasswordResponseDTOProperties = ['Messages', 'Successfull'];
        RADGeneratedModels.searchObjectPositionRequestDTOProperties = ['CurrentPagingSize', 'PdoIDToFind'];
        RADGeneratedModels.pdIteratorOperatorDTOProperties = ['DisablePrincipalFilter', 'HistorizationDates', 'IncludeAllPrincipals', 'IncludeBasePrincipals', 'IncludePrincipalFromSubject', 'IncludeSubPrincipals', 'MemoryOptimizationEnabled', 'OnlySubPrincipals', 'PrincipalFilter', 'SearchObjectPositionRequest', 'ShowCurrentObjects', 'ShowDeletedObjects', 'ShowHistoricedObjects', 'ShowObjectsWithoutPrincipal', 'VisibleDeletedObjects'];
        RADGeneratedModels.searchParameterDTOProperties = ['ExtensionModules', 'HistorizationPoints'];
        RADGeneratedModels.taskDTOProperties = ['Description', 'DueDate', 'PDO_ID', 'PDTypeName', 'Title'];
        RADGeneratedModels.taskInformationDTOProperties = ['AllAssignedTasksCount', 'AllTasksCount', 'AssignedTopTasks', 'TaskListViewId'];
        RADGeneratedModels.searchEntryDTOProperties = ['Alias', 'HistorizationDate', 'HistorizationGroup_PDO_ID', 'IsHistorized', 'ObjectIdentValue', 'PDO_Changed', 'PDO_ChangedBy', 'PDO_Created', 'PDO_CreatedBy', 'PDO_ID', 'Principal', 'TypeErgName', 'TypeName'];
        RADGeneratedModels.searchResultDTOProperties = ['Entries', 'ExtentCount'];
        RADGeneratedModels.searchQueryDTOProperties = ['ExtensionModules', 'From', 'HistorizationPointPDO_ID', 'IncludeHistory', 'PageSize', 'Query', 'ReverseSort', 'SearchInAllPrincipals', 'SelectedType', 'SortField', 'To'];
        RADGeneratedModels.notificationDTOProperties = ['Notification_PDOID', 'NotificationMessage'];
        RADGeneratedModels.systemLockNotificationDTOProperties = ['IsUserOnExceptionList', 'LockEnd', 'LockStart', 'Notification_PDOID', 'NotificationMessage', 'PreNotification'];
        RADGeneratedModels.keepAliveDTOProperties = ['LastCacheUpdate', 'LastRightCacheUpdate', 'Notifications', 'UtcNow'];
        RADGeneratedModels.loginDTOProperties = ['ClientInfo', 'DefaultPersistenceContext', 'IsAuthorized', 'Menus', 'ResultErgName', 'RightCacheCreated', 'Taskinformation', 'UserSpecificData'];
        return RADGeneratedModels;
    }());
    return RADGeneratedModels;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUkFER2VuZXJhdGVkTW9kZWxzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUkFER2VuZXJhdGVkTW9kZWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBEQUEwRDs7O0lBRTFEOztPQUVHO0lBRUg7UUFBQTtRQTIvQ0EsQ0FBQztRQXA4Q2lCLGdDQUFhLEdBQTNCLFVBQTRCLEtBQWE7WUFDckMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBRWhCLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUU1QixPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDO1FBRWEsa0NBQWUsR0FBN0IsVUFBOEIsZUFBb0I7WUFDOUMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQWlCLENBQUM7WUFDL0IsSUFBSSxDQUFDLFlBQVksR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDLFVBQVUsQ0FBQztZQUM3QyxJQUFJLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUM7WUFDekMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQztZQUMzRCxJQUFJLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQyxVQUFVLENBQUM7WUFDN0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUMsV0FBVyxDQUFDO1lBQy9DLElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2RSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsOEJBQVcsR0FBekIsVUFBMEIsV0FBZ0I7WUFDdEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO1lBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQztZQUNyQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsV0FBVyxDQUFDLGlCQUFpQixDQUFDO1lBQ3ZELElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1lBRXhCLElBQUksV0FBVyxDQUFDLGFBQWEsSUFBSSxJQUFJLEVBQUU7Z0JBQ25DLEtBQUssSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLGFBQWEsRUFBRTtvQkFDdEMsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDeEY7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSw4QkFBVyxHQUF6QixVQUEwQixXQUFnQjtZQUN0QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7WUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDO1lBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsV0FBVyxDQUFDLGVBQWUsQ0FBQztZQUNuRCxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7WUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQzNDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxXQUFXLENBQUMsaUJBQWlCLENBQUM7WUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ25DLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7WUFDM0MsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLG1DQUFnQixHQUE5QixVQUErQixnQkFBcUI7WUFDaEQsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLENBQUM7WUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFFckIsSUFBSSxnQkFBZ0IsQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUNyQyxLQUFLLElBQUksRUFBRSxJQUFJLGdCQUFnQixDQUFDLFVBQVUsRUFBRTtvQkFDeEMsSUFBSSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxFQUFFO3dCQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxHQUFHLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDekY7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUM7WUFFaEMsSUFBSSxnQkFBZ0IsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ2hELEtBQUssSUFBSSxFQUFFLElBQUksZ0JBQWdCLENBQUMscUJBQXFCLEVBQUU7b0JBQ25ELElBQUksZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxFQUFFO3dCQUMzRCxJQUFJLENBQUMscUJBQXFCLENBQUMsRUFBRSxDQUFDLEdBQUcsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsRUFBRSxDQUFDLENBQUM7cUJBQy9FO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUM7WUFDNUQsSUFBSSxnQkFBZ0IsQ0FBQyxlQUFlLElBQUksSUFBSSxFQUFFO2dCQUMxQyxJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDekY7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7YUFDL0I7WUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUVwQixJQUFJLGdCQUFnQixDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3BDLEtBQUssSUFBSSxFQUFFLElBQUksZ0JBQWdCLENBQUMsU0FBUyxFQUFFO29CQUN2QyxJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3JGO2FBQ0o7WUFFRCxJQUFJLENBQUMsZ0NBQWdDLEdBQUcsRUFBRSxDQUFDO1lBRTNDLElBQUksZ0JBQWdCLENBQUMsZ0NBQWdDLElBQUksSUFBSSxFQUFFO2dCQUMzRCxLQUFLLElBQUksRUFBRSxJQUFJLGdCQUFnQixDQUFDLGdDQUFnQyxFQUFFO29CQUM5RCxJQUFJLENBQUMsZ0NBQWdDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3JHO2FBQ0o7WUFFRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO1lBRWhDLElBQUksZ0JBQWdCLENBQUMscUJBQXFCLElBQUksSUFBSSxFQUFFO2dCQUNoRCxLQUFLLElBQUksRUFBRSxJQUFJLGdCQUFnQixDQUFDLHFCQUFxQixFQUFFO29CQUNuRCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQy9FO2FBQ0o7WUFFRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7WUFDOUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUUzQixJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDM0MsS0FBSyxJQUFJLEVBQUUsSUFBSSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDOUMsSUFBSSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLEVBQUU7d0JBQ3RELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDckU7aUJBQ0o7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSw4QkFBVyxHQUF6QixVQUEwQixXQUFnQjtZQUN0QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ25DLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEscUNBQWtCLEdBQWhDLFVBQWlDLGtCQUF1QjtZQUNwRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxvQkFBb0IsQ0FBQztZQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztZQUMxQyxJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQztZQUN0QyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUVoQixJQUFJLGtCQUFrQixDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2xDLEtBQUssSUFBSSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSyxFQUFFO29CQUN0QyxJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUM7b0JBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hGO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsaUNBQWMsR0FBNUIsVUFBNkIsY0FBbUI7WUFDNUMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUM7WUFDOUIsSUFBSSxjQUFjLENBQUMsYUFBYSxJQUFJLElBQUksRUFBRTtnQkFDdEMsSUFBSSxRQUFRLEdBQUcsa0JBQWtCLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ25GO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzdCO1lBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFFMUIsSUFBSSxjQUFjLENBQUMsZUFBZSxJQUFJLElBQUksRUFBRTtnQkFDeEMsS0FBSyxJQUFJLEVBQUUsSUFBSSxjQUFjLENBQUMsZUFBZSxFQUFFO29CQUMzQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ2pFO2FBQ0o7WUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLGNBQWMsQ0FBQyxXQUFXLENBQUM7WUFDOUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBRXpCLElBQUksY0FBYyxDQUFDLGNBQWMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZDLEtBQUssSUFBSSxFQUFFLElBQUksY0FBYyxDQUFDLGNBQWMsRUFBRTtvQkFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUMvRDthQUNKO1lBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQztZQUNwRCxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQyxlQUFlLENBQUM7WUFDdEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFFMUIsSUFBSSxjQUFjLENBQUMsZUFBZSxJQUFJLElBQUksRUFBRTtnQkFDeEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsZUFBZSxFQUFFO29CQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQ2xFO2FBQ0o7WUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUVsQixJQUFJLGNBQWMsQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUNoQyxLQUFLLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxPQUFPLEVBQUU7b0JBQ3BDLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3FCQUMxRjtpQkFDSjthQUNKO1lBRUQsSUFBSSxDQUFDLElBQUksR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ2hDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxjQUFjLENBQUMsd0JBQXdCLENBQUM7WUFDeEUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztZQUU5QixJQUFJLGNBQWMsQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLEVBQUU7Z0JBQzVDLEtBQUssSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLG1CQUFtQixFQUFFO29CQUNoRCxJQUFJLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3hELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxjQUFjLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQzNFO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsY0FBYyxDQUFDLGlCQUFpQixDQUFDO1lBQzFELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxjQUFjLENBQUMsaUJBQWlCLENBQUM7WUFDMUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxjQUFjLENBQUMsYUFBYSxDQUFDO1lBQ2xELElBQUksQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQztZQUN0QyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsK0JBQVksR0FBMUIsVUFBMkIsWUFBaUI7WUFDeEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDO1lBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQztZQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxZQUFZLENBQUMsbUJBQW1CLENBQUM7WUFDNUQsSUFBSSxDQUFDLElBQUksR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQztZQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQyxVQUFVLENBQUM7WUFDMUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDO1lBQ3BDLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSw0QkFBUyxHQUF2QixVQUF3QixTQUFjO1lBQ2xDLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztZQUN6QixJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQyxjQUFjLENBQUM7WUFDL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsWUFBWSxDQUFDO1lBQzNDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztZQUNyQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsU0FBUyxDQUFDLHFCQUFxQixDQUFDO1lBQzdELElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztZQUM3QyxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVyQixJQUFJLFNBQVMsQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUM5QixLQUFLLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxVQUFVLEVBQUU7b0JBQ2xDLElBQUksU0FBUyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDcEQ7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFDO1lBRXRDLElBQUksU0FBUyxDQUFDLDJCQUEyQixJQUFJLElBQUksRUFBRTtnQkFDL0MsS0FBSyxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsMkJBQTJCLEVBQUU7b0JBQ25ELElBQUksU0FBUyxDQUFDLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDM0QsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQywyQkFBMkIsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDdEY7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUM7WUFDekMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFFcEIsSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDN0IsS0FBSyxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsU0FBUyxFQUFFO29CQUNqQyxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUN6QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ2xEO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsOEJBQVcsR0FBekIsVUFBMEIsV0FBZ0I7WUFDdEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO1lBQzNCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxXQUFXLENBQUMsbUJBQW1CLENBQUM7WUFDM0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFFbEIsSUFBSSxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDN0IsS0FBSyxJQUFJLEdBQUcsSUFBSSxXQUFXLENBQUMsT0FBTyxFQUFFO29CQUNqQyxJQUFJLFFBQVEsR0FBRyxXQUFXLENBQUM7b0JBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM3RTthQUNKO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsV0FBVyxDQUFDLGlCQUFpQixDQUFDO1lBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUNqQyxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7WUFDbkMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztZQUM3RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsV0FBVyxDQUFDLHFCQUFxQixDQUFDO1lBQy9ELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUM7WUFDN0QsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxVQUFVLENBQUM7WUFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDO1lBQ25ELElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVyQixJQUFJLFdBQVcsQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUNoQyxLQUFLLElBQUksR0FBRyxJQUFJLFdBQVcsQ0FBQyxVQUFVLEVBQUU7b0JBQ3BDLElBQUksUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ25GO2FBQ0o7WUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7WUFDN0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztZQUNyRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLG1CQUFtQixDQUFDO1lBQzNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7WUFDckQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztZQUM3RCxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUM7WUFDakQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztZQUNyRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLG1CQUFtQixDQUFDO1lBQzNELElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQztZQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7WUFDbkQsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQztZQUNqRSxJQUFJLENBQUMseUJBQXlCLEdBQUcsV0FBVyxDQUFDLHlCQUF5QixDQUFDO1lBQ3ZFLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxXQUFXLENBQUMscUJBQXFCLENBQUM7WUFDL0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztZQUM3RCxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7WUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBRWhCLElBQUksV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQzNCLEtBQUssSUFBSSxHQUFHLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRTtvQkFDL0IsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDO29CQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDekU7YUFDSjtZQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQztZQUN6QyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsNkJBQVUsR0FBeEIsVUFBeUIsVUFBZTtZQUNwQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUM7WUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFFbkIsSUFBSSxVQUFVLENBQUMsUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDN0IsS0FBSyxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsUUFBUSxFQUFFO29CQUNqQyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM5RTthQUNKO1lBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQztZQUN0RCxJQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQUM7WUFDaEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUMxQyxJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxVQUFVLENBQUMscUJBQXFCLENBQUM7WUFDOUQsSUFBSSxDQUFDLDhCQUE4QixHQUFHLFVBQVUsQ0FBQyw4QkFBOEIsQ0FBQztZQUNoRixJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUM7WUFDNUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQztZQUN4RCxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxlQUFlLENBQUM7WUFDbEQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQztZQUMxRCxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDaEMsSUFBSSxVQUFVLENBQUMsUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDN0IsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDO2dCQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNyRTtpQkFBTTtnQkFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN4QjtZQUVELElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQztZQUM1QixJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFDO1lBRXRDLElBQUksVUFBVSxDQUFDLDJCQUEyQixJQUFJLElBQUksRUFBRTtnQkFDaEQsS0FBSyxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsMkJBQTJCLEVBQUU7b0JBQ3BELElBQUksVUFBVSxDQUFDLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDNUQsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQywyQkFBMkIsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDdkY7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUNwQyxJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDO1lBQzFDLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUMxQyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEscUNBQWtCLEdBQWhDLFVBQWlDLGtCQUF1QjtZQUNwRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxvQkFBb0IsQ0FBQztZQUNsQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBRTNCLElBQUksa0JBQWtCLENBQUMsZ0JBQWdCLElBQUksSUFBSSxFQUFFO2dCQUM3QyxLQUFLLElBQUksR0FBRyxJQUFJLGtCQUFrQixDQUFDLGdCQUFnQixFQUFFO29CQUNqRCxJQUFJLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3RHO2FBQ0o7WUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztZQUMxQyxJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztZQUMxQyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUVoQixJQUFJLGtCQUFrQixDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2xDLEtBQUssSUFBSSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSyxFQUFFO29CQUN0QyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hGO2FBQ0o7WUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQztZQUNwQyxJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztZQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztZQUNsRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsZ0NBQWEsR0FBM0IsVUFBNEIsYUFBa0I7WUFDMUMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDO1lBQzdCLElBQUksQ0FBQywyQkFBMkIsR0FBRyxhQUFhLENBQUMsMkJBQTJCLENBQUM7WUFDN0UsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUU1QixJQUFJLGFBQWEsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ3pDLEtBQUssSUFBSSxHQUFHLElBQUksYUFBYSxDQUFDLGlCQUFpQixFQUFFO29CQUM3QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lCQUNyRTthQUNKO1lBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUUzQixJQUFJLGFBQWEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7Z0JBQ3hDLEtBQUssSUFBSSxHQUFHLElBQUksYUFBYSxDQUFDLGdCQUFnQixFQUFFO29CQUM1QyxJQUFJLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNqRzthQUNKO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDO1lBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7WUFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFFM0IsSUFBSSxhQUFhLENBQUMsZ0JBQWdCLElBQUksSUFBSSxFQUFFO2dCQUN4QyxLQUFLLElBQUksR0FBRyxJQUFJLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDNUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDbkU7YUFDSjtZQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBRXJCLElBQUksYUFBYSxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQ2xDLEtBQUssSUFBSSxHQUFHLElBQUksYUFBYSxDQUFDLFVBQVUsRUFBRTtvQkFDdEMsSUFBSSxRQUFRLEdBQUcsY0FBYyxDQUFDO29CQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDckY7YUFDSjtZQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO1lBQzNDLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSwwQkFBTyxHQUFyQixVQUFzQixPQUFZO1lBQzlCLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztZQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVyQixJQUFJLE9BQU8sQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUM1QixLQUFLLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxVQUFVLEVBQUU7b0JBQ2hDLElBQUksUUFBUSxHQUFHLGNBQWMsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQy9FO2FBQ0o7WUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztZQUNyQyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsb0NBQWlCLEdBQS9CLFVBQWdDLGlCQUFzQjtZQUNsRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxtQkFBbUIsQ0FBQztZQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztZQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUVyQixJQUFJLGlCQUFpQixDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQ3RDLEtBQUssSUFBSSxHQUFHLElBQUksaUJBQWlCLENBQUMsVUFBVSxFQUFFO29CQUMxQyxJQUFJLFFBQVEsR0FBRyxtQkFBbUIsQ0FBQztvQkFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDekY7YUFDSjtZQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1lBQy9DLElBQUksQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1lBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxHQUFHLEdBQUcsaUJBQWlCLENBQUMsR0FBRyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQztZQUNuRSxJQUFJLENBQUMsU0FBUyxHQUFHLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztZQUM5QyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsa0NBQWUsR0FBN0IsVUFBOEIsZUFBb0I7WUFDOUMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQWlCLENBQUM7WUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxlQUFlLENBQUMsaUJBQWlCLENBQUM7WUFDM0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQztZQUNqQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUVsQixJQUFJLGVBQWUsQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUNqQyxLQUFLLElBQUksR0FBRyxJQUFJLGVBQWUsQ0FBQyxPQUFPLEVBQUU7b0JBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDbkQ7YUFDSjtZQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQztZQUN2QyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsOEJBQVcsR0FBekIsVUFBMEIsV0FBZ0I7WUFDdEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO1lBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7WUFDbkMsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsMEJBQU8sR0FBckIsVUFBc0IsT0FBWTtZQUM5QixJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7WUFDdkIsSUFBSSxPQUFPLENBQUMsZ0JBQWdCLElBQUksSUFBSSxFQUFFO2dCQUNsQyxJQUFJLFFBQVEsR0FBRyxtQkFBbUIsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ2xGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7YUFDaEM7WUFFRCxJQUFJLE9BQU8sQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFO2dCQUM5QixJQUFJLFFBQVEsR0FBRyxtQkFBbUIsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDMUU7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7YUFDNUI7WUFFRCxJQUFJLE9BQU8sQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLEVBQUU7Z0JBQ3BDLElBQUksUUFBUSxHQUFHLG1CQUFtQixDQUFDO2dCQUNuQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDdEY7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQzthQUNsQztZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSwwQkFBTyxHQUFyQixVQUFzQixPQUFZO1lBQzlCLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztZQUN2QixJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFDO1lBRXRDLElBQUksT0FBTyxDQUFDLDJCQUEyQixJQUFJLElBQUksRUFBRTtnQkFDN0MsS0FBSyxJQUFJLEdBQUcsSUFBSSxPQUFPLENBQUMsMkJBQTJCLEVBQUU7b0JBQ2pELElBQUksT0FBTyxDQUFDLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDekQsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQywyQkFBMkIsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDcEY7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQywyQkFBMkIsR0FBRyxPQUFPLENBQUMsMkJBQTJCLENBQUM7WUFDdkUsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO1lBQ25DLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxPQUFPLENBQUMsdUJBQXVCLENBQUM7WUFDL0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztZQUN2QyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDakMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQztZQUNuQyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUVoQixJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUN2QixLQUFLLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDdkM7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxnQ0FBYSxHQUEzQixVQUE0QixhQUFrQjtZQUMxQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxlQUFlLENBQUM7WUFDN0IsSUFBSSxDQUFDLEVBQUUsR0FBRyxhQUFhLENBQUMsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztZQUN2QyxJQUFJLGFBQWEsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUM1QixJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ3BCO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLHVDQUFvQixHQUFsQyxVQUFtQyxLQUFhO1lBQzVDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUVoQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUUxQyxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDO1FBRWEsK0JBQVksR0FBMUIsVUFBMkIsWUFBaUI7WUFDeEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDO1lBQzVCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsaUJBQWlCLENBQUM7WUFDeEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQztZQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBQ2pDLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxrQ0FBZSxHQUE3QixVQUE4QixlQUFvQjtZQUM5QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxpQkFBaUIsQ0FBQztZQUMvQixJQUFJLGVBQWUsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7Z0JBQzFDLElBQUksUUFBUSxHQUFHLFNBQVMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQzFGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7YUFDaEM7WUFFRCxJQUFJLGVBQWUsQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUNwQyxJQUFJLFFBQVEsR0FBRyxlQUFlLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzlFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzFCO1lBRUQsSUFBSSxlQUFlLENBQUMsYUFBYSxJQUFJLElBQUksRUFBRTtnQkFDdkMsSUFBSSxRQUFRLEdBQUcsa0JBQWtCLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3BGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzdCO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDLFVBQVUsQ0FBQztZQUM3QyxJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1RSxJQUFJLGVBQWUsQ0FBQyxhQUFhLElBQUksSUFBSSxFQUFFO2dCQUN2QyxJQUFJLFFBQVEsR0FBRyxjQUFjLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3BGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzdCO1lBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUMsVUFBVSxDQUFDO1lBQzdDLElBQUksQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQztZQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdkUsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLGtEQUErQixHQUE3QyxVQUE4QywrQkFBb0M7WUFDOUUsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsaUNBQWlDLENBQUM7WUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRywrQkFBK0IsQ0FBQyxNQUFNLENBQUM7WUFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRywrQkFBK0IsQ0FBQyxTQUFTLENBQUM7WUFDM0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLDhCQUFXLEdBQXpCLFVBQTBCLFdBQWdCO1lBQ3RDLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQztZQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7WUFDN0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQzNDLElBQUksQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxVQUFVLENBQUM7WUFDekMsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUM7WUFDN0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQztZQUMzQyxJQUFJLFdBQVcsQ0FBQyw0QkFBNEIsSUFBSSxJQUFJLEVBQUU7Z0JBQ2xELElBQUksUUFBUSxHQUFHLGlDQUFpQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsNEJBQTRCLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxDQUFDLDRCQUE0QixDQUFDLENBQUM7YUFDOUc7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQzthQUM1QztZQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuRSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsNkJBQVUsR0FBeEIsVUFBeUIsVUFBZTtZQUNwQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUM7WUFDMUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQztZQUMzRCxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUM7WUFDNUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQztZQUN0RCxJQUFJLENBQUMseUJBQXlCLEdBQUcsVUFBVSxDQUFDLHlCQUF5QixDQUFDO1lBQ3RFLElBQUksQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQztZQUM1QyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDeEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQztZQUNoRSxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFDMUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQztZQUNwRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFDekMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDeEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQztZQUN4QyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFDMUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBRWpCLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQzNCLEtBQUssSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtvQkFDL0IsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUM3QztpQkFDSjthQUNKO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLHVDQUFvQixHQUFsQyxVQUFtQyxvQkFBeUI7WUFDeEQsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsc0JBQXNCLENBQUM7WUFDcEMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztZQUVuQyxJQUFJLG9CQUFvQixDQUFDLHdCQUF3QixJQUFJLElBQUksRUFBRTtnQkFDdkQsS0FBSyxJQUFJLEdBQUcsSUFBSSxvQkFBb0IsQ0FBQyx3QkFBd0IsRUFBRTtvQkFDM0QsSUFBSSxvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ25FLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsd0JBQXdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztxQkFDM0g7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFFNUIsSUFBSSxvQkFBb0IsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ2hELEtBQUssSUFBSSxHQUFHLElBQUksb0JBQW9CLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3BELElBQUksb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUM1RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLEdBQUcsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQzdFO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLG9CQUFvQixDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQ3pDLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNuRjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUMxQjtZQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQW9CLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQW9CLENBQUMsYUFBYSxDQUFDO1lBQ3hELElBQUksQ0FBQyxFQUFFLEdBQUcsb0JBQW9CLENBQUMsRUFBRSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsVUFBVSxDQUFDO1lBQ2xELElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQW9CLENBQUMsV0FBVyxDQUFDO1lBQ3BELElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQW9CLENBQUMsV0FBVyxDQUFDO1lBQ3BELElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQW9CLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FBQztZQUNoRSxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFvQixDQUFDLFdBQVcsQ0FBQztZQUNwRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO1lBRW5DLElBQUksb0JBQW9CLENBQUMsd0JBQXdCLElBQUksSUFBSSxFQUFFO2dCQUN2RCxLQUFLLElBQUksR0FBRyxJQUFJLG9CQUFvQixDQUFDLHdCQUF3QixFQUFFO29CQUMzRCxJQUFJLG9CQUFvQixDQUFDLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDbkUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxHQUFHLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7cUJBQ3BJO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUMsb0JBQW9CLENBQUM7WUFDdEUsSUFBSSxDQUFDLDhCQUE4QixHQUFHLG9CQUFvQixDQUFDLDhCQUE4QixDQUFDO1lBQzFGLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsVUFBVSxDQUFDO1lBQ2xELElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQW9CLENBQUMsV0FBVyxDQUFDO1lBQ3BELElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVFLElBQUksb0JBQW9CLENBQUMsT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDdEMsSUFBSSxRQUFRLEdBQUcsWUFBWSxDQUFDO2dCQUM1QixJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzdFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2FBQ3ZCO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLGlDQUFjLEdBQTVCLFVBQTZCLGNBQW1CO1lBQzVDLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO1lBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUVuQixJQUFJLGNBQWMsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUNqQyxLQUFLLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxRQUFRLEVBQUU7b0JBQ3JDLElBQUksY0FBYyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQzdDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDckQ7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQztZQUM1QyxJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDNUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDO1lBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDLFNBQVMsQ0FBQztZQUMxQyxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFFakIsSUFBSSxjQUFjLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDL0IsS0FBSyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsTUFBTSxFQUFFO29CQUNuQyxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ2pEO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsMENBQXVCLEdBQXJDLFVBQXNDLHVCQUE0QjtZQUM5RCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyx5QkFBeUIsQ0FBQztZQUN2QyxJQUFJLHVCQUF1QixDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQzVDLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN0RjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUMxQjtZQUVELElBQUksQ0FBQyxtQkFBbUIsR0FBRyx1QkFBdUIsQ0FBQyxtQkFBbUIsQ0FBQztZQUN2RSxJQUFJLENBQUMsWUFBWSxHQUFHLHVCQUF1QixDQUFDLFlBQVksQ0FBQztZQUN6RCxJQUFJLENBQUMsY0FBYyxHQUFHLHVCQUF1QixDQUFDLGNBQWMsQ0FBQztZQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLHVCQUF1QixDQUFDLGFBQWEsQ0FBQztZQUMzRCxJQUFJLENBQUMsVUFBVSxHQUFHLHVCQUF1QixDQUFDLFVBQVUsQ0FBQztZQUNyRCxJQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDLFdBQVcsQ0FBQztZQUN2RCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsdUJBQXVCLENBQUMsb0JBQW9CLENBQUM7WUFDekUsSUFBSSxDQUFDLFVBQVUsR0FBRyx1QkFBdUIsQ0FBQyxVQUFVLENBQUM7WUFDckQsSUFBSSxDQUFDLFdBQVcsR0FBRyx1QkFBdUIsQ0FBQyxXQUFXLENBQUM7WUFDdkQsSUFBSSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDL0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFFbkIsSUFBSSx1QkFBdUIsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUMxQyxLQUFLLElBQUksR0FBRyxJQUFJLHVCQUF1QixDQUFDLFFBQVEsRUFBRTtvQkFDOUMsSUFBSSxRQUFRLEdBQUcsWUFBWSxDQUFDO29CQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMzRjthQUNKO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLHFDQUFrQixHQUFoQyxVQUFpQyxrQkFBdUI7WUFDcEQsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsb0JBQW9CLENBQUM7WUFDbEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUU1QixJQUFJLGtCQUFrQixDQUFDLGlCQUFpQixJQUFJLElBQUksRUFBRTtnQkFDOUMsS0FBSyxJQUFJLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDbEQsSUFBSSxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQzFELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDM0U7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsa0JBQWtCLENBQUMsWUFBWSxDQUFDO1lBQ3BELElBQUksQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1lBQ2hELElBQUksQ0FBQyxPQUFPLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDO1lBQzFDLElBQUksQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1lBQ2hELElBQUksQ0FBQyxXQUFXLEdBQUcsa0JBQWtCLENBQUMsV0FBVyxDQUFDO1lBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFFLElBQUksa0JBQWtCLENBQUMsT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDcEMsSUFBSSxRQUFRLEdBQUcsWUFBWSxDQUFDO2dCQUM1QixJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzNFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2FBQ3ZCO1lBRUQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztZQUU3QixJQUFJLGtCQUFrQixDQUFDLGtCQUFrQixJQUFJLElBQUksRUFBRTtnQkFDL0MsS0FBSyxJQUFJLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxrQkFBa0IsRUFBRTtvQkFDbkQsSUFBSSxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQzNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDN0U7aUJBQ0o7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxnQ0FBYSxHQUEzQixVQUE0QixhQUFrQjtZQUMxQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxlQUFlLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDO1lBQy9DLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQztZQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7WUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztZQUM3QyxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztZQUU3QixJQUFJLGFBQWEsQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLEVBQUU7Z0JBQzFDLEtBQUssSUFBSSxHQUFHLElBQUksYUFBYSxDQUFDLGtCQUFrQixFQUFFO29CQUM5QyxJQUFJLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3RELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxhQUFhLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3hFO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsNEJBQVMsR0FBdkIsVUFBd0IsU0FBYztZQUNsQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsWUFBWSxDQUFDO1lBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQztZQUN2QyxJQUFJLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUM7WUFDakMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUU1QixJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ3JDLEtBQUssSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDLGlCQUFpQixFQUFFO29CQUN6QyxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ2pELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ2xFO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUM7WUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUMsV0FBVyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1lBRTdCLElBQUksU0FBUyxDQUFDLGtCQUFrQixJQUFJLElBQUksRUFBRTtnQkFDdEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsa0JBQWtCLEVBQUU7b0JBQzFDLElBQUksU0FBUyxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDcEU7aUJBQ0o7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxnQ0FBYSxHQUEzQixVQUE0QixhQUFrQjtZQUMxQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxlQUFlLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDO1lBQy9DLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQztZQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7WUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztZQUM3QyxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztZQUU3QixJQUFJLGFBQWEsQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLEVBQUU7Z0JBQzFDLEtBQUssSUFBSSxHQUFHLElBQUksYUFBYSxDQUFDLGtCQUFrQixFQUFFO29CQUM5QyxJQUFJLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3RELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxhQUFhLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3hFO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsd0NBQXFCLEdBQW5DLFVBQW9DLHFCQUEwQjtZQUMxRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyx1QkFBdUIsQ0FBQztZQUNyQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcscUJBQXFCLENBQUMsaUJBQWlCLENBQUM7WUFDakUsSUFBSSxDQUFDLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxNQUFNLENBQUM7WUFDMUMsSUFBSSxDQUFDLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUM7WUFDekMsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLHdDQUFxQixHQUFuQyxVQUFvQyxxQkFBMEI7WUFDMUQsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsdUJBQXVCLENBQUM7WUFDckMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLHFCQUFxQixDQUFDLHdCQUF3QixDQUFDO1lBQzlFLElBQUkscUJBQXFCLENBQUMsV0FBVyxJQUFJLElBQUksRUFBRTtnQkFDM0MsSUFBSSxRQUFRLEdBQUcsdUJBQXVCLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdEY7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7YUFDM0I7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsK0NBQTRCLEdBQTFDLFVBQTJDLDRCQUFpQztZQUN4RSxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyw4QkFBOEIsQ0FBQztZQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLDRCQUE0QixDQUFDLFlBQVksQ0FBQztZQUM5RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1lBRTdCLElBQUksNEJBQTRCLENBQUMsa0JBQWtCLElBQUksSUFBSSxFQUFFO2dCQUN6RCxLQUFLLElBQUksR0FBRyxJQUFJLDRCQUE0QixDQUFDLGtCQUFrQixFQUFFO29CQUM3RCxJQUFJLFFBQVEsR0FBRyx1QkFBdUIsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyw0QkFBNEIsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3BIO2FBQ0o7WUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsNEJBQTRCLENBQUMsZ0JBQWdCLENBQUM7WUFDdEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLDRCQUE0QixDQUFDLGtCQUFrQixDQUFDO1lBQzFFLElBQUksQ0FBQyxVQUFVLEdBQUcsNEJBQTRCLENBQUMsVUFBVSxDQUFDO1lBQzFELElBQUksQ0FBQyxxQkFBcUIsR0FBRyw0QkFBNEIsQ0FBQyxxQkFBcUIsQ0FBQztZQUNoRixJQUFJLENBQUMsVUFBVSxHQUFHLDRCQUE0QixDQUFDLFVBQVUsQ0FBQztZQUMxRCxJQUFJLENBQUMsV0FBVyxHQUFHLDRCQUE0QixDQUFDLFdBQVcsQ0FBQztZQUM1RCxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRixJQUFJLENBQUMsOEJBQThCLEdBQUcsNEJBQTRCLENBQUMsOEJBQThCLENBQUM7WUFDbEcsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLHdDQUFxQixHQUFuQyxVQUFvQyxxQkFBMEI7WUFDMUQsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsdUJBQXVCLENBQUM7WUFDckMsSUFBSSxxQkFBcUIsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ2pELElBQUksUUFBUSxHQUFHLHNCQUFzQixDQUFDO2dCQUN0QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUNsRztpQkFBTTtnQkFDSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUM7WUFDdkQsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxVQUFVLENBQUM7WUFDbkQsSUFBSSxxQkFBcUIsQ0FBQyx5QkFBeUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ3pELElBQUksUUFBUSxHQUFHLDhCQUE4QixDQUFDO2dCQUM5QyxJQUFJLENBQUMseUJBQXlCLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMscUJBQXFCLENBQUMseUJBQXlCLENBQUMsQ0FBQzthQUNsSDtpQkFBTTtnQkFDSCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO2FBQ3pDO1lBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxVQUFVLENBQUM7WUFDbkQsSUFBSSxDQUFDLFdBQVcsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUM7WUFDckQsSUFBSSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDN0UsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLGlDQUFjLEdBQTVCLFVBQTZCLGNBQW1CO1lBQzVDLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO1lBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDNUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFFMUIsSUFBSSxjQUFjLENBQUMsZUFBZSxJQUFJLElBQUksRUFBRTtnQkFDeEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsZUFBZSxFQUFFO29CQUM1QyxJQUFJLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQztvQkFDakMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hHO2FBQ0o7WUFFRCxJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDNUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDO1lBQzlDLElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0RSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsdUNBQW9CLEdBQWxDLFVBQW1DLG9CQUF5QjtZQUN4RCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxzQkFBc0IsQ0FBQztZQUNwQyxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFvQixDQUFDLFlBQVksQ0FBQztZQUN0RCxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFvQixDQUFDLFVBQVUsQ0FBQztZQUNsRCxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFvQixDQUFDLFVBQVUsQ0FBQztZQUNsRCxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFvQixDQUFDLFdBQVcsQ0FBQztZQUNwRCxJQUFJLG9CQUFvQixDQUFDLGFBQWEsSUFBSSxJQUFJLEVBQUU7Z0JBQzVDLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN6RjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzthQUM3QjtZQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVFLElBQUksb0JBQW9CLENBQUMsU0FBUyxJQUFJLElBQUksRUFBRTtnQkFDeEMsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDO2dCQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2pGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2FBQ3pCO1lBRUQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLDRCQUFTLEdBQXZCLFVBQXdCLFNBQWM7WUFDbEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDLFlBQVksQ0FBQztZQUMzQyxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFdBQVcsQ0FBQztZQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDakUsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLDRDQUF5QixHQUF2QyxVQUF3Qyx5QkFBOEI7WUFDbEUsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsMkJBQTJCLENBQUM7WUFDekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFFbkIsSUFBSSx5QkFBeUIsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO2dCQUM1QyxLQUFLLElBQUksR0FBRyxJQUFJLHlCQUF5QixDQUFDLFFBQVEsRUFBRTtvQkFDaEQsSUFBSSx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDaEU7aUJBQ0o7YUFDSjtZQUVELElBQUksQ0FBQyxXQUFXLEdBQUcseUJBQXlCLENBQUMsV0FBVyxDQUFDO1lBQ3pELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxpREFBOEIsR0FBNUMsVUFBNkMsOEJBQW1DO1lBQzVFLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLGdDQUFnQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw4QkFBOEIsQ0FBQyxpQkFBaUIsQ0FBQztZQUMxRSxJQUFJLENBQUMsV0FBVyxHQUFHLDhCQUE4QixDQUFDLFdBQVcsQ0FBQztZQUM5RCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsd0NBQXFCLEdBQW5DLFVBQW9DLHFCQUEwQjtZQUMxRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyx1QkFBdUIsQ0FBQztZQUNyQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcscUJBQXFCLENBQUMsc0JBQXNCLENBQUM7WUFDM0UsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztZQUU3QixJQUFJLHFCQUFxQixDQUFDLGtCQUFrQixJQUFJLElBQUksRUFBRTtnQkFDbEQsS0FBSyxJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxrQkFBa0IsRUFBRTtvQkFDdEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lCQUMvRTthQUNKO1lBRUQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLHFCQUFxQixDQUFDLG9CQUFvQixDQUFDO1lBQ3ZFLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxxQkFBcUIsQ0FBQyxxQkFBcUIsQ0FBQztZQUN6RSxJQUFJLENBQUMsMkJBQTJCLEdBQUcscUJBQXFCLENBQUMsMkJBQTJCLENBQUM7WUFDckYsSUFBSSxDQUFDLG9CQUFvQixHQUFHLHFCQUFxQixDQUFDLG9CQUFvQixDQUFDO1lBQ3ZFLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxxQkFBcUIsQ0FBQyx5QkFBeUIsQ0FBQztZQUNqRixJQUFJLENBQUMsaUJBQWlCLEdBQUcscUJBQXFCLENBQUMsaUJBQWlCLENBQUM7WUFDakUsSUFBSSxDQUFDLGVBQWUsR0FBRyxxQkFBcUIsQ0FBQyxlQUFlLENBQUM7WUFDN0QsSUFBSSxxQkFBcUIsQ0FBQywyQkFBMkIsSUFBSSxJQUFJLEVBQUU7Z0JBQzNELElBQUksUUFBUSxHQUFHLGdDQUFnQyxDQUFDO2dCQUNoRCxJQUFJLENBQUMsMkJBQTJCLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMscUJBQXFCLENBQUMsMkJBQTJCLENBQUMsQ0FBQzthQUN0SDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDO2FBQzNDO1lBRUQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDO1lBQ25FLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQztZQUNuRSxJQUFJLENBQUMscUJBQXFCLEdBQUcscUJBQXFCLENBQUMscUJBQXFCLENBQUM7WUFDekUsSUFBSSxDQUFDLDJCQUEyQixHQUFHLHFCQUFxQixDQUFDLDJCQUEyQixDQUFDO1lBQ3JGLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUM7WUFFaEMsSUFBSSxxQkFBcUIsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLEVBQUU7Z0JBQ3JELEtBQUssSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUMscUJBQXFCLEVBQUU7b0JBQ3pELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDckY7YUFDSjtZQUVELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSw2QkFBVSxHQUF4QixVQUF5QixLQUFhO1lBQ2xDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUVoQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVuQixPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDO1FBRWEscUNBQWtCLEdBQWhDLFVBQWlDLGtCQUF1QjtZQUNwRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxvQkFBb0IsQ0FBQztZQUNsQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBRTNCLElBQUksa0JBQWtCLENBQUMsZ0JBQWdCLElBQUksSUFBSSxFQUFFO2dCQUM3QyxLQUFLLElBQUksR0FBRyxJQUFJLGtCQUFrQixDQUFDLGdCQUFnQixFQUFFO29CQUNqRCxJQUFJLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3RHO2FBQ0o7WUFFRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1lBRTlCLElBQUksa0JBQWtCLENBQUMsbUJBQW1CLElBQUksSUFBSSxFQUFFO2dCQUNoRCxLQUFLLElBQUksR0FBRyxJQUFJLGtCQUFrQixDQUFDLG1CQUFtQixFQUFFO29CQUNwRCxJQUFJLFFBQVEsR0FBRyx1QkFBdUIsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzVHO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsMEJBQU8sR0FBckIsVUFBc0IsT0FBWTtZQUM5QixJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7WUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUMzQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEscUNBQWtCLEdBQWhDLFVBQWlDLGtCQUF1QjtZQUNwRCxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxvQkFBb0IsQ0FBQztZQUNsQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsa0JBQWtCLENBQUMscUJBQXFCLENBQUM7WUFDdEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUM7WUFDdEQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUUzQixJQUFJLGtCQUFrQixDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDN0MsS0FBSyxJQUFJLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDakQsSUFBSSxRQUFRLEdBQUcsU0FBUyxDQUFDO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEc7YUFDSjtZQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsa0JBQWtCLENBQUMsY0FBYyxDQUFDO1lBQ3hELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxpQ0FBYyxHQUE1QixVQUE2QixjQUFtQjtZQUM1QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztZQUM5QixJQUFJLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQyxLQUFLLENBQUM7WUFDbEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQztZQUMxRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsY0FBYyxDQUFDLHlCQUF5QixDQUFDO1lBQ3pFLElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUNoRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsY0FBYyxDQUFDLGdCQUFnQixDQUFDO1lBQ3hELElBQUksQ0FBQyxVQUFVLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQztZQUM3QyxJQUFJLENBQUMsWUFBWSxHQUFHLGNBQWMsQ0FBQyxhQUFhLENBQUM7WUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDO1lBQzdDLElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQztZQUNqRCxJQUFJLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDO1lBQzFDLElBQUksQ0FBQyxXQUFXLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFDeEMsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLGtDQUFlLEdBQTdCLFVBQThCLGVBQW9CO1lBQzlDLElBQUksSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVuQixJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFpQixDQUFDO1lBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBRWxCLElBQUksZUFBZSxDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ2pDLEtBQUssSUFBSSxHQUFHLElBQUksZUFBZSxDQUFDLE9BQU8sRUFBRTtvQkFDckMsSUFBSSxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNqRjthQUNKO1lBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUMsV0FBVyxDQUFDO1lBQy9DLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSxpQ0FBYyxHQUE1QixVQUE2QixjQUFtQjtZQUM1QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztZQUM5QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBRTNCLElBQUksY0FBYyxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDekMsS0FBSyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7b0JBQzdDLElBQUksUUFBUSxHQUFHLG9CQUFvQixDQUFDO29CQUNwQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2xHO2FBQ0o7WUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFDaEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztZQUN2RSxJQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQyxjQUFjLENBQUM7WUFDcEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQztZQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLGNBQWMsQ0FBQyxXQUFXLENBQUM7WUFDOUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQztZQUNsRSxJQUFJLGNBQWMsQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFO2dCQUNyQyxJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2FBQzVCO1lBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDO1lBQzFDLElBQUksQ0FBQyxFQUFFLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQztZQUM1QixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsNENBQXlCLEdBQXZDLFVBQXdDLHlCQUE4QjtZQUNsRSxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRywyQkFBMkIsQ0FBQztZQUN6QyxJQUFJLENBQUMscUJBQXFCLEdBQUcseUJBQXlCLENBQUMscUJBQXFCLENBQUM7WUFDN0UsSUFBSSxDQUFDLE9BQU8sR0FBRyx5QkFBeUIsQ0FBQyxPQUFPLENBQUM7WUFDakQsSUFBSSxDQUFDLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQyxTQUFTLENBQUM7WUFDckQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHlCQUF5QixDQUFDLGtCQUFrQixDQUFDO1lBQ3RFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyx5QkFBeUIsQ0FBQyxtQkFBbUIsQ0FBQztZQUN6RSxJQUFJLENBQUMsZUFBZSxHQUFHLHlCQUF5QixDQUFDLGVBQWUsQ0FBQztZQUNqRSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRWEsa0NBQWUsR0FBN0IsVUFBOEIsZUFBb0I7WUFDOUMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQWlCLENBQUM7WUFDL0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQztZQUM1RCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLG1CQUFtQixDQUFDO1lBQy9ELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFYSwrQkFBWSxHQUExQixVQUEyQixZQUFpQjtZQUN4QyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7WUFFbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxjQUFjLENBQUM7WUFDNUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsZUFBZSxDQUFDO1lBQ3BELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxZQUFZLENBQUMsb0JBQW9CLENBQUM7WUFDOUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFFeEIsSUFBSSxZQUFZLENBQUMsYUFBYSxJQUFJLElBQUksRUFBRTtnQkFDcEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO29CQUN4QyxJQUFJLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQztvQkFDakMsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7b0JBRTVCLEtBQUssSUFBSSxRQUFRLElBQUksWUFBWSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDbEQsSUFBSSxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFOzRCQUNwRSxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7NEJBQ3pCLE1BQU07eUJBQ1Q7cUJBQ0o7b0JBRUQsSUFBSSxnQkFBZ0IsS0FBSyxLQUFLLEVBQUU7d0JBQzVCLElBQUksZ0JBQWdCLEtBQUssS0FBSyxFQUFFOzRCQUM1QixJQUFJLG9CQUFvQixHQUFHLElBQUksQ0FBQzs0QkFFaEMsS0FBSyxJQUFJLFFBQVEsSUFBSSxZQUFZLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dDQUNsRCxJQUFJLGtCQUFrQixDQUFDLG1DQUFtQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7b0NBQzlFLG9CQUFvQixHQUFHLEtBQUssQ0FBQztvQ0FDN0IsTUFBTTtpQ0FDVDs2QkFDSjs0QkFFRCxJQUFJLG9CQUFvQixLQUFLLElBQUksRUFBRTtnQ0FDL0IsUUFBUSxHQUFHLDJCQUEyQixDQUFDOzZCQUMxQzs0QkFFRCxnQkFBZ0IsR0FBRyxvQkFBb0IsQ0FBQzt5QkFDM0M7cUJBRUo7b0JBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzFGO2FBQ0o7WUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7WUFDbEMsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVhLDJCQUFRLEdBQXRCLFVBQXVCLFFBQWE7WUFDaEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBRW5CLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDO1lBQ3hCLElBQUksUUFBUSxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQzdCLElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDdkU7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDMUI7WUFFRCxJQUFJLENBQUMseUJBQXlCLEdBQUcsUUFBUSxDQUFDLHlCQUF5QixDQUFDO1lBQ3BFLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQztZQUMxQyxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUN4QixJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzdEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2FBQ3JCO1lBRUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO1lBQzVDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsaUJBQWlCLENBQUM7WUFDcEQsSUFBSSxRQUFRLENBQUMsZUFBZSxJQUFJLElBQUksRUFBRTtnQkFDbEMsSUFBSSxRQUFRLEdBQUcsb0JBQW9CLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ2pGO2lCQUFNO2dCQUNILElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO2FBQy9CO1lBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUUzQixJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7Z0JBQ25DLEtBQUssSUFBSSxHQUFHLElBQUksUUFBUSxDQUFDLGdCQUFnQixFQUFFO29CQUN2QyxJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQy9DLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQy9EO2lCQUNKO2FBQ0o7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBdi9DYSxzQ0FBbUIsR0FBRyxDQUFDLGNBQWMsRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM1Riw0Q0FBeUIsR0FBRyxDQUFDLGNBQWMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbkksd0NBQXFCLEdBQUcsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNsRSx3Q0FBcUIsR0FBRyxDQUFDLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUN6Ryx3Q0FBcUIsR0FBRyxDQUFDLGlCQUFpQixFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsbUJBQW1CLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN2Siw2Q0FBMEIsR0FBRyxDQUFDLFlBQVksRUFBRSx1QkFBdUIsRUFBRSxtQkFBbUIsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsa0NBQWtDLEVBQUUsdUJBQXVCLEVBQUUsb0JBQW9CLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUNqTyx3Q0FBcUIsR0FBRyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM1RSwrQ0FBNEIsR0FBRyxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3RFLDJDQUF3QixHQUFHLENBQUMsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLGlCQUFpQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsMEJBQTBCLEVBQUUscUJBQXFCLEVBQUUsbUJBQW1CLEVBQUUsbUJBQW1CLEVBQUUsZUFBZSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzNULHlDQUFzQixHQUFHLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDckksb0NBQWlCLEdBQUcsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUN0RyxzQ0FBbUIsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsdUJBQXVCLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsNkJBQTZCLEVBQUUsVUFBVSxFQUFFLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUM3TSx3Q0FBcUIsR0FBRyxDQUFDLHFCQUFxQixFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLG1CQUFtQixFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsc0JBQXNCLEVBQUUsdUJBQXVCLEVBQUUsc0JBQXNCLEVBQUUsd0JBQXdCLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLHFCQUFxQixFQUFFLGtCQUFrQixFQUFFLHNCQUFzQixFQUFFLGdCQUFnQixFQUFFLGtCQUFrQixFQUFFLHFCQUFxQixFQUFFLFlBQVksRUFBRSxpQkFBaUIsRUFBRSx3QkFBd0IsRUFBRSwyQkFBMkIsRUFBRSx1QkFBdUIsRUFBRSxzQkFBc0IsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztRQUNybkIsdUNBQW9CLEdBQUcsQ0FBQyxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsZ0JBQWdCLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLHVCQUF1QixFQUFFLGdDQUFnQyxFQUFFLGNBQWMsRUFBRSxvQkFBb0IsRUFBRSxpQkFBaUIsRUFBRSxxQkFBcUIsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSw2QkFBNkIsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUMxWCwrQ0FBNEIsR0FBRyxDQUFDLGtCQUFrQixFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDckgsMENBQXVCLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxtQkFBbUIsRUFBRSxrQkFBa0IsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDeE4sOENBQTJCLEdBQUcsQ0FBQyxRQUFRLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUN6Syw0Q0FBeUIsR0FBRyxDQUFDLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN0RyxvQ0FBaUIsR0FBRyxDQUFDLGtCQUFrQixFQUFFLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBQy9FLG9DQUFpQixHQUFHLENBQUMsNkJBQTZCLEVBQUUsNkJBQTZCLEVBQUUsV0FBVyxFQUFFLHlCQUF5QixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDck0sMENBQXVCLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3JELHlDQUFzQixHQUFHLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDeEYsNENBQXlCLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQy9LLDREQUF5QyxHQUFHLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQ3BFLHdDQUFxQixHQUFHLENBQUMsY0FBYyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxzQkFBc0IsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLDhCQUE4QixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3pMLHVDQUFvQixHQUFHLENBQUMsc0JBQXNCLEVBQUUsY0FBYyxFQUFFLG1CQUFtQixFQUFFLDJCQUEyQixFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsd0JBQXdCLEVBQUUsYUFBYSxFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDclUsaURBQThCLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxtQkFBbUIsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGFBQWEsRUFBRSwwQkFBMEIsRUFBRSxzQkFBc0IsRUFBRSxnQ0FBZ0MsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNoWCwyQ0FBd0IsR0FBRyxDQUFDLGNBQWMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNwSSxvREFBaUMsR0FBRyxDQUFDLFlBQVksRUFBRSxxQkFBcUIsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsc0JBQXNCLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDck8sMENBQXVCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2pJLCtDQUE0QixHQUFHLENBQUMsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixDQUFDLENBQUM7UUFDdEssMENBQXVCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2pJLHNDQUFtQixHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsbUJBQW1CLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztRQUNsSixrREFBK0IsR0FBRyxDQUFDLG1CQUFtQixFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMzRSxrREFBK0IsR0FBRyxDQUFDLDBCQUEwQixFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzlFLHlEQUFzQyxHQUFHLENBQUMsY0FBYyxFQUFFLG9CQUFvQixFQUFFLGtCQUFrQixFQUFFLG9CQUFvQixFQUFFLFlBQVksRUFBRSx1QkFBdUIsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQzFPLGtEQUErQixHQUFHLENBQUMsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSwyQkFBMkIsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzFKLDJDQUF3QixHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksRUFBRSxpQkFBaUIsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3BILGlEQUE4QixHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDckksc0RBQW1DLEdBQUcsQ0FBQyxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDbEUsMkRBQXdDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNoRixrREFBK0IsR0FBRyxDQUFDLHdCQUF3QixFQUFFLG9CQUFvQixFQUFFLHNCQUFzQixFQUFFLHVCQUF1QixFQUFFLDZCQUE2QixFQUFFLHNCQUFzQixFQUFFLDJCQUEyQixFQUFFLG1CQUFtQixFQUFFLGlCQUFpQixFQUFFLDZCQUE2QixFQUFFLG9CQUFvQixFQUFFLG9CQUFvQixFQUFFLHVCQUF1QixFQUFFLDZCQUE2QixFQUFFLHVCQUF1QixDQUFDLENBQUM7UUFDNVosK0NBQTRCLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1FBQzNFLG9DQUFpQixHQUFHLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2hGLCtDQUE0QixHQUFHLENBQUMsdUJBQXVCLEVBQUUsZUFBZSxFQUFFLGtCQUFrQixFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDaEgsMkNBQXdCLEdBQUcsQ0FBQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsMkJBQTJCLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM3Tyw0Q0FBeUIsR0FBRyxDQUFDLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN2RCwyQ0FBd0IsR0FBRyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sRUFBRSwwQkFBMEIsRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RNLDRDQUF5QixHQUFHLENBQUMsb0JBQW9CLEVBQUUscUJBQXFCLENBQUMsQ0FBQztRQUMxRSxzREFBbUMsR0FBRyxDQUFDLHVCQUF1QixFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsb0JBQW9CLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUN4Six5Q0FBc0IsR0FBRyxDQUFDLGlCQUFpQixFQUFFLHNCQUFzQixFQUFFLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNoRyxxQ0FBa0IsR0FBRyxDQUFDLFlBQVksRUFBRSwyQkFBMkIsRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxtQkFBbUIsRUFBRSxpQkFBaUIsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBczhDekwseUJBQUM7S0FBQSxBQTMvQ0QsSUEyL0NDO0lBQ0QsT0FBUyxrQkFBa0IsQ0FBQyJ9