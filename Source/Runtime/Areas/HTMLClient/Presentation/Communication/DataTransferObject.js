/// <amd-module name="Communication/DataTransferObject" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Communication/DataTransferObject", ["require", "exports", "Class", "Management/ComponentManager", "Utility/Convert"], function (require, exports, Class, ComponentManager, Convert) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller DataTransferObjects. Setzt Standard-Properties und bietet Funktionalität
    * zur erleichterten DataTransferObject-Entwicklung
    */
    var DataTransferObject = /** @class */ (function (_super) {
        __extends(DataTransferObject, _super);
        function DataTransferObject(params) {
            var _this = _super.call(this) || this;
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(DataTransferObject);
            _this.xmlNamespace = _this.paramOrDefault(params.xmlNamespace, null);
            _this.xmlProperties = _this.paramOrDefault(params.xmlProperties, []);
            return _this;
        }
        /*
        * Setzt den Wert eines Properties dieser Instanz unter Beachtung des Typs
        */
        DataTransferObject.prototype.setProperty = function (prop, value) {
            var propValue = this[prop];
            var isObservable = false;
            if (ko.isObservable(propValue)) {
                isObservable = true;
                propValue = propValue();
            }
            if (value && value['@i:nil'] === 'true') {
                propValue = null;
            }
            else if (_.isBoolean(propValue)) {
                propValue = JSON.parse(value);
            }
            else if (_.isNumber(propValue)) {
                propValue = JSON.parse(value);
            }
            else {
                propValue = value;
            }
            if (isObservable === true) {
                this[prop](propValue);
            }
            else {
                this[prop] = propValue;
            }
        };
        /*
        * Erzeugt aus der Instanz ein JS-Objekt, welches die XML-Properties beinhaltet
        */
        DataTransferObject.prototype.getXMLObject = function () {
            var object = {};
            object[this.type] = {};
            if (this.xmlNamespace != null) {
                object[this.type]['@xmlns:i'] = 'http://www.w3.org/2001/XMLSchema-instance';
                object[this.type]['@xmlns'] = 'http://schemas.datacontract.org/2004/07/' + this.xmlNamespace;
            }
            for (var i = 0; i < this.xmlProperties.length; i++) {
                var xmlProp = this.xmlProperties[i];
                var value = ko.toJS(this[xmlProp]);
                if (value != null) {
                    if (_.isArray(value)) {
                        for (var j = 0; j < value.length; j++) {
                            if (value[j] != null) {
                                if (value[j].getXMLObject != null) {
                                    var innerObject = value[j].getXMLObject();
                                    var type = _.keys(innerObject)[0];
                                    if (object[this.type][xmlProp] == null) {
                                        object[this.type][xmlProp] = {};
                                        object[this.type][xmlProp][type] = [];
                                    }
                                    object[this.type][xmlProp][type].push(innerObject[type]);
                                }
                                else {
                                    object[this.type][xmlProp].push(value[j]);
                                }
                            }
                        }
                    }
                    else {
                        if (value.getXMLObject != null) {
                            var innerObject = value.getXMLObject();
                            object[this.type][xmlProp] = innerObject;
                        }
                        else {
                            object[this.type][xmlProp] = value;
                        }
                    }
                }
                else {
                    object[this.type][xmlProp] = null;
                }
            }
            return object;
        };
        /*
         * Setzt die Properties eines Objekts auf der Instanz eines DataTransferObjects
         */
        DataTransferObject.setProperties = function (instance, object, convertToArray) {
            for (var i in instance) {
                for (var j in object) {
                    if (i === j) {
                        if (instance.xmlProperties.indexOf(i) === -1) {
                            instance.xmlProperties.push(i);
                        }
                        if (_.isArray(instance[i]) === false) {
                            instance.setProperty(i, object[j]);
                        }
                        else {
                            if (object[j] != null) {
                                // Wenn zwar ein Array erwartet wird, aber nur ein Objekt vorhanden ist, dann stecke das Objekt
                                // in ein neues Array, falls das entsprechende Flag "convertToArray" gesetzt ist.
                                // Das Objekt wird somit als ein-elementiges Array interpretiert.
                                if (!_.isArray(object[j]) && convertToArray) {
                                    object[j] = [object[j]];
                                }
                                if (_.isArray(object[j])) {
                                    for (var k = 0; k < object[j].length; k++) {
                                        var value = object[j][k];
                                        if (_.isObject(value)) {
                                            value = DataTransferObject.createInstance(j, object[j][k]);
                                        }
                                        instance[i].push(value);
                                    }
                                }
                                else {
                                    var keys = _.keys(object[j]);
                                    var alias = _.find(keys, function (key) {
                                        return key.substring(0, 1) !== '@';
                                    });
                                    if (object[j][alias] != null) {
                                        if (_.isArray(object[j][alias]) === false) {
                                            object[j][alias] = [object[j][alias]];
                                        }
                                        for (var k = 0; k < object[j][alias].length; k++) {
                                            var value = object[j][alias][k];
                                            if (_.isObject(value)) {
                                                value = DataTransferObject.createInstance(alias, object[j][alias][k]);
                                            }
                                            instance[i].push(value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        /*
         * Erzeugt eine neue DataTransferObject-Instanz anhand eines Alias
         */
        DataTransferObject.createInstance = function (alias, params, convertToArray) {
            var Type = ComponentManager.getModel(alias);
            if (Type == null) {
                return null;
            }
            var model = new Type();
            if (params != null) {
                DataTransferObject.setProperties(model, params, convertToArray);
            }
            if (model.init != null) {
                model.init();
            }
            return model;
        };
        /*
         * Erzeugt eine neue DataTransferObject-Instanz auf Basis eines JS-Objekts
         */
        DataTransferObject.parseFromObject = function (object, convertToArray) {
            if (object == null) {
                return null;
            }
            var keys = _.keys(object);
            if (keys.length !== 1) {
                return null;
            }
            var alias = keys[0];
            var root = object[alias];
            var instance = DataTransferObject.createInstance(alias, root, convertToArray);
            return instance;
        };
        /*
         * Erzeugt eine neue DataTransferObject-Instanz auf Basis eines XML-Strings
         */
        DataTransferObject.parseFromXML = function (xml, convertToArray) {
            var object = Convert.XMLStringToObject(xml);
            var model = DataTransferObject.parseFromObject(object, convertToArray);
            return model;
        };
        /*
         * Erzeugt einen XML-String aus einer DataTransferObject-Instanz
         */
        DataTransferObject.toXMLString = function (instance) {
            var object = instance.getXMLObject();
            var xml = Convert.ObjectToXMLString(object);
            return xml;
        };
        return DataTransferObject;
    }(Class));
    return DataTransferObject;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YVRyYW5zZmVyT2JqZWN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiRGF0YVRyYW5zZmVyT2JqZWN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBEQUEwRDs7Ozs7Ozs7Ozs7OztJQU0xRDs7O01BR0U7SUFDRjtRQUFpQyxzQ0FBSztRQVlsQyw0QkFBWSxNQUFNO1lBQWxCLFlBQ0ksaUJBQU8sU0FPVjtZQUxHLHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUVsRCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNuRSxLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQzs7UUFDdkUsQ0FBQztRQUVEOztVQUVFO1FBQ0ssd0NBQVcsR0FBbEIsVUFBbUIsSUFBWSxFQUFFLEtBQWE7WUFDMUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztZQUV6QixJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzVCLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLFNBQVMsR0FBRyxTQUFTLEVBQUUsQ0FBQzthQUMzQjtZQUVELElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxNQUFNLEVBQUU7Z0JBQ3JDLFNBQVMsR0FBRyxJQUFJLENBQUM7YUFDcEI7aUJBQU0sSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUMvQixTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNqQztpQkFBTSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzlCLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2pDO2lCQUFNO2dCQUNILFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDckI7WUFFRCxJQUFJLFlBQVksS0FBSyxJQUFJLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN6QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDO2FBQzFCO1FBQ0wsQ0FBQztRQUVEOztVQUVFO1FBQ0sseUNBQVksR0FBbkI7WUFDSSxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFFdkIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksRUFBRTtnQkFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRywyQ0FBMkMsQ0FBQztnQkFDNUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRywwQ0FBMEMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2FBQ2hHO1lBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNoRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUVuQyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNsQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDbkMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFO2dDQUNsQixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFO29DQUMvQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7b0NBQzFDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBRWxDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUU7d0NBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO3dDQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQ0FDekM7b0NBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUNBQzVEO3FDQUFNO29DQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lDQUM3Qzs2QkFDSjt5QkFDSjtxQkFDSjt5QkFBTTt3QkFDSCxJQUFJLEtBQUssQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFOzRCQUM1QixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7NEJBQ3ZDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsV0FBVyxDQUFDO3lCQUM1Qzs2QkFBTTs0QkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEtBQUssQ0FBQzt5QkFDdEM7cUJBQ0o7aUJBQ0o7cUJBQU07b0JBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUM7aUJBQ3JDO2FBQ0o7WUFFRCxPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyxnQ0FBYSxHQUEzQixVQUE0QixRQUE0QixFQUFFLE1BQWMsRUFBRSxjQUF3QjtZQUM5RixLQUFLLElBQUksQ0FBQyxJQUFJLFFBQVEsRUFBRTtnQkFDcEIsS0FBSyxJQUFJLENBQUMsSUFBSSxNQUFNLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDVCxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFOzRCQUMxQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDbEM7d0JBRUQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTs0QkFDbEMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3RDOzZCQUFNOzRCQUNILElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRTtnQ0FFbkIsK0ZBQStGO2dDQUMvRixpRkFBaUY7Z0NBQ2pGLGlFQUFpRTtnQ0FDakUsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksY0FBYyxFQUFFO29DQUN6QyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQ0FDM0I7Z0NBRUQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29DQUN0QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTt3Q0FDdkMsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dDQUV6QixJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7NENBQ25CLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3lDQUM5RDt3Q0FFRCxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FDQUMzQjtpQ0FDSjtxQ0FBTTtvQ0FDSCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUM3QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFDLEdBQVc7d0NBQ2pDLE9BQU8sR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDO29DQUN2QyxDQUFDLENBQUMsQ0FBQztvQ0FFSCxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUU7d0NBQzFCLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUU7NENBQ3ZDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3lDQUN6Qzt3Q0FFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0Q0FDOUMsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRDQUVoQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0RBQ25CLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZDQUN6RTs0Q0FFRCxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lDQUMzQjtxQ0FDSjtpQ0FDSjs2QkFDSjt5QkFDSjtxQkFDSjtpQkFDSjthQUNKO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ1csaUNBQWMsR0FBNUIsVUFBNkIsS0FBYSxFQUFFLE1BQVcsRUFBRSxjQUF3QjtZQUM3RSxJQUFJLElBQUksR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFRLENBQUM7WUFFbkQsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLEtBQUssR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1lBRXZCLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDaEIsa0JBQWtCLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUM7YUFDbkU7WUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNwQixLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDaEI7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQ7O1dBRUc7UUFDVyxrQ0FBZSxHQUE3QixVQUE4QixNQUFXLEVBQUUsY0FBdUI7WUFDOUQsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO2dCQUNoQixPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUUxQixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUNuQixPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QixJQUFJLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztZQUU5RSxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDO1FBRUQ7O1dBRUc7UUFDVywrQkFBWSxHQUExQixVQUEyQixHQUFXLEVBQUUsY0FBdUI7WUFDM0QsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLElBQUksS0FBSyxHQUFHLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUM7WUFFdkUsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUVEOztXQUVHO1FBQ1csOEJBQVcsR0FBekIsVUFBMEIsUUFBNEI7WUFDbEQsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3JDLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUU1QyxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUM7UUFDTCx5QkFBQztJQUFELENBQUMsQUFuT0QsQ0FBaUMsS0FBSyxHQW1PckM7SUFFRCxPQUFTLGtCQUFrQixDQUFDIn0=