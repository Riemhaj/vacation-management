/// <amd-module name="Communication/RADEnvironmentProxy" />
define("Communication/RADEnvironmentProxy", ["require", "exports", "Management/RADApplication", "Communication/RADGeneratedModels", "Utility/ExceptionBuilder", "Communication/RADEnvironmentProxyInterceptor"], function (require, exports, RADApplication, RADGeneratedModels, ExceptionBuilder, RADEnvironmentProxyInterceptor) {
    "use strict";
    var RADEnvironmentProxy = /** @class */ (function () {
        function RADEnvironmentProxy() {
            this._activeCommunicationCounter = 0;
            RADEnvironmentProxy.clientAction = ko.observable(null).extend({ notify: 'always' });
            RADEnvironmentProxyInterceptor.intercept(this);
        }
        RADEnvironmentProxy.getInstance = function () {
            if (RADEnvironmentProxy.instance == null)
                RADEnvironmentProxy.instance = new RADEnvironmentProxy();
            return RADEnvironmentProxy.instance;
        };
        RADEnvironmentProxy.prototype._args = function (keys, args) {
            var result = { token: RADApplication.token };
            for (var i = 0; i < keys.length; i++) {
                result[keys[i]] = args[0][keys[i]];
            }
            return result;
        };
        RADEnvironmentProxy.prototype._options = function (args) {
            return $.extend(true, {
                _convert: true,
                _async: true,
                _fail: function (operation, keys, jqXHR) {
                    if (jqXHR.status === 500) {
                        ExceptionBuilder.throwDotNet(operation, keys, jqXHR.responseText, args[0]);
                    }
                    else if (jqXHR.status === 501 || jqXHR.status === 404 || jqXHR.status === 503) {
                        ExceptionBuilder.onCommunicationError();
                    }
                    else if (jqXHR.status === 401) {
                        RADApplication.unauthorizedRequestCallback();
                    }
                    else if (jqXHR.status != 200) {
                        RADApplication.noConnectionToServerCallback();
                    }
                }
            }, args[0]);
        };
        RADEnvironmentProxy.prototype._convert = function (params, value) {
            var _converters = {
                DTO: function (value) {
                    if (params.type in RADGeneratedModels) {
                        return RADGeneratedModels[params.type](value);
                    }
                    return value;
                },
                Array: function (value) {
                    if (params.type in RADGeneratedModels) {
                        var result = [];
                        for (var i = 0; i < value.length; i++) {
                            result.push(RADGeneratedModels[params.type](value[i]));
                        }
                        return result;
                    }
                    return value;
                },
                Object: function (value) {
                    if (params.type in RADGeneratedModels) {
                        var result = {};
                        for (var key in value) {
                            result[key] = RADGeneratedModels[params.type](value[key]);
                        }
                        return result;
                    }
                    return value;
                },
                Raw: function (value) {
                    return value;
                }
            };
            return _converters[params.returns](value);
        };
        RADEnvironmentProxy.prototype._addServerComFlag = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            proxy._activeCommunicationCounter++;
            $('body').addClass('activeServerCommunication');
        };
        RADEnvironmentProxy.prototype._removeServerComFlag = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            if (--proxy._activeCommunicationCounter <= 0) {
                $('body').removeClass('activeServerCommunication');
                proxy._activeCommunicationCounter = 0;
            }
        };
        RADEnvironmentProxy.prototype._deferred = function (params) {
            var proxy = RADEnvironmentProxy.getInstance();
            var args = proxy._args(params.keys, params.arguments);
            var options = proxy._options(params.arguments);
            var isWorker = params.operation === 'keepAlive' || params.operation === 'getCurrentUserTaskInfo';
            if (!isWorker) {
                RADEnvironmentProxy.clientAction({ operation: params.operation, args: args });
            }
            proxy._addServerComFlag();
            var deferred = $.ajax({
                url: MODEL.HTMLClientBaseURL + 'HTMLClient/Json/' + params.operation,
                type: 'POST',
                headers: { '__RequestVerificationToken': RADApplication.xcsrfToken },
                cache: false,
                async: options._async,
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(args)
            });
            if (options._convert) {
                deferred = deferred.pipe(function (value) {
                    return proxy._convert(params, value);
                });
            }
            if (!isWorker) {
                deferred.fail(function (jqXHR) {
                    options._fail(params.operation, params.keys, jqXHR);
                });
            }
            deferred.always(function () {
                proxy._removeServerComFlag();
            });
            return deferred;
        };
        RADEnvironmentProxy.applyDetailViewContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'applyDetailViewContext',
                returns: 'DTO',
                type: 'validationDTO',
                keys: ['detailViewContextID', 'values']
            });
        };
        RADEnvironmentProxy.aquireLockObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'aquireLockObject',
                returns: 'Raw',
                type: 'boolean',
                keys: ['lockGroupID', 'pdoID']
            });
        };
        RADEnvironmentProxy.autoLogout = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'autoLogout',
                returns: 'Raw',
                type: 'boolean',
                keys: []
            });
        };
        RADEnvironmentProxy.beginTransaction = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'beginTransaction',
                returns: 'Raw',
                type: 'boolean',
                keys: []
            });
        };
        RADEnvironmentProxy.callOperation = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'callOperation',
                returns: 'DTO',
                type: 'operationResultDTO',
                keys: ['pdoID', 'operationName', 'operationParameters', 'contextParameters']
            });
        };
        RADEnvironmentProxy.callOperationFromDVContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'callOperationFromDVContext',
                returns: 'DTO',
                type: 'operationResultDTO',
                keys: ['detailViewContextID', 'operationName', 'operationParameters', 'property_values', 'contextParameters', 'nestedContextName', 'withValidation']
            });
        };
        RADEnvironmentProxy.callStaticOperation = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'callStaticOperation',
                returns: 'DTO',
                type: 'operationResultDTO',
                keys: ['pdTypeName', 'operationName', 'operationParameters', 'contextParameters']
            });
        };
        RADEnvironmentProxy.callStaticOperationWithoutTransaction = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'callStaticOperationWithoutTransaction',
                returns: 'DTO',
                type: 'operationResultDTO',
                keys: ['pdTypeName', 'operationName', 'operationParameters', 'contextParameters']
            });
        };
        RADEnvironmentProxy.changeLanguage = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'changeLanguage',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['lang']
            });
        };
        RADEnvironmentProxy.changePassword = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'changePassword',
                returns: 'DTO',
                type: 'changePasswordResponseDTO',
                keys: ['oldPassword', 'newPassword']
            });
        };
        RADEnvironmentProxy.changePersistenceContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'changePersistenceContext',
                returns: 'Raw',
                type: 'boolean',
                keys: ['persistenceContextID']
            });
        };
        RADEnvironmentProxy.changePrincipal = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'changePrincipal',
                returns: 'DTO',
                type: 'principalDTO',
                keys: ['principal']
            });
        };
        RADEnvironmentProxy.commitTransaction = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'commitTransaction',
                returns: 'Raw',
                type: 'boolean',
                keys: []
            });
        };
        RADEnvironmentProxy.connect = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'connect',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdoID', 'assoziationName', 'topdTypeName', 'toPDOID', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.connectMany = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'connectMany',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdoID', 'assoziationName', 'topdTypeName', 'toPDOIDs', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.createAndLockInlineEditingObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createAndLockInlineEditingObject',
                returns: 'DTO',
                type: 'genericDTO',
                keys: ['pdoID', 'contextRelationName', 'lockGroupID']
            });
        };
        RADEnvironmentProxy.createDetailViewContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createDetailViewContext',
                returns: 'DTO',
                type: 'detailViewContextDTO',
                keys: ['typeName', 'pdoID', 'acquireLock', 'contextTypeName', 'contextRelationName', 'contextParameters']
            });
        };
        RADEnvironmentProxy.createDetailViewContextAndConnectNew = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createDetailViewContextAndConnectNew',
                returns: 'DTO',
                type: 'detailViewContextDTO',
                keys: ['typeName', 'contextPDOID', 'contextTypeName', 'contextRelationName', 'contextParameters']
            });
        };
        RADEnvironmentProxy.createInlineEditingContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createInlineEditingContext',
                returns: 'DTO',
                type: 'inlineEditingContextDTO',
                keys: ['pdTypeName', 'contextPdoId', 'contextRelationName', 'detailViewContextID']
            });
        };
        RADEnvironmentProxy.createIterator = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createIterator',
                returns: 'DTO',
                type: 'iteratorDTO',
                keys: ['pdTypeName', 'properties', 'filter', 'orderby', 'retrieveExtentCount', 'contextPDO_ID']
            });
        };
        RADEnvironmentProxy.createIteratorForAssoziation = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createIteratorForAssoziation',
                returns: 'DTO',
                type: 'iteratorDTO',
                keys: ['pdTypeName', 'pdo_id', 'assoziationName', 'properties', 'filter', 'orderby', 'retrieveExtentCount', 'itOperator']
            });
        };
        RADEnvironmentProxy.createIteratorWithOperator = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createIteratorWithOperator',
                returns: 'DTO',
                type: 'iteratorDTO',
                keys: ['pdTypeName', 'properties', 'filter', 'orderby', 'retrieveExtentCount', 'contextPDO_ID', 'itOperator']
            });
        };
        RADEnvironmentProxy.createLockGroup = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createLockGroup',
                returns: 'Raw',
                type: 'int64',
                keys: []
            });
        };
        RADEnvironmentProxy.createPersistenceContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'createPersistenceContext',
                returns: 'Raw',
                type: 'int64',
                keys: []
            });
        };
        RADEnvironmentProxy.deleteDetailViewContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteDetailViewContext',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['detailViewContextID']
            });
        };
        RADEnvironmentProxy.deleteDetailViewContextStack = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteDetailViewContextStack',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['detailViewContextIdStack']
            });
        };
        RADEnvironmentProxy.deleteIterator = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteIterator',
                returns: 'Raw',
                type: 'boolean',
                keys: ['iteratorID']
            });
        };
        RADEnvironmentProxy.deleteLockGroup = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteLockGroup',
                returns: 'Raw',
                type: 'boolean',
                keys: ['lockGroupID']
            });
        };
        RADEnvironmentProxy.deleteManyObjects = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteManyObjects',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdTypeName', 'pdoIDs']
            });
        };
        RADEnvironmentProxy.deleteObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deleteObject',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdTypeName', 'pdoID']
            });
        };
        RADEnvironmentProxy.deletePersistenceContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'deletePersistenceContext',
                returns: 'Raw',
                type: 'boolean',
                keys: ['persistenceContextID']
            });
        };
        RADEnvironmentProxy.describeSearchResult = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'describeSearchResult',
                returns: 'Object',
                type: 'string',
                keys: ['pdoId']
            });
        };
        RADEnvironmentProxy.disconnect = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'disconnect',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdoID', 'assoziationName', 'topdTypeName', 'toPDOID', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.disconnectMany = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'disconnectMany',
                returns: 'DTO',
                type: 'changeObjectDTO',
                keys: ['pdoID', 'assoziationName', 'topdTypeName', 'toPDOIDs', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.exportData = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'exportData',
                returns: 'Raw',
                type: 'string',
                keys: ['iteratorID', 'exportType', 'columns', 'filter']
            });
        };
        RADEnvironmentProxy.flushErrorMessages = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'flushErrorMessages',
                returns: 'Object',
                type: 'iList`1',
                keys: []
            });
        };
        RADEnvironmentProxy.getAllResourceItems = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getAllResourceItems',
                returns: 'Object',
                type: 'string',
                keys: ['lang']
            });
        };
        RADEnvironmentProxy.getApplication = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getApplication',
                returns: 'DTO',
                type: 'applicationDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getAssignedPrincipals = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getAssignedPrincipals',
                returns: 'Array',
                type: 'principalDTO',
                keys: ['username']
            });
        };
        RADEnvironmentProxy.getAvailableSearchParameters = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getAvailableSearchParameters',
                returns: 'DTO',
                type: 'searchParameterDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getCurrentUserTaskInfo = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getCurrentUserTaskInfo',
                returns: 'DTO',
                type: 'taskInformationDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getDetailView = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getDetailView',
                returns: 'DTO',
                type: 'detailViewDTO',
                keys: ['pdoID']
            });
        };
        RADEnvironmentProxy.getExtent = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getExtent',
                returns: 'Raw',
                type: 'object[][]',
                keys: ['iteratorID', 'start', 'end']
            });
        };
        RADEnvironmentProxy.getExtentCount = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getExtentCount',
                returns: 'Raw',
                type: 'int64',
                keys: ['iteratorID']
            });
        };
        RADEnvironmentProxy.getExtentWithPDOIDAsFirstColumn = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getExtentWithPDOIDAsFirstColumn',
                returns: 'Raw',
                type: 'object[][]',
                keys: ['iteratorID', 'start', 'end']
            });
        };
        RADEnvironmentProxy.getListView = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getListView',
                returns: 'DTO',
                type: 'listViewDTO',
                keys: ['listViewPDO_ID']
            });
        };
        RADEnvironmentProxy.getListViewByName = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getListViewByName',
                returns: 'DTO',
                type: 'listViewDTO',
                keys: ['listViewName']
            });
        };
        RADEnvironmentProxy.getListViewByObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getListViewByObject',
                returns: 'DTO',
                type: 'listViewDTO',
                keys: ['listViewName', 'pdoID']
            });
        };
        RADEnvironmentProxy.getMenuImagesSprite = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getMenuImagesSprite',
                returns: 'DTO',
                type: 'imageSpriteDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getMenuTree = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getMenuTree',
                returns: 'DTO',
                type: 'navigationNodeDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getNavigationTree = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getNavigationTree',
                returns: 'DTO',
                type: 'navigationNodeDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getObject',
                returns: 'DTO',
                type: 'genericDTO',
                keys: ['pdTypeName', 'pdoID', 'properties']
            });
        };
        RADEnvironmentProxy.getObjectHistoryInformations = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getObjectHistoryInformations',
                returns: 'DTO',
                type: 'objectHistoryInformationsDTO',
                keys: ['pdtypename', 'pdoID']
            });
        };
        RADEnvironmentProxy.getObjects = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getObjects',
                returns: 'Array',
                type: 'genericDTO',
                keys: ['iteratorID', 'start', 'end']
            });
        };
        RADEnvironmentProxy.getPrincipals = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getPrincipals',
                returns: 'Array',
                type: 'principalDTO',
                keys: []
            });
        };
        RADEnvironmentProxy.getProperties = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getProperties',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['pdoID', 'properties']
            });
        };
        RADEnvironmentProxy.getPropertiesForDetailViewContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getPropertiesForDetailViewContext',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['detailViewContextID', 'pdoID', 'properties']
            });
        };
        RADEnvironmentProxy.getResourceItem = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getResourceItem',
                returns: 'Raw',
                type: 'string',
                keys: ['lang', 'resourceName']
            });
        };
        RADEnvironmentProxy.getResourceItems = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getResourceItems',
                returns: 'Object',
                type: 'string',
                keys: ['lang', 'resourceNames']
            });
        };
        RADEnvironmentProxy.getResourceItemsForPDO = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getResourceItemsForPDO',
                returns: 'Object',
                type: 'string',
                keys: ['lang', 'pdo_id']
            });
        };
        RADEnvironmentProxy.getResourceItemsForType = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getResourceItemsForType',
                returns: 'Object',
                type: 'string',
                keys: ['lang', 'pdTypeName']
            });
        };
        RADEnvironmentProxy.getResourceItemsForTypes = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getResourceItemsForTypes',
                returns: 'Object',
                type: 'iDictionary`2',
                keys: ['lang', 'pdTypeNames']
            });
        };
        RADEnvironmentProxy.getSearchExtent = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getSearchExtent',
                returns: 'DTO',
                type: 'searchResultDTO',
                keys: ['start', 'end']
            });
        };
        RADEnvironmentProxy.getSearchSuggestions = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getSearchSuggestions',
                returns: 'Raw',
                type: 'string[]',
                keys: ['query']
            });
        };
        RADEnvironmentProxy.getSummaryView = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'getSummaryView',
                returns: 'DTO',
                type: 'summaryViewContextDTO',
                keys: ['pdtypename', 'pdoID']
            });
        };
        RADEnvironmentProxy.keepAlive = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'keepAlive',
                returns: 'DTO',
                type: 'keepAliveDTO',
                keys: ['clientTime']
            });
        };
        RADEnvironmentProxy.login = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'login',
                returns: 'DTO',
                type: 'loginDTO',
                keys: ['username', 'password', 'principal']
            });
        };
        RADEnvironmentProxy.loginEncoded = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'loginEncoded',
                returns: 'DTO',
                type: 'loginDTO',
                keys: ['username', 'password', 'principal']
            });
        };
        RADEnvironmentProxy.logout = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'logout',
                returns: 'Raw',
                type: 'boolean',
                keys: []
            });
        };
        RADEnvironmentProxy.newObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'newObject',
                returns: 'DTO',
                type: 'genericDTO',
                keys: ['pdTypeName']
            });
        };
        RADEnvironmentProxy.outputMessage = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'outputMessage',
                returns: 'Raw',
                type: 'boolean',
                keys: ['message', 'priority']
            });
        };
        RADEnvironmentProxy.refreshDetailViewContext = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'refreshDetailViewContext',
                returns: 'DTO',
                type: 'detailViewContextDTO',
                keys: ['detailViewContextID']
            });
        };
        RADEnvironmentProxy.releaseLockedObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'releaseLockedObject',
                returns: 'Raw',
                type: 'boolean',
                keys: ['lockGroupID', 'pdoID']
            });
        };
        RADEnvironmentProxy.rollbackTransaction = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'rollbackTransaction',
                returns: 'Raw',
                type: 'boolean',
                keys: []
            });
        };
        RADEnvironmentProxy.saveObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'saveObject',
                returns: 'DTO',
                type: 'saveResultDTO',
                keys: ['pdoID', 'lockGroupID']
            });
        };
        RADEnvironmentProxy.saveObjectWithTransferProperties = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'saveObjectWithTransferProperties',
                returns: 'DTO',
                type: 'saveResultDTO',
                keys: ['pdoID', 'lockGroupID', 'values']
            });
        };
        RADEnvironmentProxy.search = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'search',
                returns: 'DTO',
                type: 'searchResultDTO',
                keys: ['query']
            });
        };
        RADEnvironmentProxy.setProperties = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'setProperties',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['pdoID', 'values']
            });
        };
        RADEnvironmentProxy.swap = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'swap',
                returns: 'DTO',
                type: 'resultDTO',
                keys: ['pdoID', 'assoziationName', 'first', 'second']
            });
        };
        RADEnvironmentProxy.tryAutoLogin = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'tryAutoLogin',
                returns: 'DTO',
                type: 'loginDTO',
                keys: ['principal']
            });
        };
        RADEnvironmentProxy.tryToDeleteObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'tryToDeleteObject',
                returns: 'DTO',
                type: 'tryDeletionDTO',
                keys: ['pdTypeName', 'pdoIDs']
            });
        };
        RADEnvironmentProxy.tryToDisconnectObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'tryToDisconnectObject',
                returns: 'DTO',
                type: 'tryDeletionDTO',
                keys: ['pdoID', 'assoziationName', 'topdTypeName', 'pdoIDs']
            });
        };
        RADEnvironmentProxy.updateAndValidateProperties = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'updateAndValidateProperties',
                returns: 'DTO',
                type: 'updateAndValidateDTO',
                keys: ['detailViewContextID', 'valuesToUpdate', 'valuesToValidate', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.updateAndValidatePropertiesInGenericObject = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'updateAndValidatePropertiesInGenericObject',
                returns: 'DTO',
                type: 'updateAndValidateDTO',
                keys: ['pdoID', 'lockGroupID', 'valuesToUpdate', 'valuesToValidate', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.updateClientCache = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'updateClientCache',
                returns: 'DTO',
                type: 'clientUpdateDTO',
                keys: ['level']
            });
        };
        RADEnvironmentProxy.updateProperties = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'updateProperties',
                returns: 'DTO',
                type: 'updateDTO',
                keys: ['pdoID', 'values', 'scopedProperties', 'nestedContextName']
            });
        };
        RADEnvironmentProxy.updateUserSpecificData = function () {
            var proxy = RADEnvironmentProxy.getInstance();
            return proxy._deferred({
                arguments: arguments,
                operation: 'updateUserSpecificData',
                returns: 'Raw',
                type: 'boolean',
                keys: ['key', 'value']
            });
        };
        return RADEnvironmentProxy;
    }());
    return RADEnvironmentProxy;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUkFERW52aXJvbm1lbnRQcm94eS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlJBREVudmlyb25tZW50UHJveHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMkRBQTJEOzs7SUFhM0Q7UUFNSTtZQUNJLElBQUksQ0FBQywyQkFBMkIsR0FBRyxDQUFDLENBQUM7WUFDckMsbUJBQW1CLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7WUFDcEYsOEJBQThCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25ELENBQUM7UUFFYywrQkFBVyxHQUExQjtZQUNLLElBQUcsbUJBQW1CLENBQUMsUUFBUSxJQUFJLElBQUk7Z0JBQ25DLG1CQUFtQixDQUFDLFFBQVEsR0FBRyxJQUFJLG1CQUFtQixFQUFFLENBQUM7WUFFN0QsT0FBTyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7UUFDekMsQ0FBQztRQUVPLG1DQUFLLEdBQWIsVUFBYyxJQUFJLEVBQUUsSUFBSTtZQUNwQixJQUFJLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEM7WUFDRCxPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBRU8sc0NBQVEsR0FBaEIsVUFBaUIsSUFBSTtZQUNqQixPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxNQUFNLEVBQUUsSUFBSTtnQkFDWixLQUFLLEVBQUUsVUFBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUs7b0JBQzFCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7d0JBQ3RCLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQzlFO3lCQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7d0JBQzdFLGdCQUFnQixDQUFDLG9CQUFvQixFQUFFLENBQUM7cUJBQzNDO3lCQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUM7d0JBQzVCLGNBQWMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO3FCQUNoRDt5QkFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO3dCQUM1QixjQUFjLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztxQkFDakQ7Z0JBQ0wsQ0FBQzthQUNKLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEIsQ0FBQztRQUVPLHNDQUFRLEdBQWhCLFVBQWlCLE1BQU0sRUFBRSxLQUFLO1lBQzFCLElBQUksV0FBVyxHQUFHO2dCQUNkLEdBQUcsRUFBRSxVQUFDLEtBQUs7b0JBQ1AsSUFBSSxNQUFNLENBQUMsSUFBSSxJQUFJLGtCQUFrQixFQUFFO3dCQUNuQyxPQUFPLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDakQ7b0JBQ0QsT0FBTyxLQUFLLENBQUM7Z0JBQ2pCLENBQUM7Z0JBQ0QsS0FBSyxFQUFFLFVBQUMsS0FBSztvQkFDVCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksa0JBQWtCLEVBQUU7d0JBQ25DLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQzt3QkFDaEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQzFEO3dCQUNELE9BQU8sTUFBTSxDQUFDO3FCQUNqQjtvQkFDRCxPQUFPLEtBQUssQ0FBQztnQkFDakIsQ0FBQztnQkFDRCxNQUFNLEVBQUUsVUFBQyxLQUFLO29CQUNWLElBQUksTUFBTSxDQUFDLElBQUksSUFBSSxrQkFBa0IsRUFBRTt3QkFDbkMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO3dCQUNoQixLQUFLLElBQUksR0FBRyxJQUFJLEtBQUssRUFBRTs0QkFDbkIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt5QkFDN0Q7d0JBQ0QsT0FBTyxNQUFNLENBQUM7cUJBQ2pCO29CQUNELE9BQU8sS0FBSyxDQUFDO2dCQUNqQixDQUFDO2dCQUNELEdBQUcsRUFBRSxVQUFDLEtBQUs7b0JBQ1AsT0FBTyxLQUFLLENBQUM7Z0JBQ2pCLENBQUM7YUFDSixDQUFDO1lBQ0YsT0FBTyxXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFFTywrQ0FBaUIsR0FBekI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxLQUFLLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztZQUNwQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDcEQsQ0FBQztRQUVPLGtEQUFvQixHQUE1QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLElBQUksRUFBRSxLQUFLLENBQUMsMkJBQTJCLElBQUksQ0FBQyxFQUM1QztnQkFDSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxDQUFDLDJCQUEyQixDQUFDLENBQUM7Z0JBQ25ELEtBQUssQ0FBQywyQkFBMkIsR0FBRyxDQUFDLENBQUM7YUFDekM7UUFDTCxDQUFDO1FBRU8sdUNBQVMsR0FBakIsVUFBa0IsTUFBTTtZQUNwQixJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RELElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQy9DLElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxTQUFTLEtBQUssV0FBVyxJQUFJLE1BQU0sQ0FBQyxTQUFTLEtBQUssd0JBQXdCLENBQUM7WUFDakcsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDWCxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUNqRjtZQUNELEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQzFCLElBQUksUUFBUSxHQUFRLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLEdBQUcsRUFBRSxLQUFLLENBQUMsaUJBQWlCLEdBQUcsa0JBQWtCLEdBQUcsTUFBTSxDQUFDLFNBQVM7Z0JBQ3BFLElBQUksRUFBRSxNQUFNO2dCQUNaLE9BQU8sRUFBRSxFQUFFLDRCQUE0QixFQUFFLGNBQWMsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BFLEtBQUssRUFBRSxLQUFLO2dCQUNaLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTTtnQkFDckIsV0FBVyxFQUFFLGtCQUFrQjtnQkFDL0IsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzthQUM3QixDQUFDLENBQUM7WUFDSCxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7Z0JBQ2xCLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSztvQkFDM0IsT0FBTyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDekMsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUNELElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ1gsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUs7b0JBQ2hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN4RCxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDWixLQUFLLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUM7UUFFYSwwQ0FBc0IsR0FBcEM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsd0JBQXdCO2dCQUNuQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsZUFBZTtnQkFDckIsSUFBSSxFQUFFLENBQUMscUJBQXFCLEVBQUUsUUFBUSxDQUFDO2FBQzFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxvQ0FBZ0IsR0FBOUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsa0JBQWtCO2dCQUM3QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDO2FBQ2pDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw4QkFBVSxHQUF4QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxvQ0FBZ0IsR0FBOUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsa0JBQWtCO2dCQUM3QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxpQ0FBYSxHQUEzQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxlQUFlO2dCQUMxQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsb0JBQW9CO2dCQUMxQixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsZUFBZSxFQUFFLHFCQUFxQixFQUFFLG1CQUFtQixDQUFDO2FBQy9FLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw4Q0FBMEIsR0FBeEM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsNEJBQTRCO2dCQUN2QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsb0JBQW9CO2dCQUMxQixJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxlQUFlLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLEVBQUUsbUJBQW1CLEVBQUUsbUJBQW1CLEVBQUUsZ0JBQWdCLENBQUM7YUFDdkosQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHVDQUFtQixHQUFqQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxxQkFBcUI7Z0JBQ2hDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxvQkFBb0I7Z0JBQzFCLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxlQUFlLEVBQUUscUJBQXFCLEVBQUUsbUJBQW1CLENBQUM7YUFDcEYsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHlEQUFxQyxHQUFuRDtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSx1Q0FBdUM7Z0JBQ2xELE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxvQkFBb0I7Z0JBQzFCLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxlQUFlLEVBQUUscUJBQXFCLEVBQUUsbUJBQW1CLENBQUM7YUFDcEYsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGtDQUFjLEdBQTVCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGdCQUFnQjtnQkFDM0IsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQzthQUNqQixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsa0NBQWMsR0FBNUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsZ0JBQWdCO2dCQUMzQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsMkJBQTJCO2dCQUNqQyxJQUFJLEVBQUUsQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDO2FBQ3ZDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw0Q0FBd0IsR0FBdEM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsMEJBQTBCO2dCQUNyQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNqQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsbUNBQWUsR0FBN0I7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsaUJBQWlCO2dCQUM1QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsY0FBYztnQkFDcEIsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDO2FBQ3RCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxxQ0FBaUIsR0FBL0I7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsbUJBQW1CO2dCQUM5QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwyQkFBTyxHQUFyQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsaUJBQWlCO2dCQUN2QixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsQ0FBQzthQUNyRixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsK0JBQVcsR0FBekI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsYUFBYTtnQkFDeEIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLGNBQWMsRUFBRSxVQUFVLEVBQUUsbUJBQW1CLENBQUM7YUFDdEYsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLG9EQUFnQyxHQUE5QztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxrQ0FBa0M7Z0JBQzdDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxZQUFZO2dCQUNsQixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsYUFBYSxDQUFDO2FBQ3hELENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwyQ0FBdUIsR0FBckM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUseUJBQXlCO2dCQUNwQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsc0JBQXNCO2dCQUM1QixJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxpQkFBaUIsRUFBRSxxQkFBcUIsRUFBRSxtQkFBbUIsQ0FBQzthQUM1RyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsd0RBQW9DLEdBQWxEO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLHNDQUFzQztnQkFDakQsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxxQkFBcUIsRUFBRSxtQkFBbUIsQ0FBQzthQUNwRyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsOENBQTBCLEdBQXhDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDRCQUE0QjtnQkFDdkMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLHlCQUF5QjtnQkFDL0IsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLGNBQWMsRUFBRSxxQkFBcUIsRUFBRSxxQkFBcUIsQ0FBQzthQUNyRixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsa0NBQWMsR0FBNUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsZ0JBQWdCO2dCQUMzQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsYUFBYTtnQkFDbkIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLGVBQWUsQ0FBQzthQUNsRyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsZ0RBQTRCLEdBQTFDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDhCQUE4QjtnQkFDekMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGFBQWE7Z0JBQ25CLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsWUFBWSxDQUFDO2FBQzVILENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw4Q0FBMEIsR0FBeEM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsNEJBQTRCO2dCQUN2QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsYUFBYTtnQkFDbkIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLGVBQWUsRUFBRSxZQUFZLENBQUM7YUFDaEgsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLG1DQUFlLEdBQTdCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGlCQUFpQjtnQkFDNUIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsSUFBSSxFQUFFLEVBQUU7YUFDWCxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsNENBQXdCLEdBQXRDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDBCQUEwQjtnQkFDckMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsSUFBSSxFQUFFLEVBQUU7YUFDWCxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsMkNBQXVCLEdBQXJDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLHlCQUF5QjtnQkFDcEMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLElBQUksRUFBRSxDQUFDLHFCQUFxQixDQUFDO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxnREFBNEIsR0FBMUM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsOEJBQThCO2dCQUN6QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsV0FBVztnQkFDakIsSUFBSSxFQUFFLENBQUMsMEJBQTBCLENBQUM7YUFDckMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGtDQUFjLEdBQTVCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGdCQUFnQjtnQkFDM0IsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQ3ZCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxtQ0FBZSxHQUE3QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxpQkFBaUI7Z0JBQzVCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxDQUFDLGFBQWEsQ0FBQzthQUN4QixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEscUNBQWlCLEdBQS9CO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLG1CQUFtQjtnQkFDOUIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQzthQUNqQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsZ0NBQVksR0FBMUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsY0FBYztnQkFDekIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQzthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsNENBQXdCLEdBQXRDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDBCQUEwQjtnQkFDckMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLENBQUMsc0JBQXNCLENBQUM7YUFDakMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHdDQUFvQixHQUFsQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLE9BQU8sRUFBRSxRQUFRO2dCQUNqQixJQUFJLEVBQUUsUUFBUTtnQkFDZCxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUM7YUFDbEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDhCQUFVLEdBQXhCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLFlBQVk7Z0JBQ3ZCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxpQkFBaUI7Z0JBQ3ZCLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLG1CQUFtQixDQUFDO2FBQ3JGLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxrQ0FBYyxHQUE1QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxnQkFBZ0I7Z0JBQzNCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxpQkFBaUI7Z0JBQ3ZCLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixDQUFDO2FBQ3RGLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw4QkFBVSxHQUF4QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsUUFBUTtnQkFDZCxJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUM7YUFDMUQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHNDQUFrQixHQUFoQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxvQkFBb0I7Z0JBQy9CLE9BQU8sRUFBRSxRQUFRO2dCQUNqQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSx1Q0FBbUIsR0FBakM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUscUJBQXFCO2dCQUNoQyxPQUFPLEVBQUUsUUFBUTtnQkFDakIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDO2FBQ2pCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxrQ0FBYyxHQUE1QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxnQkFBZ0I7Z0JBQzNCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxFQUFFO2FBQ1gsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHlDQUFxQixHQUFuQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixJQUFJLEVBQUUsY0FBYztnQkFDcEIsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDO2FBQ3JCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxnREFBNEIsR0FBMUM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsOEJBQThCO2dCQUN6QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsb0JBQW9CO2dCQUMxQixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwwQ0FBc0IsR0FBcEM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsd0JBQXdCO2dCQUNuQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsb0JBQW9CO2dCQUMxQixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxpQ0FBYSxHQUEzQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxlQUFlO2dCQUMxQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsZUFBZTtnQkFDckIsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDO2FBQ2xCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw2QkFBUyxHQUF2QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxXQUFXO2dCQUN0QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUM7YUFDdkMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGtDQUFjLEdBQTVCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGdCQUFnQjtnQkFDM0IsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsSUFBSSxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQ3ZCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxtREFBK0IsR0FBN0M7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsaUNBQWlDO2dCQUM1QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUM7YUFDdkMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLCtCQUFXLEdBQXpCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGFBQWE7Z0JBQ3hCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxhQUFhO2dCQUNuQixJQUFJLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzthQUMzQixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEscUNBQWlCLEdBQS9CO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLG1CQUFtQjtnQkFDOUIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGFBQWE7Z0JBQ25CLElBQUksRUFBRSxDQUFDLGNBQWMsQ0FBQzthQUN6QixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsdUNBQW1CLEdBQWpDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLHFCQUFxQjtnQkFDaEMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGFBQWE7Z0JBQ25CLElBQUksRUFBRSxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUM7YUFDbEMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHVDQUFtQixHQUFqQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxxQkFBcUI7Z0JBQ2hDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxFQUFFO2FBQ1gsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLCtCQUFXLEdBQXpCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGFBQWE7Z0JBQ3hCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxtQkFBbUI7Z0JBQ3pCLElBQUksRUFBRSxFQUFFO2FBQ1gsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHFDQUFpQixHQUEvQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxtQkFBbUI7Z0JBQzlCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxtQkFBbUI7Z0JBQ3pCLElBQUksRUFBRSxFQUFFO2FBQ1gsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDZCQUFTLEdBQXZCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLFdBQVc7Z0JBQ3RCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxZQUFZO2dCQUNsQixJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLFlBQVksQ0FBQzthQUM5QyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsZ0RBQTRCLEdBQTFDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDhCQUE4QjtnQkFDekMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLDhCQUE4QjtnQkFDcEMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQzthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsOEJBQVUsR0FBeEI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsWUFBWTtnQkFDdkIsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLElBQUksRUFBRSxZQUFZO2dCQUNsQixJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQzthQUN2QyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsaUNBQWEsR0FBM0I7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsZUFBZTtnQkFDMUIsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLElBQUksRUFBRSxjQUFjO2dCQUNwQixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxpQ0FBYSxHQUEzQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxlQUFlO2dCQUMxQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsV0FBVztnQkFDakIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQzthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEscURBQWlDLEdBQS9DO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLG1DQUFtQztnQkFDOUMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLElBQUksRUFBRSxDQUFDLHFCQUFxQixFQUFFLE9BQU8sRUFBRSxZQUFZLENBQUM7YUFDdkQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLG1DQUFlLEdBQTdCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGlCQUFpQjtnQkFDNUIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFLGNBQWMsQ0FBQzthQUNqQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsb0NBQWdCLEdBQTlCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGtCQUFrQjtnQkFDN0IsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLElBQUksRUFBRSxRQUFRO2dCQUNkLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRSxlQUFlLENBQUM7YUFDbEMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDBDQUFzQixHQUFwQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSx3QkFBd0I7Z0JBQ25DLE9BQU8sRUFBRSxRQUFRO2dCQUNqQixJQUFJLEVBQUUsUUFBUTtnQkFDZCxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDO2FBQzNCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwyQ0FBdUIsR0FBckM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUseUJBQXlCO2dCQUNwQyxPQUFPLEVBQUUsUUFBUTtnQkFDakIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQzthQUMvQixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsNENBQXdCLEdBQXRDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDBCQUEwQjtnQkFDckMsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLElBQUksRUFBRSxlQUFlO2dCQUNyQixJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxtQ0FBZSxHQUE3QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxpQkFBaUI7Z0JBQzVCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxpQkFBaUI7Z0JBQ3ZCLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7YUFDekIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHdDQUFvQixHQUFsQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxVQUFVO2dCQUNoQixJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUM7YUFDbEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGtDQUFjLEdBQTVCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGdCQUFnQjtnQkFDM0IsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLHVCQUF1QjtnQkFDN0IsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQzthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsNkJBQVMsR0FBdkI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsV0FBVztnQkFDdEIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQzthQUN2QixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEseUJBQUssR0FBbkI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsT0FBTztnQkFDbEIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLElBQUksRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxDQUFDO2FBQzlDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxnQ0FBWSxHQUExQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxjQUFjO2dCQUN6QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUM7YUFDOUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDBCQUFNLEdBQXBCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxFQUFFO2FBQ1gsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDZCQUFTLEdBQXZCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLFdBQVc7Z0JBQ3RCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxZQUFZO2dCQUNsQixJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDdkIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGlDQUFhLEdBQTNCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGVBQWU7Z0JBQzFCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7YUFDaEMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLDRDQUF3QixHQUF0QztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSwwQkFBMEI7Z0JBQ3JDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxzQkFBc0I7Z0JBQzVCLElBQUksRUFBRSxDQUFDLHFCQUFxQixDQUFDO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSx1Q0FBbUIsR0FBakM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUscUJBQXFCO2dCQUNoQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDO2FBQ2pDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSx1Q0FBbUIsR0FBakM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUscUJBQXFCO2dCQUNoQyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSw4QkFBVSxHQUF4QjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsZUFBZTtnQkFDckIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQzthQUNqQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsb0RBQWdDLEdBQTlDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGtDQUFrQztnQkFDN0MsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGVBQWU7Z0JBQ3JCLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsUUFBUSxDQUFDO2FBQzNDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwwQkFBTSxHQUFwQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxRQUFRO2dCQUNuQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsaUJBQWlCO2dCQUN2QixJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUM7YUFDbEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGlDQUFhLEdBQTNCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGVBQWU7Z0JBQzFCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxXQUFXO2dCQUNqQixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDO2FBQzVCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSx3QkFBSSxHQUFsQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsV0FBVztnQkFDakIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUM7YUFDeEQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLGdDQUFZLEdBQTFCO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLGNBQWM7Z0JBQ3pCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxVQUFVO2dCQUNoQixJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUM7YUFDdEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHFDQUFpQixHQUEvQjtZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSxtQkFBbUI7Z0JBQzlCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUM7YUFDakMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVhLHlDQUFxQixHQUFuQztZQUNJLElBQUksS0FBSyxHQUFHLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlDLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQztnQkFDbkIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLFNBQVMsRUFBRSx1QkFBdUI7Z0JBQ2xDLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxjQUFjLEVBQUUsUUFBUSxDQUFDO2FBQy9ELENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSwrQ0FBMkIsR0FBekM7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsNkJBQTZCO2dCQUN4QyxPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsc0JBQXNCO2dCQUM1QixJQUFJLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxnQkFBZ0IsRUFBRSxrQkFBa0IsRUFBRSxtQkFBbUIsQ0FBQzthQUMzRixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsOERBQTBDLEdBQXhEO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLDRDQUE0QztnQkFDdkQsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsRUFBRSxrQkFBa0IsRUFBRSxtQkFBbUIsQ0FBQzthQUM1RixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEscUNBQWlCLEdBQS9CO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLG1CQUFtQjtnQkFDOUIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDO2FBQ2xCLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFYSxvQ0FBZ0IsR0FBOUI7WUFDSSxJQUFJLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxPQUFPLEtBQUssQ0FBQyxTQUFTLENBQUM7Z0JBQ25CLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixTQUFTLEVBQUUsa0JBQWtCO2dCQUM3QixPQUFPLEVBQUUsS0FBSztnQkFDZCxJQUFJLEVBQUUsV0FBVztnQkFDakIsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxtQkFBbUIsQ0FBQzthQUNyRSxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRWEsMENBQXNCLEdBQXBDO1lBQ0ksSUFBSSxLQUFLLEdBQUcsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUMsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDO2dCQUNuQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsU0FBUyxFQUFFLHdCQUF3QjtnQkFDbkMsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQzthQUN6QixDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUwsMEJBQUM7SUFBRCxDQUFDLEFBL2pDRCxJQStqQ0M7SUFDRCxPQUFTLG1CQUFtQixDQUFDIn0=