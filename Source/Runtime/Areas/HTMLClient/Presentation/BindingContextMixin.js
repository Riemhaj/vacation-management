/// <amd-module name="BindingContextMixin" />
define("BindingContextMixin", ["require", "exports"], function (require, exports) {
    "use strict";
    /*
    * Dies ist eine Mixin für Control, View, Factory etc.
    * Wann immer eine Klasse dazu in der Lage ist Knockout - Bindings ausführen, sollte sie einen BindingContext(-Mixin)
    * als Property haben oder hier von erben.Bei jedem ko.applyBindings(...) welches potentiell Controls, Factories etc.initialisiert, muss dieser mit übergeben werden.
    * Alle so erzeugten Controls, Factories werden dann in diesem BindingContext hinterlegt, um beim Disposing des einbettenden
    * Objekts mit - disposed zu werden.
    */
    var BindingContextMixin = /** @class */ (function () {
        function BindingContextMixin() {
            this._registeredDisposables = [];
        }
        BindingContextMixin.prototype.registerDisposable = function (disposable) {
            if (disposable == null) {
                return;
            }
            this._registeredDisposables.push(disposable);
        };
        return BindingContextMixin;
    }());
    return BindingContextMixin;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQmluZGluZ0NvbnRleHRNaXhpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkJpbmRpbmdDb250ZXh0TWl4aW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNkNBQTZDOzs7SUFFN0M7Ozs7OztNQU1FO0lBQ0Y7UUFBQTtZQUVXLDJCQUFzQixHQUFlLEVBQUUsQ0FBQztRQVNuRCxDQUFDO1FBUFUsZ0RBQWtCLEdBQXpCLFVBQTBCLFVBQVU7WUFDaEMsSUFBSSxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUNwQixPQUFPO2FBQ1Y7WUFFRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFDTCwwQkFBQztJQUFELENBQUMsQUFYRCxJQVdDO0lBRUQsT0FBUyxtQkFBbUIsQ0FBQyJ9