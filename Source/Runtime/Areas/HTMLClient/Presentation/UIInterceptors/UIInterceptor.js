/// <amd-module name="UIInterceptors/UIInterceptor" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("UIInterceptors/UIInterceptor", ["require", "exports", "Class", "Management/ComponentManager"], function (require, exports, Class, ComponentManager) {
    "use strict";
    var UIInterceptor = /** @class */ (function (_super) {
        __extends(UIInterceptor, _super);
        function UIInterceptor() {
            var _this = _super.call(this) || this;
            _this.isDisposable = _this.paramOrDefault(true);
            _this.applyPrototypeToInstance(UIInterceptor);
            return _this;
        }
        UIInterceptor.prototype.afterStartup = function (callback) {
            callback();
        };
        UIInterceptor.prototype.afterLogin = function (callback) {
            callback();
        };
        UIInterceptor.prototype.beforeLoadRootLayout = function () {
        };
        UIInterceptor.prototype.afterLoadRootLayout = function () {
        };
        UIInterceptor.prototype.neededDefaultResourceItems = function () {
            return null;
        };
        UIInterceptor.prototype.beforeLoadMenuTree = function (rootNode) {
        };
        UIInterceptor.prototype.beforeLoadNavigationTree = function (rootNode) {
        };
        /**
         * Erzeugt eine neue Instanz eines UIInterceptors anhand einer Komponentenbezeichnung
         */
        UIInterceptor.createInstance = function (component) {
            var Component = ComponentManager.getUIInterceptor(component);
            if (Component == null) {
                throw 'UIInterceptor-Komponente "' + component + '" wurde nicht gefunden.';
            }
            return new Component();
        };
        return UIInterceptor;
    }(Class));
    return UIInterceptor;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlJbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlVJSW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsc0RBQXNEOzs7Ozs7Ozs7Ozs7O0lBS3REO1FBQTRCLGlDQUFLO1FBRTdCO1lBQUEsWUFDSSxpQkFBTyxTQUtWO1lBSEcsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTlDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7UUFDakQsQ0FBQztRQUVNLG9DQUFZLEdBQW5CLFVBQW9CLFFBQW9CO1lBQ3BDLFFBQVEsRUFBRSxDQUFDO1FBQ2YsQ0FBQztRQUVNLGtDQUFVLEdBQWpCLFVBQWtCLFFBQW9CO1lBQ2xDLFFBQVEsRUFBRSxDQUFDO1FBQ2YsQ0FBQztRQUVNLDRDQUFvQixHQUEzQjtRQUNBLENBQUM7UUFFTSwyQ0FBbUIsR0FBMUI7UUFDQSxDQUFDO1FBRU0sa0RBQTBCLEdBQWpDO1lBQ0ksT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVNLDBDQUFrQixHQUF6QixVQUEwQixRQUFhO1FBQ3ZDLENBQUM7UUFFTSxnREFBd0IsR0FBL0IsVUFBZ0MsUUFBYTtRQUM3QyxDQUFDO1FBRUQ7O1dBRUc7UUFDVyw0QkFBYyxHQUE1QixVQUE2QixTQUFpQjtZQUMxQyxJQUFJLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQXlCLENBQUM7WUFFckYsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUNuQixNQUFNLDRCQUE0QixHQUFHLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQzthQUM5RTtZQUVELE9BQU8sSUFBSSxTQUFTLEVBQUUsQ0FBQztRQUMzQixDQUFDO1FBRUwsb0JBQUM7SUFBRCxDQUFDLEFBL0NELENBQTRCLEtBQUssR0ErQ2hDO0lBRUQsT0FBUyxhQUFhLENBQUMifQ==