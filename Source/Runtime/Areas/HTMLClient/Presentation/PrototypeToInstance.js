/// <amd-module name="PrototypeToInstance" />
define("PrototypeToInstance", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function applyPrototypeToInstance(constructorFunction) {
        var _this = this;
        if (!(Object.getPrototypeOf(this) instanceof constructorFunction)) {
            Object.getOwnPropertyNames(constructorFunction.prototype).forEach(function (name) {
                if (name != "constructor" && name != "__proto__")
                    _this[name] = constructorFunction.prototype[name].bind(_this);
            });
        }
    }
    exports.applyPrototypeToInstance = applyPrototypeToInstance;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJvdG90eXBlVG9JbnN0YW5jZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlByb3RvdHlwZVRvSW5zdGFuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNkNBQTZDOzs7O0lBRTdDLGtDQUF5QyxtQkFBd0I7UUFBakUsaUJBT0M7UUFORyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLG1CQUFtQixDQUFDLEVBQUU7WUFDL0QsTUFBTSxDQUFDLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ2xFLElBQUksSUFBSSxJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksV0FBVztvQkFDNUMsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUM7WUFDcEUsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFQRCw0REFPQztJQUFBLENBQUMifQ==