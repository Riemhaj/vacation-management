var CF =
/******/ (function (modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if (installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
                /******/
};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
            /******/
}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function (value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function (exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
                /******/
});
            /******/
};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function (module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
            /******/
};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function (object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 13);
        /******/
})
/************************************************************************/
/******/([
/* 0 */
/***/ function (module, exports) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var Constants = (function () {
                    function Constants() {
                    }
                    Constants.EVENT_NAMESPACE = 'CFGRID';
                    Constants.EVENT_CLICK = 'click.' + Constants.EVENT_NAMESPACE;
                    Constants.EVENT_MOUSEOVER = 'mouseover.' + Constants.EVENT_NAMESPACE;
                    Constants.EVENT_MOUSEDOWN = 'mousedown.' + Constants.EVENT_NAMESPACE;
                    Constants.EVENT_KEYDOWN = 'keydown.' + Constants.EVENT_NAMESPACE;
                    Constants.EVENT_SCROLL = 'scroll.' + Constants.EVENT_NAMESPACE;
                    Constants.EVENT_DBLCLICK = 'dblclick.' + Constants.EVENT_NAMESPACE;
                    Constants.CLASS_PREFIX = 'cf-grid';
                    Constants.FOOTER_CLASS_PREFIX = Constants.CLASS_PREFIX + '-footer';
                    Constants.FOOTER_CLASS_PAGING = Constants.FOOTER_CLASS_PREFIX + '-paging';
                    Constants.FOOTER_CLASS_PAGESIZE = Constants.FOOTER_CLASS_PAGING + '-pagesize';
                    Constants.FOOTER_CLASS_PAGESIZE_CURRENT = Constants.FOOTER_CLASS_PAGESIZE + '-current';
                    Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER = Constants.FOOTER_CLASS_PAGESIZE + '-options';
                    Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_VISIBLE = Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER + '-visible';
                    Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_HIDDEN = Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER + '-hidden';
                    Constants.FOOTER_CLASS_PAGESIZE_OPTION = Constants.FOOTER_CLASS_PAGESIZE + '-option';
                    Constants.FOOTER_CLASS_PAGESIZE_OPTION_ACTIVE = Constants.FOOTER_CLASS_PAGESIZE_OPTION + '-active';
                    Constants.FOOTER_CLASS_PAGES = Constants.FOOTER_CLASS_PAGING + '-pages';
                    Constants.FOOTER_CLASS_PAGES_DISABLED = Constants.FOOTER_CLASS_PAGES + '-disabled';
                    Constants.FOOTER_CLASS_PAGE = Constants.FOOTER_CLASS_PAGES + '-page';
                    Constants.FOOTER_CLASS_PAGE_ACTIVE = Constants.FOOTER_CLASS_PAGE + '-active';
                    Constants.FOOTER_CLASS_PAGE_DISABLED = Constants.FOOTER_CLASS_PAGE + '-disabled';
                    Constants.FOOTER_CLASS_PAGE_START = Constants.FOOTER_CLASS_PAGE + '-start';
                    Constants.FOOTER_CLASS_PAGE_END = Constants.FOOTER_CLASS_PAGE + '-end';
                    Constants.FOOTER_CLASS_PAGE_PREVIOUS = Constants.FOOTER_CLASS_PAGE + '-previous';
                    Constants.FOOTER_CLASS_PAGE_NEXT = Constants.FOOTER_CLASS_PAGE + '-next';
                    Constants.CLASS_ROW = Constants.CLASS_PREFIX + '-row';
                    Constants.CLASS_ROW_SELECTED = Constants.CLASS_ROW + '-selected';
                    Constants.CLASS_DUMMY_ROW = Constants.CLASS_PREFIX + '-dummy-row';
                    Constants.CLASS_CELL_FILLCOLUMN = Constants.CLASS_PREFIX + '-fillcolumn';
                    Constants.CLASS_ISEDITING = 'is-editing';
                    Constants.CLASS_EDITMODE_SINGLE = 'editmode-single';
                    Constants.CLASS_EDITMODE_MULTIPLE = 'editmode-multiple';
                    Constants.CLASS_CANADD = 'can-add';
                    Constants.CLASS_ROW_IN_EDIT = 'row-in-editing';
                    Constants.CLASS_GRID_DISABLED = 'disabled';
                    Constants.CLASS_ROW_BUTTON = Constants.CLASS_ROW + '-button';
                    Constants.CLASS_ROW_EDITBUTTON = Constants.CLASS_ROW + '-editButton';
                    Constants.CLASS_ROW_DELETEBUTTON = Constants.CLASS_ROW + '-deleteButton';
                    Constants.CLASS_ROW_SAVEBUTTON = Constants.CLASS_ROW + '-saveButton';
                    Constants.CLASS_ADDBUTTON = Constants.CLASS_PREFIX + '-addButton';
                    Constants.CLASS_RESIZER = Constants.CLASS_PREFIX + '-resizer';
                    Constants.FOOTER_CLASS_SUMMARYROW = Constants.FOOTER_CLASS_PREFIX + '-summaryrow';
                    return Constants;
                }());
                exports.Constants = Constants;


                /***/
},
/* 1 */
/***/ function (module, exports) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var HTMLTable = (function () {
                    function HTMLTable(wrapperCSS, options, grid) {
                        this.$wrapper = undefined;
                        this.$table = undefined;
                        this.$colgroup = undefined;
                        this._Options = {};
                        this._Options = options;
                        this.grid = grid;
                        this.$wrapper = $('<div class="' + wrapperCSS + '">');
                        this.$table = $('<table role="grid">');
                        this.$colgroup = $('<colgroup>');
                        this.$colgroup.appendTo(this.$table);
                        this.$table.appendTo(this.$wrapper);
                        this.$table[0].style.borderSpacing = "0";
                        this.$table[0].style.tableLayout = "fixed";
                        this.$table[0].style.width = "100%";
                    }
                    HTMLTable.prototype.getWrapper = function () {
                        return this.$wrapper;
                    };
                    return HTMLTable;
                }());
                exports.HTMLTable = HTMLTable;


                /***/
},
/* 2 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var Tables_1 = __webpack_require__(10);
                var HTMLFooter_1 = __webpack_require__(7);
                var ColumnResizer_1 = __webpack_require__(4);
                var Constants_1 = __webpack_require__(0);
                var ColumnWidthHelper_1 = __webpack_require__(5);
                var EventHelper_1 = __webpack_require__(6);
                var SortState;
                (function (SortState) {
                    SortState["ASC"] = "ASC";
                    SortState["DESC"] = "DESC";
                })(SortState = exports.SortState || (exports.SortState = {}));
                var CoreFrameGrid = (function () {
                    function CoreFrameGrid(params) {
                        this._wrapper = undefined;
                        this.Header = undefined;
                        this.Body = undefined;
                        this.Footer = undefined;
                        this.columnWidthsCalculated = false;
                        this.dataRows = [];
                        this.headerCols = undefined;
                        this.selectedIds = undefined;
                        this.options = {};
                        this.ColumnResizer = undefined;
                        this.EventHelper = undefined;
                        this.useFillColumn = false;
                        this._$selectedRows = [];
                        this.setDefaultOptions();
                        this._wrapper = params.element;
                        this._wrapper.addClass(Constants_1.Constants.CLASS_PREFIX);
                        this._wrapper.attr('tabindex', 0);
                        this.headerCols = params.columns;
                        this.dataRows = params.data;
                        this.options.height = params.height;
                        if (params.colMinWidth != undefined)
                            this.options.colMinWidth = params.colMinWidth;
                        if (params.showHeader != undefined)
                            this.options.showHeader = params.showHeader;
                        if (params.doubleClickToEdit != undefined)
                            this.options.doubleClickToEdit = params.doubleClickToEdit;
                        if (params.applyOnLostFocus != undefined)
                            this.options.applyOnLostFocus = params.applyOnLostFocus;
                        if (params.isEditable != undefined)
                            this.options.isEditable = params.isEditable;
                        if (params.isEnabled != undefined)
                            this.options.isEnabled = params.isEnabled;
                        if (params.editMode != undefined)
                            this.options.editMode = params.editMode;
                        if (params.canAdd != undefined)
                            this.options.canAdd = params.canAdd;
                        if (params.rowIdName != undefined)
                            this.options.rowIdName = params.rowIdName;
                        if (params.paging != undefined)
                            this.options.paging = params.paging;
                        if (params.summaryRow != undefined)
                            this.options.summaryRow = params.summaryRow;
                        if (params.selectionMode != undefined)
                            this.options.selectionMode = params.selectionMode;
                        if (params.locator != undefined)
                            this.options.locator = params.locator;
                        this.onColumnResizedHandler = params.onColumnResized;
                        if (params.selected != undefined)
                            this.selectedIds = params.selected;
                        if (params.onBeforeRender != undefined)
                            this.onBeforeRender = params.onBeforeRender;
                        if (params.onAfterRender != undefined)
                            this.onAfterRender = params.onAfterRender;
                        this.handler = {
                            onSelectionChangeHandler: params.onSelectionChange,
                            onDblClickHandler: params.onDblClick,
                            onMouseOverHandler: params.onMouseOver,
                            onMouseDownHandler: params.onMouseDown,
                            onSortChangeHandler: params.onSortChange,
                            onSaveHandler: params.onSave,
                            onDeleteHandler: params.onDelete,
                            onBeforeEditHandler: params.onBeforeEdit,
                            onAddHandler: params.onAdd,
                            onKeyDownHandler: params.onKeyDown
                        };
                        if (params.orderBy)
                            this.orderBy = params.orderBy;
                        this.resetWrapperClasses();
                    }
                    CoreFrameGrid.prototype.setDefaultOptions = function () {
                        this.options.headerSortName = 'position';
                        this.options.rowIdName = 'id';
                        this.options.height = undefined;
                        this.options.sortDataName = 'sortstate';
                        this.options.isEditable = false;
                        this.options.isEnabled = true;
                        this.options.doubleClickToEdit = false;
                        this.options.applyOnLostFocus = false;
                        this.options.colMinWidth = 50;
                        this.options.paging = {
                            isEnabled: false
                        };
                        this.options.summaryRow = {
                            isEnabled: false
                        };
                        this.options.locator = '';
                        this.options.showHeader = true;
                        this.onBeforeRender = function () { };
                        this.onAfterRender = function () { };
                    };
                    CoreFrameGrid.prototype.init = function () {
                        this.Header = new Tables_1.HeaderTable(this.options, this);
                        this.Body = new Tables_1.BodyTable(this.options, this);
                        this.Footer = new HTMLFooter_1.Footer(this.options, this);
                        this._wrapper.append(this.Header.getWrapper());
                        this._wrapper.append(this.Body.getWrapper());
                        this._wrapper.append(this.Footer.getWrapper());
                        if (this.useLocator())
                            this._wrapper.attr('data-locator', this.options.locator);
                        this._wrapper.attr('data-role', 'grid');
                        this.setColumns(this.headerCols);
                        this.updatePaging(this.options.paging);
                        if (this.options.height)
                            this._wrapper[0].style.height = this.options.height;
                        if (this.Header.isHeaderVisible()) {
                            this._wrapper.addClass('hasheader');
                        }
                        else
                            this._wrapper.addClass('noheader');
                        if (this.Footer.isFooterVisible()) {
                            this._wrapper.addClass('hasfooter');
                        }
                        else
                            this._wrapper.addClass('nofooter');
                        this.ColumnResizer = new ColumnResizer_1.ColumnResizer(this);
                        this.EventHelper = new EventHelper_1.EventHelper(this, this.handler);
                        this.EventHelper.registerAll();
                        this.getHeader().SetSortExpression(this.orderBy);
                    };
                    CoreFrameGrid.prototype.useLocator = function () {
                        return this.options.locator && this.options.locator != '';
                    };
                    CoreFrameGrid.prototype.addEditColumn = function () {
                        if (this.options.isEditable) {
                            var editColumn = {
                                isEditColumn: true,
                                width: '70px',
                                hidden: false
                            };
                            this.headerCols.splice(0, 0, editColumn);
                        }
                    };
                    CoreFrameGrid.prototype.getIndexByRowId = function (id) {
                        var _this = this;
                        var indexOfRow = 0;
                        this.dataRows.forEach(function (element, index) {
                            if (element[_this.options.rowIdName] == id)
                                indexOfRow = index;
                        });
                        return indexOfRow;
                    };
                    CoreFrameGrid.prototype.getRowIdByIndex = function (index) {
                        var row = this.dataRows[index];
                        if (!row)
                            return null;
                        return row[this.options.rowIdName];
                    };
                    CoreFrameGrid.prototype.ChangePercentWidthToPixel = function () {
                        var cols = this.Header.$theadRow[0].children;
                        var maxWidth = this.Header.$table[0].offsetWidth;
                        for (var i = 0; i < this.headerCols.length; i++) {
                            if (typeof this.headerCols[i].width == "string" && this.headerCols[i].width.indexOf('%') >= 0) {
                                var col = cols[i];
                                var w = col.offsetWidth;
                                if (w == 0) {
                                    var anteil = parseFloat(this.headerCols[i].width);
                                    w = maxWidth * (anteil / 100);
                                }
                                this.headerCols[i].width = w;
                            }
                        }
                    };
                    CoreFrameGrid.prototype.getSelectedIds = function () {
                        return this.selectedIds;
                    };
                    CoreFrameGrid.prototype.getSelectedData = function () {
                        var _this = this;
                        if (this._$selectedRows.length == 0) {
                            return [];
                        }
                        var result = [];
                        this._$selectedRows.forEach(function (i) {
                            result.push(i.data('dataRaw'));
                        });
                        result = result.filter(function (r) {
                            return r !== undefined;
                        });
                        if (this.options.rowIdName) {
                            result = result.sort(function (a, b) {
                                var indexA = _this.getIndexByRowId(a[_this.options.rowIdName]);
                                var indexB = _this.getIndexByRowId(b[_this.options.rowIdName]);
                                if (indexA < indexB)
                                    return -1;
                                else if (indexA > indexB)
                                    return 1;
                                else
                                    return 0;
                            });
                        }
                        return result;
                    };
                    CoreFrameGrid.prototype.getSelectedRows = function () {
                        return this._$selectedRows;
                    };
                    CoreFrameGrid.prototype.EnterEditModeById = function (rowId, scrollToELement) {
                        if (scrollToELement === void 0) { scrollToELement = false; }
                        var $tr;
                        if ($tr = this.GetTrByIds([rowId])) {
                            var $newRow = this.Body.EnterEditModeFor($tr, scrollToELement);
                            this.SelectRowElement($newRow, false, true);
                        }
                    };
                    CoreFrameGrid.prototype.SelectRowElement = function (el, multiSelect, reset) {
                        if (reset)
                            this.deselectAll();
                        el.addClass(Constants_1.Constants.CLASS_ROW_SELECTED);
                        var id = el.data('rowId');
                        if (multiSelect && this.options.selectionMode == 'multiple') {
                            this._$selectedRows.push(el);
                            this.selectedIds.push(id);
                        }
                        else {
                            this.selectedIds = [id];
                            this._$selectedRows = [el];
                        }
                    };
                    CoreFrameGrid.prototype.GetTrByIds = function (rowIds) {
                        rowIds = rowIds.filter(function (e) { return e != undefined; });
                        var $trs = this.Body.$tbody.find('tr').filter(function (index, element) {
                            var currentRowId = $(element).data('rowId');
                            var matching = rowIds.filter(function (r) {
                                if (r + '' == currentRowId + '')
                                    return true;
                            });
                            if (matching.length == 1)
                                return true;
                        });
                        return $trs;
                    };
                    CoreFrameGrid.prototype.SelectRowByIds = function (rowIds, multiSelect, reset) {
                        var _this = this;
                        if (multiSelect === void 0) { multiSelect = false; }
                        if (reset === void 0) { reset = false; }
                        var $newTrs = this.GetTrByIds(rowIds);
                        if (reset)
                            this.deselectAll();
                        $newTrs.each(function (i, el) {
                            _this.SelectRowElement($(el), multiSelect, false);
                        });
                        this.optimizeScrollPosition();
                    };
                    CoreFrameGrid.prototype.SelectRowByIndex = function (index) {
                        var id = this.getRowIdByIndex(index);
                        this.SelectRowByIds([id], false, true);
                    };
                    CoreFrameGrid.prototype.RemoveRowById = function (rowId) {
                        var _this = this;
                        var $tr = this.GetTrByIds([rowId]);
                        if ($tr) {
                            this.dataRows = _.filter(this.dataRows, function (row) {
                                return row[_this.options.rowIdName] != rowId;
                            });
                            if ($tr.hasClass(Constants_1.Constants.CLASS_ROW_SELECTED))
                                this._$selectedRows = [];
                            $tr.remove();
                        }
                    };
                    CoreFrameGrid.prototype.setColumns = function (newColumns) {
                        this.headerCols = newColumns;
                        this.addEditColumn();
                        this.Header.FillHeader();
                        this.RefillBody();
                        ColumnWidthHelper_1.ColumnWidthHelper.generateColumnGroups(this);
                        if (this.ColumnResizer)
                            this.ColumnResizer.RearrangeResizer();
                    };
                    CoreFrameGrid.prototype.RefillBody = function () {
                        this.onBeforeRender();
                        var selectedIds = this.getSelectedIds();
                        this.Body.FillBody(this.dataRows);
                        if (selectedIds)
                            this.SelectRowByIds(selectedIds);
                        if (this.dataRows.length == 0) {
                            var columnLength = this.getSortedCols().length;
                            var dummyRow = '<tr class="' + Constants_1.Constants.CLASS_ROW + ' ' + Constants_1.Constants.CLASS_DUMMY_ROW + '">';
                            for (var x = 0; x < columnLength; x++) {
                                dummyRow += '<td>&nbsp;</td>';
                            }
                            if (this.useFillColumn)
                                dummyRow += '<td class="' + Constants_1.Constants.CLASS_CELL_FILLCOLUMN + '">&nbsp;</td>';
                            dummyRow += '</tr>';
                            this.Body.$tbody.append(dummyRow);
                        }
                        else {
                            if (!this.columnWidthsCalculated) {
                                ColumnWidthHelper_1.ColumnWidthHelper.generateColumnGroups(this);
                                this.columnWidthsCalculated = true;
                            }
                        }
                        this.onAfterRender();
                    };
                    CoreFrameGrid.prototype.SetData = function (newData) {
                        this.dataRows = newData;
                        this.RefillBody();
                        this.ColumnResizer.RearrangeResizer();
                        this.Footer.updatePaging(this.options.paging);
                        this.Footer.updateSummaryRow();
                    };
                    CoreFrameGrid.prototype.SetSelectedIds = function (selectedIds) {
                        this.SelectRowByIds(selectedIds, true, true);
                    };
                    CoreFrameGrid.prototype.selectAll = function () {
                        this.SelectRowByIds(this.getRowIds(), true, true);
                    };
                    CoreFrameGrid.prototype.deselectRowElement = function (element) {
                        element.removeClass(Constants_1.Constants.CLASS_ROW_SELECTED);
                        this._$selectedRows = this._$selectedRows.filter(function (sr) {
                            return sr.hasClass(Constants_1.Constants.CLASS_ROW_SELECTED);
                        });
                        var rowId = element.data('rowId');
                        var idIndex = this.selectedIds.indexOf(rowId);
                        this.selectedIds.splice(idIndex, 1);
                    };
                    CoreFrameGrid.prototype.deselectAll = function () {
                        this.Body.$tbody.find('.' + Constants_1.Constants.CLASS_ROW).removeClass(Constants_1.Constants.CLASS_ROW_SELECTED);
                        this._$selectedRows = [];
                        this.selectedIds = [];
                    };
                    CoreFrameGrid.prototype.selectPrevious = function () {
                        if (this.dataRows.length == 0)
                            return null;
                        var newIndex = 0;
                        var indexofFirstSelected = this.getIndexOfFirstSelected();
                        if (indexofFirstSelected == 0)
                            return { id: this.dataRows[indexofFirstSelected][this.options.rowIdName], index: indexofFirstSelected };
                        else if (indexofFirstSelected != null)
                            newIndex = indexofFirstSelected - 1;
                        this.SelectRowByIndex(newIndex);
                        return { id: this.dataRows[newIndex][this.options.rowIdName], index: newIndex };
                    };
                    CoreFrameGrid.prototype.selectNext = function () {
                        if (this.dataRows.length == 0)
                            return null;
                        var newIndex = 0;
                        var indexofFirstSelected = this.getIndexOfFirstSelected();
                        if (indexofFirstSelected == this.dataRows.length - 1)
                            return { id: this.dataRows[indexofFirstSelected][this.options.rowIdName], index: indexofFirstSelected };
                        else if (indexofFirstSelected != null)
                            newIndex = indexofFirstSelected + 1;
                        this.SelectRowByIndex(newIndex);
                        return { id: this.dataRows[newIndex][this.options.rowIdName], index: newIndex };
                    };
                    CoreFrameGrid.prototype.getIndexOfFirstSelected = function () {
                        if (this._$selectedRows.length == 0)
                            return null;
                        var idOfFirstSelected = this._$selectedRows[0].data('rowId');
                        var indexofFirstSelected = this.getIndexByRowId(idOfFirstSelected);
                        return indexofFirstSelected;
                    };
                    CoreFrameGrid.prototype.optimizeScrollPosition = function () {
                        if (this._$selectedRows.length == 1) {
                            var viewTop = this.getBody().getWrapper().scrollTop();
                            var viewHeight = this.getBody().getWrapper().height();
                            ;
                            var viewBottom = viewTop + viewHeight;
                            var rowTop = this._$selectedRows[0][0].offsetTop;
                            var rowHeight = this._$selectedRows[0].height();
                            var rowBottom = rowTop + rowHeight;
                            if (rowTop < viewTop)
                                this.getBody().getWrapper().scrollTop(rowTop);
                            else if (rowBottom > viewBottom)
                                this.getBody().getWrapper().scrollTop(rowBottom - viewHeight);
                        }
                    };
                    CoreFrameGrid.prototype.setCanAdd = function (canAdd) {
                        this.options.canAdd = canAdd;
                        var $oldAddButton = this._wrapper.find('.cf-grid-addButton');
                        if (canAdd && $oldAddButton.length == 0) {
                            var $addButton = $('<div class="cf-grid-addButton">');
                            this._wrapper.append($addButton);
                        }
                        else if (!canAdd) {
                            $oldAddButton.remove();
                        }
                        this.resetWrapperClasses();
                    };
                    CoreFrameGrid.prototype.setHeight = function (height) {
                        this.options.height = height;
                        this._wrapper[0].style.height = this.options.height;
                    };
                    CoreFrameGrid.prototype.setIsEnabled = function (isEnabled) {
                        this.options.isEnabled = isEnabled;
                        this.resetWrapperClasses();
                    };
                    CoreFrameGrid.prototype.setSortExpression = function (orderBy) {
                        this.getHeader().SetSortExpression(orderBy);
                    };
                    CoreFrameGrid.prototype.setIsEditable = function (isEditable) {
                        this.options.isEditable = isEditable;
                        if (this.headerCols[0] && this.headerCols[0].isEditColumn) {
                            this.headerCols.splice(0, 1);
                            this.setColumns(this.headerCols);
                        }
                        else
                            this.setColumns(this.headerCols);
                    };
                    CoreFrameGrid.prototype.getSortedCols = function () {
                        return _.sortBy(this.headerCols, this.options.headerSortName).filter(function (col) {
                            if (col.hidden === true)
                                return false;
                            else
                                return true;
                        });
                    };
                    CoreFrameGrid.prototype.getVisibleColumns = function () {
                        return this.headerCols.filter(function (col) {
                            if (col.hidden === true)
                                return false;
                            else
                                return true;
                        });
                    };
                    CoreFrameGrid.prototype.resetWrapperClasses = function () {
                        if (this.options.isEnabled)
                            this._wrapper.removeClass(Constants_1.Constants.CLASS_GRID_DISABLED);
                        else
                            this._wrapper.addClass(Constants_1.Constants.CLASS_GRID_DISABLED);
                        if (this.options.editMode == 'single')
                            this._wrapper.addClass(Constants_1.Constants.CLASS_EDITMODE_SINGLE);
                        else
                            this._wrapper.removeClass(Constants_1.Constants.CLASS_EDITMODE_SINGLE);
                        if (this.options.editMode == 'multiple')
                            this._wrapper.addClass(Constants_1.Constants.CLASS_EDITMODE_MULTIPLE);
                        else
                            this._wrapper.removeClass(Constants_1.Constants.CLASS_EDITMODE_MULTIPLE);
                        if (this.Body) {
                            if (this.Body.$tbody.find('tr.' + Constants_1.Constants.CLASS_ROW_IN_EDIT).length > 0)
                                this._wrapper.addClass(Constants_1.Constants.CLASS_ISEDITING);
                            else
                                this._wrapper.removeClass(Constants_1.Constants.CLASS_ISEDITING);
                        }
                        if (this.options.canAdd)
                            this._wrapper.addClass(Constants_1.Constants.CLASS_CANADD);
                        else
                            this._wrapper.removeClass(Constants_1.Constants.CLASS_CANADD);
                    };
                    CoreFrameGrid.prototype.isAddingAllowed = function () {
                        if (!this.options.isEnabled)
                            return false;
                        if (this.options.editMode == 'multiple' && this.options.canAdd)
                            return true;
                        if (this.options.editMode == 'single' && this.options.canAdd && !this._wrapper.hasClass(Constants_1.Constants.CLASS_ISEDITING))
                            return true;
                        else
                            return false;
                    };
                    CoreFrameGrid.prototype.isEditingAllowed = function () {
                        if (!this.options.isEnabled || !this.options.isEditable)
                            return false;
                        if (this.options.editMode == 'multiple')
                            return true;
                        if (this.options.editMode == 'single' && !this._wrapper.hasClass(Constants_1.Constants.CLASS_ISEDITING))
                            return true;
                        else
                            return false;
                    };
                    CoreFrameGrid.prototype.isDeletingAllowed = function () {
                        if (!this.options.isEnabled)
                            return false;
                    };
                    CoreFrameGrid.prototype.getTotalRows = function () {
                        if (this._totalRows)
                            return this._totalRows;
                        else
                            return this.dataRows.length;
                    };
                    CoreFrameGrid.prototype.getRows = function () {
                        return this.dataRows;
                    };
                    CoreFrameGrid.prototype.getRowIds = function () {
                        var _this = this;
                        var ids = [];
                        this.getRows().forEach(function (row) {
                            ids.push(row[_this.options.rowIdName]);
                        });
                        return ids;
                    };
                    CoreFrameGrid.prototype.updatePaging = function (paging) {
                        this.options.paging = paging;
                        this.Footer.updatePaging(paging);
                    };
                    CoreFrameGrid.prototype.getWrapper = function () {
                        return this._wrapper;
                    };
                    CoreFrameGrid.prototype.getHeader = function () {
                        return this.Header;
                    };
                    CoreFrameGrid.prototype.getBody = function () {
                        return this.Body;
                    };
                    CoreFrameGrid.prototype.getOptions = function () {
                        return this.options;
                    };
                    CoreFrameGrid.prototype.getEventHelper = function () {
                        return this.EventHelper;
                    };
                    CoreFrameGrid.prototype.onColumnResized = function (columnName, newWidth) {
                        var col = this.headerCols.filter(function (col) {
                            return col.name === columnName;
                        });
                        if (col.length > 0)
                            col[0].widthAutoCalculated = false;
                        if (typeof this.onColumnResizedHandler == "function")
                            this.onColumnResizedHandler(columnName, newWidth);
                    };
                    CoreFrameGrid.prototype.dispose = function () {
                        $('body').off(Constants_1.Constants.EVENT_CLICK);
                    };
                    return CoreFrameGrid;
                }());
                exports.CoreFrameGrid = CoreFrameGrid;


                /***/
},
/* 3 */
/***/ function (module, exports) {

                if (typeof require('Management/ComponentManager') === 'undefined') { var e = new Error("Cannot find module \"require('Management/ComponentManager')\""); e.code = 'MODULE_NOT_FOUND'; throw e; }
                module.exports = require('Management/ComponentManager');

                /***/
},
/* 4 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var Constants_1 = __webpack_require__(0);
                var ColumnResizer = (function () {
                    function ColumnResizer(grid) {
                        this.grid = grid;
                        this.isPressed = false;
                        this.Start = undefined;
                        this.StartX = undefined;
                        this.StartWidth = undefined;
                        this.MinWidth = undefined;
                        this.ToResize = {
                            BodyEL: undefined,
                            ResizerEL: undefined,
                            ColumnName: undefined
                        };
                        this.NS = Constants_1.Constants.EVENT_NAMESPACE;
                        this.UsingPercentWidth = false;
                        this.RegisterEvents();
                        this.RearrangeResizer();
                        this.MinWidth = grid.getOptions().colMinWidth;
                    }
                    ColumnResizer.prototype.RegisterEvents = function () {
                        var _this = this;
                        this.grid.getHeader().getWrapper().on('mousedown.' + this.NS, 'div.' + Constants_1.Constants.CLASS_RESIZER, function (e) { _this.OnMouseDown(e); });
                    };
                    ColumnResizer.prototype.CheckIsUsingPercent = function (i) {
                        var th = this.grid.getHeader().$theadRow[0].children[i];
                        var col = this.grid.getHeader().$colgroup[0].children[i];
                        var w = col.style.width;
                        if (w.indexOf('%') >= 0) {
                            this.UsingPercentWidth = true;
                            col.style.width = th.offsetWidth + "px";
                        }
                    };
                    ColumnResizer.prototype.RestoreWhenUsingPercent = function () {
                        if (this.UsingPercentWidth) {
                        }
                        this.UsingPercentWidth = false;
                    };
                    ColumnResizer.prototype.OnMouseDown = function (ev) {
                        var _this = this;
                        ev.preventDefault();
                        ev.stopPropagation();
                        var $target = $(ev.currentTarget);
                        var i = $target.data('id');
                        this.ToResize.ColumnName = $target.data('columnname');
                        this.ToResize.BodyEL = $(this.grid.getBody().$colgroup[0].children[i]);
                        this.ToResize.ResizerEL = $(ev.currentTarget);
                        this.CheckIsUsingPercent(i);
                        this.Start = $(this.grid.getHeader().$colgroup[0].children[i]);
                        this.isPressed = true;
                        this.StartX = ev.pageX;
                        this.StartWidth = this.Start.width();
                        $(document).on('mousemove.' + this.NS, function (e) { return _this.OnMouseMove(e); });
                        $(document).on('mouseup.' + this.NS, function (e) { return _this.OnMouseUp(); });
                    };
                    ColumnResizer.prototype.OnMouseMove = function (ev) {
                        if (this.isPressed) {
                            this.ResizeElements(this.StartWidth + (ev.pageX - this.StartX));
                        }
                    };
                    ColumnResizer.prototype.OnMouseUp = function () {
                        if (this.isPressed) {
                            this.isPressed = false;
                            this.RearrangeResizer();
                            this.RestoreWhenUsingPercent();
                            $(document).off('mousemove.' + this.NS);
                            $(document).off('mouseup.' + this.NS);
                            this.grid.onColumnResized(this.ToResize.ColumnName, this.ToResize.BodyEL.width());
                        }
                    };
                    ColumnResizer.prototype.RearrangeResizer = function () {
                        var columns = this.grid.getVisibleColumns();
                        var ths = this.grid.getHeader().$theadRow[0].children;
                        var allResizer = this.grid.getHeader().GetAllResizer();
                        var allWidth = 0;
                        for (var index = 0; index < allResizer.length; index++) {
                            var res = allResizer[index];
                            var th = ths[index];
                            var offsetWidth = $(th)[0].offsetWidth;
                            if (offsetWidth == 0)
                                allWidth += parseInt(columns[index].width + "");
                            else
                                allWidth += offsetWidth;
                            res.style.left = (allWidth - 5) + 'px';
                            res.style.top = '0px';
                            res.style.height = '100%';
                            res.style.position = 'absolute';
                            res.style.width = '10px';
                            res.style.cursor = 'col-resize';
                        }
                    };
                    ColumnResizer.prototype.ResizeElements = function (newWidth) {
                        if (newWidth > this.MinWidth) {
                            this.Start.width(newWidth);
                            this.ToResize.BodyEL.width(newWidth);
                        }
                    };
                    return ColumnResizer;
                }());
                exports.ColumnResizer = ColumnResizer;


                /***/
},
/* 5 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var Constants_1 = __webpack_require__(0);
                var ColumnWidthHelper = (function () {
                    function ColumnWidthHelper() {
                    }
                    ColumnWidthHelper.generateColumnGroups = function (grid) {
                        var columns = grid.getVisibleColumns();
                        var colGroups = '';
                        var onlyPercentWidth = true;
                        var totalWidth = 0;
                        columns.forEach(function (col) {
                            var width = ColumnWidthHelper.AutoCalcWidth(col, grid.getBody().getAllValuesForColumn(col));
                            if (width.indexOf('%') == -1)
                                onlyPercentWidth = false;
                            col.width = width;
                            totalWidth += parseInt(width);
                            colGroups += '<col style="width:' + width + '">';
                        });
                        if (!(onlyPercentWidth && totalWidth == 100)) {
                            grid.useFillColumn = true;
                            colGroups += '<col style="width:auto;">';
                            var fillCellsExists = grid.getBody().$tbody.find('.' + Constants_1.Constants.CLASS_CELL_FILLCOLUMN).length > 0;
                            if (!fillCellsExists) {
                                var $fillCell = $('<td>').addClass(Constants_1.Constants.CLASS_CELL_FILLCOLUMN);
                                grid.getBody().$table.find('tr').append($fillCell);
                            }
                        }
                        else {
                            grid.useFillColumn = false;
                            grid.getBody().$tbody.find('.' + Constants_1.Constants.CLASS_CELL_FILLCOLUMN).remove();
                        }
                        grid.getBody().$colgroup.empty();
                        grid.getBody().$colgroup.append(colGroups);
                        colGroups += '<col style="width:' + grid.getBody().getScrollbarThickness() + 'px">';
                        grid.getHeader().$colgroup.empty();
                        grid.getHeader().$colgroup.append(colGroups);
                    };
                    ColumnWidthHelper.AutoCalcWidth = function (col, values) {
                        if (col.width && col.widthAutoCalculated !== true) {
                            if (typeof (col.width) == "number")
                                return col.width + 'px';
                            else
                                return col.width + '';
                        }
                        var title = col.title;
                        var result = 0;
                        var widthElement = $('<span>').appendTo('body');
                        var textWidthTitle = ColumnWidthHelper.getWidth(widthElement, title);
                        var titleWidth = textWidthTitle + 22;
                        widthElement.remove();
                        var completeWidth = 0;
                        var averageWidth = 0;
                        var widths = [];
                        var widthPerLetter = Math.max(6.85, textWidthTitle / title.length);
                        for (var i = 0; i < values.length; i++) {
                            var currentValue = values[i];
                            if (!currentValue)
                                continue;
                            else if (currentValue instanceof jQuery)
                                currentValue = currentValue.text();
                            else if (currentValue instanceof HTMLElement)
                                currentValue = currentValue.textContent;
                            var width = currentValue.length * widthPerLetter;
                            widths.push(width);
                            completeWidth += width;
                        }
                        if (widths.length > 0) {
                            averageWidth = completeWidth / widths.length;
                            if (titleWidth < averageWidth && averageWidth > 50) {
                                var widthsBiggerThanAverage = widths.filter(function (width) { return width > averageWidth; });
                                var biggerAverage = ColumnWidthHelper.average(widthsBiggerThanAverage);
                                var maxWidth = Math.max.apply(null, widths);
                                var newWidth = 0;
                                if (maxWidth < 100) {
                                    newWidth = maxWidth + 22 * 1.5;
                                }
                                else if (widthsBiggerThanAverage.length > widths.length * 0.75) {
                                    newWidth = ((biggerAverage + maxWidth) / 2) + 22;
                                }
                                else if (widthsBiggerThanAverage.length > widths.length * 0.50) {
                                    newWidth = biggerAverage + 22;
                                }
                                else {
                                    newWidth = averageWidth + 22;
                                }
                                result = Math.max(titleWidth, newWidth);
                            }
                            else if (titleWidth >= averageWidth) {
                                if (titleWidth - averageWidth < 15) {
                                    result = titleWidth + 10;
                                }
                                else {
                                    result = titleWidth;
                                }
                            }
                        }
                        else {
                            result = titleWidth;
                        }
                        if (result < 50) {
                            result = 50;
                        }
                        else if (result > 500) {
                            result = 500;
                        }
                        col.widthAutoCalculated = true;
                        return result + 'px';
                    };
                    ColumnWidthHelper.average = function (arr) {
                        var sum = 0;
                        if (arr.length === 0) {
                            return 0;
                        }
                        for (var i = 0; i < arr.length; i++) {
                            sum += arr[i];
                        }
                        return sum / arr.length;
                    };
                    ColumnWidthHelper.getWidth = function (element, value) {
                        element.text(value);
                        var width = element.outerWidth() + 11;
                        return width;
                    };
                    return ColumnWidthHelper;
                }());
                exports.ColumnWidthHelper = ColumnWidthHelper;


                /***/
},
/* 6 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var index_1 = __webpack_require__(2);
                var Constants_1 = __webpack_require__(0);
                var EventHelper = (function () {
                    function EventHelper(grid, handler) {
                        this.grid = grid;
                        this.DOUBLECLICK_DELAY = 200;
                        this.DOUBLECLICK_PREVENT = false;
                        this.lastRowClickedWithoutShiftID = undefined;
                        this.lastRowClickedWithoutShiftIndex = undefined;
                        this.onSelectionChangeHandler = handler.onSelectionChangeHandler;
                        this.onSortChangeHandler = handler.onSortChangeHandler;
                        this.onDblClickHandler = handler.onDblClickHandler;
                        this.onMouseOverHandler = handler.onMouseOverHandler;
                        this.onMouseDownHandler = handler.onMouseDownHandler;
                        this.onKeyDownHandler = handler.onKeyDownHandler;
                        this.onSaveHandler = handler.onSaveHandler;
                        this.onDeleteHandler = handler.onDeleteHandler;
                        this.onAddHandler = handler.onAddHandler;
                        this.onBeforeEditHandler = handler.onBeforeEditHandler;
                    }
                    EventHelper.prototype.registerAll = function () {
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_CLICK, 'tr', this.clickRow.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_DBLCLICK, 'tr', this.doubleClickRow.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_MOUSEOVER, 'tr', this.mouseOverRow.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_MOUSEDOWN, 'tr', this.mouseDownRow.bind(this));
                        this.grid.getWrapper().on(Constants_1.Constants.EVENT_KEYDOWN, this.wrapperKeyDown.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.CLASS_ROW_EDITBUTTON, this.editClick.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.CLASS_ROW_DELETEBUTTON, this.deleteClick.bind(this));
                        this.grid.getBody().$tbody.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.CLASS_ROW_SAVEBUTTON, this.saveClick.bind(this));
                        this.grid.getWrapper().on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.CLASS_ADDBUTTON, this.addClick.bind(this));
                        this.grid.getWrapper().on(Constants_1.Constants.EVENT_CLICK, this.wrapperClick.bind(this));
                        this.grid.getHeader().$theadRow.on(Constants_1.Constants.EVENT_CLICK, 'th.sortable', this.sortableHeaderClick.bind(this));
                        this.grid.getBody().getWrapper().on(Constants_1.Constants.EVENT_SCROLL, this.tableBodyScroll.bind(this));
                    };
                    EventHelper.prototype.clickRow = function (ev) {
                        var _this = this;
                        this.DOUBLECLICK_TIMER = setTimeout(function () {
                            if (!_this.DOUBLECLICK_PREVENT) {
                                var $tr = $(ev.currentTarget);
                                if (!ev.shiftKey || (ev.shiftKey && _this.lastRowClickedWithoutShiftIndex == undefined)) {
                                    var rowID = $tr.data('rowId');
                                    _this.lastRowClickedWithoutShiftID = rowID;
                                    _this.lastRowClickedWithoutShiftIndex = _this.grid.getIndexByRowId(rowID);
                                    if ($tr.hasClass(Constants_1.Constants.CLASS_ROW_SELECTED) && ev.ctrlKey)
                                        _this.grid.deselectRowElement($tr);
                                    else
                                        _this.grid.SelectRowElement($tr, ev.ctrlKey, !ev.ctrlKey);
                                }
                                else {
                                    var clickedID = $tr.data('rowId');
                                    var indexOfClickedRow = _this.grid.getIndexByRowId(clickedID);
                                    var startIndex = (indexOfClickedRow >= _this.lastRowClickedWithoutShiftIndex ? _this.lastRowClickedWithoutShiftIndex : indexOfClickedRow);
                                    var endIndex = (indexOfClickedRow < _this.lastRowClickedWithoutShiftIndex ? _this.lastRowClickedWithoutShiftIndex : indexOfClickedRow);
                                    if (endIndex >= _this.grid.getRows().length)
                                        endIndex = (_this.grid.getRows().length - 1);
                                    var ids = [];
                                    for (var x = startIndex; x < endIndex + 1; x++) {
                                        ids.push(_this.grid.getRows()[x][_this.grid.getOptions().rowIdName]);
                                    }
                                    _this.grid.SelectRowByIds(ids, true, true);
                                }
                                _this.fireSelectionChangedHandler();
                            }
                            _this.DOUBLECLICK_PREVENT = false;
                        }, this.DOUBLECLICK_DELAY);
                    };
                    EventHelper.prototype.doubleClickRow = function (ev) {
                        clearTimeout(this.DOUBLECLICK_TIMER);
                        this.DOUBLECLICK_PREVENT = true;
                        if (this.grid.getOptions().doubleClickToEdit)
                            this.editClick(ev);
                        else if (typeof this.onDblClickHandler == "function") {
                            var $tr = $(ev.currentTarget).closest('tr');
                            this.grid.SelectRowElement($tr, false, true);
                            var rowData = $tr.data('dataRaw');
                            var id = $tr.data('rowId');
                            var result = this.onDblClickHandler(id, rowData, ev);
                        }
                    };
                    EventHelper.prototype.mouseOverRow = function (ev) {
                        if (typeof this.onMouseOverHandler == "function") {
                            var $tr = $(ev.currentTarget).closest('tr');
                            var rowData = $tr.data('dataRaw');
                            var id = $tr.data('rowId');
                            var result = this.onMouseOverHandler(id, rowData, ev);
                        }
                    };
                    EventHelper.prototype.mouseDownRow = function (ev) {
                        if (typeof this.onMouseDownHandler == "function") {
                            var $tr = $(ev.currentTarget).closest('tr');
                            var rowData = $tr.data('dataRaw');
                            var id = $tr.data('rowId');
                            var result = this.onMouseDownHandler(id, rowData, ev);
                        }
                    };
                    EventHelper.prototype.editClick = function (ev) {
                        var _this = this;
                        ev.stopPropagation();
                        if (!this.grid.isEditingAllowed())
                            return;
                        var $tr = $(ev.currentTarget).closest('tr');
                        var data = $tr.data('dataRaw');
                        var id = $tr.data('rowId');
                        if (typeof this.onBeforeEditHandler == "function") {
                            this.onBeforeEditHandler(data);
                            this.grid.EnterEditModeById(id);
                        }
                        else if (typeof this.onBeforeEditHandler == "object" && typeof this.onBeforeEditHandler.then == "function") {
                            this.onBeforeEditHandler(data).then(function (enterEditMode) {
                                if (enterEditMode)
                                    _this.grid.EnterEditModeById(id);
                            });
                        }
                        else
                            this.grid.EnterEditModeById(id);
                    };
                    EventHelper.prototype.deleteClick = function (ev) {
                        var _this = this;
                        ev.stopPropagation();
                        if (typeof this.onDeleteHandler == "function") {
                            var $tr = $(ev.currentTarget).closest('tr');
                            this.grid.SelectRowElement($tr, false, true);
                            var data = $tr.data('dataRaw');
                            var id = $tr.data('rowId');
                            var result = this.onDeleteHandler(data);
                            if (typeof result == "object" && typeof result.then == "function")
                                result.then(function (deleteSucces) {
                                    if (deleteSucces)
                                        _this.grid.resetWrapperClasses();
                                });
                            else if (result)
                                this.grid.resetWrapperClasses();
                        }
                    };
                    EventHelper.prototype.saveClick = function (ev) {
                        var _this = this;
                        ev.stopPropagation();
                        var $tr = $(ev.currentTarget).closest('tr');
                        this.grid.SelectRowElement($tr, false, true);
                        var data = $tr.data('dataRaw');
                        var id = $tr.data('rowId');
                        if (typeof this.onSaveHandler == "function") {
                            var result = this.onSaveHandler(data);
                            if (typeof result == "object" && typeof result.then == "function")
                                result.then(function (saveSuccess) {
                                    if (saveSuccess)
                                        _this.grid.getBody().LeaveEditModeFor($tr);
                                });
                            else if (result)
                                this.grid.getBody().LeaveEditModeFor($tr);
                        }
                    };
                    EventHelper.prototype.addClick = function (ev) {
                        if (!this.grid.isAddingAllowed())
                            return;
                        if (typeof this.onAddHandler == "function")
                            this.onAddHandler();
                    };
                    EventHelper.prototype.wrapperClick = function (ev) {
                        var $target = $(ev.target);
                        if (this.grid.getOptions().applyOnLostFocus) {
                            var selectedRows = this.grid.getSelectedRows();
                            if ((selectedRows && selectedRows.length > 0 &&
                                (selectedRows[0] == $target || $.contains(selectedRows[0][0], $target[0])))
                                || !this.grid.getWrapper().hasClass(Constants_1.Constants.CLASS_ISEDITING))
                                return;
                            else {
                                ev.currentTarget = selectedRows[0];
                                this.saveClick(ev);
                            }
                        }
                    };
                    EventHelper.prototype.sortableHeaderClick = function (ev) {
                        if (this.grid.getWrapper().hasClass(Constants_1.Constants.CLASS_ISEDITING))
                            return;
                        var options = this.grid.getOptions();
                        var $th = $(ev.currentTarget);
                        var old = $th.attr('data-' + options.sortDataName);
                        var sortExpression = [];
                        if (!old || old == index_1.SortState.DESC) {
                            sortExpression.push({
                                field: $th.data('sortfield'),
                                dir: index_1.SortState.ASC
                            });
                        }
                        else {
                            sortExpression.push({
                                field: $th.data('sortfield'),
                                dir: index_1.SortState.DESC
                            });
                        }
                        this.grid.getHeader().SetSortExpression(sortExpression);
                        if (typeof this.onSortChangeHandler === "function") {
                            this.onSortChangeHandler(this.grid.getHeader().GetSortExpression());
                        }
                    };
                    EventHelper.prototype.tableBodyScroll = function (ev) {
                        this.grid.getHeader().getWrapper()[0].scrollLeft = ev.currentTarget.scrollLeft;
                    };
                    EventHelper.prototype.resetShiftKlick = function () {
                        this.setParameterForShiftKlick(undefined, undefined);
                    };
                    EventHelper.prototype.setParameterForShiftKlick = function (id, index) {
                        this.lastRowClickedWithoutShiftID = id;
                        this.lastRowClickedWithoutShiftIndex = index;
                    };
                    EventHelper.prototype.wrapperKeyDown = function (ev) {
                        var newSelected = undefined;
                        switch (ev.keyCode) {
                            case 38:
                                ev.preventDefault();
                                newSelected = this.grid.selectPrevious();
                                if (newSelected != null) {
                                    this.setParameterForShiftKlick(newSelected.id, newSelected.index);
                                    this.fireSelectionChangedHandler();
                                }
                                break;
                            case 40:
                                ev.preventDefault();
                                newSelected = this.grid.selectNext();
                                if (newSelected != null) {
                                    this.setParameterForShiftKlick(newSelected.id, newSelected.index);
                                    this.fireSelectionChangedHandler();
                                }
                                break;
                            case 65:
                                if (ev.ctrlKey) {
                                    ev.preventDefault();
                                    this.grid.selectAll();
                                    this.fireSelectionChangedHandler();
                                }
                                break;
                            default:
                                if (typeof this.onKeyDownHandler == "function") {
                                    var $rows = this.grid.getSelectedRows();
                                    if ($rows.length == 0)
                                        return;
                                    var $tr = $($rows[0]);
                                    var rowData = $tr.data('dataRaw');
                                    var id = $tr.data('rowId');
                                    this.onKeyDownHandler(id, rowData, ev);
                                }
                        }
                    };
                    EventHelper.prototype.fireSelectionChangedHandler = function () {
                        if (typeof this.onSelectionChangeHandler == "function") {
                            var selectedData = this.grid.getSelectedData();
                            this.onSelectionChangeHandler(selectedData);
                        }
                    };
                    return EventHelper;
                }());
                exports.EventHelper = EventHelper;


                /***/
},
/* 7 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var Constants_1 = __webpack_require__(0);
                var Footer = (function () {
                    function Footer(options, grid) {
                        this.options = options;
                        this.currentPage = undefined;
                        this.totalPages = undefined;
                        this.totalRows = undefined;
                        this.pageSize = undefined;
                        this.pageSizes = undefined;
                        this.grid = grid;
                        this.initFooter();
                    }
                    Footer.prototype.initFooter = function () {
                        this.pageSizesOpen = false;
                        this.$wrapper = $('<div class="' + Constants_1.Constants.FOOTER_CLASS_PREFIX + '">');
                        if (!this.isFooterVisible())
                            this.$wrapper.css('display', 'none');
                        else {
                            if (this.isSummaryRowEnabled()) {
                                this.initSummaryRow();
                                this.$wrapper.append(this.$summaryRow);
                            }
                            if (this.isPagingEnabled()) {
                                this.initPaging();
                                this.$wrapper.append(this.$paging);
                            }
                        }
                    };
                    Footer.prototype.isFooterVisible = function () {
                        var isPagingEnabled = this.isPagingEnabled();
                        var isSummaryRowEnabled = this.isSummaryRowEnabled();
                        return isPagingEnabled || isSummaryRowEnabled;
                    };
                    Footer.prototype.initSummaryRow = function () {
                        if (!this.$summaryRow)
                            this.$summaryRow = $('<span class="' + Constants_1.Constants.FOOTER_CLASS_SUMMARYROW + '">');
                        this.$summaryRow.empty();
                        var summaryRowContent = '';
                        if (this.options.summaryRow.preText) {
                            summaryRowContent += '<span';
                            if (this.grid.useLocator())
                                summaryRowContent += ' data-locator="' + this.options.locator + '.summaryrow.total.pre"';
                            summaryRowContent += '>' + this.options.summaryRow.preText + '</span>';
                        }
                        summaryRowContent += '<span';
                        if (this.grid.useLocator())
                            summaryRowContent += ' data-locator="' + this.options.locator + '.summaryrow.total.rowcount"';
                        summaryRowContent += '>' + (this.totalRows || this.grid.getTotalRows()) + '</span>';
                        if (this.options.summaryRow.postText) {
                            summaryRowContent += '<span';
                            if (this.grid.useLocator())
                                summaryRowContent += ' data-locator="' + this.options.locator + '.summaryrow.total.post"';
                            summaryRowContent += '>' + this.options.summaryRow.postText + '</span>';
                        }
                        this.$summaryRow.append(summaryRowContent);
                    };
                    Footer.prototype.initPaging = function () {
                        var _this = this;
                        if (!this.$paging) {
                            this.$paging = $('<div>');
                            this.$paging.addClass(Constants_1.Constants.FOOTER_CLASS_PAGING);
                            this.pageSizesWrapper = document.createElement('div');
                            this.pageSizesWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGESIZE);
                            this.optionsWrapper = document.createElement('div');
                            this.optionsWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER);
                            this.optionsWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_HIDDEN);
                            this.pagesWrapper = document.createElement('div');
                            this.pagesWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGES);
                            this.$paging.css('position', 'relative');
                            this.$paging.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTION, function (ev) {
                                ev.stopPropagation();
                                _this.resetShiftKlick();
                                if (typeof _this.options.paging.onPageSizeChanged == "function") {
                                    var newValue = parseInt(ev.currentTarget.textContent);
                                    _this.options.paging.onPageSizeChanged(newValue);
                                    _this.options.paging.onPageChanged(1);
                                    _this.closePageSizes();
                                }
                            });
                            this.$paging.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.FOOTER_CLASS_PAGESIZE, function (ev) {
                                ev.stopPropagation();
                                if (_this.optionsWrapper.classList.contains(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_HIDDEN)) {
                                    _this.openPageSizes();
                                }
                                else {
                                    _this.closePageSizes();
                                }
                            });
                            $('body').on(Constants_1.Constants.EVENT_CLICK, function (ev) {
                                if (_this.pageSizesOpen) {
                                    _this.closePageSizes();
                                }
                            });
                            this.$paging.on(Constants_1.Constants.EVENT_CLICK, '.' + Constants_1.Constants.FOOTER_CLASS_PAGE, function (ev) {
                                if (ev.currentTarget &&
                                    (ev.currentTarget.classList.contains(Constants_1.Constants.FOOTER_CLASS_PAGE_ACTIVE) || ev.currentTarget.classList.contains(Constants_1.Constants.FOOTER_CLASS_PAGE_DISABLED)))
                                    return;
                                _this.resetShiftKlick();
                                var newPage = parseInt($(ev.currentTarget).data('page'));
                                if (_this.currentPage != newPage) {
                                    _this.currentPage = newPage;
                                    _this.options.paging.currentPage = newPage;
                                    _this.redrawPages();
                                    if (typeof _this.options.paging.onPageSizeChanged == "function") {
                                        _this.options.paging.onPageChanged(newPage);
                                    }
                                }
                            });
                        }
                        this.optionsWrapper.innerHTML = '';
                        this.pageSizesWrapper.innerHTML = '';
                        this.pagesWrapper.innerHTML = '';
                        if (this.isPagingEnabled()) {
                            this.redrawPages();
                            if (this.pageSizes) {
                                var pageSize = '<span class="' + Constants_1.Constants.FOOTER_CLASS_PAGESIZE_CURRENT + '"';
                                if (this.grid.useLocator())
                                    pageSize += ' data-locator="' + this.options.locator + '.page-size-select"';
                                pageSize += '>' + this.pageSize + '</span>';
                                var options_1 = '';
                                this.pageSizes.forEach(function (element) {
                                    options_1 += '<span class="' + Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTION + '"';
                                    if (_this.grid.useLocator())
                                        options_1 += ' data-locator="' + _this.options.locator + '.page-size-select.' + element + '"';
                                    options_1 += '>' + element + '</span>';
                                });
                                this.optionsWrapper.innerHTML = options_1;
                                this.pageSizesWrapper.innerHTML = pageSize;
                                this.pageSizesWrapper.appendChild(this.optionsWrapper);
                                this.$paging[0].appendChild(this.pagesWrapper);
                                this.$paging[0].appendChild(this.pageSizesWrapper);
                            }
                        }
                    };
                    Footer.prototype.resetShiftKlick = function () {
                        this.grid.getEventHelper().resetShiftKlick();
                    };
                    Footer.prototype.openPageSizes = function () {
                        this.pageSizesOpen = true;
                        this.optionsWrapper.classList.remove(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_HIDDEN);
                        this.optionsWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_VISIBLE);
                        this.optionsWrapper.style.top = '-' + this.optionsWrapper.offsetHeight + 'px';
                    };
                    Footer.prototype.closePageSizes = function () {
                        this.pageSizesOpen = false;
                        this.optionsWrapper.classList.remove(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_VISIBLE);
                        this.optionsWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGESIZE_OPTIONSWRAPPER_HIDDEN);
                    };
                    Footer.prototype.redrawPages = function () {
                        var pages = '';
                        var startIndex = 1;
                        var endIndex = this.totalPages;
                        if (this.totalPages > 10) {
                            var mod = this.currentPage % 10;
                            if (mod == 0)
                                startIndex = (((this.currentPage / 10) - 1) * 10) + 1;
                            else
                                startIndex = this.currentPage - (this.currentPage % 10) + 1;
                            endIndex = startIndex + 9;
                            if (endIndex > this.totalPages)
                                endIndex = this.totalPages;
                        }
                        pages += '<span class="';
                        pages += Constants_1.Constants.FOOTER_CLASS_PAGE + ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_START;
                        pages += (this.currentPage == 1 ? ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_DISABLED : '') + '"';
                        pages += (this.currentPage == 1 ? '' : ' data-page="' + 1 + '"');
                        pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.first"' : '');
                        pages += '></span>';
                        pages += '<span class="';
                        pages += Constants_1.Constants.FOOTER_CLASS_PAGE + ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_PREVIOUS;
                        pages += (this.currentPage == 1 ? ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_DISABLED : '') + '"';
                        pages += (this.currentPage == 1 ? '' : ' data-page="' + (this.currentPage - 1) + '"');
                        pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.prev"' : '');
                        pages += '></span>';
                        if (this.totalPages > 10 && this.currentPage > 10) {
                            var x = startIndex - 1;
                            pages += '<span class="' + Constants_1.Constants.FOOTER_CLASS_PAGE + '"';
                            pages += ' data-page="' + x + '"';
                            pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.[' + x + ']"' : '');
                            pages += '>...</span>';
                        }
                        for (var x = startIndex; x <= endIndex; x++) {
                            pages += '<span class="';
                            pages += Constants_1.Constants.FOOTER_CLASS_PAGE + (x == this.currentPage ? ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_ACTIVE : '') + '"';
                            pages += ' data-page="' + x + '"';
                            pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.[' + x + ']"' : '');
                            pages += '>' + x + '</span>';
                        }
                        if (endIndex < this.totalPages) {
                            var x = endIndex + 1;
                            pages += '<span class="' + Constants_1.Constants.FOOTER_CLASS_PAGE + '"';
                            pages += ' data-page="' + x + '"';
                            pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.[' + x + ']"' : '');
                            pages += '>...</span>';
                        }
                        pages += '<span class="';
                        pages += Constants_1.Constants.FOOTER_CLASS_PAGE + ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_NEXT;
                        pages += (this.currentPage == this.totalPages ? ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_DISABLED : '') + '"';
                        pages += (this.currentPage == this.totalPages ? '' : ' data-page="' + (this.currentPage + 1) + '"');
                        pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.next"' : '');
                        pages += '></span>';
                        pages += '<span class="';
                        pages += Constants_1.Constants.FOOTER_CLASS_PAGE + ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_END;
                        pages += (this.currentPage == this.totalPages ? ' ' + Constants_1.Constants.FOOTER_CLASS_PAGE_DISABLED : '') + '"';
                        pages += (this.currentPage == this.totalPages ? '' : ' data-page="' + this.totalPages + '"');
                        pages += (this.grid.useLocator() ? ' data-locator="' + this.options.locator + '.page.last"' : '');
                        pages += '></span>';
                        this.pagesWrapper.innerHTML = pages;
                        if (this.totalPages == 1)
                            this.pagesWrapper.classList.add(Constants_1.Constants.FOOTER_CLASS_PAGES_DISABLED);
                        else
                            this.pagesWrapper.classList.remove(Constants_1.Constants.FOOTER_CLASS_PAGES_DISABLED);
                    };
                    Footer.prototype.isPagingEnabled = function () {
                        return this.options.paging && this.options.paging.isEnabled;
                    };
                    Footer.prototype.isSummaryRowEnabled = function () {
                        return this.options.summaryRow && this.options.summaryRow.isEnabled;
                    };
                    Footer.prototype.updatePaging = function (paging) {
                        var reInit = false;
                        if (this.currentPage != paging.currentPage) {
                            this.currentPage = paging.currentPage;
                            reInit = true;
                        }
                        if (this.pageSize != paging.pageSize) {
                            this.pageSize = paging.pageSize;
                            reInit = true;
                        }
                        if (this.pageSizes != paging.pageSizes) {
                            this.pageSizes = paging.pageSizes;
                            reInit = true;
                        }
                        var totalRows = this.options.paging.totalRows || this.grid.getTotalRows();
                        if (this.totalRows != totalRows) {
                            this.totalRows = totalRows;
                            reInit = true;
                        }
                        this.totalPages = this.totalRows / this.options.paging.pageSize;
                        if (this.totalPages % 1 !== 0)
                            this.totalPages = parseInt(this.totalPages + '') + 1;
                        if (this.totalPages < 1)
                            this.totalPages = 1;
                        if (reInit) {
                            this.initPaging();
                            this.updateSummaryRow();
                        }
                    };
                    Footer.prototype.updateSummaryRow = function () {
                        this.initSummaryRow();
                    };
                    Footer.prototype.getWrapper = function () {
                        return this.$wrapper;
                    };
                    return Footer;
                }());
                exports.Footer = Footer;


                /***/
},
/* 8 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                var __extends = (this && this.__extends) || (function () {
                    var extendStatics = Object.setPrototypeOf ||
                        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
                    return function (d, b) {
                        extendStatics(d, b);
                        function __() { this.constructor = d; }
                        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
                    };
                })();
                Object.defineProperty(exports, "__esModule", { value: true });
                var HTMLTable_1 = __webpack_require__(1);
                var Constants_1 = __webpack_require__(0);
                var BodyTable = (function (_super) {
                    __extends(BodyTable, _super);
                    function BodyTable(options, grid) {
                        var _this = _super.call(this, 'GridBodyTable', options, grid) || this;
                        _this.$tbody = undefined;
                        _this.ComponentManager = undefined;
                        _this.Factory = undefined;
                        _this.$tbody = $('<tbody role="rowgroup">');
                        _this.$tbody.attr('tabindex', '-1');
                        _this.$tbody.appendTo(_this.$table);
                        _this.$wrapper[0].style.overflowX = "auto";
                        _this.$wrapper[0].style.overflowY = "scroll";
                        try {
                            _this.ComponentManager = __webpack_require__(3);
                            if (_this.ComponentManager && !_this.ComponentManager.getFactory) {
                                _this.ComponentManager = null;
                            }
                            _this.Factory = __webpack_require__(11);
                            if (_this.Factory && !_this.Factory.createInstance) {
                                _this.Factory = null;
                            }
                        }
                        catch (_a) {
                        }
                        return _this;
                    }
                    BodyTable.prototype.getScrollbarThickness = function () {
                        var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'), widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
                        $outer.remove();
                        return 100 - widthWithScroll;
                    };
                    BodyTable.prototype.FillBody = function (rows) {
                        var _this = this;
                        this.$tbody.empty();
                        if (!rows) {
                            return;
                        }
                        var cols = this.grid.getVisibleColumns();
                        var trs = [];
                        rows.forEach(function (row, index) {
                            var $row = _this.CreateDataRow(row, index);
                            trs.push($row);
                        });
                        this.$tbody.append(trs);
                    };
                    BodyTable.prototype.EnterEditModeFor = function ($row, scrollToELement) {
                        var $newRow = this.ChangeEditModeFor($row, true);
                        if (scrollToELement)
                            this.$wrapper[0].scrollTop = $newRow[0].offsetTop;
                        return $newRow;
                    };
                    BodyTable.prototype.LeaveEditModeFor = function ($row) {
                        this.ChangeEditModeFor($row, false);
                    };
                    BodyTable.prototype.ChangeEditModeFor = function ($row, editMode) {
                        var rowData = $row.data('dataRaw');
                        var index = this.grid.dataRows.indexOf(rowData);
                        var $newEditRow = this.CreateDataRow(rowData, index, editMode);
                        $row.replaceWith($newEditRow);
                        $row.remove();
                        this.grid.resetWrapperClasses();
                        return $newEditRow;
                    };
                    BodyTable.prototype.CreateDataRow = function (row, index, editMode) {
                        if (editMode === void 0) { editMode = false; }
                        var $tr = $('<tr onselectstart="return false" class="' + Constants_1.Constants.CLASS_ROW + '">');
                        var $tdMain = $('<td onselectstart="return false" role="gridcell">');
                        if (editMode) {
                            $tr.addClass(Constants_1.Constants.CLASS_ROW_IN_EDIT);
                        }
                        var cols = this.grid.getVisibleColumns();
                        var $saveButtonMain = $('<div class="' + Constants_1.Constants.CLASS_ROW_BUTTON + ' ' + Constants_1.Constants.CLASS_ROW_SAVEBUTTON + '">');
                        var $editButtonMain = $('<div class="' + Constants_1.Constants.CLASS_ROW_BUTTON + ' ' + Constants_1.Constants.CLASS_ROW_EDITBUTTON + '">');
                        var $deleteButtonMain = $('<div class="' + Constants_1.Constants.CLASS_ROW_BUTTON + ' ' + Constants_1.Constants.CLASS_ROW_DELETEBUTTON + '">');
                        var _loop_1 = function (colIndex) {
                            var currentCol = cols[colIndex];
                            var raw = currentCol.value || currentCol.field;
                            var uiGenerator = this_1.ResolveUiGenerator(currentCol.uiGenerator);
                            var editor = this_1.ResolveUiGenerator(currentCol.editor);
                            var $td = $tdMain.clone();
                            if (cols[colIndex].isEditColumn) {
                                if (editMode) {
                                    var $saveButton = $saveButtonMain.clone();
                                    $saveButton.appendTo($td);
                                    var $deleteButton = $deleteButtonMain.clone();
                                    $deleteButton.appendTo($td);
                                }
                                else {
                                    var $editButton = $editButtonMain.clone();
                                    $editButton.appendTo($td);
                                    var $deleteButton = $deleteButtonMain.clone();
                                    $deleteButton.appendTo($td);
                                }
                            }
                            else {
                                var cellContent = void 0;
                                if (editMode) {
                                    if (typeof editor == "function")
                                        cellContent = editor(raw, row);
                                    else
                                        cellContent = uiGenerator(raw, row);
                                }
                                else
                                    cellContent = uiGenerator(raw, row);
                                if (cellContent && typeof cellContent === 'object' && typeof cellContent.then === 'function') {
                                    cellContent.then(function (value) {
                                        $td.append(value);
                                    });
                                }
                                else {
                                    $td.append(cellContent);
                                }
                            }
                            if (cols[colIndex].dataClass)
                                $td.addClass(cols[colIndex].dataClass);
                            $td.attr('data-column', cols[colIndex].name);
                            if (this_1.grid.useLocator())
                                $td.attr('data-locator', this_1._Options.locator + '.row.' + index + '.col.[' + cols[colIndex].name + ']');
                            $td.appendTo($tr);
                        };
                        var this_1 = this;
                        for (var colIndex = 0; colIndex < cols.length; colIndex++) {
                            _loop_1(colIndex);
                        }
                        if (this.grid.useFillColumn) {
                            var fillCell = $tdMain.clone();
                            fillCell.addClass(Constants_1.Constants.CLASS_CELL_FILLCOLUMN);
                            $tr.append(fillCell);
                        }
                        if (this.grid.useLocator())
                            $tr.attr('data-locator', this._Options.locator + '.row.' + index);
                        //Edited by P-cation 20201001
                        $tr.attr('rowId', row[this._Options.rowIdName]);
                        $tr.data('dataRaw', row);
                        $tr.data('rowId', row[this._Options.rowIdName]);
                      
                        return $tr;
                    };
                    BodyTable.prototype.ResolveUiGenerator = function (uiGenerator) {
                        var _this = this;
                        if (typeof uiGenerator == "function") {
                            return function (cell, row) {
                                if (typeof cell == "function")
                                    return uiGenerator(cell(row), row);
                                else
                                    return uiGenerator(row[cell], row);
                            };
                        }
                        return function (cell, row) {
                            if (typeof cell == "function")
                                return cell(row);
                            else if (typeof row[cell] == "string") {
                                return _this.escape(row[cell]);
                            }
                            else
                                return row[cell];
                        };
                    };
                    BodyTable.prototype.escape = function (str) {
                        var escapedString = str;
                        escapedString = escapedString.replace('&', '&amp;');
                        escapedString = escapedString.replace('#', '&num;');
                        escapedString = escapedString.replace('<', '&lt;');
                        escapedString = escapedString.replace('>', '&gt;');
                        escapedString = escapedString.replace('"', '&quot;');
                        escapedString = escapedString.replace('`', '&#96;');
                        escapedString = escapedString.replace('\'', '&#x27;');
                        return escapedString;
                    };
                    BodyTable.prototype.getAllValuesForColumn = function (column) {
                        var raw = column.value || column.field;
                        var uiGenerator = this.ResolveUiGenerator(column.uiGenerator);
                        var values = [];
                        this.grid.getRows().forEach(function (row) {
                            values.push(uiGenerator(raw, row));
                        });
                        return values;
                    };
                    return BodyTable;
                }(HTMLTable_1.HTMLTable));
                exports.BodyTable = BodyTable;


                /***/
},
/* 9 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                var __extends = (this && this.__extends) || (function () {
                    var extendStatics = Object.setPrototypeOf ||
                        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
                    return function (d, b) {
                        extendStatics(d, b);
                        function __() { this.constructor = d; }
                        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
                    };
                })();
                Object.defineProperty(exports, "__esModule", { value: true });
                var HTMLTable_1 = __webpack_require__(1);
                var Constants_1 = __webpack_require__(0);
                var HeaderTable = (function (_super) {
                    __extends(HeaderTable, _super);
                    function HeaderTable(options, grid) {
                        var _this = _super.call(this, 'GridHeaderTable', options, grid) || this;
                        _this.$thead = undefined;
                        _this.$theadRow = undefined;
                        _this._BodyScrollbarThickness = undefined;
                        _this.ComponentManager = undefined;
                        _this.ResourceManager = undefined;
                        _this.$thead = $('<thead role="rowgroup">');
                        _this.$theadRow = $('<tr role="row">');
                        _this.$theadRow.appendTo(_this.$thead);
                        _this.$thead.appendTo(_this.$table);
                        _this.$wrapper[0].style.overflow = 'hidden';
                        _this.$wrapper[0].style.position = 'relative';
                        try {
                            _this.ComponentManager = __webpack_require__(3);
                            _this.ResourceManager = __webpack_require__(12);
                            if (_this.ResourceManager && !_this.ResourceManager.get) {
                                _this.ResourceManager = null;
                            }
                            if (_this.ComponentManager && !_this.ComponentManager.getFactory) {
                                _this.ComponentManager = null;
                            }
                        }
                        catch (_a) {
                        }
                        return _this;
                    }
                    HeaderTable.prototype.GetSortExpression = function () {
                        var _this = this;
                        var ret = [];
                        this.$theadRow.find('[data-' + this._Options.sortDataName + ']').each(function (i, element) {
                            var $el = $(element);
                            var sort = $el.attr('data-' + _this._Options.sortDataName);
                            var value = $el.data('sortfield');
                            ret.push({
                                dir: sort,
                                field: value
                            });
                        });
                        return ret;
                    };
                    HeaderTable.prototype.SetSortExpression = function (sort) {
                        var _this = this;
                        if (sort == null || sort.length == 0)
                            return;
                        var $allTh = this.$theadRow.find('th');
                        $allTh.removeAttr('data-' + this._Options.sortDataName);
                        $allTh.each(function (index, element) {
                            var $elem = $(element);
                            if ($elem.data('sortfield') == sort[0].field)
                                $elem.attr('data-' + _this._Options.sortDataName, sort[0].dir);
                        });
                    };
                    HeaderTable.prototype.ResolveErgName = function (ergname) {
                        if (!ergname)
                            ergname = '';
                        if (this.ResourceManager) {
                            return this.ResourceManager.get(ergname);
                        }
                        return ergname;
                    };
                    HeaderTable.prototype.FillHeader = function () {
                        var _this = this;
                        var _ths = [];
                        var _newResizer = '';
                        var sortedCols = this.grid.getVisibleColumns();
                        sortedCols.forEach(function (col, index) {
                            var title = (col.ergName ? _this.ResolveErgName(col.ergName) : col.title);
                            var filteredTitle = (title ? title : '');
                            var $th = $('<th role="columnheader">' + filteredTitle + '</th>');
                            if (_this.grid.useLocator())
                                $th.attr('data-locator', _this._Options.locator + '.header.[' + (col.name ? col.name : (col.field ? col.field : filteredTitle)) + ']');
                            if (col.field)
                                $th.attr('data-field', col.field);
                            $th.attr('data-title', filteredTitle);
                            $th.attr('data-index', index);
                            if (col.isSortable && col.sortValue) {
                                $th.addClass('sortable');
                                $th.data('sortfield', col.sortValue);
                            }
                            _ths.push($th);
                            _newResizer += '<div class="' + Constants_1.Constants.CLASS_RESIZER + '" data-id="' + index + '"  data-columnname="' + col.name + '"></div>';
                        });
                        _ths.push(document.createElement("th"));
                        _ths.push(document.createElement("th"));
                        this.$wrapper.children('.' + Constants_1.Constants.CLASS_RESIZER).remove();
                        this.$wrapper.append(_newResizer);
                        this.$theadRow.empty();
                        this.$theadRow.append(_ths);
                    };
                    HeaderTable.prototype.isHeaderVisible = function () {
                        return this._Options.showHeader;
                    };
                    HeaderTable.prototype.GetAllResizer = function () {
                        return this.$wrapper.find('div.' + Constants_1.Constants.CLASS_RESIZER);
                    };
                    return HeaderTable;
                }(HTMLTable_1.HTMLTable));
                exports.HeaderTable = HeaderTable;


                /***/
},
/* 10 */
/***/ function (module, exports, __webpack_require__) {

                "use strict";
                "use strict";
                Object.defineProperty(exports, "__esModule", { value: true });
                var HTMLTable_1 = __webpack_require__(1);
                exports.HTMLTable = HTMLTable_1.HTMLTable;
                var HTMLBodyTable_1 = __webpack_require__(8);
                exports.BodyTable = HTMLBodyTable_1.BodyTable;
                var HTMLHeaderTable_1 = __webpack_require__(9);
                exports.HeaderTable = HTMLHeaderTable_1.HeaderTable;


                /***/
},
/* 11 */
/***/ function (module, exports) {

                if (typeof require('Factories/Factory') === 'undefined') { var e = new Error("Cannot find module \"require('Factories/Factory')\""); e.code = 'MODULE_NOT_FOUND'; throw e; }
                module.exports = require('Factories/Factory');

                /***/
},
/* 12 */
/***/ function (module, exports) {

                if (typeof require('Management/ResourceManager') === 'undefined') { var e = new Error("Cannot find module \"require('Management/ResourceManager')\""); e.code = 'MODULE_NOT_FOUND'; throw e; }
                module.exports = require('Management/ResourceManager');

                /***/
},
/* 13 */
/***/ function (module, exports, __webpack_require__) {

                module.exports = __webpack_require__(2);


                /***/
}
/******/]);