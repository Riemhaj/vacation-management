﻿var CF1 =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

"use strict";
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Component = (function () {
    function Component(options) {
        this._Wrapper = document.createElement('div');
        this.Options = options;
    }
    return Component;
}());
exports.Component = Component;


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var editor_1 = __webpack_require__(3);
var toolbar_1 = __webpack_require__(4);
var DOMCleaner_1 = __webpack_require__(2);
var RichTextEditor = (function () {
    function RichTextEditor(node, options) {
        var _this = this;
        this._Wrapper = undefined;
        this.Editor = undefined;
        this.Toolbar = undefined;
        this.Options = {};
        this.CSS_READONLY = "readonly";
        this.SetDefaultOptions(options);
        this.Editor = new editor_1.Editor(this.Options);
        this.Toolbar = new toolbar_1.Toolbar(this.Options);
        this._Wrapper = node;
        $(this._Wrapper).empty();
        this._Wrapper.className = this.Options.cssPrefix;
        this.AppendToWrapper(this.Toolbar);
        this.AppendToWrapper(this.Editor);
        this.Editor.InitIframe().then(function () {
            _this.RegisterEvents();
            _this.RegisterApi();
        });
        return {
            SetValue: this.SetValue.bind(this),
            GetValue: this.GetCleanValue.bind(this),
            SetReadOnly: this.SetReadOnly.bind(this),
            SetLanguage: this.SetLanguage.bind(this)
        };
    }
    RichTextEditor.prototype.RegisterEvents = function () {
        var _this = this;
        this.Toolbar.$Wrapper.on('click', 'button', function (ev) {
            if (_this.Options.ReadOnly)
                return;
            var cmd = ev.currentTarget.dataset.cmd;
            if (cmd) {
                _this.Editor.ExecCommand(cmd);
            }
        });
        this.Toolbar.$Wrapper.on('change', 'select', function (ev) {
            if (_this.Options.ReadOnly)
                return;
            var cmd = ev.currentTarget.dataset.cmd;
            var selection = ev.currentTarget.selectedOptions;
            var value = selection && selection.length > 0 ? selection[0].value : null;
            if (value && cmd) {
                if (cmd == "fontName")
                    _this.Editor.ChangeFontName(value);
                if (cmd == "fontSize")
                    _this.Editor.ChangeFontSize(value);
            }
        });
    };
    RichTextEditor.prototype.RegisterApi = function () {
        var _this = this;
        this.Editor.RegisterOnChanges(function () {
            $(_this._Wrapper).trigger('TextChanged', [_this.GetCleanValue()]);
        }, 200);
    };
    RichTextEditor.prototype.SetValue = function (val) {
        this.Editor.SetValue(DOMCleaner_1.DOMCleaner.Clean(val));
    };
    Object.defineProperty(RichTextEditor.prototype, "RAWValue", {
        get: function () {
            return this.Editor.GetValue();
        },
        enumerable: true,
        configurable: true
    });
    RichTextEditor.prototype.GetCleanValue = function () {
        var raw = this.RAWValue;
        return DOMCleaner_1.DOMCleaner.Clean(raw);
    };
    RichTextEditor.prototype.SetReadOnly = function (readonly) {
        if (this.Options.ReadOnly != readonly) {
            this.Options.ReadOnly = readonly;
            this.Editor.isReadOnly = readonly;
            if (readonly) {
                this._Wrapper.classList.add(this.CSS_READONLY);
            }
            else {
                this._Wrapper.classList.remove(this.CSS_READONLY);
            }
        }
    };
    RichTextEditor.prototype.SetLanguage = function (language) {
        this.Editor.SetLanguage(language);
    };
    RichTextEditor.prototype.SetDefaultOptions = function (opt) {
        opt = opt || {};
        this.Options.cssPrefix = opt.cssPrefix || "coreframe-richtextbox";
        this.Options.ReadOnly = opt.ReadOnly != null ? opt.ReadOnly : false;
        this.Options.Value = opt.Value || '';
        this.Options.Language = opt.Language || '';
        this.Options.iconBaseClass = opt.iconBaseClass || 'k-icon';
        this.Options.toolbarItems = opt.toolbarItems || this.GetDefaultToolbarItems();
    };
    RichTextEditor.prototype.GetDefaultToolbarItems = function () {
        var items = [];
        items.push({ CMD: 'bold', iconClass: 'k-i-bold', tooltip: '' });
        items.push({ CMD: 'italic', iconClass: 'k-i-italic', tooltip: '' });
        items.push({ CMD: 'underline', iconClass: 'k-i-underline', tooltip: '' });
        items.push({ CMD: 'strikeThrough', iconClass: 'k-i-strikethrough', tooltip: '' });
        items.push({ isSeperator: true });
        items.push({
            CMD: 'fontName', tooltip: '', values: [
                "Arial",
                "Calibri",
                "Comic Sans MS"
            ]
        });
        return items;
    };
    RichTextEditor.prototype.AppendToWrapper = function (component) {
        this._Wrapper.appendChild(component._Wrapper);
    };
    return RichTextEditor;
}());
exports.RichTextEditor = RichTextEditor;


/***/ },
/* 2 */
/***/ function(module, exports) {

"use strict";
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DOMCleaner = (function () {
    function DOMCleaner() {
    }
    DOMCleaner.Clean = function (nodeOrString) {
        if (typeof nodeOrString == "string") {
            this.Log("Create TMP Element", nodeOrString);
            var tmp = document.createElement('div');
            tmp.className = this.TMPCLASSNAME;
            tmp.innerHTML = nodeOrString;
            return this.Clean(tmp);
        }
        else {
            var node = nodeOrString;
            this.CleanChildren(node);
            if (node.nodeName == "DIV" && node.className == this.TMPCLASSNAME) {
                this.Log("returning from tmp", node.innerHTML);
                return node.innerHTML;
            }
            this.Log("returning", node.outerHTML);
            return node.outerHTML;
        }
    };
    DOMCleaner.CleanChildren = function (parent) {
        this.Log("cleaning node", parent);
        for (var i = 0; i < parent.children.length; i++) {
            var childNode = parent.children[i];
            if (this.HasNoContent(childNode)) {
                parent.removeChild(childNode);
            }
            else if (this.SKIP_TAGS.indexOf(childNode.nodeName) == -1) {
                this.Log("Skip Tag", childNode.nodeName);
                this.CleanChildren(childNode);
                var tmp = document.createElement('span');
                tmp.innerHTML = childNode.innerHTML;
                parent.appendChild(tmp);
                parent.replaceChild(tmp, childNode);
            }
            else {
                this.RemoveAttributes(childNode);
                this.CleanChildren(childNode);
            }
        }
    };
    DOMCleaner.HasNoContent = function (node) {
        var hasBRinside = node.innerHTML.indexOf('<br') > -1;
        return node.innerText == '' && !hasBRinside;
    };
    DOMCleaner.RemoveAttributes = function (node) {
        for (var i = 0; i < node.attributes.length; i++) {
            var attr = node.attributes[i];
            if (this.SKIP_ATTRIBUTES.indexOf(attr.name) == -1) {
                this.Log("Skip Attr", attr.name);
                node.removeAttribute(attr.name);
            }
        }
    };
    DOMCleaner.Log = function (msg, data) {
        if (this.IS_DEBUG)
            console.log(msg, data);
    };
    DOMCleaner.IS_DEBUG = false;
    DOMCleaner.SKIP_ATTRIBUTES = ['face'];
    DOMCleaner.SKIP_TAGS = ['B', 'I', 'U', 'STRIKE', 'BR', 'P', 'FONT', 'SPAN'];
    DOMCleaner.TMPCLASSNAME = "tempDiv";
    return DOMCleaner;
}());
exports.DOMCleaner = DOMCleaner;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = __webpack_require__(0);
var Editor = (function (_super) {
    __extends(Editor, _super);
    function Editor(opt) {
        var _this = _super.call(this, opt) || this;
        _this.isInit = false;
        _this.IFrame = document.createElement('iframe');
        _this.IFrame.style.height = '100%';
        _this.IFrame.style.width = '100%';
        _this.IFrame.style.border = '0';
        _this._Wrapper.appendChild(_this.IFrame);
        _this._Wrapper.className = "editor";
        return _this;
    }
    Object.defineProperty(Editor.prototype, "isReadOnly", {
        set: function (readonly) {
            this.Body.contentEditable = (!readonly).toString();
        },
        enumerable: true,
        configurable: true
    });
    Editor.prototype.InitIframe = function () {
        var _this = this;
        var def = $.Deferred();
        window.setTimeout(function () {
            var doc = (_this.IFrame.contentWindow || _this.IFrame.contentDocument);
            if (doc.document)
                doc = doc.document;
            _this.IFrameDocument = doc;
            _this.Body = _this.IFrameDocument.body;
            _this.Body.contentEditable = (!_this.Options.ReadOnly).toString();
            _this.Body.spellcheck = false;
            _this.ExecCommand('DefaultParagraphSeparator', 'p');
            _this.IFrameStyleSheet = (function () {
                var style = document.createElement('style');
                style.appendChild(document.createTextNode(""));
                _this.IFrameDocument.head.appendChild(style);
                return style.sheet;
            })();
            _this.IFrameStyleSheet.insertRule("html { height:100%;}", 0);
            _this.IFrameStyleSheet.insertRule("p { margin:0; }", 0);
            _this.IFrameStyleSheet.insertRule("body { height:calc(100% - 10px); margin: 0; padding:5px 0 5px 5px;}", 0);
            _this.isInit = true;
            _this.SetValue(_this.Options.Value);
            _this.SetLanguage(_this.Options.Language);
            def.resolve();
        });
        return def;
    };
    Editor.prototype.RegisterOnChanges = function (callback, throttle) {
        var _this = this;
        var timeOut = null;
        this.Body.addEventListener('input', function () {
            window.clearTimeout(timeOut);
            timeOut = window.setTimeout(function () {
                if (typeof callback == "function")
                    callback();
            }, throttle);
        });
        this.Body.addEventListener('paste', function (e) {
            e.preventDefault();
            var txt = e.clipboardData.getData("text/plain");
            var encoded = _this.escape(txt).replace(/\n/g, "<br />");
            _this.ExecCommand("insertHTML", encoded);
        });
    };
    Editor.prototype.ExecCommand = function (cmd, value) {
        this.IFrameDocument.execCommand(cmd, false, value);
    };
    Editor.prototype.ChangeFontName = function (fontName) {
        this.ExecCommand('fontName', fontName);
    };
    Editor.prototype.ChangeFontSize = function (fontSize) {
        this.ExecCommand('fontSize', "1");
        var fontElements = this.IFrameDocument.getElementsByTagName("font");
        for (var i = 0, len = fontElements.length; i < len; ++i) {
            if (fontElements[i].size == 1) {
                fontElements[i].removeAttribute("size");
                fontElements[i].style.fontSize = fontSize;
            }
        }
    };
    Editor.prototype.GetValue = function () {
        return this.isInit ? this.Body.innerHTML : '';
    };
    Editor.prototype.SetValue = function (val) {
        var _old = this.GetHash(this.GetValue());
        var _new = this.GetHash(val);
        if (this.isInit && _old != _new)
            this.Body.innerHTML = val;
    };
    Editor.prototype.SetLanguage = function (val) {
        var $html = $(this.IFrameDocument.getElementsByTagName('html'));
        $html.attr('lang', val);
    };
    Editor.prototype.GetHash = function (str) {
        var hash = 0, i, chr, len;
        if (str.length === 0)
            return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    };
    Editor.prototype.escape = function (str) {
        var escapedString = str;
        escapedString = escapedString.replace('&', '&amp;');
        escapedString = escapedString.replace('#', '&num;');
        escapedString = escapedString.replace('<', '&lt;');
        escapedString = escapedString.replace('>', '&gt;');
        escapedString = escapedString.replace('"', '&quot;');
        escapedString = escapedString.replace('`', '&#96;');
        escapedString = escapedString.replace('\'', '&#x27;');
        return escapedString;
    };
    return Editor;
}(component_1.Component));
exports.Editor = Editor;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = __webpack_require__(0);
var Toolbar = (function (_super) {
    __extends(Toolbar, _super);
    function Toolbar(opt) {
        var _this = _super.call(this, opt) || this;
        _this._Wrapper.className = 'toolbar';
        _this._Wrapper.innerText = "toolbar";
        _this.$Wrapper = $(_this._Wrapper);
        _this.RenderToolButtons();
        return _this;
    }
    Toolbar.prototype.RenderToolButtons = function () {
        var _this = this;
        this.$Wrapper.empty();
        var itemsArray = [];
        this.Options.toolbarItems.forEach(function (item) {
            if (_this.isSeperator(item)) {
                itemsArray.push(_this.CreateSeperator(item));
            }
            else if (_this.isSelectableItem(item)) {
                itemsArray.push(_this.CreateSelectableItem(item));
            }
            else {
                itemsArray.push(_this.CreateItem(item));
            }
        });
        this.$Wrapper.append(itemsArray);
    };
    Toolbar.prototype.CreateSeperator = function (item) {
        var span = document.createElement('span');
        span.className = 'seperator';
        return span;
    };
    Toolbar.prototype.CreateItem = function (item) {
        var i = document.createElement('button');
        i.type = "button";
        i.dataset.cmd = item.CMD;
        var span = document.createElement('span');
        span.classList.add(this.Options.iconBaseClass);
        span.classList.add(item.iconClass);
        if (item.text) {
            span.textContent = item.text;
        }
        i.appendChild(span);
        return i;
    };
    Toolbar.prototype.CreateSelectableItem = function (item) {
        var select = document.createElement('select');
        select.dataset.cmd = item.CMD;
        item.values.forEach(function (option) {
            var opt = document.createElement('option');
            opt.value = typeof option == "string" ? option : option.value;
            opt.text = typeof option == "string" ? option : option.text;
            select.appendChild(opt);
        });
        return select;
    };
    Toolbar.prototype.isSeperator = function (arg) {
        return arg.isSeperator !== undefined;
    };
    Toolbar.prototype.isSelectableItem = function (arg) {
        return arg.values !== undefined;
    };
    return Toolbar;
}(component_1.Component));
exports.Toolbar = Toolbar;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }
/******/ ]);