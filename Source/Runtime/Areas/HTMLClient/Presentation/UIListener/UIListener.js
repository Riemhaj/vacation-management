/// <amd-module name="UIListener/UIListener" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("UIListener/UIListener", ["require", "exports", "Class", "Management/ComponentManager"], function (require, exports, Class, ComponentManager) {
    "use strict";
    var UIListener = /** @class */ (function (_super) {
        __extends(UIListener, _super);
        function UIListener(params) {
            var _this = _super.call(this) || this;
            _this.subUIListeners = [];
            _this.applyPrototypeToInstance(UIListener);
            return _this;
        }
        //#region HOOKS
        UIListener.prototype.onBeforeSave = function (context) { };
        ;
        UIListener.prototype.onBeforeApply = function (context) { };
        ;
        UIListener.prototype.onBeforeCancel = function (context) { };
        ;
        UIListener.prototype.onAfterDeleteFromListView = function (context) { };
        ;
        UIListener.prototype.onAfterApply = function (context, isClosing) { };
        ;
        UIListener.prototype.onPropertyValueUpdated = function (context, property) { };
        ;
        UIListener.prototype.onDetailViewReady = function (context) { };
        ;
        UIListener.prototype.onDetailViewClosed = function (context) { };
        ;
        UIListener.prototype.onAssociationUpdated = function (context, relation) { };
        ;
        UIListener.prototype.onAfterCallOperation = function (context, operation, args) { };
        ;
        UIListener.prototype.onBeforeCallOperation = function (context, operation) { };
        ;
        UIListener.prototype.onBeforeRender = function (context) { };
        ;
        UIListener.prototype.onBeforeAssociationUpdated = function (context, relation, actionType) { };
        ;
        UIListener.prototype.onBeforeRenderSummaryView = function (context) { };
        ;
        UIListener.prototype.onSummaryViewReady = function (context) { };
        ;
        UIListener.prototype.onBeforeAssociationUpdatedAsync = function (context, relation, actionType) { };
        ;
        UIListener.prototype.onAfterRefresh = function (context) { };
        ;
        UIListener.prototype.onGenericObjectUpdated = function (context, updatedProperties) { };
        ;
        UIListener.prototype.onListViewReady = function (context) { };
        ;
        UIListener.prototype.onPartialRendered = function (context) { };
        ;
        //#endregion
        UIListener.prototype.createChain = function () {
            var _this = this;
            for (var i = 0; i < arguments.length; i++) {
                var uiListener = null;
                var params = {};
                var argument = arguments[i];
                if (typeof argument === "string") {
                    uiListener = argument;
                }
                else if (typeof argument === "object" && argument.hasOwnProperty('uiListener') && argument.hasOwnProperty('params')) {
                    if (typeof argument.uiListener === "string" && typeof argument.params === "object") {
                        uiListener = argument.uiListener;
                        params = argument.params;
                    }
                }
                if (uiListener) {
                    var thisSubUIListener = UIListener.createInstance(uiListener, params);
                    this.subUIListeners.push(thisSubUIListener);
                }
            }
            this.onBeforeSave = function (context) {
                return _this.noneAsync(_.invoke(_this.subUIListeners, 'onBeforeSave', context), false);
            };
            this.onBeforeApply = function (context) {
                return _this.noneAsync(_.invoke(_this.subUIListeners, 'onBeforeApply', context), false);
            };
            this.onAfterApply = function (context, isClosing) {
                _.invoke(_this.subUIListeners, 'onAfterApply', context, isClosing);
            };
            this.onAfterDeleteFromListView = function (context) {
                _.invoke(_this.subUIListeners, 'onAfterDeleteFromListView', context);
            };
            this.onBeforeCancel = function (context) {
                return _this.noneAsync(_.invoke(_this.subUIListeners, 'onBeforeCancel', context), false);
            };
            this.onPropertyValueUpdated = function (context, property) {
                _.invoke(_this.subUIListeners, 'onPropertyValueUpdated', context, property);
            };
            this.onDetailViewReady = function (context) {
                _.invoke(_this.subUIListeners, 'onDetailViewReady', context);
            };
            this.onDetailViewClosed = function (context) {
                _.invoke(_this.subUIListeners, 'onDetailViewClosed', context);
            };
            this.onAssociationUpdated = function (context, relation) {
                _.invoke(_this.subUIListeners, 'onAssociationUpdated', context, relation);
            };
            this.onBeforeAssociationUpdated = function (context, relation, actionType) {
                return _this.none(_.invoke(_this.subUIListeners, 'onBeforeAssociationUpdated', context, relation, actionType), false);
            };
            this.onBeforeAssociationUpdatedAsync = function (context, relation, actionType) {
                return _this.noneAsync(_.invoke(_this.subUIListeners, 'onBeforeAssociationUpdatedAsync', context, relation, actionType), false);
            };
            this.onAfterCallOperation = function (context, operation, args) {
                _.invoke(_this.subUIListeners, 'onAfterCallOperation', context, operation, args);
            };
            this.onBeforeCallOperation = function (context, operation) {
                return _this.none(_.invoke(_this.subUIListeners, 'onBeforeCallOperation', context, operation), false);
            };
            this.onAfterRefresh = function (context) {
                _.invoke(_this.subUIListeners, 'onAfterRefresh', context);
            };
            this.onGenericObjectUpdated = function (context, updatedProperties) {
                _.invoke(_this.subUIListeners, 'onGenericObjectUpdated', context, updatedProperties);
            };
            this.onBeforeRender = function (context) {
                _.invoke(_this.subUIListeners, 'onBeforeRender', context);
            };
            this.onListViewReady = function (context) {
                _.invoke(_this.subUIListeners, 'onListViewReady', context);
            };
            this.onPartialRendered = function (context) {
                _.invoke(_this.subUIListeners, 'onPartialRendered', context);
            };
            this.onBeforeRenderSummaryView = function (context) {
                _.invoke(_this.subUIListeners, 'onBeforeRenderSummaryView', context);
            };
            this.onSummaryViewReady = function (context) {
                _.invoke(_this.subUIListeners, 'onSummaryViewReady', context);
            };
        };
        UIListener.prototype.isDetailViewContext = function (context) {
            var contextType = context.type;
            var isDetailViewContext = contextType.startsWith("DetailView");
            return isDetailViewContext;
        };
        UIListener.prototype.isListViewContext = function (context) {
            return !this.isDetailViewContext(context);
        };
        /**
         * Hilfsfunktion: Gibt "false" zurück, falls einer der Werte in der Liste dem Parameter "value" entspricht; ansonsten true
         * @param list
         * @param value
         */
        UIListener.prototype.none = function (list, value) {
            var result = true;
            _.each(list, function (val) {
                if (val === value) {
                    result = false;
                }
            });
            return result;
        };
        UIListener.prototype.noneAsync = function (list, value) {
            var self = this;
            var deferred = $.Deferred();
            $.when.apply($, list).then(function () {
                if (self.isDisposed) {
                    deferred.reject();
                }
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i] === value) {
                        deferred.resolve(false);
                        return;
                    }
                }
                deferred.resolve(true);
            });
            return deferred;
        };
        UIListener.prototype.dispose = function () {
            for (var i = 0; i < this.subUIListeners.length; i++) {
                this.subUIListeners[i].dispose();
            }
            this.subUIListeners = [];
            _super.prototype.dispose.call(this);
        };
        /**
         * Erzeugt eine neue Instanz eines UIListeners anhand einer Komponentenbezeichnung
         * @param component
         * @param params
         */
        UIListener.createInstance = function (component, params) {
            if (!params) {
                params = {};
            }
            var Component = ComponentManager.getUIListener(component || 'DefaultUIListener');
            if (Component == null) {
                Component = ComponentManager.getUIListener('DefaultUIListener');
                console.log('UIListener "' + component + '" wurde nicht gefunden. Fallback "DefaultUIListener" wird verwendet."');
            }
            return new Component(params);
        };
        return UIListener;
    }(Class));
    return UIListener;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlMaXN0ZW5lci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlVJTGlzdGVuZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsK0NBQStDOzs7Ozs7Ozs7Ozs7O0lBTS9DO1FBQXlCLDhCQUFLO1FBSTFCLG9CQUFZLE1BQU87WUFBbkIsWUFDSSxpQkFBTyxTQUdWO1lBTk0sb0JBQWMsR0FBRyxFQUFFLENBQUM7WUFLdkIsS0FBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxDQUFDOztRQUM5QyxDQUFDO1FBRUQsZUFBZTtRQUdSLGlDQUFZLEdBQW5CLFVBQW9CLE9BQU8sSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUUxQixrQ0FBYSxHQUFwQixVQUFxQixPQUFPLElBQUksQ0FBQztRQUFBLENBQUM7UUFFM0IsbUNBQWMsR0FBckIsVUFBc0IsT0FBTyxJQUFJLENBQUM7UUFBQSxDQUFDO1FBRTVCLDhDQUF5QixHQUFoQyxVQUFpQyxPQUFPLElBQUksQ0FBQztRQUFBLENBQUM7UUFFdkMsaUNBQVksR0FBbkIsVUFBb0IsT0FBTyxFQUFFLFNBQVMsSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUVyQywyQ0FBc0IsR0FBN0IsVUFBOEIsT0FBTyxFQUFFLFFBQVEsSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUU5QyxzQ0FBaUIsR0FBeEIsVUFBeUIsT0FBTyxJQUFJLENBQUM7UUFBQSxDQUFDO1FBRS9CLHVDQUFrQixHQUF6QixVQUEwQixPQUFPLElBQUksQ0FBQztRQUFBLENBQUM7UUFFaEMseUNBQW9CLEdBQTNCLFVBQTRCLE9BQU8sRUFBRSxRQUFRLElBQUksQ0FBQztRQUFBLENBQUM7UUFFNUMseUNBQW9CLEdBQTNCLFVBQTRCLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxJQUFJLENBQUM7UUFBQSxDQUFDO1FBRW5ELDBDQUFxQixHQUE1QixVQUE2QixPQUFPLEVBQUUsU0FBUyxJQUFJLENBQUM7UUFBQSxDQUFDO1FBRTlDLG1DQUFjLEdBQXJCLFVBQXNCLE9BQU8sSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUU1QiwrQ0FBMEIsR0FBakMsVUFBa0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLElBQUksQ0FBQztRQUFBLENBQUM7UUFFOUQsOENBQXlCLEdBQWhDLFVBQWlDLE9BQU8sSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUV2Qyx1Q0FBa0IsR0FBekIsVUFBMEIsT0FBTyxJQUFJLENBQUM7UUFBQSxDQUFDO1FBRWhDLG9EQUErQixHQUF0QyxVQUF1QyxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUVuRSxtQ0FBYyxHQUFyQixVQUFzQixPQUFPLElBQUksQ0FBQztRQUFBLENBQUM7UUFFNUIsMkNBQXNCLEdBQTdCLFVBQThCLE9BQU8sRUFBRSxpQkFBaUIsSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUV2RCxvQ0FBZSxHQUF0QixVQUF1QixPQUFPLElBQUksQ0FBQztRQUFBLENBQUM7UUFFN0Isc0NBQWlCLEdBQXhCLFVBQXlCLE9BQU8sSUFBSSxDQUFDO1FBQUEsQ0FBQztRQUN0QyxZQUFZO1FBRUwsZ0NBQVcsR0FBbEI7WUFBQSxpQkFxR0M7WUFwR0csS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRTVCLElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFO29CQUM5QixVQUFVLEdBQUcsUUFBUSxDQUFDO2lCQUN6QjtxQkFDSSxJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2pILElBQUksT0FBTyxRQUFRLENBQUMsVUFBVSxLQUFLLFFBQVEsSUFBSSxPQUFPLFFBQVEsQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO3dCQUNoRixVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQzt3QkFDakMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7cUJBQzVCO2lCQUNKO2dCQUVELElBQUksVUFBVSxFQUFFO29CQUNaLElBQUksaUJBQWlCLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQ3RFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7aUJBQy9DO2FBQ0o7WUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQUMsT0FBTztnQkFDeEIsT0FBTyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDekYsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFDLE9BQU87Z0JBQ3pCLE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsZUFBZSxFQUFFLE9BQU8sQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzFGLENBQUMsQ0FBQztZQUVGLElBQUksQ0FBQyxZQUFZLEdBQUcsVUFBQyxPQUFPLEVBQUUsU0FBUztnQkFDbkMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLGNBQWMsRUFBRSxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDdEUsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLHlCQUF5QixHQUFHLFVBQUMsT0FBTztnQkFDckMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLDJCQUEyQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQztZQUVGLElBQUksQ0FBQyxjQUFjLEdBQUcsVUFBQyxPQUFPO2dCQUMxQixPQUFPLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLGdCQUFnQixFQUFFLE9BQU8sQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzNGLENBQUMsQ0FBQztZQUVGLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxVQUFDLE9BQU8sRUFBRSxRQUFRO2dCQUM1QyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsd0JBQXdCLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQy9FLENBQUMsQ0FBQztZQUVGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxVQUFDLE9BQU87Z0JBQzdCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNoRSxDQUFDLENBQUM7WUFFRixJQUFJLENBQUMsa0JBQWtCLEdBQUcsVUFBQyxPQUFPO2dCQUM5QixDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsb0JBQW9CLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDakUsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFVBQUMsT0FBTyxFQUFFLFFBQVE7Z0JBQzFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLDBCQUEwQixHQUFHLFVBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVO2dCQUM1RCxPQUFPLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLDRCQUE0QixFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEgsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLCtCQUErQixHQUFHLFVBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVO2dCQUNqRSxPQUFPLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLGlDQUFpQyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbEksQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFVBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJO2dCQUNqRCxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsc0JBQXNCLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNwRixDQUFDLENBQUM7WUFFRixJQUFJLENBQUMscUJBQXFCLEdBQUcsVUFBQyxPQUFPLEVBQUUsU0FBUztnQkFDNUMsT0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSx1QkFBdUIsRUFBRSxPQUFPLEVBQUUsU0FBUyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEcsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFDLE9BQU87Z0JBQzFCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM3RCxDQUFDLENBQUM7WUFFRixJQUFJLENBQUMsc0JBQXNCLEdBQUcsVUFBQyxPQUFPLEVBQUUsaUJBQWlCO2dCQUNyRCxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsd0JBQXdCLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDeEYsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFDLE9BQU87Z0JBQzFCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM3RCxDQUFDLENBQUM7WUFFRixJQUFJLENBQUMsZUFBZSxHQUFHLFVBQUMsT0FBTztnQkFDM0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLGlCQUFpQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzlELENBQUMsQ0FBQztZQUVGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxVQUFDLE9BQU87Z0JBQzdCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNoRSxDQUFDLENBQUM7WUFFRixJQUFJLENBQUMseUJBQXlCLEdBQUcsVUFBQyxPQUFPO2dCQUNyQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLEVBQUUsMkJBQTJCLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDeEUsQ0FBQyxDQUFDO1lBRUYsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFVBQUMsT0FBTztnQkFDOUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2pFLENBQUMsQ0FBQztRQUNOLENBQUM7UUFFTSx3Q0FBbUIsR0FBMUIsVUFBMkIsT0FBTztZQUM5QixJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQy9CLElBQUksbUJBQW1CLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUUvRCxPQUFPLG1CQUFtQixDQUFDO1FBQy9CLENBQUM7UUFFTSxzQ0FBaUIsR0FBeEIsVUFBeUIsT0FBTztZQUM1QixPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFFRDs7OztXQUlHO1FBQ0sseUJBQUksR0FBWixVQUFhLElBQUksRUFBRSxLQUFLO1lBQ3BCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQztZQUNsQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFDLEdBQUc7Z0JBQ2IsSUFBSSxHQUFHLEtBQUssS0FBSyxFQUFFO29CQUNmLE1BQU0sR0FBRyxLQUFLLENBQUM7aUJBQ2xCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDO1FBRU8sOEJBQVMsR0FBakIsVUFBa0IsSUFBSSxFQUFFLEtBQUs7WUFDekIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ2pCLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQkFDckI7Z0JBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3ZDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTt3QkFDeEIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDeEIsT0FBTztxQkFDVjtpQkFDSjtnQkFDRCxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLENBQUMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQztRQUVNLDRCQUFPLEdBQWQ7WUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDcEM7WUFFRCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUV6QixpQkFBTSxPQUFPLFdBQUUsQ0FBQztRQUNwQixDQUFDO1FBRUQ7Ozs7V0FJRztRQUNXLHlCQUFjLEdBQTVCLFVBQTZCLFNBQVMsRUFBRSxNQUFNO1lBQzFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ1QsTUFBTSxHQUFHLEVBQUUsQ0FBQzthQUNmO1lBQ0QsSUFBSSxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsYUFBYSxDQUFvQixTQUFTLElBQUksbUJBQW1CLENBQUMsQ0FBQztZQUVwRyxJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQ25CLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsU0FBUyxHQUFHLHVFQUF1RSxDQUFDLENBQUE7YUFDcEg7WUFFRCxPQUFPLElBQUksU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLENBQUM7UUFFTCxpQkFBQztJQUFELENBQUMsQUF4T0QsQ0FBeUIsS0FBSyxHQXdPN0I7SUFFRCxPQUFTLFVBQVUsQ0FBQyJ9