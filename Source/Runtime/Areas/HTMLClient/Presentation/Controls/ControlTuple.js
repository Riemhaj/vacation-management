/// <amd-module name="Controls/ControlTuple" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Controls/ControlTuple", ["require", "exports", "Class"], function (require, exports, Class) {
    "use strict";
    var ControlTuple = /** @class */ (function (_super) {
        __extends(ControlTuple, _super);
        function ControlTuple(control) {
            var _this = _super.call(this) || this;
            _this.control = control.getControl();
            _this.isDisposable = true;
            return _this;
        }
        return ControlTuple;
    }(Class));
    return ControlTuple;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbFR1cGxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiQ29udHJvbFR1cGxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLCtDQUErQzs7Ozs7Ozs7Ozs7OztJQUsvQztRQUEyQixnQ0FBSztRQUk1QixzQkFBWSxPQUFnQjtZQUE1QixZQUNJLGlCQUFPLFNBSVY7WUFGRyxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNwQyxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzs7UUFDN0IsQ0FBQztRQUNMLG1CQUFDO0lBQUQsQ0FBQyxBQVZELENBQTJCLEtBQUssR0FVL0I7SUFFRCxPQUFTLFlBQVksQ0FBQyJ9