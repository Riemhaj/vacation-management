/// <amd-module name="Controls/Control" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Controls/Control", ["require", "exports", "BindingContext", "Management/ComponentManager", "Management/ComponentObserver", "Controls/ControlTuple", "Management/RADApplication", "Management/ResourceManager"], function (require, exports, BindingContext, ComponentManager, ComponentObserver, ControlTuple, RADApplication, ResourceManager) {
    "use strict";
    /*
    * Repräsentiert die Oberklasse aller Controls. Setzt Standard-Properties und bietet Funktionalität
    * zur erleichterten Control-Entwicklung
    */
    var Control = /** @class */ (function (_super) {
        __extends(Control, _super);
        function Control(viewModel, element) {
            var _this = _super.call(this) || this;
            if (viewModel == null || 'undefined' === typeof viewModel.isDisposed) {
                throw new Error('Dem Control wurde kein ControlViewModel übergeben!');
            }
            _this.undisposeProperties.push('context');
            _this.undisposeProperties.push('viewModel');
            _this.isDisposable = true;
            _this.disposeProperties = true;
            _this.widget = _this.observableOrDefault(null).extend({ notify: 'always' });
            _this.context = viewModel;
            _this.timeout = 50;
            _this.keyEvents = [];
            if (_this.type != "Object") {
                ComponentObserver.onControlInit(_this.identifier);
                _this.defineSubscription(_this.widget, function (widget) {
                    ComponentObserver.onControlReady(_this.identifier);
                });
            }
            // TODO: Nach TS-Portierung entfernen
            _this.applyPrototypeToInstance(Control);
            return _this;
        }
        Control.prototype.getControl = function () {
            throw new Error('Die Funktion "getControl" wurde nicht implementiert');
        };
        /*
        * Erzeugt das ControlTuple und das Control
        */
        Control.prototype.getTuple = function () {
            if (this.context.isDisposed === true) {
                return null;
            }
            this.tuple = new ControlTuple(this);
            return this.tuple;
        };
        /*
        * Wendet Bindings auf ein DOM-Element an
        */
        Control.prototype.applyBindings = function (element, bindings) {
            if (element[0] == null) {
                throw 'Bindings können nicht angewendet werden. Element ist null.';
            }
            ko.applyBindingsToNode(element[0], $.extend({ uiElement: this }, bindings));
        };
        /*
        * Registriert ein Template in einem Kontext
        */
        Control.prototype.registerTemplate = function (string, context, callback) {
            return this.registerTemplateAsync(string, context, this.timeout, callback);
        };
        /*
        * Registriert ein Template in einem Kontext (sofort)
        */
        Control.prototype.registerTemplateAsync = function (string, context, delay, callback) {
            return this.async(function () {
                var element = $(string).appendTo(context.empty());
                if (callback) {
                    callback(element);
                }
            }, delay);
        };
        /*
        * Wendet Bindings auf ein Template an
        */
        Control.prototype.applyTemplate = function (string, context, callback) {
            return this.applyTemplateAsync(string, context, this.timeout, callback);
        };
        /*
         * Wendet Bindings auf ein Template an (asynchron)
         */
        Control.prototype.applyTemplateAsync = function (string, context, delay, callback) {
            var _this = this;
            return this.registerTemplateAsync(string, context, delay, function (element) {
                ko.applyBindings({
                    uiElement: _this,
                    viewModel: _this.context,
                    RADApplication: RADApplication,
                    ResourceManager: ResourceManager
                }, element[0]);
                if (callback != null) {
                    callback();
                }
            });
        };
        /*
         * Registriert eine Funktion auf einem Ctrl+key-Press-Event
         */
        Control.prototype.registerKeyEvent = function (key, element, func) {
            var _this = this;
            var _internalFunc = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                if (args[0].preventDefault)
                    args[0].preventDefault();
                func.apply(_this, args);
            };
            var reg = {
                element: element,
                func: _internalFunc
            };
            this.keyEvents.push(reg);
            element.on('keydown', null, key, _internalFunc);
            return {
                dispose: function () {
                    element.off('keydown', null, _internalFunc);
                    var idx = _this.keyEvents.indexOf(reg);
                    if (idx != -1) {
                        _this.keyEvents.splice(idx, 1);
                    }
                }
            };
        };
        /*
        * Definiert einen Callback für ein erzeugtes Kendo-Widget
        */
        Control.prototype.onWidgetCreated = function (callback) {
            var _this = this;
            this.defineSubscription(this.widget, function (widget) {
                if (_this.isDisposed === false && _this.context != null && _this.context.isDisposed === false) {
                    callback(widget);
                }
            });
        };
        Control.prototype.addLabelReference = function (label) {
            return null;
        };
        /*
         * Erzeugt eine neue Instanz eines Controls anhand einer Komponentenbezeichnung
         */
        Control.createInstance = function (component, viewModel, element) {
            var Component = ComponentManager.getControl(component);
            if (Component == null) {
                throw 'Control-Komponente "' + component + '" wurde nicht gefunden.';
            }
            return new Component(viewModel, element);
        };
        /*
        * Disposed das Control
        */
        Control.prototype.dispose = function () {
            for (var i = 0; i < this.keyEvents.length; i++) {
                this.keyEvents[i].element.off('keydown', this.keyEvents[i].func);
            }
            _super.prototype.dispose.call(this);
        };
        return Control;
    }(BindingContext));
    return Control;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29udHJvbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkNvbnRyb2wudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMENBQTBDOzs7Ozs7Ozs7Ozs7O0lBUzFDOzs7TUFHRTtJQUNGO1FBQXNCLDJCQUFjO1FBNkJoQyxpQkFBbUIsU0FBUyxFQUFFLE9BQTRCO1lBQTFELFlBQ0ksaUJBQU8sU0EwQlY7WUF4QkcsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFdBQVcsS0FBSyxPQUFPLFNBQVMsQ0FBQyxVQUFVLEVBQUU7Z0JBQ2xFLE1BQU0sSUFBSSxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN6RTtZQUVELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUUzQyxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQzFFLEtBQUksQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLEtBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBRXBCLElBQUksS0FBSSxDQUFDLElBQUksSUFBSSxRQUFRLEVBQUU7Z0JBQ3ZCLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRWpELEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLFVBQUMsTUFBTTtvQkFDeEMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdEQsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUVELHFDQUFxQztZQUNyQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLENBQUM7O1FBQzNDLENBQUM7UUFFTSw0QkFBVSxHQUFqQjtZQUNJLE1BQU0sSUFBSSxLQUFLLENBQUMscURBQXFELENBQUMsQ0FBQztRQUMzRSxDQUFDO1FBRUQ7O1VBRUU7UUFDSywwQkFBUSxHQUFmO1lBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ2xDLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO1FBRUQ7O1VBRUU7UUFDSywrQkFBYSxHQUFwQixVQUFxQixPQUFPLEVBQUUsUUFBUTtZQUNsQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3BCLE1BQU0sNERBQTRELENBQUM7YUFDdEU7WUFFRCxFQUFFLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUNoRixDQUFDO1FBRUQ7O1VBRUU7UUFDSyxrQ0FBZ0IsR0FBdkIsVUFBd0IsTUFBTSxFQUFFLE9BQU8sRUFBRSxRQUFRO1lBQzdDLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvRSxDQUFDO1FBRUQ7O1VBRUU7UUFDSyx1Q0FBcUIsR0FBNUIsVUFBNkIsTUFBTSxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsUUFBUTtZQUN6RCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBRWQsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFFbEQsSUFBSSxRQUFRLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNyQjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNkLENBQUM7UUFFRDs7VUFFRTtRQUNLLCtCQUFhLEdBQXBCLFVBQXFCLE1BQU0sRUFBRSxPQUFPLEVBQUUsUUFBUTtZQUMxQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUUsQ0FBQztRQUVEOztXQUVHO1FBQ0ksb0NBQWtCLEdBQXpCLFVBQTBCLE1BQU0sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFFBQVE7WUFBMUQsaUJBYUM7WUFaRyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFDLE9BQU87Z0JBQzlELEVBQUUsQ0FBQyxhQUFhLENBQUM7b0JBQ2IsU0FBUyxFQUFFLEtBQUk7b0JBQ2YsU0FBUyxFQUFFLEtBQUksQ0FBQyxPQUFPO29CQUN2QixjQUFjLEVBQUUsY0FBYztvQkFDOUIsZUFBZSxFQUFFLGVBQWU7aUJBQ25DLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWYsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNsQixRQUFRLEVBQUUsQ0FBQztpQkFDZDtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVEOztXQUVHO1FBQ0ksa0NBQWdCLEdBQXZCLFVBQXdCLEdBQUcsRUFBRSxPQUFPLEVBQUUsSUFBSTtZQUExQyxpQkEwQkM7WUF6QkcsSUFBSSxhQUFhLEdBQUc7Z0JBQUMsY0FBTztxQkFBUCxVQUFPLEVBQVAscUJBQU8sRUFBUCxJQUFPO29CQUFQLHlCQUFPOztnQkFDeEIsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYztvQkFDdEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUU3QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMzQixDQUFDLENBQUM7WUFFRixJQUFJLEdBQUcsR0FBRztnQkFDTixPQUFPLEVBQUUsT0FBTztnQkFDaEIsSUFBSSxFQUFFLGFBQWE7YUFDdEIsQ0FBQztZQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXpCLE9BQU8sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFFaEQsT0FBTztnQkFDSCxPQUFPLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO29CQUM1QyxJQUFJLEdBQUcsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdEMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUU7d0JBQ1gsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUNqQztnQkFDTCxDQUFDO2FBQ0osQ0FBQztRQUNOLENBQUM7UUFFRDs7VUFFRTtRQUNLLGlDQUFlLEdBQXRCLFVBQXVCLFFBQVE7WUFBL0IsaUJBTUM7WUFMRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFDLE1BQU07Z0JBQ3hDLElBQUksS0FBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksS0FBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUN4RixRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3BCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRU0sbUNBQWlCLEdBQXhCLFVBQXlCLEtBQTBCO1lBQy9DLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRDs7V0FFRztRQUNXLHNCQUFjLEdBQTVCLFVBQTZCLFNBQVMsRUFBRSxTQUFTLEVBQUUsT0FBTztZQUN0RCxJQUFJLFNBQVMsR0FBUSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFNUQsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUNuQixNQUFNLHNCQUFzQixHQUFHLFNBQVMsR0FBRyx5QkFBeUIsQ0FBQzthQUN4RTtZQUVELE9BQU8sSUFBSSxTQUFTLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzdDLENBQUM7UUFFRDs7VUFFRTtRQUNLLHlCQUFPLEdBQWQ7WUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNwRTtZQUVELGlCQUFNLE9BQU8sV0FBRSxDQUFDO1FBQ3BCLENBQUM7UUFDTCxjQUFDO0lBQUQsQ0FBQyxBQXhNRCxDQUFzQixjQUFjLEdBd01uQztJQUVELE9BQVMsT0FBTyxDQUFDIn0=