﻿function Module(display, value, installed, tooltip, version, creationTime, dependencies, licenseState) {
    var self = this;

    self.Display = display;
    self.Value = value;
    self.Installed = ko.observable(installed);
    self.ToolTip = tooltip;
    self.AssemblyVersion = version;
    self.AssemblyCreationTime = creationTime;
    self.Dependencies = dependencies;
    self.LicenseState = licenseState;
    self.GeneratedWith = ko.observableArray();
};


function LogEntry(logname, countevents, hasnewevents, events) {
    var self = this;

    self.LogName = logname;
    self.CountEvents = ko.observable(countevents);
    self.HasNewEvents = ko.observable(hasnewevents);
    self.Events = ko.observableArray(events);
}

function Logger(name, level) {
    var self = this;

    self.Name = name;
    self.Level = ko.observable(level);


}

function Backup() {
    var self = this;

    self.timestamp = "";
};

function Message(callback) {
    var self = this;
    self.callback = callback;
    self.uuid = Math.random() * 100000;

    var polling = function() {
        $.ajax({
            async: true,
            type: "POST",
            url: baseSiteURL+"CallEnvelope/GetMessages",
            data: self,
        sender: "EnvelopePolling",
        success: function(data, textStatus, XMLHttpRequest) {
            if (data.hasOwnProperty('Messages') && data.Messages!=null) {
                var msg = data.Messages;

                if ($.isFunction(callback) ) {
                    for (var i = 0; i < msg.length; i++)
                        callback(msg[i]);
                }

                self.index += msg.length;
            }
            var requestPending = data.hasOwnProperty('RequestPending') && data.RequestPending;
            if (requestPending) {
                setTimeout(polling, 1000);
            }
        }
    });
    };

    self.enableServerMessageDispatcher = function () {
        //Während der Serialisierung beim Ajax-Request wird diese Operation erneut aufgerufen. Das darf nicht passieren.
        self.enableServerMessageDispatcher = function () { };
        self.index = 0;
        self.parameter = null;
        setTimeout(polling, 3000);
    };

    self.getUUID = function () {
        return self.uuid;
    }


}

ko.observableArray.fn.pushAll = function (valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};