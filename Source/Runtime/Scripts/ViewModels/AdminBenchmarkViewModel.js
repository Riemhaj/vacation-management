﻿function AdminBenchmarkViewModel() {
    
    //Vars
    var self = this;
    self.ModalVM = {};
    self.ConsoleVM = {};
    self.blockView = ko.observable(false);

    self.modalTitle = ko.observable("");
    self.modalText = ko.observable("");
    self.modalCloseBtn = ko.observable(false);
    self.modalBar = ko.observable("progress-bar-info");

    self.BenchmarkData = ko.observable({});

    //Methods

    self.init = function (data) {
    }

    self.performBenchmark = function (obj, ev) {

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Performing Benchmark", "This can take several minutes...");
            self.server.post(baseSiteURL + "Benchmark/PerformBenchmark", {}, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);
                    self.BenchmarkData(data.Data);
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            });
        }
    };

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }

    self.server = {};
    self.server.post = function (url, data, success) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
            },
            dataType: 'json'
        });
        msg.enableServerMessageDispatcher();
    };
    self.server.get = function (url, success) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            dataType: 'json'
        });
    };
};