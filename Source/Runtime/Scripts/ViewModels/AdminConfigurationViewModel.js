﻿function AdminConfigurationViewModel() {
    var self = this;

    self.ModalVM = {};
    self.currentSystemFingerPrint = ko.observable("");
    self.generatedSystemFingerprint = ko.observable("");
    self.usePessimisticLocking = ko.observable();
    self.log4NetConfigPath = ko.observable();
    self.applicationPath = ko.observable();
    self.nHibernateConfigPath = ko.observable();
    self.defaultTemplate = ko.observable();
    self.licenseReadProperties = ko.observableArray([]);
    self.module = ko.observableArray([]);
    self.isRemotingEnabled = ko.observable(false);
        
    self.init = function (data) {
        var newConfig = convertPropertiesToLower(data);
        self.currentSystemFingerPrint(newConfig.currentsystemfingerprint);
        self.generatedSystemFingerprint(newConfig.generatedsystemfingerprint);
        self.usePessimisticLocking(newConfig.usepessimisticlocking);
        self.log4NetConfigPath(newConfig.log4netconfigpath);
        self.applicationPath(newConfig.applicationpath);
        self.nHibernateConfigPath(newConfig.nhibernateconfigpath);
        self.defaultTemplate(newConfig.defaulttemplate);
        self.isRemotingEnabled(newConfig.isremotingenabled);

        self.licenseReadProperties.removeAll();
        for (var key in newConfig.licensereadproperties)
            self.licenseReadProperties.push({ name: key, value: newConfig.licensereadproperties[key] });
        
        self.module.removeAll();
        var RadLicenseState = "Unlicensed";
        if (newConfig.hasOwnProperty("radlicense")) {
            RadLicenseState = newConfig.radlicense;
        } else if (newConfig.hasOwnProperty("radlicensestate")) {
            RadLicenseState = newConfig.radlicensestate;
        }

        self.module.push({ Name: 'RAD Framework', LicenseState: RadLicenseState });
        for (var i in newConfig.module)
            self.module.push({ Name: newConfig.module[i].Value, LicenseState: newConfig.module[i].LicenseState });
    }

    self.reloadLicense = function () {


        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Reload License", "Please wait...");
            self.server.post(baseSiteURL + "Admin/ReloadLicense", {}, function (data) {

                if (responseSuccess(data)) {
                    self.ModalVM.successModal("update informations...");
                    if (data.hasOwnProperty("configurationData")) {
                        self.init(data.configurationData);
                    }
                    self.ModalVM.hideModal();
                }
                else
                    self.ModalVM.errorModal("Unknown Error");

            }, function (data) {
                self.ModalVM.errorModal(data);
            });
        }

    }

    self.toggleFingerPrint = function (data,ev) {
        $(ev.currentTarget).next('.panel-body').slideToggle("fast");
    };

    var convertPropertiesToLower = function (obj) {
        var key, keys = Object.keys(obj);
        var n = keys.length;
        var newobj = {}
        while (n--) {
            key = keys[n];
            newobj[key.toLowerCase()] = obj[key];
        }
        return newobj;
    }

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }
    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
                  
            },
            dataType: 'json'
        });
    };
}