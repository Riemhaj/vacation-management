﻿function ModalWindowViewModel() {
    var self = this;

    self.isFree = ko.observable(true);
    self.modalTitle = ko.observable("");
    self.modalText = ko.observable("");
    self.modalNotificationText = ko.observable("");
    self.modalCloseBtn = ko.observable(false);
    self.modalSecondaryBtnVisible = ko.observable(false);
    self.withProgressBar = ko.observable(true);
    self.coloredButtons = ko.observable(false);
    self.modalSecondaryBtnText = ko.observable("");
    self.modalBar = ko.observable("progress-bar-info");
    self.messages = ko.observableArray(["", "", "", "", ""]);
    self.doAfterHide = null;
    self.doSecondaryAfterHide = null;
    self.BtnText = ko.observable();
    self.options = ko.observableArray([]);

    self.init = function () {

        $('#adminModal').on('hidden.bs.modal', function () {
            self.isFree(true);
            if ($.isFunction(self.doAfterHide)) {
                self.doAfterHide();
                self.doAfterHide = null;
                self.doSecondaryAfterHide = null;
            }
        })
    }

    self.showModal = function (title, text) {
        self.isFree(false);
        self.options.removeAll();
        self.messages.removeAll();
        self.messages(["", "", "", "", ""]);
        self.BtnText("Ok");
        self.modalTitle(title);
        self.modalNotificationText("");
        self.modalSecondaryBtnVisible(false);
        self.coloredButtons(false);
        self.withProgressBar(true);
        self.modalSecondaryBtnText("");
        self.doSecondaryAfterHide = null;
        self.modalText(text);
        self.modalBar("progress-bar-info");
        self.modalCloseBtn(false);
        $('#adminModal').modal({ keyboard: false,
            backdrop: 'static'
        });

    }

    self.clickSecondaryBtn = function(el,ev){
        self.doAfterHide = self.doSecondaryAfterHide;
    };

    self.pushMessage = function (msg) {
        if (!msg)
            return;
        if (self.messages().length >= 5)
            self.messages.shift();
        self.messages.push(msg);

    };

    self.setActionAfterHide = function (fkt) {
        if (!$.isFunction(fkt))
            return;
        self.doAfterHide = fkt;
    };

    self.setSecondaryActionAfterHide = function (fkt) {
        if (!$.isFunction(fkt))
            return;
        self.doSecondaryAfterHide = fkt;
    };

    self.successModal = function (text) {
        self.modalBar("progress-bar-success");
        self.modalText(text);
        self.modalCloseBtn(true);
    };

    self.errorModal = function (text) {
        self.modalBar("progress-bar-danger");
        self.modalText(text);
        self.modalCloseBtn(true);
    };

    self.completeModal = function (text) {
        if (!self.modalCloseBtn()) {
            self.modalBar("progress-bar-warning");
            self.modalText(text);
            self.modalCloseBtn(true);
        }
    };

    self.setButtonText = function (text) {
        if (text != "")
            self.BtnText(text);
    };

    self.setNotificationText = function (text) {
        if (text != "")
            self.modalNotificationText(text);
    };

    self.hideModal = function () {
        $('#adminModal').modal('hide');
    };

    self.showErrorAndLogout = function () {
        self.doAfterHide = null;

        self.showModal("Your session is expired", "You have to login again.");
        self.BtnText("go to login");
        self.modalCloseBtn(true);
        self.doAfterHide = function () {
            self.server.post(baseSiteURL + "Admin/Logout", {}, function (data) {
                if (data.Success) {
                    document.location.href = baseSiteURL+"Admin/Login";
                }
            });
        };
        self.withProgressBar(false);

    };

    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            dataType: 'json'
        });
    };
};