﻿function LoginViewModel() {
    var self = this;
    //Vars
    self.password = ko.observable("");
    self.passwordConfirm = ko.observable("");
    self.errorMsg = ko.observable("");
    self.successMsg = ko.observable("");

    //Methods
    self.setNewPW = function (data, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.password() != self.passwordConfirm())
            return;

        $('#errorMsg').hide();
        $('#successMsg').hide();

        $('input[type=password]').attr("disabled", "disabled");

        var newpw = self.password();
        newpw = $.base64.encode(newpw);
        self.server.post(baseSiteURL + "Admin/SetNewPassword", { password: newpw }, function (data) {
            if (data.Success) {
                self.successMsg(data.Message);
                $('#successMsg').slideDown('fast');
                document.location.href = baseSiteURL + "Admin/";
            }
            else {
                self.errorMsg(data.Message);
                $('#errorMsg').slideDown('slow');
                $('input[type=password]').removeAttr("disabled");
            }
        });
    };

    self.login = function (data, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        $('#errorMsg').hide();
        $('#successMsg').hide();

        $('#login_input').attr("disabled","disabled");

        var newpw = self.password();
        newpw = $.base64.encode(newpw);
        self.server.post(baseSiteURL + "Admin/DoLogin", { password: newpw }, function (data) {
            if (data.Success) {
                self.successMsg(data.Message);
                $('#successMsg').slideDown('fast');
                document.location.href = baseSiteURL + "Admin/";
            }
            else {
                self.errorMsg(data.Message);
                $('#errorMsg').slideDown('fast');
                $('#login_input').removeAttr("disabled");
                $('#login_input').focus();
                    $('#login_input').select();
                
            }
        });
    };


    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            dataType: 'json'
        });
    };
}