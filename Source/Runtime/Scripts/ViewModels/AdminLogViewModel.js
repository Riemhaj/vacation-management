﻿function AdminLogViewModel() {
    var self = this;

    self.ModalVM = {};
    self.logs = ko.observableArray([]);
    self.logger = ko.observableArray([]);
    self.logDetails = ko.observable();
    self.availableLevels = ko.observableArray([]);
    self.availableLevels().push("OFF");
    self.availableLevels().push("FATAL");
    self.availableLevels().push("ERROR");
    self.availableLevels().push("WARN");
    self.availableLevels().push("INFO");
    self.availableLevels().push("DEBUG");
    self.availableLevels().push("ALL");

    //Methods
    self.init = function (logs,logger) {
        if (logs) {
            for (var i in logs) {
                self.logs().push(new LogEntry(logs[i].LogName, logs[i].CountEvents, false, logs[i].Events));
            }
        }

        if (logger) {
            for (var i in logger) {
                self.logger().push(new Logger(logger[i].Name, logger[i].Level));
            }
        }
        

    };

self.showLogDetails = function (data, ev) {
        self.logDetails(data);
        $('#logOverview').fadeOut(100, function () {
            $('#logDetails').fadeIn(100);
        });
};

    self.changeLoggerLevel = function (data, ev) {

        self.server.post(baseSiteURL + "Log/ChangeLoggerLevel",
            { loggerName: data.Name,newLevel: data.Level() },
            function (responseData) {
                if (responseSuccess(responseData)) {

                }
                else {
                    data.Level(responseData.lvl);
                }
            });
    }

    self.downloadLogZip = function (data, ev) {
        document.location.href = baseSiteURL + "Log/DownloadLogArchive";
    };

    self.loadNextEvents = function (data, ev) {
        self.server.post(baseSiteURL + "Log/GetNextLogs",
            { filename: data.LogName, lastIndex: data.Events().length, count: 25, lastTimestamp: data.Events()[data.Events().length - 1].Timestamp },
            function (responseData) {
                if (responseSuccess(responseData)) {
                    if (responseData.Data.HasNoMoreEvents) {
                        $(ev.currentTarget).fadeOut('fast');
                    }
                    data.Events.pushAll(responseData.Data.Events);
                }
                else {
                    $(ev.currentTarget).fadeOut('fast');
                }
            });
    }

    self.showOverview = function (data, ev) {
        $('#logDetails').fadeOut(100, function () {
            $('#logOverview').fadeIn(100, function () {
                self.logDetails("");
            });
        });
    }

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }

    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
            },
            dataType: 'json'
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success, data) {
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            cache: false,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            dataType: 'json'
        });
    };
    

};