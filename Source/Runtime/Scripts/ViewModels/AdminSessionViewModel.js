﻿function AdminSessionViewModel() {
    var self = this;

    //Private Member Start
    var getClientInfoesEndpoint = "";
    var forceLogoutClientInfoEndpoint = "";
    var handleRefresh = function (data) {
        if (data != null && data != undefined && $.isArray(data)) {
            for (var idx in data) {
                var element = data[idx];
                self.Sessions.push(element);
            }
        }
    };

    var handleFreeAllLocks = function (id) {
        $.ajax({
            type: "GET",
            url: forceLogoutClientInfoEndpoint + "?id=" + id,
            cache: false,
            success: function (data) {
                if (self.ModalVM.isFree()) {
                    self.ModalVM.showModal("Success", "User has been successfully logged off.");
                    self.ModalVM.withProgressBar(false);
                    self.ModalVM.coloredButtons(true);
                    self.ModalVM.modalCloseBtn(true);
                    self.ModalVM.setActionAfterHide(function () {
                        self.refresh();
                    });
                }
            },
            error: function (xhr, stat, errorThrown) {
                self.ModalVM.errorModal("Error" + stat + errorThrown);
            },
            dataType: 'json'
        });
    };
    //Private Member Ende

    self.ModalVM = {};
    self.Sessions = ko.observableArray();

    self.freeAllLocks = function (subject) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Force Log off', "Caution: The user will be logged off.<br />All data in pending transactions will be lost. Make sure, that you have informed the user!");
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                handleFreeAllLocks(subject.ID);
            });
        }
    };

    self.refresh = function () {
        self.Sessions.removeAll();

        $.ajax({
            type: "GET",
            url: getClientInfoesEndpoint,
            cache: false,
            success: function (data) {
                handleRefresh(data);
            },
            error: function(xhr, stat, errorThrown) {
                self.ModalVM.errorModal("Error" + stat + errorThrown);
            },
            dataType: 'json'
        });

    };

    self.init = function (data) {
        getClientInfoesEndpoint = baseSiteURL + "Admin/GetClientInfos";
        forceLogoutClientInfoEndpoint = baseSiteURL + "Admin/ForceLogoutClientInfo";
    };

};