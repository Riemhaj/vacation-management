﻿function AdminLockViewModel() {
    var self = this;

    var getAllLocksEndpoint = "";
    var releaseLockEndpoint = "";
    var releaseAllLocksEndpoint = "";
    var handleRefresh = function(data) {
        if (data != null && data != undefined && $.isArray(data)) {
            for (var idx in data) {
                var element = data[idx];
                self.Locks.push(element);
            }
        }
    };

    var handleReleaseLock = function(pdoid) {
        $.ajax({
            type: "GET",
            url: releaseLockEndpoint + "?pdoid="+pdoid,
            cache: false,
            success: function (data) {
                if (data != null && data.Success != undefined) {
                    if (self.ModalVM.isFree()) {
                        self.ModalVM.showModal("Success", "Locks has been released.");
                        self.ModalVM.withProgressBar(false);
                        self.ModalVM.coloredButtons(true);
                        self.ModalVM.modalCloseBtn(true);
                        self.ModalVM.setActionAfterHide(function () {
                            self.refresh();
                        });
                    }
                }
            },
            error: function (xhr, stat, errorThrown) {
                self.ModalVM.errorModal("Error" + stat + errorThrown);
            },
            dataType: 'json'
        });
    }

    self.ModalVM = {};
    self.Locks = ko.observableArray();

    self.refresh = function() {
        self.Locks.removeAll();
        $.ajax({
            type: "GET",
            url: getAllLocksEndpoint,
            cache: false,
            success: function (data) {
                handleRefresh(data);
            },
            error: function (xhr, stat, errorThrown) {
                self.ModalVM.errorModal("Error" + stat + errorThrown);
            },
            dataType: 'json'
        });
    };
        
    self.releaseLock = function (subject) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Force Log off', "Caution: The lock will be released.<br /> This can lead to data losses. Make sure, that you have informed the user!");
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                handleReleaseLock(subject.PDOID);
            });
        }
    };

    self.init = function(data) {
        getAllLocksEndpoint = baseSiteURL + "Admin/GetAllLocks";
        releaseLockEndpoint = baseSiteURL + "Admin/ReleaseLock";
        releaseAllLocksEndpoint = baseSiteURL + "Admin/ReleaseAllLocks";
    };
};