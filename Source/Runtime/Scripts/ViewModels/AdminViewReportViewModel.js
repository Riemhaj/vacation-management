﻿function AdminViewReportViewModel() {
    var self = this;

    self.ModalVM = {};
    self.viewReportOption = ko.observable("customized");


    self.generateViewReport = function (obj,ev) {
        
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Generating Report", "Waiting for response...");
            self.server.post(baseSiteURL + "ViewReport/GenerateViewReport", { reportTypeOption: self.viewReportOption() }, function (data) {
                if (responseSuccess(data)) {

                    var msg = data.Message + '<br />Click on the link below to download the Report.<br /><br />';
                    msg += '<a href="'+baseSiteURL+'ViewReport/DownloadViewReport?fileName=' + data.FileName + '">Download Report</a>';
                    self.ModalVM.successModal(msg);

                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            });
        }
    }

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }
    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
            },
            dataType: 'json'
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            dataType: 'json'
        });
    };

}