﻿function AdminInstallationViewModel() {
    //Vars
    var self = this;
    self.ModalVM = {};
    self.ConsoleVM = {};
    self.blockView = ko.observable(false);
    self.isSchemaDropable = ko.observable(true);
    self.isSchemaUpdateable = ko.observable(true);
    self.isSchemaValid = ko.observable(false);
    self.isFrameworkGeneratable = ko.observable(true);
    self.RADVersion = ko.observable("");
    self.RADCreationTime = ko.observable("");
    self.emptySystem = ko.observable();
    self.Modules = ko.observableArray([]);
    self.ModulesGeneratedWithRad = ko.observableArray([]);
    self.checkedForInstallation = ko.observableArray([]);
    self.toggleAllUninstalledModulesCheckbox = ko.observable(true);

    self.modalTitle = ko.observable("");
    self.modalText = ko.observable("");
    self.modalCloseBtn = ko.observable(false);
    self.modalBar = ko.observable("progress-bar-info");

    self.uninstalledModules = ko.computed(function () {
        var _uninstalledModules = [];
        for (var i in self.Modules())
            if (!self.Modules()[i].Installed())
                _uninstalledModules.push(self.Modules()[i]);
        return _uninstalledModules;
    }, this);

    //Methods

    self.init = function (data) {
        self.emptySystem(data.isEmptySystem);
        self.isSchemaValid(data.isSchemaValid);
        self.RADVersion(data.RADVersion);
        self.RADCreationTime(data.RADCreationTime);
        self.isFrameworkGeneratable(!data.disableFrameworkGen);
        var _modulesGeneratedWith = [];

        for (var i in data.modules) {
            if (data.modules[i].LicenseState == 'Valid')
                self.checkedForInstallation.push(data.modules[i].Value);

            if (data.modules[i].Dependencies.length > 0 && data.modules[i].Dependencies.indexOf("RAD") > -1) {
                data.modules[i].Dependencies.splice(data.modules[i].Dependencies.indexOf("RAD"), 1);
            }
            if (data.modules[i].GeneratingWith.length > 0) {
                if (data.modules[i].GeneratingWith == "RAD")
                    self.ModulesGeneratedWithRad.push(new Module(data.modules[i].Display, data.modules[i].Value, data.modules[i].Installed, data.modules[i].ToolTip, data.modules[i].AssemblyVersion, data.modules[i].AssemblyCreationTime, data.modules[i].Dependencies, data.modules[i].LicenseState));
                else
                    _modulesGeneratedWith.push(data.modules[i]);
                continue;
            }

            self.Modules.push(new Module(data.modules[i].Display, data.modules[i].Value, data.modules[i].Installed, data.modules[i].ToolTip, data.modules[i].AssemblyVersion, data.modules[i].AssemblyCreationTime, data.modules[i].Dependencies, data.modules[i].LicenseState));
        }

        for (var i in self.Modules()) {
            for (var x in _modulesGeneratedWith) {
                if (self.Modules()[i].Value == _modulesGeneratedWith[x].GeneratingWith) {
                    self.Modules()[i].GeneratedWith.push(new Module(_modulesGeneratedWith[x].Display, _modulesGeneratedWith[x].Value, _modulesGeneratedWith[x].Installed, _modulesGeneratedWith[x].ToolTip, _modulesGeneratedWith[x].AssemblyVersion, _modulesGeneratedWith[x].AssemblyCreationTime, _modulesGeneratedWith[x].Dependencies, _modulesGeneratedWith[x].LicenseState));
                }
            }
        }

    }

    self.GetModulesDisplayName = function (alias) {
        for (var i in self.Modules()) {
            if (self.Modules()[i].Value == alias)
                return self.Modules()[i].Display;
        }
    };

    self.toggleAllUninstalledModules = function (data, ev) {
        var tag = ev.originalEvent.currentTarget.nodeName;
        var isCheckboxChecked = self.toggleAllUninstalledModulesCheckbox();
        var removeCheck = true;

        if ((tag == "INPUT" && isCheckboxChecked) || (tag == "LABEL" && !isCheckboxChecked)) {
            removeCheck = false;
        }

        if (removeCheck) {
            $('.selectAndUnselectModules').prop('checked', false);

            self.toggleAllUninstalledModulesCheckbox(false);

            self.checkedForInstallation.removeAll();
        }
        else {
            $('.selectAndUnselectModules').prop('checked', true);

            self.toggleAllUninstalledModulesCheckbox(true);

            for (var i in self.Modules()) {
                if (self.Modules()[i].LicenseState == 'Valid')
                    self.checkedForInstallation.push(self.Modules()[i].Value);
            }
        }
        return true;
    }

    self.unselectAllUninstalledModules = function (cb, obj) {
        self.toggleAllUninstalledModulesCheckbox(false);
        return true;
    }

    self.getInstallationInfo = function () {

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Loading information", "Waiting for response...");
            self.server.get(baseSiteURL + "Installation/GetInstallationInfo", function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);

                    self.emptySystem(data.isEmptySystem);
                    self.isFrameworkGeneratable(!data.disableFrameworkGen);

                    self.Modules.removeAll();
                    self.ModulesGeneratedWithRad.removeAll();
                    var _modulesGeneratedWith = [];

                    for (var i in data.Modules) {
                        if (data.Modules[i].LicenseState == 'Valid')
                            self.checkedForInstallation.push(data.Modules[i].Value);

                        if (data.Modules[i].Dependencies.length > 0 && data.Modules[i].Dependencies.indexOf("RAD") > -1) {
                            data.Modules[i].Dependencies.splice(data.Modules[i].Dependencies.indexOf("RAD"), 1);
                        }
                        if (data.Modules[i].GeneratingWith.length > 0) {
                            if (data.Modules[i].GeneratingWith == "RAD")
                                self.ModulesGeneratedWithRad.push(new Module(data.Modules[i].Display, data.Modules[i].Value, data.Modules[i].Installed, data.Modules[i].ToolTip, data.Modules[i].AssemblyVersion, data.Modules[i].AssemblyCreationTime, data.Modules[i].Dependencies, data.Modules[i].LicenseState));
                            else
                                _modulesGeneratedWith.push(data.Modules[i]);
                            continue;
                        }

                        self.Modules.push(new Module(data.Modules[i].Display, data.Modules[i].Value, data.Modules[i].Installed, data.Modules[i].ToolTip, data.Modules[i].AssemblyVersion, data.Modules[i].AssemblyCreationTime, data.Modules[i].Dependencies, data.Modules[i].LicenseState));
                    }

                    for (var i in self.Modules()) {
                        for (var x in _modulesGeneratedWith) {
                            if (self.Modules()[i].Value == _modulesGeneratedWith[x].GeneratingWith) {
                                self.Modules()[i].GeneratedWith.push(new Module(_modulesGeneratedWith[x].Display, _modulesGeneratedWith[x].Value, _modulesGeneratedWith[x].Installed, _modulesGeneratedWith[x].ToolTip, _modulesGeneratedWith[x].AssemblyVersion, _modulesGeneratedWith[x].AssemblyCreationTime, _modulesGeneratedWith[x].Dependencies, _modulesGeneratedWith[x].LicenseState));
                            }
                        }
                    }
                    self.ModalVM.hideModal();
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            });
        }
    };

    self.viewSchema = function (obj, ev) {
        document.location.href = baseSiteURL + "Installation/ViewSchemaScript";
    }

    self.dropSchema = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Drop Schema", "Waiting for response...");
            self.server.post(baseSiteURL + "Installation/DropSchema", {}, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);
                    self.emptySystem(true);
                    self.isSchemaValid(false);
                    self.isFrameworkGeneratable(false);
                    for (var i in self.Modules()) {
                        self.Modules()[i].Installed(false);
                    }
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            });
        }

    }

    self.updateSchema = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Update Schema", "Waiting for response...");
            self.server.post(baseSiteURL + "Installation/UpdateSchema", {}, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);
                    self.ModalVM.setActionAfterHide(function () {
                        self.getInstallationInfo();
                    });
                    if (self.emptySystem())
                        self.isFrameworkGeneratable(true);
                    self.emptySystem(false);
                    self.isSchemaValid(true);
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            });
        }
    }

    self.integrityCheck = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Integrity-Check", "Waiting for response...");

            var para = {};
            if (obj == "WithCorrection")
                $.extend(para, { WithCorrection: true });


            self.server.post(baseSiteURL + "Installation/IntegrityCheck", para, function (data) {
                if (responseSuccess(data)) {
                    var div = $('<div></div>').get(0);

                    ko.renderTemplate('IntegrityCheckTmpl', data.Message, {
                        afterRender: function (nodes) {
                            var test = nodes;
                        }
                    },
                    div);

                    self.ModalVM.successModal($(div).html());



                    if (data.showSecondaryBtn == true) {

                        self.ModalVM.modalSecondaryBtnVisible(true);
                        self.ModalVM.modalSecondaryBtnText("Korrektur starten");

                        self.ModalVM.setSecondaryActionAfterHide(function () {
                            self.integrityCheck("WithCorrection", ev);
                        });
                    }
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            });
        }
    };

    self.frameworkGen = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;


        if (self.ModalVM.isFree()) {


            self.ModalVM.showModal("Generating RAD Framework", "Waiting for response...<br />This could take a few minutes.");

            self.server.post(baseSiteURL + "Installation/frameworkGen", {}, function (data) {
                if (data.hasOwnProperty("Success"))
                    if (responseSuccess(data)) {
                        self.ModalVM.setActionAfterHide(function () {
                            self.getInstallationInfo();
                        });
                        self.isFrameworkGeneratable(false);
                        blockLink(ev.currentTarget, true);
                        self.ModalVM.successModal(data.Message);
                    }
                    else if (data.hasOwnProperty("Message")) {
                        self.ModalVM.modalSecondaryBtnVisible(true);
                        self.ModalVM.modalSecondaryBtnText("Download log-files");

                        self.ModalVM.setSecondaryActionAfterHide(function () {
                            window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                        });
                        self.ModalVM.errorModal(data.Message);
                    }
                    else
                        self.ModalVM.errorModal("Unknown Error");
            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            });
        }
    }

    self.domainGen = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;
        var bar = $(ev.currentTarget).prev('.loadingBarSmall');

        $(bar).addClass('active progress-striped');
        $(bar).find('.progress-bar').removeClass('progress-bar-info');

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Generating Module", "Waiting for response...");

            self.server.post(baseSiteURL + "Installation/DomainGen", { modulName: obj.Value }, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);

                    self.ModalVM.setActionAfterHide(function () {
                        self.getInstallationInfo();
                    });
                    blockLink(ev.currentTarget, true);
                    obj.Installed(true);
                    $(bar).removeClass('progress-striped');
                    $(bar).find('.progress-bar').addClass('progress-bar-success');
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                    //var title = data.Message.replace(/<\/?[^>]+(>|$)/g, "");
                    //$(bar).attr("title", title);
                    $(bar).removeClass('progress-striped');
                    //$(bar).find('.progress-bar').addClass('progress-bar-danger');
                    $(bar).find('.progress-bar').addClass('progress-bar-info');
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            });
        }
    };

    self.startInstallation = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;
        $(ev.currentTarget).addClass('disabled');

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Starting Installation", "Waiting for response...");

            var checkedModules = self.checkedForInstallation();

            self.server.post(baseSiteURL + "Installation/StartInstallation", { modules: self.checkedForInstallation() }, function (data) {
                if (responseSuccess(data)) {

                    self.ModalVM.setButtonText("Startup RAD Application");
                    self.ModalVM.setActionAfterHide(function () {

                        if (self.ModalVM.isFree()) {
                            self.ModalVM.showModal("Starting Application", "Waiting for response...");
                            self.server.post(baseSiteURL + "Installation/StartApplication", {}, function (data) {
                                self.ModalVM.hideModal();
                                window.open(baseSiteURL, "_blank");
                            }, function (data) {
                                self.ModalVM.errorModal(data);
                                $(ev.currentTarget).removeClass('disabled');
                            });
                        }
                    });
                    self.ModalVM.successModal(data.Message);
                    self.emptySystem(false);
                    self.isFrameworkGeneratable(false);
                    self.isSchemaValid(true);
                    for (var i in self.Modules()) {
                        var obj = self.Modules()[i];
                        var match = ko.utils.arrayFirst(self.checkedForInstallation(), function (item) {
                            return (obj.Value === item);
                        });
                        if (match != null)
                            obj.Installed(true);
                    }
                    $(ev.currentTarget).removeClass('disabled');

                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                    $(ev.currentTarget).removeClass('disabled');
                }
                else {
                    self.ModalVM.errorModal("Unknown Error");
                    $(ev.currentTarget).removeClass('disabled');
                }
                self.checkedForInstallation.removeAll();

            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            }, function () {
                self.ModalVM.completeModal("Installation done.");
                self.ConsoleVM.push("Installation done.");
            });

        }
    };

    self.checkDependencies = function (obj, ev) {
        if (obj.Dependencies.length > 0)
            return false;
        else
            return true;

    }

    self.startApplication = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Starting Application", "Waiting for response...");
            self.server.post(baseSiteURL + "Installation/StartApplication", {}, function (data) {
                self.ModalVM.hideModal();
                window.open(baseSiteURL, "_blank");
            });
        }
    }

    self.stopApplication = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Shutdown Application", "Waiting for response...");

            self.server.post(baseSiteURL + "Installation/StopApplication", null, function (data) {
                if (responseSuccess(data)) {

                    self.ModalVM.successModal(data.Message);
                    $(ev.currentTarget).removeClass('disabled');

                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            });

        }
    }

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }



    var blockLink = function (el, lineThrough) {
        if (el) {
            if (lineThrough)
                $(el).addClass('disabled lineThrough');
            else
                $(el).addClass('disabled');
        }
    }

    var releaseLink = function (el) {
        if (el)
            $(el).removeClass('disabled lineThrough');
    }

    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success, error,complete) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
                else if ($.isFunction(error)) {
                    error("An error occurred: " + status + " Error: " + thrownError);
                }
            },
            complete: function () {
                if ($.isFunction(complete)) {
                    complete();
                }
            },
            dataType: 'json',
            traditional: true
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            error: function (xhr, status, thrownError) {
                if ($.isFunction(error)) {
                    error("An error occurred: " + status + " Error: " + error);
                }
            },
            dataType: 'json'
        });
    };

};
