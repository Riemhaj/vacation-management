﻿function LicenseAgreementsViewModel() {
    //Vars
    var self = this;
    self.Notes = ko.observableArray([]);
    self.Licenses = ko.observableArray([]);
    self.isFullScreen = ko.observable();

    self.init = function (data,isfullscreen) {
        self.isFullScreen(isfullscreen);
        for (var i in data.Notes)
            self.Notes.push(data.Notes[i]);
        for (var i in data.Licenses)
            self.Licenses.push({ Libary: data.Licenses[i].Libary, Link: data.Licenses[i].Link, Type: data.Licenses[i].Type });
    }

    self.getDownloadLink = function (data, ev) {
        if (!data.hasOwnProperty("Link") || data.Link == "")
            return;

        document.location.href = baseSiteURL + "Information/DownloadLicenseAgreement?filename=" + data.Link;
    }

}