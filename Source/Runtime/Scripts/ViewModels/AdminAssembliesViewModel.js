﻿function AdminAssembliesViewModel() {
    var self = this;

    //Private Member Start
    var assemblyTypes = [{ Id: "", Title: "(All)" }, { Id: "Lib", Title: "Lib" }, { Id: "Extension", Title: "Extension" }, { Id: "MVC", Title: "MVC" }, { Id: "MVCLib", Title: "MVCLib" }, { Id: "Runtime", Title: "Runtime" }, { Id: "CodeBehind", Title: "CodeBehind" }, { Id: "Global", Title: "Global" }]; //TODO das ist statisch?!
    var lastCacheUpdate = null;
    var cachedRetData = null;

    var doFiltering = function (filter, retData) {

        if (filter != null && retData != null) {
            var properties = Object.getOwnPropertyNames(filter);
            var hasAnyFilterStatement = 0;
            for (var iP = 0; iP < properties.length; iP++) {
                var value = filter[properties[iP]];
                if (value != undefined && (value != "" || (typeof (value) == typeof (true)) || (typeof (value) == typeof (0)))) {
                    hasAnyFilterStatement++;
                }
            }
            

            if (hasAnyFilterStatement > 0) {
                var retFiltered = [];

                for (var i = 0; i < retData.length; i++) {
                    var element = retData[i];

                    var fittsTheCriteria = 0;
                    for (var iP = 0; iP < properties.length; iP++) {
                        var value = filter[properties[iP]];
                        if (value != undefined && (value != "" || (typeof (value) == typeof (true)) || (typeof(value) == typeof(0)))) {
                            var elementValue = element[properties[iP]];
                            if (elementValue == undefined || elementValue == null || elementValue === value) {
                                fittsTheCriteria++;
                            } else if (typeof (elementValue) == typeof (value) && typeof (elementValue) == typeof ("") && elementValue.indexOf != undefined) {
                                if (elementValue.indexOf(value) >= 0) {
                                    fittsTheCriteria++;
                                }
                            }
                        }
                    }

                    if (fittsTheCriteria == hasAnyFilterStatement) {
                        retFiltered.push(element);
                    }
                }
                retData = retFiltered;
            }
            self.AssemblyCount(retData.length);
        }
        return retData;
    };

    var ajaxLoader = {
        loadData: function (filter) {
            var ret = $.Deferred();
            if (lastCacheUpdate == null || ((new Date()).getTime() - 15000 > lastCacheUpdate.getTime())) {
                $.ajax({
                    type: "GET", url: self.AppDomainAssembliesUrl(), cache: false, contentType: "application/json", dataType: "json", data: filter, success: function (retData) {
                        cachedRetData = retData;
                        lastCacheUpdate = new Date();
                        ret.resolve(doFiltering(filter, retData));
                    }, error: function (xhr, stat, errorThrown) {
                        self.ModalVM.errorModal("Error" + stat + errorThrown);
                    }
                });
            } else {
                setTimeout(function () {
                    ret.resolve(doFiltering(filter, cachedRetData));
                }, 500);
            }
            return ret.promise();
        },
        inserItem: function () {
        },
        updateItem: function () {
        },
        deleteItem: function () {
        }
    };

    //Private Member Ende

    self.ModalVM = {};
    self.Sessions = ko.observableArray();
    self.AssemblyCount = ko.observable();
    self.AppDomainAssembliesUrl = ko.observable();

    self.AssembliesGridVM = ko.observable({
        width: "100%",
        height: "500px",

        inserting: false,
        editing: false,
        sorting: true,
        paging: false,
        selecting: true,
        pageLoading: false,
        filtering: true,
        autoload: true,
        pageSize: 1000,

        loadIndication: true,
        loadIndicationDelay: 1000,
        loadMessage: "Loading Assemblies. Please wait...",

        controller: ajaxLoader,

        noDataContent: "No data available",

        fields: [
            { name: "Name", title: "Name", type: "text" },
            { name: "AssemblyType", title: "Type", type: "select", items: assemblyTypes, valueField: "Id", textField: "Title", width: 40 },
            { name: "FullName", title: "FullName", type: "text" },
            { name: "IsDynamic", title: "IsDynamic", type: "checkbox", width: 40 },
            { type: "control", deleteButton: false, editButton: false, clearFilterButton: true, width: 30 }]
    });

    self.refresh = function () {
        if (self.AssembliesGridVM().jsGridObject != null) {
            self.AssembliesGridVM().jsGridObject.jsGrid("loadData");
        }

    };

    self.init = function (data) {
        self.AppDomainAssembliesUrl(baseSiteURL + "Admin/GetAppDomainAssemblies");
    };
};