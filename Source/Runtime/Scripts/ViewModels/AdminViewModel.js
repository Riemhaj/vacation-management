﻿
function AdminViewModel() {
    // Vars
    var self = this;
    self.isAdvancedMode = ko.observable(false);
    self.activeTemplate = ko.observable("");
    self.testvar = ko.observable("");

  


    // Subscribes
    self.isAdvancedMode.subscribe(function (newValue) {
        if (newValue)
            return;

        if (self.activeTemplate() == 'logTmpl' || self.activeTemplate() == 'reportsTmpl'
            || self.activeTemplate() == 'configurationTmpl') {
            self.activeTemplate('installationTmpl');
        }

    });

    //Methods
    self.showTemplate = function (data) {
        if (data == self.activeTemplate()) {
            self.activeTemplate("emptyTmpl");
        }
            self.activeTemplate(data);
    };

    self.logout = function (data, ev) {
        self.server.post(baseSiteURL + "Admin/Logout", { }, function (data) {
            if (data.Success) {
                document.location.href = baseSiteURL;
            }
        });
    };


    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
            },
            dataType: 'json'
        });
    };
    


}