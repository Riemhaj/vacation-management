﻿function AdminMaintenanceViewModel(maintenancemodeObs) {
    //Vars
    var self = this;
    self.ModalVM = {};
    self.StatisticVM = maintenancemodeObs;
    self.ConsoleVM = {};
    self.blockView = ko.observable(false);

    self.modalTitle = ko.observable("");
    self.modalText = ko.observable("");
    self.modalCloseBtn = ko.observable(false);
    self.modalBar = ko.observable("progress-bar-info");

    self.isMaintenanceMode = ko.computed({
        read: function () {
            return self.StatisticVM.statistics.IsMaintenanceMode();
        },
        write: function (value) {
            self.StatisticVM.statistics.IsMaintenanceMode(value);
        }
    });

    self.btnText = ko.computed(function () {
        if (self.isMaintenanceMode()) {
            return 'Deactivate maintenance mode';
        }
        else {
            return 'Activate maintenance mode';
        }
    });

    //Methods

    self.btnClick = function (obj,ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
        return;
        var newMaintenanceMode = !self.isMaintenanceMode();
        var title = newMaintenanceMode ? 'Activate maintenance mode' : 'Deactivate maintenance mode';


        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal(title, "Waiting for response...");
            self.server.post(baseSiteURL + "Installation/SetMaintenanceMode", {mode: newMaintenanceMode}, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);
                    self.isMaintenanceMode(newMaintenanceMode);
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");
            }, function (data) {
                self.ModalVM.errorModal(data);
                $(ev.currentTarget).removeClass('disabled');
            });
        }

    };

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }


    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success, error,complete) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
                else if ($.isFunction(error)) {
                    error("An error occurred: " + status + " Error: " + thrownError);
                }
            },
            complete: function () {
                if ($.isFunction(complete)) {
                    complete();
                }
            },
            dataType: 'json',
            traditional: true
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            error: function (xhr, status, thrownError) {
                if ($.isFunction(error)) {
                    error("An error occurred: " + status + " Error: " + error);
                }
            },
            dataType: 'json'
        });
    };

};
