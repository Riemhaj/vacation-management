﻿function ConsoleViewModel() {
    // Vars
    var self = this;

    self.messages = ko.observableArray([]);

    // Methoden
    self.push = function (msg) {
        if (!msg)
            return;
        self.messages.push(msg);
    };

    self.scrollToBottom = function (data,ev) {
        $(ev.currentTarget).scrollTop(999999);
    }
}