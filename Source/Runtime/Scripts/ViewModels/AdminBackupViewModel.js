﻿function AdminBackupViewModel() {
    //Vars
    var self = this;
    self.ModalVM = {};
    self.ConsoleVM = {};
    self.InstallationVM = {};
    self.AdminVM = {};

    self.backups = ko.observableArray([]);
    self.visibleCreateBackupBtn = ko.observable(true);
    self.backupComment = ko.observable("");
    self.topLevelPrincipals = ko.observableArray([]);
    self.isMultiPrincipalMode = ko.observable(false);
    self.selectedTopLevelPrincipalsList = new Array();
    self.selectedTopLevelPrincipals = ko.computed(function () {
        self.selectedTopLevelPrincipalsList.length = 0;
        var ret = "";
        ko.utils.arrayForEach(self.topLevelPrincipals(), function(i) {
            if (i.selected()) {
                if (ret.length > 0) {
                    ret += ", ";
                }
                self.selectedTopLevelPrincipalsList.push(i.Id);
                ret += i.Id;
            }
        });
        return ret;
    })

    self.isMultiPrincipalMode.subscribe(function (value) {
        self.topLevelPrincipals.removeAll();

        if (value) {
            self.server.get(baseSiteURL + "Admin/GetTopLevelPrincipals",
                function (principalList) {
                    if (principalList != null && Array.isArray(principalList)) {
                        for (var principalIdx in principalList) {
                            var model = principalList[principalIdx];
                            model.selected = ko.observable(false);
                            self.topLevelPrincipals.push(model);
                        }
                    }
                },
                {});
        }
    });

    self.init = function (data) {
        //if (data)
        //    self.backups(data);
    }

    self.hideCreateBackupBtn = function () {
        self.visibleCreateBackupBtn(false);
    };

    self.showCreateBackupBtn = function () {
        self.visibleCreateBackupBtn(true);
    };

    self.createBackup = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Create Backup", "All background processes will be stopped immediately.");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.modalSecondaryBtnText("Cancel");
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalCloseBtn(true);

            var option1 = {
                value: ko.observable(false),
                text: 'Automatically shutdown after backup and switch in maintenance-mode.',
                id: '1'
            };
            var option2 = {
                value: ko.observable(true),
                text: 'Enforce a logout for all Users (recommended)',
                id: '2'
            }
            var option3 = {
                value: ko.observable(true),
                text: 'Include any files',
                id: '3'
            };

            self.ModalVM.options.push(option2);
            self.ModalVM.options.push(option1);

            if (self.AdminVM.isAdvancedMode()) {
                self.ModalVM.options.push(option3);
            }

            self.ModalVM.setActionAfterHide(function () {
                var shutDownAfterBackpup = option1.value();
                var includeAnyFiles = option3.value();
                var forceUserLogout = option2.value();
                
                showBackupModal(shutDownAfterBackpup, includeAnyFiles, forceUserLogout, self.AdminVM.isAdvancedMode() ? self.selectedTopLevelPrincipalsList : []);
            });

        }
    };

    function showBackupModal(shutDownAfterBackpup, includeAnyFiles, forceUserLogout, selectedPrincipals) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Create Backup", "Please wait...");
            self.server.post(baseSiteURL + "Backup/CreateBackup", { comment: self.backupComment(), shutDownAfterBackup: shutDownAfterBackpup, includeAnyFiles: includeAnyFiles, forceUserLogout: forceUserLogout, selectedTopLevelPrincipals: selectedPrincipals}, function (data) {
                if (responseSuccess(data)) {
                    self.ModalVM.successModal(data.Message);
                    self.backupComment("");
                    self.getBackups();
                }
                else if (data.hasOwnProperty("Message")) {
                    self.ModalVM.modalSecondaryBtnVisible(true);
                    self.ModalVM.modalSecondaryBtnText("Download log-files");

                    self.ModalVM.setSecondaryActionAfterHide(function () {
                        window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                    });
                    self.ModalVM.errorModal(data.Message);
                }
                else
                    self.ModalVM.errorModal("Unknown Error");

                self.visibleCreateBackupBtn(true);
            }, function (data) {
                self.ModalVM.errorModal(data);
            });
        }
    }

    self.getBackups = function () {

        self.server.get(baseSiteURL + "Backup/GetBackups", function (data) {
            if (responseSuccess(data)) {
                if (data.hasOwnProperty("Backups")) {
                    self.backups.removeAll();

                    for (var backupIdx in data.Backups) {
                        var backup = data.Backups[backupIdx];
                        backup.isABM = ko.observable(false);
                        backup.isABMImportGlobalEntities = ko.observable(true);
                        backup.ABMRestrictPrincipalList = ko.observableArray();
                        backup.isABMRestrictType = ko.observable(false);
                        backup.ABMRestrictTypeList = ko.observableArray();
                        backup.ABMRestrictSelectedTypeList = ko.observableArray();
                        backup.isABMRestrictPDOID = ko.observable(false);
                        backup.ABMRestrictPDOIDList = ko.observable("");
                        self.backups.push(backup);
                    }
                }
            }
            else if (data.hasOwnProperty("Message")) {

            }

        }, {});

    };

    self.showConflicts = function (obj, ev) {
        if (!obj.hasOwnProperty("File"))
            return;

        window.open(baseSiteURL + "Backup/GetConflicts?filename=" + obj.File, "_blank");
    };

    self.showPrincipalImport = function(obj, ev) {
        if (!obj.hasOwnProperty("File") || !obj.hasOwnProperty("isABM"))
            return;


        ko.utils.arrayForEach(self.backups(),
            function(item) {
                if (item != obj) {
                    item.isABM(false);
                }
            });

        var toggleABM = obj.isABM();
        obj.isABM(!toggleABM);

        if (obj.isABM()) {
            self.server.get(baseSiteURL + "Backup/GetBackupManifest",
                function(manifest) {
                    obj.ABMRestrictPrincipalList.removeAll();
                    obj.ABMRestrictTypeList.removeAll();

                    for (var idx in manifest.AllExportedPrincipals) {
                        var principal = manifest.AllExportedPrincipals[idx];
                        if (principal.IsTopLevel) {
                            principal.selected = ko.observable(false);
                            obj.ABMRestrictPrincipalList.push(principal);
                        }
                    }
                    obj.ABMRestrictTypeList(manifest.AllExportedTypes);
                },
                { filename: obj.File });
        }
    };

    self.importAdvancedBackupMode = function (obj, ev) {

        //Welche Mandanten wurden ausgewaehlt?
        var selectedPrincipalList = ko.utils.arrayFilter(obj.ABMRestrictPrincipalList(),
            function (item) { return item.hasOwnProperty('selected') && item.selected() === true; });

        var errorMessage;
        if (selectedPrincipalList.length === 0) {
            errorMessage = 'At least one selected principal is necessary';
        }
        else if (obj.isABMRestrictType() && obj.ABMRestrictSelectedTypeList().length === 0) {
            errorMessage = 'At least one selected type is necessary';
        }
        else if (obj.isABMRestrictPDOID() && obj.ABMRestrictPDOIDList() === '') {
            errorMessage = 'At least one PDO ID is necessary';
        }

        if (errorMessage) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Validation failed', errorMessage);
            self.ModalVM.withProgressBar(false);
            self.ModalVM.modalCloseBtn(true);
            return;
        }

        var ret = {
            IsImportGlobalEntities: obj.isABMImportGlobalEntities(),
            RestrictPrincipalList: ko.utils.arrayMap(selectedPrincipalList, function(item) { return item.PDO_ID; }),
            RestrictTypeList: obj.isABMRestrictType() ? obj.ABMRestrictSelectedTypeList() : [],
            RestrictPDO_IDCSVList: obj.isABMRestrictPDOID() ? obj.ABMRestrictPDOIDList() : ''
        };

        //Bestaetigung
        if (self.ModalVM.isFree()) {

            var body = '<p><label>Selected principals</label><br>';

            var principalValues = ko.utils.arrayMap(selectedPrincipalList, function (item) {
                return '' + item.Name + ' (' + item.PDO_ID + ')';
            });

            body += principalValues.join('<br>') + "<br></p>";

            body += '<p><label>Selected types</label><br/>';

            body += ret.RestrictTypeList.join('<br>') + "</p>";

            body += '<p><label>Selected PDO_IDs</label><br>' + ret.RestrictPDO_IDCSVList + '<br></p>';

            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Confirm your input', body);
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                //Nach der Bestaetigung...
                if (self.ModalVM.isFree()) {
                    self.ModalVM.showModal("Import Backup in advanced mode", "Please wait...");


                    callImport({ filename: obj.File, withDrop: false, incrementalUpdate: true, onlyImport: true, advancedImportModel: ret }, function () {
                        self.ModalVM.completeModal("Import Backup in advanced mode done.");
                    });
                }
            });
        }
    };

    self.showImport = function (obj, ev) {
        $(ev.currentTarget).parent().hide();
        $(ev.currentTarget).parent().parent().find('button').show();
    };

    self.abortImport = function (obj, ev) {
        $(ev.currentTarget).parent().find('.hideForImport').show();
        $(ev.currentTarget).parent().find('button').hide();
    };

    self.showDropAndImportModal = function (obj, ev) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Drop Schema and import Backup', "Caution: The database contains data.<br />All your data will be lost. Make sure, that you saved your data!");
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                self.dropAndImport(obj, false, false);
            });
        }
    }

    self.showUpdateSchemaAndMetaModal = function (obj, ev) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Update Schema and Metainformations (experimental)', "Caution: If the updating process fails, your System will be broken.<br />Make sure, that you saved your data!");
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                self.dropAndImport(obj, true, false);
            });
        }
    }

    self.showDropAndOnlyImportModal = function (obj, ev) {
        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal('<span class="glyphicon glyphicon-warning-sign"></span> Drop Schema and importing data without Migration (experimental)', "Caution: All your data will be lost. Make sure, that you saved your data! <br /> If the updating process fails, your System will be broken.<br />Make sure, that you saved your data!");
            self.ModalVM.setButtonText("Continue");
            self.ModalVM.withProgressBar(false);
            self.ModalVM.coloredButtons(true);
            self.ModalVM.modalCloseBtn(true);
            self.ModalVM.modalSecondaryBtnVisible(true);
            self.ModalVM.modalSecondaryBtnText("Abort");

            self.ModalVM.setActionAfterHide(function () {
                self.dropAndImport(obj, false, true);
            });
        }
    }    

    self.dropAndImport = function (obj, onlyPatch, onlyImport) {
        if (self.ModalVM.isFree()) {

            var message = "Drop Schema and import Backup";
            if (onlyPatch)
                message = "Updating Database-Schema and Metainformations";

            self.ModalVM.showModal(message, "Please wait...");

            callImport({
                    filename: obj.File,
                    withDrop: !onlyPatch,
                    incrementalUpdate: onlyPatch,
                    onlyImport: onlyImport
                },
                function() {
                    self.ModalVM.completeModal("Import Backup done.");
                    self.ConsoleVM.push("Drop and Import done.");
                });
        }
    };

    self.importBackup = function (obj, ev) {
        if ($(ev.currentTarget).hasClass('disabled'))
            return;

        self.abortImport(obj, ev);
        if (!obj.hasOwnProperty("File"))
            return;

        if (self.ModalVM.isFree()) {
            self.ModalVM.showModal("Import Backup", "Please wait...");


            callImport({ filename: obj.File }, null);
        }
    };

    self.downloadBackup = function (obj, ev) {
        if (!obj.hasOwnProperty("File"))
            return;

        document.location.href = baseSiteURL + "Backup/downloadBackup?filename=" + obj.File;
    };


    var responseSuccess = function(data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    };

    var callImport = function(payload, completeFunction) {
        self.server.post(baseSiteURL + "Backup/ImportBackup", payload, function (data) {
            if (responseSuccess(data)) {
                //self.ModalVM.setActionAfterHide(self.InstallationVM.startApplication);

                self.ModalVM.setButtonText("Startup RAD Application");
                self.ModalVM.setActionAfterHide(function () {

                    if (self.ModalVM.isFree()) {
                        self.ModalVM.showModal("Starting Application", "Waiting for response...");
                        self.server.post(baseSiteURL + "Installation/StartApplication", {}, function (data) {
                            self.ModalVM.hideModal();
                            window.open(baseSiteURL, "_blank");
                            document.location = baseSiteURL + "Admin";
                        });
                    }

                });
                if (data.Notification) {
                    self.ModalVM.setNotificationText(data.Notification.replace(/(\r\n|\n|\r)/g, "<br />"));
                }

                self.ModalVM.successModal(data.Message);
            }
            else if (data.hasOwnProperty("Error") && data.hasOwnProperty("Message")) {
                self.ModalVM.modalSecondaryBtnVisible(true);
                self.ModalVM.modalSecondaryBtnText("Download log-files");

                self.ModalVM.setSecondaryActionAfterHide(function () {
                    window.open(baseSiteURL + "Log/DownloadLog", "_blank");
                });
                self.ModalVM.errorModal(data.Message);
            }
            else
                self.ModalVM.errorModal("Unknown Error");

            self.visibleCreateBackupBtn(true);
        }, function (data) {
            self.ModalVM.errorModal(data);
        }, completeFunction);
    };

    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success, error,complete) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
                else if ($.isFunction(error)) {
                    error("An error occurred: " + status + "<br /> Error: " + thrownError);
                }
            },
            complete: function () {
                if ($.isFunction(complete)) {
                    complete();
                }
            },
            dataType: 'json'
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success, data) {
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            cache: false,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            dataType: 'json'
        });
    };

};