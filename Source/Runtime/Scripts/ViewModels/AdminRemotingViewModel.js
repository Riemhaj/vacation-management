﻿function AdminRemotingViewModel() {
    var self = this;

    self.radEnvironmentServiceURL = ko.observable("");
    self.radRemotingAssemblyDownloadLink = ko.observable("");

    self.init = function (data) {

        var browserUrl = window.location.href;
        var indexOf = browserUrl.lastIndexOf('/Admin');
        if (indexOf >= 0)
            browserUrl = browserUrl.substring(0, indexOf + 1);


        self.radEnvironmentServiceURL(browserUrl + data.radEnvironmentServiceURL);
        self.radRemotingAssemblyDownloadLink(baseSiteURL + "Admin/RemotingAssembly");
    };
};