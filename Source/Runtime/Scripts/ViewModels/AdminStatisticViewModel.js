﻿function AdminStatisticViewModel() {
    
    var self = this;
    self.statistics = {};
    self.ModalVM = {};
    self.statistics.MachineName = ko.observable();
    self.statistics.OSVersion = ko.observable();
    self.statistics.Is64Bit = ko.observable();
    self.statistics.Processor = ko.observable();
    self.statistics.ProcessorCount = ko.observable();
    self.statistics.ProcessID = ko.observable();
    self.statistics.ThreadCount = ko.observable();
    self.statistics.CPUUsage = ko.observable();
    self.statistics.FreeRAMMB = ko.observable();
    self.statistics.MemoryUsageMB = ko.observable();
    self.statistics.SessionCount = ko.observable();
        
    self.statistics.IsPDMetaInitMode = ko.observable();
    self.statistics.IsDisableJobsMode = ko.observable();
    self.statistics.IsHistorizationMode = ko.observable();
    self.statistics.IsNoIntercepterMode = ko.observable();
    self.statistics.IsMetaGeneratorMode = ko.observable();
    self.statistics.IsJournalEnabled = ko.observable();
    self.statistics.IsMaintenanceMode = ko.observable();
    self.statistics.DatabaseStatisticDownloadLink = ko.observable();

    self.RBSGroupNames = ko.observableArray();    

    var optionsPlot =
    {
        xaxis: { mode: "time" },
        lines: { show: true },
        points: { show: false },
        select: { mode: "xy" },
        legend: { show: true, position: "nw" },
        selection: { mode: "x" }
    };

    // Methods

    self.init = function (data,GroupNames) {
        self.RBSGroupNames(GroupNames);

        self.statistics.ChangeHibernateStatisticMode = function (toggle) {
            var title = toggle ? "Enable Database-Statistic" : "Disable Database-Statistic";
            if (self.ModalVM.isFree()) {
                self.ModalVM.showModal(title, "Waiting for response...");
                self.server.get(baseSiteURL + "Admin/ChangeHibernateStatisticMode?toggle="+toggle, function (data) {
                    if (responseSuccess(data)) {
                        self.ModalVM.successModal(data.Message);

                        self.ModalVM.hideModal();
                    }
                    else if (data.hasOwnProperty("Message")) {
                        self.ModalVM.errorModal(data.Message);
                    }
                    else
                        self.ModalVM.errorModal("Unknown Error");
                });
            }
        };

        self.statistics.DatabaseStatisticDownloadLink(baseSiteURL + "Admin/DatabaseStatistic");

        self.statistics.MachineName = data.MachineName;
        self.statistics.OSVersion = data.OSVersion;
        self.statistics.Is64Bit = data.Is64Bit;
        self.statistics.Processor = data.Processor;
        self.statistics.ProcessorCount = data.ProcessorCount;
        self.statistics.ProcessID = data.ProcessID;
        self.statistics.ThreadCount = data.ThreadCount;
        self.statistics.CPUUsage = data.CPUUsage;
        self.statistics.FreeRAMMB = data.FreeRAMMB;
        self.statistics.MemoryUsageMB = data.MemoryUsageMB;
        self.statistics.SessionCount = data.SessionCount;

        self.statistics.IsStarted = data.IsStarted;
        self.statistics.ServerNodeCount = data.ServerNodeCount;
        self.statistics.ServerNodeCountStarted = data.ServerNodeCountStarted;
        self.statistics.IsMasterNode = data.IsMasterNode;
        self.statistics.NodeID = data.NodeID;
        self.statistics.DBConnectionWaterlevel = data.DBConnectionWaterlevel;

        self.statistics.IsMaintenanceMode(data.IsMaintenanceMode);
        self.statistics.IsPDMetaInitMode(data.IsPDMetaInitMode);
        self.statistics.IsDisableJobsMode(data.IsDisableJobsMode);
        self.statistics.IsHistorizationMode(data.IsHistorizationMode);
        self.statistics.IsNoIntercepterMode(data.IsNoIntercepterMode);
        self.statistics.IsMetaGeneratorMode(data.IsMetaGeneratorMode);
        self.statistics.IsJournalEnabled(data.IsJournalEnabled);

    }

    self.startpolling = function () {
        for (var i in self.RBSGroupNames()) {

            var placeholder = $("#placeholderPlot_" + self.RBSGroupNames()[i]);

            $.plot(placeholder, [], optionsPlot);

                self.getPlotData(self.RBSGroupNames()[i]);
           
        }

        //initalisieren des scrollspy
        $('.statsContent').scrollspy({ target: '#menu',offset:30 });
  
    }

    self.getPlotData = function (groupName) {
        self.server.post(baseSiteURL+"Statistics/GetPlotData",{plotName:groupName},function(data){
            if(responseSuccess(data))
            {
                if ($("#placeholderPlot_" + groupName).width() == null)
                    return;
                plotData(data);
                setTimeout(function () {
                    self.getPlotData(groupName);
                }, 1000 * 30);
            }
        });
        
    };

    var plotData = function (curves) {
        $.plot($("#placeholderPlot_" + curves.GroupName),curves.Data, optionsPlot);

        $("#placeholderPlot_" + curves.GroupName).bind("plotselected", function (event, ranges) {
            plot = $.plot($("#placeholderPlot_" + curves.GroupName), curves.Data, 
                    $.extend(true, {},optionsPlot, 
                            { xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }}));
        });

        };

    var responseSuccess = function (data) {
        if (data.hasOwnProperty("Success"))
            if (data.Success == true) {
                return true;
            }

        return false;
    }

    //SERVER REQUESTS
    self.server = {};
    self.server.post = function (url, data, success) {
        var msg = new Message(function (data) {
            if (data) {
                self.ModalVM.pushMessage(data);
                self.ConsoleVM.push(data);
            }
        });
        $.extend(data, { uuid: msg.getUUID() });

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (returnData) {
                if ($.isFunction(success)) {
                    success(returnData);
                }
            },
            error: function (xhr, status, thrownError) {
                if (xhr.status == 403) {
                    self.ModalVM.showErrorAndLogout();
                }
            },
            dataType: 'json'
        });
        msg.enableServerMessageDispatcher();
    };

    self.server.get = function (url, success) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
            },
            dataType: 'json'
        });
    };
};