﻿ko.bindingHandlers.fadeToggle = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        $(element).click(function () {
            $(value).stop(true, true).slideToggle('fast');
        });
    }
};

ko.bindingHandlers.jsGrid = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        value.jsGridObject = $(element).jsGrid(value);
    }
};

ko.bindingHandlers.pressEnter = {
    init: function (element, valueAccessor) {
        var val = valueAccessor();
        if (!$.isFunction(val))
            return;
        $(element).keyup(function (event) {
            if (event.keyCode == 13) {
                val("",event);
            }
        });
    }
}

ko.bindingHandlers.slideToggle = {
    update: function (element, valueAccessor) {
        var value = valueAccessor();

        if (value() == true) {
            $(element).stop(true, true).slideDown('fast');
        }
        if (value() == false && $(element).css('display') !== 'none') {
            $(element).stop(true, true).slideUp('fast');
        }

    }
};

ko.bindingHandlers.templateLoaderHelper = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        if (value.hasOwnProperty('Name')) {
            if ($.isFunction(value.Name))
                var Name = value.Name();
        }
        if (Name != "" && $('#' + Name).length > 0)
            ko.renderTemplate(Name, bindingContext['$root'], null, element);
        return { controlsDescendantBindings: true };
    }
};

ko.bindingHandlers.dateTime = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        if ($.isFunction(value))
            value = value();

        var date = new Date(parseInt(value.substr(6)));
        if (date != "Invalid Date") {
            var year = date.getFullYear();
            var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            var month = date.getMonth() + 1;
            month = month < 10 ? '0' + month : month;
            var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
            var minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

            var dateString = day + '.' + month + '.' + year + ' ' + hour + ':' + minute + ':' + seconds;

            $(element).html(dateString);
        }
    }
};

ko.bindingHandlers.fileSize = {
    update: function (element, valueAccessor) {
        var fileSizeInBytes = ko.unwrap(valueAccessor());
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        var erg = Math.max(fileSizeInBytes, 0.1).toFixed(2) + byteUnits[i];

        $(element).text(erg);
    }
};

ko.bindingHandlers.creationTimeTitle = {
    update: function (element,valueAccessor) {
        var value = valueAccessor();
        if ($.isFunction(value))
            value = value();

        var date = new Date(parseInt(value.substr(6)));
        if (date != "Invalid Date") {
            var year = date.getFullYear();
            var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            var month = date.getMonth() + 1;
            month = month < 10 ? '0' + month : month;
            var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
            var minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

            var dateString = day + '.' + month + '.' + year + ' ' + hour + ':' + minute + ':' + seconds;

            $(element).attr("title","creation time: "+dateString);
        }
    }
}

ko.bindingHandlers.backupTable = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        return ko.bindingHandlers.foreach.init(element, valueAccessor(), allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        ko.bindingHandlers.foreach.update(element, valueAccessor(), allBindingsAccessor, viewModel, bindingContext);
        $(element).find('.comment').hide();
        var commentBtn = $(element).find('.commentToggle');

        $(commentBtn).off("click");
        $(commentBtn).click(function () {
            var comment = $(this).parents('tr').find('.comment').get(0);

            if ($(this).hasClass('active')) {
                $(comment).stop(true, true).slideUp('fast');
                $(this).removeClass('active');
            } else {
                $(comment).stop(true, true).slideDown('fast');
                $(this).addClass('active');
            }
        });

    }
};

ko.bindingHandlers.animateNew = {

    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        var firstchild = $(element).find('div.hide:last-child').get(0);
        if (!$(firstchild).is(':animated')) {
            var other = $(element).find('div:not(.hide)');
            $(other).stop(true, true).show();
            $(firstchild).fadeIn('fast').removeClass('hide');
        }
    }
};

ko.bindingHandlers.scrollBottom = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();

        $(element).scrollTop(99999);
    }
};

ko.bindingHandlers.callFkt = {
    init: function(element,valueAccessor){
        var value = valueAccessor();

        if ($.isFunction(value))
            value();
    }
};

ko.bindingHandlers.dependenciesChecked = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        var dependencies = value.dependency;
        var alias = value.alias;
        var allModules = value.allModules;
        var checked = value.checkedArray;

        var checkDependenciesRecursive = function (alias) {
            var modul = getModule(alias);
            if (modul !== null) {
                for (i in modul.Dependencies) {
                    checkDependenciesRecursive(modul.Dependencies[i]);
                }
            }
            if (checked.indexOf(alias) < 0)
                checked.push(alias);
        }

        var getModule = function (alias) {
            for (i in allModules) {
                if (allModules[i].Value === alias)
                    return allModules[i];
            }
            return null;
        }

        $(element).click(function () {
            // "checked" ändert sich VOR diesem Event und zeigt daher jetzt den NEUEN Status!!
            if ($(element).prop('checked')) {
                for (i in dependencies) {
                    checkDependenciesRecursive(dependencies[i]);
                }
                checked.push(alias);
            }
            else {
                checked.remove(alias);
            }

        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        var dependencies = value.dependency;
        var alias = value.alias;
        var checked = value.checkedArray();
        
        for (i in dependencies) {
            if (checked.indexOf(dependencies[i]) < 0) {
                value.checkedArray.remove(alias);
                $(element).prop('checked', false);
                return;
            }
        }

        if (checked.indexOf(alias) > -1 && !$(element).prop('checked'))
            $(element).prop('checked', true);
    },

};


ko.bindingHandlers.levelToClass = {
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        var level = value();


        $(element).removeClass('active success warning danger info');
        switch (level.toUpperCase()) {
            case 'OFF':
                break;
            case 'FATAL':
            case 'ERROR':
                    $(element).addClass('danger');
                break;
            case 'WARN':
            case 'INFO':
            case 'DEBUG':
                $(element).addClass('warning');
                break;
            case 'ALL':
                $(element).addClass('success');
                break;
        }

    }
};

ko.bindingHandlers.nl2br = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var field = ko.utils.unwrapObservable(valueAccessor());
        if (field == null)
            field = "";
        field = field.replace(/\n/g, '<br />');
        ko.bindingHandlers.html.update(element, function () { return field; });
    }
};
