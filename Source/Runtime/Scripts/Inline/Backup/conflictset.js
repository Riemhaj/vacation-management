$(document).ready(function () {

    $("#topAccordion").accordion({
        active: false,
        collapsible: true,
        animate: false,
        heightStyle: 'content',
        autoHeight: true
    });
    $(".subAccordion").accordion({
        active: false,
        collapsible: true,
        animate: false,
        heightStyle: 'content',
        autoHeight: true
    });


    $('#hideWhenLoaded').fadeOut('fast', function () {
        $("#showWhenLoaded").fadeIn('fast');
    });
});