﻿var licenseData = JSON.parse($('#licenseData').html());
var isFullScreen = JSON.parse($('#isFullScreen').html());

ViewModels.LicenseVM = new LicenseAgreementsViewModel();
ViewModels.LicenseVM.init(licenseData, isFullScreen);