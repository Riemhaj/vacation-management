var installationData = JSON.parse($('#installationData').html());
var maintenanceData = JSON.parse($('#maintenanceData').html());
var backupData = JSON.parse($('#backupData').html());
var statisticsData = JSON.parse($('#statisticsData').html());
var statisticsRBSGroupNames = JSON.parse($('#statisticsRBSGroupNames').html());
var logs = JSON.parse($('#logs').html());
var logger = JSON.parse($('#logger').html());
var configuration = JSON.parse($('#configuration').html());

ViewModels.ModalVM = new ModalWindowViewModel();
ViewModels.ModalVM.init();
ViewModels.ConsoleVM = new ConsoleViewModel();
ViewModels.AdminViewModel = new AdminViewModel();
ViewModels.AdminInstallationVM = new AdminInstallationViewModel();
ViewModels.AdminInstallationVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminInstallationVM.ConsoleVM = ViewModels.ConsoleVM;
ViewModels.AdminInstallationVM.init(installationData);
ViewModels.AdminStatisticVM = new AdminStatisticViewModel();
ViewModels.AdminStatisticVM.init(statisticsData, statisticsRBSGroupNames);
ViewModels.AdminStatisticVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminMaintenanceVM = new AdminMaintenanceViewModel(ViewModels.AdminStatisticVM);
ViewModels.AdminMaintenanceVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminMaintenanceVM.ConsoleVM = ViewModels.ConsoleVM;
ViewModels.AdminBackupVM = new AdminBackupViewModel();
ViewModels.AdminBackupVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminBackupVM.ConsoleVM = ViewModels.ConsoleVM;
ViewModels.AdminBackupVM.InstallationVM = ViewModels.AdminInstallationVM;
ViewModels.AdminBackupVM.AdminVM = ViewModels.AdminViewModel;
ViewModels.AdminBackupVM.init(backupData);
ViewModels.AdminBenchmarkVM = new AdminBenchmarkViewModel();
ViewModels.AdminBenchmarkVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminBenchmarkVM.ConsoleVM = ViewModels.ConsoleVM;
ViewModels.AdminLogVM = new AdminLogViewModel();
ViewModels.AdminLogVM.init(logs, logger);
ViewModels.AdminLogVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminViewReportVM = new AdminViewReportViewModel();
ViewModels.AdminViewReportVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminConfigurationVM = new AdminConfigurationViewModel();
ViewModels.AdminConfigurationVM.ModalVM = ViewModels.ModalVM;
ViewModels.AdminConfigurationVM.init(configuration);
ViewModels.AdminRemotingViewModel = new AdminRemotingViewModel();
ViewModels.AdminRemotingViewModel.init(configuration);
ViewModels.AdminSessionViewModel = new AdminSessionViewModel();
ViewModels.AdminSessionViewModel.ModalVM = ViewModels.ModalVM;
ViewModels.AdminSessionViewModel.init(configuration);
ViewModels.AdminLockViewModel = new AdminLockViewModel();
ViewModels.AdminLockViewModel.ModalVM = ViewModels.ModalVM;
ViewModels.AdminLockViewModel.init(configuration);
ViewModels.AdminAssembliesViewModel = new AdminAssembliesViewModel();
ViewModels.AdminAssembliesViewModel.ModalVM = ViewModels.ModalVM;
ViewModels.AdminAssembliesViewModel.init(configuration);


$(document).ready(function () {
    setTimeout(function () {
        ViewModels.AdminViewModel.showTemplate("startsiteTmpl");
    }, 100);
});