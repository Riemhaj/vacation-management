$(document).ready(function () {
    $("#topAccordion").accordion({
        active: false,
        collapsible: true,
        heightStyle: 'content'
    });
    $(".subAccordion").accordion({
        active: false,
        collapsible: true,
        heightStyle: 'content'
    });
});